$.ajaxSetup({
        headers: {
          'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
        }
});
var pusher = new Pusher('5f84d2a344876b9fea04', {
        cluster: 'ap1',
        forceTLS: true
    });
var channel = pusher.subscribe('chat-message');
channel.bind('send-message', function (data) {
    if($('#private_id').val() === data.to){
        if($('#count-message').html() === ''){
             $('#count-message').html(1);
        }else{
             $('#count-message').html(parseInt($('#count-message').html()) + 1);
        }
    }
});
