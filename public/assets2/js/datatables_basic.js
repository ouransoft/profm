/* ------------------------------------------------------------------------------
 *
 *  # Basic datatables
 *
 *  Demo JS code for datatable_basic.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var DatatableBasic = function () {


    //
    // Setup module components
    //

    // Basic Datatable examples
    var _componentDatatableBasic = function () {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }
        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Lọc:</span> _INPUT_',
                searchPlaceholder: 'Nhập từ khóa tìm kiếm',
                lengthMenu: '<span>Hiển thị:</span> _MENU_',
                paginate: {'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'},
                info: "Hiển thị từ _START_ đến _END_ trên _TOTAL_ kết quả "
            }
        });

        // Basic datatable
        $('.datatable-basic').DataTable({
            searching: false,
            paging: true, 
            lengthChange: false,
            info: false,
        });
        $('.datatable-todo').DataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "bAutoWidth": false
        });
        $('.datatable-project').DataTable({
            "pagingType": "simple",
            columnDefs: [
             { type: 'date-uk', targets: 7 }
            ],
            dom: '<"datatable-header"lip><"datatable-scroll"t>',
            language: {
                lengthMenu: '<span>Hiển thị:</span> _MENU_',
                paginate: {
                         previous: "<i class='icon icon-circle-left2'>",
                         next: "<i class='icon icon-circle-right2'>"
                     },
                info: " _START_ - _END_ trong số _TOTAL_  "
            },
            searching: false,
            info: " _START_ - _END_ trong số _TOTAL_  "
        });
        $('.datatable-project-mobile').DataTable({
            "pagingType": "simple",
            dom: '<"datatable-header"ip><"datatable-scroll"t>',
            language: {
                lengthMenu: '<span>Hiển thị:</span> _MENU_',
                paginate: {
                         previous: "<i class='icon icon-circle-left2'>",
                         next: "<i class='icon icon-circle-right2'>"
                     },
                info: "Hiển thị _START_ - _END_ trong số _TOTAL_  "
            },
            searching: false,
            info: "Hiển thị _START_ - _END_ trong số _TOTAL_  "
        });

        // Alternative pagination
        $('.datatable-pagination').DataTable({
            pagingType: "simple",
            language: {
                paginate: {'next': $('html').attr('dir') == 'rtl' ? 'Next &larr;' : 'Next &rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr; Prev' : '&larr; Prev'}
            }
        });

        // Datatable with saving state
        $('.datatable-save-state').DataTable({
            stateSave: true
        });

        // Scrollable datatable
        var table = $('.datatable-scroll-y').DataTable({
            autoWidth: true,
            scrollY: 300
        });

        // Resize scrollable table when sidebar width changes
        $('.sidebar-control').on('click', function () {
            table.columns.adjust().draw();
        });
    };

    // Select2 for length menu styling
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentDatatableBasic();
            _componentSelect2();
        }
    }
}();

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
	"date-uk-pre": function ( a ) {
		if (a == null || a == "") {
			return 0;
		}
		var ukDatea = a.split('/');
		return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
	},

	"date-uk-asc": function ( a, b ) {
		return ((a < b) ? -1 : ((a > b) ? 1 : 0));
	},

	"date-uk-desc": function ( a, b ) {
		return ((a < b) ? 1 : ((a > b) ? -1 : 0));
	}
} );
// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    DatatableBasic.init();
});
