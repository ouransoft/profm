

$.ajaxSetup({
    headers: {
        'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
    }
});
$('body').delegate('.modal-create-member', 'click', function () {
    $('#modal_create_member').modal('show');
})
function getFile() {
    document.getElementById("upfile").click();
}
function getFile1() {
    document.getElementById("upfile1").click();
}
function sub(obj) {
    var file = obj.value;
    var file_data = obj.files[0];
    $this = $(this);
    var form_data = new FormData();
    var $input = $('.image_data');
    form_data.append('file', file_data);
    $.ajax({
        url: '/api/uploadImage',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                $input.val(response.image);
                $('.image-avatar').attr('style', 'background-image:url(' + response.image + ')');
            } else {
                alert('File upload không hợp lệ');
            }

        }
    });
}
$('body').delegate('.submit-create-member', 'click', function (e) {
    e.preventDefault();
    var department_id = $.trim($('#validate_department_id').val());
    var login_id = $.trim($('#validate_login_id').val());
    var level = $.trim($('#validate_level').val());
    var full_name = $.trim($('#validate_full_name').val());
    var password = $.trim($('#validate_password').val());
    var check = true;
    if (department_id == '') {
        check = false;
        $('#error_department').html('Trường bắt buộc không được để trống');
    } else {
        $('#error_department').html('');
    }
    if (login_id == '') {
        check = false;
        $('#error_login').html('Trường bắt buộc không được để trống');
    } else {
        $('#error_login').html('');
    }
    if (level == '') {
        check = false;
        $('#error_level').html('Trường bắt buộc không được để trống');
    } else {
        $('#error_level').html('');
    }
    if (full_name == '') {
        check = false;
        $('#error_full_name').html('Trường bắt buộc không được để trống');
    } else {
        $('#error_full_name').html('');
    }
    if (password == '') {
        check = false;
        $('#error_password').html('Trường bắt buộc không được để trống');
    } else {
        $('#error_password').html('');
    }
    if (check === true) {
        $('#create_member').submit();
    } else {
        e.preventDefault();
    }
})
function sub(obj) {
    var file = obj.value;
    var file_data = obj.files[0];
    $this = $(this);
    var form_data = new FormData();
    var $input = $('.image_data');
    form_data.append('file', file_data);
    $.ajax({
        url: '/api/uploadImage',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
            if (response.success == true) {
                $input.val(response.image);
                $('.image-avatar').attr('style', 'background-image:url(' + response.image + ')');
            } else {
                alert('File upload không hợp lệ');
            }

        }
    });
}
/*$('.submit-create-member').click(function(){
    if($('.password').val() === $('.confirm-password').val()){
        $('#create_member').submit();
        var notification = notifier.notify("success", "Thêm thành viên thành công");
        notification.push();
    }else{
        var notifier = new Notifier();
        var notification = notifier.notify("warning", "Xác nhận MK không trùng khớp, mới nhập lại");
        notification.push();
    }
})*/
$('.submit-edit-member').click(function () {
    $('#update_member').submit();
})
$("#update_member").submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        type: "POST",
        url: '/api/update-member',
        data: form.serialize(),
        success: function (response) {
            if (response.success == true) {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Sửa thông tin thành công");
                notification.push();
                setTimeout(function () { location.reload(); }, 2000);
            }
        }
    });
});
$(document).ready(function () {
    $('.select-search').select2({
        allowClear: true,
    });
});
$(document).ready(function () {
    $('.select').select2({
        allowClear: true,
        minimumResultsForSearch: Infinity
    });
});
$('.old-password').blur(function () {
    var password = $(this).val();
    $this = $(this);
    $.ajax({
        url: '/api/check-password',
        method: 'POST',
        data: { password: password },
        success: function (response) {
            if (response.success == 'false' && !$this.parent().find('.invalid-feedback').length) {
                $this.parent().append(`<div class="invalid-feedback">
                                            Mật khẩu không đúng
                                        </div>`);
            } else if (response.success == 'true' && $this.parent().find('.invalid-feedback').length) {
                $this.parent().find('.invalid-feedback').remove();
            }
        }
    })
})
$('.confirm-news-password,.news-password').blur(function () {
    $this = $(this);
    var str = $(this).val();
    if (str.match(/^(?=.*[0-9])(?=.*[a-z])([a-zA-Z0-9]{6,20})$/) == null && !$this.parent().find('.invalid-feedback').length) {
        $this.parent().append(`<div class="invalid-feedback">
                                            Mật khẩu tối thiểu 6 ký tự bao gồm cả chữ số
                                        </div>`);
    } else if (str.match(/^(?=.*[0-9])(?=.*[a-z])([a-zA-Z0-9]{6,})$/) != null && $this.parent().find('.invalid-feedback').length) {
        $this.parent().find('.invalid-feedback').remove();
    }
})
$('.confirm-news-password').blur(function () {
    if ($(this).val() != $('.news-password').val()) {
        $this.parent().append(`<div class="invalid-feedback">
            Nhập lại không đúng
        </div>`);
    } else {
        $this.parent().find('.invalid-feedback').remove();
    }
});
$('#update_avatar').submit(function (e) {
    e.preventDefault();
    $this = $(this);
    $.ajax({
        url: '/api/update-info',
        method: 'POST',
        data: new FormData(this),
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
            if (response.success == 'true') {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Cập nhật thông tin thành công");
                notification.push();
                $this[0].reset();
                $('#modal_update_avatar').modal('hide');
                setTimeout(function () {
                    location.reload();
                }, 1500);

            }
        }
    })
})
$('#reset_password').submit(function (e) {
    e.preventDefault();
    $this = $(this);
    $.ajax({
        url: '/api/reset-password',
        method: 'POST',
        data: new FormData(this),
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
            if (response.success == 'true') {
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Thay đổi mật khẩu thành công");
                notification.push();
                $this[0].reset();
                $('#modal_reset_password').modal('hide');
            }
        }
    })
})
if ($('.upload-images').length) {
    $('.upload-images').each(function () {
        var images = $(this).parents('.div-image').find('.image-link').val();
        var id = $(this).parents('.div-image').find('.image-id').val();
        var name = $(this).parents('.div-image').find('.image-name').val();
        if (images === '') {
        } else {
            images = images.split(',');
            id = id.split(',');
            name = name.split(',');
            for (i = 0; i < images.length; i++) {
                if ($(this).hasClass('disable')) {
                    button_html = '';
                } else {
                    button_html = '<div class="file-footer-buttons">' +
                        '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-id="' + id[i] + '"><i class="icon-trash"></i></button>' +
                        '</div>';
                }

                $(this).parents('.div-image').find('.file-drop-zone').append('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">' +
                    '<div class="kv-file-content">' +
                    '<img src="/' + images[i] + '" class="file-preview-image kv-preview-data" title="' + name[i] + '" alt="' + name[i] + '" style="width:auto;height:auto;max-width:100%;max-height:100%;">' +
                    '</div>' +
                    '<div class="file-thumbnail-footer">' +
                    '<div class="file-footer-caption" title="' + name[i] + '">' +
                    '<div class="file-caption-info">' + name[i] + '</div>' +
                    '<div class="file-size-info"></div>' +
                    '</div>' +
                    '<div class="file-actions">' +
                    button_html +
                    '</div>' +
                    '</div>' +
                    '</div>');
            }
        }
    });

};
$('body').delegate('.upload-images', 'change', function () {
    $this = $(this);
    var file_data = [];
    var form_data = new FormData();
    var input_link = $(this).parents('.div-image').find('.image-link');
    var input_id = $(this).parents('.div-image').find('.image-id');
    var input_name = $(this).parents('.div-image').find('.image-name');
    var link_value = $(this).parents('.div-image').find('.image-link').val();
    var id_value = $(this).parents('.div-image').find('.image-id').val();
    var name_value = $(this).parents('.div-image').find('.image-name').val();
    var parent = $(this).parents('.div-image').find('.file-drop-zone');
    if (id_value === '') {
        id_value = [];
        name_value = [];
        link_value = [];
    } else {
        id_value = id_value.split(',');
        name_value = name_value.split(',');
        link_value = link_value.split(',');
    }
    for (var i = 0; i < $(this)[0].files.length; i++) {
        form_data.append('file[]', $(this).prop('files')[i]);
    }
    form_data.append('type', $(this).data('type'));
    $.ajax({
        url: '/api/upload',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function () {
            parent.addClass("loading");
        },
        success: function (response) {
            if (response.success == "true") {
                for (var i = 0; i < response.data.length; i++) {
                    data = response.data[i];
                    link_value.push(data.id);
                    id_value.push(data.id);
                    name_value.push(data.name);
                    $this.parents('.div-image').find('.file-drop-zone').append('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">' +
                        '<div class="kv-file-content">' +
                        '<img src="/' + data.link + '" class="file-preview-image kv-preview-data" title="' + data.name + '" alt="' + data.name + '" style="width:auto;height:auto;max-width:100%;max-height:100%;">' +
                        '</div>' +
                        '<div class="file-thumbnail-footer">' +
                        '<div class="file-footer-caption" title="' + data.name + '">' +
                        '<div class="file-caption-info">' + data.name + '</div>' +
                        '<div class="file-size-info"></div>' +
                        '</div>' +
                        '<div class="file-actions">' +
                        '<div class="file-footer-buttons file-button">' +
                        '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-id="' + data.id + '"><i class="icon-trash"></i></button>' +

                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');

                }
                input_link.val(link_value.join(','));
                input_id.val(id_value.join(','));
                input_name.val(name_value.join(','));
            } else {
                alert('File upload không hợp lệ');
            }

        },
        complete: function () {
            parent.removeClass("loading");
        }
    });
});
function dropHandler(ev) {
    $this = ev.target;
    var file_data = [];
    var form_data = new FormData();
    var input_id = $this.parentElement.parentElement.parentElement.getElementsByClassName('image-id')[0];
    var id_value = $this.parentElement.parentElement.parentElement.getElementsByClassName('image-id')[0].value;
    if (id_value === '') {
        id_value = [];
    } else {
        id_value = id_value.split(',');
    }
    var form_data = new FormData();
    ev.preventDefault();
    if (ev.dataTransfer.items) {
        for (var i = 0; i < ev.dataTransfer.items.length; i++) {
            if (ev.dataTransfer.items[i].kind === 'file') {
                var file = ev.dataTransfer.items[i].getAsFile();
                form_data.append('file[]', file);
            }
        }
        $.ajax({
            url: '/api/upload',
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.success == 'true') {
                    for (var i = 0; i < response.data.length; i++) {
                        html = '<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">' +
                            '<div class="kv-file-content">' +
                            '<img src="' + response.data[i].link + '" class="file-preview-image kv-preview-data" title="' + response.name[i] + '" alt="' + response.name[i] + '" style="width:auto;height:auto;max-width:100%;max-height:100%;">' +
                            '</div>' +
                            '<div class="file-thumbnail-footer">' +
                            '<div class="file-footer-caption" title="' + response.data[i].name + '">' +
                            '<div class="file-caption-info">' + response.data[i].name + '</div>' +
                            '<div class="file-size-info"></div>' +
                            '</div>' +
                            '<div class="file-actions">' +
                            '<div class="file-footer-buttons file-button">' +
                            '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-id="' + response.data[i].id + '"><i class="icon-trash"></i></button>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                        $this.innerHTML += html;
                        id_value.push(response.image[i]);
                        var res = id_value.join(',');
                        input_id.value = res;
                    }
                } else {
                    alert('File upload không hợp lệ');
                }
            }
        })
    } else {
        for (var i = 0; i < ev.dataTransfer.files.length; i++) {
            console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
        }
    }
}
$('body').delegate('.upload-image', 'change', function () {
    var file_data = $(this).prop('files')[0];
    $this = $(this);
    var form_data = new FormData();
    var input_link = $(this).parents('.div-image').find('.image-link');
    var input_id = $(this).parents('.div-image').find('.image-id');
    var input_name = $(this).parents('.div-image').find('.image-name');
    var parent = $(this).parents('.div-image').find('.file-drop-zone');
    form_data.append('file[]', file_data);
    form_data.append('type', $(this).data('type'));
    $.ajax({
        url: '/api/upload',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function () {
            parent.addClass("loading");
        },
        success: function (response) {
            if (response.success == "true") {
                var data = response.data[0];
                input_link.val(data.id);
                input_id.val(data.id);
                input_name.val(data.name);
                $this.parents('.div-image').find('.file-drop-zone').html('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">' +
                    '<div class="kv-file-content">' +
                    '<img src="/' + data.link + '" class="file-preview-image kv-preview-data" title="' + data.name + '" alt="' + data.name + '" style="width:auto;height:auto;max-width:100%;max-height:100%;">' +
                    '</div>' +
                    '<div class="file-thumbnail-footer">' +
                    '<div class="file-footer-caption" title="' + data.name + '">' +
                    '<div class="file-caption-info">' + data.name + '</div>' +
                    '<div class="file-size-info"></div>' +
                    '</div>' +

                    '</div>' +
                    '</div>');

                input_link.val(data.id);
                input_id.val(data.id);
                input_name.val(data.name);
            } else {
                alert('File upload không hợp lệ');
            }

        },
        complete: function () {
            parent.removeClass("loading");
        }
    });
});
if ($('.upload-image').length) {
    $('.upload-image').each(function () {
        var image = $(this).parents('.div-image').find('.image-link').val();
        var id = $(this).parents('.div-image').find('.image-id').val();
        var name = $(this).parents('.div-image').find('.image-name').val();
        if (image !== '') {
            $(this).parents('.div-image').find('.file-drop-zone').append(`<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">
                                                                            <div class="kv-file-content">
                                                                                <img src="` + image + `" class="file-preview-image kv-preview-data" title="` + name + `" alt="` + name + `" style="width:auto;height:auto;max-width:100%;max-height:100%;">
                                                                            </div>
                                                                            <div class="file-thumbnail-footer">
                                                                                <div class="file-footer-caption" title="` + name + `">
                                                                                <div class="file-caption-info">` + name + `</div>
                                                                                <div class="file-size-info"></div>
                                                                                </div>
                                                                                <div class="file-actions">
                                                                                    <div class="file-footer-buttons">
                                                                                        <button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Xóa ảnh" data-id="`+ id + `"><i class="icon-trash"></i></button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>`);

        }
    });
};
$('body').delegate('.kv-file-remove,.fileinput-remove', 'click', function () {
    var id = $(this).data('id');
    var input_link = $(this).parents('.div-image').find('.image-link');
    var input_id = $(this).parents('.div-image').find('.image-id');
    var input_name = $(this).parents('.div-image').find('.image-name');
    var link_value = $(this).parents('.div-image').find('.image-link').val();
    var id_value = $(this).parents('.div-image').find('.image-id').val();
    var name_value = $(this).parents('.div-image').find('.image-name').val();
    id_value = id_value.split(',');
    link_value = link_value.split(',');
    name_value = name_value.split(',');
    var index = id_value.indexOf(id.toString());
    if (index > -1) {
        link_value.splice(index, 1);
        id_value.splice(index, 1);
        name_value.splice(index, 1);
    }
    var new_link = link_value.join(',');
    var new_id = id_value.join(',');
    var new_name = name_value.join(',');
    input_link.val(new_link);
    input_id.val(new_id);
    input_name.val(new_name);
    $(this).parents('.file-preview-frame').remove();
});
$('.show-hidden').click(function () {
    if ($(this).hasClass('show')) {
        $(this).parents('.dashboard').find('.dashboardContent').hide(300);
        $(this).html('<i class="fas fa-chevron-down"></i>')
        $(this).removeClass('show');
    } else {
        $(this).parents('.dashboard').find('.dashboardContent').show(300);
        $(this).html('<i class="fas fa-chevron-up"></i>');
        $(this).addClass('show');
    }
})



