//[Dashboard Javascript]

//Project:	Indusui admin - Responsive Admin Template
//Primary use:   Used only for the main dashboard (index.html)

$(function () {

  'use strict';

        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5();

        $('.dash-chat').slimScroll({
                height: '420'
        });
        
            // bradcrumb section

            $('#thismonth').sparkline([8, 5, 4, 7, 9, 7, 10, 9], {
                            type: 'bar',
                            height: '35',
                            barWidth: '4',
                            resize: true,
                            barSpacing: '4',
                            barColor: '#4029a7'
                    });
            $('#lastyear').sparkline([8, 5, 4, 7, 9, 7, 10, 9], {
                            type: 'bar',
                            height: '35',
                            barWidth: '4',
                            resize: true,
                            barSpacing: '4',
                            barColor: '#f3000b'
                    });


}); // End of use strict

