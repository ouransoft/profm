//[Dashboard Javascript]

//Project:	Indusui admin - Responsive Admin Template
//Primary use:   Used only for the main dashboard (index.html)

$(function () {

  'use strict';
		//bootstrap WYSIHTML5 - text editor
		$('.textarea').wysihtml5();

		$('.dash-chat').slimScroll({
			height: '420'
		  });
		


		var options = {
            chart: {
                height: 395,
                type: 'bar',
            },
			colors : ['#4029a7','#128c80', '#f3000b', '#331400'],
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [{
                name: 'Tổng',
                data: [11, 10, 9, 20]
            },{
                name: 'Đúng tiến độ',
                data: [1, 5, 1, 5]
            },{
                name: 'Ngoài tiến độ',
                data: [6, 3, 6, 12]
            },{
                name: 'Không hoàn thành',
                data: [4, 2, 2, 3]
            }],
            xaxis: {
                categories: ['Tuần 1', 'Tuần 2', 'Tuần 3', 'Tuần 4'],
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val
                    }
                }
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#ticketoverview"),
            options
        );

        chart.render();


		// bradcrumb section

		$('#thismonth').sparkline([8, 5, 4, 7, 9, 7, 10, 9], {
				type: 'bar',
				height: '35',
				barWidth: '4',
				resize: true,
				barSpacing: '4',
				barColor: '#4029a7'
			});
		$('#lastyear').sparkline([8, 5, 4, 7, 9, 7, 10, 9], {
				type: 'bar',
				height: '35',
				barWidth: '4',
				resize: true,
				barSpacing: '4',
				barColor: '#f3000b'
			});


}); // End of use strict

