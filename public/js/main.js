/* global GRN_ScheduleSimpleAdd, JSON, isTouchDevice */
!function($) {
    "use strict";
    grn.base.namespace("grn.component.schedule.multi_view.main");
    var url_component = grn.component.url
      , date_component = grn.component.date
      , multi_view = grn.component.schedule.multi_view
      , default_settings = {
        container: {
            view: "#view",
            viewMain: ".view_main_area",
            calendar: ".calendar_area",
            timeSheet: ".showtime",
            timeSheetHeader: ".personal_calendar_time_sheet",
            withoutTimeEvents: ".without_time_events_area",
            allDayEvents: ".all_day_area",
            normalEvents: ".normal_events_area",
            miniCalendarNavigator: "#schedule_calendar",
            dateNavigator: ".moveButtonBlock-grn"
        },
        eventListUrl: "api/ajax_event_list",
        simpleAddUrl: url_component.page("schedule/simple_add"),
        beginDate: "",
        eventTimeUnit: 15,
        dates: [],
        today: {},
        events: {},
        initialMembers: [],
        memberList: {},
        numberOfDays: 7,
        facilityPlacement: "after_subject",
        loginUser: grn.data.login,
        enableDateFooter: true,
        enableDateNavigatorFooter: true,
        newEventId: null,
        uid: null,
        gid: null,
        gidIsEmpty: false,
        plid: "",
        refererKey: null,
        usingPrivilege: null,
        showTodos: true,
        showExpiredTodos: true,
        enableDragAndDrop: false,
        shortDateFormat: "",
        locale: "",
        viewType: ""
    };
    grn.component.schedule.multi_view.main = function(settings) {
        this.settings = $.extend({}, default_settings, settings),
        this.ignoreNextHashChange = false
    }
    ;
    var G = grn.component.schedule.multi_view.main;
    G.prototype.init = function() {
        var settings = this.settings;
        settings.today.value = date_component.parse(settings.today.value),
        settings.memberList = new multi_view.member_list,
        this._emptyMembers = false,
        this.$view = $(settings.container.view),
        this._initViewMainArea(),
        this.$currentDate = this.$view.find(".displaydate"),
        this.memberDialog = new multi_view.select_user_org_facility_dialog,
        this.memberDialog.setMemberList(settings.memberList),
        this.$openMemberDialogLink = this.$view.find("#select_users_facilities"),
        this.memberPicker = new grn.component.schedule.user_facility_picker,
        this.memberPicker.init(),
        this.memberDropdownlist = new multi_view.select_user_org_facility_dropdownlist,
        this.miniCalendarNavigator = new multi_view.mini_calendar_navigator(settings),
        this.dateNavigator = new multi_view.date_navigator(settings),
        this._initDisplayOptionsDropdownList(),
        this.settings.enableDateNavigatorFooter && (this.calendarFooter = new multi_view.calendar_footer(settings)),
        this._setTriggerJsAPIHandler(settings.jsAPIHandler),
        this._bindEvents(),
        this._loadState(),
        this._reload()
    }
    ,
    G.prototype._setTriggerJsAPIHandler = function(jsAPIHandler) {
        this.jsAPIHandler = jsAPIHandler
    }
    ,
    G.prototype._bindEvents = function() {
        this.memberDialog.on("apply", $.proxy(this.memberDialogOnApply, this)),
        this.$openMemberDialogLink.on("click", this.memberDialog.showDialog.bind(this.memberDialog)),
        this.memberPicker.on("change", $.proxy(this._membersPickerOnChange, this)),
        this.memberDropdownlist.on("select", $.proxy(this._memberDropdownlistOnSelect, this)),
        this.miniCalendarNavigator.on("change", $.proxy(this._miniCalendarNavigatorOnChange, this)),
        this.dateNavigator.on("change", $.proxy(this._dateNavigatorOnChange, this)),
        $(window).on("hashchange", $.proxy(this._onHashChange, this))
    }
    ,
    G.prototype._onHashChange = function() {
        this.ignoreNextHashChange ? this.ignoreNextHashChange = false : (this._loadState(),
        this._reload())
    }
    ,
    G.prototype._onEventDragStart = function() {
        multi_view.event_tooltip.deactivate()
    }
    ,
    G.prototype._onEventDragStop = function() {
        multi_view.event_tooltip.activate()
    }
    ,
    G.prototype._renderCalendar = function() {
        var settings = this.settings, time_sheet, normal_event_renderer, without_time_event_renderer, banner_renderer;
        this.$currentDate.text(settings.dates[0].text_full),
        new multi_view.time_sheet(settings).render(),
        new multi_view.normal_event_renderer(settings).render(),
        new multi_view.without_time_event_renderer(settings).render(),
        new multi_view.banner_renderer(settings).render(),
        isTouchDevice(),
        this.calendarFooter && this.calendarFooter.refresh()
    }
    ,
    G.prototype._initViewMainArea = function() {
        this.$viewMain = this.$view.find(this.settings.container.viewMain),
        0 === this.$viewMain.length && (this.$viewMain = this.$view),
        this.$viewMain.css("position", "relative")
    }
    ,
    G.prototype._initQuickAdd = function() {
        var settings = this.settings, self = this, qAdd = Object.create(GRN_ScheduleSimpleAdd), selected_members = [], MEMBER_TYPE = multi_view.member_list.MEMBER_TYPE, members;
        settings.memberList.getMembers().forEach(function(member) {
            if (member.selected) {
                var prefix = "";
                switch (member.type) {
                case MEMBER_TYPE.ORGANIZATION:
                    selected_members.push("g" + member.id),
                    prefix = "g";
                    break;
                case MEMBER_TYPE.FACILITY:
                    prefix = "f"
                }
                selected_members.push(prefix + member.id)
            }
        });
        var configs = $.extend({
            members: selected_members,
            onAddSuccess: function() {
                self._reload()
            }
        }, settings);
        qAdd.cfg(configs),
        $(".dialog").remove();
        var $selector = ".personal_day_calendar_time_row, .without_time_event_cell";
        this.$view.find($selector).each(function() {
            var row = this;
            row.firstInit = false,
            row.dialog = void 0
        }),
        this.$view.off("dblclick", $selector, $.proxy(qAdd.doubleClickPersonalHandler, qAdd)),
        this.$view.on("dblclick", $selector, $.proxy(qAdd.doubleClickPersonalHandler, qAdd)),
        qAdd.enableDragAdd()
    }
    ,
    G.prototype._initDisplayOptionsDropdownList = function() {
        function _update_settings() {
            self.settings.showTodos = $show_todos_checkbox.is(":checked"),
            self.settings.showExpiredTodos = $show_expired_todos_checkbox.is(":checked")
        }
        var self = this;
        this.$displayOptionDropDown = this.$view.find(".js_schedule_display_options_dropdown");
        var $show_todos_checkbox = this.$displayOptionDropDown.find(".js_show_todos input")
          , $show_expired_todos_checkbox = this.$displayOptionDropDown.find(".js_show_expired_todos input");
        this.$displayOptionDropDown.find("input").change(_update_settings),
        _update_settings()
    }
    ,
    G.prototype._reloadEvent = function() {
        var self = this, request;
        return this._fetchEvents().done(function(data) {
            self._parseEventsData(data),
            self.settings.dates = data.dates,
            self.settings.events = data.events,
            self.settings.minTime = 7,
            self.settings.maxTime = 24,
            self.settings.calendarWeekStart = data.calendarWeekStart,
            self.settings.memberList = multi_view.member_list.createFromArray(data.members, true),
            self.settings.facilityPlacement = data.facilityPlacement
        })
    }
    ,
    G.prototype._parseEventsData = function(data) {
        data.events.by_date.forEach(function(day_data) {
            var eventsNormal;
            (day_data.events.normal || []).forEach(function(event) {
                event.start_date_object = grn.component.date.parse(event.start_date),
                "end_date"in event && (event.end_date_object = grn.component.date.parse(event.end_date))
            })
        }),
        data.events.banner.forEach(function(event) {
            event.start_date_object = grn.component.date.parse(event.start_date),
            event.end_date_object = grn.component.date.parse(event.end_date)
        })
    }
    ,
    G.prototype._fetchEvents = function() {
        var self = this
          , settings = this.settings
          , request_data = {
            referer_key: settings.refererKey,
            begin_date: settings.beginDate,
            number_of_days: settings.numberOfDays,
            uid: settings.uid,
            gid: settings.gid,
            eid: settings.eid,
            selected_group_type: settings.selectedGroupType
        };
        "selected" === settings.gid && (request_data.members = settings.memberList.getMembers().map(function(member) {
            return JSON.stringify({
                id: member.id,
                type: member.type,
                selected: member.selected,
                colorId: member.colorId
            })
        })),
        "search" === settings.gid && settings.searchText && (request_data.search_text = settings.searchText),
        this._emptyMembers && (request_data.members = "empty");
        var request = new grn.component.ajax.request({
            grnUrl: this.settings.eventListUrl,
            grnLoadingIndicator: this._toggleSpinner.bind(this),
            method: "post",
            data: request_data
        });
        return request.on("errorClosed", function(e, jqXHR) {
            self._handleCybozuError(request.getErrorHandler().getCybozuError(jqXHR))
        }),
        request.send()
    }
    ,
    G.prototype._handleCybozuError = function(error) {
        switch (error) {
        case "GRN_CMMN_00105":
        case "GRN_SCHD_13203":
        case "GRN_SCHD_13050":
        case "GRN_SCHD_13042":
            this.settings.gid = "login",
            this.settings.uid = null,
            this._reloadAndSaveState()
        }
    }
    ,
    G.prototype._loadState = function() {
        var settings = this.settings
          , url_params = url_component.parseQueryString(location.search)
          , url_hash = url_component.parseHash(location.hash)
          , member_id = (url_params = $.extend(url_params, url_hash)).uid || null
          , group_id = url_params.gid || null;
        settings.gidIsEmpty = null === group_id;
        var member_list = null;
        settings.memberList.clear(),
        url_params.members && (member_list = multi_view.util.parseMembersParam(url_params.members));
        var member_list_is_empty = member_list && 0 === member_list.getMembers().length;
        this._emptyMembers = "" === url_params.members || member_list_is_empty,
        member_list ? (settings.gid = "selected",
        settings.memberList = member_list) : group_id ? ("virtual" === (settings.gid = group_id) && (settings.gid = "selected"),
        "search" === group_id && (settings.searchText = url_params.search_text),
        "selected" === group_id && member_id && (settings.uid = member_id),
        "selected" !== group_id && (settings.uid = null)) : member_id ? (settings.uid = member_id,
        settings.gid = "selected") : settings.gid = "login",
        settings.selectedGroupType = url_params.p || "",
        settings.beginDate = url_params.bdate || settings.beginDate;
        var beginDate = grn.component.date.parse(settings.beginDate);
        settings.beginDate !== jQuery.datepicker.formatDate("yy-mm-dd", beginDate) && (settings.beginDate = jQuery.datepicker.formatDate("yy-mm-dd", settings.today.value))
    }
    ,
    G.prototype._saveState = function() {
        var hash_params = {};
        "selected" === this.settings.gid && (hash_params.members = multi_view.util.serializeMembers(this.settings.memberList.getMembers())),
        hash_params.gid = this.settings.gidIsEmpty ? "" : this.settings.gid,
        hash_params.bdate = this.settings.beginDate,
        hash_params.p = this.settings.selectedGroupType,
        this.ignoreNextHashChange = true,
        url_component.changeCurrentLocationHash(hash_params)
    }
    ,
    G.prototype._reload = function() {
        var self = this
          , begin_date = date_component.parse(this.settings.beginDate);
        return this._reloadEvent().done(function(data) {
            self.dateNavigator.setDate(begin_date),
            self.miniCalendarNavigator.selectDate(begin_date),
            self._renderCalendar(),
            self.memberPicker.setMemberList(self.settings.memberList),
            self.memberDialog.setMemberList(self.settings.memberList),
            data.group ? self.memberDropdownlist.setSelectedGroup(data.group, self.settings.selectedGroupType) : self.memberDropdownlist.setSelectedGroup(self.settings.gid, self.settings.selectedGroupType),
            self._updateLinks(),
            self._addUserClassNameJsApi();
            var dates = self._prepareDateForJsApiTrigger(data);
            self.jsAPIHandler.processTrigger(dates)
        })
    }
    ,
    G.prototype._prepareDateForJsApiTrigger = function(data) {
        var dates = [];
        return data.dates.forEach(function(date) {
            dates.push(date.value)
        }),
        dates
    }
    ,
    G.prototype._reloadAndSaveState = function() {
        this._reload().done(this._saveState.bind(this))
    }
    ,
    G.prototype._updateLinks = function() {
        var settings = this.settings
          , selected_members = settings.memberList.getMembers().filter(function(member) {
            return member.selected
        })
          , print_link_class = "schedule_print_personal_week";
        $("#menu_part a, .global_navi_title a").each(function() {
            var href = this.getAttribute("href");
            if (!href.match(/^javascript:/)) {
                var is_print_link = $(this).hasClass(print_link_class)
                  , gid = settings.gidIsEmpty && !is_print_link ? "" : settings.gid
                  , url_params = {
                    bdate: settings.beginDate,
                    gid: gid,
                    p: settings.selectedGroupType
                };
                "selected" === settings.gid && (url_params.uid = ""),
                href.match(/schedule\/add|schedule\/adjust_search/) && (url_params.members = multi_view.util.serializeMembers(selected_members)),
                this.setAttribute("href", grn.component.url.setParameters(href, url_params))
            }
        })
    }
    ,
    G.prototype.memberDialogOnApply = function() {
        this.settings.memberList = this.memberDialog.getMemberList(),
        this.settings.gid = "selected",
        this.settings.gidIsEmpty = false,
        this.settings.uid = null,
        this._emptyMembers = 0 === this.settings.memberList.getMembers().length,
        this._reloadAndSaveState()
    }
    ,
    G.prototype._membersPickerOnChange = function() {
        this.settings.memberList = this.memberPicker.getMemberList(),
        this.settings.gid = "selected",
        this.settings.gidIsEmpty = false,
        this.settings.uid = null,
        this._emptyMembers = 0 === this.settings.memberList.getMembers().length,
        this._reloadAndSaveState()
    }
    ,
    G.prototype._memberDropdownlistOnSelect = function(ev, data) {
        this.settings.gid = data.gid,
        this.settings.gidIsEmpty = false,
        this.settings.uid = null,
        this._emptyMembers = false,
        this.settings.selectedGroupType = "",
        data.node && data.node.extra_param && (this.settings.selectedGroupType = data.node.extra_param),
        this._reloadAndSaveState()
    }
    ,
    G.prototype._miniCalendarNavigatorOnChange = function(ev, date) {
        this.settings.beginDate = jQuery.datepicker.formatDate("yy-mm-dd", date),
        this._reloadAndSaveState()
    }
    ,
    G.prototype._dateNavigatorOnChange = function(ev, date) {
        this.settings.beginDate = jQuery.datepicker.formatDate("yy-mm-dd", date),
        this._reloadAndSaveState()
    }
    ,
    G.prototype._toggleSpinner = function(option) {
        var zIndex = 900;
        this._$spinner || (this._$spinner = $("<div class='spinnerBoxBase-grn spinnerCentered'><div class='spinnerBox-grn'></div></div>").css("z-index", 900)),
        this._$overlay || (this._$overlay = $(document.createElement("div")).addClass("viewoverlay")),
        "show" === option ? (this._$spinner.appendTo(document.body),
        this._$overlay.appendTo(this.$viewMain)) : (this._$spinner.remove(),
        this._$overlay.remove())
    }
    ,
    G.prototype._addUserClassNameJsApi = function() {
        var member_list = this.settings.memberList
          , login_user_id = grn.data.login.id
          , login_user_in_list = member_list.getById(login_user_id, "user")
          , user_calendar_class_name = "js_customization_schedule_user_id"
          , $user_calendar = this.$view.find("[class^='" + user_calendar_class_name + "']").first();
        "undefined" !== typeof login_user_in_list && login_user_in_list.selected && (user_calendar_class_name += "_" + login_user_id),
        jQuery($user_calendar).attr("class", user_calendar_class_name)
    }
}(jQuery);
