grn.base.namespace("grn.page.schedule.view"),
function($) {
    "use strict";
    var G = grn.page.schedule.view;
    G.init = function() {
        $("#history_link").on("click", G.handleToggleFacilityApprovalHistory),
        G.jumpToComments()
    }
    ,
    G.handleToggleFacilityApprovalHistory = function() {
        $("#history_balloon_inline, #history_table").toggle(),
        $("#history_icon_inline").toggleClass("icon_show_element_b_grn icon_hide_element_b_grn")
    }
    ,
    G.jumpToComments = function() {
        if (void 0 !== location.search) {
            for (var has_follow_id = false, params = location.search.split("&"), i = 0; i < params.length; i++) {
                var node;
                0 == params[i].indexOf("follow_id=") && (has_follow_id = true)
            }
            has_follow_id && $("#schedule_comments") && window.scrollTo(0, $("#schedule_comments").offset().top)
        }
    }
    ,
    $(document).ready(G.init)
}(jQuery);
