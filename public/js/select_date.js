function cb_ui_select_date_init_day(form, prefix) {
    var select_year = form.elements[prefix + "year"]
      , select_month = form.elements[prefix + "month"]
      , select_day = form.elements[prefix + "day"]
      , year = select_year.options[select_year.selectedIndex].value
      , month = select_month.options[select_month.selectedIndex].value;
    if (year && month) {
        for (var start_date, wday = new Date(year,month - 1,1).getDay(), options = select_day.options, last_day = cb_ui_select_date_get_last_day(year, month), offset = 0; options.length > 0 && "" == options[offset].value; )
            offset++;
        for (i = 0; i < last_day; i++)
            day = i + 1,
            opt_idx = i + offset,
            opt_idx >= options.length && cb_ui_select_date_add_option(options, day),
            options[opt_idx].value = day,
            options[opt_idx].className = 0 == wday ? "s_date_sunday" : 6 == wday ? "s_date_saturday" : "",
            null != g_arrayHolidays && g_arrayHolidays.length > 0 && cb_ui_select_date_check_holiday(g_arrayHolidays, year, month, day) && (options[opt_idx].className = "s_date_holiday"),
            null != g_arrayWorkdays && g_arrayWorkdays.length > 0 && cb_ui_select_date_check_workday(g_arrayWorkdays, year, month, day) && (options[opt_idx].className = ""),
            options[opt_idx].text = day + "(" + wday_name[wday] + ")",
            ++wday > 6 && (wday = 0);
        for (; last_day + offset < options.length; )
            options.remove ? options.remove(options.length - 1) : options[options.length - 1] = null;
        "1970" == year && "1" == month && "1" == options[offset].value && (options.remove ? (options.remove(offset),
        options.remove(offset)) : (options[offset] = null,
        options[offset] = null)),
        options[offset].selected = true
    }
}
function cb_ui_select_date_reset_year_range(form, prefix) {
    var selectYear = form.elements[prefix + "year"]
      , selectedYearOption = selectYear.options[selectYear.selectedIndex]
      , year = selectedYearOption.value;
    if ("" != year) {
        var yearUnit = "";
        if ("undefined" == typeof G_yearUnit) {
            var fullTextYear = selectedYearOption.text
              , yearNumberPosition = fullTextYear.indexOf(year);
            yearNumberPosition > -1 && (yearUnit = fullTextYear.substr(yearNumberPosition + year.length))
        } else
            yearUnit = G_yearUnit;
        year = parseInt(year),
        (isNaN(year) || year > 2037 || year < 1970) && (year = (new Date).getFullYear());
        var maxNumberOptions = 21
          , startYearValue = year - 10;
        startYearValue <= 1970 && (maxNumberOptions = 21 - (1970 - startYearValue),
        startYearValue = 1970);
        for (var select_year_length = selectYear.length, o = null, i = 0; i < select_year_length; i++)
            if ("" == selectYear.options[i].value) {
                (o = document.createElement("OPTION")).value = "",
                o.text = selectYear.options[i].text;
                break
            }
        selectYear.innerHTML = "",
        null != o && selectYear.options.add(o);
        for (var numberOptions = 0; startYearValue < 2038 && !(numberOptions >= maxNumberOptions); startYearValue++) {
            var tempOption = document.createElement("OPTION");
            tempOption.value = startYearValue,
            tempOption.text = startYearValue + yearUnit,
            startYearValue == year && (tempOption.selected = true),
            selectYear.add(tempOption),
            numberOptions++
        }
    }
}
function cb_ui_select_date_change_enddate(form) {
    var start_year = form.elements.start_year
      , start_month = form.elements.start_month
      , start_day = form.elements.start_day
      , end_year = form.elements.end_year
      , end_month = form.elements.end_month
      , end_day = form.elements.end_day
      , start_year_options = start_year.options
      , end_year_options = end_year.options
      , curr_start_year = start_year_options[start_year.selectedIndex].value
      , curr_end_year = end_year_options[end_year.selectedIndex].value
      , sd = curr_start_year;
    sd *= 100,
    sd += start_month.selectedIndex,
    sd *= 100,
    sd += start_day.selectedIndex;
    var ed = curr_end_year;
    if (ed *= 100,
    ed += end_month.selectedIndex,
    ed *= 100,
    sd > (ed += end_day.selectedIndex)) {
        end_year.innerHTML = "";
        for (var i = 0; i < start_year_options.length; i++) {
            var o = document.createElement("OPTION");
            o.value = start_year_options[i].value,
            o.text = start_year_options[i].text,
            end_year.options.add(o)
        }
        end_year.selectedIndex = start_year.selectedIndex,
        cb_ui_select_date_init_day(form, "end_"),
        end_month.selectedIndex != start_month.selectedIndex && (end_month.selectedIndex = start_month.selectedIndex,
        cb_ui_select_date_init_day(form, "end_")),
        end_day.selectedIndex = start_day.selectedIndex
    }
}
function cb_ui_select_date_get_last_day(year, month) {
    return 2 != month ? last_day[month - 1] : year % 4 != 0 ? 28 : year % 100 == 0 && year % 400 != 0 ? 28 : 29
}
function cb_ui_select_date_add_option(options, day) {
    if (document.createElement) {
        var option = document.createElement("OPTION");
        return option.value = day,
        document.all ? options.add(option) : options.add ? options.add(option, day) : options[options.length] = option,
        option
    }
    var len = options.length;
    return options[len] = new Option("",day),
    options[len]
}
function cb_ui_select_date_display_calendar(id) {
    var e = document.getElementById(id);
    e && e.style && ("none" == e.style.display ? e.style.display = "" : e.style.display = "none")
}
function cb_ui_select_date_check_holiday(holidays, year, month, day) {
    for (f = 0; f < holidays.length; f++)
        if (month_str = month < 10 ? "0" + month : month,
        date_str = day < 10 ? "0" + day : day,
        year + "-" + month_str + "-" + date_str == holidays[f])
            return true;
    return false
}
function cb_ui_select_date_check_workday(workdays, year, month, day) {
    for (f = 0; f < workdays.length; f++)
        if (month_str = month < 10 ? "0" + month : month,
        date_str = day < 10 ? "0" + day : day,
        year + "-" + month_str + "-" + date_str == workdays[f])
            return true;
    return false
}
var last_day = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
