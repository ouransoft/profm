function check_disable(e) {
    var tag_span;
    return !(e.parentNode.className.indexOf("Disable") >= 0)
}
function isKanji(str) {
    var strpattern, pattern;
    return new RegExp(/[\u3400-\u4DB5\u4E00-\u9FCB\uF900-\uFA6A\u2E80-\u2FD5]/).test(str)
}
function isHiragana(str) {
    var strpattern, pattern;
    return new RegExp(/[\u3041-\u3096]/).test(str)
}
function isKatakana(str) {
    var strpattern, pattern;
    return new RegExp(/[\u30A0-\u30FF]/).test(str)
}
function isJapanese(str) {
    return isKanji(str) || isHiragana(str) || isKatakana(str)
}
function isLatin(str) {
    var rlatins;
    return !/[^\u0000-\u007f]/.test(str)
}
function dd_remove(plid) {
    if ("undefined" != typeof browser_type && "msie" != browser_type && false) {
        var _portal_frame;
        showing = 0,
        jQuery("#overlay").remove(),
        jQuery("#ddpopup").remove(),
        (_portal_frame = "undefined" != typeof plid && "" != plid ? jQuery("#portal_frame" + plid) : jQuery("#mainareaSchedule-grn")).find("div.dddummy").remove(),
        _portal_frame.find("div.dddummy_move").remove(),
        _portal_frame.find("td.ddtd").removeClass("ddtd_hide");
        var ddtd_remove = _portal_frame.find("td.ddtd_remove");
        ddtd_remove.hasClass("group_day_calendar_color_conflicted_line") ? ddtd_remove.addClass("group_day_calendar_item_conflicted").removeClass("group_day_calendar_color_conflicted_line") : ddtd_remove.addClass("ddtd_middle group_day_calendar_item"),
        ddtd_remove.addClass("ddtd group_day_calendar_color_booked"),
        ddtd_remove.find("div.normalEventElement").css("visibility", "visible"),
        ddtd_remove.prev().hasClass("group_day_calendar_event_cell") ? ddtd_remove.removeClass("ddtd_prev_event_cell") : ddtd_remove.removeClass("ddtd_prev_item"),
        (ddtd_remove.next().hasClass("group_day_calendar_color_conflicted_line") || ddtd_remove.next().hasClass("group_day_calendar_color_available")) && ddtd_remove.next().removeClass("ddtd_next"),
        ddtd_remove.removeClass("ddtd_remove")
    }
}
function dd_remove_before_sync(plid) {
    var _portal_frame;
    "undefined" != typeof browser_type && "msie" != browser_type && false && (isTouchDevice() || (showing = 0,
    jQuery("#overlay").remove(),
    jQuery("#ddpopup").remove(),
    (_portal_frame = "undefined" != typeof plid && "" != plid ? jQuery("#portal_frame" + plid) : jQuery("#mainareaSchedule-grn")).find("div.dddummy").remove(),
    _portal_frame.find("div.dddummy_move").remove(),
    _portal_frame.find("td.ddtd").removeClass("ddtd_hide")))
}
function dd_zoom_display(plid) {
    if ("undefined" != typeof browser_type && "msie" != browser_type && false) {
        var is_portlet, _portal_frame;
        is_portlet = "undefined" != typeof plid && "" != plid ? (_portal_frame = jQuery("#portal_frame" + plid),
        1) : (_portal_frame = jQuery("#mainareaSchedule-grn"),
        0);
        var x_array = [], x_array_count = 0, tds;
        if (_portal_frame.has("#show_users_timezone" + plid + ":checked").length || _portal_frame.has("#show_users_timezonegroup_day:checked").length) {
            tds = _portal_frame.find("tr.bar_user_timezone").eq(1).children("td");
            for (var i = 2; i < tds.length; i++)
                x_array[x_array_count++] = jQuery(tds[i]).position().left;
            x_array[x_array_count++] = x_array[x_array_count - 2] + tds.last().width()
        } else {
            tds = _portal_frame.find("tr.bar_login_timezone").eq(1).children("td");
            for (var i = 2; i < tds.length; i++)
                x_array[x_array_count++] = jQuery(tds[i]).position().left;
            x_array[x_array_count++] = x_array[x_array_count - 2] + tds.last().width()
        }
        var y_array = [], y_array_height = [], y_array_count = 0, trs = _portal_frame.find("table.day_table tbody tr"), tr, day_table, offset_top = _portal_frame.find("table.day_table").eq(0)[0].offsetTop;
        if ("undefined" != typeof browser_type && "msie" == browser_type && 9 == browser_ver_major)
            for (var i = 0; i < trs.length; i++)
                tr = trs.eq(i)[0],
                y_array[i] = tr.offsetTop + offset_top,
                y_array_height[i] = tr.offsetHeight;
        else
            for (var i = 0; i < trs.length; i++)
                tr = trs.eq(i)[0],
                y_array[i] = tr.offsetTop + offset_top,
                y_array_height[i] = tr.clientHeight;
        y_array_count = trs.length,
        _portal_frame.find("div.dddummy").remove(),
        _portal_frame.find("td.ddtd").removeClass("ddtd_hide");
        var dddummy_move = _portal_frame.find("div.dddummy_move");
        dddummy_move.css("top", y_array[dddummy_move.data("top_position")]),
        dddummy_move.css("height", y_array_height[dddummy_move.data("top_position")]),
        dddummy_move.css("left", x_array[dddummy_move.data("left_position")]),
        dddummy_move.css("width", x_array[dddummy_move.data("left_position") + dddummy_move.data("duration")] - x_array[dddummy_move.data("left_position")]),
        dddummy_move.find("div.normalEventElement").height(dddummy_move.height())
    }
}
function dd_init(eid, plid) {
    if ("undefined" != typeof browser_type && "msie" != browser_type && false) {
        var is_portlet, _portal_frame, bdate_str, sdate_str, edate_str;
        "undefined" != typeof plid && "" != plid ? (_portal_frame = jQuery("#portal_frame" + plid),
        is_portlet = 1) : (_portal_frame = jQuery("#mainareaSchedule-grn"),
        is_portlet = 0,
        jQuery("#user-popup-div").css("z-index", 31),
        jQuery("#user-popup-if").css("z-index", 30),
        jQuery("#facility-popup-div").css("z-index", 31),
        jQuery("#facility-popup-if").css("z-index", 30));
        for (var ddtd_list = _portal_frame.find("td.ddtd"), ddtd, ddtd_nee, dddummy, dddummy_nee, h_array = [], w_array = [], t_array = [], l_array = [], ddimg1, ddimg2, i = 0; i < ddtd_list.length; i++) {
            if (_portal_frame.has("#dddummy" + i).length)
                return;
            (ddtd = ddtd_list.eq(i)).find("div").find("a").length && (h_array[i] = ddtd.height(),
            w_array[i] = ddtd.width(),
            t_array[i] = ddtd.position().top,
            l_array[i] = ddtd.position().left)
        }
        for (var i = 0; i < ddtd_list.length; i++)
            ddtd_nee = (ddtd = ddtd_list.eq(i)).find("div.normalEventElement"),
            ddtd.find("span.temporary").length || ddtd.find("span.share_temporary").length || "temporary" != ddtd_nee.data("event_type") && "share_temporary" != ddtd_nee.data("event_type") && ddtd.find("div").find("a").length && (ddtd.addClass("ddtd" + i),
            _portal_frame.append("<div class='dddummy' id='dddummy" + i + "'></div>"),
            bdate_str = ddtd_nee.data("event_bdate"),
            sdate_str = (sdate_str = ddtd_nee.data("event_start_date")).substr(0, 10),
            edate_str = (edate_str = ddtd_nee.data("event_end_date")).substr(0, 10),
            dddummy = _portal_frame.find("#dddummy" + i),
            bdate_str == sdate_str && bdate_str == edate_str ? (dddummy.addClass("dddummy_normal"),
            dddummy.append("<div class='ddimg1 ddimg_none'><div class='ddimg_child1'><div class='ddimg_child2'></div></div></div>"),
            ddimg1 = dddummy.find("div.ddimg1"),
            is_portlet && ddimg1.addClass("ddimg1_portlet"),
            ddimg1.height(h_array[i] + 2),
            ddimg1.find("div.ddimg_child1").height(h_array[i] + 2),
            dddummy.append("<div class='ddimg2 ddimg_none'><div class='ddimg_child1'><div class='ddimg_child2'></div></div></div>"),
            ddimg2 = dddummy.find("div.ddimg2"),
            is_portlet && ddimg2.addClass("ddimg2_portlet"),
            ddimg2.height(h_array[i] + 2),
            ddimg2.find("div.ddimg_child1").height(h_array[i] + 2)) : bdate_str == sdate_str ? (dddummy.addClass("dddummy_left"),
            dddummy.append("<div class='ddimg1 ddimg_none'><div class='ddimg_child1'><div class='ddimg_child2'></div></div></div>"),
            ddimg1 = dddummy.find("div.ddimg1"),
            is_portlet && ddimg1.addClass("ddimg1_portlet"),
            ddimg1.height(h_array[i] + 2),
            ddimg1.find("div.ddimg_child1").height(h_array[i] + 2)) : bdate_str == edate_str ? (dddummy.addClass("dddummy_right"),
            dddummy.append("<div class='ddimg2 ddimg_none'><div class='ddimg_child1'><div class='ddimg_child2'></div></div></div>"),
            ddimg2 = dddummy.find("div.ddimg2"),
            is_portlet && ddimg2.addClass("ddimg2_portlet"),
            ddimg2.height(h_array[i] + 2),
            ddimg2.find("div.ddimg_child1").height(h_array[i] + 2)) : dddummy.addClass("dddummy_none"),
            dddummy.append(ddtd_nee.clone()),
            dddummy_nee = dddummy.find("div.normalEventElement"),
            _portal_frame.has("#show_full_title" + plid + ":checked").length || _portal_frame.has("#show_full_titlegroup_day:checked").length ? (ddtd.hasClass("ddtd_middle") && (grn.browser.firefox ? dddummy_nee.css({
                position: "absolute",
                top: "50%",
                "margin-top": -ddtd_nee.height() / 2 - 2.2
            }) : dddummy_nee.css({
                position: "absolute",
                top: "50%",
                "margin-top": -ddtd_nee.height() / 2 - 3.5
            })),
            dddummy_nee.css({
                whiteSpace: "normal",
                "line-height": 1.1
            })) : dddummy_nee.css("whiteSpace", "nowrap"),
            "undefined" != typeof eid && "" != eid && eid == dddummy_nee.data("event_id") && dddummy.addClass("newevent-grn"),
            grn.browser.firefox ? dddummy.css({
                width: w_array[i] + 1,
                height: h_array[i] + 1.8,
                top: t_array[i] - 1,
                left: l_array[i] - 1
            }) : dddummy.css({
                width: w_array[i] + 2,
                height: h_array[i] + 3,
                top: t_array[i],
                left: l_array[i] - .5
            }),
            ddtd.addClass("ddtd_hide"));
        is_portlet ? showFullShortTitle("show_full_title" + plid, "portal_frame" + plid, "view_group_day", true) : showFullShortTitle("show_full_titlegroup_day", "mainareaSchedule-grn", "group_day", true)
    }
}
function dd_handle(plid) {
    if ("undefined" != typeof browser_type && "msie" != browser_type && false) {
        if ("undefined" != typeof plid && "" != plid)
            var _portal_frame = jQuery("#portal_frame" + plid)
              , is_portlet = 1;
        else
            var _portal_frame = jQuery("#mainareaSchedule-grn")
              , is_portlet = 0;
        var x_array = [], x_array_count = 0, tds;
        if (_portal_frame.has("#show_users_timezone" + plid + ":checked").length || _portal_frame.has("#show_users_timezonegroup_day:checked").length) {
            tds = _portal_frame.find("tr.bar_user_timezone").eq(1).children("td");
            for (var i = 2; i < tds.length; i++)
                x_array[x_array_count++] = jQuery(tds[i]).position().left;
            x_array[x_array_count++] = x_array[x_array_count - 2] + tds.last().width()
        } else {
            tds = _portal_frame.find("tr.bar_login_timezone").eq(1).children("td");
            for (var i = 2; i < tds.length; i++)
                x_array[x_array_count++] = jQuery(tds[i]).position().left;
            x_array[x_array_count++] = x_array[x_array_count - 2] + tds.last().width()
        }
        var time_pos = 0
          , x_target_pos = 0
          , time_str = ""
          , start_hour_str = ""
          , start_minute_str = ""
          , end_hour_str = ""
          , end_minute_str = ""
          , _event = ""
          , _start_hour = 0
          , _start_minute = 0
          , _end_hour = 0
          , _end_minute = 0
          , duration_hour = 0
          , duration_minute = 0
          , set_hour = ""
          , unit = ""
          , offset_hour = ""
          , offset_minute = ""
          , end_datetime_obj = ""
          , dd_tooltip = ""
          , page_x = 0
          , original_left = 0
          , original_width = 0
          , _bdate = ""
          , _sdate = ""
          , _edate = ""
          , unit_count = 1
          , window_obj = jQuery(window)
          , body_obj = jQuery("body")
          , document_obj = jQuery(document);
        _portal_frame.find("div.dddummy_normal").draggable({
            axis: "x",
            cursor: "move",
            start: function(ev) {
                moving = 1,
                disable_tooltip = 1,
                page_x = ev.pageX;
                var this_obj = jQuery(this);
                original_left = this_obj.position().left,
                original_width = this_obj.width(),
                this_obj.addClass("dddummy_move").find("div.ddimg1 , div.ddimg2").addClass("ddimg_none"),
                jQuery("div.mainarea").append('<div id="ddtooltip" class="showEventTitle"></div>')
            },
            drag: function(ev, ui) {
                var this_obj = jQuery(this)
                  , this_nee = this_obj.find("div.normalEventElement");
                set_hour = this_nee.data("event_set_hour"),
                unit = this_nee.data("event_unit"),
                offset_minute = _portal_frame.find("#show_users_timezonegroup_day:checked").length || is_portlet && _portal_frame.find("#show_users_timezone" + plid + ":checked").length ? (offset_hour = this_nee.data("event_offset_hour"),
                this_nee.data("event_offset_minute")) : offset_hour = 0,
                start_datetime_obj = this_nee.data("event_start_date").split(/\-|\:|\s/),
                end_datetime_obj = this_nee.data("event_end_date").split(/\-|\:|\s/),
                (duration_hour = parseInt(end_datetime_obj[3], 10) - parseInt(start_datetime_obj[3], 10)) < 0 && (duration_hour += 24),
                duration_minute = parseInt(end_datetime_obj[4], 10) - parseInt(start_datetime_obj[4], 10),
                0 == (unit_count = Math.ceil((60 * duration_hour + duration_minute) / unit)) && (unit_count = 1),
                time_pos = ev.pageX > x_array[x_array_count - 1 - unit_count] ? x_array_count - 1 - unit_count : 0,
                x_target_pos = original_left + (ev.pageX - page_x);
                for (var i = 0, max = x_array_count - 1 - unit_count; i < max; i++)
                    if (x_target_pos <= x_array[i + 1]) {
                        time_pos = i;
                        break
                    }
                if (ui.position.left = x_array[time_pos],
                x_array[time_pos] + original_width > x_array[x_array_count - 1] ? this_obj.width(x_array[x_array_count - 1] - x_array[time_pos]) : this_obj.width(original_width),
                _start_minute = parseInt(offset_minute, 10) + parseInt(time_pos, 10) * parseInt(unit, 10),
                _start_hour = parseInt(set_hour, 10) + parseInt(offset_hour, 10) + Math.floor(parseInt(_start_minute, 10) / 60),
                _start_minute = parseInt(_start_minute, 10) % 60,
                _start_hour >= 24 ? _start_hour -= 24 : _start_hour < 0 && (_start_hour += 24),
                time_str = this_nee.data("event_no_endtime") ? (start_hour_str = _start_hour > 9 ? _start_hour : "0" + _start_hour) + ":" + (start_minute_str = _start_minute > 9 ? _start_minute : "0" + _start_minute) : (_end_hour = _start_hour + duration_hour,
                (_end_minute = _start_minute + duration_minute) >= 60 ? (_end_hour++,
                _end_minute -= 60) : _end_minute < 0 && (_end_hour--,
                _end_minute += 60),
                _end_hour >= 24 ? _end_hour -= 24 : _end_hour < 0 && (_end_hour += 24),
                (start_hour_str = _start_hour > 9 ? _start_hour : "0" + _start_hour) + ":" + (start_minute_str = _start_minute > 9 ? _start_minute : "0" + _start_minute) + "-" + (end_hour_str = _end_hour > 9 ? _end_hour : "0" + _end_hour) + ":" + (end_minute_str = _end_minute > 9 ? _end_minute : "0" + _end_minute)),
                dd_tooltip = jQuery("#ddtooltip").html(time_str),
                ev.pageX + dd_tooltip.width() > x_array[x_array_count - 1])
                    var left_position = ev.pageX - 20 - dd_tooltip.width();
                else
                    var left_position = ev.pageX + 10;
                dd_tooltip.css({
                    top: ev.pageY + 10,
                    left: left_position
                })
            },
            stop: function(ev) {
                moving = 0,
                showing = 1,
                disable_tooltip = 0;
                var this_obj = jQuery(this)
                  , this_nee = this_obj.find("div.normalEventElement");
                pid = is_portlet ? plid : "",
                jQuery("#ddtooltip").remove(),
                time_pos = 0,
                x_target_pos = this_obj.position().left;
                for (var i = 0, max = x_array_count - 1; i < max; i++)
                    if (x_target_pos <= x_array[i + 1] - 1) {
                        time_pos = i,
                        this_obj.css("left", parseInt(x_array[i], 10));
                        break
                    }
                body_obj.append('<div id="ddpopup"></div>');
                var ddpopup_obj = jQuery("#ddpopup");
                if (ddpopup_obj.append(_popup_loading),
                ddpopup_obj.css({
                    top: (window_obj.height() - ddpopup_obj.outerHeight()) / 2 + window_obj.scrollTop() + "px",
                    left: (window_obj.width() - ddpopup_obj.width()) / 2 + window_obj.scrollLeft() + "px"
                }),
                show_popup("ddpopup", ev.pageX, ev.pageY),
                ddpopup_obj.css("display", "inline"),
                body_obj.append(jQuery('<div class="ddoverlay" id="overlay"/>').css({
                    width: document_obj.width() + "px",
                    height: document_obj.height() + "px"
                })),
                setTimeout(function() {
                    ddpopup_obj.find("a.btn_cancel").focus()
                }, 100),
                _event = this_nee.data("event_id"),
                _bdate = this_nee.data("event_bdate"),
                _sdate = (_sdate = this_nee.data("event_start_date")).substr(0, 10),
                _edate = (_edate = this_nee.data("event_end_date")).substr(0, 10),
                set_hour = this_nee.data("event_set_hour"),
                unit = this_nee.data("event_unit"),
                _start_minute = parseInt(time_pos, 10) * parseInt(unit, 10),
                _start_hour = parseInt(set_hour, 10) + Math.floor(parseInt(_start_minute, 10) / 60),
                _start_minute = parseInt(_start_minute, 10) % 60,
                _start_hour >= 24 ? _start_hour -= 24 : _start_hour < 0 && (_start_hour += 24),
                start_datetime_obj = this_nee.data("event_start_date").split(/\-|\:|\s/),
                end_datetime_obj = this_nee.data("event_end_date").split(/\-|\:|\s/),
                this_nee.data("event_no_endtime")) {
                    if (_end_minute = _end_hour = _edate = "",
                    parseInt(start_datetime_obj[3], 10) == _start_hour && parseInt(start_datetime_obj[4], 10) == _start_minute)
                        return void (is_portlet ? (dd_remove(plid),
                        dd_init("", plid),
                        dd_handle(plid)) : (dd_remove(),
                        dd_init(),
                        dd_handle()))
                } else if (duration_hour = parseInt(end_datetime_obj[3], 10) - parseInt(start_datetime_obj[3], 10),
                duration_minute = parseInt(end_datetime_obj[4], 10) - parseInt(start_datetime_obj[4], 10),
                duration_hour < 0 && (duration_hour += 24),
                _end_hour = _start_hour + duration_hour,
                (_end_minute = _start_minute + duration_minute) >= 60 ? (_end_hour++,
                _end_minute -= 60) : _end_minute < 0 && (_end_hour--,
                _end_minute += 60),
                _end_hour >= 24 ? _end_hour -= 24 : _end_hour < 0 && (_end_hour += 24),
                parseInt(start_datetime_obj[3], 10) == _start_hour && parseInt(start_datetime_obj[4], 10) == _start_minute && parseInt(end_datetime_obj[3], 10) == _end_hour && parseInt(end_datetime_obj[4], 10) == _end_minute)
                    return void (is_portlet ? (dd_remove(plid),
                    dd_init("", plid),
                    dd_handle(plid)) : (dd_remove(),
                    dd_init(),
                    dd_handle()));
                var ddtd_id = this_obj.prop("id").substr(7)
                  , this_ddtd = _portal_frame.find("td.ddtd" + ddtd_id).addClass("ddtd_remove");
                this_ddtd.removeClass("ddtd"),
                this_ddtd.removeClass("ddtd_middle"),
                this_ddtd.removeClass("group_day_calendar_item"),
                this_ddtd.removeClass("group_day_calendar_color_booked"),
                this_ddtd.removeClass("ddtd_hide"),
                this_ddtd.hasClass("group_day_calendar_item_conflicted") && (this_ddtd.removeClass("group_day_calendar_item_conflicted"),
                this_ddtd.addClass("group_day_calendar_color_conflicted_line")),
                this_ddtd.find("div.normalEventElement").css("visibility", "hidden"),
                this_ddtd.prev().hasClass("group_day_calendar_event_cell") ? this_ddtd.addClass("ddtd_prev_event_cell") : this_ddtd.addClass("ddtd_prev_item"),
                (this_ddtd.next().hasClass("group_day_calendar_color_conflicted_line") || this_ddtd.next().hasClass("group_day_calendar_color_available")) && this_ddtd.next().addClass("ddtd_next"),
                this_obj.css({
                    "padding-left": "1px",
                    position: "absolute"
                }),
                this_obj.attr("data-left_position", time_pos),
                this_obj.attr("data-duration", Math.ceil((60 * duration_hour + duration_minute) / unit));
                for (var tr = _portal_frame.find("table.day_table tr"), i = 0; i < tr.length; i++)
                    if (!tr.eq(i).hasClass("day_table_time_login") && this_obj.position().top + this_obj.height() / 2 > tr.eq(i).position().top && this_obj.position().top + this_obj.height() / 2 < tr.eq(i).position().top + tr.eq(i).height()) {
                        this_obj.attr("data-top_position", i);
                        break
                    }
                this_obj.removeClass("dddummy"),
                jQuery.ajax({
                    url: _url_ajax_checking,
                    data: {
                        event_id: _event,
                        bdate: _bdate,
                        end_date: _edate,
                        start_date: _sdate,
                        start_hour: _start_hour,
                        start_minute: _start_minute,
                        end_hour: _end_hour,
                        end_minute: _end_minute
                    },
                    type: "POST",
                    success: function(xhr, textStatus, jqXHR) {
                        grn.component.error_handler.hasCybozuError(jqXHR) ? GRN_ScheduleSimpleAdd.showErrMsg(jqXHR) : grn.component.error_handler.hasCybozuLogin(jqXHR) ? location.href = location.href : jQuery("#ddpopup").html(xhr)
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        GRN_ScheduleSimpleAdd.showErrMsg(jqXHR)
                    },
                    complete: function(xhr, status) {
                        setTimeout(function() {
                            jQuery("span.aButtonStandard-grn").find("a.btn_yes").length ? jQuery("a.btn_yes").focus() : jQuery("a.btn_cancel").length && jQuery("a.btn_cancel").focus()
                        }, 100)
                    }
                })
            }
        }),
        _portal_frame.find("div.dddummy_normal").resizable({
            handles: "e, w",
            minWidth: 2,
            start: function(ev, ui) {
                resizing = 1,
                disable_tooltip = 1;
                var this_obj = jQuery(this);
                this_obj.addClass("dddummy_move"),
                jQuery("div.mainarea").append('<div id="ddtooltip" class="showEventTitle"></div>'),
                this_obj.find("div.normalEventElement").height(this_obj.height())
            },
            resize: function(ev, ui) {
                var this_obj = jQuery(this)
                  , this_nee = this_obj.find("div.normalEventElement");
                if (this_nee.width(this_obj.width()),
                ui.position.left != ui.originalPosition.left) {
                    ui.position.left < x_array[0] && this_obj.css({
                        left: x_array[0],
                        width: ui.originalSize.width + ui.originalPosition.left - x_array[0]
                    }),
                    x_target_pos = ui.position.left;
                    for (var i = time_pos = 0, max = x_array_count - 1; i < max; i++)
                        if (x_target_pos <= x_array[i + 1]) {
                            time_pos = i;
                            break
                        }
                    this_obj.css({
                        left: x_array[time_pos],
                        width: ui.originalPosition.left + ui.originalSize.width - x_array[time_pos]
                    })
                } else {
                    ui.originalPosition.left + ui.size.width > x_array[x_array_count - 1] && this_obj.width(x_array[x_array_count - 1] - ui.originalPosition.left),
                    x_target_pos = ui.originalPosition.left,
                    time_pos = ev.pageX > x_array[x_array_count - 1] ? x_array_count - 1 : 1;
                    for (var i = 1; i < x_array_count; i++)
                        if (x_target_pos + ui.size.width <= x_array[i]) {
                            time_pos = i;
                            break
                        }
                    this_obj.width(x_array[time_pos] - x_target_pos)
                }
                if (x_target_pos = ui.position.left,
                ui.position.left == ui.originalPosition.left) {
                    set_hour = this_nee.data("event_set_hour"),
                    unit = this_nee.data("event_unit"),
                    offset_minute = _portal_frame.find("#show_users_timezonegroup_day:checked").length || is_portlet && _portal_frame.find("#show_users_timezone" + plid + ":checked").length ? (offset_hour = this_nee.data("event_offset_hour"),
                    this_nee.data("event_offset_minute")) : offset_hour = 0,
                    start_datetime_obj = this_nee.data("event_start_date").split(/\-|\:|\s/),
                    _start_hour = parseInt(start_datetime_obj[3], 10) + parseInt(offset_hour, 10),
                    (_start_minute = parseInt(start_datetime_obj[4], 10) + parseInt(offset_minute, 10)) >= 60 ? (_start_hour++,
                    _start_minute -= 60) : _start_minute < 0 && (_start_hour--,
                    _start_minute += 60),
                    _start_hour >= 24 ? _start_hour -= 24 : _start_hour < 0 && (_start_hour += 24),
                    time_pos = x_array_count - 1;
                    for (var i = 1; i < x_array_count; i++)
                        if (x_target_pos + ui.size.width <= x_array[i]) {
                            time_pos = i;
                            break
                        }
                    _end_minute = parseInt(offset_minute, 10) + parseInt(time_pos, 10) * parseInt(unit, 10),
                    _end_hour = parseInt(set_hour, 10) + parseInt(offset_hour, 10) + Math.floor(parseInt(_end_minute, 10) / 60),
                    _end_minute = parseInt(_end_minute, 10) % 60,
                    _end_hour >= 24 ? _end_hour -= 24 : _end_hour < 0 && (_end_hour += 24)
                } else {
                    for (var i = time_pos = 0, max = x_array_count - 1; i < max; i++)
                        if (x_target_pos <= x_array[i + 1]) {
                            time_pos = i;
                            break
                        }
                    set_hour = this_nee.data("event_set_hour"),
                    unit = this_nee.data("event_unit"),
                    offset_minute = _portal_frame.find("#show_users_timezonegroup_day:checked").length || is_portlet && _portal_frame.find("#show_users_timezone" + plid + ":checked").length ? (offset_hour = this_nee.data("event_offset_hour"),
                    this_nee.data("event_offset_minute")) : offset_hour = 0,
                    _start_minute = parseInt(offset_minute, 10) + parseInt(time_pos, 10) * parseInt(unit, 10),
                    _start_hour = parseInt(set_hour, 10) + parseInt(offset_hour, 10) + Math.floor(parseInt(_start_minute, 10) / 60),
                    _start_minute = parseInt(_start_minute, 10) % 60,
                    _start_hour >= 24 ? _start_hour -= 24 : _start_hour < 0 && (_start_hour += 24),
                    end_datetime_obj = this_nee.data("event_end_date").split(/\-|\:|\s/),
                    _end_hour = parseInt(end_datetime_obj[3], 10) + parseInt(offset_hour, 10),
                    (_end_minute = parseInt(end_datetime_obj[4], 10) + parseInt(offset_minute, 10)) >= 60 ? (_end_hour++,
                    _end_minute -= 60) : _end_minute < 0 && (_end_hour--,
                    _end_minute += 60),
                    _end_hour >= 24 ? _end_hour -= 24 : _end_hour < 0 && (_end_hour += 24)
                }
                time_str = (start_hour_str = _start_hour > 9 ? _start_hour : "0" + _start_hour) + ":" + (start_minute_str = _start_minute > 9 ? _start_minute : "0" + _start_minute) + "-" + (end_hour_str = _end_hour > 9 ? _end_hour : "0" + _end_hour) + ":" + (end_minute_str = _end_minute > 9 ? _end_minute : "0" + _end_minute),
                (dd_tooltip = jQuery("#ddtooltip")).html(time_str),
                dd_tooltip.css("top", ev.pageY + 10),
                ev.pageX + dd_tooltip.width() > x_array[x_array_count - 1] ? dd_tooltip.css("left", ev.pageX - 20 - dd_tooltip.width()) : dd_tooltip.css("left", ev.pageX + 10)
            },
            stop: function(ev, ui) {
                resizing = 0,
                showing = 1,
                disable_tooltip = 0;
                var this_obj = jQuery(this)
                  , this_nee = this_obj.find("div.normalEventElement");
                pid = "undefined" != typeof plid && "" != plid ? plid : "",
                jQuery("#ddtooltip").remove(),
                body_obj.append('<div id="ddpopup"></div>');
                var ddpopup_obj = jQuery("#ddpopup");
                ddpopup_obj.append(_popup_loading),
                ddpopup_obj.css({
                    top: (window_obj.height() - ddpopup_obj.outerHeight()) / 2 + window_obj.scrollTop() + "px",
                    left: (window_obj.width() - ddpopup_obj.width()) / 2 + window_obj.scrollLeft() + "px"
                }),
                show_popup("ddpopup", ev.pageX, ev.pageY),
                ddpopup_obj.css("display", "inline");
                var div_overlay = jQuery('<div class="ddoverlay" id="overlay"/>').css({
                    width: document_obj.width() + "px",
                    height: document_obj.height() + "px"
                });
                if (body_obj.append(div_overlay),
                setTimeout(function() {
                    jQuery("a.btn_cancel").length && jQuery("a.btn_cancel").focus()
                }, 100),
                _event = this_nee.data("event_id"),
                _bdate = this_nee.data("event_bdate"),
                _sdate = (_sdate = this_nee.data("event_start_date")).substr(0, 10),
                _edate = (_edate = this_nee.data("event_end_date")).substr(0, 10),
                x_target_pos = this_obj.position().left,
                start_datetime_obj = this_nee.data("event_start_date").split(/\-|\:|\s/),
                end_datetime_obj = this_nee.data("event_end_date").split(/\-|\:|\s/),
                ui.position.left == ui.originalPosition.left) {
                    _start_hour = parseInt(start_datetime_obj[3], 10),
                    _start_minute = parseInt(start_datetime_obj[4], 10),
                    time_pos = x_array_count - 1;
                    for (var i = 1; i < x_array_count; i++)
                        if (x_target_pos + ui.size.width <= x_array[i] - 1) {
                            time_pos = i;
                            break
                        }
                    set_hour = this_nee.data("event_set_hour"),
                    unit = this_nee.data("event_unit"),
                    _end_minute = parseInt(time_pos, 10) * parseInt(unit, 10),
                    _end_hour = parseInt(set_hour, 10) + Math.floor(parseInt(_end_minute, 10) / 60),
                    _end_minute = parseInt(_end_minute, 10) % 60,
                    _start_hour >= 24 && (_start_hour -= 24)
                } else {
                    for (var i = time_pos = 0, max = x_array_count - 1; i < max; i++)
                        if (x_target_pos <= x_array[i + 1] - 1) {
                            time_pos = i;
                            break
                        }
                    set_hour = this_nee.data("event_set_hour"),
                    unit = this_nee.data("event_unit"),
                    _start_minute = parseInt(time_pos, 10) * parseInt(unit, 10),
                    _start_hour = parseInt(set_hour, 10) + Math.floor(parseInt(_start_minute, 10) / 60),
                    _start_minute = parseInt(_start_minute, 10) % 60,
                    _start_hour >= 24 && (_start_hour -= 24),
                    _end_hour = parseInt(end_datetime_obj[3], 10),
                    _end_minute = parseInt(end_datetime_obj[4], 10)
                }
                if (parseInt(start_datetime_obj[3], 10) != _start_hour || parseInt(start_datetime_obj[4], 10) != _start_minute || parseInt(end_datetime_obj[3], 10) != _end_hour || parseInt(end_datetime_obj[4], 10) != _end_minute) {
                    var ddtd_id = this_obj.prop("id").substr(7)
                      , this_ddtd = _portal_frame.find("td.ddtd" + ddtd_id).addClass("ddtd_remove");
                    this_ddtd.removeClass("ddtd"),
                    this_ddtd.removeClass("ddtd_middle"),
                    this_ddtd.removeClass("group_day_calendar_item"),
                    this_ddtd.removeClass("group_day_calendar_color_booked"),
                    this_ddtd.removeClass("ddtd_hide"),
                    this_ddtd.hasClass("group_day_calendar_item_conflicted") && (this_ddtd.removeClass("group_day_calendar_item_conflicted"),
                    this_ddtd.addClass("group_day_calendar_color_conflicted_line")),
                    this_ddtd.find("div.normalEventElement").css("visibility", "hidden"),
                    this_ddtd.prev().hasClass("group_day_calendar_event_cell") ? this_ddtd.addClass("ddtd_prev_event_cell") : this_ddtd.addClass("ddtd_prev_item"),
                    (this_ddtd.next().hasClass("group_day_calendar_color_conflicted_line") || this_ddtd.next().hasClass("group_day_calendar_color_available")) && this_ddtd.next().addClass("ddtd_next"),
                    ui.position.left == ui.originalPosition.left ? (this_obj.attr("data-duration", Math.ceil((60 * (_end_hour - _start_hour) + (_end_minute - _start_minute)) / unit)),
                    this_obj.attr("data-left_position", time_pos - Math.ceil((60 * (_end_hour - _start_hour) + (_end_minute - _start_minute)) / unit))) : (this_obj.attr("data-left_position", time_pos),
                    this_obj.attr("data-duration", Math.ceil((60 * (_end_hour - _start_hour) + (_end_minute - _start_minute)) / unit))),
                    this_obj.css({
                        "padding-left": "1px",
                        position: "absolute"
                    });
                    for (var tr = _portal_frame.find("table.day_table").find("tr"), i = 0; i < tr.length; i++)
                        if (!tr.eq(i).hasClass("day_table_time_login") && this_obj.position().top + this_obj.height() / 2 > tr.eq(i).position().top && this_obj.position().top + this_obj.height() / 2 < tr.eq(i).position().top + tr.eq(i).height()) {
                            this_obj.attr("data-top_position", i);
                            break
                        }
                    this_obj.removeClass("dddummy"),
                    jQuery.ajax({
                        url: _url_ajax_checking,
                        data: {
                            event_id: _event,
                            bdate: _bdate,
                            end_date: _edate,
                            start_date: _sdate,
                            start_hour: _start_hour,
                            start_minute: _start_minute,
                            end_hour: _end_hour,
                            end_minute: _end_minute
                        },
                        type: "POST",
                        success: function(xhr, textStatus, jqXHR) {
                            grn.component.error_handler.hasCybozuError(jqXHR) ? GRN_ScheduleSimpleAdd.showErrMsg(jqXHR) : grn.component.error_handler.hasCybozuLogin(jqXHR) ? location.href = location.href : jQuery("#ddpopup").html(xhr)
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            GRN_ScheduleSimpleAdd.showErrMsg(jqXHR)
                        },
                        complete: function(xhr, status) {
                            setTimeout(function() {
                                jQuery("span.aButtonStandard-grn").find("a.btn_yes").length > 0 ? jQuery("a.btn_yes").focus() : jQuery("a.btn_cancel").length && jQuery("a.btn_cancel").focus()
                            }, 100)
                        }
                    })
                } else
                    "undefined" != typeof plid && "" != plid ? (dd_remove(plid),
                    dd_init("", plid),
                    dd_handle(plid)) : (dd_remove(),
                    dd_init(),
                    dd_handle())
            }
        }),
        _portal_frame.find("div.dddummy_left").resizable({
            handles: "w",
            minWidth: 2,
            start: function(ev, ui) {
                resizing = 1,
                disable_tooltip = 1;
                var this_obj = jQuery(this);
                this_obj.addClass("dddummy_move"),
                jQuery("div.mainarea").append('<div id="ddtooltip" class="showEventTitle"></div>'),
                this_obj.find("div.normalEventElement").height(this_obj.height())
            },
            resize: function(ev, ui) {
                var this_obj = jQuery(this)
                  , this_nee = this_obj.find("div.normalEventElement");
                this_nee.width(this_obj.width()),
                ui.position.left < x_array[0] && (this_obj.css("left", x_array[0]),
                this_obj.width(ui.originalSize.width + ui.originalPosition.left - x_array[0])),
                x_target_pos = ui.position.left;
                for (var i = time_pos = 0, max = x_array_count - 1; i < max; i++)
                    if (x_target_pos <= x_array[i + 1]) {
                        time_pos = i;
                        break
                    }
                this_obj.css("left", x_array[time_pos]),
                this_obj.width(ui.originalPosition.left + ui.originalSize.width - x_array[time_pos]),
                x_target_pos = ui.position.left;
                for (var i = time_pos = 0, max = x_array_count - 1; i < max; i++)
                    if (x_target_pos <= x_array[i + 1]) {
                        time_pos = i;
                        break
                    }
                set_hour = this_nee.data("event_set_hour"),
                unit = this_nee.data("event_unit"),
                offset_minute = _portal_frame.find("#show_users_timezonegroup_day:checked").length > 0 ? (offset_hour = this_nee.data("event_offset_hour"),
                this_nee.data("event_offset_minute")) : offset_hour = 0,
                _start_minute = parseInt(offset_minute, 10) + parseInt(time_pos, 10) * parseInt(unit, 10),
                _start_hour = parseInt(set_hour, 10) + parseInt(offset_hour, 10) + Math.floor(parseInt(_start_minute, 10) / 60),
                _start_minute = parseInt(_start_minute, 10) % 60,
                _start_hour >= 24 ? _start_hour -= 24 : _start_hour < 0 && (_start_hour += 24),
                start_hour_str = _start_hour > 9 ? _start_hour : "0" + _start_hour,
                start_minute_str = _start_minute > 9 ? _start_minute : "0" + _start_minute,
                end_datetime_obj = this_nee.data("event_end_date").split(/\-|\:|\s/),
                d = new Date(end_datetime_obj[0],end_datetime_obj[1] - 1,end_datetime_obj[2]),
                "undefined" != typeof _short_date_format && "undefined" != typeof _locale && (time_str = start_hour_str + ":" + start_minute_str + "-" + parseday(d, _short_date_format, _locale)),
                (dd_tooltip = jQuery("#ddtooltip")).html(time_str),
                dd_tooltip.css("top", ev.pageY + 10),
                ev.pageX + dd_tooltip.width() > x_array[x_array_count - 1] ? dd_tooltip.css("left", ev.pageX - 20 - dd_tooltip.width()) : dd_tooltip.css("left", ev.pageX + 10)
            },
            stop: function(ev, ui) {
                resizing = 0,
                showing = 1,
                disable_tooltip = 0;
                var this_obj = jQuery(this)
                  , this_nee = this_obj.find("div.normalEventElement");
                pid = "undefined" != typeof plid && "" != plid ? plid : "",
                jQuery("#ddtooltip").remove(),
                body_obj.append('<div id="ddpopup"></div>');
                var ddpopup_obj = jQuery("#ddpopup");
                ddpopup_obj.append(_popup_loading),
                ddpopup_obj.css({
                    position: "absolute",
                    top: (window_obj.height() - ddpopup_obj.outerHeight()) / 2 + window_obj.scrollTop() + "px",
                    left: (window_obj.width() - ddpopup_obj.width()) / 2 + window_obj.scrollLeft() + "px"
                }),
                show_popup("ddpopup", ev.pageX, ev.pageY),
                ddpopup_obj.css("display", "inline");
                var div_overlay = jQuery('<div class="ddoverlay" id="overlay"/>').css({
                    width: document_obj.width() + "px",
                    height: document_obj.height() + "px"
                });
                jQuery("body").append(div_overlay),
                setTimeout(function() {
                    jQuery("a.btn_cancel").length && jQuery("a.btn_cancel").focus()
                }, 100),
                _event = this_nee.data("event_id"),
                _bdate = this_nee.data("event_bdate"),
                _sdate = (_sdate = this_nee.data("event_start_date")).substr(0, 10),
                _edate = (_edate = this_nee.data("event_end_date")).substr(0, 10),
                x_target_pos = this_obj.position().left,
                start_datetime_obj = this_nee.data("event_start_date").split(/\-|\:|\s/),
                end_datetime_obj = this_nee.data("event_end_date").split(/\-|\:|\s/);
                for (var i = time_pos = 0, max = x_array_count - 1; i < max; i++)
                    if (x_target_pos <= x_array[i + 1] - 1) {
                        time_pos = i;
                        break
                    }
                if (set_hour = this_nee.data("event_set_hour"),
                unit = this_nee.data("event_unit"),
                _start_minute = parseInt(time_pos, 10) * parseInt(unit, 10),
                _start_hour = parseInt(set_hour, 10) + Math.floor(parseInt(_start_minute, 10) / 60),
                _start_minute = parseInt(_start_minute, 10) % 60,
                _start_hour >= 24 && (_start_hour -= 24),
                _end_hour = parseInt(end_datetime_obj[3], 10),
                _end_minute = parseInt(end_datetime_obj[4], 10),
                parseInt(start_datetime_obj[3], 10) != _start_hour || parseInt(start_datetime_obj[4], 10) != _start_minute || parseInt(end_datetime_obj[3], 10) != _end_hour || parseInt(end_datetime_obj[4], 10) != _end_minute) {
                    var ddtd_id = this_obj.prop("id").substr(7)
                      , this_ddtd = _portal_frame.find("td.ddtd" + ddtd_id);
                    this_ddtd.addClass("ddtd_remove");
                    var ddtd_remove = _portal_frame.find("td.ddtd_remove");
                    ddtd_remove.removeClass("ddtd"),
                    ddtd_remove.removeClass("ddtd_middle"),
                    ddtd_remove.removeClass("group_day_calendar_item"),
                    ddtd_remove.removeClass("group_day_calendar_color_booked"),
                    ddtd_remove.removeClass("ddtd_hide"),
                    ddtd_remove.hasClass("group_day_calendar_item_conflicted") && (ddtd_remove.removeClass("group_day_calendar_item_conflicted"),
                    ddtd_remove.addClass("group_day_calendar_color_conflicted_line")),
                    ddtd_remove.find("div.normalEventElement").css("visibility", "hidden"),
                    this_ddtd.prev().hasClass("group_day_calendar_event_cell") ? this_ddtd.addClass("ddtd_prev_event_cell") : this_ddtd.addClass("ddtd_prev_item"),
                    (this_ddtd.next().hasClass("group_day_calendar_color_conflicted_line") || this_ddtd.next().hasClass("group_day_calendar_color_available")) && this_ddtd.next().addClass("ddtd_next"),
                    this_obj.css({
                        "padding-left": "1px",
                        position: "absolute"
                    }),
                    this_obj.attr("data-left_position", time_pos),
                    this_obj.attr("data-duration", x_array_count - 1 - time_pos);
                    for (var tr = _portal_frame.find("table.day_table").find("tr"), i = 0; i < tr.length; i++)
                        if (!tr.eq(i).hasClass("day_table_time_login") && this_obj.position().top + this_obj.height() / 2 > tr.eq(i).position().top && this_obj.position().top + this_obj.height() / 2 < tr.eq(i).position().top + tr.eq(i).height()) {
                            this_obj.attr("data-top_position", i);
                            break
                        }
                    this_obj.removeClass("dddummy"),
                    jQuery.ajax({
                        url: _url_ajax_checking,
                        data: {
                            event_id: _event,
                            bdate: _bdate,
                            end_date: _edate,
                            start_date: _sdate,
                            start_hour: _start_hour,
                            start_minute: _start_minute,
                            end_hour: _end_hour,
                            end_minute: _end_minute
                        },
                        type: "POST",
                        success: function(xhr, textStatus, jqXHR) {
                            grn.component.error_handler.hasCybozuError(jqXHR) ? GRN_ScheduleSimpleAdd.showErrMsg(jqXHR) : grn.component.error_handler.hasCybozuLogin(jqXHR) ? location.href = location.href : jQuery("#ddpopup").html(xhr)
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            GRN_ScheduleSimpleAdd.showErrMsg(jqXHR)
                        },
                        complete: function(xhr, status) {
                            setTimeout(function() {
                                jQuery(".aButtonStandard-grn").find("a.btn_yes").length ? jQuery("a.btn_yes").focus() : jQuery("a.btn_cancel").length && jQuery("a.btn_cancel").focus()
                            }, 100)
                        }
                    })
                } else
                    "undefined" != typeof plid && "" != plid ? (dd_remove(plid),
                    dd_init("", plid),
                    dd_handle(plid)) : (dd_remove(),
                    dd_init(),
                    dd_handle())
            }
        }),
        _portal_frame.find("div.dddummy_right").resizable({
            handles: "e",
            minWidth: 2,
            start: function(ev, ui) {
                resizing = 1,
                disable_tooltip = 1;
                var this_obj = jQuery(this);
                this_obj.addClass("dddummy_move"),
                jQuery("div.mainarea").append('<div id="ddtooltip" class="showEventTitle"></div>'),
                this_obj.find("div.normalEventElement").height(this_obj.height())
            },
            resize: function(ev, ui) {
                var this_obj = jQuery(this)
                  , this_nee = this_obj.find("div.normalEventElement");
                this_nee.width(this_obj.width()),
                ui.originalPosition.left + ui.size.width > x_array[x_array_count - 1] && this_obj.width(x_array[x_array_count - 1] - ui.originalPosition.left),
                x_target_pos = ui.originalPosition.left,
                time_pos = ev.pageX > x_array[x_array_count - 1] ? x_array_count - 1 : 1;
                for (var i = 1; i < x_array_count; i++)
                    if (x_target_pos + ui.size.width <= x_array[i]) {
                        time_pos = i;
                        break
                    }
                this_obj.width(x_array[time_pos] - x_target_pos),
                x_target_pos = ui.position.left,
                set_hour = this_nee.data("event_set_hour"),
                unit = this_nee.data("event_unit"),
                offset_minute = _portal_frame.find("#show_users_timezonegroup_day:checked").length > 0 ? (offset_hour = this_nee.data("event_offset_hour"),
                this_nee.data("event_offset_minute")) : offset_hour = 0,
                time_pos = x_array_count - 1;
                for (var i = 1; i < x_array_count; i++)
                    if (x_target_pos + ui.size.width <= x_array[i]) {
                        time_pos = i;
                        break
                    }
                _end_minute = parseInt(offset_minute, 10) + parseInt(time_pos, 10) * parseInt(unit, 10),
                _end_hour = parseInt(set_hour, 10) + parseInt(offset_hour, 10) + Math.floor(parseInt(_end_minute, 10) / 60),
                _end_minute = parseInt(_end_minute, 10) % 60,
                _end_hour >= 24 ? _end_hour -= 24 : _end_hour < 0 && (_end_hour += 24),
                end_hour_str = _end_hour > 9 ? _end_hour : "0" + _end_hour,
                end_minute_str = _end_minute > 9 ? _end_minute : "0" + _end_minute,
                start_datetime_obj = this_nee.data("event_start_date").split(/\-|\:|\s/),
                d = new Date(start_datetime_obj[0],start_datetime_obj[1] - 1,start_datetime_obj[2]),
                "undefined" != typeof _short_date_format && "undefined" != typeof _locale && (time_str = parseday(d, _short_date_format, _locale) + "-" + end_hour_str + ":" + end_minute_str),
                (dd_tooltip = jQuery("#ddtooltip")).html(time_str),
                dd_tooltip.css("top", ev.pageY + 10),
                ev.pageX + dd_tooltip.width() > x_array[x_array_count - 1] ? dd_tooltip.css("left", ev.pageX - 20 - dd_tooltip.width()) : dd_tooltip.css("left", ev.pageX + 10)
            },
            stop: function(ev, ui) {
                resizing = 0,
                showing = 1,
                disable_tooltip = 0;
                var this_obj = jQuery(this)
                  , this_nee = this_obj.find("div.normalEventElement");
                pid = "undefined" != typeof plid && "" != plid ? plid : "",
                jQuery("#ddtooltip").remove(),
                body_obj.append('<div id="ddpopup"></div>');
                var ddpopup_obj = jQuery("#ddpopup");
                ddpopup_obj.append(_popup_loading),
                ddpopup_obj.css({
                    position: "absolute",
                    top: (window_obj.height() - ddpopup_obj.outerHeight()) / 2 + window_obj.scrollTop() + "px",
                    left: (window_obj.width() - ddpopup_obj.width()) / 2 + window_obj.scrollLeft() + "px"
                }),
                show_popup("ddpopup", ev.pageX, ev.pageY),
                ddpopup_obj.css("display", "inline");
                var div_overlay = jQuery('<div class="ddoverlay" id="overlay"/>').css({
                    width: document_obj.width() + "px",
                    height: document_obj.height() + "px"
                });
                jQuery("body").append(div_overlay),
                setTimeout(function() {
                    jQuery("a.btn_cancel").length && jQuery("a.btn_cancel").focus()
                }, 100),
                _event = this_nee.data("event_id"),
                _bdate = this_nee.data("event_bdate"),
                _sdate = (_sdate = this_nee.data("event_start_date")).substr(0, 10),
                _edate = (_edate = this_nee.data("event_end_date")).substr(0, 10),
                x_target_pos = this_obj.position().left,
                start_datetime_obj = this_nee.data("event_start_date").split(/\-|\:|\s/),
                end_datetime_obj = this_nee.data("event_end_date").split(/\-|\:|\s/),
                _start_hour = parseInt(start_datetime_obj[3], 10),
                _start_minute = parseInt(start_datetime_obj[4], 10),
                time_pos = x_array_count - 1;
                for (var i = 1; i < x_array_count; i++)
                    if (x_target_pos + ui.size.width <= x_array[i] - 1) {
                        time_pos = i;
                        break
                    }
                if (set_hour = this_nee.data("event_set_hour"),
                unit = this_nee.data("event_unit"),
                _end_minute = parseInt(time_pos, 10) * parseInt(unit, 10),
                _end_hour = parseInt(set_hour, 10) + Math.floor(parseInt(_end_minute, 10) / 60),
                _end_minute = parseInt(_end_minute, 10) % 60,
                _start_hour >= 24 && (_start_hour -= 24),
                parseInt(start_datetime_obj[3], 10) != _start_hour || parseInt(start_datetime_obj[4], 10) != _start_minute || parseInt(end_datetime_obj[3], 10) != _end_hour || parseInt(end_datetime_obj[4], 10) != _end_minute) {
                    var ddtd_id = this_obj.prop("id").substr(7)
                      , this_ddtd = _portal_frame.find("td.ddtd" + ddtd_id);
                    this_ddtd.addClass("ddtd_remove");
                    var ddtd_remove = _portal_frame.find("td.ddtd_remove");
                    ddtd_remove.removeClass("ddtd"),
                    ddtd_remove.removeClass("ddtd_middle"),
                    ddtd_remove.removeClass("group_day_calendar_item"),
                    ddtd_remove.removeClass("group_day_calendar_color_booked"),
                    ddtd_remove.removeClass("ddtd_hide"),
                    ddtd_remove.hasClass("group_day_calendar_item_conflicted") && (ddtd_remove.removeClass("group_day_calendar_item_conflicted"),
                    ddtd_remove.addClass("group_day_calendar_color_conflicted_line")),
                    ddtd_remove.find("div.normalEventElement").css("visibility", "hidden"),
                    this_ddtd.prev().hasClass("group_day_calendar_event_cell") ? this_ddtd.addClass("ddtd_prev_event_cell") : this_ddtd.addClass("ddtd_prev_item"),
                    (this_ddtd.next().hasClass("group_day_calendar_color_conflicted_line") || this_ddtd.next().hasClass("group_day_calendar_color_available")) && this_ddtd.next().addClass("ddtd_next"),
                    this_obj.css({
                        "padding-left": "1px",
                        position: "absolute"
                    }),
                    this_obj.attr("data-left_position", 0),
                    this_obj.attr("data-duration", time_pos);
                    for (var tr = _portal_frame.find("table.day_table").find("tr"), i = 0; i < tr.length; i++)
                        if (!tr.eq(i).hasClass("day_table_time_login") && this_obj.position().top + this_obj.height() / 2 > tr.eq(i).position().top && this_obj.position().top + this_obj.height() / 2 < tr.eq(i).position().top + tr.eq(i).height()) {
                            this_obj.attr("data-top_position", i);
                            break
                        }
                    this_obj.removeClass("dddummy"),
                    jQuery.ajax({
                        url: _url_ajax_checking,
                        data: {
                            event_id: _event,
                            bdate: _bdate,
                            end_date: _edate,
                            start_date: _sdate,
                            start_hour: _start_hour,
                            start_minute: _start_minute,
                            end_hour: _end_hour,
                            end_minute: _end_minute
                        },
                        type: "POST",
                        success: function(xhr, textStatus, jqXHR) {
                            grn.component.error_handler.hasCybozuError(jqXHR) ? GRN_ScheduleSimpleAdd.showErrMsg(jqXHR) : grn.component.error_handler.hasCybozuLogin(jqXHR) ? location.href = location.href : jQuery("#ddpopup").html(xhr)
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            GRN_ScheduleSimpleAdd.showErrMsg(jqXHR)
                        },
                        complete: function(xhr, status) {
                            setTimeout(function() {
                                jQuery(".aButtonStandard-grn").find("a.btn_yes").length ? jQuery("a.btn_yes").focus() : jQuery("a.btn_cancel").length && jQuery("a.btn_cancel").focus()
                            }, 100)
                        }
                    })
                } else
                    "undefined" != typeof plid && "" != plid ? (dd_remove(plid),
                    dd_init("", plid),
                    dd_handle(plid)) : (dd_remove(),
                    dd_init(),
                    dd_handle())
            }
        }),
        _portal_frame.find("div.dddummy").mouseover(function() {
            resizing || moving || (jQuery(this).addClass("dddummy_over"),
            jQuery(this).find("div.ddimg1").removeClass("ddimg_none"),
            jQuery(this).find("div.ddimg2").removeClass("ddimg_none"))
        }),
        _portal_frame.find("div.dddummy").mouseout(function() {
            resizing || moving || (jQuery(this).removeClass("dddummy_over"),
            jQuery(this).find("div.ddimg1").addClass("ddimg_none"),
            jQuery(this).find("div.ddimg2").addClass("ddimg_none"))
        })
    }
}
function show_popup(id, x, y) {
    var w = jQuery("#" + id).width()
      , h = jQuery("#" + id).height()
      , viewportW = jQuery(window).width() + jQuery(window).scrollLeft();
    viewportH = jQuery(window).height() + jQuery(window).scrollTop(),
    scrollTop = jQuery(window).scrollTop();
    var maxLeft, maxTop, left = (x > viewportW ? viewportW : x) - w / 2, top = (y > viewportH ? viewportH : y) - h;
    left + w >= viewportW && (left = viewportW - w - 2),
    left < 0 && (left = 0),
    top < scrollTop + 30 && (top = scrollTop + 30),
    top > viewportH && (top = viewportH - h - 10),
    jQuery("#" + id).css({
        top: top,
        left: left
    })
}
function ddpopup_form_submit() {
    jQuery.each(jQuery(".alter_fid"), function(k, v) {
        jQuery(this).is(":checked") && (_alter_fids += jQuery(this).val() + ",")
    }),
    jQuery.ajax({
        url: _url_ajax_command,
        data: {
            csrf_ticket: _csrf_ticket,
            event_id: _event,
            bdate: _bdate,
            end_date: _edate,
            start_date: _sdate,
            start_hour: _start_hour,
            start_minute: _start_minute,
            end_hour: _end_hour,
            end_minute: _end_minute,
            conflict_fids: _conflict_fids,
            alter_fids: _alter_fids
        },
        type: "POST",
        success: function(response, textStatus, jqXHR) {
            grn.component.error_handler.hasCybozuError(jqXHR) ? GRN_ScheduleSimpleAdd.showErrMsg(jqXHR) : grn.component.error_handler.hasCybozuLogin(jqXHR) ? location.href = location.href : _json = JSON.parse(response || "null")
        },
        error: function(jqXHR, textStatus, errorThrown) {
            GRN_ScheduleSimpleAdd.showErrMsg(jqXHR)
        },
        complete: function(jqXHR, textStatus) {
            grn.component.error_handler.hasCybozuError(jqXHR) || grn.component.error_handler.hasCybozuLogin(jqXHR) || ("undefined" == typeof pid ? dd_remove_before_sync() : dd_remove_before_sync(pid))
        }
    })
}
function ddpopup_cancel() {
    "undefined" == typeof pid ? (dd_remove(),
    dd_init(),
    dd_handle()) : (dd_remove(pid),
    dd_init("", pid),
    dd_handle(pid))
}
function enable_update_button(enable) {
    enable ? jQuery("#span_update").addClass("aButtonStandard-grn").removeClass("aButtonStandardDisable-grn") : jQuery("#span_update").addClass("aButtonStandardDisable-grn").removeClass("aButtonStandard-grn")
}
function parseday(datetime, pattern, locale) {
    var string_day;
    return string_day = datetime.getDate() < 10 ? pattern.replace("&&mday&&", "0" + datetime.getDate()) : pattern.replace("&&mday&&", datetime.getDate()),
    string_day = (string_day = (string_day = (string_day = (string_day = datetime.getMonth() + 1 < 10 ? string_day.replace("&&mon&&", "0" + (datetime.getMonth() + 1)) : string_day.replace("&&mon&&", datetime.getMonth() + 1)).replace("&&monthfull&&", GRN_DateResource[locale]["monthfull" + (datetime.getMonth() + 1)])).replace("&&year&&", datetime.getFullYear())).replace("&&wdayshort&&", GRN_DateResource[locale]["wdayshort" + datetime.getDay()])).replace("&&wdayfull&&", GRN_DateResource[locale]["wdayfull" + datetime.getDay()])
}
function rebuild_popup_title(portlet_id) {
    var is_show_tooltip = function(checkbox_id) {
        var $checkbox = jQuery("#" + checkbox_id);
        return $checkbox.length > 0 && !$checkbox.checked
    }, id_full_title;
    "undefined" !== typeof portlet_id && "" !== portlet_id ? is_show_tooltip(id_full_title = "show_full_titleportlet_personal_month" + portlet_id) ? showFullShortTitle(id_full_title, "view_personal_month_calendar" + portlet_id, "portlet_personal_month", false) : is_show_tooltip(id_full_title = "show_full_title" + portlet_id) && showFullShortTitle(id_full_title, "event_list" + portlet_id, "view_group_day", false) : is_show_tooltip(id_full_title = "show_full_titlepersonal_month") && showFullShortTitle(id_full_title, "personal_month_calendar", "personal_month", false)
}
!function($) {
    function resize_dialog(target) {
        var div_tag = target.next();
        if (div_tag.attr("style").indexOf("width") < 0) {
            for (var items_a = div_tag.find("a"), max = 0, change = true, i = 0; i < items_a.length; i++) {
                var str = items_a[i].innerHTML;
                if (max < str.length && (max = str.length) > 40) {
                    change = false;
                    break
                }
            }
            if (change) {
                var size_width = 0;
                target.innerWidth() < 0 ? div_tag.css({
                    width: "0px"
                }) : div_tag.css({
                    width: target.innerWidth() + "px"
                })
            }
        }
    }
    function show_dropdown(target) {
        target.parent().addClass("search-schedule-dropdownContents-open"),
        target.children().removeClass("search-schedule-ButtonTypeArrowDown-grn"),
        target.children().addClass("search-schedule-ButtonTypeArrowUp-grn")
    }
    function hide_dropdown(target) {
        target.parent().removeClass("search-schedule-dropdownContents-open"),
        target.children().removeClass("search-schedule-ButtonTypeArrowUp-grn"),
        target.children().addClass("search-schedule-ButtonTypeArrowDown-grn")
    }
    function show_divtag(target) {
        var div_tag = target.next()
          , switchOffset = target.position();
        div_tag.css({
            left: switchOffset.left + 1 + "px",
            top: switchOffset.top + 22 + "px"
        })
    }
    function changePositionSearchScheduleType() {
        $("span.search-schedule-dropdownContents-open").each(function() {
            show_divtag($(this).children())
        })
    }
    $(window.document).on("click", function(e) {
        var target = $(e.target);
        target.attr("id") || (target = target.parent()),
        0 != target.length && ($("span.search-schedule-dropdownContents-open").each(function() {
            var tag_a = $(this).children("a");
            tag_a.attr("id") ? tag_a.attr("id") != target.attr("id") && hide_dropdown(tag_a) : hide_dropdown(tag_a)
        }),
        target.attr("id") && target.attr("id").indexOf("type_search_schedule") >= 0 && (target.parent().hasClass("search-schedule-dropdownContents-open") ? hide_dropdown(target) : (show_dropdown(target),
        show_divtag(target),
        resize_dialog(target))))
    }),
    $(window).on("resize", function() {
        setTimeout(changePositionSearchScheduleType, 0)
    })
}(jQuery),
YAHOO.util.Event.addListener(window, "load", function() {
    var buttonElements = YAHOO.util.Dom.getElementsByClassName("moveButtonBaseDisable-grn", "span");
    YAHOO.util.Dom.replaceClass(buttonElements, "moveButtonBaseDisable-grn", "moveButtonBase-grn");
    var moveButtonArrowLeftTwo = YAHOO.util.Dom.getElementsByClassName("moveButtonArrowLeftTwoDisable-grn", "span");
    YAHOO.util.Dom.replaceClass(moveButtonArrowLeftTwo, "moveButtonArrowLeftTwoDisable-grn", "moveButtonArrowLeftTwo-grn");
    var moveButtonArrowLeft = YAHOO.util.Dom.getElementsByClassName("moveButtonArrowLeftDisable-grn", "span");
    YAHOO.util.Dom.replaceClass(moveButtonArrowLeft, "moveButtonArrowLeftDisable-grn", "moveButtonArrowLeft-grn");
    var moveButtonArrowRight = YAHOO.util.Dom.getElementsByClassName("moveButtonArrowRightDisable-grn", "span");
    YAHOO.util.Dom.replaceClass(moveButtonArrowRight, "moveButtonArrowRightDisable-grn", "moveButtonArrowRight-grn");
    var moveButtonArrowRightTwo = YAHOO.util.Dom.getElementsByClassName("moveButtonArrowRightTwoDisable-grn", "span");
    YAHOO.util.Dom.replaceClass(moveButtonArrowRightTwo, "moveButtonArrowRightTwoDisable-grn", "moveButtonArrowRightTwo-grn")
});
var arrSearch = [];
!function() {
    var Dom = YAHOO.util.Dom
      , Event = YAHOO.util.Event
      , SearchBox = function(id) {
        this.textElement_ = document.getElementById("searchbox-schedule-" + id),
        this.submitButton = document.getElementById("searchbox-submit-schedules-" + id),
        this.firstFlag = true,
        this.textField_ = this.textElement_ ? Dom.getFirstChild(this.textElement_) : null,
        this.defaultText = this.textField_ ? this.textField_.value : null,
        this.init()
    };
    SearchBox.prototype.clearSearchHint = function() {
        if (!this.firstFlag)
            return true;
        this.textField_.value = "",
        Dom.removeClass(this.textField_, "prefix-grn"),
        this.firstFlag = false
    }
    ,
    SearchBox.prototype.init = function() {
        var seft = this;
        this.textField_ && (Event.addListener(this.textField_, "focus", function() {
            seft.clearSearchHint()
        }),
        Event.addListener(this.textField_, "blur", function() {
            "" === seft.textField_.value && (seft.textField_.value = seft.defaultText,
            Dom.addClass(seft.textField_, "prefix-grn"),
            seft.firstFlag = true)
        })),
        this.submitButton && Event.addListener(this.submitButton, "click", function() {
            seft.clearSearchHint(),
            this.form.submit()
        })
    }
    ,
    grn.base.namespace("grn.component.schedule.search_box"),
    grn.component.schedule.search_box = SearchBox,
    Event.onDOMReady(function() {
        var arrSearchBox = YAHOO.util.Dom.getElementsByClassName("searchbox-keyword-schedule", "div");
        for (i = 0; i < arrSearchBox.length; i++) {
            var id = arrSearchBox[i].id.split("searchbox-schedule-")[1];
            arrSearch[id] = new SearchBox(id)
        }
    })
}(),
function() {
    var Y = YAHOO.util
      , Dom = YAHOO.util.Dom
      , Event = YAHOO.util.Event
      , enter = false
      , opened_dialog_id = "";
    GRN_ScheduleSimpleAdd = {
        topTriangle: '<div class="simpleAddTriangle-grn" style="display:none"><span class="simpleAddTriangleTop-grn"></span></div>',
        bottomTriangle: '<div class="simpleAddTriangle-grn" style=""><span class="simpleAddTriangleBottom-grn"></span></div>',
        dummyContent: '<div class="simpleAddBase-grn"><table class="simpleAddTable-grn simpleAddBaseFirst-grn"><tr valign="top" id="schedule_simple_add-header"><td colspan="2" class="header-grn"><div class="date-grn"></div><div class="simpleAddClose-grn" title="閉じめE></div><div class="clear_both_0px"></div></td></tr></table></div>',
        spinner: '<div class="spinnerBoxBase-grn" style="display:none; position:absolute; z-index: 1501"><div class="spinnerBox-grn"></div></div>',
        defaultSettings: null,
        cfg: function(configs) {
            this.defaultSettings = configs
        },
        init: function(target) {
            target.firstInit || (Event.removeListener(target, "dblclick", this.clickHandler),
            Event.addListener(target, "dblclick", this.clickHandler, this, true))
        },
        dialogInit: function(target, params) {
            if (target && !target.firstInit) {
                var callback_success = function(o) {
                    var dd;
                    dlgContent && (dlgContent.innerHTML = o.responseText),
                    new YAHOO.util.DD(dialog.id).setHandleElId("schedule_simple_add-header");
                    var dlg = new GRN_ScheduleSimpleAdd.Dialog(target,target.getAttribute("plid"),params);
                    target.dialog = dlg,
                    target.firstInit = true,
                    BuilderDropdownColorControl(targetid + "-dialog")
                }
                  , url_component = grn.component.url
                  , url_params = {}
                  , url = target.getAttribute("rel");
                if (null == url)
                    url_params = params ? (url = params.simpleAddUrl,
                    {
                        uid: params.uid,
                        gid: params.gid,
                        bdate: target.getAttribute("data-bdate") || params.bdate,
                        referer_key: params.refererKey,
                        members: params.members
                    }) : (url = url_component.page("schedule/simple_add"),
                    {
                        uid: target.getAttribute("data-uid"),
                        gid: target.getAttribute("data-gid"),
                        bdate: target.getAttribute("data-bdate"),
                        referer_key: target.getAttribute("data-referer_key")
                    });
                else {
                    var url_components = url_component.parse(url);
                    url_params = url_component.parseQueryString(url),
                    url = url_components.pathname
                }
                var targetid = Dom.generateId(target, "sch-simple-add")
                  , dialog = Dom.get(targetid + "-dialog")
                  , dlgContent = void 0;
                if (!dialog) {
                    var dd;
                    (dialog = this.createElement("div", {
                        id: targetid + "-dialog",
                        class: "dialog",
                        style: "display:none;z-index:1500;position:absolute"
                    })).innerHTML = this.topTriangle + this.dummyContent + this.bottomTriangle + this.spinner,
                    document.body.appendChild(dialog),
                    dlgContent = Y.Selector.query("#" + dialog.id + " > div.simpleAddBase-grn", null, true),
                    "facility" == target.getAttribute("utype") && Dom.addClass(dlgContent, "simpleAddBaseFirstFacility-grn"),
                    new YAHOO.util.DD(dialog.id).setHandleElId("schedule_simple_add-header");
                    var ajaxRequest = new grn.component.ajax.request({
                        url: url,
                        method: "POST",
                        grnRedirectOnLoginError: true,
                        data: grn.component.url.toQueryString(url_params)
                    });
                    ajaxRequest.on("beforeShowError", function() {
                        dialog.parentNode.removeChild(dialog)
                    }),
                    ajaxRequest.send().done(function(data, textStatus, jqXHR) {
                        callback_success(jqXHR)
                    })
                }
            }
        },
        getTarget: function(target) {
            if (!target.tagName || !target.parentNode)
                return null;
            var tagName;
            switch (target.tagName.toLowerCase()) {
            case "table":
                return null;
            case "td":
                return target;
            default:
                if (target.getAttribute("data-bdate"))
                    return target
            }
            return this.getTarget(target.parentNode)
        },
        lastIntervalID: -1,
        clickHandler: function(ev) {
            Dom.setStyle(Dom.getElementsByClassName("dialog", "div"), "display", "none"),
            clearInterval(this.lastIntervalID);
            var target = Event.getTarget(ev);
            if (target) {
                var tagName;
                if (target.tagName)
                    switch (target.tagName.toLowerCase()) {
                    case "a":
                    case "img":
                        return;
                    case "div":
                        if (Dom.hasClass(target, "iconWrite-grn"))
                            return
                    }
                if (target = this.getTarget(target)) {
                    var plid_attr = target.getAttribute("plid");
                    if (plid_attr > 0) {
                        if ("undefined" != typeof do_not_have_using_privilege && "undefined" != typeof do_not_have_using_privilege[plid_attr] && 1 == do_not_have_using_privilege[plid_attr])
                            return
                    } else if ("undefined" != typeof do_not_have_using_privilege && 1 == do_not_have_using_privilege)
                        return;
                    this.dialogInit(target);
                    var position = Event.getXY(ev);
                    if (target.dialog)
                        target.dialog.showAt(position);
                    else {
                        void 0 != target.clickCount && target.clickCount ? target.clickCount = parseInt(target.clickCount) + 1 : target.clickCount = 1;
                        var targetid = Dom.generateId(target, "sch-simple-add")
                          , dialog = Dom.get(targetid + "-dialog");
                        if (opened_dialog_id == Dom.getAttribute(dialog, "id"))
                            return;
                        this.showDlgAt(dialog, position),
                        opened_dialog_id = Dom.getAttribute(dialog, "id");
                        var spinner = Y.Selector.query("#" + targetid + "-dialog > div.spinnerBoxBase-grn", null, true);
                        Dom.setStyle(spinner, "display", "");
                        var region = Dom.getRegion(dialog)
                          , x = region.left + region.width / 2 - 32
                          , y = region.top + region.height / 2 - 32;
                        Dom.setXY(spinner, [x, y]);
                        var t = setInterval(function() {
                            target.dialog && (1 == target.clickCount && (opened_dialog_id = ""),
                            target.dialog.showAt(position),
                            clearInterval(t))
                        }, 1);
                        this.lastIntervalID = t
                    }
                    enter = true,
                    Event.stopPropagation(ev)
                } else
                    Event.stopPropagation(ev)
            }
        },
        doubleClickPersonalHandler: function(ev) {
            Dom.setStyle(Dom.getElementsByClassName("dialog", "div"), "display", "none"),
            clearInterval(this.lastIntervalID);
            var target = Event.getTarget(ev);
            if (target = this.getTarget(target)) {
                var params = this.defaultSettings;
                this.dialogInit(target, params);
                var position = Event.getXY(ev);
                if (target.dialog)
                    target.dialog.showAt(position);
                else {
                    void 0 != target.clickCount && target.clickCount ? target.clickCount = parseInt(target.clickCount) + 1 : target.clickCount = 1;
                    var targetid = Dom.generateId(target, "sch-simple-add")
                      , dialog = Dom.get(targetid + "-dialog");
                    if (opened_dialog_id == Dom.getAttribute(dialog, "id"))
                        return;
                    this.showDlgAt(dialog, position),
                    opened_dialog_id = Dom.getAttribute(dialog, "id");
                    var spinner = Y.Selector.query("#" + targetid + "-dialog > div.spinnerBoxBase-grn", null, true);
                    Dom.setStyle(spinner, "display", "");
                    var region = Dom.getRegion(dialog)
                      , x = region.left + region.width / 2 - 32
                      , y = region.top + region.height / 2 - 32;
                    Dom.setXY(spinner, [x, y]);
                    var t = setInterval(function() {
                        target.dialog && (1 == target.clickCount && (opened_dialog_id = ""),
                        target.dialog.showAt(position),
                        clearInterval(t))
                    }, 1);
                    this.lastIntervalID = t
                }
                enter = true,
                Event.stopPropagation(ev)
            }
        },
        enableDragAdd: function() {
            var self = this;
            this._allowDrag = function($) {
                var $view = $("#personal_calendar_list")
                  , container = $view.find(".personal_week_calendar_date");
                0 === container.length && (container = $view.find(".personal_day_calendar_date")),
                container.find("div.personal_day_calendar_time_row_alt").each(function() {
                    var jObject = $(this)
                      , jObjectParent = jObject.parent();
                    jObject.clone().attr("data-minute", 30).addClass("duplicate").appendTo(jObjectParent)
                });
                var _dragging = function(event, ui) {
                    ui.selecting ? $(ui.selecting).addClass("drag_add_grn") : ui.unselecting ? $(ui.unselecting).removeClass("drag_add_grn") : ui.selected && $(ui.selected).addClass("drag_add_grn").removeClass("ui-selected")
                }
                  , options = {
                    filter: "div.add_quick div.personal_day_calendar_time_row_alt",
                    distance: 5,
                    appendTo: "#personal_calendar_list",
                    start: function(event, ui) {
                        container.find("div.drag_add_grn").removeClass("drag_add_grn"),
                        $("div.dialog").css("display", "none"),
                        opened_dialog_id = ""
                    },
                    stop: function(event, ui) {
                        var ev = event.originalEvent || event
                          , selectedEle = container.find("div.drag_add_grn:first");
                        selectedEle.length > 0 && $(selectedEle).parent().data("bdate") && (self.defaultSettings.bdate = $(selectedEle).parent().data("bdate"));
                        var toElement = ev.target ? ev.target : ev.toElement;
                        if (false === $.contains(this, toElement))
                            return container.find("div.drag_add_grn").removeClass("drag_add_grn"),
                            false;
                        self.doubleClickPersonalHandler(ev)
                    },
                    selecting: function(event, ui) {
                        _dragging(event, ui)
                    },
                    unselecting: function(event, ui) {
                        _dragging(event, ui)
                    },
                    selected: function(event, ui) {
                        _dragging(event, ui)
                    }
                };
                container.selectable(options)
            }
            ,
            this._allowDrag(jQuery)
        },
        mouseOverHandler: function(e, obj) {
            "personal_week_calendar_data_cell" !== obj.className && "personal_day_calendar_data_cell" !== obj.className && "v_day_border02" !== obj.className || Dom.addClass(obj, "calendarHover-grn"),
            GRN_ScheduleSimpleAdd.init(obj)
        },
        mouseOutHandler: function(e, obj) {
            "personal_week_calendar_data_cell calendarHover-grn" !== obj.className && "personal_day_calendar_data_cell calendarHover-grn" !== obj.className && "v_day_border02 calendarHover-grn" !== obj.className || Dom.removeClass(obj, "calendarHover-grn")
        },
        showErrMsg: function(o, callback) {
            var s = o.responseText;
            try {
                JSON.parse(s),
                "function" == typeof callback && callback(),
                grn.component.error_handler.show(o)
            } catch (e) {
                document.write(s),
                YAHOO.env.ua.ie > 7 ? document.write("<script type='text/javascript'>setTimeout(function(){document.close();},0);<\/script>") : document.close()
            }
        },
        createElement: function(tag, configs) {
            var el = document.createElement(tag);
            for (var cfg in configs)
                Dom.setAttribute(el, cfg, configs[cfg]);
            return el
        },
        showDlgAt: function(dialog, position) {
            function _focus() {
                var focus2Ctrl = Y.Selector.query("#" + dialog.id + " input[type=text]", null, true);
                return !!focus2Ctrl && (focus2Ctrl.focus(),
                true)
            }
            Dom.setStyle(dialog, "display", "");
            var left = position[0] - dialog.offsetWidth / 2
              , top = position[1] + 20
              , viewportW = Dom.getViewportWidth() + Dom.getDocumentScrollLeft()
              , viewportH = Dom.getViewportHeight() + Dom.getDocumentScrollTop()
              , triangleClass = "simpleAddTriangleTop-grn"
              , triangleL = dialog.offsetWidth / 2;
            left + dialog.offsetWidth >= viewportW && (left = viewportW - dialog.offsetWidth - 2),
            left < 0 && (left = 0);
            var buffer = 37;
            top + dialog.offsetHeight > viewportH && top - dialog.offsetHeight - 37 > Dom.getDocumentScrollTop() ? (top = top - dialog.offsetHeight - 37,
            triangleClass = "simpleAddTriangleBottom-grn") : top += 2,
            triangleL = position[0] - left,
            Dom.setXY(dialog, [left, top]);
            var triangles = Y.Selector.query("#" + dialog.id + " div.simpleAddTriangle-grn");
            Dom.setStyle(triangles, "display", "none");
            var triangle = Y.Selector.query("#" + dialog.id + " span." + triangleClass, null, true);
            Dom.setStyle(triangle.parentNode, "display", ""),
            Dom.setStyle(triangle, "left", triangleL + "px"),
            _focus() || setTimeout(_focus, 1e3)
        },
        Dialog: function(target, plid, params) {
            this.dialogid = target.id + "-dialog",
            this.dialogid_selector = "#" + this.dialogid,
            this.plid = plid,
            this.spinner = Y.Selector.query(this.dialogid_selector + " > div.spinnerBoxBase-grn", null, true),
            this.errorMsgElements = Y.Selector.query(this.dialogid_selector + " div.simpleAddEventAttentionMessage-grn *"),
            this.usingPurposeError = Y.Selector.query(this.dialogid_selector + " div#using_purpose_error"),
            this._parent = target,
            this.params = params,
            this.dialog = null,
            this.getDialog = function() {
                return null == this.dialog && (this.dialog = Dom.get(this.dialogid)),
                this.dialog
            }
            ;
            var scheduleSimpleAddObject = this;
            this.close = function(e, dialog) {
                null == dialog && (dialog = this.getDialog()),
                dialog.style.display = "none",
                Dom.setStyle(this.spinner, "display", "none"),
                jQuery.resetDropdwonColorControl(),
                scheduleSimpleAddObject.clearSelected()
            }
            ,
            this.clearSelected = function() {
                Dom.getElementsByClassName("drag_add_grn", "div", "personal_calendar_list", function() {
                    Dom.removeClass(this, "drag_add_grn")
                })
            }
            ,
            this.validateForm = function(form) {
                form || (form = Y.Selector.query(self.dialogid_selector + " form", null, true));
                var start_hour = form.start_hour.value
                  , start_minute = form.start_minute.value
                  , end_hour = form.end_hour.value
                  , end_minute = form.end_minute.value;
                return Dom.setStyle(this.errorMsgElements, "display", "none"),
                "" == start_hour && "" != end_hour ? (Dom.setStyle(this.errorMsgElements[0], "display", ""),
                false) : "" !== start_hour && "" !== end_hour && ("" === start_minute && (start_minute = 0),
                "" === end_minute && (end_minute = 0),
                60 * start_hour + 1 * start_minute > 60 * end_hour + 1 * end_minute) ? (Dom.setStyle(this.errorMsgElements[1], "display", ""),
                false) : enter = true
            }
            ,
            this.setEndTime = function(form) {
                form || (form = Y.Selector.query(self.dialogid_selector + " form", null, true));
                var start_hour = form.start_hour.value
                  , start_minute = form.start_minute.value
                  , end_hour = form.end_hour.value
                  , end_minute = form.end_minute.value
                  , bar_login = Y.Selector.query("table#event_list" + (this._parent ? this._parent.getAttribute("plid") : "") + " tr.bar_login_timezone", null, true)
                  , hour = this._parent.getAttribute("data-hour");
                "" != start_hour && "" == start_minute && "" == end_hour && "" == end_minute && null == bar_login && null == hour && (form.end_hour.value = "23" == start_hour ? parseInt(start_hour) : parseInt(start_hour) + 1,
                form.start_minute.value = 0,
                form.end_minute.value = 0)
            }
            ,
            this.setTime = function(time) {
                var form = Y.Selector.query(this.dialogid_selector + " form", null, true);
                form && time && parseInt(time) && (form.start_hour.value != time && (opened_dialog_id = ""),
                form.start_hour.value = time,
                form.start_minute.value = 0,
                form.end_hour.value = parseInt(time) + 1,
                form.end_minute.value = 0)
            }
            ,
            this.setTimePersonal = function(hour, minute) {
                var form = Y.Selector.query(this.dialogid_selector + " form", null, true);
                form && NaN != parseInt(hour) && NaN != parseInt(minute) && (form.start_hour.value = hour,
                form.start_minute.value = minute,
                parseInt(hour) >= 23 ? (form.end_hour.value = 23,
                form.end_minute.value = 30) : (form.end_hour.value = parseInt(hour) + 1,
                form.end_minute.value = minute))
            }
            ,
            this.setCustomTime = function(start, end) {
                var form = Y.Selector.query(this.dialogid_selector + " form", null, true);
                form && (form.start_hour.value = start.hour,
                start.hour === end.hour ? 30 === start.minute ? (end.hour += 1,
                end.minute = 0) : start.minute === end.minute ? end.minute = 30 : (end.hour += 1,
                end.minute = 0) : 0 === end.minute ? end.minute = 30 : (end.hour += 1,
                end.minute = 0),
                end.hour >= 24 && (end.hour = 0,
                end.minute = 0),
                form.end_hour.value = end.hour,
                form.start_minute.value = start.minute,
                form.end_minute.value = end.minute)
            }
            ,
            this.removePopupTitle = function() {
                for (var popup_title = Y.Selector.query(".showEventTitle"), i = 0; i < popup_title.length; i++)
                    el = popup_title[i],
                    el.remove()
            }
            ,
            this.resetDialog = function() {
                var form = Y.Selector.query(this.dialogid_selector + " form", null, true);
                if (form) {
                    form.start_hour.selectedIndex = 0,
                    form.start_minute.selectedIndex = 0,
                    form.end_hour.selectedIndex = 0,
                    form.end_minute.selectedIndex = 0,
                    form.menu.selectedIndex = 0,
                    form.title.value = "",
                    form.memo.value = "",
                    "undefined" != typeof form.using_purpose && (form.using_purpose.value = "");
                    var usingPurposeError = Y.Selector.query(this.dialogid_selector + " div#using_purpose_error");
                    Dom.setStyle(usingPurposeError, "display", "none"),
                    2 == jQuery(form).find("input[name='private']:checked").val() && jQuery(form).find(".simpleAddEventPrivateSet-grn").show(),
                    jQuery(form).find("input[name='private']").change(function() {
                        2 == jQuery(this).val() ? jQuery(form).find(".simpleAddEventPrivateSet-grn").show() : jQuery(form).find(".simpleAddEventPrivateSet-grn").hide()
                    })
                }
            }
            ,
            this.validateUsingPurpose = function(form) {
                return void 0 == form.using_purpose || (Dom.setStyle(scheduleSimpleAddObject.usingPurposeError, "display", "none"),
                YAHOO.lang.trim(form.using_purpose.value)) ? enter = true : (Dom.setStyle(scheduleSimpleAddObject.usingPurposeError, "display", ""),
                false)
            }
            ;
            for (var radios = Y.Selector.query(this.dialogid_selector + " input[type=radio]"), i = 0; i < radios.length; i++) {
                var temp_id = Math.floor(1e4 * Math.random())
                  , nextSib = Dom.getNextSibling(radios[i]);
                nextSib && (radios[i].id = temp_id,
                Dom.setAttribute(nextSib, "for", temp_id))
            }
            Event.addListener(Y.Selector.query(this.dialogid_selector + " div.simpleAddClose-grn", null, true), "click", this.close, this.getDialog()),
            Event.addListener(Y.Selector.query(this.dialogid_selector + " select[id]"), "change", function(e, obj) {
                obj.validateForm(this.form)
            }, this),
            Event.addListener(Y.Selector.query(this.dialogid_selector + ' select[name="start_hour"]'), "change", function(e, obj) {
                obj.setEndTime(this.form)
            }, this),
            this.btn = Y.Selector.query(this.dialogid_selector + " div.buttonPostMain-grn a", null, true);
            var self = this;
            this.tranport = false;
            var buttonClick = function(e) {
                if (enter) {
                    self.removePopupTitle();
                    var theForm = Y.Selector.query(self.dialogid_selector + " form", null, true);
                    if (validatorTime = self.validateForm(theForm),
                    validatorTime && theForm["sITEM[]"] && (Dom.setStyle(self.errorMsgElements, "display", "none"),
                    "" !== theForm.start_hour.value && "" !== theForm.end_hour.value || (Dom.setStyle(self.errorMsgElements[2], "display", ""),
                    validatorTime = false)),
                    validatorUsingPurpose = self.validateUsingPurpose(theForm),
                    validatorTime && validatorUsingPurpose) {
                        if (!self.tranport) {
                            var region = Dom.getRegion(self.dialog)
                              , x = region.left + region.width / 2 - 32
                              , y = region.top + region.height / 2 - 32;
                            Dom.setStyle(self.spinner, "display", ""),
                            Dom.setXY(self.spinner, [x, y]),
                            this.tranport = true;
                            var ajaxRequest = new grn.component.ajax.request({
                                url: theForm.action,
                                method: "post",
                                data: jQuery(theForm).serialize()
                            });
                            ajaxRequest.on("beforeShowError", function() {
                                self.close(null, self.dialog)
                            }),
                            ajaxRequest.send().done(self._handleAfterSave.bind(self)).always(function() {
                                self.tranport = false
                            })
                        }
                        enter = false
                    }
                }
            };
            Event.addListener(this.btn, "click", buttonClick),
            this._handleAfterSave = function(data, textStatus, jqXHR) {
                var json = YAHOO.lang.JSON.parse(jqXHR.responseText);
                if (grn.base.isNamespaceDefined("grn.js.component.schedule.calendarUpdate")) {
                    self.close(null, self.dialog);
                    var event_data = {
                        portletId: self.plid,
                        eventId: json.event_id
                    };
                    grn.js.component.schedule.calendarUpdate.notify(event_data)
                }
                params && "function" == typeof params.onAddSuccess && (self.close(null, self.dialog),
                params.onAddSuccess(json.event_id))
            }
            ,
            this.link = Y.Selector.query(this.dialogid_selector + " a.icon-advance-grn", null, true),
            Event.addListener(this.link, "click", function(e) {
                if (Event.stopEvent(e),
                enter) {
                    enter = false;
                    var f = Y.Selector.query(self.dialogid_selector + " form", null, true);
                    f.target = "_self",
                    f.action = this.href;
                    var input_hidden_add = document.createElement("input");
                    input_hidden_add.setAttribute("type", "hidden"),
                    input_hidden_add.setAttribute("name", "tab"),
                    input_hidden_add.setAttribute("value", "add"),
                    f.appendChild(input_hidden_add);
                    var input_hidden_referer_key = document.createElement("input");
                    input_hidden_referer_key.setAttribute("type", "hidden"),
                    input_hidden_referer_key.setAttribute("name", "referer_key"),
                    input_hidden_referer_key.setAttribute("value", this.getAttribute("referer")),
                    f.appendChild(input_hidden_referer_key),
                    f.submit()
                }
            }),
            Event.on(this.dialog, "keydown", function(e) {
                var keyCode;
                switch (GRN_Event.keyCode(e)) {
                case 27:
                    this.close(),
                    jQuery.resetDropdwonColorControl();
                    break;
                case 13:
                    var et = Event.getTarget(e);
                    if (!et || !et.type)
                        break;
                    if ("input" == et.tagName.toLowerCase() && "radio" == et.type) {
                        Event.preventDefault(e),
                        buttonClick();
                        break
                    }
                    "textarea" != et.tagName.toLowerCase() && et != this.link && (Event.preventDefault(e),
                    buttonClick());
                    break;
                default:
                    return
                }
            }, this, true),
            Event.on(document, "keydown", function(e) {
                var keyCode;
                27 == GRN_Event.keyCode(e) && (Dom.setStyle(Y.Selector.query("div.dialog"), "display", "none"),
                jQuery.resetDropdwonColorControl(),
                self.clearSelected()),
                opened_dialog_id = ""
            }),
            Event.on(this.dialog, "click", function(e) {
                Event.stopPropagation(e)
            })
        }
    },
    GRN_ScheduleSimpleAdd.Dialog.prototype.showAt = function(position) {
        this.removePopupTitle(),
        this.resetDialog();
        var dialog = this.getDialog();
        if (dialog) {
            Dom.removeClass(Y.Selector.query("#" + dialog.id + " div.simpleAddBaseFirstFacility-grn", null, true), "simpleAddBaseFirstFacility-grn");
            var bar_login = Y.Selector.query("table#event_list" + (this._parent ? this._parent.getAttribute("plid") : "") + " tr.bar_login_timezone", null, true);
            if (bar_login) {
                var bar_login_children = bar_login.children
                  , whereX = 0
                  , positionXs = []
                  , clickX = position[0];
                "none" == bar_login.style.display ? (Dom.setStyle(bar_login, "display", ""),
                positionXs = Dom.getX(bar_login_children),
                Dom.setStyle(bar_login, "display", "none")) : positionXs = Dom.getX(bar_login_children);
                for (var i = 0; i < positionXs.length; i++)
                    if (!(clickX > positionXs[i])) {
                        whereX = i;
                        break
                    }
                whereX = 0 == whereX ? positionXs.length : whereX,
                this.setTime(bar_login_children[whereX - 1].innerHTML)
            }
            var personalCalendar = jQuery("#personal_calendar_list td.personal_week_calendar_date");
            0 === personalCalendar.length && (personalCalendar = jQuery("#personal_calendar_list td.personal_day_calendar_date"));
            var start = personalCalendar.find("div.drag_add_grn:first")
              , end = personalCalendar.find("div.drag_add_grn:last");
            if (0 != start.length) {
                var startTime = {
                    hour: parseInt(start.data("hour")),
                    minute: parseInt(start.data("minute"))
                }
                  , endTime = {
                    hour: parseInt(end.data("hour")),
                    minute: parseInt(end.data("minute"))
                };
                this.setCustomTime(startTime, endTime)
            } else {
                var hour = this._parent.getAttribute("data-hour");
                if (hour) {
                    var minute = this._parent.getAttribute("data-minute");
                    this.setTimePersonal(hour, minute)
                }
            }
            Dom.setStyle(this.spinner, "display", "none"),
            opened_dialog_id = opened_dialog_id != this.dialogid ? (GRN_ScheduleSimpleAdd.showDlgAt(dialog, position),
            this.dialogid) : ""
        }
    }
    ,
    GRN_ScheduleSimpleAdd.Dialog.Highlight = function() {
        function ColorToHex(color) {
            if (color.indexOf("rgba") >= 0) {
                var m = /rgba?\((\d+), (\d+), (\d+)/.exec(color);
                return m ? "#" + (m[1] << 16 | m[2] << 8 | m[3]).toString(16) : color
            }
            if ("#" === color.substr(0, 1))
                return color;
            var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color), red = parseInt(digits[2]), green = parseInt(digits[3]), blue, rgb = parseInt(digits[4]) | green << 8 | red << 16;
            return digits[1] + "#" + rgb.toString(16)
        }
        var Y = YAHOO.util
          , Dom = Y.Dom
          , highlightEls = Y.Selector.query(".newevent-grn");
        Dom.removeClass(highlightEls, "newevent-grn");
        var tempEl = GRN_ScheduleSimpleAdd.createElement("div", {
            class: "addHighlight-grn",
            style: "display:none"
        });
        document.body.appendChild(tempEl);
        var highlightColor = Dom.getStyle(tempEl, "backgroundColor");
        highlightColor = ColorToHex(highlightColor),
        document.body.removeChild(tempEl);
        for (var anims = [], transparentPattern = /^transparent|rgba\(0, 0, 0, 0\)$/, i = 0; i < highlightEls.length; i++) {
            var oldBgColor = Dom.getStyle(highlightEls[i], "backgroundColor")
              , parent = null
              , bgColor = oldBgColor;
            transparentPattern.test(oldBgColor) && ((parent = Dom.getAncestorBy(highlightEls[i], function(node) {
                return bgColor = Dom.getStyle(node, "backgroundColor"),
                !transparentPattern.test(bgColor)
            })) || (bgColor = "#fff")),
            bgColor = ColorToHex(bgColor),
            Dom.addClass(highlightEls[i], "rounded");
            var anim = new Y.ColorAnim(highlightEls[i],{
                backgroundColor: {
                    from: bgColor,
                    to: highlightColor
                }
            },.5,Y.Easing.easeNone);
            anim.onComplete.subscribe(function() {
                var el = this.getEl();
                setTimeout(function() {
                    var fadeOut = new Y.ColorAnim(el,{
                        backgroundColor: {
                            from: highlightColor,
                            to: bgColor
                        }
                    },.5,Y.Easing.easeNone);
                    fadeOut.onComplete.subscribe(function() {
                        transparentPattern.test(oldBgColor) && Dom.setStyle(el, "backgroundColor", oldBgColor),
                        Dom.removeClass(el, "rounded")
                    }),
                    fadeOut.animate()
                }, 1e3)
            }),
            anims.push(anim)
        }
        for (var i = 0; i < anims.length; i++)
            anims[i].animate()
    }
    ,
    GRN_ScheduleSimpleAdd.Dialog.highlight = GRN_ScheduleSimpleAdd.Dialog.Highlight,
    GRN_ScheduleTooltipDialog = {
        topTriangle: '<div class="simpleAddTriangle-grn" style="display:none"><span class="simpleAddTriangleTop-grn"></span></div>',
        bottomTriangle: '<div class="simpleAddTriangle-grn" style=""><span class="simpleAddTriangleBottom-grn"></span></div>',
        dummyContent: '<div class="simpleAddBase-grn"><table class="simpleAddTable-grn simpleAddBaseFirst-grn"><tr valign="top" id="schedule_simple_add-header"><td colspan="2" class="header-grn"><div class="date-grn"></div><div class="simpleAddClose-grn" title="閉じめE></div><div class="clear_both_0px"></div></td></tr></table></div>',
        dialogInit: function(target, position) {
            if (target) {
                var targetid = Dom.generateId(target, "sch-simple-add"), dialog = Dom.get(targetid + "-dialog"), dlgContent = void 0, dd;
                if (!dialog)
                    (dialog = GRN_ScheduleSimpleAdd.createElement("div", {
                        id: targetid + "-dialog",
                        class: "tooltip_dialog",
                        style: "display:none;z-index:1500;position:absolute"
                    })).innerHTML = this.topTriangle + this.dummyContent + this.bottomTriangle,
                    document.body.appendChild(dialog),
                    dlgContent = Y.Selector.query("#" + dialog.id + " > div.simpleAddBase-grn", null, true),
                    new YAHOO.util.DD(dialog.id).setHandleElId("schedule_simple_add-header"),
                    GRN_ScheduleSimpleAdd.showDlgAt(dialog, position)
            }
        },
        clickHandler: function(ev) {
            tooltip_dialog = Dom.getElementsByClassName("tooltip_dialog", "div");
            for (var i = 0; i < tooltip_dialog.length; i++)
                el = tooltip_dialog[i],
                el.remove();
            var target = Event.getTarget(ev);
            if (target) {
                var tagName;
                if (target.tagName)
                    switch (target.tagName.toLowerCase()) {
                    case "div":
                        if (Dom.hasClass(target, "ui-resizable-handle"))
                            return
                    }
                if (target = this.getTarget(target)) {
                    var position = Event.getXY(ev);
                    this.dialogInit(target, position),
                    Event.stopPropagation(ev)
                } else
                    Event.stopPropagation(ev)
            }
        },
        getTarget: function(target) {
            return target.tagName && target.parentNode ? "table" == target.tagName.toLowerCase() ? null : "div" == target.tagName.toLowerCase() && target.className.indexOf("critical3") >= 0 ? target : this.getTarget(target.parentNode) : null
        }
    },
    Event.on(document, "keydown", function(e) {
        if ("undefined" !== typeof Y.Selector) {
            var keyCode;
            if (27 == GRN_Event.keyCode(e)) {
                Dom.setStyle(Y.Selector.query("div.dialog"), "display", "none"),
                jQuery.resetDropdwonColorControl();
                for (var tooltip_dialog = Dom.getElementsByClassName("tooltip_dialog", "div"), i = 0; i < tooltip_dialog.length; i++)
                    el = tooltip_dialog[i],
                    el.remove()
            }
            opened_dialog_id = ""
        }
    }),
    Event.onDOMReady(function() {
        var els = Y.Selector.query("td[rel]");
        Event.addListener(els, "mouseover", function(e) {
            GRN_ScheduleSimpleAdd.mouseOverHandler(e, this)
        }),
        Event.addListener(els, "mouseout", function(e) {
            GRN_ScheduleSimpleAdd.mouseOutHandler(e, this)
        }),
        Event.addListener(window.document, "click", function(e) {
            if (0 === e.button) {
                var eTarget = Event.getTarget(e);
                eTarget && (Dom.hasClass(eTarget.parentNode, "ui-selectable") || eTarget.parentNode && Dom.hasClass(eTarget.parentNode.parentNode, "ui-selectable")) || (Dom.setStyle(Y.Selector.query("div.dialog"), "display", "none"),
                jQuery.resetDropdwonColorControl(),
                opened_dialog_id = "",
                Dom.getElementsByClassName("drag_add_grn", "div", "personal_calendar_list", function() {
                    Dom.removeClass(this, "drag_add_grn")
                }));
                for (var tooltip = Y.Selector.query("div.tooltip_dialog"), i = 0; i < tooltip.length; i++) {
                    var el;
                    tooltip[i].remove()
                }
            }
        })
    })
}();
var AjaxObject = {
    processResult: function(o) {
        "function" == typeof processResult && processResult(o)
    },
    sendRequest: function(method, url, processResult) {
        var self = this, ajaxRequest;
        self.processResult = processResult,
        new grn.component.ajax.request({
            url: url,
            method: method
        }).send().done(function(data, textStatus, jqXHR) {
            self.processResult(jqXHR),
            GRN_ScheduleSimpleAdd.Dialog.Highlight()
        })
    }
};
jQuery.extend({
    resetDropdwonColorControl: function() {
        var ul = jQuery("dl.pulldownbutton_standard_grn dd ul.dropdownContents_grn")
          , elespan = jQuery("dl.pulldownbutton_standard_grn dt a").find("span");
        null != elespan && elespan.length > 0 && (jQuery(elespan[0]).removeAttr("class"),
        jQuery(elespan[1]).text(ul.children(":first").attr("te")),
        jQuery(elespan[2]).removeClass("pulldownbutton_arrow_up_grn").addClass("pulldownbutton_arrow_down_grn"),
        ul.css("display", "none"))
    }
}),
jQuery(document).ready(function() {
    BuilderDropdownColorControl()
}),
GRN_DateResource = {
    en: {
        wdayfull0: "Sunday",
        wdayfull1: "Monday",
        wdayfull2: "Tuesday",
        wdayfull3: "Wednesday",
        wdayfull4: "Thursday",
        wdayfull5: "Friday",
        wdayfull6: "Saturday",
        wdayshort0: "Sun",
        wdayshort1: "Mon",
        wdayshort2: "Tue",
        wdayshort3: "Wed",
        wdayshort4: "Thu",
        wdayshort5: "Fri",
        wdayshort6: "Sat",
        monthfull1: "January",
        monthfull2: "February",
        monthfull3: "March",
        monthfull4: "April",
        monthfull5: "May",
        monthfull6: "June",
        monthfull7: "July",
        monthfull8: "August",
        monthfull9: "September",
        monthfull10: "October",
        monthfull11: "November",
        monthfull12: "December",
        monthshort1: "Jan",
        monthshort2: "Feb",
        monthshort3: "Mar",
        monthshort4: "Apr",
        monthshort5: "May",
        monthshort6: "Jun",
        monthshort7: "Jul",
        monthshort8: "Aug",
        monthshort9: "Sep",
        monthshort10: "Oct",
        monthshort11: "Nov",
        monthshort12: "Dec"
    },
    ja: {
        wdayfull0: "日",
        wdayfull1: "月",
        wdayfull2: "火",
        wdayfull3: "水",
        wdayfull4: "木",
        wdayfull5: "金",
        wdayfull6: "土",
        wdayshort0: "日",
        wdayshort1: "月",
        wdayshort2: "火",
        wdayshort3: "水",
        wdayshort4: "木",
        wdayshort5: "金",
        wdayshort6: "土",
        monthfull1: "1",
        monthfull2: "2",
        monthfull3: "3",
        monthfull4: "4",
        monthfull5: "5",
        monthfull6: "6",
        monthfull7: "7",
        monthfull8: "8",
        monthfull9: "9",
        monthfull10: "10",
        monthfull11: "11",
        monthfull12: "12",
        monthshort1: "1",
        monthshort2: "2",
        monthshort3: "3",
        monthshort4: "4",
        monthshort5: "5",
        monthshort6: "6",
        monthshort7: "7",
        monthshort8: "8",
        monthshort9: "9",
        monthshort10: "10",
        monthshort11: "11",
        monthshort12: "12"
    },
    zh: {
        wdayfull0: "星期天",
        wdayfull1: "星期一",
        wdayfull2: "星期二",
        wdayfull3: "星期三",
        wdayfull4: "星期四",
        wdayfull5: "星期五",
        wdayfull6: "星期六",
        wdayshort0: "周日",
        wdayshort1: "周一",
        wdayshort2: "周二",
        wdayshort3: "周三",
        wdayshort4: "周四",
        wdayshort5: "周五",
        wdayshort6: "周六",
        monthfull1: "1",
        monthfull2: "2",
        monthfull3: "3",
        monthfull4: "4",
        monthfull5: "5",
        monthfull6: "6",
        monthfull7: "7",
        monthfull8: "8",
        monthfull9: "9",
        monthfull10: "10",
        monthfull11: "11",
        monthfull12: "12",
        monthshort1: "1",
        monthshort2: "2",
        monthshort3: "3",
        monthshort4: "4",
        monthshort5: "5",
        monthshort6: "6",
        monthshort7: "7",
        monthshort8: "8",
        monthshort9: "9",
        monthshort10: "10",
        monthshort11: "11",
        monthshort12: "12"
    },
    "zh-tw": {
        wdayfull0: "星期天",
        wdayfull1: "星期一",
        wdayfull2: "星期二",
        wdayfull3: "星期三",
        wdayfull4: "星期四",
        wdayfull5: "星期五",
        wdayfull6: "星期六",
        wdayshort0: "週日",
        wdayshort1: "週一",
        wdayshort2: "週二",
        wdayshort3: "週三",
        wdayshort4: "週四",
        wdayshort5: "週五",
        wdayshort6: "週六",
        monthfull1: "1",
        monthfull2: "2",
        monthfull3: "3",
        monthfull4: "4",
        monthfull5: "5",
        monthfull6: "6",
        monthfull7: "7",
        monthfull8: "8",
        monthfull9: "9",
        monthfull10: "10",
        monthfull11: "11",
        monthfull12: "12",
        monthshort1: "1",
        monthshort2: "2",
        monthshort3: "3",
        monthshort4: "4",
        monthshort5: "5",
        monthshort6: "6",
        monthshort7: "7",
        monthshort8: "8",
        monthshort9: "9",
        monthshort10: "10",
        monthshort11: "11",
        monthshort12: "12"
    }
};
