//DOMツリーが構築し終えたときに発生するイベント
function upload_init() {
    grn.browser.isSupportHTML5 ? (document.getElementById("html5_content").style.display = "",
    jQuery("#not_support_html5_content").remove()) : (document.getElementById("not_support_html5_content").style.display = "",
    jQuery("#html5_content").remove(),
    grn.component.button("#cabinet_button_add").disable(),
    grn.component.button("#memo_button_save").disable())
}
function expandTextarea(textarea) {
    textarea.rows < 5 && (textarea.rows = 5)
}
function confirmIfExistFailedFile() {
    return !window.isExistFailedFile || confirm(__upload_msg_confirm1 + "\n" + __upload_msg_confirm2)
}
function handleDrag(evt) {
    if (!(evt.dataTransfer.types[0].toString().indexOf("text") >= 0)) {
        evt.preventDefault(),
        evt.stopPropagation();
        var type = evt.type;
        if ("drop" == type)
            return jQuery(dropArea).removeClass("over"),
            jQuery.each(jQuery(".file_input_div").toArray(), function(k, e) {
                jQuery(e).removeClass("drag_in")
            }),
            isCheck = false,
            valid = count = 0,
            void clearTimeout(t);
        "dragover" == type && (jQuery(dropArea).addClass("over"),
        jQuery.each(jQuery(".file_input_div").toArray(), function(k, e) {
            jQuery(e).addClass("drag_in")
        }),
        evt.dataTransfer.dropEffect = "none"),
        count++,
        isCheck || (checkDragging(),
        isCheck = true)
    }
}
function checkDragging() {
    valid < count ? (valid = count,
    t = setTimeout("checkDragging()", 400)) : (jQuery(dropArea).removeClass("over"),
    jQuery.each(jQuery(".file_input_div").toArray(), function(k, e) {
        jQuery(e).removeClass("drag_in")
    }),
    isCheck = false,
    valid = count = 0,
    clearTimeout(t))
}
function traverseFiles(files) {
    if ("undefined" !== typeof files)
        for (var i = 0, l = files.length; i < l; i++)
            fileUpload = new FileUpload(files[i])
}
if (jQuery(document).ready(function() {
    upload_init(),
    grn.browser.isSupportHTML5 && jQuery.each(jQuery(".file_input_div").toArray(), function() {
        jQuery(this).mouseover(function() {
            jQuery(this).addClass("over")
        }),
        jQuery(this).mouseout(function() {
            jQuery(this).removeClass("over")
        })
    })
}),
grn.browser.isSupportHTML5) {
    var filesUpload = document.getElementById("file_upload_"), dropArea = document.getElementById("drop_"), isCheck = false, count = 0, valid = 0, t;
    window.addEventListener("dragover", handleDrag, false),
    window.addEventListener("drop", handleDrag, false),
    filesUpload.addEventListener("change", function() {
        traverseFiles(this.files),
        self.filesUpload.value = ""
    }, false),
    dropArea.addEventListener("dragleave", function(evt) {
        evt.preventDefault(),
        evt.stopPropagation()
    }, false),
    dropArea.addEventListener("dragenter", function(evt) {
        evt.preventDefault(),
        evt.stopPropagation()
    }, false),
    dropArea.addEventListener("dragover", function(evt) {
        count++,
        evt.dataTransfer.dropEffect = "copy",
        evt.preventDefault(),
        evt.stopPropagation()
    }, false),
    dropArea.addEventListener("drop", function(evt) {
        jQuery(dropArea).removeClass("over"),
        jQuery.each(jQuery("file_input_div").toArray(), function(k, e) {
            jQuery(e).removeClass("drag_in")
        }),
        traverseFiles(evt.dataTransfer.files),
        evt.preventDefault(),
        evt.stopPropagation()
    }, false),
    html5_uploadStatuses = new Object;
    var FileUpload = function(file) {
        this.file = file,
        this.xhr = null,
        this.responseText = "",
        this.fid = null,
        this.started = false,
        this.init()
    };
    FileUpload.prototype.init = function() {
        this.triggerUpload()
    }
    ,
    FileUpload.prototype.cancelUpload = function(e, fileid) {
        e.preventDefault(),
        this.xhr && this.endProcess(0),
        jQuery("#row_" + fileid).remove(),
        this.completeUpload(fileid)
    }
    ,
    FileUpload.prototype.triggerUpload = function() {
        this.sendDatas()
    }
    ,
    FileUpload.prototype.sendDatas = function() {
        var self = this;
        if (this.xhr = new XMLHttpRequest,
        this.fid = this.randomString(),
        this.onUploadOpen(this.fid, this.file.name, this.file.size),
        0 == this.file.size)
            return this.onUploadError(this.fid, "ZEROBYTE_FILE"),
            void (this.xhr = null);
        this.xhr && (this.xhr.upload.addEventListener("loadstart", function(e) {
            var t = setTimeout(self.checkRequest.bind(self), 6e5)
        }, false),
        this.xhr.upload.addEventListener("progress", function(e) {
            self.updateProgress(e, self.fid)
        }, false),
        this.xhr.upload.addEventListener("load", function(e) {
            self.endUpload(e, self.fid, self)
        }, false),
        this.xhr.upload.addEventListener("abort", function() {
            self.closeXhr(self)
        }, false),
        this.xhr.upload.addEventListener("error", function(e) {
            self.errorUpload(e)
        }, false),
        this.xhr.ontimeout = function(e) {
            alert("Request timed out.")
        }
        ,
        this.xhr.onreadystatechange = function() {
            if (this.readyState,
            4 == this.readyState) {
                var status = this.status;
                200 == status && self.endProcess(1),
                404 != status && 500 != status || self.endProcess(0)
            }
        }
        ,
        this.processUpload(this.fid))
    }
    ,
    FileUpload.prototype.processUpload = function(fid) {
        var data = new FormData;
        data.append("file", this.file),
        this.xhr.open("POST", __upload_url, true),
        this.xhr.setRequestHeader("X-File-Name", escape(this.file.name)),
        this.xhr.setRequestHeader("X-File-Type", this.file.type),
        this.xhr.setRequestHeader("X-File-Size", this.file.size),
        this.xhr.setRequestHeader("X-File-Id", fid),
        this.xhr.setRequestHeader("X-File-Upload-Ticket", __upload_ticket),
        this.xhr.send(data)
    }
    ,
    FileUpload.prototype.updateProgress = function(e, fid) {
        this.started = true,
        e.lengthComputable && (document.getElementById(fid + "_progress").style.width = e.loaded / e.total * 100 + "%")
    }
    ,
    FileUpload.prototype.endUpload = function(e, fid, self) {
        this.started = true
    }
    ,
    FileUpload.prototype.endProcess = function(control) {
        if (this.xhr) {
            if (this.xhr.responseText)
                var response = JSON.parse(this.xhr.responseText);
                if ('COMPLETE' == response.success) {
                    document.getElementById(this.fid + "_size").innerHTML = '<input type="hidden" id="' + this.fid + '_size" name="size[]" value="' + response.size + '">' + response.size,
                    document.getElementById(this.fid + "_name").innerHTML = '<input type="hidden" id="' + this.fid + '_name" name="file_name[]" value="' + response.name + '">' + response.name,        
                    document.getElementById(this.fid + "_progressItem").innerHTML = "",
                    document.getElementById(this.fid + "_message").innerHTML = "",
                    document.getElementById(this.fid + "_check").innerHTML = '<input type="checkbox" id="' + this.fid + '_checkbox" name="link[]" value="' + response.link + '" checked />';
                    var self = this;
                    document.getElementById(this.fid + "_check").addEventListener("click", function(e) {
                        self.toggleRowView(self.fid)
                    }, false),
                    document.getElementById(this.fid + "_checkbox").checked = true,
                    this.completeUpload(this.fid)
                } else
                    this.onUploadError(this.fid, response.success);
            this.xhr.abort()
        }
    }
    ,
    FileUpload.prototype.errorUpload = function(e) {}
    ,
    FileUpload.prototype.closeXhr = function() {
        this.xhr = null
    }
    ,
    FileUpload.prototype.checkRequest = function() {
        false == this.started && this.xhr && (this.onUploadError(this.fid, "REQUEST_TIME_OUT"),
        this.xhr.abort())
    }
    ,
    FileUpload.prototype.onUploadOpen = function(fileid, filename, size, e) {
        var self = this;
        filename = filename.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        var row = document.getElementById("fileTable").insertRow(-1);
        row.id = "row_" + fileid;
        var cell1 = row.insertCell(-1);
        cell1.id = fileid + "_check",
        cell1.vAlign = "middle";
        var cell2 = row.insertCell(-1);
        cell2.style.whiteSpace = "normal",
        cell2.id = fileid + "_name",        
        cell2.innerHTML = filename;
        var cell3 = row.insertCell(-1);
        cell3.id = fileid + "_size",
        cell3.innerHTML = this._getSizeString(size);
        var cell4 = row.insertCell(-1);
        cell4.id = fileid + "_progressItem",
        cell4.innerHTML = this._getProgressItem(fileid);
        var cell8 = row.insertCell(-1), cell5;
        cell8.id = fileid + "_message",
        cell8.style.whiteSpace = "nowrap",
        cell8.innerHTML = '<a href="javascript:;">' + __upload_msg_cancel + "</a>",
        cell8.firstChild && jQuery(cell8.firstChild).click(function(e) {
            self.cancelUpload(e, self.fid)
        }),
        row.insertCell(-1).innerHTML = this._getTitleForm(fileid);
        var cell6 = row.insertCell(-1);
        cell6.style.whiteSpace = "nowrap",
        cell6.appendChild(this._getVersionSelectForm(fileid)),
        cell6.innerHTML += __upload_msg_version_suffix;
        var cell7 = row.insertCell(-1);
        cell7.id = fileid + "_descriptionItem",
        cell7.innerHTML = this._getDescriptionForm(fileid),
        this.startUpload(fileid)
    }
    ,
    FileUpload.prototype.onUploadComplete = function(fileid) {}
    ,
    FileUpload.prototype.randomString = function() {
        for (var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz", string_length = 35, randomstring = "", i = 0; i < 35; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1)
        }
        return randomstring
    }
    ,
    FileUpload.prototype._getProgressItem = function(fileid) {
        return '<div style="border: 1px solid rgb(176, 176, 176); padding: 0px; width: 200px; height: 10px;" id="div_' + fileid + '"><div id="' + fileid + '_progress" style="width:0%;background-color:lightgreen;height:100%;"></div></div>'
    }
    ,
    FileUpload.prototype._getSizeString = function(size) {
        return suffix = new Array("B","KB","MB","GB","TB","PB","EB","ZB","YB"),
        idx = parseInt((String(size).length - 1) / 3),
        ret = (size / Math.pow(1024, idx)).toFixed(1) + suffix[idx],
        ret.replace(".0", "")
    }
    ,
    FileUpload.prototype._getVersionSelectForm = function(fileid) {
        var select = document.getElementById("version_select").cloneNode(true);
        if (!(window.attachEvent && -1 == navigator.userAgent.indexOf("Opera")))
            select.name = "max_version[]",
            select.id = "version_" + fileid;
        else {
            var div = document.createElement("div");
            div.setAttribute("id", "div_version_" + fileid),
            div.id = "div_version_" + fileid,
            div.style.display = "inline",
            div.innerHTML = '<select id="version_' + fileid + '" name="max_version[]">' + select.innerHTML + "</select>",
            select = div
        }
        return select
    }
    ,
    FileUpload.prototype._getDescriptionForm = function(fileid) {
        return '<textarea id="description[]" name="memo[]" class="autoexpand" wrap="virtual" style="width:100%;" rows="1" onfocus="expandTextarea(this);" ></textarea>'
    }
    ,
    FileUpload.prototype._getTitleForm = function(fileid) {
        return '<input type="text" style="width:100%;" name="subject[]" />'
    }
    ,
    FileUpload.prototype.onUploadError = function(fileid, errorcode) {
        isExistFailedFile = true,
        document.getElementById("row_" + fileid).deleteCell(7),
        document.getElementById("row_" + fileid).deleteCell(6),
        document.getElementById("row_" + fileid).deleteCell(5),
        document.getElementById("row_" + fileid).deleteCell(4),
        document.getElementById(fileid + "_progressItem").colSpan = 5,
        document.getElementById(fileid + "_progressItem").innerHTML = "ZEROBYTE_FILE" == errorcode ? this.createErrorMessageTag(__upload_msg_zerobyte_file) : "INVALID_TICKET" == errorcode || "OVER_INI_SIZE" == errorcode || "OVER_FORM_SIZE" == errorcode || "NO_FILE" == errorcode ? this.createErrorMessageTag(__upload_msg_error + errorcode + __upload_msg_error_suffix) : this.createErrorMessageTag(__upload_msg_error + "PARTIAL" + __upload_msg_error_suffix),
        this.completeUpload(fileid)
    }
    ,
    FileUpload.prototype.createErrorMessageTag = function createErrorMessageTag(message) {
        return '<span style="color:red; font-weight:bold;">' + message + "</span>"
    }
    ,
    FileUpload.prototype.toggleRowView = function(fileid) {
        document.getElementById(fileid + "_checkbox").checked ? this.enableFile(fileid) : this.disableFile(fileid)
    }
    ,
    FileUpload.prototype.enableFile = function(fileid) {
        if (!(window.attachEvent && -1 == navigator.userAgent.indexOf("Opera")))
            document.getElementById("row_" + fileid).style.MozOpacity = 1,
            document.getElementById("row_" + fileid).style.opacity = 1;
        else
            for (var i = 0; i < document.getElementById("row_" + fileid).childNodes.length; i++)
                document.getElementById("row_" + fileid).childNodes[i].style.filter = ""
    }
    ,
    FileUpload.prototype.disableFile = function(fileid) {
        if (!(window.attachEvent && -1 == navigator.userAgent.indexOf("Opera")))
            document.getElementById("row_" + fileid).style.MozOpacity = .3,
            document.getElementById("row_" + fileid).style.opacity = .3;
        else
            for (var i = 0; i < document.getElementById("row_" + fileid).childNodes.length; i++)
                document.getElementById("row_" + fileid).childNodes[i].style.filter = "alpha(opacity=30)"
    }
    ,
    FileUpload.prototype.startUpload = function(fileid) {
        for (var i in html5_uploadStatuses[fileid] = true,
        document.forms)
            this.toggleButtons(true)
    }
    ,
    FileUpload.prototype.completeUpload = function(fileid) {
        var uploadingFlag = html5_uploadStatuses[fileid] = false;
        for (var i in html5_uploadStatuses)
            uploadingFlag |= html5_uploadStatuses[i];
        uploadingFlag || this.toggleButtons(false)
    }
    ,
    FileUpload.prototype.toggleButtons = function(disable) {
        for (var i = 0; i < document.forms.length; i++) {
            var form = document.forms[i]
              , inputSubArray = jQuery(form).find("input[type='submit']").toArray();
            jQuery.each(inputSubArray, function(k, node) {
                node.disabled = disable
            });
            var inputBtnArray = jQuery(form).find("input[type='button']").toArray();
            jQuery.each(inputBtnArray, function(k, node) {
                try {
                    void 0 != node.onclick && node.onclick.toString().indexOf("submit") > 0 && (node.disabled = disable)
                } catch (e) {}
            })
        }
        grn.component.button && jQuery(".button1_main_grn, .button_submit_grn").each(function() {
            disable ? grn.component.button(this).disable() : grn.component.button(this).enable()
        })
    }
}
jQuery.each(jQuery(".file_input_div").toArray(), function() {
    jQuery(this).mouseover(function() {
        jQuery(this).addClass("over")
    }),
    jQuery(this).mouseout(function() {
        jQuery(this).removeClass("over")
    })
});
