grn.base.namespace("grn.html.download"),
function($) {
    var G = grn.html.download;
    G.old_onkeydown_handlar = null,
    G.showBundleDownloadWindow = function() {
        G.old_onkeydown_handlar = document.onkeydown,
        document.onkeydown = function(e) {
            return e = void 0 == e ? event : e,
            27 == GRN_Event.keyCode(e) && G.closeBundleDownloadWindow()
        }
        ,
        G.coverBackground(true);
        var bundleDownForm, bundleDownFormArray = $("#bundle_download_form :input[type='checkbox']").toArray();
        $.each(bundleDownFormArray, function(key, value) {
            $(value).prop("checked", false)
        }),
        $("#assumed_progress").css("width", 0),
        $("#inner_progress").css("width", 0),
        grn.component.button("#bundleDownloadButton").disable(),
        $("#bundleDownloadSize").html("0B"),
        $("#bundleSizePercentage").html("0%");
        var elm = $("#bundleDownloadWindow");
        window.attachEvent && -1 == navigator.userAgent.indexOf("Opera") ? (elm.css("width", .7 * G.getDisplayWidth() + "px"),
        elm.css("height", .65 * G.getDisplayHeight() + document.getElementById("bundleDownloadTitle").clientHeight + "px")) : (navigator.userAgent.indexOf("AppleWebKit/") > -1 || navigator.userAgent.indexOf("Gecko") > -1 && -1 == navigator.userAgent.indexOf("KHTML")) && (elm.css("width", .7 * G.getDisplayWidth() + "px"),
        elm.css("height", .65 * G.getDisplayHeight() + 32 + "px"),
        $("#bundle_download_form").css("height", .65 * G.getDisplayHeight() - 32 + "px")),
        $(elm).show()
    }
    ,
    G.closeBundleDownloadWindow = function() {
        document.onkeydown = G.old_onkeydown_handlar,
        G.coverBackground(false),
        $("#bundleDownloadWindow").hide()
    }
    ,
    G.coverBackground = function(visible) {
        var backgroundElement = $("#background");
        if (visible) {
            var body = 0 == document.documentElement.clientHeight ? document.body : document.documentElement
              , backgroundEW = Math.max(body.clientWidth, body.scrollWidth) + "px"
              , backgroundEH = Math.max(body.clientHeight, body.scrollHeight) + "px";
            backgroundElement.css({
                width: backgroundEW,
                height: backgroundEH
            }),
            backgroundElement.attr({
                class: "cover"
            }),
            backgroundElement.show()
        } else
            backgroundElement.attr({
                class: ""
            }),
            backgroundElement.css({
                width: 0,
                height: 0
            }),
            backgroundElement.hide()
    }
    ,
    G.pre_submit = function() {
        var form = $("#bundle_download_form")
          , url = form.attr("action").split("?");
        if (url.length >= 2) {
            form.attr("action", url[0] + "/-/" + G.filename + ".zip");
            var new_url = form.attr("action") + url[1];
            form.attr("action", new_url)
        }
    }
    ,
    G.calcTotalFileSize = function(checkbox) {
        var total_size = 0
          , is_exist_select = false
          , bundle_download_form = $("#bundle_download_form :input[type='checkbox']")
          , download_Button = grn.component.button("#bundleDownloadButton")
          , hundleDownloadFormArray = bundle_download_form.toArray();
        $.each(hundleDownloadFormArray, function(key, value) {
            $(value).prop("checked") && (total_size += parseInt($(value).attr("size")),
            is_exist_select = true)
        }),
        $(checkbox).prop("checked") && (document.getElementById("assumed_progress").style.width = 0),
        G.isDownloadableSize(total_size) && is_exist_select ? download_Button.enable() : download_Button.disable();
        var pct = G.getSizePercentage(total_size);
        $("#bundleSizePercentage").html(parseInt(pct) + "%"),
        parseInt(pct) > 100 ? ($("#inner_progress").css("background", "red"),
        $("#inner_progress").css("width", "100%")) : ($("#inner_progress").css("background", "lightgreen"),
        $("#inner_progress").css("width", pct + "%")),
        $("#bundleDownloadSize").html(G.getSizeString(total_size))
    }
    ,
    G.getSizeString = function(size) {
        var suffix = new Array("B","KB","MB","GB","TB","PB","EB","ZB","YB"), idx = parseInt((String(size).length - 1) / 3), ret;
        return ((size / Math.pow(1024, idx)).toFixed(1) + suffix[idx]).replace(".0", "")
    }
    ,
    G.getSizePercentage = function(size) {
        return 0 == G.max_download_size ? 0 : parseInt(Math.ceil(size / G.max_download_size * 100))
    }
    ,
    G.isDownloadableSize = function(size) {
        return 0 == G.max_download_size || parseInt(size) <= G.max_download_size
    }
    ,
    G.getDisplayWidth = function() {
        return document.all ? document.body.clientWidth : document.layers || document.getElementById ? window.innerWidth : 0
    }
    ,
    G.getDisplayHeight = function() {
        return document.all ? document.body.clientHeight : document.layers || document.getElementById ? window.innerHeight : 0
    }
    ,
    G.getWindowWidth = function() {
        return window.innerWidth ? window.innerWidth : document.documentElement && document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body && document.body.clientWidth ? document.body.clientWidth : 0
    }
    ,
    G.getWindowHeight = function() {
        return window.innerHeight ? window.innerHeight : document.documentElement && document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body && document.body.clientHeight ? document.body.clientHeight : 0
    }
    ,
    G.setSelectableView = function(fileid) {
        var downloadFileElement = $("#download" + fileid);
        if (!downloadFileElement.prop("checked")) {
            var size = downloadFileElement.attr("size")
              , assumed_pct = G.getSizePercentage(size)
              , current_pct = document.getElementById("inner_progress").style.width.replace("%", "");
            isNaN(current_pct) && (current_pct = 0),
            parseInt(current_pct) + parseInt(assumed_pct) > 100 ? ($("#assumed_progress").css("background", "#FF8C00"),
            $("#assumed_progress").css("width", 100 - current_pct + "%")) : ($("#assumed_progress").css("background", "Yellow"),
            $("#assumed_progress").css("width", assumed_pct + "%"))
        }
    }
    ,
    G.setUnselectableView = function(fileid) {
        $("#assumed_progress").css("width", 0)
    }
    ,
    G.check_download_files = function() {
        var checked = false
          , e = $("#bundle_download_form :input[name='id[]']")
          , eArray = e.toArray();
        $.each(eArray, function(key, value) {
            "id[]" != $(value).attr("name") || $(value).prop("checked") || (checked = true)
        }),
        $.each(eArray, function(key, value) {
            "id[]" == $(value).attr("name") && $(value).prop("checked", checked)
        }),
        e.length > 0 && G.calcTotalFileSize(e[0])
    }
}(jQuery);
