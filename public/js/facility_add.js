!function($) {
    "use strict";
    function get_full_list_name(list_name) {
        return "selectlist_" + list_name
    }
    if (!grn.base.isNamespaceDefined("grn.component.facility_add")) {
        grn.base.namespace("grn.component.facility_add");
        var facility_add = grn.component.facility_add
          , instances = {}
          , default_options = {
            facilityListOptions: {},
            candidateListOptions: {},
            categorySelectUrl: "",
            searchUrl: "",
            idSearchBox: "",
            initUsingPurpose: "",
            csrfTicket: ""
        };
        facility_add.FacilityAdd = function(name, form_name, page_name, facility_list_name, candidate_list_Name, options) {
            (options = $.extend({}, default_options, options)).facilityListOptions.isCalendar = options.isCalendar,
            options.candidateListOptions.isCalendar = options.isCalendar,
            options.candidateListOptions.isFacilityCandidate = true,
            this.name = name,
            this.facilityListName = facility_list_name,
            this.fullFacilityListName = get_full_list_name(this.facilityListName),
            this.facilityList = new grn.component.member_select_list.MemberSelectList(this.facilityListName,options.facilityListOptions),
            this.candidateListName = candidate_list_Name,
            this.fullCandidateListName = get_full_list_name(this.candidateListName),
            this.candidateList = new grn.component.member_select_list.MemberSelectList(this.candidateListName,options.candidateListOptions),
            this.formName = form_name,
            this.pageName = page_name,
            this.categoryFacilityURL = options.categorySelectUrl,
            this.searchFacilitiesByKeywordURL = options.searchUrl,
            this.idSearchBox = options.idSearchBox,
            this.initUsingPurpose = options.initUsingPurpose,
            this.csrfTicket = options.csrfTicket,
            this.dropdownInstance = "group-select",
            $(function() {
                facility_add.init(name)
            }),
            instances[name] = this
        }
        ,
        facility_add.get_instance = function(name) {
            return instances[name]
        }
        ,
        facility_add.FacilityAdd.prototype = {
            modifyFacilityDOM: function(facility) {
                var return_candidate = $(facility)
                  , new_id = return_candidate.attr("id").replace(this.fullCandidateListName, this.fullFacilityListName);
                return return_candidate.clone(false).attr({
                    id: new_id,
                    class: this.fullFacilityListName + " selectlist_selected_grn"
                })[0]
            },
            addFacilities: function() {
                this.facilityList.spinnerOn();
                for (var selected_candidate_list = this.candidateList.getSelectedList().toArray(), selected_list_values = $(this.facilityList.getValues()).toArray(), selected_list = this.facilityList.getList(), length = selected_candidate_list.length, list_to_add = [], i = 0; i < length; i++) {
                    var facility = selected_candidate_list[i]
                      , value = $(facility).attr("data-value")
                      , index = selected_list_values.indexOf(value);
                    index < 0 ? list_to_add.push(this.modifyFacilityDOM(facility)) : $(selected_list.get(index)).addClass(this.facilityList.selectedClassName)
                }
                this._appendFacilities(list_to_add),
                this.onOffExtendedItems(),
                this.onOffNetmeetingFacilityItems(),
                this.onOffUsingPurpose()
            },
            removeSelectedFacilities: function() {
                this.facilityList.removeSelectedFacilities(),
                this.onOffExtendedItems(),
                this.onOffNetmeetingFacilityItems(),
                this.onOffUsingPurpose()
            },
            _appendFacilities: function(list_to_add) {
                $("#ul_" + this.fullFacilityListName).append(list_to_add),
                this.facilityList.spinnerOff()
            },
            changeCategory: function(facility_group_id) {
                GRN_DropdownMenu.getInstance(this.dropdownInstance).removeSearchLabel();
                var post_data = {
                    fagid: facility_group_id,
                    page_name: this.pageName,
                    is_ancestors_path_string: true
                };
                this.candidateList.spinnerOn();
                var ajaxRequest = new grn.component.ajax.request({
                    url: this.categoryFacilityURL,
                    method: "post",
                    dataType: "json",
                    data: post_data
                });
                0 === facility_group_id ? $("#facility-group").show() : $("#facility-group").hide(),
                ajaxRequest.send().done(function(json_obj) {
                    this.candidateList.addFacilities(json_obj, true),
                    this.candidateList.viewUnSelectAllLink(),
                    this.candidateList.spinnerOff(),
                    this.candidateList.updateAncestorsPath()
                }
                .bind(this))
            },
            search: function() {
                var keyword = $("#keyword_" + this.idSearchBox).val(), search_box;
                grn.component.search_box.get_instance(this.idSearchBox).firstFlag && (keyword = "");
                var post_data = {
                    search_text: keyword,
                    page_name: this.pageName,
                    csrf_ticket: this.csrfTicket,
                    is_ancestors_path_string: true
                }
                  , ajaxRequest = new grn.component.ajax.request({
                    url: this.searchFacilitiesByKeywordURL,
                    method: "post",
                    dataType: "json",
                    data: post_data
                });
                $("#facility-group").show(),
                this.candidateList.spinnerOn(),
                ajaxRequest.send().done(function(json_obj) {
                    var search_label = grn.component.i18n.cbMsg("grn.grn", "GRN_GRN-807");
                    GRN_DropdownMenu.getInstance(this.dropdownInstance).prependSearchLabel(search_label),
                    this.candidateList.addFacilities(json_obj, true),
                    this.candidateList.viewUnSelectAllLink(),
                    this.candidateList.spinnerOff(),
                    this.candidateList.updateAncestorsPath()
                }
                .bind(this))
            },
            prepareSubmit: function() {
                var form = document.forms[this.formName]
                  , src = this.facilityList.getList().toArray()
                  , selected_users = form.elements["selected_users_" + this.facilityListName]
                  , selected_users_value = [];
                src.forEach(function(option) {
                    var value = $(option).attr("data-value");
                    value && value.length > 0 && isFinite(value) && selected_users_value.push(value)
                }),
                selected_users.value = selected_users_value.join(":")
            },
            onOffExtendedItems: function() {
                var length;
                this.facilityList.getList().length > 0 ? $("#extended_items").show() : $("#extended_items").hide()
            },
            onOffNetmeetingFacilityItems: function() {
                "function" === typeof netmeeting_facility_items_OnOff && netmeeting_facility_items_OnOff(this.facilityList.getValues())
            },
            onOffUsingPurpose: function() {
                for (var selectedItems = this.facilityList.getList(), usingPurposeElement = $("#using_purpose_element"), usingPurpose = $("#using_purpose"), numOfApprovalFacilities = 0, minHeightUsingPurpose = 90, i = 0, warning; i < selectedItems.length; i++)
                    if ("1" === $(selectedItems[i]).attr("data-approval")) {
                        numOfApprovalFacilities += 1,
                        this.initUsingPurpose && usingPurpose.is(":hidden") && usingPurpose.val(this.initUsingPurpose),
                        usingPurpose.is(":hidden") && ("undefined" !== typeof grn.page.schedule.add.defaultHeightPurpose && "" !== usingPurpose.val() ? usingPurpose.css({
                            height: grn.page.schedule.add.defaultHeightPurpose
                        }) : usingPurpose.css({
                            height: 90
                        })),
                        usingPurposeElement.show();
                        break
                    }
                0 === numOfApprovalFacilities && (usingPurpose.val(""),
                usingPurposeElement.hide(),
                jQuery("#using_purpose_error").hide())
            },
            getFacilities: function(is_candidate) {
                var facilities_list_instance, $facility_dom_element = (is_candidate ? this.candidateList : this.facilityList).$ul_list.find("li"), facility_list = [];
                return $facility_dom_element.each(function() {
                    var facility = {};
                    facility.id = $(this).attr("data-value"),
                    facility.approval = $(this).attr("data-approval"),
                    facility.ancestors = $(this).attr("data-ancestors"),
                    facility.code = $(this).attr("data-code"),
                    facility.name = $(this).attr("data-name"),
                    facility_list.push(facility)
                }),
                facility_list
            }
        },
        facility_add.init = function(name) {
            var instance = facility_add.get_instance(name);
            $("#btn_add_" + instance.candidateListName).on("click", function() {
                instance.addFacilities()
            }),
            $("#btn_rmv_" + instance.candidateListName).on("click", function() {
                instance.removeSelectedFacilities()
            }),
            $("#" + instance.facilityListName + "_order_up").on("click", function() {
                instance.facilityList.orderUp()
            }),
            $("#" + instance.facilityListName + "_order_down").on("click", function() {
                instance.facilityList.orderDown()
            }),
            $("#keyword_" + instance.idSearchBox).on("keypress", function(event) {
                return 13 === event.keyCode && instance.search(),
                13 !== event.keyCode
            }),
            $("#searchbox-submit-" + instance.idSearchBox).on("click", function() {
                instance.search()
            }),
            instance.onOffNetmeetingFacilityItems()
        }
    }
}(jQuery);
