//DOMツリーが構築し終えたときに発生するイベント
function upload_init() {
    grn.browser.isSupportHTML5 ? (jQuery("#html5_content").css("display", ""),
    jQuery("#not_support_html5_content").remove()) : (jQuery("#not_support_html5_content").css("display", ""),
    jQuery("#html5_content").remove(),
    grn.component.button("#fileupload_button_upload").disable(),
    grn.component.button("#btnUpdateAttachment").disable())
}
function getUploadedTempFiles() {
    var ajaxRequest;
    new grn.component.ajax.request({
        url: __upload_files_url,
        data: "upload_ticket=" + __upload_ticket,
        method: "post",
        grnRedirectOnLoginError: false
    }).send().done(function(data, textStatus, request) {
        var uploadTable = document.getElementById("upload_table")
          , tableHTML = "<table><tbody><tr><td></td></tr></tbody></table>";
        void 0 != request.responseText && request.responseText.length > 0 && (tableHTML = "<table>" + request.responseText + "</table>");
        var divElem = document.createElement("div");
        divElem.innerHTML = tableHTML;
        var tableElem = divElem.firstChild, childElem;
        if (jQuery(uploadTable).length > 0) {
            for (; childElem = uploadTable.firstChild; )
                uploadTable.removeChild(childElem);
            for (; childElem = tableElem.firstChild; )
                uploadTable.appendChild(childElem)
        }
    })
}
function _getSizeString(size) {
    var suffix = new Array("B","KB","MB","GB","TB","PB","EB","ZB","YB"), idx = parseInt((String(size).length - 1) / 3), ret;
    return ((size / Math.pow(1024, idx)).toFixed(1) + suffix[idx]).replace(".0", "")
}
function onChangeCheckBox(fileid) {
    jQuery("#" + fileid + "_checkbox").is(":checked") ? enableFile(fileid) : disableFile(fileid)
}
function enableFile(fileid) {
    jQuery("#" + fileid + "_filename").css("color", "black")
}
function disableFile(fileid) {
    jQuery("#" + fileid + "_filename").css("color", "gray")
}
function confirmIfExistFailedFile() {
    return !window.isExistFailedFile || confirm(__upload_msg_confirm1 + "\n" + __upload_msg_confirm2)
}
function handleDrag(evt) {
    if (!(evt.dataTransfer.types[0].toString().indexOf("text") >= 0)) {
        evt.preventDefault(),
        evt.stopPropagation();
        var type = evt.type;
        if ("drop" == type)
            return jQuery("div.drop").each(function(index, e) {
                jQuery(e).removeClass("over")
            }),
            jQuery("div.file_input_div").each(function(index, e) {
                jQuery(e).removeClass("drag_in")
            }),
            isCheck = false,
            valid = count = 0,
            void clearTimeout(t);
        "dragover" == type && (jQuery("div.drop").each(function(index, e) {
            jQuery(e).addClass("over")
        }),
        jQuery("div.file_input_div").each(function(index, e) {
            jQuery(e).addClass("drag_in")
        }),
        evt.dataTransfer.dropEffect = "none"),
        count++,
        isCheck || (checkDragging(),
        isCheck = true)
    }
}
function checkDragging() {
    valid < count ? (valid = count,
    t = setTimeout("checkDragging()", 400)) : (jQuery("div.drop").each(function(index, e) {
        jQuery(e).removeClass("over")
    }),
    jQuery("div.file_input_div").each(function(index, e) {
        jQuery(e).removeClass("drag_in")
    }),
    isCheck = false,
    valid = count = 0,
    clearTimeout(t))
}
function traverseFiles(files, id_input_upload) {
    if ("undefined" !== typeof files)
        for (var i = 0, l = files.length; i < l; i++)
            fileUpload = new FileUpload(files[i],id_input_upload)
}
if (jQuery(function() {
    upload_init(),
    getUploadedTempFiles(),
    grn.browser.isSupportHTML5 && (jQuery(".file_html5").each(function(index, e) {
        var id = e.id.split("file_upload_")[1];
        new DragDropFile(id); 
    }),
    jQuery(".file_input_div").each(function(index, f) {
        f.addEventListener("mouseover", function() {
            jQuery(f).addClass("over")
        }, false),
        f.addEventListener("mouseout", function() {
            jQuery(f).removeClass("over")
        }, false)
    }))
}),
true) {
    var isCheck = false, count = 0, valid = 0, t, DragDropFile = function(id) {
        this.filesUpload = document.getElementById("file_upload_" + id),
        this.dropArea = document.getElementById("drop_" + id),
        this.id = id,
        this.init()
    };
    DragDropFile.prototype.init = function() {
        window.addEventListener("dragover", handleDrag, false),
        window.addEventListener("drop", handleDrag, false);
        var self = this;
        this.filesUpload.addEventListener("change", function() {
            traverseFiles(this.files, self.id),
            self.filesUpload.value = ""
        }, false),
        this.dropArea.addEventListener("dragleave", function(evt) {
            evt.preventDefault(),
            evt.stopPropagation()
        }, false),
        this.dropArea.addEventListener("dragenter", function(evt) {
            evt.preventDefault(),
            evt.stopPropagation()
        }, false),
        this.dropArea.addEventListener("dragover", function(evt) {
            count++,
            evt.dataTransfer.dropEffect = "copy",
            evt.preventDefault(),
            evt.stopPropagation()
        }, false),
        this.dropArea.addEventListener("drop", function(evt) {
            jQuery("div.drop").each(function(index, e) {
                jQuery(e).removeClass("over")
            }),
            jQuery("div.file_input_div").each(function(index, e) {
                jQuery(e).removeClass("drag_in")
            }),
            traverseFiles(evt.dataTransfer.files, self.id),
            evt.preventDefault(),
            evt.stopPropagation()
        }, false)
    }
    ,
    html5_uploadStatuses = new Object;
    var FileUpload = function(file, id_input_upload) {
        this.file = file,
        this.xhr = null,
        this.responseText = "",
        this.fid = null,
        this.started = false,
        this.id_input_upload = id_input_upload,
        this.init()
    };
    FileUpload.prototype.init = function() {
        this.triggerUpload()
    }
    ,
    FileUpload.prototype.cancelUpload = function(e, fileid) {
        e.preventDefault(),
        this.xhr && this.endProcess(0),
        jQuery("#row_" + fileid).remove(),
        this.completeUpload(fileid)
    }
    ,
    FileUpload.prototype.triggerUpload = function() {
        this.sendDatas()
    }
    ,
    FileUpload.prototype.sendDatas = function() {
        var self = this;
        if (this.xhr = new XMLHttpRequest,
        this.fid = this.randomString(),
        this.onUploadOpen(this.fid, this.file.name, this.file.size),
        0 == this.file.size)
            return this.onUploadError(this.fid, "ZEROBYTE_FILE"),
            void (this.xhr = null);
        this.xhr && (this.xhr.upload.addEventListener("loadstart", function() {
            setTimeout(jQuery.proxy(self.checkRequest, self), 6e5)
        }, false),
        this.xhr.upload.addEventListener("progress", function(e) {
            self.updateProgress(e, self.fid)
        }, false),
        this.xhr.upload.addEventListener("load", function(e) {
            self.endUpload(e, self.fid, self)
        }, false),
        this.xhr.upload.addEventListener("abort", function() {
            self.closeXhr(self)
        }, false),
        this.xhr.upload.addEventListener("error", function(e) {
            self.errorUpload(e)
        }, false),
        this.xhr.ontimeout = function() {
            alert("Request timed out.")
        }
        ,
        this.xhr.onreadystatechange = function() {
            if (this.readyState,
            4 == this.readyState) {
                var status = this.status;
                200 == status && self.endProcess(1),
                404 != status && 500 != status || self.endProcess(0)
            }
        }
        ,
        this.processUpload(this.fid))
    }
    ,
    FileUpload.prototype.processUpload = function(fid) {
        var data = new FormData;
        data.append("file", this.file),
        this.xhr.open("POST", __upload_url, true),
        this.xhr.setRequestHeader("X-File-Name", escape(this.file.name)),
        this.xhr.setRequestHeader("X-File-Type", this.file.type),
        this.xhr.setRequestHeader("X-File-Size", this.file.size),
        this.xhr.setRequestHeader("X-File-Id", fid),
        this.xhr.setRequestHeader("Authorization", 'Bearer ' + $('meta[name="api_token"]').attr('content')),
        this.xhr.setRequestHeader("X-File-Upload-Ticket", __upload_ticket),
        console.log(data);
        this.xhr.send(data)
    }
    ,
    FileUpload.prototype.updateProgress = function(e, fid) {
        this.started = true,
        e.lengthComputable && (jQuery("#" + fid + "_percent").html((e.loaded / e.total * 100).toFixed() + "%"),
        jQuery("#" + fid + "_progress").css("width", e.loaded / e.total * 100 + "%"))
    }
    ,
    FileUpload.prototype.endUpload = function() {
        this.started = true
    }
    ,
    FileUpload.prototype.endProcess = function() {
        if (this.xhr) {
            if (this.xhr.responseText)
                var response = JSON.parse(this.xhr.responseText);
                if ('COMPLETE' == response.success) {
                    jQuery("#" + this.fid + "_progressItem").remove(),
                    jQuery("#" + this.fid + "_percent").remove(),
                    jQuery("#" + this.fid + "_message").remove();
                    var self = this
                      , $checkbox = $('<input type="checkbox" class="upload_checkbox js_upload_file" checked />');
                    $checkbox.attr("id", this.fid + "_checkbox"),
                    $checkbox.attr("name", "upload_fileids[]"),
                    $checkbox.attr("value", response.id),
                    $checkbox.attr("checked", true),
                    $checkbox.attr("data-id", ""),
                    $checkbox.attr("data-name", self.file.name),
                    $checkbox.attr("data-content-type", self.file.type),
                    $checkbox.attr("data-size", self.file.size.toString()),
                    jQuery("#" + this.fid + "_check").html($checkbox).on("click", function() {
                        self.onChangeCheckBox(self.fid)
                    }),
                    jQuery("#" + this.fid + "_checkbox").prop("checked", true),
                    this.completeUpload(this.fid)
                } else
                    this.onUploadError(this.fid, this.xhr.responseText);
            this.xhr.abort()
        }
    }
    ,
    FileUpload.prototype.errorUpload = function(e) {}
    ,
    FileUpload.prototype.closeXhr = function() {
        this.xhr = null
    }
    ,
    FileUpload.prototype.checkRequest = function() {
        false == this.started && this.xhr && (this.onUploadError(this.fid, "REQUEST_TIME_OUT"),
        this.xhr.abort())
    }
    ,
    FileUpload.prototype.onUploadOpen = function(fileid, filename, size) {
        var self = this;
        filename = filename.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        var row = document.getElementById("upload_table").insertRow(-1), cell1;
        row.id = "row_" + fileid,
        row.insertCell(-1).id = fileid + "_check";
        var cell2 = row.insertCell(-1), cell3;
        cell2.id = fileid + "_filename",
        cell2.innerHTML = filename + " (" + this._getSizeString(size) + ")",
        row.insertCell(-1).id = fileid + "_percent";
        var cell4 = row.insertCell(-1);
        cell4.id = fileid + "_progressItem",
        cell4.innerHTML = this._getProgressItem(fileid);
        var cell5 = row.insertCell(-1);
        cell5.id = fileid + "_message",
        cell5.style.whiteSpace = "nowrap",
        cell5.innerHTML = '<a href="#">' + __upload_msg_cancel + "</a>",
        cell5.addEventListener("click", function(e) {
            self.cancelUpload(e, fileid)
        }, false),
        this.startUpload(fileid)
    }
    ,
    FileUpload.prototype.onUploadComplete = function(fileid) {}
    ,
    FileUpload.prototype.randomString = function() {
        for (var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz", string_length = 35, randomstring = "", i = 0; i < 35; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1)
        }
        return this.id_input_upload && (randomstring = this.id_input_upload + "-" + randomstring),
        randomstring
    }
    ,
    FileUpload.prototype._getProgressItem = function(fileid) {
        return '<div style="border: 1px solid rgb(176, 176, 176); padding: 0; width: 300px; height: 10px;" id="div_' + fileid + '"><div id="' + fileid + '_progress" style="width:0;background-color:lightgreen;height:100%;margin:0;"></div></div>'
    }
    ,
    FileUpload.prototype._getSizeString = function(size) {
        var suffix = new Array("B","KB","MB","GB","TB","PB","EB","ZB","YB"), idx = parseInt((String(size).length - 1) / 3), ret;
        return ((size / Math.pow(1024, idx)).toFixed(1) + suffix[idx]).replace(".0", "")
    }
    ,
    FileUpload.prototype.onUploadError = function(fileid, errorcode) {
        isExistFailedFile = true,
        jQuery("#" + fileid + "_progressItem").html(this.createErrorMessageTag(this._createErrorMessageFromErrorCode(errorcode))),
        jQuery("#" + fileid + "_percent").remove(),
        jQuery("#" + fileid + "_message").remove(),
        this.completeUpload(fileid)
    }
    ,
    FileUpload.prototype._createErrorMessageFromErrorCode = function(errorcode) {
        var message = "";
        return message = "ZEROBYTE_FILE" == errorcode ? __upload_msg_zerobyte_file : -1 !== ["INVALID_TICKET", "OVER_INI_SIZE", "OVER_FORM_SIZE", "NO_FILE"].indexOf(errorcode) ? __upload_msg_error + errorcode + __upload_msg_error_suffix : __upload_msg_error + "PARTIAL" + __upload_msg_error_suffix
    }
    ,
    FileUpload.prototype.createErrorMessageTag = function createErrorMessageTag(message) {
        return '<span style="color:red; font-weight:bold;">' + message + "</span>"
    }
    ,
    FileUpload.prototype.onChangeCheckBox = function(fileid) {
        jQuery("#" + fileid + "_checkbox").is(":checked") ? this.enableFile(fileid) : this.disableFile(fileid)
    }
    ,
    FileUpload.prototype.enableFile = function(fileid) {
        jQuery("#" + fileid + "_filename").css("color", "black")
    }
    ,
    FileUpload.prototype.disableFile = function(fileid) {
        jQuery("#" + fileid + "_filename").css("color", "gray")
    }
    ,
    FileUpload.prototype.startUpload = function(fileid) {
        for (var i in html5_uploadStatuses[fileid] = true,
        document.forms)
            this.toggleButtons(true)
    }
    ,
    FileUpload.prototype.completeUpload = function(fileid) {
        var uploadingFlag = html5_uploadStatuses[fileid] = false;
        for (var i in html5_uploadStatuses)
            uploadingFlag |= html5_uploadStatuses[i];
        uploadingFlag || this.toggleButtons(false)
    }
    ,
    FileUpload.prototype.toggleButtons = function(disable) {
        for (var i = 0; i < document.forms.length; i++) {
            var form = document.forms[i];
            jQuery(form).find(":submit").each(function(index, node) {
                node.disabled = disable
            }),
            jQuery(form).find(":button").each(function(index, node) {
                try {
                    void 0 != node.onclick && node.onclick.toString().indexOf("submit") > 0 && (node.disabled = disable)
                } catch (e) {}
            })
        }
        jQuery("form span.aButtonSubmit-grn").each(function(index, node) {
            if (node_a = node.down("a"),
            disable) {
                var link_onclick = node_a.getAttribute("onclick");
                "" != link_onclick && (node_a.setAttribute("onclick_bk", link_onclick),
                node_a.setAttribute("onclick", "")),
                jQuery(node).addClass("button_standard_disable_grn"),
                jQuery(node).removeClass("aButtonStandard-grn")
            } else {
                var link_onclick_bk = node_a.getAttribute("onclick_bk");
                "" != link_onclick_bk && node_a.setAttribute("onclick", link_onclick_bk),
                jQuery(node).addClass("aButtonStandard-grn"),
                jQuery(node).removeClass("button_standard_disable_grn")
            }
        }),
        grn.component.button && jQuery(".button1_main_grn, .button_submit_grn").each(function() {
            disable ? grn.component.button(this).disable() : grn.component.button(this).enable()
        })
    }
}
