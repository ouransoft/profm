(window.webpackJsonp = window.webpackJsonp || []).push([[22], {
    220: function(t, e, n) {
        "use strict";
        n.r(e);
        n(2),
        n(7),
        n(6),
        n(38),
        n(3),
        n(17),
        n(10),
        n(9),
        n(0),
        n(34),
        n(8),
        n(20),
        n(1),
        n(5),
        n(4),
        n(57);
        var r = n(15)
          , i = n(102)
          , s = (n(78),
        n(32),
        n(28),
        n(52),
        n(13))
          , o = n(11)
          , a = n(19);
        function c(t) {
            "@babel/helpers - typeof";
            return (c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
                return typeof t
            }
            : function(t) {
                return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
            }
            )(t)
        }
        function u() {
            var t = function(t, e) {
                e || (e = t.slice(0));
                return Object.freeze(Object.defineProperties(t, {
                    raw: {
                        value: Object.freeze(e)
                    }
                }))
            }(['\n        <table  border="0" cellpadding="0" cellspacing="0" >\n            <tbody>\n            <tr class="ygtvrow">\n                <td class="ygtvcell ygtvln">\n                    <a href="#" class="ygtvspacer"></a>\n                </td>\n                <td class="ygtvcell ygtvhtml ygtvcontent">\n                    <div class="tree-node">\n                        <a href="', '">', "</a>\n                    </div>\n                </td>\n            </tr>\n            </tbody>\n        </table>\n        "]);
            return u = function() {
                return t
            }
            ,
            t
        }
        function l(t, e) {
            return function(t) {
                if (Array.isArray(t))
                    return t
            }(t) || function(t, e) {
                if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(t)))
                    return;
                var n = []
                  , r = !0
                  , i = !1
                  , s = void 0;
                try {
                    for (var o, a = t[Symbol.iterator](); !(r = (o = a.next()).done) && (n.push(o.value),
                    !e || n.length !== e); r = !0)
                        ;
                } catch (t) {
                    i = !0,
                    s = t
                } finally {
                    try {
                        r || null == a.return || a.return()
                    } finally {
                        if (i)
                            throw s
                    }
                }
                return n
            }(t, e) || function(t, e) {
                if (!t)
                    return;
                if ("string" == typeof t)
                    return h(t, e);
                var n = Object.prototype.toString.call(t).slice(8, -1);
                "Object" === n && t.constructor && (n = t.constructor.name);
                if ("Map" === n || "Set" === n)
                    return Array.from(n);
                if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))
                    return h(t, e)
            }(t, e) || function() {
                throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
            }()
        }
        function h(t, e) {
            (null == e || e > t.length) && (e = t.length);
            for (var n = 0, r = new Array(e); n < e; n++)
                r[n] = t[n];
            return r
        }
        function f(t, e) {
            for (var n = 0; n < e.length; n++) {
                var r = e[n];
                r.enumerable = r.enumerable || !1,
                r.configurable = !0,
                "value"in r && (r.writable = !0),
                Object.defineProperty(t, r.key, r)
            }
        }
        function p(t, e, n) {
            return e && f(t.prototype, e),
            n && f(t, n),
            t
        }
        function d(t, e) {
            return (d = Object.setPrototypeOf || function(t, e) {
                return t.__proto__ = e,
                t
            }
            )(t, e)
        }
        function g(t) {
            return function() {
                var e, n = y(t);
                if (function() {
                    if ("undefined" == typeof Reflect || !Reflect.construct)
                        return !1;
                    if (Reflect.construct.sham)
                        return !1;
                    if ("function" == typeof Proxy)
                        return !0;
                    try {
                        return Date.prototype.toString.call(Reflect.construct(Date, [], function() {})),
                        !0
                    } catch (t) {
                        return !1
                    }
                }()) {
                    var r = y(this).constructor;
                    e = Reflect.construct(n, arguments, r)
                } else
                    e = n.apply(this, arguments);
                return function(t, e) {
                    if (e && ("object" === c(e) || "function" == typeof e))
                        return e;
                    return function(t) {
                        if (void 0 === t)
                            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                        return t
                    }(t)
                }(this, e)
            }
        }
        function y(t) {
            return (y = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) {
                return t.__proto__ || Object.getPrototypeOf(t)
            }
            )(t)
        }
        var b = jQuery
          , v = function(t) {
            !function(t, e) {
                if ("function" != typeof e && null !== e)
                    throw new TypeError("Super expression must either be null or a function");
                t.prototype = Object.create(e && e.prototype, {
                    constructor: {
                        value: t,
                        writable: !0,
                        configurable: !0
                    }
                }),
                e && d(t, e)
            }(n, s["a"]);
            var e = g(n);
            function n(t) {
                var r;
                !function(t, e) {
                    if (!(t instanceof e))
                        throw new TypeError("Cannot call a class as a function")
                }(this, n),
                r = e.call(this, t);
                var i = YAHOO.grn.orgTree;
                return r.treeObject = i.get_instance(r.settings.instanceName),
                r.$trashNode = null,
                r
            }
            return p(n, [{
                key: "_getDefaultSettings",
                value: function() {
                    return {
                        element: "#tree_view",
                        instanceName: "folder_tree",
                        rootId: 1,
                        css: {
                            rootElement: "js_root",
                            rootSelectedClass: "tree-select-current"
                        }
                    }
                }
            }]),
            p(n, [{
                key: "_cacheElements",
                value: function() {
                    this.$rootElement = this._findByClassName(this.settings.css.rootElement)
                }
            }, {
                key: "_bindEvents",
                value: function() {
                    var t = this;
                    this.treeObject.onSelectNode.subscribe(this._changeTreeElement.bind(this)),
                    this.treeObject.getTree().subscribe("expandComplete", function() {
                        t.trigger("expand")
                    }),
                    this.$rootElement.on("click", this._changeTreeRootElement.bind(this))
                }
            }, {
                key: "_changeTreeRootElement",
                value: function() {
                    this.treeObject.highlightNode(this.settings.rootId),
                    this.trigger("change", {
                        folderId: 0
                    })
                }
            }, {
                key: "selectNode",
                value: function(t) {
                    t && t !== this.settings.rootId ? this.treeObject.selectNode(t) : this.treeObject.highlightNode(this.settings.rootId)
                }
            }, {
                key: "_changeTreeElement",
                value: function(t, e) {
                    var n = l(e, 1)[0];
                    this.$rootElement.removeClass(this.settings.css.rootSelectedClass);
                    var r = !0 === n.isTrash
                      , i = n.nodeId;
                    this.trigger("change", {
                        folderId: i,
                        isTrash: r
                    })
                }
            }, {
                key: "showTrash",
                value: function(t) {
                    var e = this;
                    this.hideTrash();
                    var n = Object(a.c)("cabinet/garbage", {
                        hid: t
                    })
                      , r = o.a.getMessage("grn.cabinet", "garbage");
                    this.$trashNode = b(this.html(u(), n, r));
                    var i = [{
                        isTrash: !0,
                        nodeId: t
                    }];
                    this.$trashNode.click(function(t) {
                        t.preventDefault(),
                        e._changeTreeElement(t, i)
                    }),
                    this.$trashNode.appendTo("#" + this.settings.instanceName)
                }
            }, {
                key: "hideTrash",
                value: function() {
                    this.$trashNode && this.$trashNode.remove()
                }
            }]),
            n
        }()
          , m = (n(51),
        n(21))
          , _ = n(27)
          , w = n(30)
          , T = function(t, e) {
            var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null
              , r = {
                file_ids: t,
                folder_id: e,
                csrf_ticket: Object(_.b)()
            }
              , i = new m.a({
                grnUrl: "cabinet/ajax/command_delete_multi",
                method: "post",
                dataType: "json",
                data: r
            });
            return Object(w.c)(n, i),
            i.send()
        }
          , j = function(t) {
            var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null
              , n = {
                file_ids: t,
                csrf_ticket: Object(_.b)()
            }
              , r = new m.a({
                grnUrl: "cabinet/ajax/command_restore_from_trash",
                method: "post",
                dataType: "json",
                data: n
            });
            return Object(w.c)(e, r),
            r.send()
        }
          , $ = n(48);
        function O(t) {
            "@babel/helpers - typeof";
            return (O = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
                return typeof t
            }
            : function(t) {
                return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
            }
            )(t)
        }
        function P(t, e, n, r, i, s, o) {
            try {
                var a = t[s](o)
                  , c = a.value
            } catch (t) {
                return void n(t)
            }
            a.done ? e(c) : Promise.resolve(c).then(r, i)
        }
        function k(t) {
            return function() {
                var e = this
                  , n = arguments;
                return new Promise(function(r, i) {
                    var s = t.apply(e, n);
                    function o(t) {
                        P(s, r, i, o, a, "next", t)
                    }
                    function a(t) {
                        P(s, r, i, o, a, "throw", t)
                    }
                    o(void 0)
                }
                )
            }
        }
        function S(t, e) {
            for (var n = 0; n < e.length; n++) {
                var r = e[n];
                r.enumerable = r.enumerable || !1,
                r.configurable = !0,
                "value"in r && (r.writable = !0),
                Object.defineProperty(t, r.key, r)
            }
        }
        function x(t, e, n) {
            return e && S(t.prototype, e),
            n && S(t, n),
            t
        }
        function R(t, e) {
            return (R = Object.setPrototypeOf || function(t, e) {
                return t.__proto__ = e,
                t
            }
            )(t, e)
        }
        function L(t) {
            return function() {
                var e, n = E(t);
                if (function() {
                    if ("undefined" == typeof Reflect || !Reflect.construct)
                        return !1;
                    if (Reflect.construct.sham)
                        return !1;
                    if ("function" == typeof Proxy)
                        return !0;
                    try {
                        return Date.prototype.toString.call(Reflect.construct(Date, [], function() {})),
                        !0
                    } catch (t) {
                        return !1
                    }
                }()) {
                    var r = E(this).constructor;
                    e = Reflect.construct(n, arguments, r)
                } else
                    e = n.apply(this, arguments);
                return function(t, e) {
                    if (e && ("object" === O(e) || "function" == typeof e))
                        return e;
                    return I(t)
                }(this, e)
            }
        }
        function I(t) {
            if (void 0 === t)
                throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t
        }
        function C(t, e, n) {
            return (C = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function(t, e, n) {
                var r = function(t, e) {
                    for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = E(t)); )
                        ;
                    return t
                }(t, e);
                if (r) {
                    var i = Object.getOwnPropertyDescriptor(r, e);
                    return i.get ? i.get.call(n) : i.value
                }
            }
            )(t, e, n || t)
        }
        function E(t) {
            return (E = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) {
                return t.__proto__ || Object.getPrototypeOf(t)
            }
            )(t)
        }
        var N = jQuery
          , D = null
          , A = function(t) {
            !function(t, e) {
                if ("function" != typeof e && null !== e)
                    throw new TypeError("Super expression must either be null or a function");
                t.prototype = Object.create(e && e.prototype, {
                    constructor: {
                        value: t,
                        writable: !0,
                        configurable: !0
                    }
                }),
                e && R(t, e)
            }(n, s["a"]);
            var e = L(n);
            function n(t) {
                var r;
                return function(t, e) {
                    if (!(t instanceof e))
                        throw new TypeError("Cannot call a class as a function")
                }(this, n),
                r = e.call(this, t),
                D = I(r),
                r
            }
            return x(n, [{
                key: "_getDefaultSettings",
                value: function() {
                    return N.extend(!0, {}, C(E(n.prototype), "_getDefaultSettings", this).call(this), {
                        element: "js_list_content",
                        css: {
                            contentBody: "js_content_body",
                            sort: "js_sort",
                            listSpinner: "js_list_spinner",
                            items: "input[name='eid[]']"
                        }
                    })
                }
            }]),
            x(n, [{
                key: "_bindEvents",
                value: function() {
                    this._elementOn("click", "." + this.settings.css.sort, this._handleSort.bind(this))
                }
            }, {
                key: "_refreshAndTrigger",
                value: function() {
                    var t = k(regeneratorRuntime.mark(function t() {
                        var e;
                        return regeneratorRuntime.wrap(function(t) {
                            for (; ; )
                                switch (t.prev = t.next) {
                                case 0:
                                    return t.next = 2,
                                    this.refresh();
                                case 2:
                                    return e = t.sent,
                                    this.trigger("refresh", e),
                                    t.abrupt("return", e);
                                case 5:
                                case "end":
                                    return t.stop()
                                }
                        }, t, this)
                    }));
                    return function() {
                        return t.apply(this, arguments)
                    }
                }()
            }, {
                key: "_fetchData",
                value: function() {
                    var t = this;
                    this._request && this._request.abort();
                    var e = this.settings.params
                      , n = e.folderId
                      , r = {
                        hid: n,
                        is_trash: e.isTrash,
                        sp: e.pageOffset,
                        sort: e.sort,
                        reverse: e.reverse,
                        grnLoadingIndicator: "." + this.settings.css.listSpinner
                    };
                    n ? delete r.top : r.top = 1;
                    return function(t) {
                        var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null
                          , n = t.loadingIndicator;
                        delete t.grnLoadingIndicator;
                        var r = new m.a({
                            grnUrl: "cabinet/ajax/index",
                            grnLoadingIndicator: n,
                            method: "post",
                            dataType: "json",
                            data: t
                        });
                        return Object(w.c)(e, r),
                        r.send()
                    }(r, function(e) {
                        return t._request = e
                    })
                }
            }, {
                key: "refresh",
                value: function() {
                    var t = k(regeneratorRuntime.mark(function t() {
                        var e;
                        return regeneratorRuntime.wrap(function(t) {
                            for (; ; )
                                switch (t.prev = t.next) {
                                case 0:
                                    return t.next = 2,
                                    this._fetchData();
                                case 2:
                                    return e = t.sent,
                                    this._updateList(e),
                                    t.abrupt("return", e);
                                case 5:
                                case "end":
                                    return t.stop()
                                }
                        }, t, this)
                    }));
                    return function() {
                        return t.apply(this, arguments)
                    }
                }()
            }, {
                key: "_updateList",
                value: function(t) {
                    var e = this._findByClassName(this.settings.css.contentBody);
                    return e.html(t.content),
                    (e = this._findByClassName(this.settings.css.contentBody)).hide().fadeIn(),
                    t
                }
            }, {
                key: "_handleSort",
                value: function(t) {
                    this.settings.params.sort = t.currentTarget.dataset.sort,
                    this.settings.params.reverse = t.currentTarget.dataset.reverse,
                    this.settings.params.pageOffset = 0,
                    this._refreshAndTrigger()
                }
            }, {
                key: "_getSelectedItems",
                value: function() {
                    return this._find(this.settings.css.items + ":checked")
                }
            }, {
                key: "hasSelectedItem",
                value: function() {
                    return this._getSelectedItems().length
                }
            }, {
                key: "handleDelete",
                value: function() {
                    var t = k(regeneratorRuntime.mark(function t() {
                        var e, n;
                        return regeneratorRuntime.wrap(function(t) {
                            for (; ; )
                                switch (t.prev = t.next) {
                                case 0:
                                    return e = this._getSelectedItems().map(function(t, e) {
                                        return e.value
                                    }).get(),
                                    n = parseInt(this.settings.params.folderId, 10),
                                    (isNaN(n) || n < 1) && (n = 1),
                                    t.prev = 3,
                                    t.next = 6,
                                    T(e, n);
                                case 6:
                                    return t.abrupt("return", this._refreshAndTrigger());
                                case 9:
                                    return t.prev = 9,
                                    t.t0 = t.catch(3),
                                    t.abrupt("return", Object($.a)(t.t0));
                                case 12:
                                case "end":
                                    return t.stop()
                                }
                        }, t, this, [[3, 9]])
                    }));
                    return function() {
                        return t.apply(this, arguments)
                    }
                }()
            }, {
                key: "restoreFile",
                value: function() {
                    var t = k(regeneratorRuntime.mark(function t() {
                        var e;
                        return regeneratorRuntime.wrap(function(t) {
                            for (; ; )
                                switch (t.prev = t.next) {
                                case 0:
                                    return e = this._getSelectedItems().map(function(t, e) {
                                        return e.value
                                    }).get(),
                                    t.next = 3,
                                    j(e);
                                case 3:
                                    return t.abrupt("return", this._refreshAndTrigger());
                                case 4:
                                case "end":
                                    return t.stop()
                                }
                        }, t, this)
                    }));
                    return function() {
                        return t.apply(this, arguments)
                    }
                }()
            }, {
                key: "changePage",
                value: function(t) {
                    return this.settings.params.pageOffset = t,
                    this._refreshAndTrigger()
                }
            }], [{
                key: "getInstance",
                value: function() {
                    return D
                }
            }]),
            n
        }()
          , M = n(112)
          , F = n(153);
        function V(t) {
            "@babel/helpers - typeof";
            return (V = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
                return typeof t
            }
            : function(t) {
                return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
            }
            )(t)
        }
        function B(t, e, n, r, i, s, o) {
            try {
                var a = t[s](o)
                  , c = a.value
            } catch (t) {
                return void n(t)
            }
            a.done ? e(c) : Promise.resolve(c).then(r, i)
        }
        function U(t) {
            return function() {
                var e = this
                  , n = arguments;
                return new Promise(function(r, i) {
                    var s = t.apply(e, n);
                    function o(t) {
                        B(s, r, i, o, a, "next", t)
                    }
                    function a(t) {
                        B(s, r, i, o, a, "throw", t)
                    }
                    o(void 0)
                }
                )
            }
        }
        function W(t, e) {
            for (var n = 0; n < e.length; n++) {
                var r = e[n];
                r.enumerable = r.enumerable || !1,
                r.configurable = !0,
                "value"in r && (r.writable = !0),
                Object.defineProperty(t, r.key, r)
            }
        }
        function H(t, e, n) {
            return (H = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function(t, e, n) {
                var r = function(t, e) {
                    for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = z(t)); )
                        ;
                    return t
                }(t, e);
                if (r) {
                    var i = Object.getOwnPropertyDescriptor(r, e);
                    return i.get ? i.get.call(n) : i.value
                }
            }
            )(t, e, n || t)
        }
        function q(t, e) {
            return (q = Object.setPrototypeOf || function(t, e) {
                return t.__proto__ = e,
                t
            }
            )(t, e)
        }
        function Q(t) {
            return function() {
                var e, n = z(t);
                if (function() {
                    if ("undefined" == typeof Reflect || !Reflect.construct)
                        return !1;
                    if (Reflect.construct.sham)
                        return !1;
                    if ("function" == typeof Proxy)
                        return !0;
                    try {
                        return Date.prototype.toString.call(Reflect.construct(Date, [], function() {})),
                        !0
                    } catch (t) {
                        return !1
                    }
                }()) {
                    var r = z(this).constructor;
                    e = Reflect.construct(n, arguments, r)
                } else
                    e = n.apply(this, arguments);
                return function(t, e) {
                    if (e && ("object" === V(e) || "function" == typeof e))
                        return e;
                    return function(t) {
                        if (void 0 === t)
                            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                        return t
                    }(t)
                }(this, e)
            }
        }
        function z(t) {
            return (z = Object.setPrototypeOf ? Object.getPrototypeOf : function(t) {
                return t.__proto__ || Object.getPrototypeOf(t)
            }
            )(t)
        }
        var J = jQuery
          , Y = {
            element: ".js_cabinet_index",
            params: {},
            pagination: {},
            css: {
                treeView: "js_tree_view",
                treeScrollClass: "scrollbar_s_grn tree_view_scroll_grn",
                treeWidthAdjustClass: "tree_width_ajust_grn",
                restoreButton: "js_restore_button"
            },
            id: {
                sitePosition: "js_cabinet_site_position",
                mainMenu: "js_cabinet_main_menu",
                memo: "js_cabinet_memo",
                warningLabel: "js_cabinet_warning_label",
                downloadMulti: "js_cabinet_download_multi"
            },
            splitScreenLayout: {},
            useHistory: !0
        }
          , G = function(t) {
            !function(t, e) {
                if ("function" != typeof e && null !== e)
                    throw new TypeError("Super expression must either be null or a function");
                t.prototype = Object.create(e && e.prototype, {
                    constructor: {
                        value: t,
                        writable: !0,
                        configurable: !0
                    }
                }),
                e && q(t, e)
            }(n, r["a"]);
            var e = Q(n);
            function n() {
                return function(t, e) {
                    if (!(t instanceof e))
                        throw new TypeError("Cannot call a class as a function")
                }(this, n),
                e.apply(this, arguments)
            }
            return function(t, e, n) {
                e && W(t.prototype, e),
                n && W(t, n)
            }(n, [{
                key: "_getDefaultSettings",
                value: function() {
                    return J.extend(!0, {}, H(z(n.prototype), "_getDefaultSettings", this).call(this), Y)
                }
            }, {
                key: "_onRender",
                value: function() {
                    (H(z(n.prototype), "_onRender", this).call(this),
                    this._initCabinetList(),
                    this._initPagination(),
                    this._initCategoryTree(),
                    isTouchDevice()) ? (this.$el.css("visibility", "visible"),
                    this._findByClassName(this.settings.css.treeView).removeClass(this.settings.css.treeScrollClass),
                    this.$el.removeClass(this.settings.css.treeWidthAdjustClass)) : this._initSplitterAndPosition()
                }
            }, {
                key: "_cacheElements",
                value: function() {
                    this.$sitePosition = J("#" + this.settings.id.sitePosition),
                    this.$mainMenu = J("#" + this.settings.id.mainMenu),
                    this.$memo = J("#" + this.settings.id.memo),
                    this.$warningLabel = J("#" + this.settings.id.warningLabel),
                    this.$downloadMulti = J("#" + this.settings.id.downloadMulti)
                }
            }, {
                key: "_initSplitterAndPosition",
                value: function() {
                    var t = this;
                    this.$splitScreenLayout = new F.a(this.settings.splitScreenLayout),
                    this.$splitScreenLayout.setWidth(this.settings.splitScreenLayout.defaultWidth),
                    this._initPosition(),
                    this.$fixedPositionTreeView.on("init", function() {
                        t.$splitScreenLayout.render()
                    }),
                    this.$splitScreenLayout.on("dodrag", function() {
                        t.$fixedPositionTreeView.setPositionProperties(),
                        t.$fixedPositionTreeView.trigger("change")
                    })
                }
            }, {
                key: "_initCategoryTree",
                value: function() {
                    this.$categoryTree = new v,
                    this.$categoryTree.render();
                    var t = this.history.get();
                    this._isTrashFolder() || this.$categoryTree.showTrash(t.hid)
                }
            }, {
                key: "_initCabinetList",
                value: function() {
                    this.settings.params.isTrash = this._isTrashFolder(),
                    this.$cabinetList = new A(this.settings),
                    this.$cabinetList.render()
                }
            }, {
                key: "_initPagination",
                value: function() {
                    this.$pagination = new i.a(this.settings),
                    this.$pagination.render(),
                    this._bindPaginationEvents()
                }
            }, {
                key: "_initPosition",
                value: function() {
                    var t = this;
                    this.$fixedPositionTreeView = new M.a(this.settings),
                    this.$fixedPositionTreeView.render(),
                    this.$fixedPositionTreeView.on("change", function() {
                        t.$splitScreenLayout.setSplitterPosition()
                    })
                }
            }, {
                key: "_bindPaginationEvents",
                value: function() {
                    this.$pagination.on("pageChange", this._handlePaginationChange.bind(this))
                }
            }, {
                key: "_bindEvents",
                value: function() {
                    var t = this;
                    this.$cabinetList.on("refresh", function(e, n) {
                        t.$pagination.setState(n.paginationInfo),
                        t._handleUpdateHistory(n.pageName),
                        t._setFixedPositionWithTimeout(),
                        t.$downloadMulti.html(n.downloadMulti)
                    }),
                    this.$categoryTree.on("expand", this._expandCategory.bind(this)),
                    this.$categoryTree.on("change", this._changeCategory.bind(this)),
                    this.$el.on("click", "." + this.settings.css.restoreButton, this._handleRestoreFile.bind(this))
                }
            }, {
                key: "_handleHistoryOnChange",
                value: function(t, e) {
                    var n = this
                      , r = this.$cabinetList.settings.params;
                    r.pageOffset = e.sp || 0,
                    r.folderId = 0 | e.hid,
                    r.isTrash = this._isTrashFolder(),
                    e.sort && (r.sort = e.sort),
                    e.reverse && (r.reverse = e.reverse),
                    this.$cabinetList.refresh().then(function(t) {
                        n._refreshComponents(t)
                    }),
                    this.$categoryTree.selectNode(r.folderId)
                }
            }, {
                key: "_refreshComponents",
                value: function(t) {
                    this.$sitePosition.html(t.sitePosition),
                    this.$mainMenu.html(t.mainMenu),
                    this.$memo.html(t.memo),
                    this.$warningLabel.html(t.warningLabel),
                    this.$downloadMulti.html(t.downloadMulti),
                    this._handleUpdatePageTitle(t.pageTitle),
                    this.$pagination.setState(t.paginationInfo);
                    var e = parseInt(this.$cabinetList.settings.params.folderId, 10);
                    if (this.$cabinetList.settings.params.isTrash) {
                        var n = this.$categoryTree.settings.rootId;
                        e && e !== n || this.$categoryTree.selectNode(n),
                        this.$categoryTree.hideTrash()
                    } else
                        this.$categoryTree.showTrash(e)
                }
            }, {
                key: "_changeCategory",
                value: function() {
                    var t = U(regeneratorRuntime.mark(function t(e) {
                        var n, r, i, s, o = arguments;
                        return regeneratorRuntime.wrap(function(t) {
                            for (; ; )
                                switch (t.prev = t.next) {
                                case 0:
                                    return n = o.length > 1 && void 0 !== o[1] ? o[1] : {},
                                    r = n.folderId,
                                    i = n.isTrash,
                                    this.$cabinetList.settings.params.folderId = r,
                                    this.$cabinetList.settings.params.isTrash = i,
                                    this.$cabinetList.settings.params.pageOffset = 0,
                                    delete this.$cabinetList.settings.params.sort,
                                    delete this.$cabinetList.settings.params.reverse,
                                    t.prev = 6,
                                    t.next = 9,
                                    this.$cabinetList.refresh();
                                case 9:
                                    s = t.sent,
                                    this._refreshComponents(s),
                                    this._handleUpdateHistory(s.pageName),
                                    this._setFixedPositionWithTimeout(),
                                    t.next = 18;
                                    break;
                                case 15:
                                    t.prev = 15,
                                    t.t0 = t.catch(6),
                                    Object($.a)(t.t0);
                                case 18:
                                case "end":
                                    return t.stop()
                                }
                        }, t, this, [[6, 15]])
                    }));
                    return function(e) {
                        return t.apply(this, arguments)
                    }
                }()
            }, {
                key: "_setFixedPositionWithTimeout",
                value: function() {
                    var t = this;
                    this.$fixedPositionTreeView && (clearTimeout(this.setPositionPropertiesTimeoutId),
                    this.setPositionPropertiesTimeoutId = setTimeout(function() {
                        t.$fixedPositionTreeView.setPositionProperties(),
                        t.$fixedPositionTreeView.trigger("change")
                    }, 350))
                }
            }, {
                key: "_isTrashFolder",
                value: function() {
                    return -1 !== location.pathname.indexOf("cabinet/garbage")
                }
            }, {
                key: "_expandCategory",
                value: function() {
                    if (!this._isTrashFolder()) {
                        var t = this.history.get();
                        this.$categoryTree.showTrash(t.hid)
                    }
                }
            }, {
                key: "_handlePaginationChange",
                value: function() {
                    var t = U(regeneratorRuntime.mark(function t(e, n) {
                        return regeneratorRuntime.wrap(function(t) {
                            for (; ; )
                                switch (t.prev = t.next) {
                                case 0:
                                    return t.prev = 0,
                                    t.next = 3,
                                    this.$cabinetList.changePage(n.offset);
                                case 3:
                                    t.next = 8;
                                    break;
                                case 5:
                                    t.prev = 5,
                                    t.t0 = t.catch(0),
                                    Object($.a)(t.t0);
                                case 8:
                                case "end":
                                    return t.stop()
                                }
                        }, t, this, [[0, 5]])
                    }));
                    return function(e, n) {
                        return t.apply(this, arguments)
                    }
                }()
            }, {
                key: "_handleUpdateHistory",
                value: function(t) {
                    var e = this.history.get();
                    e.sp = this.$cabinetList.settings.params.pageOffset,
                    e.sort = this.$cabinetList.settings.params.sort,
                    e.reverse = this.$cabinetList.settings.params.reverse,
                    e.hid = this.$cabinetList.settings.params.folderId,
                    e.hid ? delete e.top : (delete e.hid,
                    e.top = 1),
                    e.reverse || delete e.reverse,
                    e.sort || delete e.sort,
                    this.history.setPath(t),
                    this.history.pushState(e)
                }
            }, {
                key: "_handleUpdatePageTitle",
                value: function(t) {
                    document.title = t
                }
            }, {
                key: "_handleRestoreFile",
                value: function() {
                    this.$cabinetList.hasSelectedItem() && this.$cabinetList.restoreFile()
                }
            }]),
            n
        }()
          , K = n(16);
        Object(K.b)("grn.js.component.cabinet"),
        grn.js.component.cabinet = {
            ui: {
                index: {
                    CabinetList: A
                }
            }
        },
        Object(K.b)("grn.js.page.cabinet"),
        grn.js.page.cabinet = {
            Index: G
        }
    }
}, [[220, 0, 1]]]);
