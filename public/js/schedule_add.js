var GRN_ScheduleAdd = function() {
    var startYearSelect = document.getElementById("start_year")
      , startMonthSelect = document.getElementById("start_month")
      , startDaySelect = document.getElementById("start_day")
      , startHourSelect = document.getElementById("start_hour")
      , startMinuteSelect = document.getElementById("start_minute")
      , endYearSelect = document.getElementById("end_year")
      , endMonthSelect = document.getElementById("end_month")
      , endDaySelect = document.getElementById("end_day")
      , endHourSelect = document.getElementById("end_hour")
      , endMinuteSelect = document.getElementById("end_minute")
      , startTimezone = document.getElementById("timezone")
      , endTimezone = document.getElementById("end_timezone")
      , scheduleSubmit = grn.component.button("#schedule_submit_button")
      , validateDateStyle = document.getElementById("validate_date").style
      , invalidDateStyle = document.getElementById("invalid_date").style
      , formScheduleAdd = document.getElementById("schedule/add")
      , timezoneInfo = GRN_TimezoneInfo;
    return {
        onChangeDateTime: function() {
            var startYear = startYearSelect.value, 
            startMonth = startMonthSelect.value,
            startDay = startDaySelect.value, 
            endYear = endYearSelect.value, endMonth = endMonthSelect.value, 
            endDay = endDaySelect.value,
            startTransitions = timezoneInfo[startTimezone.value], 
            endTransitions = timezoneInfo[endTimezone.value], 
            startTransitionsLength = startTransitions.length, 
            endTransitionsLength = endTransitions.length, 
            startUTCTS, 
            endUTCTS, 
            startTS, 
            endTS, 
            startTransition, 
            endTransition, 
            hours, minutes, i;
            if(startHourSelect != null){
                var startHour = startHourSelect.value ? startHourSelect.value : 0,
                    startMinute = startMinuteSelect.value ? startMinuteSelect.value : 0  , 
                    endHour = endHourSelect.value ? endHourSelect.value : 0,
                    endMinute = endMinuteSelect.value ? startMinuteSelect.value : 0;
            }else{
                var startHour = 0,
                    startMinute = 0  , 
                    endHour = 0,
                    endMinute =  0;
            }
            if (this.validateForm(),
            "" === startHour && "" !== endHour)
                return validateDateStyle.display = "",
                void scheduleSubmit.disable();
            if (startTransitions && endTransitions) {
                if ("" !== startHour && "" !== endHour) {
                    if ("" === startMinute && (startMinute = 0),
                    "" === endMinute && (endMinute = 0),
                    startUTCTS = Date.UTC(startYear, startMonth - 1, startDay, startHour, startMinute, 0) / 1e3,
                    endUTCTS = Date.UTC(endYear, endMonth - 1, endDay, endHour, endMinute, 0) / 1e3,
                    startTS = parseInt(startUTCTS) - parseInt(startTransitions[0].offset),
                    endTS = parseInt(endUTCTS) - parseInt(endTransitions[0].offset),
                    startTransitions[0].ts > startTS || endTransitions[0].ts > endTS)
                        return void this.validateForm();
                    for (i = 1; i < startTransitionsLength && startTransitions[i].ts < parseInt(startUTCTS) - parseInt(startTransitions[i].offset); )
                        i += 1;
                    for (startTransition = startTransitions[i - 1],
                    startTS = parseInt(startUTCTS) - parseInt(startTransition.offset),
                    i = 1; i < endTransitionsLength && endTransitions[i].ts < parseInt(endUTCTS) - parseInt(endTransitions[i].offset); )
                        i += 1;
                    if (endTransition = endTransitions[i - 1],
                    (endTS = parseInt(endUTCTS) - parseInt(endTransition.offset)) < startTS)
                        return invalidDateStyle.display = "",
                        void scheduleSubmit.disable()
                }
            } else
                scheduleSubmit.enable()
        },
        validateForm: function() {
            validateDateStyle.display = "none",
            invalidDateStyle.display = "none",
            scheduleSubmit.enable()
        },
        setEndTime: function() {
            "" != startHourSelect.value && "" == startMinuteSelect.value && "" == endHourSelect.value && "" == endMinuteSelect.value && ("23" == startHourSelect.value ? endHourSelect.value = parseInt(startHourSelect.value) : endHourSelect.value = parseInt(startHourSelect.value) + 1,
            endMinuteSelect.value = 0,
            startMinuteSelect.value = 0)
        },
        SELECT_VALUE_EMPTY: -1,
        isValidYear: function(year) {
            var values;
            return -1 !== jQuery.map(startYearSelect.options, function(option) {
                return option.value
            }).indexOf(year.toString())
        },
        isValidMinute: function(minute) {
            var values;
            return -1 !== jQuery.map(startMinuteSelect.options, function(option) {
                return option.value
            }).indexOf(minute.toString())
        },
        getStartDateTime: function() {
            var hour = "" !== startHourSelect.value ? parseInt(startHourSelect.value, 10) : this.SELECT_VALUE_EMPTY
              , minute = "" !== startMinuteSelect.value ? parseInt(startMinuteSelect.value, 10) : this.SELECT_VALUE_EMPTY;
            return {
                year: parseInt(startYearSelect.value, 10),
                month: parseInt(startMonthSelect.value, 10),
                day: parseInt(startDaySelect.value, 10),
                hour: hour,
                minute: minute
            }
        },
        getEndDateTime: function() {
            var hour = "" !== endHourSelect.value ? parseInt(endHourSelect.value, 10) : this.SELECT_VALUE_EMPTY
              , minute = "" !== endMinuteSelect.value ? parseInt(endMinuteSelect.value, 10) : this.SELECT_VALUE_EMPTY;
            return {
                year: parseInt(endYearSelect.value, 10),
                month: parseInt(endMonthSelect.value, 10),
                day: parseInt(endDaySelect.value, 10),
                hour: hour,
                minute: minute
            }
        },
        setStartDateTime: function(year, month, day, hour, minute) {
            startYearSelect.value = year,
            startMonthSelect.value = month,
            startDaySelect.value = day,
            startHourSelect.value = hour !== this.SELECT_VALUE_EMPTY ? hour : "",
            startMinuteSelect.value = minute !== this.SELECT_VALUE_EMPTY ? minute : ""
        },
        setEndDateTime: function(year, month, day, hour, minute) {
            endYearSelect.value = year,
            endMonthSelect.value = month,
            endDaySelect.value = day,
            endHourSelect.value = hour !== this.SELECT_VALUE_EMPTY ? hour : "",
            endMinuteSelect.value = minute !== this.SELECT_VALUE_EMPTY ? minute : ""
        }
    }
}();
!function() {
    var func = function() {
        GRN_ScheduleAdd.onChangeDateTime()
    };
    jQuery("#start_hour").on("change", function(){
        GRN_ScheduleAdd.setEndTime();
    })
    jQuery("#start_year").on("change", func),
    jQuery("#start_month").on("change", func),
    jQuery("#start_day").on("change", func),
    jQuery("#start_hour").on("change", func),
    jQuery("#start_minute").on("change", func),
    jQuery("#end_year").on("change", func),
    jQuery("#end_month").on("change", func),
    jQuery("#end_day").on("change", func),
    jQuery("#end_hour").on("change", func),
    jQuery("#end_minute").on("change", func),
    jQuery(window).on("load", func)
}();
$('#start_hour,#start_minute,#end_hour,#end_minute').change(function(){
    if($(this).val() == ''){
        $('#start_hour,#start_minute,#end_hour,#end_minute').val('');
    }
})

