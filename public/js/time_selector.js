// time selector DIV
function grn_show_hide(target, url, target_form_param, grn_start_box_param, grn_end_box_param, kintone_dialog_mode) {
    grn_start_box = jQuery("#" + grn_start_box_param)[0],
    grn_end_box = jQuery("#" + grn_end_box_param)[0];
    var offset = jQuery("#" + target).offset();
    grn_time_selector_box ? (grn_time_selector_box.toggle(),
    grn_time_selector_box_base && grn_time_selector_box_base.toggle(),
    grn_time_selector_box.css({
        left: "1" === kintone_dialog_mode ? "125px" : "110px",
        top: "1" === kintone_dialog_mode ? offset.top + 65 + "px" : offset.top - 125 + "px"
    }),
    grn_time_selector_box_base && grn_time_selector_box_base.offset({
        top: grn_time_selector_box.offset().top,
        left: grn_time_selector_box.offset().left,
        width: grn_time_selector_box.width(),
        height: grn_time_selector_box.height()
    })) : (grn_time_selector_box = jQuery("<div>", {
        css: {
            zIndex: 100,
            position: "absolute",
            left: "1" === kintone_dialog_mode ? "125px" : "110px",
            top: "1" === kintone_dialog_mode ? offset.top + 65 + "px" : offset.top - 125 + "px",
            display: "block"
        }
    }).appendTo(document.body),
    grn_send_req(url + "?dummy=" + Math.floor(1e7 * Math.random())))
}
function grn_send_req(url) {
    jQuery.ajax({
        type: "get",
        url: url
    }).done(function(data, textStatus, jqXHR) {
        grn_onloaded(data, jqXHR)
    })
}
function grn_onloaded(data, jqXHR) {
    if (grn_time_selector_box.html(data),
    grn.browser.msie) {
        grn_time_selector_box_base = jQuery("<iframe></iframe>");
        var headers, match = jqXHR.getAllResponseHeaders().match(/X-JSON: (.*)/i), json;
        JSON.parse(match[1]).is_https && grn_time_selector_box_base.prop("src", grn_space_url),
        grn_time_selector_box_base.appendTo(document.body),
        grn_time_selector_box_base.css({
            border: 0,
            zIndex: 1,
            backgroundColor: "#FFFFFF",
            position: "absolute",
            display: "none",
            top: grn_time_selector_box.offset().top,
            left: grn_time_selector_box.offset().left,
            width: grn_time_selector_box.width(),
            height: grn_time_selector_box.height()
        }),
        grn_time_selector_box_base.toggle()
    }
}
function grn_clear_color() {
    for (grn_end_time = grn_start_time = null,
    i = 0; i < 24; i++)
        jQuery("#time" + i).length && grn_initial_display(i)
}
function grn_close_time_tool() {
    if (null != grn_start_time && null != grn_end_time)
        for (i = grn_start_time; i < grn_end_time; i++)
            jQuery("#time" + i).addClass("timeSelectHour_select");
    grn_end_time = grn_start_time = null,
    grn_time_selector_box.hide(),
    grn_time_selector_box_base && grn_time_selector_box_base.hide()
}
function grn_time_set(time) {
    null == grn_start_time ? (grn_start_time = time,
    grn_update_display()) : grn_close_time_tool()
}
function grn_end_set(time) {
    null != grn_start_time && grn_start_time <= time && (grn_end_time = time + 1,
    grn_update_display())
}
function grn_update_display() {
    for (grn_end_time || (grn_end_time = grn_start_time + 1),
    i = 0; i < 24; i++)
        jQuery("#time" + i).length && (i >= grn_start_time && i < grn_end_time ? jQuery("#time" + i).addClass("timeSelectHour_select") : grn_initial_display(i));
    grn_change_pull_down()
}
function grn_initial_display(i) {
    jQuery("#time" + i).removeClass("timeSelectHour_select"),
    i < 12 ? jQuery("#time" + i).addClass("timeSelectHour_bg00") : i >= 12 && i < 18 ? jQuery("#time" + i).addClass("timeSelectHour_bg01") : i >= 18 && i < 24 && jQuery("#time" + i).addClass("timeSelectHour_bg02")
}
function grn_change_pull_down() {
    $('#start_minute').val(0);
    $('#end_minute').val(0);
    null != grn_start_time ? grn_start_box.value = grn_start_time : grn_start_box.options[0].selected = true,
    null != grn_end_time ? grn_end_box.value = 24 == grn_end_time ? 23 : grn_end_time : grn_end_box.options[0].selected = true,
    GRN_Event.fireEvent(grn_start_box, "change"),
    GRN_Event.fireEvent(grn_end_box, "change")
}
var grn_time_selector_box, grn_time_selector_box_base, grn_start_box, grn_end_box, grn_start_time, grn_end_time;
