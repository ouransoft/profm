<!-- footer -->
<footer class="footer py-9">
    <div class="modal fade" id="modal_reset_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Thay đổi</span> mật khẩu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='reset_password'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <lable class="col-md-4">Mật khẩu cũ</lable>
                                        <div class="col-md-8 form-inline pd0">
                                            <input type="password" class='form-control old-password'>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline row">
                                        <lable class="col-md-4">Mật khẩu mới</lable>
                                        <div class="col-md-8 form-inline pd0">
                                            <input type="password" name='password' class='form-control news-password'>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline row">
                                        <lable class="col-md-4">Nhập lại</lable>
                                        <div class="col-md-8 pd0">
                                            <input type="password" class='form-control confirm-news-password'>
                                        </div>
                                    </div>
                                    <p>(*) Chú ý: Mật khẩu tối thiểu 6 ký tự bao gồm cả số</p>
                                </div>
                            </div>
                            <div class='text-center'>
                                <button type="submit" class='submit-reset-password'>Thay mật khẩu ngay</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(!is_null(\Auth::guard('member')->user()))
    <div class="modal fade" id="modal_update_avatar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Cập nhật</span> thông tin cá nhân</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='update_avatar'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="col-md-12 div-image">
                                    <div class="file-input file-input-ajax-new">
                                        <div class="input-group file-caption-main">
                                            <div class="input-group-btn input-group-append">
                                                <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">Chọn ảnh</span>
                                                    <input type="file" class="upload-image" multiple="multiple" name="avatar_upload[]" data-fouc="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="file-preview ">
                                            <div class=" file-drop-zone">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="avatar" class="image_data" value="{{\Auth::guard('member')->user()->avatar}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="inputPassword" class="col-sm-12 col-form-label">Họ và tên</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" name="full_name" value="{{\Auth::guard('member')->user()->full_name}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputPassword" class="col-sm-12 col-form-label">Email</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" name="email" type="email" value="{{\Auth::guard('member')->user()->email}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="inputPassword" class="col-sm-12 col-form-label">Số điện thoại</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" name="phone"  value="{{\Auth::guard('member')->user()->phone}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputPassword" class="col-sm-12 col-form-label">Địa chỉ</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" name="address" value="{{\Auth::guard('member')->user()->address}}">
                                    </div>
                                </div>
                            </div>
                            <div class='text-center'>
                                <button type="submit" class='submit-update-avatar'>Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="modal fade" id="modal_create_group_chat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel">Tạo nhóm chat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='frmAddGroup'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <label for="inputPassword" class="col-sm-12 col-form-label">Tên nhóm</label>
                                <div class="col-sm-12">
                                    <input class="form-control" name="name">
                                </div>
                            </div>
                            <div class="row">
                                <label for="inputPassword" class="col-sm-12 col-form-label">Thành viên nhóm</label>
                                <div class="col-sm-12 select-full">
                                    <select class='form-control select-search' id='select_member_list' name='member_id[]' multiple="" data-placeholder='Chọn thành viên'>

                                    </select>
                                </div>
                            </div>
                            <div class='text-center' style="margin-top:20px;">
                                <button type="submit" class='submit-create-group btn btn-success'>Tạo nhóm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="chat-message">
    </div>
</footer>
<input type='hidden' value='{{isset(\Auth::guard('member')->user()->id)? \Auth::guard('member')->user()->id : ''}}' id="private_id" />
<input type='hidden' value='' id="receiver" />
<script src="{!!asset('assets2/js/popper.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootstrap.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/parallax/parallax.js')!!}"></script>
<script src="{!!asset('assets2/js/scripts.js')!!}"></script>
<script src="{!!asset('assets2/js/select2.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.js')!!}"></script>
<script src="{!!asset('assets2/js/Notifier.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootbox.min.js')!!}"></script>
<script src="{!!asset('assets2/js/custom.js')!!}"></script>
<script src="{!!asset('assets2/js/main.js')!!}" id="_mainJS" data-plugins="load"></script>
<script src="{!!asset('assets2/js/pusher.min.js')!!}"></script>
<script src="{!!asset('assets2/js/jquery-ui.js')!!}"></script>
@if(!is_null(\Auth::guard('member')->user()))
<script>
    function scrollToBottomFunc() {
            $('.ps-container').animate({
                scrollTop: $('.ps-container').get(0).scrollHeight
            }, 50);
    }
    var my_id = $('#private_id').val();
        var receiver = '';
        Pusher.logToConsole = true;
        var pusher = new Pusher('5f84d2a344876b9fea04', {
            cluster: 'ap1',
            forceTLS: true
        });
        var channel = pusher.subscribe('chat-message');
        channel.bind('send-message', function (data) {
            if (my_id == data.from) {
                $('.ps-container').append(`
                    <li class="me">
                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                `+data.message+`
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(my_id === data.to && receiver == data.from ){
                $('.ps-container').append(`
                    <li class="you">
                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                `+data.message+`
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(my_id === data.to && receiver != data.from){
                if($('#count-message').html() === ''){
                     $('#count-message').html(1);
                }else{
                     $('#count-message').html(parseInt($('#count-message').html()) + 1);
                }
            }
            scrollToBottomFunc()
        });
    var channels = pusher.subscribe('chat-group-message');
        channels.bind('send-group-message', function (data) {
            if (my_id == data.from) {
                $('.ps-container').append(`
                    <li class="me">
                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                `+data.message+`
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(data.to.includes(parseInt(my_id)) == true && receiver == data.group_id){
                $('.ps-container').append(`
                    <li class="you">
                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                            <span class="notification-date">`+data.user_name+`</span>
                            <span class="chat-message-item">
                                `+data.message+`
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(Array(data.to).indexOf(my_id) == -1 && receiver != data.group_id){
                if($('#count-message').html() === ''){
                     $('#count-message').html(1);
                }else{
                     $('#count-message').html(parseInt($('#count-message').html()) + 1);
                }
            }
            scrollToBottomFunc()
        });
    $(document).on('keyup', '#content_message', function (e) {
        e.preventDefault();
        var message = $(this).val();
        var receiver_id = $('#receiver_id').val();
        var my_id = $('#my_id').val();
        var type = $('#type_message').val();
        if (e.keyCode == 13 && message != '' && receiver_id != '') {
            $(this).val('');
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                data: {message:message,receiver_id:receiver_id,my_id:my_id,type:type},
                success: function (data) {
                    scrollToBottomFunc();
                }
            })
        }
    });
    $('.seen-chat').click(function(){
        var id = {{\Auth::guard('member')->user()->id}};
        $('#search_member_message').val('');
        $.ajax({
            url: '/api/get-all-message',
            method: 'POST',
            data: {id:id},
            success: function (data) {
                $('#list-message').html(data.html);
            }
        })
    });
    $('#search_member_message').keyup(function(){
        var value = $(this).val();
        $.ajax({
            url: '/api/searching-member',
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
            },
            data: {value:value},
            success: function (data) {
                $.each(data.group_result, function(key, value){
                    $('#list-message').append(`<li class="media content-mess top-0">
                                                    <a href="javascript:void(0)" style="width:100%" class="message" data-link="2" data-type="1">
                                                        <div class="position-relative" style="margin-right:15px">
                                                            <img src="`+value['avatar']+`" class="rounded-circle" alt="" style="width:36px;height:36px;">
                                                        </div>
                                                        <div class="media-body">
                                                            <div class="media-title">
                                                                <span class="font-weight-semibold">`+value['name']+`</span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>`);
                })
                $.each(data.member_result, function(key, value){
                    $('#list-message').append(`<li class="media content-mess top-0">
                                                    <a href="javascript:void(0)" style="width:100%" class="message" data-link="2" data-type="1">
                                                        <div class="position-relative" style="margin-right:15px">
                                                            <img src="`+value['avatar']+`" class="rounded-circle" alt="" style="width:36px;height:36px;">
                                                        </div>
                                                        <div class="media-body">
                                                            <div class="media-title">
                                                                <span class="font-weight-semibold">`+value['name']+`</span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>`);
                })
            }
        })
    })
    $('body').delegate('.message','click',function(){
            var from = $(this).data('from');
            var to = $(this).data('to');
            receiver = $(this).data('to');
            $('.tab-message').removeClass('show');
            $.ajax({
                url: '/api/get-message',
                method: 'POST',
                data: {from: from,to:to},
                success: function (response) {
                    if (response.error === false) {
                        $('#chat-message').html(response.html);
                        $('#chat-message').append(response.html);
                        $('#chat-message').append(response.html);
                        $('#chat-message').append(response.html);
                        scrollToBottomFunc();
                    }
                }
            });
    })
    $('body').delegate('.close-mesage','click',function(){
        $('.chat-box').removeClass("show");
        return false;
    });
    $('body').delegate('.upload-file','change', function(){
        var file_data = $(this).prop('files')[0];
        var receiver_id = $('#receiver_id').val();
        var my_id = $('#my_id').val();
        var type = $('#type_message').val();
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('_token','{{ csrf_token() }}');
        form_data.append('receiver_id',receiver_id);
        form_data.append('type',type);
        form_data.append('my_id',my_id);
        $.ajax({
            url: '/api/send-file-message',
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(response){
                if(response.success == true){
                    $input.val(response.image);
                }
            }
            });
    });
</script>
<script>
    $('#seen-chat').click(function(){
        $('#count-message').html('');
    });
    $('.actve-message').click(function(){
        $('.actve-message').removeClass('actve-message');
    });
    $('body').delegate('.seen-notification', 'click', function () {
        var id = $(this).data('id');
        var link = $(this).data('link');
        $.ajax({
        url: '/api/seen-notification',
                method: 'POST',
                data:{id:id, _token : '{!! csrf_token() !!}'},
                success: function (response) {
                if (response.error == false) {
                    window.location.href = link;
                    //window.open(link);
                }
        }
        });
    });
    var channel = pusher.subscribe('NotificationEvent');
        channel.bind('send-notification', function(data) {
            var newNotificationHtml = `
            <li class="media">
                <a href="javascript:void(0)" class="seen-notification" data-id="${data.id}" data-link="${data.link}" style="width:100%">
                    <div class="media-body">
                        <div class="media-title">
                            <span class="font-weight-semibold color-blue">${data.full_name}</span>
                            <span class="text-muted float-right font-size-sm">${data.time}</span>
                        </div>
                        <span class="black font-small">${data.content}</span>
                    </div>
                </a>
            </li>
            `;
            var member_id = `${data.member_id}`;
            if (member_id == {!!\Auth::guard('member')->user()->id!!}){
                $('.list-notification').prepend(newNotificationHtml);
                $('#count-notification').html(`${data.count}`);
            }
        }
    );
    window.Pusher = undefined;
    $('.new-group').click(function(){
        $('#groupchat').modal('show');
    })
    $("body").delegate( "#frmAddGroup", "submit", function(e){
        e.preventDefault();
        $.ajax({
            url: '/api/add-group',
            method: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.error === false) {
                    $('#groupchat').modal('hide');
                    $('#frmAddGroup')[0].reset();
                    $('.custom-control-input').each(function () {
                        $(this).parent('span').removeClass("checked");
                        $(this).prop('checked', false);
                    });
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Thêm mới nhóm thành công");
                    notification.push();
                    setTimeout(function(){ location.reload(); }, 2000);
                }else{
                    var notifier = new Notifier();
                    var notification = notifier.notify("info", "Tên nhóm đã tồn tạo mời nhập tên khác");
                    notification.push();
                    setTimeout(function(){ location.reload(); }, 2000);
                }
            }
        });
    })
    $("#btnSearch").on('click', function (e) {
        e.preventDefault();
        if (!$('#searchTag').val().trim().length) {
            $('#searchTag').focus();
            return false;
        } else if ($('#searchTag').val().trim().match(/(\/|\'|\"|NULL|null|<|>|--|-->)/i)) {
            $('#searchTag').val('');
            $('#searchTag').attr('title', lang_pack.not_valid_character_search).attr('data-toggle', 'tooltip').tooltip();
            return false;
        } else if ($('#searchTag').attr('data-original-title')) {
            $('#searchTag').tooltip('dispose');
        }
        var keyword = $('#searchTag').val();
        var link = $('[name="poscats"] option:selected').data('route');
        location.href = link + '?keywords=' + keyword;
    });
    $(document).ready(function(){
        $('body').delegate('#header_pulldown_appmenu_grn','click',function(e){
            e.stopPropagation();
            if($(this).parent().find('#header_pulldown_appmenu_base_grn').hasClass('show')){
                $('#header_pulldown_appmenu_base_grn').removeClass('show');
            }else{
                $('#header_pulldown_appmenu_base_grn').addClass('show');
            }
        })
        $(document).on("click", function(event){
            if(!$(event.target).closest(".dropdown").length){
                $('#header_pulldown_appmenu_base_grn').removeClass('show');
            }
        });
    })
    $('body').delegate('.seen-all-notification','click',function(e){
        e.preventDefault();
        $.ajax({
            url: '/api/seen-all-notification',
            method: 'POST',
            success: function (response) {
                $('.list-notification').html('');
                $('#count-notification').html('');
            }
        });
    })
    $('body').delegate('.create-group-chat','click',function(e){
        e.preventDefault();
        $.ajax({
            url: '/api/get-list-member-html',
            method: 'POST',
            success: function (response) {
                $('#select_member_list').html(response.html);
                $('#modal_create_group_chat').modal('show')
            }
        });
    })
    $('body').delegate('.group-message','click',function(){
            var from = $(this).data('from');
            var to = $(this).data('to');
            receiver = $(this).data('to');
            $('.tab-message').removeClass('show');
            $.ajax({
                url: '/api/get-group-message',
                method: 'POST',
                data: {from: from,to:to},
                success: function (response) {
                    if (response.error === false) {
                        $('#chat-message').html(response.html);
                        scrollToBottomFunc();
                    }
                }
            });
        })
</script>
@endif
