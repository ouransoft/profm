<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="api_token" content="{{\Auth::guard('member')->user() ? \Auth::guard('member')->user()->api_token : ''}}">
<title>{{\App\Config::first()->title}}</title>
<!-- CSS -->
<link href="{!!asset('assets2/css/font.css')!!}" rel="stylesheet">
<link href="{!!asset('assets/css/std.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/schedule.css')!!}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/image_grn.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{!!asset('assets2/css/jquery.transfer.css')!!}">
<link href="{!!asset('assets2/css/fonts/etline-font.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/fontawesome/all.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/pe-icon-7-stroke.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/themify-icons.css')!!}" rel="stylesheet">
<link href="{!! asset('assets/css/components.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/plugins/slick/slick.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/bootstrap.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/icomoon/styles.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/main.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/styles.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/select2.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/schedule.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/custom.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/icon.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/sweetalert2.min.css')!!}" rel="stylesheet">

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="apple-touch-icon" href="{!!asset('assets2/img/apple-touch-icon.png')!!}">
<link rel="icon" href="{{\App\Config::first()->favicon()->first() ? asset(\App\Config::first()->favicon()->first()->link) : ''}}">
<!-- Fonts -->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>
<script src="{!!asset('assets2/js/utils.js')!!}"></script>
<script src="{!!asset('assets2/js/jquery-nice-select.js')!!}"></script>
<script src="{!!asset('assets2/js/chart.min.js')!!}"></script>
<script src="{!!asset('assets2/js/sweetalert2.min.js"')!!}"></script>
<!-- Favicons -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TSWJDSPKXP"></script>
<style>
:root {
  --main-font-size: {{\App\Config::first()->font_size}}px;
}
</style>
