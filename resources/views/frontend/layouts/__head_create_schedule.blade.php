<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="api_token" content="{{\Auth::guard('member')->user() ? \Auth::guard('member')->user()->api_token : ''}}">
<title>{{\App\Config::first()->title}}</title>
<!-- CSS -->
<link href="{!!asset('assets/css/std.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/msgbox.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/treeview.css')!!}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/print.css')}}" media="print" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/image_grn.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/mail.css')}}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/schedule.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/fag_tree.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/print.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/workflow.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/image_grn.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/main.css')!!}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/notification.css')}}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/protal.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets/css/Design-modernbrown.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('assets2/css/icon.css')!!}" rel="stylesheet">

<link rel="stylesheet" href="{!!asset('assets2/css/jquery.transfer.css')!!}">
<link href="{!!asset('assets2/css/fonts/etline-font.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/fontawesome/all.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/pe-icon-7-stroke.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/fonts/themify-icons.css')!!}" rel="stylesheet">
<!--<link href="{!! asset('assets/css/components.min.css') !!}" rel="stylesheet" type="text/css">-->
<link href="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/plugins/slick/slick.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/bootstrap.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/icomoon/styles.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/main.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/styles.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/select2.min.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/schedule.css')!!}" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<!-- Favicons -->
<link rel="apple-touch-icon" href="{!!asset('assets2/img/apple-touch-icon.png')!!}">
<link rel="icon" href="{{\App\Config::first()->favicon}}">
<!-- Fonts -->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>
<script src="{!!asset('assets2/js/utils.js')!!}"></script>
<script src="{!!asset('assets2/js/jquery-nice-select.js')!!}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TSWJDSPKXP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TSWJDSPKXP');
</script>
<script src="{{asset('/js/base.js')}}" type="text/javascript"></script>
<script language="javascript" type="text/javascript">

<!--

if( typeof grn.browser == "undefined" )
{
    grn.browser = {};
}

    grn.browser.chrome = true;
grn.browser.version = 87;


grn.browser.isSupportHTML5 = false;
if( !!window.FormData )
{
    grn.browser.isSupportHTML5 = true;
}

//-->

</script>
<script src="{{asset('/js/std.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/yahoo-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/event-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/dom-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/connection-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/treeview-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/json-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/fag_tree_26.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/tree.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/tree-facilitygroup.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/tree-organization-item.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/text_multilanguage.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/autofit.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/msgbox.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/com_header.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/button.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/url.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/i18n.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/resource-en.js')}}"></script>
<script src="{{asset('/js/request.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/error_default_view.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/error_handler.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/runtime.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/commons_chunk.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/common.js')}}" type="text/javascript"></script>
<script>
    grn.component.url.PAGE_PREFIX = "";
    grn.component.url.PAGE_EXTENSION = "";
    grn.component.url.STATIC_URL = "";
    grn.component.url.BUILD_DATE = "20200925.text";
</script>
<script type="text/javascript" language="javascript">
    grn.data = {"CSRF_TICKET":"cd3a775b3e7f9474039304062bb2bb0f","locale":"ja","login":{"id":"6","name":"\u4f50\u85e4 \u6607","slash":"","timezone":"Asia\/Tokyo","language":"ja","code":"sato","email":"sato@localhost","url":"","phone":"090-xxxx-xxxx"},"short_date_format":"&&mon&&\/&&mday&&\uff08&&wdayshort&&\uff09","assets_cache_busting":1580698759};
</script>
<script src="{{asset('/js/js_api_garoon.js')}}" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    grn.html.com_header.addEventListenerForResizeWindow();
</script>