<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        @include('frontend/layouts/__head')
    </head>
    <body>
        @include('frontend/layouts/__header')
        @include('frontend/schedule/sidebar')
        <div class="page-project" style="margin:0px;">
            @include('frontend.project.sidebar')
            @yield('content')
        </div>
        @include('frontend/layouts/footer_index')
    </body>
    @yield('script')   
</html>

