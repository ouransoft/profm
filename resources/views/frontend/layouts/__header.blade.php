<header>
    <div class="bottom-header" style="background: #333333;">
        <div>
            <div class="bottom-header-left bottom-header-left-admin">
                <div class="logo-header" style="display:flex">
                    <div id="header_pulldown_appmenu_grn" class="cloudHeader-dropdownMenu-grn header_appmenu_grn">
                        <button type="button" id="header_appmenu_title_grn" class="header_appmenu_title_grn button_style_off_grn" aria-controls="header_pulldown_appmenu_base_grn" aria-haspopup="true" aria-expanded="false"></button>
                        <div id="header_pulldown_appmenu_base_grn" class="cloudHeader-dropdownContents-grn" aria-labelledby="header_appmenu_title_grn" aria-hidden="false" style="max-height: 374.4px;">
                        <meta http-equiv="content-type" content="text/html;chaset=utf-8">
                        <div class="application_menu" title="Protal"> 
                            <span class="appmenu-item">
                               <a href="{{route('home.view')}}">
                                  <div class="icon-appMenu-portal appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Protal</nobr>
                                  </div>
                               </a>
                            </span>
                            <span class="appmenu-item" title="Scheduler" data-short_title="Space">
                               <a href="{{route('frontend.schedule.index')}}?">
                                  <div class="icon-appMenu-space appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Scheduler</nobr>
                                  </div>
                               </a>
                            </span>
                            
                            <span class="appmenu-item" title="To-do List" data-short_title="Bulletin Board">
                               <a href="{!!route('frontend.todo.index')!!}">
                                  <div class="icon-appMenu-bulletin appmenu-item-icon"></div>
                                  <div>
                                     <nobr>To-do List</nobr>
                                  </div>
                               </a>
                            </span>
                            <!--<span class="appmenu-item" title="Messages" data-short_title="Messages">
                               <a href="">
                                  <div class="icon-appMenu-message appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Time set</nobr>
                                  </div>
                               </a>
                            </span>-->
                            <span class="appmenu-item" title="Kaien" data-short_title="Workflow">
                                <a href="{{route('frontend.project.index')}}">
                                  <div class="icon-appMenu-address appmenu-item-icon"></div>
                                  <div>
                                     <nobr>Kaizen</nobr>
                                  </div>
                                </a>
                            </span>
                            
                            <span class="appmenu-item" title="Workflow" data-short_title="Workflow">
                                <a>
                                    <div class="icon-appMenu-system appmenu-item-icon"></div>
                                    <div>
                                        <nobr>TPM</nobr>
                                    </div>
                                </a>
                            </span>
                        
                            @if(\Auth::guard('member')->user()->level == \App\Member::LEVEL_ADMIN)
                                <span class="appmenu-item" title="System" data-short_title="Workflow">
                                    <a href="{{route('frontend.dashboard.index')}}">
                                      <div class="icon-appMenu-system appmenu-item-icon"></div>
                                      <div>
                                         <nobr>System</nobr>
                                      </div>
                                    </a>
                                </span>
                            @endif
                         </div>
                        </div>
                    </div>
                    <a href="@if(\Auth::guard('member')->user()->level > \App\Member::LEVEL_NV) {{route('home.view')}} @else  {{route('frontend.project.index')}} @endif"><img src="{!!asset('/img/i-vibo.png')!!}" style="margin-top: 2px;width: 180px;margin-left: 15px;"></a>
                </div>
            </div>
            <!--<form action="{{route('frontend.project.list')}}" method="get">-->
            <div class="bottom-header-right" style="padding-top:18px;float:left;">
                    <!--<input type="text" class="input-search" name="name" placeholder="Search..">-->
                <ul class="ul-notification">
                    <li>
                        <a href="javascript:void(0)" title="Tin nhắn" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle seen-chat"><i class="icon-bubbles4"></i>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id='count-message'>@if($count_message > 0){!! $count_message !!}@endif</span>
                        </a>
                        <div class="dropdown-menu" id="log_member" aria-labelledby="dropdownMenuButton">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold">{{trans('base.Your_message')}}</span>
                                <input id="search_member_message" placeholder="Tìm theo tên thành viên" name="search" type="text" style="font-size:13px;width: 50%;position: absolute;top: 9px;right: 5px;height: 25px!important;">
                            </div>
                            <div class="dropdown-content-body body-message dropdown-scrollable">
                                <ul class="media-list" id='list-message'>

                                </ul>
                            </div>
                            <div class="dropdown-content-footer justify-content-center p-0 text-center" style="border-top: 1px solid #e2e2e2;">
                                <a href="{{route('frontend.chat.index')}}" class="text-grey w-100 py-2">Xem tất cả</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" role="button" id="dropdownMenuLink" title="Thông báo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle notification-menu"><i class="icon-bell2 @if (count(Auth::guard('member')->user()->unreadNotifications)) bell @endif"></i>
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="count-notification">@if (count(Auth::guard('member')->user()->unreadNotifications)){{count(Auth::guard('member')->user()->unreadNotifications)}}@endif</span>
                        </a>
                        <div class="dropdown-menu" id="log_member" aria-labelledby="dropdownMenuButton" style="padding-bottom: 0px;">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold" style="font-size:14px;">{{trans('base.Your_notification')}}</span>
                            </div>
                            <div class="dropdown-content-body dropdown-scrollable">
                                <ul class="media-list list-notification">
                                    @foreach( \Auth::guard('member')->user()->unreadNotifications as $val)
                                    <li class="media">
                                        <a href="javascript:void(0)" class="seen-notification" style="width:100%" data-id="{{$val->id}}" data-link="{{$val->data['link']}}">
                                            <div class="media-body">
                                                <div class="media-title">
                                                    <span class="font-weight-semibold color-blue">{{$val->data['full_name']}}</span>
                                                    <span class="text-muted float-right font-size-sm">{{$val->data['time']}} ({{date('d/m',strtotime($val->created_at))}})</span>
                                                </div>
                                                <span class="black font-small">{{$val->data['content']}}</span>
                                            </div>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="dropdown-content-footer justify-content-center p-0 text-center" style="border-top: 1px solid #e2e2e2;">
                                <a href="javascript:void(0)" class="text-grey w-100 py-2 seen-all-notification" title="Load more" style="font-size: 14px;">{{trans('base.Mark_as_viewed_all')}}</a>
                            </div>
                        </div>
                    </li>
                    <!--
                    @if(\Auth::guard('member')->user()->level > 1)
                    <li style="position:relative;">
                        <a @if(\Auth::guard('member')->user()->level == 5) href="{!!route('frontend.project.list',['keyword'=>'unapproved'])!!}" @else href="javascript:void(0)" @endif>
                            <i class="fas fa-user-cog"></i>
                            @if(count(\App\Project::where('status','<',4)->whereDate('created_at','=',date('Y-m-d'))->get()) > 0 && \Auth::guard('member')->user()->level == 5)
                            <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="unapproved">{{count(\App\Project::where('status','<',4)->whereDate('created_at',date('Y-m-d'))->get())}}</span>
                            @endif
                        </a>
                    </li>
                    @endif
                    -->
                    <li>
                    <a href="#" role="button" id="dropdownMenuLink" title="Thông tin tài khoản" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">@if(is_null(\Auth::guard('member')->user()->file())) <i class="fas fa-user-circle"></i> @else <img style="width: 32px;height:32px;border-radius: 50%;border: 1px solid #7d7d7d;" src="{{asset(\Auth::guard('member')->user()->file()->link)}}" /> @endif</a>
                        <div class="dropdown-menu infomation-member" id="log_member" aria-labelledby="dropdownMenuButton">
                            <div class="content-log-member">
                                <div class="img-avatar">
                                <img src="@if(is_null(\Auth::guard('member')->user()->file())){!!asset('assets2/img/man.png')!!} @else {{asset(\Auth::guard('member')->user()->file()->link)}} @endif" style="width:250px;padding:10px 20px;">
                                </div>
                                <h4 class="orange text-center">{!!\Auth::guard('member')->user()->full_name!!}</h4>
                                <div class="info-member-log">
                                    <p><span class="fl50">{{trans('base.Employee_code')}}:</span><span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                    <p><span class="fl50">{{trans('base.Position')}}:</span><span>@if(\Auth::guard('member')->user()->position) {!!\Auth::guard('member')->user()->position->name!!} @endif</span></p>

                                </div>
                            </div>
                            <div class="button-member-log text-center">
                                <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_reset_password">{{trans('base.Change_the_password')}}</a>
                                <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_update_avatar">{{trans('base.Update_personal_information')}}</a>
                                <a href="{!!route('logoutMember')!!}" class="btn">{{trans('base.Sign_out_of_your_account')}}</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#" role="button" id="dropdownMenuLink" title="Thông tin tài khoản" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle choose-language">
                            {!!trans('base.language')!!}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item @if(session('locale') == 'en') active @endif" href="{{route('frontend.language.change',['locale'=>'en'])}}"><span class="icons icons-england" style="top:4px;margin-right: 8px;"></span>{{trans('base.English')}}</a>
                            <a class="dropdown-item @if(session('locale') == 'vi') active @endif" href="{{route('frontend.language.change',['locale'=>'vi'])}}"><span class="icons icons-vietnam" style="top:4px;margin-right: 8px;"></span>{{trans('base.Vietnamese')}}</a>
                        </div>
                    </li>
                </ul>

            </div>
           <!-- </form>-->
        </div>
    </div>
</header>

