<!-- footer -->
<footer class="footer py-9">
    <div class="bottom-footer" >
        <div class="bottom-footer-left">
            <div class="logo-footer text-right">
                <a href="javascript:void(0)"><img src="{{\App\Config::first()->logo()->first() ? asset(\App\Config::first()->logo()->first()->link) : ''}}" style="height:44px"></a>
            </div>
        </div>
        <div class="bottom-footer-right">
            <p style="margin-bottom: 0px;margin-right: 25px;">Copyright @2020 OURANSOFT TECHNOLOGY JSC. All Rights Reserved</p>
        </div>
    </div>
    <div class="modal fade" id="modal_reset_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Thay đổi</span> mật khẩu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='reset_password'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <lable class="col-md-4">Mật khẩu cũ</lable>
                                        <div class="col-md-8 form-inline pd0">
                                            <input type="password" class='form-control old-password'>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline row">
                                        <lable class="col-md-4">Mật khẩu mới</lable>
                                        <div class="col-md-8 form-inline pd0">
                                            <input type="password" name='password' class='form-control news-password'>
                                        </div>
                                    </div>
                                    <div class="form-group form-inline row">
                                        <lable class="col-md-4">Nhập lại</lable>
                                        <div class="col-md-8 pd0">
                                            <input type="password" class='form-control confirm-news-password'>
                                        </div>
                                    </div>
                                    <p>(*) Chú ý: Mật khẩu tối thiểu 6 ký tự bao gồm cả số</p>
                                </div>
                            </div>
                            <div class='text-center'>
                                <button type="submit" class='submit-reset-password'>Thay mật khẩu ngay</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(!is_null(\Auth::guard('member')->user()))
    <div class="modal fade" id="modal_update_avatar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Cập nhật</span> thông tin cá nhân</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='update_avatar'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="col-md-12 div-image">
                                    <div class="file-input file-input-ajax-new">
                                        <div class="input-group file-caption-main">
                                            <div class="input-group-btn input-group-append">
                                                <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">Chọn ảnh</span>
                                                    <input type="file" class="upload-image" name="avatar_upload" accept="image/*" data-type="{{\App\File::TYPE_MEMBER}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="file-preview ">
                                            <div class=" file-drop-zone">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" class="image-link" value="{{\Auth::guard('member')->user()->file() ? asset(\Auth::guard('member')->user()->file()->link) : ''}}">
                                    <input type="hidden" name="file_id" class="image-id" value="{{\Auth::guard('member')->user()->file() ? \Auth::guard('member')->user()->file()->id : ''}}">
                                    <input type="hidden" class="image-name" value="{{\Auth::guard('member')->user()->file() ? \Auth::guard('member')->user()->file()->name : ''}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="inputPassword" class="col-sm-12 col-form-label">Họ và tên</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" name="full_name" value="{{\Auth::guard('member')->user()->full_name}}"> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputPassword" class="col-sm-12 col-form-label">Email</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" name="email" type="email" value="{{\Auth::guard('member')->user()->email}}"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="inputPassword" class="col-sm-12 col-form-label">Số điện thoại</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" name="phone"  value="{{\Auth::guard('member')->user()->phone}}"> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="inputPassword" class="col-sm-12 col-form-label">Địa chỉ</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" name="address" value="{{\Auth::guard('member')->user()->address}}"> 
                                    </div>
                                </div>
                            </div>
                            <div class='text-center'>
                                <button type="submit" class='submit-update-avatar'>Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="modal fade" id="modal_create_group_chat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px">
                    <h5 class="modal-title" id="exampleModalLabel">Tạo nhóm chat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form method="post" id='frmAddGroup'>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <label for="inputPassword" class="col-sm-12 col-form-label">Tên nhóm</label>
                                <div class="col-sm-12">
                                    <input class="form-control" name="name">
                                </div>
                            </div>
                            <div class="row">
                                <label for="inputPassword" class="col-sm-12 col-form-label">Thành viên nhóm</label>
                                <div class="col-sm-12 select-full">
                                    <select class='form-control select-search' id='select_member_list' name='member_id[]' multiple="" data-placeholder='Chọn thành viên'>

                                    </select>
                                </div>
                            </div>
                            <div class='text-center' style="margin-top:20px;">
                                <button type="submit" class='submit-create-group btn btn-success'>Tạo nhóm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="chat-message">
    </div>
</footer>
<input type='hidden' value='{{isset(\Auth::guard('member')->user()->id)? \Auth::guard('member')->user()->id : ''}}' id="private_id" />
<input type='hidden' value='' id="receiver" />
<script src="{!!asset('assets2/js/popper.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootstrap.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/parallax/parallax.js')!!}"></script>
<script src="{!!asset('assets2/js/scripts.js')!!}"></script>
<script src="{!!asset('assets2/js/select2.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.js')!!}"></script>
<script src="{!!asset('assets2/js/Notifier.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootbox.min.js')!!}"></script>
<script src="{!!asset('assets2/js/custom.js')!!}"></script>
<script src="{!!asset('assets2/js/main.js')!!}" id="_mainJS" data-plugins="load"></script>
<script src="{!!asset('assets2/js/pusher.min.js')!!}"></script>
<script src="{!!asset('assets2/js/jquery-ui.js')!!}"></script>
@if(!is_null(\Auth::guard('member')->user()))
<script>
    function scrollToBottomFunc(element) {
            element.animate({
                scrollTop: element.get(0).scrollHeight
            }, 100);
    }
    var my_id = $('#private_id').val();
        var receiver = '';
        Pusher.logToConsole = true;
        var pusher = new Pusher('5f84d2a344876b9fea04', {
            cluster: 'ap1',
            forceTLS: true
        });
        var channel = pusher.subscribe('chat-message');
        channel.bind('send-message', function (data) {
            if(data.type == 3){
                message = ` <span class="chat-message-item">
                                <a href="{{asset('`+data.message+`')}}">`+data.file_name+`</a>
                            </span>`;
            }else if(data.type == 2){
                message = ` <span class="chat-message-item w-50">
                                <a href="{{asset('`+data.message+`')}}"><img src="{{asset('`+data.message+`')}}"></a>
                            </span>`;
            }else{
                message = ` <span class="chat-message-item">`+data.message+`</span>`;
            }
            if (my_id == data.from) {
                $('.ps-container').append(`
                    <li class="me">
                        <div class="chat-thumb"><img src="`+data.avatar+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                                `+message+`
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(my_id === data.to && receiver == data.from ){
                $('.ps-container').append(`
                    <li class="you">
                        <div class="chat-thumb"><img src="`+data.avatar+`" alt="" class="avatar-img"></div>
                        <div class="notification-event">
                             `+message+`
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                        </div>
                    </li>
                `);
            }else if(data.member_ids.includes(my_id)){
                if($('#count-message').html() === ''){
                     $('#count-message').html(1);
                }else{
                     $('#count-message').html(parseInt($('#count-message').html()) + 1);
                }
            }
            scrollToBottomFunc($('#group'+data.group_id).find('.ps-container'));
    });
    var channels = pusher.subscribe('chat-group-message');
    channels.bind('send-group-message', function (data) {
        if (my_id == data.from) {
            $('#group'+data.group_id).find('.ps-container').append(`<li class="me">
                                                                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                                                                        <div class="notification-event">
                                                                            <span class="chat-message-item">
                                                                                `+data.message+`
                                                                            </span>
                                                                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                                                                        </div>
                                                                    </li>`);
        }else if(data.to.includes(parseInt(my_id)) == true && receiver == data.group_id){
           $('#group'+data.group_id).find('.ps-container').append(`<li class="you">
                                                                        <div class="chat-thumb"><img src="`+data.image+`" alt="" class="avatar-img"></div>
                                                                        <div class="notification-event">
                                                                            <span class="chat-message-item">
                                                                                `+data.message+`
                                                                            </span>
                                                                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">{{ date('d m, h:i a') }}</time></span>
                                                                        </div>
                                                                    </li>`);
        }else if(Array(data.to).indexOf(my_id) == -1 && receiver != data.group_id){
            if($('#count-message').html() === ''){
                 $('#count-message').html(1);
            }else{
                 $('#count-message').html(parseInt($('#count-message').html()) + 1);
            }
        }
        scrollToBottomFunc($('#group'+data.group_id).find('.ps-container'));
    });
    $(document).on('keyup', '#content_message', function (e) {
        e.preventDefault();
        let form_data = $(this).parents('form').serialize();
        let message = $(this).val();
        let $this = $(this);
        if (e.keyCode == 13 && message != ''){
            $(this).val('');
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                data: form_data + '&type_file=text',
                success: function (data) {
                    scrollToBottomFunc($this.parents('.chat-list').find('.ps-container'));
                }
            })
        }
    });
    $('.seen-chat').click(function(){
        var id = {{\Auth::guard('member')->user()->id}};
        $('#search_member_message').val('');
        $.ajax({
            url: '/api/get-all-message',
            method: 'POST',
            data: {id:id},
            success: function (data) {
                let html = '';
                $.each(data.messages , function( key, value ) {
                        if(value.type == 2){
                            message = '[ Hình ảnh ]';
                        }else if(value.type == 3){
                            message = '[ File ]';
                        }else{
                            message = value.message;
                        }
                        html += `<li class="media content-mess top-0 `+(value.seen == 0 ? 'active' : '')+`" class="user`+value.id+`">
                                    <a href="javascript:void(0)" style="width:100%" class="message" data-link=`+value.id+` data-type="1">
                                        <div class="position-relative" style="margin-right:15px">
                                            <img src="{!!asset("`+value.avatar+`")!!}" class="rounded-circle" alt="" style="width:36px;height:36px;">
                                        </div>
                                        <div class="media-body">
                                            <div class="media-title">
                                                <span class="font-weight-semibold">`+value.name+`</span>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <span class="black sub-msg"> `+ message +`</span>
                                                <span class="text-muted float-right font-size-sm float">`+value.time+`</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>`;
                });
                $('#list-message').html(html);
            }
        })
    });
    $('#search_member_message').keyup(function(){
        var value = $(this).val();
        var id = {{\Auth::guard('member')->user()->id}};
        if(value != ''){
            $.ajax({
                url: '/api/searching-member',
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: {value:value},
                success: function (data) {
                    $('#list-message').html('');
                    $.each(data.group_result, function(key, value){
                        $('#list-message').append(`<li class="media content-mess top-0">
                                                        <a href="javascript:void(0)" style="width:100%" class="message" data-link="`+value['link']+`" data-type="`+value['type']+`">
                                                            <div class="position-relative" style="margin-right:15px">
                                                                <img src="{!!asset("`+value.avatar+`")!!}" class="rounded-circle" alt="" style="width:36px;height:36px;">
                                                            </div>
                                                            <div class="media-body">
                                                                <div class="media-title">
                                                                    <span class="font-weight-semibold">`+value['name']+`</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>`);
                    })
                    $.each(data.member_result, function(key, value){
                        $('#list-message').append(`<li class="media content-mess top-0">
                                                        <a href="javascript:void(0)" style="width:100%" class="message" data-link="`+value['link']+`" data-type="`+value['type']+`">
                                                            <div class="position-relative" style="margin-right:15px">
                                                                <img src="{!!asset("`+value.avatar+`")!!}" class="rounded-circle" alt="" style="width:36px;height:36px;">
                                                            </div>
                                                            <div class="media-body">
                                                                <div class="media-title">
                                                                    <span class="font-weight-semibold">`+value['name']+`</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>`);
                    })
                }
            })
        }else{
            $.ajax({
                url: '/api/get-all-message',
                method: 'POST',
                data: {id:id,full_name:value,_token: "{{ csrf_token() }}"},
                success: function (data) {
                    $('#list-message').html('');
                    let html = '';
                    $.each(data.messages , function( key, value ) {
                        if(value.type == 2){
                            message = '[ Hình ảnh ]';
                        }else if(value.type == 3){
                            message = '[ File ]';
                        }else{
                            message = value.message.substr(0,40);
                        }
                        html += `<li class="media content-mess top-0" class="user`+value.id+`">
                                    <a href="javascript:void(0)" style="width:100%" class="message" data-link=`+value.id+` data-type="1">
                                        <div class="position-relative" style="margin-right:15px">
                                            <img src="{!!asset("`+value.avatar+`")!!}" class="rounded-circle" alt="" style="width:36px;height:36px;">
                                        </div>
                                        <div class="media-body">
                                            <div class="media-title">
                                                <span class="font-weight-semibold">`+value.name+`</span>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <span class="black sub-msg"> `+ message +`</span>
                                                <span class="text-muted float-right font-size-sm float">`+value.time+`</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>`;
                    });
                    $('#list-message').html(html);
                }
            })
        }
    })
    $('body').delegate('.message','click',function(){
        var link = $(this).data('link');
        if(!$('#group'+link).length){
            var type = $(this).data('type');
            var $this = $(this);
            $('.tab-message').removeClass('show');
            $.ajax({
                url: '/api/get-message',
                method: 'POST',
                data: {link:link,type:type},
                success: function (response) {
                    if (response.error === false) {
                        var messages = ``;
                        $.each(response.html , function( key, value ) {
                            if(value.type == 3){
                                message = ` <span class="chat-message-item">
                                                <a href="{{asset('`+value.message+`')}}">`+value.file_name+`</a>
                                            </span>`;
                            }else if(value.type == 2){
                                message = ` <span class="chat-message-item w-50">
                                                <a href="{{asset('`+value.message+`')}}"><img src="{{asset('`+value.message+`')}}"></a>
                                            </span>`;
                            }else{
                                message = ` <span class="chat-message-item">`+value.message+`</span>`;
                            }
                            if(value.id == my_id){
                                messages +=`<li class="me">
                                                    <div class="chat-thumb"><img src="`+value.avatar+`" alt="" class="avatar-img"></div>
                                                    <div class="notification-event">
                                                        `+message+`
                                                        <span class="notification-date"><time class="entry-date updated">`+value.time+`</time></span>
                                                    </div>
                                            </li>`;
                            }else{
                                messages +=`<li class="you">
                                                    <div class="chat-thumb"><img src="`+value.avatar+`" alt="" class="avatar-img"></div>
                                                    <div class="notification-event">
                                                        `+message+`
                                                        <span class="notification-date"><time class="entry-date updated">`+value.time+`</time></span>
                                                    </div>
                                            </li>`;
                            }
                        })
                        if(link == 1){
                            var html =` <div class="chat-box show" id="group`+response.group_id+`">
                                            <div class="chat-head">
                                                <h6>`+response.group_name+`</h6>
                                                <div class="more">
                                                    <span class="close-mesage"><i class="fa fa-times"></i></span>
                                                </div>
                                            </div>
                                            <form enctype="multipart/form-data">                            
                                                <div class="chat-list">
                                                    <ul class="ps-container ps-theme-default ps-active-y">
                                                        `+messages+`
                                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
                                                        <div class="ps-scrollbar-y-rail" style="top: 0px; height: 290px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 215px;"></div></div>
                                                    </ul>
                                                </div>
                                            </form>
                                        </div>`;
                        }else{
                            var html =` <div class="chat-box show" id="group`+response.group_id+`">
                                            <div class="chat-head">
                                                <h6>`+response.group_name+`</h6>
                                                <div class="more">
                                                    <span class="close-mesage"><i class="fa fa-times"></i></span>
                                                </div>
                                            </div>
                                            <form enctype="multipart/form-data">                            
                                                <div class="chat-list">
                                                    <ul class="ps-container ps-theme-default ps-active-y">
                                                        `+messages+`
                                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
                                                        <div class="ps-scrollbar-y-rail" style="top: 0px; height: 290px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 215px;"></div></div>
                                                    </ul>
                                                    <div class="text-box">
                                                        <input type="hidden" name="receiver_id" value="`+response.group_id+`" id="receiver_id"/>
                                                        <input type="hidden" name="my_id" value="`+my_id+`" />
                                                        <input type="hidden" name="type" value="`+response.type+`" />
                                                        <div class="dropdown-file">
                                                            <button type="button" class="custom-file-upload dropbtn" onclick="myFunction()">
                                                                <i class="fa fa-cloud-upload-alt"></i>
                                                            </button>
                                                            <div id="myDropdown" class="dropdown-content-file">
                                                                <form method='post' action='' enctype="multipart/form-data">
                                                                    <label class="dropdown-item" for="imageInput">
                                                                        <svg class="hw-20 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"/>
                                                                        </svg>
                                                                        <span>Hình ảnh</span>
                                                                        <input type="file" class="custom-file-input position-absolute w-0" name="images[]" id="imageInput" accept="image/*" multiple>
                                                                    </label>
                                                                </form>
                                                                <form method='post' action='' enctype="multipart/form-data">
                                                                    <label class="dropdown-item" for="documentInput">
                                                                        <svg class="hw-20 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"/>
                                                                        </svg>
                                                                        <span>Tài liệu</span>
                                                                        <input type="file" class="custom-file-input position-absolute w-0" name="files[]" id="documentInput" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" multiple>
                                                                    </label>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <textarea placeholder="Tin nhắn..." name="message" id="content_message"></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>`;
                        }
                        $('#chat-message').append(html);
                        if(response.count_message == 0){
                            $('#count-message').html('');
                        }else{
                            $('#count-message').html(response.count_message);
                        }
                        scrollToBottomFunc($('#group'+response.group_id).find('.ps-container'));
                    }
                }
            });
        }
    })
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content-file");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
    $('body').delegate('.close-mesage','click',function(){
        $(this).parents('.chat-box').remove();
        return false;
    });
    $('body').delegate('.upload-file','change', function(){
        var file_data = $(this).prop('files')[0];
        var receiver_id = $('#receiver_id').val();
        var my_id = $('#my_id').val();
        var type = $('#type_message').val();
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('_token','{{ csrf_token() }}');
        form_data.append('receiver_id',receiver_id);
        form_data.append('type',type);
        form_data.append('my_id',my_id);
        $.ajax({
            url: '/api/send-file-message',
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(response){
                if(response.success == true){
                    $input.val(response.image);
                }
            }
         });
    });
</script>
<script>
    $('#seen-chat').click(function(){
        $('#count-message').html('');
    });
    $('.actve-message').click(function(){
        $('.actve-message').removeClass('actve-message');
    });
    $('body').delegate('.seen-notification', 'click', function (){
        var id = $(this).data('id');
        var link = $(this).data('link');
        $.ajax({
        url: '/api/seen-notification',
            method: 'POST',
            data:{id:id, _token : '{!! csrf_token() !!}'},
            success: function (response) {
                if (response.error == false) {
                    window.location.href = link;
                }
            }
        });
    });
    var channel = pusher.subscribe('NotificationEvent');
        channel.bind('send-notification', function(data) {
            var newNotificationHtml = `
            <li class="media">
                <a href="javascript:void(0)" class="seen-notification" data-id="${data.id}" data-link="${data.link}" style="width:100%">
                    <div class="media-body">
                        <div class="media-title">
                            <span class="font-weight-semibold color-blue">${data.full_name}</span>
                            <span class="text-muted float-right font-size-sm">${data.time}</span>
                        </div>
                        <span class="black font-small">${data.content}</span>
                    </div>
                </a>
            </li>
            `;
            var member_id = `${data.member_id}`;
            if (member_id == {!!\Auth::guard('member')->user()->id!!}){
                $('.list-notification').prepend(newNotificationHtml);
                $('#count-notification').html(`${data.count}`);
            }
        }
    );
    window.Pusher = undefined;
    $('.new-group').click(function(){
        $('#groupchat').modal('show');
    })
    $("body").delegate( "#frmAddGroup", "submit", function(e){
        e.preventDefault();
        $.ajax({
            url: '/api/add-group',
            method: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.error === false) {
                    $('#groupchat').modal('hide');
                    $('#frmAddGroup')[0].reset();
                    $('.custom-control-input').each(function () {
                        $(this).parent('span').removeClass("checked");
                        $(this).prop('checked', false);
                    });
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Thêm mới nhóm thành công");
                    notification.push();
                    setTimeout(function(){ location.reload(); }, 2000);
                }else{
                    var notifier = new Notifier();
                    var notification = notifier.notify("info", "Tên nhóm đã tồn tạo mời nhập tên khác");
                    notification.push();
                    setTimeout(function(){ location.reload(); }, 2000);
                }
            }
        });
    })
    $("#btnSearch").on('click', function (e) {
        e.preventDefault();
        if (!$('#searchTag').val().trim().length) {
            $('#searchTag').focus();
            return false;
        } else if ($('#searchTag').val().trim().match(/(\/|\'|\"|NULL|null|<|>|--|-->)/i)) {
            $('#searchTag').val('');
            $('#searchTag').attr('title', lang_pack.not_valid_character_search).attr('data-toggle', 'tooltip').tooltip();
            return false;
        } else if ($('#searchTag').attr('data-original-title')) {
            $('#searchTag').tooltip('dispose');
        }
        var keyword = $('#searchTag').val();
        var link = $('[name="poscats"] option:selected').data('route');
        location.href = link + '?keywords=' + keyword;
    });
    $('.bootstrap-select').niceSelect();
    $(document).ready(function(){
        $('body').delegate('#header_pulldown_appmenu_grn','click',function(e){
            e.stopPropagation();
            if($(this).parent().find('#header_pulldown_appmenu_base_grn').hasClass('show')){
                $('#header_pulldown_appmenu_base_grn').removeClass('show');
            }else{
                $('#header_pulldown_appmenu_base_grn').addClass('show');
            }
        })
        $(document).on("click", function(event){
            if(!$(event.target).closest(".dropdown").length){
                $('#header_pulldown_appmenu_base_grn').removeClass('show');
            }
        });
    })
    $('body').delegate('.seen-all-notification','click',function(e){
        e.preventDefault();
        $.ajax({
            url: '/api/seen-all-notification',
            method: 'POST',
            success: function (response) {
                $('.list-notification').html('');
                $('#count-notification').html('');
            }
        });
    })
    $('body').delegate('.create-group-chat','click',function(e){
        e.preventDefault();
        $.ajax({
            url: '/api/get-list-member-html',
            method: 'POST',
            success: function (response) {
                $('#select_member_list').html(response.html);
                $('#modal_create_group_chat').modal('show')
            }
        });
    })
    $('body').delegate('#imageInput','change',function(){
        $(".attach-file").toggleClass('show');
        var $this = $(this);
        var image = $(this).prop('files');
        var from = {!!\Auth::guard('member')->user()->id!!}
        var receiver_id = $('#receiver_id').val();
        var type = 2;
        var form_data = new FormData();
        form_data.append('type_file','image');
        for(let i=0;i<image.length;i++){
            form_data.append('image', image[i]);
            form_data.append('type', type);
            form_data.append('my_id', {{\Auth::guard('member')->user()->id}});
            form_data.append('receiver_id', receiver_id);
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                headers: {
                        'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                    },
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    $("#imageInput").val('');
                    scrollToBottomFunc($('#group'+receiver_id).find('.ps-container'));
                }
            })
        }
    });
    $('body').delegate('#documentInput','change',function(){
        var $this = $(this);
        $(".attach-file").toggleClass('show');
        var file = $(this).prop('files');
        var from = {!!\Auth::guard('member')->user()->id!!}
        var receiver_id = $('#receiver_id').val();
        var type = 3;
        for(let i=0;i<file.length;i++){
            var form_data = new FormData();
            form_data.append('type_file','file');
            form_data.append('file', file[i]);
            form_data.append('type', 3);
            form_data.append('my_id', {{\Auth::guard('member')->user()->id}});
            form_data.append('receiver_id', receiver_id);
             var type = $('#type').val();
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                headers: {
                        'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                    },
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    $("#fileInput").val('');
                    scrollToBottomFunc($('#group'+receiver_id).find('.ps-container'));
                }
            })
        }
    });
</script>
@endif
