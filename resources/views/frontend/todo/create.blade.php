@extends('frontend.layouts.create_schedule')
@section('content')
<style>
    .overlay{
        display: none;
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 999;
        background: rgba(255,255,255,0.8) url("/public/img/loader.gif") center no-repeat;
    }
    /* Turn off scrollbar when body element has the loading class */
    body.loading{
        overflow: hidden;   
    }
    /* Make spinner image visible when body element has the loading class */
    body.loading .overlay{
        display: block;
    }
</style>
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div role="heading" aria-level="2">
               <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">Giao việc
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea ">
   <h2 style="display:inline;" class="todo">Tạo mới</h2>
   <form name="todo/add" method="post" action="{{route('frontend.todo.store')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <p>
      </p>
      <table class="std_form" style="border-collapse: separate;">
         <tbody>
            <tr>
               <th>Tiêu đề</th>
               <td><input name="title" class="form-control" required="" oninvalid="this.setCustomValidity('Cần nhập tiêu đề công việc')"></td>
            </tr>
            <tr>
               <th>Mục đích</th>
               <td><input name="purpose" class="form-control"></td>
            </tr>
            <tr>
               <th>Địa điểm</th>
               <td><input name="address" class="form-control"></td>
            </tr>
            <tr>
               <th>Bắt đầu</th>
               <td style="display:flex">
                  <input type="date" name="start_date"  value="{{date('Y-m-d')}}" class="form-control" id="start_date_time"/>
                  <input type="time" name="start_time"  value="08:00" id ="start_hour_time" class="form-control" />
               </td>
            </tr>
            <tr>
               <th>Kết thúc</th>
               <td>
                   <div style="display:flex">
                        <input type="date" name="end_date"  value="{{date('Y-m-d')}}" class="form-control" id="end_date_time"/>
                        <input type="time" name="end_time"  value="17:00" id ="end_hour_time" class="form-control"/>
                   </div>
                   <div id="validate_end_date">
                   </div>
               </td>
            </tr>
            <tr>
               <th>Giao cho</th>
               <td>
                   <select class="form-control select-search" name="member_id[]" multiple="multiple" data-placeholder="Chọn nhân viên" required="" oninvalid="this.setCustomValidity('Cần chọn người nhận việc')">
                        {!!$member_html!!}
                   </select>
               </td>
            </tr>
            <tr>
               <th>Mức độ</th>
               <td>
                  <select class="form-control select" name="priority" data-placeholder="Tất cả">
                        {!!$priority_html!!}
                   </select>
               </td>
            </tr>
            <tr valign="top">
               <th>Nội dung</th>
               <td><textarea name="content" class="form-control" cols="50" rows="5"></textarea></td>
            </tr>
            <tr valign="top">
               <th>Ghi chú</th>
               <td><textarea name="note" class="form-control" cols="50" rows="5"></textarea></td>
            </tr>
            <tr valign="top">
               <th nowrap>File đính kèm</th>
               <td>
                  <div id="html5_content">
                     <div style="margin-top:3px;">
                        <div id="drop_" class="drop">
                           Drop files here.
                        </div>
                        <div class="file_input_div">
                           Attach files
                           <input type="file" class="file_html5 file_input_hidden" name="file[]" size="40" id="file_upload" multiple style='display:inline-block;'>
                        </div>
                     </div>
                     <div class="clear_both_0px"></div>
                     <div id="upload_message" >
                     </div>
                     <table id="upload_table" class="attachment_list_base_grn">
                        <tbody id='list_file_upload'>
                           <tr>
                              <td></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="clear_both_0px"></div>
               </td>
            </tr>
            <tr>
               <td></td>
               <td>
                    <div class="mTop10">
                      <span id="todo_button_add" class="button_grn_js button1_main_grn  button1_r_margin2_grn">
                          <button class="btn btn-primary btn-button" id="btn_submit" type="submit">Thêm</button>
                      </span>
                      <span id="todo_button_cancel" class="button_grn_js button1_normal_grn">
                          <a href="{{route('frontend.todo.index')}}" role="button">Thoát</a>
                      </span>
                    </div>
               </td>
            </tr>
         </tbody>
      </table>
   </form>
</div>
<div class="overlay"></div>
@stop
@section('script')
@parent
<script>
   $(document).on({
      ajaxStart: function(){
         $("body").addClass("loading"); 
      },
      ajaxStop: function(){ 
         $("body").removeClass("loading"); 
      }    
   });
    $('.select-search-member').select2({
      allowClear: true
    });
    $(document).on('change', '#file_upload', function () {
          var file_data = $(this).prop('files');
          var form_data = new FormData();
          for(let i=0;i<file_data.length;i++){
            form_data.append('file[]', file_data[i]);
          }
          form_data.append('type',{{\App\File::TYPE_TODO}});
          $.ajax({
                url: '/api/upload',
                type: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == 'true'){
                        if(response.success == 'true'){
                            response.data.forEach(element =>
                            $('#list_file_upload').append(`<tr>
                                                                <td>
                                                                    <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                </td>
                                                                <td>`+element.name+` (`+element.size+` Mb)</td>
                                                            </tr>`)
                            )
                        }
                    }
                }
           });
     })
     $('#end_date_time,#end_hour_time').change(function(){
         var start_date = new Date($('#start_date_time').val()+' '+ $('#start_hour_time').val());
         var end_date = new Date($('#end_date_time').val()+' '+$('#end_hour_time').val());
         if(start_date.getTime() > end_date.getTime()){
             $('#validate_end_date').html('<span class="attention margin_top sub_explanation" id="validate_subject" style="display: block;">Thời gian kết thúc phải lớn hơn thời gian bắt đầu</span>');
             $("#btn_submit").prop('disabled', true);
         }else{
             $('#validate_end_date').html('');
             $("#btn_submit").prop('disabled', false);
         }
     })
</script>
@stop
