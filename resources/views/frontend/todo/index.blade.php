@extends('frontend.layouts.create_schedule')
@section('content')
<link href="{!!asset('assets2/css/paginate.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/progress.css')!!}" rel="stylesheet">
<style>
    .tooltip {
        opacity: 1;
        position: relative;
        display: inline-block;
        vertical-align: middle;
      }

    .tooltip .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: #555;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
      position: absolute;
      z-index: 1;
      bottom: 125%;
      left: 50%;
      margin-left: -60px;
      opacity: 0;
      transition: opacity 0.3s;
    }

    .tooltip .tooltiptext::after {
      content: "";
      position: absolute;
      top: 100%;
      left: 50%;
      margin-left: -5px;
      border-width: 5px;
      border-style: solid;
      border-color: #555 transparent transparent transparent;
    }

    .tooltip:hover .tooltiptext {
      visibility: visible;
      opacity: 1;
    }
</style>
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
    <tbody>
        <tr>
           <td width="100%" valign="bottom" nowrap="">
              <div class="global_naviAppTitle-grn">
                 <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">Danh sách công việc
              </div>
              <div class="global_navi-viewChange-grn">
                 <ul>
                    <li role="heading" aria-level="2" class="global_naviBack-viewChangeSelect-grn"><span>Chưa hoàn thành</span></li>
                    <li><a class="global_naviBackTab-viewChange-grn viewChangeLeft-grn" href="{{route('frontend.todo.history')}}">Hoàn thành</a></li>
                    <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.todo.statistical')}}">Thống kê</a></li>
                 </ul>
              </div>
           </td>
           <td align="right" valign="bottom" nowrap="">
                 <div id="smart_main_menu_part" class="mr-3">
                     <span class="menu_item">
                         <a href="{!!route('frontend.todo.create')!!}">
                         <img src="{{asset('/img/write20.png')}}" border="0" alt="">Tạo mới</a>
                     </span>
                     &nbsp;
                 </div>
           </td>
        </tr>
     </tbody>
</table>
<div>
    <div style="padding:15px;">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        {{trans('base.Search_filters')}}
                        <i class="icon-circle-down2"></i>
                    </button>
                </h5>
            </div>
            <div id="collapseOne" class="collapse @if(count($_GET) > 0) show @endif" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <form id="search_project" method='GET' action='{{route('frontend.todo.index')}}'>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control" name="title" value='@if(isset($_GET['title'])){{$_GET['title']}}@endif' placeholder="Tên công việc">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control select" name="filter_result" data-placeholder="Loại công việc">
                                            <option ></option>
                                            <option value="give" @if(isset($_GET['filter_result']) && $_GET['filter_result'] == "give")) selected @endif>Người giao việc</option>
                                            <option value="receive" @if(isset($_GET['filter_result']) && $_GET['filter_result'] == "receive")) selected @endif>Người nhận việc</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control select" name="priority" data-placeholder="Chọn mức độ">
                                            {!!$priority_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control" placeholder="Ngày bắt đầu" onfocus="(this.type='date')" type="text" name="start_date" value='@if(isset($_GET['start_date'])){{$_GET['start_date']}}@endif'>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control" placeholder="Ngày kết thúc" onfocus="(this.type='date')" type="text" name="end_date" value='@if(isset($_GET['end_date'])){{$_GET['end_date']}}@endif'>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="row">
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary btn-search">{{trans('base.Search')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <table class="table datatable-todo table-bordered" style="margin-top:5px;padding-right: 10px;">
            <thead class="thead-dark">
                <tr style="box-shadow: none;">
                    <th>STT</th>
                    <th>Công việc</th>
                    <th>Thành viên tham gia</th>
                    <th>Người tạo</th>
                    <th>Ngày kết thúc</th>
                    <th>Trạng thái</th>
                    <th class='text-center'>Tiến độ</th>
                    <th>Mức độ</th>
                    <th>Tác vụ</th>
                </tr>
            </thead>
            <tbody id='records_list_project'>
                @foreach($records as $key=>$record)
                <tr>
                    <td  class="middle">{{$key + 1}}</td>
                    <td  class="middle" style="white-space: normal">
                        <a href="{{route('frontend.todo.view',$record->id)}}">{{$record->title}}</a>
                    </td>
                    <td  class="middle" style="white-space: normal">
                        @foreach($record->member as $key=>$val)
                            @if($val->pivot->join == 1)
                                <span class="badge badge-primary">{{$val->full_name}}</span>
                                @elseif($val->pivot->join == 0)
                                    <span class="badge badge-secondary">{{$val->full_name}}</span>
                                @else
                                    <button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="{{$val->pivot->reason}}" >{{$val->full_name}}</button>
                            @endif
                        @endforeach
                    </td>
                    <td class="middle">{{$record->creator ? $record->creator->full_name : ''}}</td>
                    <td class="middle">{{$record->end_date()}}</td>
                    <td class="middle">
                        <span class="badge badge-color{{$record->status}}">{{$record->nameStatus()}}</span>
                        @if($record->confirm == 2)
                        <div class="tooltip"><i class="fas fa-question-circle"></i>
                            <span class="tooltiptext">Người giao việc đánh giá chưa hoàn thành</span>
                        </div>
                        @endif
                    </td>
                    <td class="middle text-center">
                        <a href="javacript:void(0)" data-id="{{$record->id}}" class="show-progress">
                            <div class="progress-circle progress-{{number_format($record->progress())}}"><span>{{number_format($record->progress())}}</span></div>
                        </a>
                    </td>
                    <td class="middle"><span class="badge badge-priority{{$record->priority}}">{{$record->namePriority()}}</span></td>
                    <td>
                        @if($record->check == true)
                            <a href="javascript:void(0)" class="change-status" data-id="{{$record->id}}"><i class="icon-file-check"></i></a>
                        @endif
                        @if(\Auth::guard('member')->user()->id == $record->created_by)
                            @if($record->status != \App\Todo::STATUS_COMPLETE)
                                <a href="{{route('frontend.todo.edit',$record->id)}}"><i class="icon-pencil"></i></a>
                            @endif
                            <form action="{{route('frontend.todo.destroy',['id' => $record->id])}}" method="POST" style="display: inline-block">
                                {!! method_field('DELETE') !!}
                                {!! csrf_field() !!}
                                <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-action="delete">
                                    <i class="icon-bin"></i>
                                </a>
                            </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!--end of maincontents_list-->
</div>
<div class="modal fade" id="modal_change_status_todo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">CẬP NHẬT TIẾN ĐỘ CÔNG VIỆC</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" action="{{route('frontend.todo.changeStatus')}}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='id' id="todo_id">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <select class="from-control select" id="select_status" name="progress" data-placeholder="Chọn tiến độ">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn btn-success' style="height: 100%;"> Lưu </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/datatables.min.js') !!}"></script>
<script src="{!! asset('assets2/js/datatables_basic.js') !!}"></script>
<script>
$(document).ready(function(){
    $('.popover-dismiss').popover({
        trigger: 'focus'
    })
});
$('.select-fillter').change(function(e){
    e.preventDefault();
    $.ajax({
        url:'/api/get-todo',
        method:'POST',
        data: $('#form_search').serialize(),
        success:function(response){
            $('#records_list_project').html(response.result);
        }
    })
})
$('body').delegate('.change-status', 'click', function(){
    var id = $(this).data('id');
    $.ajax({
        url:'/api/getSelectStatus',
        method:'POST',
        data:{id:id},
        success: function(response){
            $('#select_status').html(response.html);
            $('#todo_id').val(id);
            $('#modal_change_status_todo').modal('show');
        }
    })
})
$('[data-action="delete"]').click(function (e) {
    var elm = this;
    bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
    if (result === true) {
    $(elm).parents('form').submit();
    }
    });
});
$('.')
</script>
@stop
