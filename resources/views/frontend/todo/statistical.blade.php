@extends('frontend.layouts.create_schedule')
@section('content')
<style>
    .table-responsive::-webkit-scrollbar
    {
        width: 5px;
        height: 8px;
        background-color: #F5F5F5;
    }
</style>
<link rel="stylesheet" href="{!!asset('assets2/dashboard/css/master_style.css')!!}">
<link href="{!!asset('assets2/css/progress.css')!!}" rel="stylesheet">
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
    <tbody>
        <tr>
             <td width="100%" valign="bottom" nowrap="">
                 <div class="global_naviAppTitle-grn">
                    <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">Danh sách công việc
                 </div>
                 <div class="global_navi-viewChange-grn">
                    <ul>
                       <li><a class="global_naviBackTab-viewChange-grn viewChangeLeft-grn" href="{{route('frontend.todo.index')}}">Chưa hoàn thành</a></li>
                       <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.todo.history')}}">Hoàn thành</a></li>
                       <li role="heading" aria-level="2" class="global_naviBack-viewChangeSelect-grn"><span>Thống kê</span></li>
                    </ul>
                 </div>
             </td>
             <td align="right" valign="bottom" nowrap="">
                 <div id="smart_main_menu_part" class="mr-3">
                     <span class="menu_item">
                         <a href="{!!route('frontend.todo.create')!!}">
                         <img src="{{asset('/img/write20.png')}}" border="0" alt="">Tạo mới</a>
                     </span>
                     &nbsp;
                 </div>
             </td>
        </tr>
    </tbody>
</table>
<div class="content-boxed mx-5" style="border-top:0px">
    <div class="row">
        <div class="col-xl-12 col-md-12 col-12 mb-4 mt-1 py-4 d-flex border-bottomborder-muted">
            <form id='form_search' style="display: flex;width:100%">
                <input type="hidden" name="time" id="choose_time" value="month">
                <div class="mr-5">
                    <div class="d-flex align-items-center person-search">
                        <input type="radio" id="radio_member" @if(!$_GET || ($_GET && $_GET['search'] == 'personal')) checked @endif name="search" value='personal'>
                        <h6 class="mb-0 ml-2">Tìm kiếm theo cá nhân</h6>
                        <i class="icon-user text-warning ml-2"></i>
                    </div>
                    <select class='form-control select select-fillter' name='member_id' data-placeholder='Chọn thành viên' id='select_member' @if($_GET && $_GET['search'] == 'department') disabled="disabled" @endif>
                        {!!$member_html!!}
                    </select>
                </div>
                <div class="ml-5">
                    <div class="d-flex align-items-center group-search">
                        <input type="radio" id="radio_team" name="search" value="department" @if($_GET && $_GET['search'] == 'department') checked @endif>
                        <h6 class="mb-0 ml-2">Tìm kiếm theo tổ nhóm</h6>
                        <i class="icon-users text-warning ml-2"></i>
                    </div>
                    <select class='form-control select select-fillter' name='department_id' data-placeholder='Chọn nhóm' id='select_team' @if(!$_GET || ($_GET && $_GET['search'] == 'personal')) disabled="disabled" @endif>
                        {!!$department_html!!}
                    </select>
                </div>
            </form>
        </div>
        <div class="col-xl-3 col-md-6 col-12">
            <div class="small-box pull-up bg-info">
                <div class="inner">
                <h3 class="text-white" id="count_week">{{$count_week}}</h3>
                <p>Số công việc được giao trong tuần</p>
                </div>
                <div class="icon">
                    <i class="icon-calendar text-white" style="font-size: 50px"></i>
                </div>
                <a href="#" class="small-box-footer"> </a>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 col-12">
            <div class="small-box pull-up bg-danger">
                <div class="inner">
                <h3 class="text-white" id="count_month">{{$count_month}}</h3>
                <p>Số công việc được giao trong tháng</p>
                </div>
                <div class="icon">
                    <i class="icon-calendar52 text-white" style="font-size: 50px"></i>
                </div>
                <a href="#" class="small-box-footer"> </a>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 col-12">
            <div class="small-box pull-up bg-warning">
                <div class="inner">
                  <h3 class="text-white" id="count_quater">{{$count_quarter}}</h3>
                  <p>Số công việc được giao trong quý</p>
                </div>
                <div class="icon">
                    <i class="icon-calendar2 text-white" style="font-size: 50px"></i>
                </div>
                <a href="#" class="small-box-footer"> </a>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 col-12">
            <div class="small-box pull-up bg-success">
                <div class="inner">
                  <h3 class="text-white" id="count_year">{{$count_year}}</h3>
                  <p>Số công việc được giao trong năm</p>
                </div>
                <div class="icon">
                    <i class="icon-calendar3 text-white" style="font-size: 50px"></i>
                </div>
                <a href="#" class="small-box-footer"> </a>
            </div>
        </div>
        <div class="col-xl-4 col-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Biểu đồ thống kê toàn bộ</h4>
                    <select class="form-control select select-filler-chart">
                        <option value="week">Theo tuần</option>
                        <option value="month" selected="">Theo tháng</option>
                        <option value="quater">Theo quý</option>
                        <option value="year">Theo năm</option>
                    </select>
                </div>
                <div class="box-body p-0">
                    <div id="donut"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title" id="title_todo">Các công việc chưa hoàn thành</h4>
                </div>
                <div class="box-body p-15">
                    <div class="table-responsive">
                        <div id="tickets_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"></div>
                                <div class="col-sm-12 col-md-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="tickets" class="table mt-0 table-hover no-wrap table-bordered dataTable no-footer" data-page-size="6" role="grid" aria-describedby="tickets_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="tickets" rowspan="1" colspan="1" aria-sort="ascending" aria-label="STT: activate to sort column descending">STT</th>
                                                <th class="sorting" tabindex="0" aria-controls="tickets" rowspan="1" colspan="1" aria-label="Công việc: activate to sort column ascending">Công việc</th>
                                                <th class="sorting" tabindex="0" aria-controls="tickets" rowspan="1" colspan="1" aria-label="Thành viên tham gia: activate to sort column ascending">Thành viên tham gia</th>
                                                <th class="sorting" tabindex="0" aria-controls="tickets" rowspan="1" colspan="1" aria-label="Trạng thái: activate to sort column ascending">Trạng thái</th>
                                                <th class="sorting" tabindex="0" aria-controls="tickets" rowspan="1" colspan="1" aria-label="Tiến độ: activate to sort column ascending">Tiến độ</th>
                                                <th class="sorting" tabindex="0" aria-controls="tickets" rowspan="1" colspan="1" aria-label="Mức độ: activate to sort column ascending">Mức độ</th>
                                            </tr>
                                        </thead>
                                        <tbody id="list_todo">
                                            @foreach($list_todo as $key=>$todo)
                                            <tr role="row">
                                                <td class="sorting_1">{{$key+1}}</td>
                                                <td><a href="{{route('frontend.todo.view',$todo->id)}}">{{$todo->title}}</a></td>
                                                <td class="flex-column h-100">
                                                    @foreach($todo->member as $key=>$val)
                                                    @if($val->pivot->join == 1)
                                                    <span class="badge badge-primary">{{$val->full_name}}</span>
                                                    @elseif($val->pivot->join == 0)
                                                    <span class="badge badge-secondary">{{$val->full_name}}</span>
                                                    @else
                                                    <button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="{{$val->pivot->reason}}" >{{$val->full_name}}</button>
                                                    @endif
                                                    @endforeach
                                                </td>
                                                <td><span class="badge badge-color{{$todo->status}}">{{$todo->nameStatus()}}</span></td>
                                                <td><div class="progress-circle progress-{{number_format($todo->progress())}}"><span>{{number_format($todo->progress())}}</span></div></td>
                                                <td><span class="badge badge-priority{{$todo->priority}}">{{$todo->namePriority()}}</span></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- jQuery 3 -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js')}}"></script>
<!-- fullscreen -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/screenfull/screenfull.js')}}"></script>
<!-- jQuery ui -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/jquery-ui/jquery-ui.js')}}"></script>
<!-- popper -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/popper/dist/popper.min.js')}}"></script>
<!-- Bootstrap 4.0-->

<!-- Slimscroll -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- apexcharts -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/apexcharts-bundle/irregular-data-series.js')}}"></script>
<script src="{{asset('assets2/dashboard/assets/vendor_components/apexcharts-bundle/dist/apexcharts.js')}}"></script>
<!-- This is data table -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/datatable/datatables.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('assets2/dashboard/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js')}}"></script>
<!-- Bx-code admin dashboard demo (This is only for demo purposes) -->
<script src="{{asset('assets2/dashboard/js/pages/todolist-chart.js')}}"></script>
<script>
    var optionDonut = {
        colors : ['#e4ae0f','#007bff','#037f03','#ff4a52'],
        chart: {
                type: 'donut',
                width: '100%',
                events: {
                    dataPointSelection: function(event, chartContext, config) {
                        var time = $('.select-filler-chart').val();
                        var search = $('input[type="radio"]:checked').val();
                        if(search == 'department'){
                            var id = $('select[name="department_id"]').val();
                        }else{
                            var id = $('select[name="member_id"]').val();
                        }
                        var pattern = config.dataPointIndex;
                        $.ajax({
                            url:'/api/get-list-todo',
                            method:'POST',
                            data:{time:time,search:search,id:id,pattern:pattern},
                            success:function(response){
                                $('#title_todo').html(response.title);
                                $('#tickets').DataTable().destroy();
                                $('#list_todo').html(response.list_todo_html);
                                $('#tickets').DataTable({
                                            "bPaginate": true,
                                            "bLengthChange": false,
                                            "bFilter": false,
                                            "bInfo": false,
                                            "bAutoWidth": false
                                });
                            }
                        })
                    }
                }
        },
        dataLabels: {
              enabled: true,
        },
        plotOptions: {
              pie: {
                donut: {
                      size: '55%',
                },
                offsetY: 20,
              },
              stroke: {
                colors: undefined
              }
        },
        series: [{{implode(',',$status_todo)}}],
        labels: ['Chưa hoàn thành','Hoàn thành trước hạn', 'Hoàn thành đúng hạn','Hoàn thành sau hạn'],
        legend: {
              position: 'bottom',
              offsetY: 0
        }
      }

    var donut = new ApexCharts(
        document.querySelector("#donut"),
        optionDonut
    )
    donut.render();
    $(document).ready(function(){
        $('.popover-dismiss').popover({
            trigger: 'focus'
        })
    });
    var table = $('#tickets').DataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "bAutoWidth": false
                });
    $('.select-fillter').change(function(e){
        e.preventDefault();
        $.ajax({
            url:'/api/get-statistical-todo',
            method:'POST',
            data:$('#form_search').serialize(),
            success:function(response){
                $('#count_week').html(response.count_week);
                $('#count_month').html(response.count_month);
                $('#count_quater').html(response.count_quater);
                $('#count_year').html(response.count_year);
                $('#tickets').DataTable().destroy();
                $('#list_todo').html(response.list_todo_html);
                $('#tickets').DataTable({
                            "bPaginate": true,
                            "bLengthChange": false,
                            "bFilter": false,
                            "bInfo": false,
                            "bAutoWidth": false
                        });
                donut.updateSeries(response.status_todo);
            }
        })
    })
    $('.select-filler-chart').change(function(){
        var time = $(this).val();
        var search = $('input[type="radio"]:checked').val();
        if(search == 'department'){
            var id = $('select[name="department_id"]').val();
        }else{
            var id = $('select[name="member_id"]').val();
        }
        $('#choose_time').val(time);
        $.ajax({
            url:'/api/get-chart-todo',
            method:'POST',
            data:{time:time,search:search,id:id},
            success:function(response){
                donut.updateSeries(response.status_todo)
            }
        })
    })
   $('input[type="radio"]').click(function(){
        if ($('#radio_member').is(':checked'))
        {
          $('#select_member').prop('disabled',false);
          $('#select_team').prop('disabled', 'disabled');
        }else{
          $('#select_member').prop('disabled','disabled');
          $('#select_team').prop('disabled', false);
        }
    });
</script>
@include('frontend.layouts.__footer')
@stop
