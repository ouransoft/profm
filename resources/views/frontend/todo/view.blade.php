@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div role="heading" aria-level="2">
               <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">To-Do List (To-Dos)
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea ">
   <div id="main_menu_part">
      <span class="float_left nowrap-grn">
         @if(isset($membertodo) && $membertodo->join == 0)
         <span class="menu_item"><span class="nowrap-grn "><a href="{{route('frontend.todo.join',$record->id)}}"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt="">Join Todo</a></span></span>
         <span class="menu_item"><span class="nowrap-grn "><a href="javascipt:void(0)" class="unjoin-todo" data-toggle="modal" data-target="#modal_unjoin_todo"><img src="{{asset('/img/out_schedule20.gif')}}" border="0" alt="">Unjoin Todo</a></span></span>
         @endif
         @if($check == true)
         <a href="javascript:void(0)" class="change-status" data-id="{{$record->id}}" style="color:#333;"><i class="icon-file-check"></i>  Cập nhật tiến độ công việc</a>
         @endif
         @if($record->created_by == \Auth::guard('member')->user()->id)
         @if($record->status != \App\Todo::STATUS_COMPLETE )
         <span class="menu_item"><span class="nowrap-grn "><a href="{{route('frontend.todo.edit',$record->id)}}"><img src="{{asset('/img/modify20.gif')}}" border="0" alt="">Edit</a></span></span>
         @endif
         <span class="menu_item">
             <span class="nowrap-grn ">
                <form action="{{route('frontend.todo.destroy',['id' => $record->id])}}" method="POST" style="display: inline-block">
                    {!! method_field('DELETE') !!}
                    {!! csrf_field() !!}
                    <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-action="delete">
                        <i class="icon-bin"></i>  Delete
                    </a>              
                </form>
            </span>
         </span>
         @endif
      </span>
      <div class="clear_both_0px"></div>
   </div>
   <!--menubar_end-->
   <table class="view_table" width="80%">
      <colgroup>
         <col width="20%">
         <col width="80%">
      </colgroup>
      <tbody>
         <tr>
            <th>Tiêu đề</th>
            <td id="todo-title">{{$record->title}}</td>
         </tr>
         <tr>
            <th>Mục đích</th>
            <td id="todo-title">{{$record->purpose}}</td>
         </tr>
         <tr>
            <th>Bắt đầu</th>
            <td id="todo-limit">{{$record->start_date()}}</td>
         </tr>
         <tr>
            <th>Kết thúc</th>
            <td id="todo-limit">{{$record->end_date()}}</td>
         </tr>
         <tr>
            <th>Trạng thái</th>
            <td>
                <span class="badge badge-color{{$record->status}}">{{$record->nameStatus()}}</span>
            </td>
         </tr>
         <tr>
            <th>Mức độ</th>
            <td>
                <span class="badge badge-priority{{$record->priority}}">{{$record->namePriority()}}</span>
            </td>
         </tr>
         @if($record->member())
         <tr>
            <th>Thành viên</th>
            <td>
                @foreach($record->member as $key=>$val)
                   @if($val->pivot->join == 1)
                   <span class="badge badge-primary">{{$val->full_name}}</span>
                   @elseif($val->pivot->join == 0)
                   <span class="badge badge-secondary">{{$val->full_name}}</span>
                   @else
                   <button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="{{$val->pivot->reason}}" >{{$val->full_name}}</button>
                   @endif
                 @endforeach
            </td>
         </tr>
         @endif
         <tr valign="top">
            <th>Nội dung</th>
            <td id="todo-memo">
               <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;">{{$record->content}}</pre>
            </td>
         </tr>
         <tr valign="top">
            <th>Ghi chú</th>
            <td id="todo-memo">
               <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;">{{$record->note}}</pre>
            </td>
         </tr>
         <tr valign="top">
            <th>Tệp đính kèm</th>
            <td id="todo-memo">
                @if(count($record->files) > 0) 
                    @foreach($record->files as $key=>$value)
                        <a href="{{asset($value->link)}}" class='d-block'><img src="{{asset('/img/disk20.gif')}}" border="0" alt=""> {{$value->name}} ({{$value->size}} Mb)</a>
                    @endforeach
                @endif
            </td>
         </tr>
      </tbody>
   </table>
   <div class="ui feed">
        <div class="ui comments">
            @foreach($comments as $key=>$comment)
            <div class="comment" id="comment{{$comment->id}}">
                   <div class="ui icon basic tiny buttons hide"><a class="ui button delete-comment" data-comment_id="{{$comment->id}}" href="javascript:void(0)"><i class="icon-cross"></i></a></div>
                   <a class="avatar"><img class="ui avatar image" src="{{is_null($comment->member->avatar) ? asset('img/user30.png') : $comment->member->avatar}}"></a>
                   <div class="content">
                      <a class="author">{{$comment->member->full_name}}</a>
                      <div class="metadata">
                         <div class="date">{{\App\Helpers\StringHelper::time_ago($comment->created_at)}}</div>
                      </div>
                      <div class="text">{{$comment->comment}}</div>
                      <div class="extra images"></div>
                   </div>
           </div>
           @endforeach
           <div class="comment">
               <a class="avatar">
                    <img class="ui avatar image" src="{{asset('img/user30.png')}}">
               </a>
               <div class="content">
                   <div>
                       <div class="pull-right" style="width:50px;padding-left:5px;">
                            <input type="button" style="display:none;" class="ui button mini" value="Gửi">
                       </div>
                       <div style="margin-right:50px;">
                           <textarea name="content" id="projecttask_comment_61549" placeholder="Viết bình luận..."></textarea>
                           <input type="file" id="ffile-61549" name="ffile" multiple="" class="hide">
                       </div>
                   </div>
               </div>
           </div>
        </div>
   </div>
</div>
<div class="modal fade" id="modal_unjoin_todo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">LÝ DO<span class="orange"> KHÔNG THAM GIA</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" action="{{route('frontend.todo.unjoin')}}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='id' value='{{$record->id}}'>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='reason' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn submit-return-project' style="height: 100%;"> GỬI </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_change_status_todo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">CẬP NHẬT TIẾN ĐỘ CÔNG VIỆC</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" action="{{route('frontend.todo.changeStatus')}}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='id' id="todo_id">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <select class="from-control select" id="select_status" name="progress" data-placeholder="Chọn tiến độ">
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn btn-success' style="height: 100%;"> Lưu </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $(document).ready(function(){
     $('.popover-dismiss').popover({
      trigger: 'focus'
    })
   });
    $('textarea[name="content"]').keypress(
        function(e){
            var comment = $(this).val();
            var todo_id = {{$record->id}}
            if (e.keyCode == 13) {
                if ($(this).index() == 0) {
                    $.ajax({
                        url:'/api/send-comment',
                        method:'POST',
                        data:{comment:comment,todo_id:todo_id},
                        success: function(response){
                            $('textarea').val('');
                            $('.ui.comments').prepend(response.html);
                        }
                    })
                }
                else {
                    // code for others
                }
            }
    });
    $('.delete-comment').click(function(){
        var comment_id = $(this).data('comment_id');
        $.ajax({
            url:'/api/delete-comment',
            method:'POST',
            data:{comment_id:comment_id},
            success: function(response){
                $('#'+response.id).remove();
            }
        })
    })
    $('body').delegate('.change-status','click',function(){
        var id = $(this).data('id');
        $.ajax({
                url:'/api/getSelectStatus',
                method:'POST',
                data:{id:id},
                success: function(response){
                    $('#select_status').html(response.html);
                    $('#todo_id').val(id);
                    $('#modal_change_status_todo').modal('show');
                }
            })
    })
     $('[data-action="delete"]').click(function (e) {
        var elm = this;
        bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
            if (result === true) {
                $(elm).parents('form').submit();
            }
        });
    });
</script>
@stop