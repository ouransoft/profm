@extends('frontend.layouts.create_schedule')
@section('content')
<div class="content-boxed">
<link href="{!!asset('assets2/css/paginate.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/progress.css')!!}" rel="stylesheet">
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
    <tbody>
        <tr>
            <td width="100%" valign="bottom" nowrap="">
                <div class="global_naviAppTitle-grn">
                    <img src="{{asset('/img/todo20.gif')}}" border="0" alt="">Danh sách công việc
                </div>
                <div class="global_navi-viewChange-grn">
                    <ul>
                        <li><a class="global_naviBackTab-viewChange-grn viewChangeLeft-grn" href="{{route('frontend.todo.index')}}">Chưa hoàn thành</a></li>
                        <li role="heading" aria-level="2" class="global_naviBack-viewChangeSelect-grn"><span>Hoàn thành</span></li>
                        <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.todo.statistical')}}">Thống kê</a></li>
                    </ul>
                </div>
            </td>
            <td align="right" valign="bottom" nowrap="">
                <div id="smart_main_menu_part" class="mr-3">
                    <span class="menu_item">
                        <a href="{!!route('frontend.todo.create')!!}">
                        <img src="{{asset('/img/write20.png')}}" border="0" alt="">Tạo mới</a>
                    </span>
                    &nbsp;
                </div>
            </td>
        </tr>
    </tbody>
</table>
<div>
    <div style="padding:15px;">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        {{trans('base.Search_filters')}}
                        <i class="icon-circle-down2"></i>
                    </button>
                </h5>
            </div>
            <div id="collapseOne" class="collapse @if(count($_GET) > 0) show @endif" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <form id="search_project" method='GET' action='{{route('frontend.todo.history')}}'>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control" name="title" value='@if(isset($_GET['title'])){{$_GET['title']}}@endif' placeholder="Tên công việc">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control select" name="filter_result" data-placeholder="Loại công việc">
                                            <option ></option>
                                            <option value="give" @if(isset($_GET['filter_result']) && $_GET['filter_result'] == "give")) selected @endif>Người giao việc</option>
                                            <option value="receive" @if(isset($_GET['filter_result']) && $_GET['filter_result'] == "receive")) selected @endif>Người nhận việc</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control select" name="priority" data-placeholder="Chọn mức độ">
                                            {!!$priority_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control" placeholder="Ngày bắt đầu" onfocus="(this.type='date')" type="text" name="start_date" value='@if(isset($_GET['start_date'])){{$_GET['start_date']}}@endif'>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group mb-0">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control" placeholder="Ngày kết thúc" onfocus="(this.type='date')" type="text" name="end_date" value='@if(isset($_GET['end_date'])){{$_GET['end_date']}}@endif'>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="row">
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary btn-search">{{trans('base.Search')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <table class="table datatable-todo table-bordered" style="margin-top:5px;">
            <thead class="thead-dark">
                <tr style="box-shadow: none;">
                    <th>STT</th>
                    <th>Công việc</th>
                    <th>Thành viên tham gia</th>
                    <th>Người tạo</th>
                    <th>Ngày kết thúc</th>
                    <th>Ngày hoàn thành</th>
                    <th>Trạng thái</th>
                    <th class='text-center'>Tiến độ</th>
                    <th>Mức độ</th>
                    <th>Tác vụ</th>
                </tr>
            </thead>
            <tbody id='records_list_project'>
                @foreach($records as $key=>$record)
                <tr>
                    <td  class="middle">{{$key + 1}}</td>
                    <td  class="middle" style="white-space: normal"><a href="{{route('frontend.todo.view',$record->id)}}">{{$record->title}}</a></td>
                    <td  class="middle" style="white-space: normal">
                        @foreach($record->member as $key=>$val)
                            @if($val->pivot->join == 1)
                                <span class="badge badge-primary">{{$val->full_name}}</span>
                                @elseif($val->pivot->join == 0)
                                    <span class="badge badge-secondary">{{$val->full_name}}</span>
                                @else
                                    <button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="{{$val->pivot->reason}}" >{{$val->full_name}}</button>
                            @endif
                        @endforeach
                    </td>
                    <td  class="middle">{{$record->creator->full_name}}</td>
                    <td  class="middle">{{$record->end_date()}}</td>
                    <td  class="middle">{{$record->complete_date()}}</td>
                    <td class="middle"><span class="badge badge-color{{$record->status}}">{{$record->nameStatus()}}</span></td>
                    <td  class="middle text-center" >
                        <div class="progress-circle progress-100"><span>100</span></div>
                    </td>
                    <td class="middle"><span class="badge badge-priority{{$record->priority}}">{{$record->namePriority()}}</span></td>
                    <td class="middle">
                        @if(\Auth::guard('member')->user()->id == $record->created_by)
                            @if($record->status == \App\Todo::STATUS_COMPLETE && ($record->confirm == \App\Todo::CONFIRM_PENDDING || $record->confirm == \App\Todo::CONFIRM_CANCEL) )
                            <a href="{{route('frontend.todo.confirm',$record->id)}}" title="Xác nhận" class="pr-2"><i class="fas fa-check" style="color: green"></i></a>
                            <a href="{{route('frontend.todo.return',$record->id)}}" title="Trả về" class="pr-2"><i class="fas fa-undo-alt" style="color: red"></i></a>
                            @endif
                        @endif
                        <form action="{{route('frontend.todo.destroy',['id' => $record->id])}}" method="POST" style="display: inline-block">
                            {!! method_field('DELETE') !!}
                            {!! csrf_field() !!}
                            <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-action="delete">
                                <i class="icon-bin"></i>
                            </a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/datatables.min.js') !!}"></script>
<script src="{!! asset('assets2/js/datatables_basic.js') !!}"></script>
<script>
$(document).ready(function(){
    $('.popover-dismiss').popover({
        trigger: 'focus'
    })
});
 $('[data-action="delete"]').click(function (e) {
    var elm = this;
    bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
        if (result === true) {
        $(elm).parents('form').submit();
        }
    });
});
</script>
@stop
