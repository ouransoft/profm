<!-- Sidebar Start -->
<aside class="sidebar">
    <!-- Tab Content Start -->
    <div class="tab-content">
        <!-- Chat Tab Content Start -->
        <div class="tab-pane active" id="chats-content">
            <div class="d-flex flex-column h-100">
                <div class="hide-scrollbar h-100" id="chatContactsList">

                    <!-- Chat Header Start -->
                    <div class="sidebar-header sticky-top p-2">
                        <div class="d-flex justify-content-between align-items-center">
                            <!-- Chat Tab Pane Title Start -->
                            <h5 class="font-weight-semibold mb-0">I-VIBO Chats</h5>
                            <!-- Chat Tab Pane Title End -->
                        </div>

                        <!-- Sidebar Header Start -->
                        <div class="sidebar-sub-header d-flex justify-content-between align-items-center">
                            <!-- Sidebar Search Start -->
                            <div class="d-flex align-items-start">
                                <button class="quit-searching">
                                    <ion-icon name="arrow-back-circle" class="back-icon text-primary"></ion-icon>
                                </button>
                                <form class="form-inline">
                                    <div class="input-group">
                                        <input type="text" class="form-control search border-right-0 transparent-bg pr-0" placeholder="Tìm kiếm...">
                                        <div class="input-group-append">
                                            <div class="input-group-text transparent-bg border-left-0" role="button">
                                                <!-- Default :: Inline SVG -->
                                                <svg class="text-muted hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                                </svg>

                                                <!-- Alternate :: External File link -->
                                                <!-- <img class="injectable hw-20" src="./../../assets/media/heroicons/outline/search.svg" alt=""> -->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- Sidebar Search End -->
                            <ul class="nav flex-nowrap">
                                <li class="nav-item list-inline-item mr-0">
                                    <div class="dropdown">
                                        <a class="nav-link text-muted px-1" id="create-group" href="javascript:void(0)" role="button" title="Tạo nhóm chat mới" data-toggle="modal" data-target="#createGroup">
                                            <ion-icon name="add-outline" class="position-absolute" style="left: -5px; top: 11px;"></ion-icon>
                                            <ion-icon name="people-outline" style="font-size: 28px"></ion-icon>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- Sidebar Header End -->
                    </div>
                    <!-- Chat Header End -->
                    <ul class="searching-list pl-2 mr-2 my-1" id="searchUserTab">

                    </ul>
                    <!-- Chat Contact List Start -->
                    <ul class="contacts-list" id="chatContactTab" data-chat-list="">
                        @php
                            $text = "";
                        @endphp
                        @foreach ($all_messages as $key=>$record)
                            @if ($record['type'] == 2)
                                @php
                                    $text = "[ Hình ảnh ]";
                                @endphp
                            @else
                                @php
                                    $text = "file";
                                @endphp
                            @endif
                            @if($record['type'] != 1)
                                <!-- Chat Item Start -->
                                <li class="contacts-item friends @if($key == 0) active @endif" id="group_{{$record['id']}}">
                                    <a class="contacts-link link" data-link="{{$record['id']}}" data-type="1">
                                        <div class="avatar avatar-online">
                                            <img src="{{$record['avatar']}}" alt="avatar">
                                        </div>
                                        <div class="contacts-content">
                                            <div class="contacts-info">
                                                <h6 class="chat-name text-truncate">{{$record['name']}}</h6>
                                                <div class="chat-time">{{$record['time']}}</div>
                                            </div>
                                            <div class="contacts-texts">
                                                @if($record['seen'] == 0)
                                                    <p class="text-dark font-weight-bold">{!!$text!!}</p>
                                                @else
                                                    <p>{!!$text!!}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- Chat Item End -->
                            @else
                                <!-- Chat Item Start -->
                                <li class="contacts-item friends @if($key == 0) active @endif" id='group_{{$record['id']}}'>
                                    <a class="contacts-link link" data-link="{{$record['id']}}" data-type="1">
                                        <div class="avatar avatar-online">
                                            <img src="{{$record['avatar']}}" alt="avatar">
                                        </div>
                                        <div class="contacts-content">
                                            <div class="contacts-info">
                                                <h6 class="chat-name text-truncate">{{$record['name']}}</h6>
                                                <div class="chat-time">{{$record['time']}}</div>
                                            </div>
                                            <div class="contacts-texts">
                                                @if($record['seen'] == 0)
                                                    <p class="text-dark font-weight-bold">{!!$record['message']!!}</p>
                                                @else
                                                    <p>{!!$record['message']!!}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- Chat Item End -->
                            @endif
                        @endforeach
                    </ul>
                    <!-- Chat Contact List End -->
                </div>
            </div>
        </div>
        <!-- Chats Tab Content End -->
    </div>
    <!-- Tab Content End -->
</aside>
<!-- Sidebar End -->
<script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
    var current_group_id = {!!$group_id!!}
    var current_group_type = {!!$group_type!!}
    $('document').ready(function(){
        if(current_group_type == 99){
            $('.chat-footer').css('display','none');
        }
    })
    function scrollToBottomFunc() {
        $('#messageBody').animate({
            scrollTop: $('.message-day').get(0).scrollHeight
        }, 50);
    }
    $('.search').focus(function(){
        $('#searchUserTab').css('display','block');
        $('#chatContactTab').css('display','none');
        $('.quit-searching').css('display','block');
        $('.search').on('input',function(){
            var value = $(this).val();
            $.ajax({
                url: '/api/searching-member',
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: {value:value},
                success: function (data) {
                    if(value == ""){
                        $('#searchUserTab').html('');
                    }else{
                        $('#searchUserTab').html('');
                        $('#searchUserTab').append('<h6 class="text-primary py-2">Kết quả tìm kiếm cho "'+value+'"</h6>');
                        $('#searchUserTab').append('<h5>Nhóm</h5>');
                        $.each(data.group_result, function(key, value){
                            $('#searchUserTab').append('<li class="searching-item">'+
                                                            '<a class="searching-link link py-2 px-3 my-1 border border-muted rounded w-100 d-flex align-items-center text-dark" data-link="'+value['link']+'" data-type="'+value['type']+'">'+
                                                                '<div class="avatar avatar-online mr-2">'+
                                                                    '<img src="'+value['avatar']+'" alt="avatar">'+
                                                                '</div>'+
                                                                '<div class="searching-content">'+
                                                                    '<div class="searching-info">'+
                                                                        '<h5 class="searching-name">'+value['name']+'</h5>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</a>'+
                                                        '</li>');
                        })
                        $('#searchUserTab').append('<h5>Trò chuyện</h5>');
                        $.each(data.member_result, function(key, value){
                            $('#searchUserTab').append('<li class="searching-item">'+
                                                            '<a class="searching-link link py-2 px-3 my-1 border border-muted rounded w-100 d-flex align-items-center text-dark" data-link="'+value['link']+'" data-type="'+value['type']+'">'+
                                                                '<div class="avatar avatar-online mr-2">'+
                                                                    '<img src="'+value['avatar']+'" alt="avatar">'+
                                                                '</div>'+
                                                                '<div class="searching-content">'+
                                                                    '<div class="searching-info">'+
                                                                        '<h5 class="searching-name">'+value['name']+'</h5>'+
                                                                    '</div>'+
                                                                '</div>'+
                                                            '</a>'+
                                                        '</li>');
                        })
                    }
                }
            })
        })
    })
    $('.quit-searching').click(function(){
        $('#searchUserTab').css('display','none');
        $('#chatContactTab').css('display','block');
        $('.quit-searching').css('display','none');
        $('#searchUserTab').html('');
        $('.search').val('');
        var value = "";
        $.ajax({
            url: '/api/searching-member',
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
            },
            data: {value:value},
            success: function (data) {
                $('#searchUserTab').html(data.html);
            }
        })
    })
    $('body').delegate('.link', 'click', function () {
        $('.friends').removeClass('active');
        $(this).parent().addClass('active');
        $('#content_message').val('');
        $('#searchUserTab').html('');
        $('#searchUserTab').css('display','none');
        $('#chatContactTab').css('display','block');
        $('.quit-searching').css('display','none');
        $('.search').val('');
        var link = $(this).data('link');
        var type = $(this).data('type');
        $.ajax({
            url: '/api/get-message',
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
            },
            data: {link:link,type:type},
            success: function (data) {
                if(data.to_type == 1 ){
                    if($('.link[data-link="'+link+'"] .contacts-texts .text-dark').hasClass('font-weight-bold')){
                        $('.link[data-link="'+link+'"] .contacts-texts .text-dark').removeClass('font-weight-bold');
                        $('.link[data-link="'+link+'"] .contacts-texts .text-dark').addClass('text-muted');
                    }
                    current_group_id = data.group_id;
                    if(data.to_member_phone == null){
                        var phone = "Chưa cập nhật";
                    }else{
                        var phone = data.to_member_phone;
                    }
                    if(data.to_member_email == null){
                        var email = "Chưa cập nhật";
                    }else{
                        var email = data.to_member_email;
                    }
                    if(data.to_member_address == null){
                        var address = "Chưa cập nhật";
                    }else{
                        var address = data.to_member_address;
                    }
                    let list_member_html = `<a class="chat-info-group-header" data-toggle="collapse" href="#profile-info" role="button" aria-expanded="true" aria-controls="profile-info">
                                                <h6 class="mb-0">Thông tin</h6>
                                                 <svg class="hw-20 text-muted" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                                </svg>
                                             </a>
                                            <div class="chat-info-group-body collapse show" id="profile-info">
                                                <div class="chat-info-group-content list-item-has-padding">
                                                    <ul class="list-group list-group-flush ">
                                                        <li class="list-group-item border-0">
                                                            <p class="small text-muted mb-0">Số điện thoại</p>
                                                            <p class="mb-0 to-member-phone">`+ phone +`</p>
                                                        </li>
                                                        <li class="list-group-item border-0">
                                                            <p class="small text-muted mb-0">Email</p>
                                                            <p class="mb-0 to-member-email">`+ email +`</p>
                                                        </li>
                                                        <li class="list-group-item border-0">
                                                            <p class="small text-muted mb-0">Địa chỉ</p>
                                                            <p class="mb-0 to-member-address">`+ address +`</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>`;
                    $('.chat-info-group').html(list_member_html);
                   
                }else if(data.group_type != 99){
                    var member_html = '';
                    $.each(data.members, function(key, value){
                        member_html +=` <li class="list-group-item" id="member-item-633">
                                            <div class="media align-items-center">
                                                <div class="avatar mr-2">
                                                    <img src="`+value.avatar+`" alt="`+value.full_name+`">
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="text-truncate">
                                                        <a href="#" class="text-reset">`+value.full_name+`</a>
                                                    </h6>
                                                    <p class="text-muted mb-0">`+value.position_name+`</p>
                                                </div>
                                                <div class="media-options ml-1">
                                                    <div class="dropdown">
                                                        <button class="btn btn-secondary btn-icon btn-minimal btn-sm text-muted" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"></path>
                                                            </svg>
                                                        </button>
                                                        <div class="dropdown-menu">
                       
                                                            <a class="dropdown-item leave-group" href="javascript:void(0)" data-member_id="`+value.id+`" data-group_id="`+data.group_id+`">Mời ra khỏi nhóm</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>`;
                    })
                    let list_member_html = `<a class="chat-info-group-header" data-toggle="collapse" href="#participants-list" role="button" aria-expanded="true" aria-controls="participants-list">
                                                <h6 class="mb-0">Thành viên nhóm</h6>
                                                <svg class="hw-20 text-muted" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"></path>
                                                </svg>
                                              </a>
                                            <div class="chat-info-group-body collapse show" id="participants-list">
                                                <div class="chat-info-group-content list-item-has-padding">
                                                    <ul class="list-group list-group-flush list-group-participant">
                                                        `+member_html+`
                                                    </ul>
                                                    <div class="padding-button"> 
                                                        <div class="btn btn-primary btn-block" id="add-member" data-group_id="`+data.group_id+`" role="button" data-toggle="modal" data-target="#addMember">Thêm thành viên</div>
                                                    </div>
                                                </div>
                                            </div>`;
                    $('.chat-info-group').html(list_member_html);
                }else{
                    $('.chat-info-group').html('');
                }
                if(data.group_type == 99){
                    $('.chat-footer').css('display','none');
                }else{
                    $('.chat-footer').css('display','block');
                }
                $('.current-group-avatar').attr("src",data.avatar);
                $('.group-name').html(data.group_name);
                $('#receiver_id').val(data.group_id);
                $('#type').val(data.type);
                if(data.to_type == 1){
                    $('.member-info').addClass('show');
                }else{
                    $('.member-info').removeClass('show');
                }
                $('.message-day').html('');

                if(data.html.length > 0){
                    var date = data.html[0]['date'];
                    $('.message-day').append('<div class="message-divider sticky-top pb-2" data-label="'+data.html[0]['date']+'">&nbsp;</div>');
                }
                $.each(data.html, function(key, value){
                    if(value['date'] != date){
                        date = value['date'];
                        $('.message-day').append('<div class="message-divider sticky-top pb-2" data-label="'+date+'">&nbsp;</div>');
                    }
                    if(value.id == 0){
                        $('.message-day').append('<h6 class="text-center">'+value.message+'</h6>');
                    }
                    else if(value.id == {!!\Auth::guard('member')->user()->id!!}){
                        var seen_avatar = '';
                        $.each(value.member_seen, function(key, value){
                            seen_avatar += '<img src="'+value["avatar"]+'" alt="avatar" class="avatar-seen" title="'+value["name"]+' đã xem" style="border-radius: 30px; width: 15px; height: 15px; margin-left: 3px">';
                        })
                        if(value.type == 2){
                            $('.message-day').append(`<div class="message self" id="message_`+value.message_id+`">
                                                        <div class="message-wrapper d-flex flex-column align-items-end">
                                                            <div class="form-row w-50 mr-3 mb-1">
                                                                <div class="col">
                                                                    <a class="popup-media" href="/`+value.message+`">
                                                                        <img class="img-fluid rounded" src="/`+value.message+`" alt="image">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="message-options">
                                                            <div class="avatar avatar-sm"><img alt="avatar" src="`+value.avatar+`"></div>
                                                            <span class="message-date">`+value.time+`</span>
                                                            <div class="dropdown">
                                                            <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                                </svg>
                                                            </a>
                                                            <div class="dropdown-menu">
                                                                
                                                                <a class="dropdown-item d-flex align-items-center text-danger delete-message" href="javascript:void(0)" data-id="`+value.message_id+`">
                                                                    <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                                    </svg>
                                                                    <span>Xóa tin</span>
                                                                </a>
                                                            </div>
                                                    </div>
                                                        </div>
                                                        <a href="javascript:void(0)" class="seen-area d-flex justify-content-end mr-4 mt-1">
                                                            `+seen_avatar+`
                                                        </a>
                                                    </div>`);
                        }else if(value.type == 3){
                            $('.message-day').append(`<div class="message self" id="message_`+value.message_id+`">
                                                        <div class="message-wrapper">
                                                            <div class="message-content">
                                                                `+ (value.reply_id > 0 ?
                                                                    `<div class="reply-content">
                                                                        <span class="reply-name">`+value.reply_name+`</span>
                                                                        <span>`+value.reply_message+`</span>
                                                                   </div>` : '') +`
                                                                <span>
                                                                    <div class="document">
                                                                        <div class="btn btn-warning btn-icon rounded-circle text-light mr-2">
                                                                            <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                                                                            </svg>
                                                                        </div>
                                                                        <div class="document-body">
                                                                            <h6>
                                                                                <a href="/`+value.message+`" class="text-reset" download>`+value.file_name+`</a>
                                                                            </h6>
                                                                            <ul class="list-inline small mb-0">
                                                                                <li class="list-inline-item">
                                                                                    <span class="text-warning">`+value.size+`</span>
                                                                                </li>
                                                                                <li class="list-inline-item">
                                                                                    <span class="text-warning text-uppercase">`+value.ext+`</span>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="message-options">
                                                            <div class="avatar avatar-sm"><img alt="avatar" src="`+value.avatar+`"></div>
                                                            <span class="message-date">`+value.time+`</span>
                                                            <div class="dropdown">
                                                                <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                                    </svg>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    
                                                                    <a class="dropdown-item d-flex align-items-center text-danger delete-message" href="javascript:void(0)" data-id="`+value.message_id+`">
                                                                        <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                                        </svg>
                                                                        <span>Xóa tin</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a href="javascript:void(0)" class="seen-area d-flex justify-content-end mr-4 mt-1">
                                                            `+seen_avatar+`
                                                        </a>
                                                    </div>`);
                        }else{
                            $('.message-day').append(`<div class="message self" id="message_`+value.message_id+`">
                                                        <div class="message-wrapper">
                                                            <div class="message-content">
                                                                `+ (value.reply_id > 0 ?
                                                                    `<div class="reply-content">
                                                                        <span class="reply-name">`+value.reply_name+`</span>
                                                                        <span>`+value.reply_message+`</span>
                                                                   </div>` : '') +`
                                                                <span>`+value.message+`</span>
                                                            </div>
                                                        </div>
                                                        <div class="message-options">
                                                            <div class="avatar avatar-sm"><img alt="avatar" src="`+value.avatar+`"></div>
                                                            <span class="message-date">`+value.time+`</span>
                                                            <div class="dropdown">
                                                                <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                                    </svg>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    
                                                                    <a class="dropdown-item d-flex align-items-center text-danger delete-message" href="javascript:void(0)" data-id="`+value.message_id+`">
                                                                        <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                                        </svg>
                                                                        <span>Xóa tin</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a href="javascript:void(0)" class="seen-area d-flex justify-content-end mr-4 mt-1">
                                                            `+seen_avatar+`
                                                        </a>
                                                    </div>`);
                        }
                    }else{
                        var seen_avatar = '';
                        $.each(value.member_seen, function(key, value){
                            seen_avatar += '<img src="'+value["avatar"]+'" alt="avatar" class="avatar-seen" title="'+value["name"]+' đã xem" style="border-radius: 30px; width: 15px; height: 15px; margin-left: 3px">';
                        })
                        if(data.type == 2){
                            $('.message-day').append(`<div class="message" id="message_`+value.message_id+`">
                                                        <div class="message-wrapper d-flex flex-column align-items-start">
                                                            <div class="form-row w-50 ml-3 mb-1">
                                                                <div class="col">
                                                                    <a class="popup-media" href="/`+value.message+`">
                                                                        <img class="img-fluid rounded" src="/`+value.message+`" alt="image">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="message-options">
                                                            <div class="avatar avatar-sm"><img alt="avatar" src="`+value.avatar+`"></div>
                                                            <span class="message-date">`+value.time+`</span>
                                                            <div class="dropdown">
                                                                <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                                    </svg>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item d-flex align-items-center reply-message" href="javascript:void(0)" data-id="`+value.message_id+`" data-message="`+value.message+`" data-name="`+value.from+`">
                                                                        <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6"/>
                                                                        </svg>
                                                                        <span>Trả lời</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a href="javascript:void(0)" class="seen-area d-flex justify-content-end mr-4 mt-1">
                                                            `+seen_avatar+`
                                                        </a>
                                                    </div>`);
                        }else if(data.type == 3){
                            $('.message-day').append(`<div class="message" id="message_`+value.message_id+`">
                                                        <div class="message-wrapper">
                                                            <div class="message-content">
                                                                `+ (value.reply_id > 0 ?
                                                                `<div class="reply-content">
                                                                    <span class="reply-name">`+value.reply_name+`</span>
                                                                    <span>`+value.reply_message+`</span>
                                                                </div>` : '') +`
                                                                <span>
                                                                    <div class="document">
                                                                        <div class="btn btn-warning btn-icon rounded-circle text-light mr-2">
                                                                            <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                                                                            </svg>
                                                                        </div>
                                                                        <div class="document-body">
                                                                            <h6>
                                                                                <a href="/`+value.message+`" class="text-reset" download>`+value.file_name+`</a>
                                                                            </h6>
                                                                            <ul class="list-inline small mb-0">
                                                                                <li class="list-inline-item">
                                                                                    <span class="text-warning">`+value.size+`</span>
                                                                                </li>
                                                                                <li class="list-inline-item">
                                                                                    <span class="text-warning text-uppercase">`+value.ext+`</span>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="message-options">
                                                            <div class="avatar avatar-sm"><img alt="avatar" src="`+value.avatar+`"></div>
                                                            <span class="message-date">`+value.time+`</span>
                                                            <div class="dropdown">
                                                                <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                                    </svg>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item d-flex align-items-center reply-message" href="javascript:void(0)" data-id="`+value.message_id+`" data-message="`+value.message+`" data-name="`+value.from+`">
                                                                        <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6"/>
                                                                        </svg>
                                                                        <span>Trả lời</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a href="javascript:void(0)" class="seen-area d-flex justify-content-end mr-4 mt-1">
                                                            `+seen_avatar+`
                                                        </a>
                                                    </div>`);
                        }else{
                            $('.message-day').append(`<div class="message" id="message_`+value.message_id+`">
                                                        <div class="message-wrapper">
                                                            <div class="message-content">
                                                                <h6 class="text-dark">`+value.from+`</h6>          
                                                                `+ (value.reply_id > 0 ?
                                                                    `<div class="reply-content-self">
                                                                        <span class="reply-name">`+value.reply_name+`</span>
                                                                        <span>`+value.reply_message+`</span>
                                                                   </div>` : '') +`
                                                                <span>`+value.message+`</span>
                                                            </div>
                                                        </div>
                                                        <div class="message-options">
                                                            <div class="avatar avatar-sm"><img alt="avatar" src="`+value.avatar+`"></div>
                                                            <span class="message-date">`+value.time+`</span>
                                                            <div class="dropdown">
                                                                <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                                    </svg>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item d-flex align-items-center reply-message" href="javascript:void(0)" data-id="`+value.message_id+`" data-message="`+value.message+`" data-name="`+value.from+`">
                                                                        <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6"/>
                                                                        </svg>
                                                                        <span>Trả lời</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                         </div>
                                                        <a href="javascript:void(0)" class="seen-area d-flex justify-content-end mr-4 mt-1">
                                                            `+seen_avatar+`
                                                        </a>
                                                    </div>`);
                        }
                    }
                    $('.message-day').scrollTop($('.message-day').height());
                })
            scrollToBottomFunc()
            }
        })
    })
</script>
