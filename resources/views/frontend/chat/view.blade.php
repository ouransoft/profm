@extends('frontend.chat.index')
@section('content')

<!-- Main Start -->
<main class="main main-visible">

    <!-- Chats Page Start -->
    <div class="chats">
        <div class="chat-body">

            <!-- Chat Header Start-->
            <div class="chat-header">
                <!-- Chat Back Button (Visible only in Small Devices) -->
                <button class="btn btn-secondary btn-icon btn-minimal btn-sm text-muted d-xl-none" type="button" data-close="">
                    <!-- Default :: Inline SVG -->
                    <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"/>
                    </svg>
                </button>

                <!-- Chat participant's Name -->
                <div class="media chat-name align-items-center text-truncate">
                    <div class="avatar avatar-online d-none d-sm-inline-block mr-3">
                        <img class="current-group-avatar" src="{{$avatar}}" alt="avatar">
                    </div>

                    <div class="media-body align-self-center ">
                        <h6 class="text-truncate mb-0 group-name">{{$group_name}}</h6>
                        {{-- <small class="text-muted">Online</small> --}}
                    </div>
                </div>

                <!-- Chat Options -->
                <ul class="nav flex-nowrap">
                    <li class="nav-item list-inline-item d-none d-sm-block mr-0">
                        <div class="dropdown">
                            <a class="dropdown-item align-items-center d-flex px-0" href="#" data-chat-info-toggle="">
                                <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                </svg>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- Chat Header End-->

            <!-- Search Start -->
            <div class="collapse border-bottom px-3" id="searchCollapse">
                <div class="container-xl py-2 px-0 px-md-3">
                    <div class="input-group bg-light ">
                        <input type="text" class="form-control form-control-md border-right-0 transparent-bg pr-0" placeholder="Search">
                        <div class="input-group-append">
                            <span class="input-group-text transparent-bg border-left-0">
                                <svg class="hw-20 text-muted" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Search End -->

            <!-- Chat Content Start-->
            <div class="chat-content p-2" id="messageBody" style="overflow-y: scroll">
                <div class="message-scroll">
                    <!-- Message Day Start -->
                    <div class="message-day">
                        <div class="message-divider sticky-top pb-2" data-label="{!!date('d-m-Y',strtotime($messages[0]->created_at))!!}">&nbsp;</div>
                        @php
                            $time = strtotime(date('d-m-Y',strtotime($messages[0]->created_at)));
                        @endphp
                        @foreach ($messages as $message)
                            @if(strtotime(date('d-m-Y',strtotime($message->created_at))) > $time)
                                @php
                                    $time = strtotime(date('d-m-Y',strtotime($message->created_at)))
                                @endphp
                                <div class="message-divider sticky-top pb-2" data-label="{{date('d-m-Y',strtotime($message->created_at))}}">&nbsp;</div>
                            @endif
                            @if ($message->from == 0)
                                <h6 class="text-center">{{$message->message}}</h6>
                            @elseif ($message->from == $user_id)
                                <!-- Self Message Start -->
                                <div class="message self" id="message_{{$message->id}}">
                                    @if ($message->type == 2)
                                        <div class="message-wrapper d-flex flex-column align-items-end">
                                            <div class="form-row w-50 mr-3 mb-1">
                                                <div class="col">
                                                    <a class="popup-media" href="/{!!$message->message!!}">
                                                        <img class="img-fluid rounded" src="/{!!$message->message!!}" alt="image">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($message->type == 3)
                                        <div class="message-wrapper">
                                            <div class="message-content">
                                                @if($message->reply_id > 0)
                                                <div class='reply-content'>
                                                    <span class='reply-name'>{{$message->reply->membersend->full_name}}</span>
                                                    <span>{{$message->reply->message}}</span>
                                                </div>
                                                @endif
                                                <span>
                                                    <div class="document">
                                                        <div class="btn btn-warning btn-icon rounded-circle text-light mr-2">
                                                            <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="document-body">
                                                            <h6>
                                                                <a href="/{!!$message->message!!}" class="text-reset" download>{{$message->file_name}}</a>
                                                            </h6>
                                                            <ul class="list-inline small mb-0">
                                                                <li class="list-inline-item">
                                                                    <span class="text-warning">{{$message->size}}</span>
                                                                </li>
                                                                <li class="list-inline-item">
                                                                    <span class="text-warning text-uppercase">{{$message->extension}}</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="message-wrapper">
                                            <div class="message-content">
                                                @if($message->reply_id > 0)
                                                <div class='reply-content'>
                                                    <span class='reply-name'>{{$message->reply->membersend->full_name}}</span>
                                                    <span>{{$message->reply->message}}</span>
                                                </div>
                                                @endif
                                                <span>{!!$message->message!!}</span>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="message-options">
                                        <div class="avatar avatar-sm"><img alt="avatar" src="{{\App\Member::where('id',$message->from)->first()->avatar}}"></div>
                                        <span class="message-date">{!!date('h:i A',strtotime($message->created_at))!!}</span>
                                        <div class="dropdown">
                                            <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                </svg>
                                            </a>
                                            <div class="dropdown-menu">
                                                @if($message->from != \Auth::guard('member')->user()->id)
                                                <a class="dropdown-item d-flex align-items-center" href="#">
                                                    <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6"/>
                                                    </svg>
                                                    <span>Trả lời</span>
                                                </a>
                                                @endif
                                                <a class="dropdown-item d-flex align-items-center text-danger delete-message" href="javascript:void(0)" data-id="{{$message->id}}">
                                                    <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                    </svg>
                                                    <span>Xóa tin</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" class="seen-area d-flex justify-content-end mr-4 mt-1" data-messageid="{{$message->id}}">
                                        @php
                                            $member_in_message = \App\ReadMessage::where('group_id',$group_id)->where('message_id',$message->id)->where('seen',1)->get();
                                        @endphp
                                        @foreach($member_in_message as $member_record)
                                            <img src="{{\App\Member::where('id',$member_record->member_id)->first()->avatar}}" alt="avatar" class="avatar-seen" title="{!!\App\Member::where('id',$member_record->member_id)->first()->full_name!!} đã xem" style="border-radius: 30px; width: 15px; height: 15px; margin-left: 3px">
                                        @endforeach
                                    </a>
                                </div>
                                <!-- Self Message End -->
                            @else
                                <div class="message" id="message_{{$message->id}}">
                                    @if ($message->type == 2)
                                        <div class="message-wrapper d-flex flex-column align-items-start">
                                            <div class="form-row w-50 ml-3 mb-1">
                                                <div class="col">
                                                    <a class="popup-media" href="/{!!$message->message!!}">
                                                        <img class="img-fluid rounded" src="/{!!$message->message!!}" alt="image">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($message->type == 3)
                                        <div class="message-wrapper">
                                            <div class="message-content">
                                                @if($message->reply_id > 0)
                                                <div class='reply-content'>
                                                    <span class='reply-name'>{{$message->reply->membersend->full_name}}</span>
                                                    <span>{{$message->reply->message}}</span>
                                                </div>
                                                @endif
                                                <span>
                                                    <div class="document">
                                                        <div class="btn btn-warning btn-icon rounded-circle text-light mr-2">
                                                            <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                                                            </svg>
                                                        </div>
                                                        <div class="document-body">
                                                            <h6>
                                                                <a href="/{!!$message->message!!}" class="text-reset" download>{{$message->file_name}}</a>
                                                            </h6>
                                                            <ul class="list-inline small mb-0">
                                                                <li class="list-inline-item">
                                                                    <span class="text-warning">{{$message->size}}</span>
                                                                </li>
                                                                <li class="list-inline-item">
                                                                    <span class="text-warning text-uppercase">{{$message->extension}}</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="message-wrapper">
                                            <div class="message-content">
                                                <h6 class="text-dark">{{\App\GroupMember::where('member_id',$message->from)->where('group_id',$message->group_id)->first() ? \App\GroupMember::where('member_id',$message->from)->where('group_id',$message->group_id)->first()->name : ''}}</h6>
                                                @if($message->reply_id > 0)
                                                <div class='reply-content-self'>
                                                    <span class='reply-name'>{{$message->reply->membersend->full_name}}</span>
                                                    <span>{{$message->reply->message}}</span>
                                                </div>
                                                @endif
                                                <span>{!!$message->message!!}</span>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="message-options">
                                        <div class="avatar avatar-sm"><img alt="avatar" src="{{\App\Member::where('id',$message->from)->first()->avatar}}"></div>
                                        <span class="message-date">{!!date('h:i A',strtotime($message->created_at))!!}</span>
                                        <div class="dropdown">
                                            <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                </svg>
                                            </a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item d-flex align-items-center reply-message" href="javascript:void(0)" data-id="{{$message->id}}" data-message="{{$message->message}}" data-name="{{\App\GroupMember::where('member_id',$message->from)->where('group_id',$message->group_id)->first() ? \App\GroupMember::where('member_id',$message->from)->where('group_id',$message->group_id)->first()->name : ''}}">
                                                    <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6"/>
                                                    </svg>
                                                    <span>Trả lời</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" class="seen-area d-flex justify-content-end mr-4 mt-1" data-messageid="{{$message->id}}">
                                        @php
                                            $member_in_message = \App\ReadMessage::where('group_id',$group_id)->where('message_id',$message->id)->where('member_id','!=',$user_id)->where('seen',1)->get();
                                        @endphp
                                        @foreach($member_in_message as $member_record)
                                            <img src="{{\App\Member::where('id',$member_record->member_id)->first()->avatar}}" alt="avatar" class="avatar-seen" title="{!!\App\Member::where('id',$member_record->member_id)->first()->full_name!!} đã xem" style="border-radius: 30px; width: 15px; height: 15px; margin-left: 3px">
                                        @endforeach
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <!-- Message Day End -->
                </div>

                <!-- Scroll to finish -->
                <div class="chat-finished" id="chat-finished"></div>
            </div>
            <!-- Chat Content End-->

            <!-- Chat Footer Start-->

            <div class="chat-footer">
                <div class="top-chat" style="border-top: 1px solid #e5e9f2;">
                </div>
                <div class="attachment">
                    <div class="dropdown">
                        <button class="btn btn-secondary btn-icon btn-minimal btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"/>
                            </svg>

                            <!-- <img class="injectable hw-20" src="./../../assets/media/heroicons/outline/plus-circle.svg" alt=""> -->
                        </button>
                        <div class="dropdown-menu attach-file">
                            <form method='post' action='' enctype="multipart/form-data">
                                <label class="dropdown-item" for="imageInput">
                                    <svg class="hw-20 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"/>
                                    </svg>
                                    <span>Hình ảnh</span>
                                    <input type="file" class="custom-file-input position-absolute w-0" name="images[]" id="imageInput" accept="image/*" multiple>
                                </label>
                            </form>
                            <form method='post' action='' enctype="multipart/form-data">
                                <label class="dropdown-item" for="documentInput">
                                    <svg class="hw-20 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"/>
                                    </svg>
                                    <span>Tài liệu</span>
                                    <input type="file" class="custom-file-input position-absolute w-0" name="files[]" id="documentInput" accept=".xlsx,.xls,.doc,.docx,.txt,.pdf" multiple>
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
                <input type='hidden' name='type' value='1' id="type" />
                <input type='hidden' name='reply_id' value='0' id="reply_id" />
                <input type='hidden' name='receiver_id' value='{{$group_id}}' id="receiver_id" />
                <textarea class="form-control emojionearea-form-control" id="content_message" rows="1" placeholder="Nhập tin nhắn..."></textarea>

                <div class="btn btn-primary btn-icon send-icon rounded-circle text-light mb-1" role="button">
                    <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3"/>
                    </svg>

                    <!-- <img src="./../../assets/media/heroicons/outline/arrow-right.svg" alt="" class="injectable"> -->
                </div>
            </div>

            <!-- Chat Footer End-->
        </div>

        <!-- Chat Info Start -->
        <div class="chat-info">
            <div class="d-flex h-100 flex-column">

                <!-- Chat Info Header Start -->
                <div class="chat-info-header px-2">
                    <div class="container-fluid">
                        <ul class="nav justify-content-between align-items-center">
                            <!-- Sidebar Title Start -->
                            <li class="text-center">
                                <h5 class="text-truncate mb-0">Thông tin chi tiết</h5>
                            </li>
                            <!-- Sidebar Title End -->

                            <!-- Close Sidebar Start -->
                            <li class="nav-item list-inline-item">
                                <a class="nav-link text-muted px-0" href="javascript:void(0)" data-chat-info-close="">
                                    <svg class="hw-22" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                </a>
                            </li>
                            <!-- Close Sidebar End -->
                        </ul>
                    </div>
                </div>
                <!-- Chat Info Header End  -->

                <!-- Chat Info Body Start  -->
                <div class="hide-scrollbar flex-fill">

                    <!-- User Profile Start -->
                    <div class="text-center p-3">

                        <!-- User Profile Picture -->
                        <div class="avatar avatar-xl mx-5 mb-3">
                            <img class="avatar-img current-group-avatar" src="{{$avatar}}" alt="">
                        </div>

                        <!-- User Info -->
                        <h5 class="mb-1 group-name">{{$group_name}}</h5>
                    </div>
                    <!-- User Profile End -->

                    <!-- Member -->
                    @if($to_type == 0 && $group)
                    <div class="chat-info-group">
                        <a class="chat-info-group-header" data-toggle="collapse" href="#participants-list" role="button" aria-expanded="true" aria-controls="participants-list">
                            <h6 class="mb-0">Thành viên nhóm</h6>
                            <svg class="hw-20 text-muted" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"/>
                            </svg>
                          </a>

                        <div class="chat-info-group-body collapse show" id="participants-list">
                            <div class="chat-info-group-content list-item-has-padding">
                                <!-- List Group Start -->
                                <ul class="list-group list-group-flush list-group-participant">
                                    <!-- List Group Item Start -->
                                    @foreach($group->member as $key=>$member)
                                    <li class="list-group-item" id="member-item-{{$member->id}}">
                                        <div class="media align-items-center">
                                            <div class="avatar mr-2">
                                                <img src="{{$member->avatar}}" alt="{{$member->full_name}}">
                                            </div>
                                            <div class="media-body">
                                                <h6 class="text-truncate">
                                                    <a href="#" class="text-reset">{{$member->full_name}}</a>
                                                </h6>
                                                <p class="text-muted mb-0">{{$member->position ? $member->position->name : 'Nhân viên'}}</p>
                                            </div>

                                            <div class="media-options ml-1">
                                                <div class="dropdown">
                                                    <button class="btn btn-secondary btn-icon btn-minimal btn-sm text-muted" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"/>
                                                        </svg>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                       
                                                        <a class="dropdown-item leave-group" href="javascript:void(0)" data-member_id="{{$member->id}}" data-group_id="{{$group->id}}">Mời ra khỏi nhóm</a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                    <!-- List Group Item End -->
                                </ul>
                                <div class="padding-button"> 
                                    <div class="btn btn-primary btn-block" id="add-member" data-group_id="{{$group->id}}" role="button" data-toggle="modal" data-target="#addMember">Thêm thành viên</div>
                                </div>
                                <!-- List Group End -->
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($to_type == 1)
                    <div class="chat-info-group show">
                        <a class="chat-info-group-header" data-toggle="collapse" href="#profile-info" role="button" aria-expanded="true" aria-controls="profile-info">
                            <h6 class="mb-0">Thông tin</h6>
                             <svg class="hw-20 text-muted" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
                            </svg>
                          </a>

                        <div class="chat-info-group-body collapse show" id="profile-info">
                            <div class="chat-info-group-content list-item-has-padding">
                                <!-- List Group Start -->
                                <ul class="list-group list-group-flush ">

                                    <!-- List Group Item Start -->
                                    <li class="list-group-item border-0">
                                        <p class="small text-muted mb-0">Số điện thoại</p>
                                        <p class="mb-0 to-member-phone">@if($to_type == 1) {{ $to_member_phone = \App\Member::where('id',$to_member_id)->first()->phone }} @if($to_member_phone == "") Chưa cập nhật @endif @endif</p>
                                    </li>
                                    <!-- List Group Item End -->

                                    <!-- List Group Item Start -->
                                    <li class="list-group-item border-0">
                                        <p class="small text-muted mb-0">Email</p>
                                        <p class="mb-0 to-member-email">@if($to_type == 1) {{ $to_member_email = \App\Member::where('id',$to_member_id)->first()->email }} @if($to_member_email == "") Chưa cập nhật @endif @endif</p>
                                    </li>
                                    <!-- List Group Item End -->

                                    <!-- List Group Item Start -->
                                    <li class="list-group-item border-0">
                                        <p class="small text-muted mb-0">Địa chỉ</p>
                                        <p class="mb-0 to-member-address">@if($to_type == 1) {{ $to_member_address = \App\Member::where('id',$to_member_id)->first()->address }} @if($to_member_address == "") Chưa cập nhật @endif @endif</p>
                                    </li>
                                    <!-- List Group Item End -->
                                </ul>
                                <!-- List Group End -->
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- User Information End -->

                    <!-- Shared Media Start -->
                    {{-- <div class="chat-info-group">
                        <a class="chat-info-group-header" data-toggle="collapse" href="#shared-media" role="button" aria-expanded="true" aria-controls="shared-media">
                            <h6 class="mb-0">Hình ảnh</h6>
                            <svg class="hw-20 text-muted" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"/>
                            </svg>
                        </a>
                        <div class="chat-info-group-body collapse show" id="shared-media">
                            <div class="chat-info-group-content">
                                <div class="form-row">
                                    @foreach ($messages as $message)
                                        @if($message->type == 2)
                                            <div class="col-4 col-md-2 col-xl-4">
                                                <a href="#">
                                                    <img src="{{$message->message}}" class="img-fluid rounded border" alt="img">
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!-- Shared Media End -->

                    <!-- Shared Files Start -->
                    {{-- <div class="chat-info-group">
                        <a class="chat-info-group-header" data-toggle="collapse" href="#shared-files" role="button" aria-expanded="true" aria-controls="shared-files">
                            <h6 class="mb-0">Tài liệu</h6>
                            <svg class="hw-20 text-muted" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"/>
                            </svg>
                        </a>
                        <div class="chat-info-group-body collapse show" id="shared-files">
                            <div class="chat-info-group-content list-item-has-padding">
                                 <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="document">
                                            <div class="btn btn-primary btn-icon rounded-circle text-light mr-2">
                                                <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"/>
                                                </svg>
                                            </div>
                                            <div class="document-body">
                                                <h6 class="text-truncate">
                                                    <a href="#" class="text-reset" title="effects-of-global-warming.docs">Effects-of-global-warming.docs</a>
                                                </h6>
                                                <ul class="list-inline small mb-0">
                                                    <li class="list-inline-item">
                                                        <span class="text-muted">79.2 KB</span>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <span class="text-muted text-uppercase">docs</span>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="document-options ml-1">
                                                <div class="dropdown">
                                                    <button class="btn btn-secondary btn-icon btn-minimal btn-sm text-muted" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"/>
                                                        </svg>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="#">Download</a>
                                                        <a class="dropdown-item" href="#">Share</a>
                                                        <a class="dropdown-item" href="#">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> --}}
                    <!-- Shared Files End -->

                </div>
                <!-- Chat Info Body Start  -->

            </div>
        </div>
        <!-- Chat Info End -->
        <!-- List member info -->
    </div>
    <!-- Chats Page End -->

</main>
<script src="{!!asset('assets2/js/pusher.min.js')!!}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $('.avatar-seen').tooltip();
    $( document ).ready(function() {
        scrollToBottomFunc()
    });
    function scrollToBottomFunc() {
        $('#messageBody').animate({
            scrollTop: $('.message-day').get(0).scrollHeight
        }, 50);
    }

    var my_id = {{\Auth::Guard('member')->user()->id}};
    var receiver = '';

    Pusher.logToConsole = true;
    var pusher = new Pusher('5f84d2a344876b9fea04', {
        cluster: 'ap1',
        forceTLS: true
    });
    var channel = pusher.subscribe('chat-message');
    channel.bind('send-message', function (data) {
        $.ajax({
            url: '/api/get-all-message',
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
            },
            data: {id:my_id},
            success: function (data) {
                var element_group_id = $('.friends.active').attr('id');
                $('#chatContactTab').html('');
                var text = "";
                $.each(data.messages, function(key, value){
                    if(value.type == 2){
                        text = "[ Hình ảnh ]";
                    }else if(value.type == 3){
                        text = "[ File ]";
                    }
                    if(value.type != 1){
                        var text_truncate = '<p class="text-truncate">'+text+'</p>';
                        if(value.seen == 0){
                            var text_truncate = '<p class="text-truncate font-weight-bold">'+text+'</p>';
                        }
                        $('#chatContactTab').append('<li class="contacts-item friends" id="group_'+value.id+'">'+
                                                        '<a class="contacts-link link" data-link="'+value.id+'" data-type="1">'+
                                                            '<div class="avatar avatar-online">'+
                                                                '<img src="'+value.avatar+'" alt="avatar">'+
                                                            '</div>'+
                                                            '<div class="contacts-content">'+
                                                                '<div class="contacts-info">'+
                                                                    '<h6 class="chat-name text-truncate">'+value.name+'</h6>'+
                                                                    '<div class="chat-time">'+value.time+'</div>'+
                                                                '</div>'+
                                                                '<div class="contacts-texts">'+
                                                                    text_truncate +
                                                                '</div>'+
                                                            '</div>'+
                                                        '</a>'+
                                                    '</li>')
                    }else{
                        var text_truncate = '<p class="text-truncate">'+value.message+'</p>';
                        if(value.seen == 0){
                            var text_truncate = '<p class="text-dark font-weight-bold">'+value.message+'</p>';
                        }
                        $('#chatContactTab').append('<li class="contacts-item friends" id="group_'+value.id+'">'+
                                                        '<a class="contacts-link link" data-link="'+value.id+'" data-type="1">'+
                                                            '<div class="avatar avatar-online">'+
                                                                '<img src="'+value.avatar+'" alt="avatar">'+
                                                            '</div>'+
                                                            '<div class="contacts-content">'+
                                                                '<div class="contacts-info">'+
                                                                    '<h6 class="chat-name text-truncate">'+value.name+'</h6>'+
                                                                    '<div class="chat-time">'+value.time+'</div>'+
                                                                '</div>'+
                                                                '<div class="contacts-texts">'+
                                                                    text_truncate +
                                                                '</div>'+
                                                            '</div>'+
                                                        '</a>'+
                                                    '</li>')
                    }
                });
                $('#'+element_group_id).addClass('active');
            }
        })
        if(data.from == {!!\Auth::guard('member')->user()->id!!}){
            current_group_id = data.group_id;
        }
        if(data.group_id == current_group_id){
            if(data.from == 0){
                $('.message-day').append('<h6 class="text-center">'+data.message+'</h6>');
            }
            else if (my_id == data.from) {
                if(data.type == 2){
                    $('.message-day').append(`<div class="message self" id="message_`+data.message_id+`">
                                                <div class="message-wrapper d-flex flex-column align-items-end">
                                                    <div class="form-row w-50 mr-3 mb-1">
                                                        <div class="col">
                                                            <a class="popup-media" href="/`+data.message+`">
                                                                <img class="img-fluid rounded" src="/`+data.message+`" alt="image">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="message-options">
                                                    <div class="avatar avatar-sm"><img alt="avatar" src="`+data.avatar+`"></div>'+
                                                    <span class="message-date">`+data.time+`</span>
                                                    <div class="dropdown">
                                                        <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                            </svg>
                                                        </a>
                                                        <div class="dropdown-menu">
                                                            
                                                            <a class="dropdown-item d-flex align-items-center text-danger delete-message" href="javascript:void(0)" data-id="`+data.message_id+`">
                                                                <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                                </svg>
                                                                <span>Xóa tin</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`);
                }else if(data.type == 3){
                    $('.message-day').append(`<div class="message self" id="message_`+data.message_id+`">
                                                <div class="message-wrapper">
                                                    <div class="message-content">
                                                        `+ (data.reply_id > 0 ?
                                                        `<div class="reply-content">
                                                            <span class="reply-name">`+data.reply_name+`</span>
                                                            <span>`+data.reply_message+`</span>
                                                        </div>` : '') +`
                                                        <span>
                                                            <div class="document">
                                                                <div class="btn btn-warning btn-icon rounded-circle text-light mr-2">
                                                                    <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>'+
                                                                    </svg>
                                                                </div>
                                                                <div class="document-body">
                                                                    <h6>
                                                                        <a href="/`+data.message+`" class="text-reset" download>`+data.file_name+`</a>
                                                                    </h6>
                                                                    <ul class="list-inline small mb-0">
                                                                        <li class="list-inline-item">
                                                                            <span class="text-warning">`+data.size+`</span>
                                                                        </li>
                                                                        <li class="list-inline-item">
                                                                            <span class="text-warning text-uppercase">`+data.ext+`</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="message-options">'+
                                                    <div class="avatar avatar-sm"><img alt="avatar" src="`+data.avatar+`"></div>
                                                    <span class="message-date">`+data.time+`</span>
                                                    <div class="dropdown">
                                                        <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                            </svg>
                                                        </a>
                                                        <div class="dropdown-menu">
                                                            
                                                            <a class="dropdown-item d-flex align-items-center text-danger delete-message" href="javascript:void(0)" data-id="`+data.message_id+`">
                                                                <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                                </svg>
                                                                <span>Xóa tin</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                 </div>
                                            </div>`);
                }else{
                    $('.message-day').append(`<div class="message self" id="message_`+data.message_id+`">
                                                <div class="message-wrapper">
                                                    <div class="message-content">
                                                        `+ (data.reply_id > 0 ?
                                                        `<div class="reply-content">
                                                            <span class="reply-name">`+data.reply_name+`</span>
                                                            <span>`+data.reply_message+`</span>
                                                        </div>` : '') +`
                                                        <span>`+data.message+`</span>
                                                    </div>
                                                </div>
                                                <div class="message-options">
                                                    <div class="avatar avatar-sm"><img alt="avatar" src="`+data.avatar+`"></div>
                                                    <span class="message-date">`+data.time+`</span>
                                                    <div class="dropdown">
                                                        <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                            </svg>
                                                        </a>
                                                        <div class="dropdown-menu">
                                                            
                                                            <a class="dropdown-item d-flex align-items-center text-danger delete-message" href="javascript:void(0)" data-id="`+data.message_id+`">
                                                                <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                                </svg>
                                                                <span>Xóa tin</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`);
                }
            }else{
                if(data.type == 2){
                    $('.message-day').append(`<div class="message" id="message_`+data.message_id+`">
                                                <div class="message-wrapper d-flex flex-column align-items-start">
                                                    <div class="form-row w-50 ml-3 mb-1">
                                                        <div class="col">
                                                            <a class="popup-media" href="/`+data.message+`">
                                                                <img class="img-fluid rounded" src="/`+data.message+`" alt="image">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="message-options">
                                                    <div class="avatar avatar-sm"><img alt="avatar" src="`+data.avatar+`"></div>
                                                    <span class="message-date">`+data.time+`</span>
                                                    <div class="dropdown">
                                                    <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item d-flex align-items-center reply-message" href="javascript:void(0)" data-id="`+data.message_id+`" data-message="`+data.message+`" data-name="`+data.from_name+`">
                                                            <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6"/>
                                                            </svg>
                                                            <span>Trả lời</span>
                                                        </a>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>`);
                }else if(data.type == 3){
                    $('.message-day').append(`<div class="message" id="message_`+data.message_id+`">
                                                <div class="message-wrapper">
                                                    <div class="message-content">
                                                        `+ (data.reply_id > 0 ?
                                                        `<div class="reply-content">
                                                            <span class="reply-name">`+data.reply_name+`</span>
                                                            <span>`+data.reply_message+`</span>
                                                        </div>` : '') +`
                                                        <span>
                                                            <div class="document">
                                                                <div class="btn btn-warning btn-icon rounded-circle text-light mr-2">
                                                                    <svg class="hw-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                        '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>'+
                                                                    </svg>
                                                                </div>
                                                                <div class="document-body">
                                                                    <h6>
                                                                        <a href="/`+data.message+`" class="text-reset" download>`+data.file_name+`</a>
                                                                    </h6>
                                                                    <ul class="list-inline small mb-0">
                                                                        <li class="list-inline-item">
                                                                            <span class="text-warning">`+data.size+`</span>
                                                                        </li>
                                                                        <li class="list-inline-item">
                                                                            <span class="text-warning text-uppercase">`+data.ext+`</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="message-options">
                                                    <div class="avatar avatar-sm"><img alt="avatar" src="`+data.avatar+`"></div>
                                                    <span class="message-date">`+data.time+`</span>
                                                    <div class="dropdown">
                                                        <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                            </svg>
                                                        </a>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item d-flex align-items-center reply-message" href="javascript:void(0)" data-id="`+data.message_id+`" data-message="`+data.message+`" data-name="`+data.from_name+`">
                                                                <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6"/>
                                                                </svg>
                                                                <span>Trả lời</span>
                                                            </a>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`);
                }else{
                    $('.message-day').append(`<div class="message" id="message_`+data.message_id+`">
                                                <div class="message-wrapper">
                                                    <div class="message-content">
                                                         <h6 class="text-dark">`+data.from_name+`</h6>
                                                         `+ (data.reply_id > 0 ?
                                                        `<div class="reply-content-self">
                                                            <span class="reply-name">`+data.reply_name+`</span>
                                                            <span>`+data.reply_message+`</span>
                                                        </div>` : '') +`
                                                         <span>`+data.message+`</span>
                                                    </div>
                                                </div>
                                                <div class="message-options">
                                                    <div class="avatar avatar-sm"><img alt="avatar" src="`+data.avatar+`"></div>
                                                    <span class="message-date">`+data.time+`</span>
                                                    <div class="dropdown">
                                                        <a class="text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <svg class="hw-18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"/>
                                                            </svg>
                                                        </a>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item d-flex align-items-center reply-message" href="javascript:void(0)" data-id="`+data.message_id+`" data-message="`+data.message+`" data-name="`+data.from_name+`">
                                                                <svg class="hw-18 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h10a8 8 0 018 8v2M3 10l6 6m-6-6l6-6"/>
                                                                </svg>
                                                                <span>Trả lời</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`);
                }
            }
        }
        scrollToBottomFunc()
    });
    
    $('body').delegate('.send-icon','click',function(e){
        e.preventDefault();
        $('.top-chat').html('');
        var message = $('.emojionearea-editor').html();
        $('.emojionearea-editor').html('');
        var receiver_id = $('#receiver_id').val();
        var type = $('#type').val();
        var reply_id = $('#reply_id').val();
        if (message != '' && receiver_id != '') {
            $('#content_message').val('');
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: {message:message,receiver_id:receiver_id,my_id:my_id,type:type,type_file:'text',reply_id:reply_id},
                success: function (data) {
                    scrollToBottomFunc();
                }
            })
        }
    })
    $("#imageInput").change(function(){
        $(".attach-file").toggleClass('show');
        $('.top-chat').html('');
        var image = $(this).prop('files');
        var from = {!!\Auth::guard('member')->user()->id!!}
        var receiver_id = $('#receiver_id').val();
        var reply_id = $('#reply_id').val();
        var type = $('#type').val();
        var form_data = new FormData();
        form_data.append('type_file','image');
        for(let i=0;i<image.length;i++){
            form_data.append('image', image[i]);
            form_data.append('type', type);
            form_data.append('type_file', 2);
            form_data.append('reply_id', reply_id);
            form_data.append('receiver_id', receiver_id);
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                headers: {
                        'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                    },
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    $("#imageInput").val('');
                    scrollToBottomFunc();
                }
            })
        }
    });
    $("#documentInput").change(function(){
        $('.top-chat').html('');
        $(".attach-file").toggleClass('show');
        var file = $(this).prop('files');
        var from = {!!\Auth::guard('member')->user()->id!!}
        var receiver_id = $('#receiver_id').val();
        var reply_id = $('#reply_id').val();
        var type = $('#type').val();
        for(let i=0;i<file.length;i++){
            var form_data = new FormData();
            form_data.append('type_file','file');
            form_data.append('file', file[i]);
            form_data.append('reply_id', reply_id);
            form_data.append('receiver_id', receiver_id);
            var type = $('#type').val();
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                headers: {
                        'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                    },
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    $("#fileInput").val('');
                    scrollToBottomFunc();
                }
            })
        }
    });
    $('body').delegate('.close-reply','click',function(){
        $('#reply_id').val(0);
        $(this).parent().html('');
    })
    $('body').delegate('.reply-message','click',function(){
        $('#reply_id').val($(this).data('id'));
        $('.top-chat').html(`<a class="nav-link text-muted px-0 close-reply" href="javascript:void(0)" data-chat-info-close="">
                                <svg class="hw-22" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                </svg>
                            </a>
                            <div class="content-rep" style="padding:6px 18px;">
                                <span style="display:block;">Trả lời @`+$(this).data('name')+`</span>
                                <span>`+$(this).data('message')+`</span>
                            </div>`);
    })
    </script>
@stop
