<!DOCTYPE html>
<html lang="en">
@include('frontend.chat.head')
<body class="chats-tab-open">

    @include('frontend.chat.header')

    <!-- Main Layout Start -->
    <div class="main-layout">
        @include('frontend.chat.sidebar')
        @yield('content')
        <div class="backdrop"></div>

        <!-- All Modals Start -->

        <!-- Modal 1 :: Start a Conversation-->
        <div class="modal modal-lg-fullscreen fade" id="startConversation" tabindex="-1" role="dialog" aria-labelledby="startConversationLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-dialog-zoom">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="startConversationLabel">New Chat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-0 hide-scrollbar">
                    <div class="row">
                        <div class="col-12">
                            <!-- Search Start -->
                            <form class="form-inline w-100 p-2 border-bottom">
                                <div class="input-group w-100 bg-light">
                                    <input type="text" class="form-control form-control-md search border-right-0 transparent-bg pr-0" placeholder="Search">
                                    <div class="input-group-append">
                                        <div class="input-group-text transparent-bg border-left-0" role="button">
                                            <!-- Default :: Inline SVG -->
                                            <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                            </svg>

                                            <!-- Alternate :: External File link -->
                                            <!-- <img class="injectable hw-20" src="./../../assets/media/heroicons/outline/search.svg" alt=""> -->
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- Search End -->
                        </div>

                        <div class="col-12">
                                <!-- List Group Start -->
                                <ul class="list-group list-group-flush">

                                <!-- List Group Item Start -->
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="avatar avatar-online mr-2">
                                            <img src="../../assets/media/avatar/1.png" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h6 class="text-truncate">
                                                <a href="#" class="text-reset">Catherine Richardson</a>
                                            </h6>

                                            <p class="text-muted mb-0">Online</p>
                                        </div>
                                    </div>
                                </li>
                                <!-- List Group Item End -->

                                <!-- List Group Item Start -->
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="avatar avatar-online mr-2">
                                            <img src="../../assets/media/avatar/2.png" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h6 class="text-truncate">
                                                <a href="#" class="text-reset">Katherine Schneider</a>
                                            </h6>

                                            <p class="text-muted mb-0">Online</p>
                                        </div>
                                    </div>
                                </li>
                                <!-- List Group Item End -->

                                <!-- List Group Item Start -->
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="avatar avatar-offline mr-2">
                                            <img src="../../assets/media/avatar/3.png" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h6 class="text-truncate">
                                                <a href="#" class="text-reset">Brittany K. Williams</a>
                                            </h6>

                                            <p class="text-muted mb-0">Offline</p>
                                        </div>
                                    </div>
                                </li>
                                <!-- List Group Item End -->

                                <!-- List Group Item Start -->
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="avatar avatar-busy mr-2">
                                            <img src="../../assets/media/avatar/4.png" alt="">
                                        </div>
                                        <div class="media-body">
                                            <h6 class="text-truncate"><a href="#" class="text-reset">Christina Turner</a></h6>
                                            <p class="text-muted mb-0">Busy</p>
                                        </div>
                                    </div>
                                </li>
                                <!-- List Group Item End -->

                                <!-- List Group Item Start -->
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="avatar avatar-away mr-2">
                                            <img src="../../assets/media/avatar/5.png" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h6 class="text-truncate"><a href="#" class="text-reset">Annie Richardson</a></h6>
                                            <p class="text-muted mb-0">Away</p>
                                        </div>
                                    </div>
                                </li>
                                <!-- List Group Item End -->

                            </ul>
                            <!-- List Group End -->
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

        <!-- Modal 2 :: Create Group -->
        <div class="modal modal-lg-fullscreen fade" id="createGroup" tabindex="-1" role="dialog" aria-labelledby="createGroupLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-dialog-zoom">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title js-title-step" id="creatMemberLabel">&nbsp;</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body py-0 hide-scrollbar">
                        <div class="row hide pt-2" data-step="1" data-title="Tạo nhóm mới">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="groupName">Tên nhóm</label>
                                    <input type="text" class="form-control form-control-md" id="groupName" placeholder="Nhập tên nhóm" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Chọn ảnh nhóm</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="profilePictureInput" accept="image/*">
                                        <label class="custom-file-label forProfilePictureInput" for="profilePictureInput">Chọn ảnh</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 px-0 border-bottom">
                                <!-- Search Start -->
                                <form class="form-inline w-100 px-2 pb-2">
                                    <div class="input-group w-100 bg-light">
                                        <input type="text" class="form-control form-control-md search-member-group border-right-0 transparent-bg pr-0" placeholder="Tìm kiếm thành viên">
                                        <div class="input-group-append">
                                            <div class="input-group-text transparent-bg border-left-0" role="button">
                                                <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="member-added mx-2 mb-2">

                                </div>
                                <!-- Search End -->
                            </div>

                            <div class="col-12 px-0">
                                <h6 class="text-primary ml-2 mt-2 searching-for">Tất cả thành viên</h6>
                                <!-- List Group Start -->
                                <ul class="list-group list-group-flush group-member">

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer create-group-footer">
                        <span class="position-absolute ml-3 create-group-warning" style="left: 0; color: rgb(255, 0, 0)">*Bạn chưa nhập tên nhóm!</span>
                        <button type="button" class="btn btn-primary js-btn-step submit-group" data-orientation="create" disabled>Tạo nhóm</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- All Modals End -->
        <!-- Modal 3 :: Add member -->
        <div class="modal modal-lg-fullscreen fade" id="addMember" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-dialog-zoom">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title js-title-step" id="addMemberLabel">Thêm thành viên</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body py-0 hide-scrollbar">
                        <div class="row pt-2" data-step="1" data-title="Thêm thành viên">
                            <div class="col-12 px-0 border-bottom">
                                <!-- Search Start -->
                                <form class="form-inline w-100 px-2 pb-2">
                                    <div class="input-group w-100 bg-light">
                                        <input type="text" class="form-control form-control-md search-list-member-group border-right-0 transparent-bg pr-0" placeholder="Tìm kiếm thành viên">
                                        <div class="input-group-append">
                                            <div class="input-group-text transparent-bg border-left-0" role="button">
                                                <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="list-member-add mx-2 mb-2">

                                </div>
                                <!-- Search End -->
                            </div>

                            <div class="col-12 px-0">
                                <h6 class="text-primary ml-2 mt-2 searching-for">Tất cả thành viên</h6>
                                <!-- List Group Start -->
                                <ul class="list-group list-group-flush group-list-member">

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer create-group-footer">
                        <button type="button" class="btn btn-primary js-btn-step submit-add-member" data-group_id ="" data-orientation="create" disabled>Thêm</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- All Modals End -->

    </div>
    <!-- Main Layout End -->

    <script>
        $('body').delegate('#create-group','click', function(){
            $('#groupName').val('');
            $('.search-member-group').val('');
            $('#profilePictureInput').val('');
            $('.forProfilePictureInput').html('Chọn ảnh');
            $('.member-added').html('');
            $.ajax({
                method: 'POST',
                url: '/api/searching-member-group',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: {name:""},
                success: function(data){
                    $.each(data.member, function(key, value){
                        $('.group-member').append('<li class="list-group-item" id="member-'+value.id+'">'+
                                                    '<div class="media">'+
                                                        '<div class="avatar avatar-online mr-2">'+
                                                            '<img class="avatar-'+value.id+'" src="'+value.avatar+'" alt="avatar">'+
                                                        '</div>'+
                                                        '<div class="media-body">'+
                                                            '<h6 class="text-truncate">'+
                                                                '<a href="#" class="text-reset" id="name-'+value.id+'" data-id="'+value.id+'">'+value.full_name+'</a>'+
                                                            '</h6>'+
                                                        '</div>'+
                                                        '<div class="media-options">'+
                                                            '<div class="custom-control custom-checkbox">'+
                                                                '<ion-icon name="add-circle" id="'+value.id+'" class="text-primary add-member" style="font-size: 28px"></ion-icon>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</li>')
                    })
                }
            })
        })
        $('body').delegate('#add-member','click', function(){
            $('.search-member-group').val('');
            $('.list-member-add').html('');
            let group_id = $(this).data('group_id');
            $('.submit-add-member').attr('data-group_id',group_id);
            $.ajax({
                method: 'POST',
                url: '/api/get-list-member',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: {group_id:group_id},
                success: function(data){
                    $.each(data.members, function(key, value){
                        $('.group-list-member').append('<li class="list-group-item" id="member-list-'+value.id+'">'+
                                                    '<div class="media">'+
                                                        '<div class="avatar avatar-online mr-2">'+
                                                            '<img class="avatar-'+value.id+'" src="'+value.avatar+'" alt="avatar">'+
                                                        '</div>'+
                                                        '<div class="media-body">'+
                                                            '<h6 class="text-truncate">'+
                                                                '<a href="#" class="text-reset" id="name-'+value.id+'" data-id="'+value.id+'">'+value.full_name+'</a>'+
                                                            '</h6>'+
                                                        '</div>'+
                                                        '<div class="media-options">'+
                                                            '<div class="custom-control custom-checkbox">'+
                                                                '<ion-icon name="add-circle" id="'+value.id+'" class="text-primary add-member-list" style="font-size: 28px"></ion-icon>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</li>')
                    })
                }
            })
        })

        $('.search-member-group').on('input', function(){
            var name = $(this).val();
            $.ajax({
                method: 'POST',
                url: '/api/searching-member-group',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: {name:name},
                success: function(data){
                    if(name != ""){
                        $('.searching-for').html('kết quả tìm kiếm cho: "'+name+'"');
                    }else{
                        $('.searching-for').html('Tất cả thành viên');
                    }
                    $('.group-member').html('');
                    $.each(data.member, function(key, value){
                        $('.group-member').append('<li class="list-group-item" id="member-'+value.id+'">'+
                                                    '<div class="media">'+
                                                        '<div class="avatar avatar-online mr-2">'+
                                                            '<img class="avatar-'+value.id+'" src="'+value.avatar+'" alt="avatar">'+
                                                        '</div>'+
                                                        '<div class="media-body">'+
                                                            '<h6 class="text-truncate">'+
                                                                '<a href="#" class="text-reset" id="name-'+value.id+'" data-id="'+value.id+'">'+value.full_name+'</a>'+
                                                            '</h6>'+
                                                        '</div>'+
                                                        '<div class="media-options">'+
                                                            '<div class="custom-control custom-checkbox">'+
                                                                '<ion-icon name="add-circle" id="'+value.id+'" class="text-primary add-member" style="font-size: 28px"></ion-icon>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</li>')
                    })
                }
            })
        })
        $('.search-list-member-group').on('input', function(){
            var group_id = $('.submit-add-member').data('group_id');
            var keyword = $(this).val();
            $.ajax({
                method: 'POST',
                url: '/api/get-list-member',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: {group_id:group_id,keyword:keyword},
                success: function(data){
                    if(keyword != ""){
                        $('.searching-for').html('kết quả tìm kiếm cho: "'+keyword+'"');
                    }else{
                        $('.searching-for').html('Tất cả thành viên');
                    }
                    $('.group-list-member').html('');
                    $.each(data.members, function(key, value){
                        $('.group-list-member').append('<li class="list-group-item" id="member-list-'+value.id+'">'+
                                                    '<div class="media">'+
                                                        '<div class="avatar avatar-online mr-2">'+
                                                            '<img class="avatar-'+value.id+'" src="'+value.avatar+'" alt="avatar">'+
                                                        '</div>'+
                                                        '<div class="media-body">'+
                                                            '<h6 class="text-truncate">'+
                                                                '<a href="#" class="text-reset" id="name-'+value.id+'" data-id="'+value.id+'">'+value.full_name+'</a>'+
                                                            '</h6>'+
                                                        '</div>'+
                                                        '<div class="media-options">'+
                                                            '<div class="custom-control custom-checkbox">'+
                                                                '<ion-icon name="add-circle" id="'+value.id+'" class="text-primary add-member-list" style="font-size: 28px"></ion-icon>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</li>')
                    })
                }
            })
        })
        $('body').delegate('.add-member','click', function(){
            var id = $(this).attr('id');
            var name = $('#name-'+id).html();
            var avatar = $('.avatar-'+id).attr('src');
            $('#member-'+id).remove();
            $('.member-added').append('<a href="javascript:void(0)" data-id="'+id+'" data-name="'+name+'" data-avatar="'+avatar+'" class="member-added-id rounded border border-muted bg-primary text-white px-1 mr-1 py-1 d-inline-flex align-items-center">'+name+'<ion-icon name="close-circle" class="text-light pl-1 remove-member"></ion-icon></a>')
            if($('#groupName').val() != ""){
                $('.submit-group').attr('disabled',false);
            }else{
                $('.create-group-warning').addClass('show');
            }
        })
        $('body').delegate('.add-member-list','click', function(){
            var id = $(this).attr('id');
            var name = $('#name-'+id).html();
            var avatar = $('.avatar-'+id).attr('src');
            $('#member-list-'+id).remove();
            $('.list-member-add').append('<a href="javascript:void(0)" data-id="'+id+'" data-name="'+name+'" data-avatar="'+avatar+'" class="member-id rounded border border-muted bg-primary text-white px-1 mr-1 py-1 d-inline-flex align-items-center">'+name+'<ion-icon name="close-circle" class="text-light pl-1 remove-member-list"></ion-icon></a>')
            $('.submit-add-member').attr('disabled',false);
        })
        $('body').delegate('.remove-member-list','click', function(){
            var id = $(this).parent('.member-id').data('id');
            var name = $(this).parent('.member-id').data('name');
            var avatar = $(this).parent('.member-id').data('avatar');
            $(this).parent('.member-id').remove();
            $('.group-member').append('<li class="list-group-item" id="member-list-'+id+'">'+
                                            '<div class="media">'+
                                                '<div class="avatar avatar-online mr-2">'+
                                                    '<img class="avatar-'+id+'" src="'+avatar+'" alt="avatar">'+
                                                '</div>'+
                                                '<div class="media-body">'+
                                                    '<h6 class="text-truncate">'+
                                                        '<a href="#" class="text-reset" id="name-'+id+'" data-id="'+id+'">'+name+'</a>'+
                                                    '</h6>'+
                                                '</div>'+
                                                '<div class="media-options">'+
                                                    '<div class="custom-control custom-checkbox">'+
                                                        '<ion-icon name="add-circle" id="'+id+'" class="text-primary add-member-list" style="font-size: 28px"></ion-icon>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</li>');
            if($('.list-member-add').html()==""){
                $('.submit-add-member').attr('disabled',true);
            }
        })
        $('#groupName').on('input',function(){
            if($(this).val() != ""){
                if($('.member-added').html() != ""){
                    $('.submit-group').attr('disabled',false);
                }
                $('.create-group-warning').removeClass('show');
            }else{
                if($('.member-added').html() != ""){
                    $('.submit-group').attr('disabled',true);
                }
                $('.create-group-warning').addClass('show');
            }
        })
        $('body').delegate('.remove-member','click', function(){
            var id = $(this).parent('.member-added-id').data('id');
            var name = $(this).parent('.member-added-id').data('name');
            var avatar = $(this).parent('.member-added-id').data('avatar');
            $(this).parent('.member-added-id').remove();
            $('.group-member').append('<li class="list-group-item" id="member-'+id+'">'+
                                            '<div class="media">'+
                                                '<div class="avatar avatar-online mr-2">'+
                                                    '<img class="avatar-'+id+'" src="'+avatar+'" alt="avatar">'+
                                                '</div>'+
                                                '<div class="media-body">'+
                                                    '<h6 class="text-truncate">'+
                                                        '<a href="#" class="text-reset" id="name-'+id+'" data-id="'+id+'">'+name+'</a>'+
                                                    '</h6>'+
                                                '</div>'+
                                                '<div class="media-options">'+
                                                    '<div class="custom-control custom-checkbox">'+
                                                        '<ion-icon name="add-circle" id="'+id+'" class="text-primary add-member" style="font-size: 28px"></ion-icon>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</li>');
            if($('.member-added').html()==""){
                $('.submit-group').attr('disabled',true);
            }
        })
        $('body').delegate('.submit-group','click', function(){
            var group_name = $('#groupName').val();
            var group_img = $('#profilePictureInput').prop('files');
            var id = [];
            $('.submit-group').attr('disabled',true);
            id.push({!!\Auth::guard('member')->user()->id!!})
            $('.member-added-id').each(function(){
                id.push($(this).data('id'));
            })
            var form_data = new FormData();
            form_data.append('id', id);
            if(group_img.length == 0){
                form_data.append('group_img', "");
            }else{
                form_data.append('group_img', group_img[0]);
            }
            form_data.append('group_name', group_name);
            $.ajax({
                method: 'POST',
                url: '/api/create-group',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data){

                }
            })
        })
        $('body').delegate('.submit-add-member','click', function(){
            let member_id = [];
            $(this).attr('disabled',true);
            $('.member-id').each(function(){
                member_id.push($(this).data('id'));
            })
            let group_id = $(this).data('group_id');
            var form_data = new FormData();
            form_data.append('id', member_id);
            form_data.append('group_id',group_id);
            form_data.append('member_id',{{\Auth::guard('member')->user()->id}});
            $.ajax({
                method: 'POST',
                url: '/api/add-member-group',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data){
                    $('#addMember').modal('hide');
                    $.each(data.members, function(key, value){
                        $('.list-group-participant').append(`<li class="list-group-item" id="member-item-`+value.id+`">
                                                            <div class="media align-items-center">
                                                                <div class="avatar mr-2">
                                                                    <img src="`+value.avatar+`" alt="`+value.full_name+`">
                                                                </div>
                                                                <div class="media-body">
                                                                    <h6 class="text-truncate">
                                                                        <a href="#" class="text-reset">`+value.full_name+`</a>
                                                                    </h6>
                                                                    <p class="text-muted mb-0">`+value.position_name+`</p>
                                                                </div>

                                                                <div class="media-options ml-1">
                                                                    <div class="dropdown">
                                                                        <button class="btn btn-secondary btn-icon btn-minimal btn-sm text-muted" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"></path>
                                                                            </svg>
                                                                        </button>
                                                                        <div class="dropdown-menu">
                                                                           
                                                                            <a class="dropdown-item leave-group"  href="javascript:void(0)" data-member_id="`+value.id+`" data-group_id="`+group_id+`">Mời ra khỏi nhóm</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>`);
                    })
                    
                }
            })
        })
        $('body').delegate('.leave-group','click',function(){
            let member_id = $(this).data('member_id');
            let group_id = $(this).data('group_id');
            bootbox.confirm("Xóa thành viên này khỏi nhóm ? ", function(result){ 
                if(result == true){
                     $.ajax({
                        method: 'POST',
                        url: '/api/leave-group',
                        headers: {
                            'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                        },
                        data: {member_id:member_id,group_id:group_id},
                        success: function(data){
                            $('#member-item-'+member_id).remove();
                        }
                    })
                }
            });
        })
        $('body').delegate('.delete-message','click',function(){
            let id = $(this).data('id');
            $.ajax({
                url:'/api/delete-message',
                method:'POST',
                headers: {
                            'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                        },
                data:{id:id},
                success:function(){
                    $('#message_'+id).remove();
                }
            })
        })
    </script>
    @include('frontend.chat.footer')
    <script>
        var textbox = $("#content_message").emojioneArea({
            shortcuts: false,
            events: {
                keyup: function (editor, event) {
                 if(event.keyCode == 13 && !event.altKey)
                    {  
                        event.preventDefault();
                        $('.top-chat').html('');
                        var message = $('.emojionearea-editor').html().replaceAll("<div><br></div>","");
                        this.setText('');
                        var receiver_id = $('#receiver_id').val();
                        var type = $('#type').val();
                        var reply_id = $('#reply_id').val();
                        if (message != '' && receiver_id != '') {
                            $(this).val('');
                            $.ajax({
                                url: '/api/send-message',
                                method: 'POST',
                                headers: {
                                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                                },
                                data: {message:message,receiver_id:receiver_id,my_id:my_id,type:type,type_file:1,reply_id:reply_id},
                                success: function (data) {
                                    scrollToBottomFunc();
                                }
                            })
                        }
                    }else if(event.keyCode == 13 && event.altKey){
                        var message = $('.emojionearea-editor').html() + `<div><br/></div>`;
                        $('.emojionearea-editor').html(message);
                        var el = document.getElementsByClassName("emojionearea-editor")[0];
                        var range = document.createRange();
                        var sel = window.getSelection();
                        range.setStart(el.lastChild,el.lastChild);
                        range.collapse(true);
                        sel.removeAllRanges();
                        sel.addRange(range);
                        el.focus();  
                    }
                }
              }
    });
    </script>
</body>
</html>
