<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <title>{{\App\Config::first()->title}}</title>

    <meta name="api_token" content="{{\Auth::guard('member')->user() ? \Auth::guard('member')->user()->api_token : ''}}">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" href="{!!asset('assets2/img/apple-touch-icon.png')!!}">
    <link rel="icon" href="{{\App\Config::first()->favicon}}">
    <link rel="stylesheet" href="{{asset('/chat/webfonts/inter/inter.css')}}">
    <link rel="stylesheet" href="{{asset('/chat/css/app.min.css')}}">
    <link href="{!!asset('assets2/css/icomoon/styles.min.css')!!}" rel="stylesheet">
    <link href="{!!asset('/assets2/css/icon.css')!!}" rel="stylesheet">
</head>
