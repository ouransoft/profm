
<script src="{{asset('chat/vendors/jquery/jquery-3.5.0.min.js')}}"></script>
<script src="{{asset('chat/vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('chat/vendors/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('chat/vendors/svg-inject/svg-inject.min.js')}}"></script>
<script src="{{asset('chat/vendors/modal-stepes/modal-steps.min.js')}}"></script>
<script src="{{asset('chat/vendors/emojione/emojionearea.min.js')}}"></script>
<script src="{{asset('chat/js/app.js')}}"></script>
<script src="{{asset('assets2/js/bootbox.min.js')}}"></script>
