<header style="background: #333333">
    <div class="d-flex justify-content-between">
        <div class="col-md-3 d-flex align-items-center pl-0">
            <div class="menu h-100 pr-3">
                <button class="my-2 menu-header bg-transparent" aria-controls="header_pulldown_appmenu_base_grn" aria-haspopup="true" aria-expanded="false"></button>
                <div class="menu-tab row position-absolute bg-white py-1 px-3">
                    <div class="col-md-4 py-1 px-4">
                        <a href="{{route('home.view')}}" class="d-flex flex-column justify-content-center align-items-center">
                            <img src="{!!asset('img/Portal.png')!!}" alt="img" style="width: 32px; height: 32px">
                            <span class="text-dark" style="font-size: 11px; white-space: nowrap;">Protal</span>
                        </a>
                    </div>
                    <div class="col-md-4 py-1 px-4">
                        <a href="{{route('frontend.schedule.index')}}" class="d-flex flex-column justify-content-center align-items-center">
                            <img src="{!!asset('img/Scheduler.png')!!}" alt="img" style="width: 32px; height: 32px">
                            <span class="text-dark" style="font-size: 11px; white-space: nowrap;">Scheduler</span>
                        </a>
                    </div>
                    <div class="col-md-4 py-1 px-4">
                        <a href="{!!route('frontend.todo.index')!!}" class="d-flex flex-column justify-content-center align-items-center">
                            <img src="{!!asset('img/To_do_list.png')!!}" alt="img" style="width: 32px; height: 32px">
                            <span class="text-dark" style="font-size: 11px; white-space: nowrap;">To-do List</span>
                        </a>
                    </div>
                    <div class="col-md-4 py-1 px-4">
                        <a href="{{route('frontend.project.index')}}" class="d-flex flex-column justify-content-center align-items-center">
                            <img src="{!!asset('img/kaizen.png')!!}" alt="img" style="width: 32px; height: 32px">
                            <span class="text-dark" style="font-size: 11px; white-space: nowrap;">Kaizen</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="img">
                <a href="{{route('home.view')}}">
                    <img src="{!!asset('/img/i-vibo.png')!!}" alt="logo" style="width: 180px">
                </a>
            </div>
        </div>
        <div class="col-md-4 right-header-chat">
            <ul class="ul-notification d-flex align-items-center h-100 pr-3">
                <li>
                    <a href="#" role="button" id="dropdownMenuLink" title="Thông báo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle notification-menu"><i class="icon-bell2 @if (count(Auth::guard('member')->user()->unreadNotifications)) bell @endif"></i>
                        <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0" id="count-notification">@if (count(Auth::guard('member')->user()->unreadNotifications)){{count(Auth::guard('member')->user()->unreadNotifications)}}@endif</span>
                    </a>
                    <div class="dropdown-menu" id="log_member" aria-labelledby="dropdownMenuButton" style="padding-bottom: 0px;">
                        <div class="dropdown-content-header">
                            <span class="font-weight-semibold" style="font-size:14px;">{{trans('base.Your_notification')}}</span>
                        </div>
                        <div class="dropdown-content-body dropdown-scrollable">
                            <ul class="media-list list-notification pl-0">
                                @foreach( \Auth::guard('member')->user()->unreadNotifications as $val)
                                <li class="media">
                                    <a href="javascript:void(0)" class="seen-notification" style="width:100%" data-id="{{$val->id}}" data-link="{{$val->data['link']}}">
                                        <div class="media-body">
                                            <div class="media-title">
                                                <span class="font-weight-semibold color-blue">{{$val->data['full_name']}}</span>
                                                <span class="text-muted float-right font-size-sm">{{$val->data['time']}} ({{date('d/m',strtotime($val->created_at))}})</span>
                                            </div>
                                            <span class="text-dark black font-small" style="font-size: 14px">{{$val->data['content']}}</span>
                                        </div>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="dropdown-content-footer justify-content-center p-0 text-center" style="border-top: 1px solid #e2e2e2;display:flex;">
                            <a href="javascript:void(0)" class="text-grey w-100 py-2 seen-all-notification" title="Load more" style="font-size: 14px;">{{trans('base.Mark_as_viewed_all')}}</a>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="#" role="button" id="dropdownMenuLink" title="Thông tin tài khoản" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">@if(is_null(\Auth::guard('member')->user()->avatar)) <i class="fas fa-user-circle"></i> @else <img style="width: 32px;height:32px;border-radius: 50%;border: 1px solid #7d7d7d;" src="{{\Auth::guard('member')->user()->avatar}}" /> @endif</a>
                    <div class="dropdown-menu infomation-member" id="log_member" aria-labelledby="dropdownMenuButton">
                        <div class="content-log-member">
                            <div class="img-avatar">
                                <img src="@if(is_null(\Auth::guard('member')->user()->avatar)){!!asset('assets2/img/man.png')!!} @else {{\Auth::guard('member')->user()->avatar}} @endif" style="width:250px;padding:10px 20px;">
                            </div>
                            <h4 class="orange text-center">{!!\Auth::guard('member')->user()->full_name!!}</h4>
                            <div class="info-member-log">
                                <p><span class="fl50">{{trans('base.Employee_code')}}:</span><span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                <p><span class="fl50">{{trans('base.Position')}}:</span><span>@if(\Auth::guard('member')->user()->position) {!!\Auth::guard('member')->user()->position->name!!} @endif</span></p>

                            </div>
                        </div>
                        <div class="button-member-log text-center">
                            <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_reset_password">{{trans('base.Change_the_password')}}</a>
                            <a href="javascript:void(0)" class="btn" data-toggle="modal" data-target="#modal_update_avatar">{{trans('base.Update_personal_information')}}</a>
                            <a href="{!!route('logoutMember')!!}" class="btn">{{trans('base.Sign_out_of_your_account')}}</a>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="#" role="button" id="dropdownMenuLink" title="Thông tin tài khoản" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle choose-language">
                        {!!trans('base.language')!!}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item @if(session('locale') == 'en') active @endif" href="{{route('frontend.language.change',['locale'=>'en'])}}"><span class="icons icons-england" style="top:4px;margin-right: 8px;"></span>{{trans('base.English')}}</a>
                        <a class="dropdown-item @if(session('locale') == 'vi') active @endif" href="{{route('frontend.language.change',['locale'=>'vi'])}}"><span class="icons icons-vietnam" style="top:4px;margin-right: 8px;"></span>{{trans('base.Vietnamese')}}</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</header>
<script src="{!!asset('assets2/js/jquery.min.js')!!}"></script>
<script>
    $(document).ready(function(){
        var headerH = $('header').height();
        var windowH = $(window).height();
        $('.main-layout').height(windowH-headerH);
    })
    $('.menu-header').click(function(){
        $('.menu-tab').toggleClass('show');
    })
    $('body').delegate('.seen-notification', 'click', function () {
        var id = $(this).data('id');
        var link = $(this).data('link');
        $.ajax({
        url: '/api/seen-notification',
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                data:{id:id, _token : '{!! csrf_token() !!}'},
                success: function (response) {
                if (response.error == false) {
                    window.location.href = link;
                    //window.open(link);
                }
        }
        });
    });
</script>
