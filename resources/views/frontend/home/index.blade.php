<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>{{$config->company_name}}</title>
    <link rel="icon" href="{{$config->favicon}}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{!!asset('assets2/login-page/css/bootstrap.min.css')!!}">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{!!asset('assets2/login-page/css/fontawesome-all.min.css')!!}">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{!!asset('assets2/login-page/font/flaticon.css')!!}">
    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('mobile/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/custom.css')}}">
    <link rel="stylesheet" href="{!!asset('assets2/login-page/style.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets2/login-page/custom.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets2/login-page/css/bootstrap5.min.css')!!}">
</head>
<body data-bg-image="{!!asset('assets2/login-page/img/anh2.jpg')!!}">
    <div class="container d-flex align-items-center" style="height: 100vh">
        <div class="row">
            <div class="col-6 px-5 py-5 left">
                <form action="{!!route('loginMember')!!}" method="POST" class="rounded px-5 py-3">
                    <img src="{{asset('assets2/img/i-VIBO (White).png')}}" alt="logo" class="mb-3">
                    @csrf
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-1">
                        <label class="text-light" for="login_id" style="font-size: 18px;">{!!trans('base.Username')!!}</label>
                        <div class="d-flex align-items-center">
                            <input type="text" class="form-control pl-4" name="login_id" required="required">
                            <ion-icon class="text-muted position-absolute pl-1" name="person" style="font-size: 16px"></ion-icon>
                        </div>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-2">
                        <label class="text-light" for="password" style="font-size: 18px;">{!!trans('base.Password')!!}</label>
                        <div class="d-flex align-items-center">
                            <input type="password" class="form-control pl-4" name="password" required="required">
                            <ion-icon class="text-muted position-absolute pl-1" name="lock-closed" style="font-size: 16px"></ion-icon>
                        </div>
                    </div>
                    <div class="form-group fxt-transformY-50 fxt-transition-delay-3">
                        <div class="fxt-content-between w-100 pt-3">
                            <button type="submit" class="fxt-btn-fill w-100 py-2 rounded border-0 login-btn">{!!trans('base.Login')!!}</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-6 px-5 py-5 right">
                @if($config->login_type == 2)
                    <div class="rounded schedule-area h-100 d-flex align-items-center" style="background-image: linear-gradient(to right, #ff7700, #ffbc00);">
                        <div id="carouselExampleIndicators" class="carousel slide h-70 w-100" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @foreach ($slides as $key => $slide)
                                <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" class="@if($key==0) active @endif"></li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner">
                                @foreach ($slides as $key => $slide)
                                    <div class="carousel-item @if($key==0) active @endif">
                                        <img class="d-block w-100" src="{!!$slide->image()->first() ? asset($slide->image()->first()->link) : '' !!}" alt="{{$slide->title}}">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5 class="text-white">{{$slide->title}}</h5>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                @else
                    <div class="rounded schedule-area h-100">
                        <h1 class="pb-3 pt-4 mb-0 text-center text-light rounded-top" style="background-color: #292929c0;">{{$config->company_name}}</h1>
                        <span class="mb-0 pb-2 d-flex align-items-center justify-content-center" style="background-color: #292929c0; color:#ff7700;font-size: 24px">
                            <ion-icon name="calendar"></ion-icon>
                            <span class="text-white">Lịch sự kiện</span>
                        </span>
                        <div class="login-ivibo">
                            <nav>
                                <div class="nav nav-tabs d-flex flex-nowrap justify-content-between" id="nav-tab" role="tablist">
                                    @foreach ($schedule_arr as $key => $record)
                                        <button class="nav-link d-flex flex-column flex-fill align-items-center rounded-0 border-0 px-1 py-1 select-btn {{$date_now == date('Y-m-d',strtotime($record['day'])) ? 'active':''}}" id="{{date('d',strtotime($record['day']))}}" data-bs-toggle="tab" data-bs-target="#d{{date('d',strtotime($record['day']))}}" type="button" role="tab" aria-controls="d{{date('d',strtotime($record['day']))}}" aria-selected="true">
                                            <span class="text-light">@if($record['weekday'] == 0) CN @else Thứ {{$record['weekday']+1}} @endif</span>
                                            <span class="text-light">{{date('d',strtotime($record['day']))}}/{{date('m',strtotime($record['day']))}}</span>
                                        </button>
                                    @endforeach
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                @foreach ($schedule_arr as $key => $schedule)
                                    <div class="tab-pane fade show px-2 py-2 pb-0 {{$date_now == date('Y-m-d',strtotime($schedule['day'])) ? 'active':''}}" id="d{{date('d',strtotime($schedule['day']))}}" role="tabpanel" aria-labelledby="{{date('d',strtotime($schedule['day']))}}">
                                        @foreach ($schedule['event'] as $record)
                                            @if ($record != "")
                                                <div class="d-flex flex-column py-1">
                                                    <span class="pl-1" style="font-size: 16px;font-weight: 500;">
                                                        {{-- <ion-icon name="ellipse" style="font-size: 12px;color: #ff7700"></ion-icon> --}}
                                                        {{$record->title}}
                                                    </span>
                                                    <span class="pl-1 d-flex align-items-center text-muted" style="font-size: 12px">
                                                        <ion-icon name="time" class="text-muted" style="margin-right: 5px;"></ion-icon>
                                                        {{date('H:i',strtotime($record->start_date))}} - {{date('H:i',strtotime($record->end_date))}}
                                                    </span>
                                                </div>
                                            @else
                                            <div class="d-flex align-items-center">
                                                <span class="pl-1" style="color: #ff7700">Không có sự kiện</span>
                                            </div>
                                            @endif
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</body>
<!-- jquery-->
<script src="{!!asset('assets2/login-page/js/jquery-3.5.0.min.js')!!}"></script>
<!-- Popper js -->
<script src="{!!asset('assets2/login-page/js/popper.min.js')!!}"></script>
<!-- Bootstrap js -->
<script src="{!!asset('assets2/login-page/js/bootstrap.min.js')!!}"></script>
<script src="{!!asset('assets2/login-page/js/bootstrap.bundle.min.js')!!}"></script>
<!-- Validator js -->
<!-- Ionicons -->
<script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
<!-- Custom Js -->
<script src="https://unpkg.com/imagesloaded@4.1.4/imagesloaded.pkgd.min.js"></script>
<script src="{!!asset('assets2/login-page/js/main.js')!!}"></script>
<script src="{!!asset('assets2/js/Notifier.min.js')!!}"></script>
<script>
    $('.your-class').carousel();
</script>
@if (Session::has('error'))
<script>
    var notifier = new Notifier();
    var notification = notifier.notify("warning", "Đăng nhập không thành công");
    notification.push();
</script>
@endif
