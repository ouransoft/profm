<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        @include('frontend/layouts/__head')
    </head>
    <body class="page-body">
        <!-- Page content -->
        <div id="page">
            <!-- Main content -->
            <div class="content-wrapper">
                @include('frontend/layouts/__header')
                @include('frontend/schedule/sidebar')
                <div class="content">
                    <div class="page-project">
                        <div class="sidebar-project shadow">
                            <div class="card-body" style="border-bottom: 1px solid;">
                                <div class="media">
                                    <div class="avatar" style="width: 30%">
                                        <a href="#">
                                            <img src="{{\Auth::guard('member')->user()->avatar}}" width="100%" height="auto" class="rounded-circle" alt="">
                                        </a>
                                    </div>
                                    <div class="media-body" style="margin-left: 12px;line-height: inherit;">
                                        <div class="media-title font-weight-semibold" style="font-size: 18px;line-height:inherit">{{\Auth::guard('member')->user()->full_name}}</div>
                                        <div class="font-size-xs" style="font-size:16px;">
                                            <span class="icons icons-position" style="width:25px;"></span> Staff
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <h3 class="title-sidebar-system"><span class="orange">THIẾT LẬP</span> HỆ THỐNG</h3>
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="padding-bottom: 15px;">
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.dashboard.index') active @endif" href="{!!route('frontend.dashboard.index')!!}"><span class="icons icons-statistical"></span>Trang chủ</a>
                                <a class="nav-link @if(\Request::route()->getName() == 'frontend.config.index') active @endif" href="{!!route('frontend.config.index')!!}"><span class="icons icons-system"></span>Cấu hình hệ thống</a>
                            </div>
                            <div class="list-system">
                                <h3 class="title-sidebar-system"><span class="orange">CÔNG CỤ</span> QUẢN TRỊ</h3>
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link @if(\Request::route()->getName() == 'frontend.department.index') active @endif" href="{!!route('frontend.department.index')!!}"><span class="icons icons-diagram"></span>QL phòng ban</a>
                                    <a class="nav-link @if(\Request::route()->getName() == 'frontend.position.index') active @endif" href="{!!route('frontend.position.index')!!}"><img  src="{!!asset('assets2/img/send.png')!!}">QL chức vụ</a>
                                    <a class="nav-link @if(\Request::route()->getName() == 'frontend.member.index') active @endif" href="{!!route('frontend.member.index')!!}"><span class="icons icons-member"></span>QL thành viên</a>
                                    <a class="nav-link @if(\Request::route()->getName() == 'frontend.level.index') active @endif" href="{!!route('frontend.level.index')!!}"><span class="icons icons-performance"></span>QL cấp độ đề án</a>
                                    <a class="nav-link @if(\Request::route()->getName() == 'frontend.slide.index') active @endif" href="{!!route('frontend.slide.index')!!}" style="display:block;height: auto;"><span class="icons icons-image"></span>QL hình ảnh tiêu biểu</a>
                                    <a class="nav-link @if(\Request::route()->getName() == 'frontend.equipment.index') active @endif" href="{!!route('frontend.equipment.index')!!}"><span class="icons icons-wrench"></span>QL trang thiết bị</a>
                                </div>
                            </div>
                        </div>
                        <div class="project-content">
                            @yield('content')
                        </div>
                    </div>
                </div>
                @include('frontend/layouts/footer_index')
                <!-- /footer -->
            </div>
            <!-- /main content -->
        </div>
        <!-- /page content -->
    </body>
    @yield('script')
    <script src="{!! asset('assets2/js/datatables.min.js') !!}"></script>
    <script src="{!! asset('assets2/js/select2.min.js') !!}"></script>
    <script src="{!! asset('assets2/js/datatables_basic.js') !!}"></script>
    <script>
    $('.next-sidebar').click(function(){
        $('.sidebar').addClass('open');
    })
    $('.close-sidebar').click(function(){
        $('.sidebar').removeClass('open');
    })
    $('body').delegate('.btn-add', 'click', function(){
        form = $('#postData');
        url = '/api/add-' + form.data('model');
        $.ajax({
                url:url,
                method:"POST",
                data:form.serialize(),
                success:function(response){
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Thêm mới thành công");
                notification.push();
                setTimeout(function(){ location.reload(); }, 2000);
                }
        })
    })
    $("input").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            form = $('#postData');
            url = '/api/add-' + form.data('model');
            $.ajax({
                    url:url,
                    method:"POST",
                    data:form.serialize(),
                    success:function(response){
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Thêm mới thành công");
                    notification.push();
                    setTimeout(function(){ location.reload(); }, 2000);
                    }
            })
        }
    });
    $('.edit-data').click(function(){
        url = '/api/edit-' + $(this).data('model');
        id = $(this).data('id');
        $.ajax({
                url:url,
                method:"POST",
                data:{id:id},
                success:function(response){
                $.each(response.data, (key, item) => {
                console.log(key);
                if(key == 'image'){
                    if(item != ''){
                        $('.file-drop-zone').html('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">'+
                                                        '<div class="kv-file-content">'+
                                                                    '<img src="'+item.link+'" class="file-preview-image kv-preview-data" style="width:auto;height:auto;max-width:100%;max-height:100%;">'+
                                                        '</div>'+
                                                        '<div class="file-thumbnail-footer">'+
                                                        '<div class="file-footer-caption" title="'+item.name+'">'+
                                                            '<div class="file-caption-info">'+item.name+'</div>'+
                                                            '<div class="file-size-info"></div>'+
                                                        '</div>'+
                                                        '<div class="file-actions">'+
                                                            '<div class="file-footer-buttons file-button">'+
                                                                '<button type="button" class="kv-file-remove btn btn-link btn-icon btn-xs" title="Remove file" data-url="" data-id="'+item.id+'"><i class="icon-trash"></i></button>'+
                                                            '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                    '</div>');
                        $('.image-link').val(item.link);
                        $('.image-id').val(item.id);
                        $('.image-name').val(item.name);
                    }else{
                        $('.file-drop-zone').html('');
                        $('.image-link').val('');
                        $('.image-id').val('');
                        $('.image-name').val('');
                    }
                }else if(key == 'file'){
                    $('.file-drop-zone').html('<div class="file-preview-frame krajee-default  file-preview-initial file-sortable kv-preview-thumb">'+
                                                    '<div class="kv-file-content">'+
                                                                 '<img src="/img/file-icon.png" class="file-preview-image kv-preview-data" style="width:auto;height:auto;max-width:100%;max-height:100%;">'+
                                                     '</div>'+
                                                     '<div class="file-thumbnail-footer">'+
                                                     '<div class="file-footer-caption" title="'+item+'">'+
                                                         '<div class="file-caption-info">'+item+'</div>'+
                                                         '<div class="file-size-info"></div>'+
                                                     '</div>'+

                                                 '</div>'+
                                                 '</div>');
                    $('.image_data').val(item);
                }else if(key == 'select'){
                    $('select').html(item);
                    $('.select-search').select2();
                }else if(key == 'status'){
                    if(item == 1 ){
                        $('.form-check-input').attr("checked", true);
                    }
                }else{
                    $(`[name="${key}"]`).val(item);
                    let html = `<button type="button" class="btn btn-sm btn-cancel btn-default">Thoát</button>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-success btn-update" data-id="` + id + `">Lưu</a>`;
                    $(`form#postData .button-group`).html(html);
                }
                    });
                }
        })
    });
    $('body').delegate('.btn-update', 'click', function(){
        form = $('#postData');
        url = '/api/update-' + form.data('model');
        id = $(this).data('id');
        $.ajax({
                url:url,
                method:"POST",
                data:form.serialize() + '&id=' + id,
                success:function(response){
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Cập nhật thành công");
                notification.push();
                setTimeout(function(){ location.reload(); }, 2000);
                }
        })
        })
    $('[data-action="delete"]').click(function (e) {
        var elm = this;
        bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
            if (result === true) {
                $(elm).parents('form').submit();
            }
        });
    });
     $('body').delegate('.btn-cancel','click',function(){
        $('#postData')[0].reset();
        $('.file-preview-frame').remove();
        $('.image_data').val('');
        let html = `<a href="/home" class="btn btn-sm btn-default">Quay lại</a>
                            <button type="button" class="btn btn-sm btn-primary btn-add">Thêm</button>`;
    $(`form#postData .button-group`).html(html);
    });
    </script>
</html>
