@extends('frontend.layouts.view_schedule')
@section('content')
<body class="page-body">
   <div class="col-md-12" style="padding-top: 15px;">
   <div class="portlet_base_grn ">
      <style type="text/css">
         .tableFixed{
         width:100%;
         table-layout:fixed;
         }
         .normalEvent {}
         .normalEventElement{}
         .showEventTitle{
         position:absolute;
         max-width: 300px;
         overflow:hidden;
         z-index: 20;
         }
         .hideEventTitle{}
         .hideEventTitle .normalEvent {
         overflow:hidden;
         vertical-align:top;
         }
         .userElement{
         overflow:hidden;
         }
         .nowrap_class{
         white-space:nowrap;
         padding:2px;
         overflow:hidden;
         }
      </style>
      <style type="text/css">
         .differ_tz_color{
              background-color:#FFDBDE;
         }
         .hide_event{
              display:none;
         }
      </style>
      <table class="top_title">
         <tbody>
            <tr>
               <td><span class="portlet_title_grn"><a href="{{route('frontend.schedule.index')}}">{{trans('base.Scheduler_Groups')}} ({{trans('base.Week')}})</a></span></td>
               <td align="right">
                  <div class="search_navi">
                     <form name="user_week336" method="GET" action="/schedule/index?">
                        <input type="hidden" name="gid" value="search">
                        <input type="hidden" name="type_search" value="user">
                        <div class="searchboxChangeTarget-grn">
                           <div class="searchbox-grn">
                              <div class="searchbox-select-schedule"></div>
                              <div id="searchbox-schedule-336" class="input-text-outer-schedule searchbox-keyword-schedule">
                                 <input type="text" name="search_text" value="Tìm kiếm theo tên thành viên" autocomplete="off" class="input-text-schedule prefix-grn" id="schedules_search_text" maxlength="100">
                                 <input type="button" id="searchbox-submit-schedules-336" class="searchbox-submit-schedule" onclick="JavaScript:void(0);">
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
      <form name="schedule/index" method="GET" action="/schedule/index?">
         <input type="hidden" name="bdate" value="{{date('Y-m-d')}}">
         <div class="portal_frame portal_frame_schedule_groupweek_grn">
            <div class="margin_bottom">
                <table width="100%">
                   <tbody>
                      <tr>
                         <td class="v-allign-middle" nowrap="nowrap">
                            <table cellspacing="0" cellpadding="0" border="0">
                               <tbody>
                                  <tr>
                                     <td nowrap="nowrap">
                                        <link href="{!!asset('assets/css/fag_tree.css')!!}" rel="stylesheet" type="text/css">
                                        <table id="group-select" border="0" cellspacing="0" cellpadding="0" class="wrap_dropdown_menu" style="width: 332px;">
                                           <tbody>
                                              <tr height="20">
                                                 <td id="title" class="dropdown_menu_current" height="20" nowrap="">{{trans('base.Choose_by_department')}}</td>
                                                 <td id="user-button" class="dropdown_menu_user" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                    <img src="{{asset('/img/user-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0">
                                                 </td>
                                                 <td id="facility-button" class="dropdown_menu_facility" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                    <img src="{{asset('/img/facility-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0">
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                        <div id="user-popup" class="wrap_dropdown_option" style="visibility: visible; display: none;">
                                        </div>
                                        <div id="facility-popup" class="wrap_dropdown_option" style="display: none;">  
                                        </div>
                                        <div id="dummy-popup" class="wrap_dropdown_option"></div>
                                        <div></div>
                                        <div id="facility-popup-dummy_root" style="border:1px solid #000000; top:-10000px; left:-10000px; position:absolute; background-color:#FFFFFF; overflow:scroll; width:1px;height:1px;">
                                           <div id="facility-popup-dummy_tree_wrap_tree1" class="wrap_tree1" style="width: 320px;">
                                              <div id="facility-popup-dummy_tree_wrap_tree2" class="wrap_tree2" style="width: 298px; overflow-x: scroll; height: 240px;">
                                                 <div id="facility-popup-dummy_tree">
                                                    <div class="ygtvitem">
                                                       
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                        <script type="text/javascript">
                                           (function () {

                                               var group_select_id    = 'group-select';
                                               var title_id           = 'title';
                                               var user_button_id     = 'user-button';
                                               var facility_button_id = 'facility-button';
                                               var user_popup_id      = 'user-popup';
                                               var facility_popup_id  = 'facility-popup';
                                               var is_multi_view      = false;

                                               var dropdown = new GRN_DropdownMenu(
                                                   group_select_id, title_id, user_button_id, facility_button_id,
                                                   GRN_DropdownMenu.prototype.PreferOrganization,
                                                   user_popup_id, facility_popup_id,
                                                   clickOrganizationCallback, clickFacilityGroupCallback,
                                                   "" );

                                               function updateTitle( title ) {
                                                   var old_width = jQuery('#' + group_select_id).outerWidth();
                                                   jQuery('#' + group_select_id).css( {'width':''} );
                                                   jQuery('#' + title_id).html( title );
                                                   if( old_width > jQuery('#' + group_select_id).outerWidth() ) {
                                                       jQuery('#' + group_select_id).css( { 'width': old_width + 'px'} );
                                                   }
                                               }

                                               function clickOrganizationCallback( group_item ) {
                                                   return function(){
                                                       updateTitle( group_item.name )
                                                       dropdown.organization.hide();

                                                       if (is_multi_view) {

                                                           jQuery(document).trigger("scheduler.select_user_org_facility_dropdownlist.select", {gid: group_item.gid, target: dropdown});
                                                           return;
                                                       }
                                                       location.href = "/schedule/index?"+ '&bdate=' + document.forms["schedule/index"].bdate.value + '&gid='+group_item.gid;
                                                   }
                                               }

                                               function clickFacilityGroupCallback( node ) {
                                                   if( node.extra_param ) { 
                                                       updateTitle( node.label );
                                                   }
                                                   else {
                                                       if( node.oid == 'f' ) {
                                                           updateTitle( '(All facilities)' );
                                                       }else{
                                                           updateTitle(  node.label + ' (Facility group)' );
                                                       }
                                                   }
                                                   dropdown.facility.hide();

                                                   var oid = node.oid;
                                                   

                                                   location.href = "/schedule/index?"+ '&bdate=' + document.forms["schedule/index"].bdate.value + '&eid='+oid + '&p='+node.extra_param;
                                               }
                                               dropdown.initializeOrganization(
                                                   new Array(
                                                   {!!$department!!}
                                                            ) );

                                               var group_select_width = dropdown.organization.getWidth( jQuery('#' + title_id).outerWidth() );
                                               jQuery('#' + group_select_id).css( {'width': group_select_width +"px"} );

                                               dropdown.updateTitle = updateTitle;

                                               dropdown.initializeFacilityGroup( { 'page_name': "schedule/index",
                                                                                   'ajax_path':'/schedule/json/accessible_facility_tree?',
                                                                                   'csrf_ticket':'20f137f8be0895ef4f146389639bbf0c',
                                                                                   'callback':clickFacilityGroupCallback,
                                                                                   'selectedOID':"x",
                                                                                   'title_width': jQuery('#' + title_id).outerWidth(),
                                                                                   'node_info':
                                                                                   [{!!$equipment!!}]
                                                                                 });
                                           }());

                                        </script>
                                     </td>
                                     <td nowrap="nowrap"><a class="selectPulldownSub-grn" id="search_some_member" href="javascript:void(0);"  title="Select users"><img src="{!!asset('/img/blankB16.png')!!}" border="0" alt=""></a></td>
                                  </tr>
                               </tbody>
                            </table>
                         </td>
                         <td class="v-allign-middle" align="center">
                            <span class="displaydate">{{date('D, F d, Y',strtotime($date_now))}}</span>
                         </td>
                         <td class="v-allign-middle" nowrap="nowrap" align="right">
                            <div class="moveButtonBlock-grn">
                               <span class="moveButtonBase-grn">
                                   <a href="javascript:void(0)" class='load-view' data-action ='prev' data-day='7' title="{{trans('base.Previous_week')}}"><span class="moveButtonArrowLeftTwo-grn"></span></a>  
                               </span>
                               <span class="moveButtonBase-grn">
                                    <a href="javascript:void(0)" class='load-view' data-action ='prev' data-day='1' title="{{trans('base.Previous_day')}}"><span class="moveButtonArrowLeft-grn"></span></a>    
                                </span>
                                <span class="moveButtonBase-grn" title=""><a href="javascript:void(0)" class='load-today' data-start_day='{{date('Y-m-d')}}' data-action ='next' data-day='0'>{{trans('base.Today')}}</a></span>
                                <span class="moveButtonBase-grn"><a href="javascript:void(0)" class='load-view' data-action ='next' data-day='1' title="{{trans('base.Next_day')}}"><span class="moveButtonArrowRight-grn"></span></a></span>
                                <span class="moveButtonBase-grn"><a href="javascript:void(0)" class='load-view' data-action ='next' data-day='7' title="Next week"><span class="moveButtonArrowRightTwo-grn"></span></a></span>
                            </div>
                         </td>
                      </tr>
                   </tbody>
                </table>
             </div>
            <div id="gw_336_cal_div" class="js_customization_schedule_view_type_GROUP_WEEK">
               <table id="schedule_groupweek_336" class="calendar scheduleWrapper scheduleWrapperGroupWeek portlet_groupweek " width="100%" cellspacing="0" cellpadding="0" style="font-size:100%;;">
                   <colgroup>
                        <col class="group_week_calendar_column_first">
                        <col class="group_week_calendar_column_common">
                        <col class="group_week_calendar_column_common">
                        <col class="group_week_calendar_column_common">
                        <col class="group_week_calendar_column_common">
                        <col class="group_week_calendar_column_common">
                        <col class="group_week_calendar_column_common">
                        <col class="group_week_calendar_column_common">
                  </colgroup>
                  {!!$html!!}
               </table>
            </div>
            <script src="{!!asset('/js/schedule_gw.js')!!}" type="text/javascript"></script>
         </div>
         <!--end of portal_frame -->
      </form>
      <script src="{!!asset('/js/dragdrop-min.js')!!}" type="text/javascript"></script>
      <script src="{!!asset('/js/selector-min.js')!!}" type="text/javascript"></script>
      <script src="{!!asset('/js/schedules.js')!!}" type="text/javascript"></script>
      <script src="{!!asset('/js/schedule.js')!!}" type="text/javascript"></script>
   </div>
   </div>
   <div class="row" style="margin:0px">
        <div class="col-md-8">
            <div class="dashboard">
                <div class="dashboardHeader">
                    <div class="pull-left" style="color:#fe7701">{{trans('base.Notifications')}}</div>
                    <div class='tap-all2'>
                        <a class='tap-all active notifitap' data-status ='0' href="javascript:void(0)">{{trans('base.Unread')}}</a> 
                         <a class='tap-all notifitap' data-status ='1' href="javascript:void(0)">{{trans('base.Read')}}</a>    
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="dashboardContent" style="overflow-y: scroll;width:100%;height:125px;">
                    <div class="table-content" style="margin-top:0px;padding:0 20px;">
                    <table class="list_column">
                           <colgroup>
                              <col width="15%">
                              <col width="50%">
                              <col width="25%">
                           </colgroup>
                           <tbody id='list_notification'>
                               <tr valign="top">
                                    <th nowrap="">
                                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Sender')}}</font></font></div>
                                    </th>
                                    <th nowrap="">{{trans('base.Content')}}</th>
                                    <th nowrap="">{{trans('base.Created_at')}}</th>
                                </tr>
                                @foreach( \Auth::guard('member')->user()->unreadNotifications as $val)
                                <tr valign="top">
                                    <td nowrap="" style="padding-left:0px"><span class="selectlist_user_grn"></span>{{$val->data['full_name']}}</th>
                                    <td nowrap="">
                                        <a class="seen-notification" data-id="{{$val->id}}" data-link="{{$val->data['link']}}" href="javascript:void(0)">{{$val->data['content']}}</a>
                                    </td>
                                    <td nowrap="">{{date('d/m',strtotime($val->created_at))}} ({{$val->data['time']}})</th>
                                </tr>
                                @endforeach
                           </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="dashboard">
                <form name="todo/index" method="post" action="{!!route('frontend.todo.update_status')!!}">
                <div class="dashboardHeader">
                    <div class="pull-left">{{trans('base.To_do_list')}}</div>
                    <div class='tap-all1'>
                        <a class='tap-all active todo-tap' data-status="pending" href="javascript:void(0)">{{trans('base.Uncomplete')}}</a> 
                         <a class='tap-all todo-tap' data-status="complete" href="javascript:void(0)">{{trans('base.Complete')}}</a>    
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="dashboardContent" style="overflow-y: scroll;width:100%;height:216px;">
                    <div class="table-content" style="margin-top:0px;padding:0 20px;">                                                                                                                    
                        <table class="list_column">
                           <colgroup>
                              <col width="35%">
                              <col width="25%">
                              <col width="25%">
                              <col width="15%">
                           </colgroup>
                           <tbody id='list_todo'>
                              <tr valign="top">
                                 <th nowrap="">
                                    <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tiêu đề</font></font></div>
                                 </th>
                                 <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trạng thái</font></font></th>
                                 <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Deadline</font></font></th>
                                 <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Độ ưu tiên</font></font></th>
                              </tr>
                              @foreach($todos as $key=>$todo)
                              <tr class="linetwo">
                                <td><span class="nowrap-grn "><a href="{{route('frontend.todo.view',$todo->id)}}">{{$todo->title}}</a></span></td>
                                <td nowrap=""><span class="badge badge-color{{$todo->status}}">{{$todo->nameStatus()}}</span></td>
                                <td nowrap="">{{$todo->end_date()}}</td>
                                <td nowrap="">
                                    <span class="badge badge-priority{{$todo->priority}}">{{$todo->namePriority()}}</span>
                                </td>
                             </tr>
                              @endforeach
                           </tbody>
                        </table>                                                                                                                                                                                                                                  
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dashboard">
                <div class="dashboardHeader">
                    <div class="pull-left">{{trans('base.Time_set')}}</div>
                    <div class="clearfix"></div>
                </div>
                <div class="dashboardContent">
                     <p style="margin-left:20px;">{{trans('base.Content_under_construction')}}</p>
                    <!--
                    <div class="table-content">
                        <div class='row'>
                            <div class='col-md-6'>
                                <p style="margin-left:30px"> Start at 7:45 AM </p>
                            </div>
                            <div class='col-md-6'>
                                <a class='tap-all active'>End</a> 
                                <a class='tap-all'>Out</a>   
                            </div>
                        </div>
                    </div>
                    -->
                </div>
            </div>
            <div class="dashboard">
                <div class="dashboardHeader">
                    <div class="pull-left">Kaizen systems</div>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="dashboardContent" style="overflow-y: scroll;width:100%;height:216px;">
                    <div class="table-content" style="margin-top:0px;">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend><span class="orange">TOP TEAMS</span> of Month</legend>
                                    @foreach($rank_team as $key=>$val)
                                    <p>{{$key + 1}}. Tem {{$val->name}} <span>{{$val->count}} đề án</span></p>
                                    @endforeach
                                </fieldset>
                            </div>
                            <div class="col-md-12">
                                <fieldset>
                                    <legend><span class="orange">TOP USER</span> of Quarter</legend>
                                    <table class="top-user" style="margin:0px;">
                                        <tbody>
                                            @foreach($rank_quarter as $key=>$val)
                                            <tr>
                                                <td style="width:40%"><p>{{$key + 1}}. {{\App\Member::find($val->id)->full_name}}</p></td>
                                                <td><p>{{\App\Member::find($val->id)->login_id}}</p></td>
                                                <td><p>Team @if(\App\Member::find($val->id)->department) {{\App\Member::find($val->id)->department->name}} @endif</p></td>
                                                <td><p>{{$val->count}} đề án</p></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modalbox" id="modal_search_member" data-backdrop="static" aria-modal="true" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px;">
                    <h5 class="modal-title">Tìm kiếm thành viên</h5>
                    <a href="javascript:void(0);" data-dismiss="modal" class="close-attendees"><i class='icon-cross' style="font-size:20px;color:#333;"></i></a>
                </div>
                <div class="modal-body">
                    <div class="input-group rounded" style="border: 1px solid #aaaaaa!important;">
                        <input type="search" id="input_search_member" class="form-control rounded" placeholder="Tìm nhân viên" aria-label="Search" aria-describedby="search-addon">
                        <button type="button" class="search-member search-position input-group-text border-0" id="search-addon" style="background:#fd8113;border-radius: 0px;">
                            <i class='icon-search4 text-white'></i>
                        </button>
                    </div>
                    <div class="form-group my-2">
                        <select id="select_attendees" class="select2" data-placeholder='Tất cả'>
                            <option></option>
                            {!!$department_html!!}
                        </select>
                    </div>
                    <div class='row'>
                        <div class="card col-md-6">
                            <h6 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">Danh sách thành viên</h6>
                            <div class="border user-list">
                                <ul class="listview member-list pd0">
                                    @foreach($list_member as $key=>$value)
                                    <li class="d-flex align-items-center px-1 li-add">
                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                            <i class='icon-user'></i>
                                        </span>
                                        <div class="d-flex align-items-end justify-content-between w-100">
                                            <span class="attendees-name">{{$value->full_name}} ({{$value->login_id}})</span>
                                            <span class="text-warning add-member-btn" data-id='{{$value->id}}' data-name='{{$value->full_name}} ({{$value->login_id}})' data-department_id="{{$value->department_id}}">
                                                <i class='icon-plus3'></i>
                                            </span>
                                        </div>
                                    </li> 
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="card col-md-6">
                            <h6 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">Danh sách đã chọn</h6>
                            <div class="border user-list">
                                <ul class="listview member-choose pd0">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-1">
                        <button type="button" class="submit-member btn btn-primary mr-1 mb-1" data-dismiss="modal">Tìm kiếm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@stop
@section('script')
@parent
<script>
   $('body').delegate('.load-view','click',function(){
        var bdate = $('#day_start').val();
        var day = $(this).data('day');
        var action = $(this).data('action');
        $.ajax({
            url:'/api/load-schedule',
            method:'POST',
            data:{bdate:bdate,day:day,action:action},
            success: function(response){
                $('#schedule_groupweek_336').html(response.html);
                $('.displaydate').html(response.date);
            }
        })
   })
   $('body').delegate('.load-today','click',function(){
        var bdate = {{date('Y-md-d')}};
        var day = 0;
        var action = $(this).data('action');
        $.ajax({
            url:'/api/load-schedule',
            method:'POST',
            data:{bdate:bdate,day:day,action:action},
            success: function(response){
                $('#schedule_groupweek_336').html(response.html);
                $('.displaydate').html(response.date);
            }
        })
   })
   $('.todo-tap').click(function(){
        var status = $(this).data('status');
        $('.todo-tap').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            url:'/api/load-todo',
            method:'POST',
            data:{status:status},
            success: function(response){
                $('#list_todo').html(response.html);
                $('.check-list-todo').html(response.check_list_html);
            }
        })
   })
   $('body').delegate('.notifitap','click',function(){
        var status = $(this).data('status');
        $('.notifitap').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            url:'/api/load-notification',
            method:'POST',
            data:{status:status},
            success: function(response){
                $('#list_notification').html(response.html);
                $('.check-list-todo').html(response.check_list_html);
            }
        })
    })
   $('body').delegate('.complete-list-todo','click',function(){
        var id = [];
        $("input[type=checkbox][name='id[]']:checked").each(function(){
            id.push($(this).val());
        });
        $.ajax({
            url:'/api/complete-list-todo',
            method:'POST',
            data:{id:id},
            success: function(response){
                $('#list_todo').html(response.html);
            }
        })
   })
   $('body').delegate('.delete-list-todo','click',function(){
        var id = [];
        $("input[type=checkbox][name='id[]']:checked").each(function(){
            id.push($(this).val());
        });
        $.ajax({
            url:'/api/delete-list-todo',
            method:'POST',
            data:{id:id},
            success: function(response){
                $('#list_todo').html(response.html);
            }
        })
   })
</script>
<script language="JavaScript" type="text/javascript">
    var enable_shift_click = false;
    var element_name = 'id[]';
    function CheckAll()
    {
        var e = document.forms["todo/index"].elements;
        var l = e.length;
        var checked = false;
        for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" && ! e[i].checked  ) { checked = true;break; } }
        for( i = 0 ; i < l ; i ++ ) { if( e[i].name == "id[]" && e[i].style.display=="" ) { e[i].checked=checked; } }
        if( typeof setHightlight == 'function' )
            setHightlight();
    }

    applyShiftClick();

    function setHightlight()
    {
        if (!enable_shift_click)
            return;

        var table = jQuery("table.list_column").get(0);
        if (table) {
            var rows = jQuery(table).find("tr");
            rows.each(function (index, row) {
                var row_jquery_obj = jQuery(row);
                var chkbox = row_jquery_obj.find("input[type='checkbox']").eq(0);
                if (chkbox.length == 0)
                    return;

                if (chkbox.is(":checked")) {
                    row_jquery_obj.addClass("row_highlight");
                }
                else {
                    row_jquery_obj.removeClass("row_highlight");
                }

            });
        }
    }
    function applyShiftClick()
    {
        if (!enable_shift_click)
            return;

        var list = jQuery("table.list_column").get(0);
        var row = null;
        var row2 = null;
        if (list) {
            var chkboxs = fastGetElementsByName(list, 'input', element_name);
            var i = 0;
            jQuery.each(chkboxs, function (index, chkbox) {
                chkbox.index = i;

                var chkbox_jpuery_object = jQuery(chkbox);
                chkbox_jpuery_object.on("click", function (e) {
                    row = chkbox_jpuery_object.parents("tr").eq(0);
                    if (this.checked) {
                        row.addClass("row_highlight");
                    }
                    else {
                        row.removeClass("row_highlight");
                    }

                    //handle Shift click
                    var last_click = list.last_click;
                    var last_click_index = last_click ? last_click.index : 0;
                    var current = this;
                    var current_index = this.index;
                    var low = Math.min(last_click_index, current_index);
                    var high = Math.max(last_click_index, current_index);

                    if (e.shiftKey) {
                        var chkboxs2 = fastGetElementsByName(list, 'input', element_name);
                        jQuery(chkboxs2).each(function (index, item) {
                            row2 = jQuery(item).parents("tr").eq(0);

                            if (item.index >= low && item.index <= high) {
                                item.checked = current.checked;
                            }

                            if (item.checked) {
                                row2.addClass('row_highlight');
                            }
                            else {
                                row2.removeClass('row_highlight');
                            }
                        });
                    }
                    list.last_click = e.target;
                });
                i++;
            });
        }
    }
    $('#search_some_member').click(function(){
       $('#modal_search_member').modal('show');
   })
   $('body').delegate('.add-member-btn','click',function(){
        $('.member-choose').append(`<li class="d-flex align-items-center px-1">
                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                            <i class="icon-user"></i>
                                        </span>
                                        <div class="d-flex align-items-end justify-content-between w-100">
                                            <span class="attendees-name">`+$(this).data('name')+`</span>
                                            <span class="text-warning remove-member-btn" data-id='`+$(this).data('id')+`' data-name='`+$(this).data('name')+`'>
                                                <i class="icon-minus3"></i>
                                            </span>
                                        </div>
                                    </li>`);
        $(this).parent().parent().remove();
   });
   $('body').delegate('.remove-member-btn','click',function(){
        $('.member-list').append(`<li class="d-flex align-items-center px-1">
                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                            <i class="icon-user"></i>
                                        </span>
                                        <div class="d-flex align-items-end justify-content-between w-100">
                                            <span class="attendees-name">`+$(this).data('name')+`</span>
                                            <span class="text-warning add-member-btn" data-id='`+$(this).data('id')+`' data-name='`+$(this).data('name')+`'>
                                                <i class="icon-plus3"></i>
                                            </span>
                                        </div>
                                    </li>`);
        $(this).parent().parent().remove();
   });
   function convertViToEn(str, toUpperCase = false) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng
        str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư

        return toUpperCase ? str.toUpperCase() : str;
    }
   $('body').delegate('#input_search_member','keyup',function(){
        var keyword = $(this).val();
        if($('#select_attendees').val() == ''){
            var department_id = [];
        }else{
            var department_id = $('#select_attendees').val().split(",");
        }
        var convert_keyword = convertViToEn($(this).val());
        if(keyword != '' || department_id.length > 0){
            $('.add-member-btn').each(function(){
                if((($(this).data('name').indexOf(keyword) != -1 || convertViToEn($(this).data('name')).indexOf(convert_keyword) != -1)) &&
                        (department_id.includes(String($(this).data('department_id'))) || department_id.length == 0)){
                    $(this).parent().parent().removeClass('d-none');
                }else{
                    $(this).parent().parent().addClass('d-none');
                }
            });
        }else{
            $('.li-add').removeClass('d-none');
        }
   })
   $('body').delegate('#select_attendees','change',function(){
        if($(this).val() == ''){
            var department_id = [];
        }else{
            var department_id = $(this).val().split(",");
        }
        var keyword = $('#input_search_member').val();
        var convert_keyword = convertViToEn($(this).val());
        if(keyword != '' || department_id.length > 0){
            $('.add-member-btn').each(function(){
                if((($(this).data('name').indexOf(keyword) != -1 || convertViToEn($(this).data('name')).indexOf(convert_keyword) != -1) || keyword == '') &&
                    department_id.includes(String($(this).data('department_id')))){
                    $(this).parent().parent().removeClass('d-none');
                }else{
                    $(this).parent().parent().addClass('d-none');
                }
            });
        }else{
            $('.li-add').removeClass('d-none');
        }
   })
   $('body').delegate('.submit-member','click',function(){
         if($('.remove-member-btn').length){
            var uids = [];
            $('.remove-member-btn').each(function(){
                uids.push(parseInt($(this).data('id')));
            });
            var url = window.location.href.split('?')[0]+'/schedule/index?uid='+uids.join(',');
            window.location = url;
         }
   })
   function formatResult(node) {
        var level = 0;
        if(node.element !== undefined){
          level = (node.element.className);
          if(level.trim() !== ''){
            level = (parseInt(level.match(/\d+/)[0]));
          }
        }
        var $result = $('<span style="padding-left:' + (20 * level) + 'px;">' + node.text + '</span>');
        return $result;
     };
     $(".select2").select2({
        allowClear: true,
        templateResult: formatResult,
      });
 </script>
@stop
