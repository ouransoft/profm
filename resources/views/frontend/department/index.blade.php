@extends('frontend.home.home')
@section('content')
<link rel="stylesheet" href="{!!asset('/assets2/css/treeview.css')!!}">
<div class="content project-content">
    <h4 style="margin-top:25px;text-transform: uppercase">
        <img style="margin-right: 12px;width:45px;" src="{{\App\Config::first()->favicon()->first() ? asset(\App\Config::first()->favicon()->first()->link) : ''}}">
        {{$share_config->company_name}}
    </h4>
    <ul class="tree">
        {!!$html!!}
    </ul>
</div>
<script>
$('body').delegate('.add-icon','click',function(){
    if($(this).data('parent_id') !== ''){
        $(this).html('<i class="fas fa-chevron-circle-down"></i>');
        $(this).parent().parent().append(`<li>
                                                <a href="javascript:void(0)" class="add-icon" data-parent_id='`+$(this).data('parent_id')+`' data-level='`+$(this).data('level')+`'>
                                                   <i class="fas fa-chevron-circle-right"></i>
                                                </a>
                                           </li>`);
        $(this).parent('li').append(`<div class="department-detail-parent">
                                        <div class="department-detail">
                                            <input class="add-content" data-parent_id='`+$(this).data('parent_id')+`' data-level='`+$(this).data('level')+`'>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0)" class="add-icon" data-parent_id=''>
                                                <i class="icon-plus-circle2"></i>
                                            </a>
                                        </li>
                                    </ul>`);
        $(this).removeClass('add-icon');
        $(this).addClass('hidden-icon');
    }
})
$('body').delegate('.hidden-icon','click',function(){
    $(this).next().next().attr('style','display:none');
    $(this).html('<i class="fas fa-chevron-circle-right"></i>');
    $(this).removeClass('hidden-icon');
    $(this).addClass('show-icon');
})
$('body').delegate('.show-icon','click',function(){
    $(this).next().next().attr('style','display:block');
    $(this).html('<i class="fas fa-chevron-circle-down"></i>');
    $(this).removeClass('show-icon');
    $(this).addClass('hidden-icon');
})
$('body').delegate('.add-content','keypress',function(e){
    if(e.which === 13){
        var content = $(this).val();
        var parent_id = $(this).data('parent_id');
        var level = $(this).data('level');
        var $this = $(this);
        $.ajax({
                url:'/api/add-department',
                method:'POST',
                data:{name:content,parent_id:parent_id,level:level},
                success: function(response){
                    if(response.success == true){
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Tạo mới thành công");
                        notification.push();
                        $this.parent().parent().parent('li').find('ul').html(`<li>
                                                                                <a href="javascript:void(0)" class="add-icon" data-parent_id="`+response.id+`" data-level='`+(parseInt(response.level) + 1)+`'>
                                                                                    <i class="icon-plus-circle2"></i>
                                                                                </a>
                                                                              </li>`);
                        $this.parent().html(`<span style="width:40%">`+content+`</span>
                               <div class="department-action">
                                   <a><span class="icons icons-user-dark" style="float: none;top:4px;margin-right: 5px"></span>0</a>
                                   <a><span class="icons icons-month" style="float: none;top:4px;margin-right: 5px"></span>0</a>
                                   <a><span class="icons icons-quarter" style="float: none;top:4px;margin-right: 5px"></span>0</a>
                                   <a><span class="icons icons-year" style="float: none;top:4px;margin-right: 5px"></span>0</a>
                                   <div class="action">
                                       <a href="javascript:void(0)" class="edit-department" data-id="`+response.id+`"><i class="fas fa-edit"></i></a>
                                       <a href="javascript:void(0)" class="delete-department" data-id="`+response.id+`"><i class="icon-bin"></i></a>
                                   </div>
                               </div>`);
                    }else{
                        var notifier = new Notifier();
                        var notification = notifier.notify("warning", response.message);
                        notification.push();
                    }
                }
            })
    }
})
$('body').delegate('.edit-department','click',function(){
    var content = $(this).parents('.department-detail').find('.content-department span').html();
    $(this).parents('.department-detail').find('.content-department').html('<input class="edit-content" value="'+content+'" data-id="'+$(this).data('id')+'">');
})
$('body').delegate('.edit-content','keypress',function(e){
    if(e.which === 13){
        var id = $(this).data('id');
        content = $(this).val();
        $.ajax({
                url:'/api/update-department',
                method:'POST',
                data:{name:content,id:id},
                success: function(response){
                    if(response.success == true){
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Cập nhật thành công");
                        notification.push();
                    }
                }
         })
         $(this).parent().html('<span>'+content+'</span>');
    }
})
$('body').delegate('.delete-department','click',function(e){
    var $this = $(this);
    var id = $(this).data('id');
    bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
        if (result === true) {
            $.ajax({
                    url:'/api/delete-department',
                    method:'POST',
                    data:{id:id},
                    success: function(response){
                        $this.parents('.department-detail-parent').parent('li').remove();
                    }
             })
        }
    });
})
</script>
@stop
