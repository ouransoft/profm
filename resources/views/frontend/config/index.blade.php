@extends('frontend.home.home')
@section('content')
<div class="content project-content">
    <h4 style="margin-top:25px">{{trans('base.Manage_website_configuration')}}</h4>
    <div class="form-create">
        <form method="POST" action="{{route('frontend.config.update')}}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
            <div class="form-group row">
                <label class="col-md-2 required control-label text-right text-semibold">Logo website:</label>
                <div class="col-md-6 div-image">
                    <div class="file-input file-input-ajax-new">
                        <div class="input-group file-caption-main">
                            <div class="input-group-btn input-group-append">
                                <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                    <input type="file" class="upload-image" name="image_upload" data-fouc="" data-type="{{\App\File::TYPE_LOGO}}">
                                </div>
                            </div>
                        </div>
                        <div class="file-preview ">
                            <div class=" file-drop-zone">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="image-link" value="{{$record->logo()->first() ? $record->logo()->first()->link : ''}}">
                    <input type="hidden" name="image" class="image-id" value="{{$record->logo()->first() ? $record->logo()->first()->id : ''}}">
                    <input type="hidden" class="image-name" value="{{$record->logo()->first() ? $record->logo()->first()->name : ''}}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 required control-label text-right text-semibold">Favicon: </label>
                <div class="col-md-6 div-image">
                    <div class="file-input file-input-ajax-new">
                        <div class="input-group file-caption-main">
                            <div class="input-group-btn input-group-append">
                                <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                    <input type="file" class="upload-image"  name="favicon_upload" data-fouc="" data-type="{{\App\File::TYPE_FAVICON}}">
                                </div>
                            </div>
                        </div>
                        <div class="file-preview ">
                            <div class=" file-drop-zone">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="image-link" value="{{$record->favicon()->first() ? $record->favicon()->first()->link : ''}}">
                    <input type="hidden" name="favicon" class="image-id" value="{{$record->favicon()->first() ? $record->favicon()->first()->id : ''}}">
                    <input type="hidden" class="image-name" value="{{$record->favicon()->first() ? $record->favicon()->first()->name : ''}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label text-right">{{trans('base.Website_name')}}</label>
                <div class="col-sm-6">
                    <input class="form-control" name="title" type="text" value="{{$record->title}}"> 
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label text-right">{{trans('base.font_size')}}</label>
                <div class="col-sm-6">
                    <input class="form-control" name="font_size" type="text" value="{{$record->font_size}}"> 
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 required control-label text-right text-semibold">{{trans('base.FAQ')}}</label>
                <div class="col-md-6">
                    <textarea class="form-control" id="content_question" name="content">{!!$record->content!!}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <button class="btn btn-submit" type="submit" style="background:#fd7700;"><img src="/public/assets2/img/send.png">  {{trans('base.Save')}}</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/project.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
@if (Session::has('success'))
<script>
    var notifier = new Notifier();
    var notification = notifier.notify("success", "Cập nhật thành công");
    notification.push();
</script>
@endif
<script>
    ClassicEditor
            .create( document.querySelector('#content_question') )
            .catch( error => {
                console.error( error );
    });
</script>
@stop