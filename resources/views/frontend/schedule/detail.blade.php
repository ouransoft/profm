@extends('frontend.layouts.admin')
@section('content')
<body class="page-body">
   <div class="sidebar-top">
      <ul class="pd0">
         <li>
            <a href="#">
               <div class="">
                  <img src="{{asset('/img/Portal.png')}}">
                  <h6>Protal</h6>
               </div>
            </a>
         </li>
         <li>
            <a href="#">
               <div class="">
                  <img src="{{asset('/img/Scheduler.png')}}">
                  <h6>Scheduler</h6>
               </div>
            </a>
         </li>
         <li>
            <a href="#">
               <div class="">
                  <img src="{{asset('/img/Bullentin_Board.png')}}">
                  <h6>Bullentin Board</h6>
               </div>
            </a>
         </li>
         <li>
            <a href="#">
               <div class="">
                  <img src="{{asset('/img/Cabinet.png')}}">
                  <h6>Cabinet</h6>
               </div>
            </a>
         </li>
         <li>
            <a href="#">
               <div class="">
                  <img src="{{asset('/img/To_do_list.png')}}">
                  <h6>To-do List</h6>
               </div>
            </a>
         </li>
         <li>
            <a href="#">
               <div class="">
                  <img src="{{asset('/img/Time set.png')}}">
                  <h6>Time set</h6>
               </div>
            </a>
         </li>
         <li>
            <a href="#">
               <div class="">
                  <img src="{{asset('/img/Report.png')}}">
                  <h6>Report</h6>
               </div>
            </a>
         </li>
         <li>
            <a href="#">
               <div class="">
                  <img src="{{asset('/img/kaizen.png')}}">
                  <h6>Change good</h6>
               </div>
            </a>
         </li>
      </ul>
      <div class="sidebar-bottom">
         <div class="button-off">
            <a class="sidebar-top-off on"><i class="fas fa-chevron-up"></i></a>
         </div>
      </div>
    </div>
    <div class="global_navi" role="heading" aria-level="2">
        <a href="/schedule/index?gid=&amp;from_portal=1&amp;plid=336" style="height: 34px;vertical-align: middle;line-height: 50px;display: inline-block;padding: 0px 30px;font-size: 20px;border-right: 1px solid #333;margin-top: 10px;">
            <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="" style="margin-bottom: 10px;width: 30px;">
           Scheduler
        </a>
        <span class="globalNavi-item-last-grn" style="padding: 0 20px;top:10px;">
           Apointerment details
        </span>
    </div>
    <div class="mainarea ">
   <script>
      grn.component.url.PAGE_PREFIX = "";
      grn.component.url.PAGE_EXTENSION = "";
      grn.component.url.STATIC_URL = "/garoon3";
      grn.component.url.BUILD_DATE = "20200925.text";
   </script>    <script src="/garoon3/fw/jquery/jquery-ui-1.12.1.custom.min.js?20200925.text" type="text/javascript"></script>
   <script src="/garoon3/grn/html/component/window_simple_dialog.js?20200925.text" type="text/javascript"></script>
   <script id="template_window_simple_dialog_v2" type="text/template">
      <div class="subWindowBase-grn" id="window_dialog_v2" role="dialog">
          <div class="subwindow_title_grn" id="window_dialog_header">
            <span class="subwindow_title_base_grn nowrap-grn"><a href="javascript:void(0)" id="back_button" class="icon_back_grn icon_inline_grn mLeft10">戻る</a><h2 id="window_title" class="subWindowTitleText-grn nowrap-grn inline_block_grn"></h2></span>
            <a href="javascript:;" role="button" title="閉じる" aria-label="閉じる">
                <div id="window_dialog_close" class="subWindowClose-grn"></div>
            </a>
            <div class="clear_both_0px"></div>
          </div>
          <div class="subWindowContent-grn" id="window_content">
              <div class="content" style="min-height: 120px;">
                  <div class="tAlignCenter-grn" style=" margin-top: 30px; margin-right: auto; margin-left: auto; width: 570px; height: 90px;">
                  </div>
              </div>
          </div>
      </div>
   </script>
   <script id="template_window_simple_dialog_v1" type="text/template">
      <div class="subWindowBase-grn" id="window_dialog_v1" style="display:none;">
          <table class="subWindowTable-grn">
              <tr id="window_dialog_header">
                  <td class="subWindowTitleLeft-grn"><div id="window_title" class="subWindowTitleText-grn"></div></td>
                  <td class="subWindowTitleRight-grn"><div id="window_dialog_close"  class="subWindowClose-grn" title="閉じる"></div></td>
              </tr>
              <tr>
                  <td colspan="2">
                      <div class="subWindowContent-grn" id="window_content">
                          <img src="/garoon3/grn/image/cybozu/spinner.gif?20200925.text" border="0" alt="">
                      </div>
                  </td>
              </tr>
          </table>
      </div>
   </script><script src="/garoon3/grn/html/component/member_list_dialog.js?20200925.text" type="text/javascript"></script>
   <script src="/garoon3/grn/html/page/schedule/view.js?20200925.text" type="text/javascript"></script>
   <script src="/garoon3/js/dist/schedule.js?20200925.text" type="text/javascript"></script>
   <div class="mainareaSchedule-grn">
      <!--menubar-->
      <div class="fullmenu_grn">
         <div id="main_menu_part">
            <div class="mainMenuGroup-grn">
               <span class="menu_item"><span class="nowrap-grn "><a href="/schedule/repeat_modify?event=985&amp;bdate=2020-12-23&amp;uid=6&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="{{asset('/img/modify20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thay đổi </font></font></a></span>
               </span>
               <span class="menu_item"><span class="nowrap-grn "><a href="/schedule/delete?event=985&amp;bdate=2020-12-23&amp;uid=6&amp;gid=&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="{{asset('img/delete20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Xóa Xóa </font></font></a></span></span>
               <span class="menu_item"><span class="nowrap-grn "><a href="/schedule/repeat_add?event=985&amp;bdate=2020-12-23&amp;uid=6&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="{{asset('img/reuse20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sử dụng lại </font></font></a></span>
               </span>
               <span class="menu_item"><span class="nowrap-grn "><a href="/schedule/leave?event=985&amp;bdate=2020-12-23&amp;uid=6&amp;gid=&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="{{asset('/img/out_schedule20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thoát khỏi cuộc hẹn này </font></font></a></span></span>
               <span class="menu_item">
                  <script src="/garoon3/grn/html/display_options.js?20200925.text" type="text/javascript"></script>
                  <link href="/garoon3/grn/html/display_options.css?20200925.text" rel="stylesheet" type="text/css">
                  <script type="text/javascript" language="javascript">
                     <!--
                     
                     GRN_DisplayOptions['_view'] = GRN_DisplayOptionFactory.create();
                     
                     GRN_DisplayOptions['_view'].setPage('');
                     GRN_DisplayOptions['_view'].setCSRFTicket('4074830fe66b251b311813bed0f3394d');
                     GRN_DisplayOptions['_view'].setListID('');
                     GRN_DisplayOptions['_view'].setPLID('_view');
                     jQuery(document).ready(function(){
                         GRN_DisplayOptions['_view'].init();
                     });
                     
                     //-->
                  </script>
                  <span class="display_options">
                     <span><a id="display_options_switch_view" href="javascript:void(0);"><img src="/garoon3/grn/image/cybozu/image-common/menu20.png?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"></font><span id="display_options_down_allow_view"><img src="/garoon3/grn/image/cybozu/arrow_down_single_gray.gif?20200925.text" border="0" alt="" class="menu_arrow_grn"></span><span id="display_options_up_allow_view" style="display:none;"><img src="/garoon3/grn/image/cybozu/arrow_up_single_gray.gif?20200925.text" border="0" alt="" class="menu_arrow_grn"></span></a></span>
                     <span id="display_options_dialog_view" class="display_options_dialog" style="display: none; left: 942px; top: 232px;">
                        <ul>
                           <li class="options_dialog_subtitle_grn text_color_sub_grn bold_grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Xem các cuộc hẹn của người tham dự / cơ sở</font></font></li>
                           <li class="display_options_dialog_ch">
                              <span class="nowrap-grn "><a href="/schedule/group_day?event=985&amp;event_date=2020-12-23&amp;bdate=2020-12-23&amp;uid=6&amp;gid=virtual"><img src="/garoon3/grn/image/cybozu/cal_gday20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ngày nhóm</font></font></a></span>
                           </li>
                           <li class="display_options_dialog_ch">
                              <span class="nowrap-grn "><a href="/schedule/index?event=985&amp;event_date=2020-12-23&amp;bdate=2020-12-23&amp;uid=6&amp;gid=virtual"><img src="/garoon3/grn/image/cybozu/cal_gweek20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tuần nhóm</font></font></a></span>
                           </li>
                           <li class="display_options_dialog_ch">
                              <span class="nowrap-grn "><a href="/schedule/personal_day?event=985&amp;event_date=2020-12-23&amp;bdate=2020-12-23&amp;uid=6&amp;gid=selected#members=g.7-u.6-u.3-u.51-u.19-u.14"><img src="/garoon3/grn/image/cybozu/cal_pday20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ngày</font></font></a></span>
                           </li>
                           <li class="display_options_dialog_ch">
                              <span class="nowrap-grn "><a href="/schedule/personal_week?event=985&amp;event_date=2020-12-23&amp;bdate=2020-12-23&amp;uid=6&amp;gid=selected#members=g.7-u.6-u.3-u.51-u.19-u.14"><img src="/garoon3/grn/image/cybozu/cal_pweek20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">tuần</font></font></a></span>
                           </li>
                           <li class="partition_options_dialog_grn">
                              <div class="border_partition_pulldown_grn"></div>
                           </li>
                           <li class="display_options_dialog_ch">
                              <a href="/space/space_add_from_schedule?eid=985"><img src="/garoon3/grn/image/cybozu/space20.png?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tạo không gian</font></font></a>
                           </li>
                        </ul>
                     </span>
                     <iframe id="iframe-breadcrumb_view" class="iframe-breadcrumb" style="display:none;" src=""></iframe>
                  </span>
               </span>
               <span class="menu_item">
               <span class="extension_main_part"><span class="display_report_detail_schedule" onclick="display_on_off('display_report_open:display_report_swith_image_open:display_report_swith_image_close')">
               <span id="display_report_swith_image_open">
               <img src="/garoon3/grn/image/cybozu/report20.gif?20200925.text" border="0" alt=""><a href="javascript:void(0);"><font style="vertical-align: inherit;"></font></a><img src="/garoon3/grn/image/cybozu/arrow_down_single_gray.gif?20200925.text" border="0" alt="" class="menu_arrow_grn">
               </span>
               <span id="display_report_swith_image_close" style="display:none;">
               <img src="/garoon3/grn/image/cybozu/report20.gif?20200925.text" border="0" alt=""><a href="javascript:void(0);"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Báo cáo</font></font></a><img src="/garoon3/grn/image/cybozu/arrow_up_single_gray.gif?20200925.text" border="0" alt="" class="menu_arrow_grn">
               </span>
               </span>
               </span>
               </span>
               <span class="menu_item">
               </span>
            </div>
            <div class="clear_both_0px"></div>
            <div class="menuReportBase-grn" id="display_report_open" style="display:none;">
               <div class="menuReport-grn">
                  <span class="extension_main_menu_part">
                  <span class="menu_item"><img src="/garoon3/grn/image/cybozu/report20.gif?20200925.text" border="0" alt=""><a href="javascript:;" onclick="showFormSelectWindow();"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tạo </font></font></a></span>
                  <font style="vertical-align: inherit;"><span class="menu_item"><a href="javascript:;" onclick="showReportRelationWindow(0);"><font style="vertical-align: inherit;">báo cáo Kết hợp với báo cáo</font></a></span></font><span class="menu_item">
                  <img src="/garoon3/grn/image/cybozu/reportrelation20.gif?20200925.text" border="0" alt="">
                  <a href="javascript:;" onclick="showReportRelationWindow(0);"><font style="vertical-align: inherit;"></font></a>
                  </span>                                                                    </span>
                  <span class="nowrap-grn">
                  <span class="icon-informationSub-grn mLeft10 mRight3"></span><span id="repeat_event_warnning" class="messageSub-grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sau khi đã đặt, cuộc hẹn cho ngày này sẽ không còn là cuộc hẹn lặp lại.</font></font></span>
                  </span>
               </div>
               <div></div>
               <div class="clear_both_0px"></div>
            </div>
         </div>
         <link href="/garoon3/grn/html/report.css?20200925.text" rel="stylesheet" type="text/css">
         <script src="/garoon3/grn/html/report.js?20200925.text" type="text/javascript"></script>
         <div class="cover" style="display: none; width: 1903px; height: 2043px;" id="background"></div>
         <script language="JavaScript" type="text/javascript">
            <!--
                var eid = "985";
                var date = "2020-12-23";
                var selectReportListUrl = "/report/ajax/select_report_list?";
                var categoryListUrl = "/report/ajax/category_list?";
                var plusImage = '/garoon3/grn/image/cybozu/plus.gif';
                var minusImage = '/garoon3/grn/image/cybozu/minus.gif';    
            
                var formListUrl = "/report/ajax/send_form_list?";
                var userType = "view";
                var csrf_ticket = "4074830fe66b251b311813bed0f3394d";
            
                function disableButtons(form){
                    jQuery("#reportRelationForm input[type='button']").each(function(){jQuery(this).prop("disabled",true);});
                    jQuery("#reportRelationForm input[type='radio']").each(function(){jQuery(this).prop("disabled",true);});
                    jQuery("#reportRelationForm input[type='text']").each(function(){jQuery(this).prop("disabled",true);});
                    reportRelationNaviCallBack = function(){};
                    document.onkeydown = function(){};
                    document.getElementById("closeRelationDialog").onclick = function(){};
                    document.body.style.cursor = 'progress';
                }
                //-->
         </script>
         <div style="display:none;">
            <font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><input type="submit" value="Vâng "></font></font>
            <font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><input type="button" value="không" onclick="closeReportRelationConfirmWindow();"></font></font>
         </div>
         <div id="reportRelationWindow" class="msgbox" style="display:none;">
            <div class="title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
               Chọn báo cáo để liên kết
               </font></font><a id="closeRelationDialog" style="position: absolute; right: 5px;top:5px;text-decoration:none;" onclick="closeReportRelationWindow();" href="javascript:;"><img src="/garoon3/grn/image/cybozu/close20.gif?20200925.text" border="0" alt=""></a>
            </div>
            <form method="post" id="reportRelationForm" action="/report/command_report_schedule_relation?">
               <div class="content" style="position:relative;">
                  <div style="position:absolute;right:20px;top:5px;">
                     <span style="width:20px;">
                     <img border="0" align="top" id="reportLoadingImage" src="/garoon3/grn/image/cybozu/spinner.gif?20200925.text">
                     </span>
                     <input type="text" id="searchReportText" onkeypress="return redirectEnterkey(event.keyCode,'selectReportSearch');">&nbsp;<font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><input type="button" id="selectReportSearch" value="Tìm kiếm" onclick="showReportRelationWindow(0);"></font></font>&nbsp;
                  </div>
                  <div id="reportRelationContent">
                  </div>
                  <br><span class="sub_explanation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Khi bạn kết hợp báo cáo, cuộc hẹn cho ngày này không còn là cuộc hẹn lặp lại.</font></font></span>
                  <div class="mTop10 tAlignCenter-grn"><span id="reportRelationSubmit" class="button_grn_js button1_main_grn button_disable_filter_grn button1_r_margin2_grn" onclick="grn.component.button.util.submit(this);" data-auto-disable="true"><a href="javascript:void(0);" role="button" aria-disabled="true"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Liên kết</font></font></a></span><span id="report_button_cancel" class="button_grn_js button1_normal_grn" onclick="closeReportRelationWindow();"><a href="javascript:void(0);" role="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Huỷ bỏ</font></font></a></span></div>
               </div>
               <input type="hidden" name="addFlg" value="1">
               <input type="hidden" name="csrf_ticket" value="4074830fe66b251b311813bed0f3394d">
               <input type="hidden" name="eid" value="985">
               <input type="hidden" name="date" value="2020-12-23">
               <input type="hidden" name="from" value="schedule">
            </form>
         </div>
         <script type="text/javascript" src="/garoon3/fw/yui/build/yahoo/yahoo-min.js?20200925.text"></script>
         <script type="text/javascript" src="/garoon3/fw/yui/build/treeview/treeview.js?20200925.text"></script>
         <div id="categoryFormListWindow" class="msgbox" style="display:none;">
            <div class="title">
               <a class="categoryFormListWindow_close" onclick="closeFormSelectWindow();" href="javascript:;"><img src="/garoon3/grn/image/cybozu/close20.gif?20200925.text" border="0" alt=""></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
               Chọn hình thức
               </font></font>
            </div>
            <div id="categoryFormListWindow_inner" class="content">
               <table class="categoryFormListWindow_table">
                  <tbody>
                     <tr valign="top">
                        <td style="width:50%;">
                           <div id="tree_part" class="categoryFormListWindow_tree_part">
                              <script type="text/javascript" src="/garoon3/fw/yui/build/yahoo/yahoo-min.js?20200925.text"></script>
                              <script type="text/javascript" src="/garoon3/fw/yui/build/event/event-min.js?20200925.text"></script>
                              <script type="text/javascript" src="/garoon3/fw/yui/build/dom/dom-min.js?20200925.text"></script>
                              <script type="text/javascript" src="/garoon3/fw/yui/build/treeview/treeview-min.js?20200925.text"></script>
                              <script type="text/javascript" src="/garoon3/fw/yui/build/connection/connection-min.js?20200925.text"></script>
                              <script type="text/javascript" src="/garoon3/grn/html/org_tree_26.js?20200925.text"></script>
                              <script language="javascript">
                                 <!--
                                 YAHOO.grn.orgTree.csrf_ticket= "4074830fe66b251b311813bed0f3394d";
                                 YAHOO.grn.orgTree.app_path= "/garoon3";
                                 //-->
                              </script>
                              <div id="tree_view" class="tree-view">
                                 <span id="folder_tree_top">
                                 <span class="nowrap-grn "><a class="tree-select-current" href="javascript:onSelectCategory('top');"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">(nguồn gốc)</font></font></a></span>
                                 </span>
                                 <div id="folder_tree">
                                    <div class="ygtvitem" id="ygtv0">
                                       <div class="ygtvchildren" id="ygtvc0">
                                          <div class="ygtvitem" id="ygtv1">
                                             <table id="ygtvtableel1" border="0" cellpadding="0" cellspacing="0" class="ygtvtable ygtvdepth0 ygtv-collapsed ygtv-highlight0">
                                                <tbody>
                                                   <tr class="ygtvrow">
                                                      <td id="ygtvt1" class="ygtvcell ygtvtn"><a href="#" class="ygtvspacer"></a></td>
                                                      <td id="ygtvcontentel1" class="ygtvcell ygtvhtml ygtvcontent">
                                                         <div class="tree-node"><a id="folder_tree-node--1" href="javascript:onSelectCategory(-1)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">(Chưa được phân loại)</font></font></a> <span class="tree-unread-num" style="display:none;" id="tree-unread-num--1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0</font></font></span></div>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                             <div class="ygtvchildren" id="ygtvc1" style="display:none;"></div>
                                          </div>
                                          <div class="ygtvitem" id="ygtv2">
                                             <table id="ygtvtableel2" border="0" cellpadding="0" cellspacing="0" class="ygtvtable ygtvdepth0 ygtv-collapsed ygtv-highlight0">
                                                <tbody>
                                                   <tr class="ygtvrow">
                                                      <td id="ygtvt2" class="ygtvcell ygtvln"><a href="#" class="ygtvspacer"></a></td>
                                                      <td id="ygtvcontentel2" class="ygtvcell ygtvhtml ygtvcontent">
                                                         <div class="tree-node"><a id="folder_tree-node-2" href="javascript:onSelectCategory(2)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mẫu đơn</font></font></a> <span class="tree-unread-num" style="display:none;" id="tree-unread-num-2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0</font></font></span></div>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                             <div class="ygtvchildren" id="ygtvc2" style="display:none;"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <script language="javascript">
                                 <!--
                                     var tree_id   = 'folder_tree';
                                     var async_url = '/report/category_json?';
                                     var page_name = 'schedule%2Fview';
                                     var link_url  = '/schedule/view?';
                                     var trash_id  = null;
                                     var deleted_folder_caption = "削除済み";
                                     var update_group_caption = "更新予定";
                                     var new_group_caption = "新規";
                                     var selected_oid = 0;
                                     var tree_data = [
                                 {
                                 "oid":"-1",
                                     "name":"（未分類）",
                                     "expanded":"",
                                     "count":"0",
                                          "link_url":"\/scripts\/garoon\/grn.exe\/schedule\/view?top=1",                            "children":[]
                                 },
                                 {
                                 "oid":"2",
                                     "name":"サンプルフォーム",
                                     "expanded":"",
                                     "count":"0",
                                                                     "children":[]
                                 },
                                 ];
                                     var tree_params = '';
                                     var obj_folder_tree = new YAHOO.grn.orgTree(tree_id, async_url, page_name, link_url, selected_oid, tree_params);
                                     obj_folder_tree.setOnSelect('onSelectCategory');
                                     obj_folder_tree.setOidKey('cid');    obj_folder_tree.setSubscribeTitle('更新通知の設定がON');
                                     YAHOO.util.Event.onDOMReady(function()
                                     {
                                         obj_folder_tree.initWithNumber(tree_data);
                                     });
                                 //-->
                              </script>                            
                           </div>
                        </td>
                        <script type="text/javascript" src="/garoon3/fw/yui/build/yahoo/yahoo-min.js?20200925.text"></script>
                        <script language="javascript">
                           <!--
                               YAHOO.util.Event.onDOMReady(function()
                               {
                                   onSelectCategory(1);
                                   jQuery(window).resize(function(){
                                       var body = document.body;
                                       jQuery('#background').width(Math.max(body.clientWidth,body.scrollWidth)).height(Math.max(body.clientHeight,body.scrollHeight));
                                   });
                               });
                           //-->
                        </script>
                        <td width="50%">
                           <div id="view_part" class="categoryFormListWindow_contents">
                              <img border="0" align="top" id="reportLoadingImage" src="/garoon3/grn/image/cybozu/spinner.gif?20200925.text" style="display: none;">
                              <div class="margin_bottom">
                                 <div>
                                    <span class="bold"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">(nguồn gốc)</font></font></span>
                                 </div>
                              </div>
                              <div class="margin_bottom"></div>
                              <style type="text/css">
                                 <!-- 
                                    .report_category_memo{
                                      width:98%;
                                      padding:0.8em 0.3em 0.5em 0.8em;
                                      background-color:#ffffff;
                                      border:3px double #ccccdd;
                                      font-size:90%;
                                      letter-spacing:0.1em;
                                      line-height:120%;
                                    }
                                    -->
                              </style>
                              <div style="height:1.0em;"></div>
                              <style type="text/css">
                                 <!-- 
                                    .category_hr
                                    {
                                      font-weight:bold;
                                      padding:5px,10px,5px,10px;
                                      border-bottom:2px dotted #999999;
                                    }
                                    -->
                              </style>
                              <p class="category_hr"></p>
                              <div class="margin_bottom">
                                 <div>
                                    <span class="bold"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mẫu được chọn gần đây</font></font></span>
                                 </div>
                              </div>
                              <nobr>
                                 <span class="nowrap-grn "><a href="/report/send_form?cid=2&amp;fid=3&amp;eid=985&amp;date=2020-12-23"><img src="/garoon3/grn/image/cybozu/report20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phút ②</font></font></a></span>
                              </nobr>
                              <blockquote style="margin-top:0px">
                                 <tt>
                                    <font size="+0">
                                       <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đây là biểu mẫu ghi lại thành phần tham dự và nội dung cuộc họp. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bạn cũng có thể tạo báo cáo bằng cách sử dụng biểu mẫu này từ liên kết "Tạo Báo cáo" trên màn hình chi tiết lịch biểu.</font></font></pre>
                                    </font>
                                 </tt>
                              </blockquote>
                              <nobr>
                                 <span class="nowrap-grn "><a href="/report/send_form?cid=2&amp;fid=5&amp;eid=985&amp;date=2020-12-23"><img src="/garoon3/grn/image/cybozu/report20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phút②</font></font></a></span>
                              </nobr>
                              <blockquote style="margin-top:0px">
                                 <tt>
                                    <font size="+0">
                                       <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;"></pre>
                                    </font>
                                 </tt>
                              </blockquote>
                              <nobr>
                                 <span class="nowrap-grn "><a href="/report/send_form?cid=2&amp;fid=12&amp;eid=985&amp;date=2020-12-23"><img src="/garoon3/grn/image/cybozu/report20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bản ghi nhớ cơ hội</font></font></a></span>
                              </nobr>
                              <blockquote style="margin-top:0px">
                                 <tt>
                                    <font size="+0">
                                       <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;"></pre>
                                    </font>
                                 </tt>
                              </blockquote>
                              <nobr>
                                 <span class="nowrap-grn "><a href="/report/send_form?cid=2&amp;fid=1&amp;eid=985&amp;date=2020-12-23"><img src="/garoon3/grn/image/cybozu/report20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">báo cáo</font></font></a></span>
                              </nobr>
                              <blockquote style="margin-top:0px">
                                 <tt>
                                    <font size="+0">
                                       <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Định dạng báo cáo có mục đích chung.</font></font></pre>
                                    </font>
                                 </tt>
                              </blockquote>
                              <nobr>
                                 <span class="nowrap-grn "><a href="/report/send_form?cid=2&amp;fid=2&amp;eid=985&amp;date=2020-12-23"><img src="/garoon3/grn/image/cybozu/report20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phút ①</font></font></a></span>
                              </nobr>
                              <blockquote style="margin-top:0px">
                                 <tt>
                                    <font size="+0">
                                       <pre class="format_contents" style="white-space:-moz-pre-wrap;white-space:pre-wrap;display:inline;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đây là biểu mẫu ghi lại thành phần tham dự và nội dung cuộc họp. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bạn cũng có thể tạo báo cáo bằng cách sử dụng biểu mẫu này từ liên kết "Tạo Báo cáo" trên màn hình chi tiết lịch biểu.</font></font></pre>
                                    </font>
                                 </tt>
                              </blockquote>
                           </div>
                        </td>
                     </tr>
                  </tbody>
               </table>
               &nbsp;&nbsp;<span class="sub_explanation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Khi bạn tạo báo cáo, cuộc hẹn cho ngày này không còn là cuộc hẹn lặp lại.</font></font></span>
            </div>
         </div>
         <!--menubar_end-->
         <div id="display_day_open" style="display:none;">
            <div class="groupDayCalendarDate-grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thứ tư, ngày 23 tháng 12 năm 2020</font></font></div>
            <table class="day_table group_day_calendar">
               <tbody>
                  <tr class="day_table_time_login">
                     <td class="view_calendar_timebar1"><br></td>
                     <td class="view_calendar_timebar2"><br></td>
                     <td align="center" class="m" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">số 8</font></font></td>
                     <td align="center" class="m" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">9</font></font></td>
                     <td align="center" class="m" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mười</font></font></td>
                     <td align="center" class="m" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">11</font></font></td>
                     <td align="center" class="e" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12</font></font></td>
                     <td align="center" class="e" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">13</font></font></td>
                     <td align="center" class="e" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">14</font></font></td>
                     <td align="center" class="e" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">15</font></font></td>
                     <td align="center" class="e" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">16</font></font></td>
                     <td align="center" class="e" colspan="6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">17</font></font></td>
                  </tr>
                  <tr class="day_table_time_login">
                     <td class="view_calendar_timebar_sec1"><img src="/garoon3/grn/image/cybozu/spacer1.gif?20200925.text" border="0" alt=""></td>
                     <td class="view_calendar_timebar_sec2"><img src="/garoon3/grn/image/cybozu/spacer1.gif?20200925.text" border="0" alt=""></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="m"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                     <td class="e"></td>
                  </tr>
                  <tr>
                     <td class="view_calendar_user">
                        <a href="/schedule/user_view?uid=6&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="/garoon3/grn/image/cybozu/loginuser20.gif?20200925.text" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Noboru Sato</font></font></a><br>
                        <span class="nowrap-grn "><a href="/schedule/personal_month?bdate=2020-12-23&amp;uid=6&amp;gid="><img src="/garoon3/grn/image/cybozu/cal_pmon20.gif?20200925.text" border="0" alt=""></a></span>
                     </td>
                     <td class="view_calendar_event_cell">
                        <div class="view_calendar_event_item">
                           <div class="normalEventElement "><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chiến thắng đầu tiên</font></font></div>
                        </div>
                        <a href="/schedule/view?event=1040&amp;bdate=2020-12-23&amp;uid=6&amp;gid=&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="/garoon3/grn/image/cybozu/banner16.gif?20200925.text" border="0" alt="">
                        <span class="event_color5_grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Làm ở nhà</font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Telework      </font></font></a><br>
                     </td>
                     <td colspan="4" class="group_day_calendar_item group_day_calendar_color_offtime"><br>&nbsp;</td>
                     <td colspan="1" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                        <div class="normalEventElement" data-event_id="985" data-event_data="朝礼:Garoon「スケジュール」の使い方" data-event_start_date="2020-12-23 08:40:00" data-event_end_date="2020-12-23 08:50:00" data-event_bdate="2020-12-23" data-event_set_hour="8" data-event_unit="10" data-event_offset_hour="0" data-event_offset_minute="0" data-event_no_endtime=""><a href="/schedule/view?bdate=2020-12-23&amp;uid=6&amp;gid=&amp;col_span=60&amp;disable_link=&amp;referer_key=38259eee4357ab5084cf3459badb1391&amp;facility_id=&amp;timezone=Asia%2FTokyo&amp;quick_add=FALSE&amp;event=985"><span class="event_color7_grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Thăm quan</font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Khách thăm quan</font></font><img src="/garoon3/grn/image/cybozu/repeat16.gif?20200925.text" border="0" align="absmiddle"></a></div>
                     </td>
                     <td colspan="1" class="group_day_calendar_item group_day_calendar_color_offtime"><br>&nbsp;</td>
                     <td colspan="6" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                        <div class="normalEventElement" data-event_id="451" data-event_data="プロジェクタ予約" data-event_start_date="2020-12-23 09:00:00" data-event_end_date="2020-12-23 10:00:00" data-event_bdate="2020-12-23" data-event_set_hour="8" data-event_unit="10" data-event_offset_hour="0" data-event_offset_minute="0" data-event_no_endtime=""><a href="/schedule/view?bdate=2020-12-23&amp;uid=6&amp;gid=&amp;col_span=60&amp;disable_link=&amp;referer_key=38259eee4357ab5084cf3459badb1391&amp;facility_id=&amp;timezone=Asia%2FTokyo&amp;quick_add=FALSE&amp;event=451"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặt trước máy chiếu</font></font><img src="/garoon3/grn/image/cybozu/repeat16.gif?20200925.text" border="0" align="absmiddle"></a></div>
                     </td>
                     <td colspan="18" class="group_day_calendar_item group_day_calendar_color_available">
                        <div class="addEvent">
                           <a href="/schedule/add?uid=6&amp;gid=&amp;bdate=2020-12-23&amp;referer_key=38259eee4357ab5084cf3459badb1391">
                              <div class="iconWrite-grn"></div>
                           </a>
                        </div>
                     </td>
                     <td colspan="6" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                        <div class="normalEventElement" data-event_id="1031" data-event_data="会議:業務ヒアリング" data-event_start_date="2020-12-23 13:00:00" data-event_end_date="2020-12-23 14:00:00" data-event_bdate="2020-12-23" data-event_set_hour="8" data-event_unit="10" data-event_offset_hour="0" data-event_offset_minute="0" data-event_no_endtime=""><a href="/schedule/view?bdate=2020-12-23&amp;uid=6&amp;gid=&amp;col_span=60&amp;disable_link=&amp;referer_key=38259eee4357ab5084cf3459badb1391&amp;facility_id=&amp;timezone=Asia%2FTokyo&amp;quick_add=FALSE&amp;event=1031"><span class="event_color1_grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gặp gỡ</font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Điều trần kinh doanh</font></font><img src="/garoon3/grn/image/cybozu/repeat16.gif?20200925.text" border="0" align="absmiddle"></a></div>
                     </td>
                     <td colspan="6" class="group_day_calendar_item group_day_calendar_color_available">
                        <div class="addEvent">
                           <a href="/schedule/add?uid=6&amp;gid=&amp;bdate=2020-12-23&amp;referer_key=38259eee4357ab5084cf3459badb1391">
                              <div class="iconWrite-grn"></div>
                           </a>
                        </div>
                     </td>
                     <td colspan="6" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                        <div class="normalEventElement" data-event_id="1041" data-event_data="会議:経費申請システム導入について" data-event_start_date="2020-12-23 15:00:00" data-event_end_date="2020-12-23 16:00:00" data-event_bdate="2020-12-23" data-event_set_hour="8" data-event_unit="10" data-event_offset_hour="0" data-event_offset_minute="0" data-event_no_endtime=""><a href="/schedule/view?bdate=2020-12-23&amp;uid=6&amp;gid=&amp;col_span=60&amp;disable_link=&amp;referer_key=38259eee4357ab5084cf3459badb1391&amp;facility_id=&amp;timezone=Asia%2FTokyo&amp;quick_add=FALSE&amp;event=1041"><span class="event_color1_grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gặp gỡ</font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Giới thiệu hệ thống ứng dụng chi phí</font></font><img src="/garoon3/grn/image/cybozu/repeat16.gif?20200925.text" border="0" align="absmiddle"></a></div>
                     </td>
                     <td colspan="12" class="group_day_calendar_item group_day_calendar_color_available">
                        <div class="addEvent">
                           <a href="/schedule/add?uid=6&amp;gid=&amp;bdate=2020-12-23&amp;referer_key=38259eee4357ab5084cf3459badb1391">
                              <div class="iconWrite-grn"></div>
                           </a>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <!--menubar_end-->
         <!--view-->
         <script language="javascript" text="text/javascript" src="/garoon3/grn/html/star.js?20200925.text"></script>
         <script language="javascript" text="text/javascript">
            var options = {url: '/star/ajax_request?',
                           csrf_ticket: '4074830fe66b251b311813bed0f3394d',
                           list_id: 'event_list'               };
            
            var obj_star_list = new GRN_StarList(options);
         </script>
         <div class="detail_title_grn">
            <div id="event_list">
               <span class="star inline_block_grn off" id="grn.schedule:event_985:bdate_2020-12-23" app_path="/garoon3/grn/image/cybozu/" onclick="obj_star_list._onClick(this);"><img src="{!!asset('/img/star_off.png')!!}" border="0" alt=""></span><span class="title_mark_schedule_grn"></span>
               <h2 class="schedule inline_block_grn"><span class="event_color7_grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Buổi sáng lắp ráp</font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cách sử dụng Garoon "Schedule"</font></font></h2>
            </div>
         </div>
         <div class="js_customization_schedule_header_space"></div>
         <table class="viewTable-grn">
            <tbody>
               <tr id="date_and_time_row_grn">
                  <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ngày và giờ</font></font></th>
                  <td><span class="schedule_text_noticeable_grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2020/12/23 (T4) </font></font><span class="mLeft15"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">08:40 ~ 08:50</font></font></span></span></td>
               </tr>
               @if($schedule->pattern == 3)
               <tr>
                  <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">sự lặp lại</font></font></th>
                  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hàng ngày (trừ Thứ Bảy và Chủ Nhật) </font><span class="mLeft15"><font style="vertical-align: inherit;">2020/04/16 (Thứ Năm</font></span><font style="vertical-align: inherit;"> ) </font></font><span class="mLeft15"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-2021/04/15 (Thứ Năm)</font></font></span>
                  </td>
               </tr>
               @endif
               <tr valign="top">
                  <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cơ sở</font></font></th>
                  <td>
                     <!--GTM-1677-->
                     <div class="schedule_member_base_grn">
                     </div>
                     <!--End GTM-1677-->
                  </td>
               </tr>
               <!--GTM-1677-->
               <!--End GTM-1677-->
               <tr valign="top" id="attendees_row_grn">
                  <th nowrap="">
                     <span class="schedule_view_member_list"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Người tham gia (6 người)</font></font></span>
                  </th>
                  <td>
                     <div class="schedule_member_base_grn">
                         
                        <span class="user-grn"><a href="/schedule/user_view?uid=6&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đào Hải</font></font></a></span>
                        @foreach($schedule->member as $key=>$val)                                                                                                                             
                        <span class="user-grn"><a href="/schedule/user_view?uid=3&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="{{asset('/img/user20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$val->full_name}}</font></font></a></span>
                        @endforeach
                     </div>
                  </td>
               </tr>
               <tr valign="top" id="notes_row_grn">
                  <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bản ghi nhớ</font></font></th>
                  <td style="max-width:500px;">
                     
                  </td>
                  <!--hack wrap text in table td-->
               </tr>
            </tbody>
         </table>
         <div class="viewTableSubInfo-grn">
            <span class="nowrap-grn mRight15"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
            Người đăng ký
            </font></font><a href="/schedule/user_view?uid=6&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{!!$schedule->created_by->full_name!!}</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> {!!date('l , F d, Y h:i A',strtotime($schedule->created_at))!!}
            </font></font></span>
            <span class="nowrap-grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
            được cập nhật bởi
            </font></font><a href="/schedule/user_view?uid=6&amp;referer_key=38259eee4357ab5084cf3459badb1391"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">@if(is_null($schedule->update_person)) {!!$schedule->created_by->full_name!!} @else {!!$schedule->updater->full_name!!} @endif</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> &nbsp;{!!date('l , F d, Y h:i A',strtotime($schedule->updated_at))!!}
            </font></font></span>
         </div>
         <div class="viewTableCommentArea-grn comment_std_grn">
            <form id="follow" name="follow" method="post" action="/schedule/command_view?">
               <input type="hidden" name="csrf_ticket" value="4074830fe66b251b311813bed0f3394d">
               <input type="hidden" name="event" value="985">
               <input type="hidden" name="uid" value="6">
               <input type="hidden" name="gid" value="">
               <input type="hidden" name="date" value="2020-12-23">
               <input type="hidden" name="referer_key" value="38259eee4357ab5084cf3459badb1391">
               <input type="hidden" name="mention">
               <input type="hidden" id="initial_textarea_data_editor_id" value="">
               <link href="/garoon3/grn/html/component/editor/ui.css?20200925.text" rel="stylesheet" type="text/css">
               <script src="/garoon3/grn/html/component/editor/editor.js?20200925.text" type="text/javascript"></script>
               <script>
                  jQuery(document).on('grn.textarea:initialized', function() {
                      var ed = grn.component.editor;
                  
                      ed.Editor.locale = "ja";
                      ed.Editor.confirm_message = "テキストに切り替えると、\n編集中の文書の文字以外の情報が削除されます。\nこの操作は取り消しできません。\nよろしいですか？";
                      ed.Editor.image_upload_url = "/schedule/rich_editor/add_image?";
                      ed.Editor.image_download_url = "/schedule/rich_editor/file_download?";
                      ed.Editor.grn_color_palette = "/re/grn_color_palette?";
                      ed.Editor.allowed = "" ? true : false;
                      ed.Editor.setting_type = "0";
                  
                      var editor = new ed.Editor(
                              "data_editor_id",
                              "editor",
                               0);
                  
                      // Initialize Mention component
                                  
                              var mention_settings = {
                                  
                                  name: "data_mention",
                                  element: "#data_mention",
                                  accessPluginEncoded: "YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjQ6e3M6NjoiYXBwX2lkIjtzOjg6InNjaGVkdWxlIjtzOjg6ImV2ZW50X2lkIjtzOjM6Ijk4NSI7czo2OiJhY3Rpb24iO2E6MTp7aTowO3M6NDoicmVhZCI7fXM6NzoiZmVhdHVyZSI7czo3OiJtZW50aW9uIjt9fQ==",
                                  textEditor: editor
                                  
                              };
                          
                          new grn.js.component.common.ui.mention.Mention(mention_settings).render();
                          });
               </script>                
               <div class="clear_both_1px">&nbsp;</div>
            </form>
         </div>
         <!--view_end--->
      </div>
   </div>
</div>
    
</body>
@stop
@section('script')
@parent
<script>
   $('.sidebar-top-off').click(function () {
   if ($(this).hasClass('on')) {
   $('.sidebar-top ul').hide(300);
   $(this).html('<i class="fas fa-chevron-down"></i>')
   $(this).removeClass('on');
   } else {
   $('.sidebar-top ul').show(300);
   $(this).html('<i class="fas fa-chevron-up"></i>');
   $(this).addClass('on');
   }
   })
</script>
@stop
