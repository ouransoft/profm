@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
    <tbody>
        <tr>
            <td width="100%" valign="bottom" nowrap="">
                <div class="global_naviAppTitle-grn">
                    <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="">{{trans('base.Scheduler')}}
                </div>
                <div class="global_navi-viewChange-grn">
                    <ul>
                        <li><a class="global_naviBackTab-viewChange-grn viewChangeLeft-grn" href="{{route('frontend.schedule.group_day')}}">{{trans('base.Group_day')}}</a></li>
                        <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.index')}}">{{trans('base.Group_week')}}</a></li>
                        <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_day')}}">{{trans('base.Day')}}</a></li>
                        <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_week')}}">{{trans('base.Week')}}</a></li>   
                        <li role="heading" aria-level="2" class="global_naviBack-viewChangeSelect-grn"><span>{{trans('base.Month')}}</span></li>
                        <li><a class="global_naviBackTab-viewChange-grn viewChangeRight-grn" href="{{route('frontend.schedule.personal_year')}}">{{trans('base.Year')}}</a></li>
                    </ul>
                </div>
            </td>
            <td align="right" valign="bottom" nowrap="">
            </td>
        </tr>
    </tbody>
</table>
<script src="{{asset('/js/yahoo-dom-event.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/animation-min.js')}}" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
var cy_schedule_um_mon_name = new Array("January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "December");
var cy_schedule_um_dateformat_short = "&&month&& &&year&&";
var cy_schedule_um_dateformat_long = "&&month&& &&day&&, &&year&&";
var cy_schedule_um_dateformat_tilda = " - ";
</script>
<script type="text/javascript" language="javascript">
    var command_show_hide_absent_schedule_url = "/schedule/command_show_hide_absent_schedule?";
</script>
<script language="JavaScript" type="text/javascript">
<!--

// For bookmark
var hash_array = location.hash.split('bdate=');
var href_query_array = location.href.split('?');
if (hash_array.length > 1 && href_query_array.length > 1 && hash_array[1].match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
    var url_params = grn.component.url.parseQueryString(location.href);
    url_params['bdate'] = hash_array[1];

    var hash_param = '';
    if (hash_array[0].length > 1) {
        hash_param = hash_array[0];
    }

    location.href = href_query_array[0] + '?' + grn.component.url.toQueryString(url_params) + hash_param;
}

//-->
</script>
<script src="{{asset('/js/schedule_um.js')}}" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
<!--
    var cy_schedule_um_baseline_id = 5;
    var cy_schedule_um_week_display = 6;
var cy_schedule_um_week_buffer = new Object();
    cy_schedule_um_week_buffer['0'] = '2020-11-22';
    cy_schedule_um_week_buffer['1'] = '2020-11-29';
    cy_schedule_um_week_buffer['2'] = '2020-12-06';
    cy_schedule_um_week_buffer['3'] = '2020-12-13';
    cy_schedule_um_week_buffer['4'] = '2020-12-20';
    cy_schedule_um_week_buffer['5'] = '2020-12-27';
    cy_schedule_um_week_buffer['6'] = '2021-01-03';
    cy_schedule_um_week_buffer['7'] = '2021-01-10';
    cy_schedule_um_week_buffer['8'] = '2021-01-17';
    cy_schedule_um_week_buffer['9'] = '2021-01-24';
    cy_schedule_um_week_buffer['10'] = '2021-01-31';
    cy_schedule_um_week_buffer['11'] = '2021-02-07';
    cy_schedule_um_week_buffer['12'] = '2021-02-14';
    cy_schedule_um_week_buffer['13'] = '2021-02-21';
    cy_schedule_um_week_buffer['14'] = '2021-02-28';
    cy_schedule_um_week_buffer['15'] = '2021-03-07';

var cy_schedule_um_ajax_url = '/schedule/ajax_personal_month?uid=51&gid=&referer_key=0fdf72adf8fd33ef32d8bf0526cedb15';
var cy_schedule_um_startwday = '0';
var um_property = new CyScheduleUmProperty(cy_schedule_um_baseline_id, cy_schedule_um_week_display, cy_schedule_um_week_buffer, cy_schedule_um_ajax_url, '', true, cy_schedule_um_startwday);

var plid_array = new Array();
plid_array[0] = um_property;




function DisplayElement_personal_month(rootElement, valueCheck)
{
    var scheduleWrapper = jQuery(rootElement);
    var absentSchedule = jQuery(scheduleWrapper.find('tr.schedule_absent'));
    var banner = jQuery(scheduleWrapper.find('div.schedule_absent'));
    if(!valueCheck)
    {
        absentSchedule.each(function(index, e){
            jQuery(e).off('mouseover');
            jQuery(e).off('mouseout');
            jQuery(e).show();
            if(jQuery(e).next()){
                jQuery(e).next().remove();
            }
            });

        banner.each(function(index, e){
            jQuery(e).off('mouseover');
            jQuery(e).off('mouseout');
            jQuery(e).children().each(function(index, e1){
                jQuery(e1).show();
            });
            jQuery(e).removeClass().addClass('schedule_absent_hide');
        });
    }
    else
    {
        absentSchedule.each(function(index, e){
            jQuery(e).hide();
            var table = jQuery(e).parent().parent()[0];
            var row = table.insertRow(1);
            var cell = row.insertCell(0);
            cell.colspan = 2;
            cell.innerHTML = '&nbsp;&nbsp;&nbsp;';
            row.className = 'hidden_event';
            jQuery(row).on('mouseover', function(event){
                jQuery(row).hide();
                jQuery(row).prev().show();
            });

            jQuery(row).prev().on('mouseout', function(event){
                jQuery(row).prev().hide();
                jQuery(row).show();
            });
        });

        banner.each(function(index, e){
            jQuery(e).children().each(function(index, e1){
                    e1.hide();
                })
            jQuery(e).removeClass().addClass('schedule_absent_show');
            jQuery(e).on('mouseover', function(event){
                jQuery(e).children().each(function(index, e1){
                    jQuery(e1).show();
                    })
                jQuery(e).removeClass().addClass('schedule_absent_hide');
                });
            jQuery(e).on('mouseout', function(event){
                jQuery(e).children().each(function(e1){
                    jQuery(e1).hide();
                    });
                jQuery(e).removeClass().addClass('schedule_absent_show');
                });
       });
    }
}
function show_hide_absent_schedule_personal_month(id,tag_name,screen,flag)
{
   valueCheck = jQuery('#' + id);
  if(screen == 'personal_month')
  {
      if( valueCheck.prop('checked'))
      {
        DisplayElement_personal_month(tag_name,false);
        status = 1;
      }
      else
      {
        DisplayElement_personal_month(tag_name,true);
        status = 0;
      }

      if(flag == 1)
      {
          new jQuery.ajax({
              url: command_show_hide_absent_schedule_url,
              type: 'POST',
              data: 'status=' + status + '&page=personal_month' +
                  '&csrf_ticket=b71da2c8d458bb4a35117ab4dceae0e9'
          });
       }
  }
}
<!--GRN2-2286-->
//-->
</script>
<div class="mainarea">
   <div class="mainareaSchedule-grn">
      <div id="view">
         <script src="{{asset('/js/schedules.js')}}" type="text/javascript"></script>
         <!--menubar-->
         <div id="menu_part">
             <p>Lịch trình của {{$member->full_name}}</p>
            <div id="smart_rare_menu_part" style="white-space:nowrap;">
               <script language="JavaScript" type="text/javascript">
                  <!--
                  function search_submit()
                  {
                      var f = document.forms["search"];
                      if(!f)
                      {
                          return;
                      }
                      if(arrSearch['schedules'].firstFlag)
                      {
                          var input_search = document.getElementById( 'schedules_search_text' );
                          input_search.value = '';
                      }
                      var referer_bdate_value = __get_referer_bdate();
                      if ( referer_bdate_value.length > 0 )
                      {
                          var referer_bdate = document.createElement( "input" );
                          referer_bdate.type = "hidden";
                          referer_bdate.name = "referer_bdate";
                          referer_bdate.value = referer_bdate_value;
                          f.appendChild(referer_bdate);
                      }
                      f.submit();
                  }
                  -->
               </script>
            </div>
            <div class="clear_both_0px"></div>
         </div>
         <!--menubar_end-->
         <script language="JavaScript" type="text/javascript">
            <!--
            function __get_referer_bdate()
            {
                var referer_bdate = '';
                var param_elements = document.getElementsByName('bdate');
                for( var i = 0; i < param_elements.length; i++ )
                {
                    if (param_elements[i].getAttribute('value'))
                    {
                        referer_bdate = param_elements[i].getAttribute('value');
                        break;
                    }
                }
                return referer_bdate;
            }
            -->
         </script>
         <!--GRN2-2286--><!--<div align="right"><input type="checkbox" name="show_absent_schedule_personal_month" id="show_absent_schedule_personal_month" class="" value="1" onclick="show_hide_absent_schedule_personal_month('show_absent_schedule_personal_month','table.calendar','personal_month',1)" checked><label id="show_absent_schedule_personal_month_label" for="show_absent_schedule_personal_month" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">Show skipped appointments</label>&nbsp;</div>--><!--GRN2-2286-->
         <table width="100%">
            <tr>
               <td nowrap class="v-allign-middle" width="40%">
                    <div id="smart_main_menu_part">
                       <span class="menu_item">
                       <a href="{{route('frontend.schedule.create')}}" onclick="">
                       <img src="{{asset('/img/write20.png')}}" border="0" alt="">{{trans('base.New')}}</a>
                       </span>
                       &nbsp;
                       <span class="menu_item">
                       <span class="nowrap-grn "><a class="schedule_print_personal_month" onclick="CreatePDFfromHTML()" href="javascript:void(0)"><img src="/img/print20.gif" border="0" alt="" class="schedule_print_personal_month">{{trans('base.Print')}} PDF</a></span>
                       </span>
                    </div>
               </td>
               <td align="center" class="v-allign-middle nowrap-grn" width="20%">
                  <span id="schedule_um__date_title" class="displaydate">{{date('F Y',strtotime($date))}}</span>
                  <span class="viewSubCalendar-grn">
                  <span id="showIcon-grn" class="showIconOff-grn" title="Show calendar">
                  <span class="subCalendar-grn"></span>
                  <img src="{{asset('/img/spinner.gif')}}" id="wait_image" name="wait_image" style="display:none">
                  </span>
                  </span>
               </td>
               <td class="v-allign-middle" align="right" nowrap width="40%">
                   <div class="moveButtonBlock-grn"><span class="moveButtonBaseDisable-grn" title="{{trans('base.Previous_month')}}"><a href="{{route('frontend.schedule.personal_month',['date'=>$prev_month])}}" id="onloadAble0" name="onloadAble" class="scheduleMove"  title="{{trans('base.Previous_month')}}"><span class="moveButtonArrowLeftDisable-grn"></span></a></span><span class="moveButtonBaseDisable-grn"><a href="{{route('frontend.schedule.personal_month',['date'=>$prev_month])}}" id="onloadAble2" name="onloadAble" class="scheduleMove"> {{trans('base.This_month')}} </a></span><span class="moveButtonBaseDisable-grn" title="{{trans('base.Next_month')}}"><a href="{{route('frontend.schedule.personal_month',['date'=>$next_month])}}" id="onloadAble4" name="onloadAble" class="scheduleMove" ><span class="moveButtonArrowRightDisable-grn"></span></a></span></div>
                  <style type="text/css">
                     .tableFixed{
                     width:100%;
                     table-layout:fixed;
                     }
                     .normalEvent {}
                     .normalEventElement{}
                     .showEventTitle{
                     position:absolute;
                     max-width: 300px;
                     overflow:hidden;
                     z-index: 20;
                     }
                     .hideEventTitle{}
                     .hideEventTitle .normalEvent {
                     overflow:hidden;
                     vertical-align:top;
                     }
                     .userElement{
                     overflow:hidden;
                     }
                     .nowrap_class{
                     white-space:nowrap;
                     padding:2px;
                     overflow:hidden;
                     }
                  </style>
                  <span id="measure_string" style="visibility:hidden;white-space:nowrap;"></span>
                  <script type="text/javascript" language="javascript">
                     var isScheduleGroupDay = false;
                     var popup_dimensions = {};
                     jQuery(document).on('mousemove', getMousePosition);
                     jQuery(document).on('mouseover', getMousePosition);
                     var mouse_position = null;
                     function getMousePosition(e){
                         mouseX = e.pageX;
                         mouseY = e.pageY;
                         mouse_position = {x:mouseX,y:mouseY};
                         
                         // hide popup with schedule Group Day
                         if( isScheduleGroupDay )
                         {
                             if( mouseX < popup_dimensions.left || mouseX > popup_dimensions.left + popup_dimensions.width ||
                                 mouseY < popup_dimensions.top || mouseY > popup_dimensions.top + popup_dimensions.height )
                             {
                                 var popup_title = document.getElementById('popup_show_title');
                                 if(popup_title){
                                     jQuery(popup_title).remove();
                                 }
                             }
                         }
                         
                         return true;
                     }
                     
                     function isGroupWeekPortlet(){
                         if( typeof gw_table_prefix_items !== 'undefined' ){
                             return true;
                         }
                         return false;
                     }
                     
                     // page_on_load show that page is just on load
                     function showFullShortTitle(check_id,schedule_id,page,page_on_load)
                     {
                         if(check_id == 'show_title_phonemessage')
                         {
                             showTitlePhoneMsg(schedule_id);
                             return;
                         }
                         
                         // check groupday or groupweekschedule
                         if( page == "group_day" || page == "view_group_day" || page == "confirm" )
                             isScheduleGroupDay = true;
                         else
                             isScheduleGroupDay = false;
                     
                         var show_full_title = jQuery('#' + check_id);
                         if( !show_full_title.length )
                             return;
                         var scheduleWrapper = jQuery('#' + schedule_id);
                         var normalEventElements = jQuery(fastGetElementsByClassName(schedule_id,'div','normalEventElement'));
                         var status = 0;
                     
                         // show full title
                         if( show_full_title.prop('checked') )
                         {
                             if(!page_on_load)
                             {
                                 scheduleWrapper.removeClass('tableFixed hideEventTitle');
                                 
                                 if( isGroupWeekPortlet() ){
                                     gw_table_prefix_items['classes'] = gw_table_prefix_items['classes'].filter(function(elm){
                                         return elm != 'tableFixed' && elm != 'hideEventTitle';
                                     });
                                     gw_table_prefix_items['classes'] = gw_table_prefix_items['classes'];
                                 }
                                 
                                 // show or hide shortcut
                                 scheduleWrapper.find('div.shortcut_box_full').each(function(index, elm){
                                     jQuery(elm).show();
                                 });
                                 scheduleWrapper.find('div.shortcut_box_short').each(function(index, elm){
                                     jQuery(elm).hide();
                                 });
                             }
                             
                             // remove nowrap_class of username
                             var userElements = scheduleWrapper.find('td.userBox')
                             userElements.each(function(index, userElement){
                                 jQuery(userElement).removeClass('nowrap_class');
                             });
                     
                             normalEventElements.each(function(index, elm){
                                 jQuery(elm).off('mouseover');
                                 jQuery(elm).off('mouseout');
                             });
                             status = 1;
                         }
                         else // short title, full when over
                         {
                             if(!page_on_load)
                             {
                                 scheduleWrapper.addClass('tableFixed hideEventTitle');
                                 
                                 if( isGroupWeekPortlet() ){
                                     gw_table_prefix_items['classes'].push('tableFixed');
                                     gw_table_prefix_items['classes'].push('hideEventTitle');
                                 }
                                 
                                 // show or hide shortcut
                                 scheduleWrapper.find('div.shortcut_box_full').each(function(index, elm){
                                     jQuery(elm).hide();
                                 });
                                 scheduleWrapper.find('div.shortcut_box_short').each(function(index, elm){
                                     jQuery(elm).show();
                                 });
                             }
                             processDisplayUserName(schedule_id);
                             var body = document.getElementById('body');
                             normalEventElements.each(function(index, e){
                                 // save schedule type
                                 e.isScheduleGroupDay = isScheduleGroupDay;
                                 
                                 jQuery(e).on('mouseover', function(event){
                                     if(typeof disable_tooltip == "undefined" || disable_tooltip == 0)
                                     {
                                         if(isTouchDevice()){return;}
                                         // assign schedule type
                                         isScheduleGroupDay = e.isScheduleGroupDay;
                     
                                         var popup_title = document.getElementById('popup_show_title');
                                         if(popup_title){
                                             jQuery(popup_title).remove();
                                         }
                     
                                         var normalEvent = jQuery(e).parents('td.normalEvent').get(0);
                     
                                         // create popup
                                         var popup_title = document.createElement("div");
                                         popup_title.setAttribute("id", "popup_show_title");
                                         popup_title.setAttribute("class", "showEventTitle");
                                         popup_title.innerHTML = e.innerHTML;
                                         
                                         // disable link
                                         if( !isScheduleGroupDay )
                                         {
                                             var a_tag = jQuery(popup_title).find('a:first-child');
                                             if(a_tag.length){
                                                 a_tag.removeAttr('href');
                                             }
                                         }
                                         else
                                         {
                                             // register star
                                             var popup_star_item = jQuery(popup_title).find('.star:first-child');
                                             var event_star_item = jQuery(e).find('.star:first-child');
                                             if(popup_star_item && event_star_item.star_list){
                                                 popup_star_item.on('click', event_star_item.star_list._onClick.bind(event_star_item.star_list,popup_star_item));
                                             }
                                         }
                                         
                                         // append to body
                                         body.appendChild(popup_title);
                                         
                                         // position
                                         if( isScheduleGroupDay )
                                         {
                                             var pos = findPositionElement(e);
                                             mouse_position.x = pos.left;
                                             mouse_position.y = pos.top;
                                             jQuery(popup_title).css({top: mouse_position.y + 'px', left: mouse_position.x + 'px'});
                                         }
                                         else
                                         {
                                             jQuery(popup_title).css({top: mouse_position.y + 5 + 'px', left: mouse_position.x + 5 + 'px'});
                                         }
                                         
                                         // caculate width for IE
                                         var IE = document.all ? true : false;
                                         if(IE)
                                         {
                                             var tooltip_width = measureStringByPixel(popup_title.innerHTML) + 20;
                                             var body_width = Math.max(body.clientWidth, body.scrollWidth);
                                             if( mouse_position.x + tooltip_width > body_width ){
                                                 tooltip_width = body_width - mouse_position.x;
                                             }
                                             if(tooltip_width > 300){
                                                 tooltip_width = '300';
                                             }
                                             jQuery(popup_title).css({width:tooltip_width + 'px'});
                                         }
                                         
                                         // store popup dimension
                                         popup_dimensions.top = mouse_position.y;
                                         popup_dimensions.left = mouse_position.x;
                                         popup_dimensions.width = popup_title.offsetWidth;
                                         popup_dimensions.height = popup_title.offsetHeight;
                                     }
                                 });
                                 
                                 // remove popup when mouse leave
                                 if( !isScheduleGroupDay )
                                 {
                                     jQuery(e).on('mouseout', function(event){
                                         var popup_title = document.getElementById('popup_show_title');
                                         if(popup_title){
                                             jQuery(popup_title).remove();
                                         }
                                     });
                                 }
                             });
                             status = 0;
                         }
                         // save status
                         if(!page_on_load)
                         {
                             new jQuery.ajax({
                                 url: show_full_title_url,
                                 type: 'POST',
                                 data: 'status='+status + '&page='+ page + '&csrf_ticket=b71da2c8d458bb4a35117ab4dceae0e9'
                             });
                             /* GTM-101 */
                             /* Drag drop schedule */
                             /*
                             if( isScheduleGroupDay )
                             {
                                 if( page == "view_group_day" )
                                 {
                                     dd_remove(schedule_id.substring(10));
                                     dd_init("",schedule_id.substring(10));
                                     dd_handle(schedule_id.substring(10));
                                 }
                                 else if( page == "group_day" )
                                 {
                                     dd_remove();
                                     dd_init();
                                     dd_handle();
                                 }
                             }
                             */
                             /* End GTM-101 */
                         }
                     }
                     
                     // process display user name
                     function processDisplayUserName(schedule_id)
                     {
                         var scheduleWrapper = jQuery('#' + schedule_id);
                         if(!scheduleWrapper.length)
                             return;
                         var userBoxs = scheduleWrapper.find('td.userBox');
                         userBoxs.each(function(index, userBox){
                             jQuery(userBox).addClass('nowrap_class');
                         });
                     }
                     
                     // measure length of string by pixel
                     function measureStringByPixel(data){
                         var measure= document.getElementById('measure_string');
                         measure.innerHTML = data;
                         var width = measure.offsetWidth;
                         measure.innerHTML = '';
                         return width;
                     }
                     
                     // find position on IE
                     function findPositionElement(obj){
                         var left = 0;
                         var top = 0;
                         if(obj.offsetParent)
                         {
                             do {
                                 left += obj.offsetLeft;
                                 top += obj.offsetTop;
                             } while (obj = obj.offsetParent);
                         }
                         return {left:left,top:top};
                     }
                     
                     function showTitlePhoneMsg(schedule_id)
                     {
                         processDisplayUserName(schedule_id);
                         var body = document.getElementById('body');
                         var normalEventElements = jQuery(fastGetElementsByClassName(schedule_id,'div','normalEventElement'));
                         normalEventElements.each(function(index, e){
                             jQuery(e).on('mouseover', function(event){
                                 var popup_title = document.getElementById('popup_show_title');
                                 if(popup_title){
                                     jQuery(popup_title).remove();
                                 }
                                 var normalEvent = jQuery(e).parents('td.normalEvent').get(0);
                     
                                 // create popup
                                 var popup_title = document.createElement('div');
                                 popup_title.setAttribute('id', 'popup_show_title');
                                 popup_title.setAttribute('class', 'showEventTitle');
                                 popup_title.innerHTML = e.innerHTML;
                                 
                                 var a_tag = jQuery(popup_title).find('a:first-child');
                                 if(a_tag.length){
                                     a_tag.removeAttr('href');
                                 }
                                 
                                 // append to body
                                 body.appendChild(popup_title);
                                 
                                 jQuery(popup_title).css({top: mouse_position.y + 5 + 'px', left: mouse_position.x + 5 + 'px'});
                                 
                                 // caculate width for IE
                                 var IE = document.all ? true : false;
                                 if(IE)
                                 {
                                     var tooltip_width = measureStringByPixel(popup_title.innerHTML) + 20;
                                     var body_width = Math.max(body.clientWidth, body.scrollWidth);
                                     if( mouse_position.x + tooltip_width > body_width ){
                                         tooltip_width = body_width - mouse_position.x;
                                     }
                                     if(tooltip_width > 300){
                                         tooltip_width = '300';
                                     }
                                     jQuery(popup_title).css({width:tooltip_width + 'px'});
                                 }
                                 
                                 // store popup dimension
                                 popup_dimensions.top = mouse_position.y;
                                 popup_dimensions.left = mouse_position.x;
                                 popup_dimensions.width = popup_title.offsetWidth;
                                 popup_dimensions.height = popup_title.offsetHeight;
                             });
                             
                             jQuery(e).on('mouseout', function(event){
                                 var popup_title = document.getElementById('popup_show_title');
                                 if(popup_title){
                                     jQuery(popup_title).remove();
                                 }
                             });
                         });
                     }                    
                  </script>
                  <script type="text/javascript" language="javascript">
                     var show_full_title_url = "/api/command_show_full_title?";
                     jQuery(window).on('load', function(event){
                         showFullShortTitle('show_full_titlepersonal_month','personal_month_calendar','personal_month',true);
                         });
                  </script>     
               </td>
            </tr>
         </table>
         <script language="javascript" type="text/javascript">
            <!--
            var grn_schedule_navi_command_on;
            
            var grn_schedule_navi_cal_url = "/api/command_navi_calendar_display?";
            
            var title_show_calendar = "Show calendar";
            var title_hide_calendar = "Hide calendar";
            
            function grn_schedule_navi_cal()
            {
                var icon_tag = window.document.getElementById( 'showIcon-grn' );
                if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOff-grn'))
                {
                    jQuery('#schedule_calendar').show();
            
                    jQuery('#wait_image').show();
                    if(window.document.getElementById( 'subCalendar-grn-image' ))
                    {
                        jQuery('#subCalendar-grn-image').hide();
                    }
                    
                    grn_schedule_navi_command_on = true;
                    grn_schedule_send_req('on');
                    
                    YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOff-grn');
                    YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOn-grn');
                    
                    icon_tag.setAttribute("title", title_hide_calendar);
                }
                else
                {
                    if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOn-grn'))
                    {
                        jQuery('#schedule_calendar').hide();
            
                        grn_schedule_navi_command_on = false;
                        grn_schedule_send_req('off');
                        
                        YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOn-grn');
                        YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOff-grn');
                        
                        icon_tag.setAttribute("title", title_show_calendar);
                    }
                }
            }
            
            function grn_schedule_send_req(navi_cal_display_flag)
            {
                var post_body = jQuery.param({navi_cal_display_flag:navi_cal_display_flag});
                post_body += '&csrf_ticket=b71da2c8d458bb4a35117ab4dceae0e9';
                var oj = new jQuery.ajax({
                        url         : grn_schedule_navi_cal_url,
                        type        : 'POST',
                        data        : post_body,
                        complete    : grn_schedule_onloaded
                    });
            }
            
            function grn_schedule_onloaded(jqXHR)
            {
                var headers = jqXHR.getAllResponseHeaders();
                var regex = /X-Cybozu-Error/i;
                if( headers.match( regex ) )
                {
                    document.body.innerHTML = jqXHR.responseText;
                    return false;
                }
            
                jQuery('#wait_image').hide();
                if(window.document.getElementById( 'subCalendar-grn-image' ))
                {
                    jQuery('#subCalendar-grn-image').show();
                }
            
                if (grn_schedule_navi_command_on)
                {
                    jQuery('#navi_cal_label_on').show();
                    jQuery('#navi_cal_label_off').hide();
                }
                else
                {
                    jQuery('#navi_cal_label_off').show();
                    jQuery('#navi_cal_label_on').hide();
                }
            }
            
            YAHOO.util.Event.addListener( window.document.getElementById( 'showIcon-grn' ), 'click', grn_schedule_navi_cal );
            //-->
            
         </script>    
         <center>
            <img src="{{asset('/img/spinner.gif')}}" id="wait_image" name="wait_image" style="display:none">
            <div id="schedule_calendar" style="display:none">
               {!!$calendar_html!!}
            </div>
         </center>
         <script language="javascript" type="text/javascript">
            <!--
            function doMoveCalednar(move_date)
            {
                var url = "/api/schedule_navi_calendar_month?location=schedule%2Fpersonal_month&uid=51&gid=&vwdate={{$date}}"+'&cndate='+move_date;
                ajax = new jQuery.ajax({
                    url: url,
                    type: 'GET',
                    async: true,
                    success: function (result) {
                        jQuery('#schedule_calendar').html(result);
                    },
                });
            }
            
            //-->
            
        </script>
        <div class="html-content" style="width:100%">
            <div class='header-table' style="display:none">
                <h1>{{date('F Y',strtotime($date))}}</h1>
            </div>
            <table id="personal_month_calendar" class="personal_month_calendar  js_customization_schedule_view_type_MONTH" style="margin-top:0px">
                <tbody>
                   <tr>
                      <td class="personalMonthDayWeek-grn s_date_sunday_even">Sun</td>
                      <td class="personalMonthDayWeek-grn">Mon</td>
                      <td class="personalMonthDayWeek-grn">Tue</td>
                      <td class="personalMonthDayWeek-grn">Wed</td>
                      <td class="personalMonthDayWeek-grn">Thu</td>
                      <td class="personalMonthDayWeek-grn">Fri</td>
                      <td class="personalMonthDayWeek-grn s_date_saturday_even">Sat</td>
                   </tr>
                </tbody>
                <tbody id="um__body" class="js_customization_schedule_user_id_51">
                   {!!$html!!}
                </tbody>
                <tbody>

                </tbody>
             </table>
                <div class='bottom-table' style="display:none">
                    <div class='content-left'>
                    <p>{{$member->full_name}}</p>
                </div>
                <div class='content-right'>
                    <p>{{date('d/m/Y H:i A')}}</p>
                </div>
            </div>
        </div>
         <div class="none">&nbsp;</div>
         
      </div>
      <!--view_end--->
      <div id="loading" class="loading" style="display:none;position:absolute;"><img src="/garoon3/grn/image/cybozu/spinner.gif?20200925.text" border="0" alt="">Loading…</div>
   </div>
   <script src="{{asset('/js/dragdrop-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/selector-min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/dist/schedule.js')}}" type="text/javascript"></script>
   <script text="text/javascript">
      // namespace
      grn.base.namespace("grn.component.error_handler");
      
      (function () {
          var G = grn.component.error_handler;
      
          G.show = function (request, my_callback) {
              var s = request.responseText;
              if (s != undefined) {
                  var title = '';
                  var msg = '';
                  var json = null;
      
                  try {
                      json = JSON.parse(s);
                  } catch(e){}
      
                  if (json) {
                      var show_backtrace = "";
                      title = '';//json.code;
      
                      if (show_backtrace) {
                          msg = msg + "<div style='height:145px; overflow: auto;'><table><tr><td>";
                      }
                      msg = msg + "<div><img border='0' src='/garoon3/grn/image/cybozu/warn100x60.gif?20200925.text'></div>";
                      msg = msg + "<div class='bold'>" + 'Error (' + json.code + ")</div><div>" + json.diagnosis + "</div><br>";
                      msg = msg + "<div class='bold'>" + 'Cause' + "</div><div>" + json.cause + "</div><br>";
                      msg = msg + "<div class='bold'>" + 'Countermeasure' + "</div><div>" + json.counter_measure + "</div>";
      
                      if (show_backtrace) {
                          msg = msg + "<br><hr>";
                          msg = msg + "<div class='bold'>( Beta/Debug only. by common.ini )</div><br>";
                          msg = msg + "<div class='bold'>Developer Info</div><br>";
                          if (json.developer_info) {
                              msg = msg + json.developer_info;
                          }
                          msg = msg + "<div class='bold'>Backtrace</div><br>";
                          msg = msg + "How to read backtraces(to be written) / How to read backtraces (to be written)<br>";
                          msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                          if (json.backtrace) {
                              msg = msg + json.backtrace;
                          }
                          msg = msg + "</pre>";
                          msg = msg + "<br>$G_INPUT<br>";
                          msg = msg + "<pre style='border:1px solid #6666ff; background:#eeeeff; padding:10px;'>";
                          if (json.input) {
                              msg = msg + json.input;
                          }
                          msg = msg + "</pre>";
                          msg = msg + "</td></tr></table></div>";
                      }
                  }
                  else {
                      title = 'Error';
                      msg = grn_split_tags(s, 1000);
                  }
              }
              else {
                  title = "Error";
                  msg = "Connection failed.";
              }
      
              GRN_MsgBox.show(msg, title, GRN_MsgBoxButtons.ok, {
                  ui: [],
                  caption: {
                      ok: 'OK'
                  },
      
                  callback: function (result, form) {
                      GRN_MsgBox._remove();
                      if (typeof my_callback != 'undefined') my_callback();
                  }
              });
          };
          G.getHeader = function (request) {
              if (typeof(request.getAllResponseHeaders) == 'function') {
                  return request.getAllResponseHeaders();
              }
              else {
                  return request.getAllResponseHeaders; // for YAHOO.util.Connect.asyncRequest
              }
          };
          G.hasCybozuError = function (request) {
              var headers = G.getHeader(request);
              return (headers && headers.match(new RegExp(/X-Cybozu-Error/i))) ? true : false;
          };
          G.hasCybozuLogin = function (request) {
              var headers = G.getHeader(request);
              return (headers.match(new RegExp(/X-CybozuLogin/i)) || !headers.match(new RegExp(/X-Cybozu-User/i)));
          };
      })();
      
   </script>
   <script>new grn.js.page.schedule.MonthIndex({"eventId":null})</script>
</div>
@stop
@section('script')
@parent
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script>
function CreatePDFfromHTML() {
    $('.header-table').attr('style','');
    $('.bottom-table').attr('style','');
    var HTML_Width = $(".html-content").width();
    var HTML_Height = $(".html-content").height();
    var top_left_margin = 15;
    var PDF_Width = HTML_Width + (top_left_margin * 2);
    var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;
    var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
    html2canvas($(".html-content")[0]).then(function (canvas) {
        var imgData = canvas.toDataURL("image/jpeg", 1.0);
        var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
        for (var i = 1; i <= totalPDFPages; i++) { 
            pdf.addPage(PDF_Width, PDF_Height);
            pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
        }
        pdf.save("CalendarMonth.pdf");
    });
    $('.header-table').attr('style','display:none');
    $('.bottom-table').attr('style','display:none');
}
</script>
@stop