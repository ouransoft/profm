@extends('frontend.layouts.create_schedule')
@section('content')
<style>
    pre{
        white-space: pre-line;
        font-size:100%;
        font-family: "Roboto";
    }
</style>
<body class="page-body">
    <div class="global_navi" role="heading" aria-level="2" style="border-bottom: none;margin-bottom: 15px;">
        <a href="@if(Session::get('redirect_route')) {{route(Session::get('redirect_route'))}} @endif" style="height: 34px;vertical-align: middle;line-height: 50px;display: inline-block;padding: 0px 20px;font-size: 20px;border-right: 1px solid #333;margin-top: 10px;line-height: 4px;color: #333;">
            <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="" style="margin-bottom: 10px;width: 30px;">
           {{trans('base.Scheduler')}}
        </a>
        <span class="globalNavi-item-last-grn" style="padding: 0 20px;top:10px;">
           {{trans('base.Apointerment_details')}}
        </span>
    </div>
    <div class="mainarea ">  
   <script src="{{asset('/js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/window_simple_dialog.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/view.js')}}" type="text/javascript"></script>
   <script src="{{asset('/js/schedule.js')}}" type="text/javascript"></script>
   <div class="mainareaSchedule-grn">
      <!--menubar-->
      <div class="fullmenu_grn">
         <div id="main_menu_part">
            <div class="mainMenuGroup-grn">
                @if(\Auth::guard('member')->user()->id == $schedule->uid)
                    <span class="menu_item">
                        @if($schedule->pattern == 1)
                        <span class="nowrap-grn "><a href="{{route('frontend.schedule.edit',$schedule->id)}}"><img src="{{asset('/img/modify20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Edit')}}</font></font></a></span>
                        @elseif($schedule->pattern == 2)
                        <span class="nowrap-grn "><a href="{{route('frontend.schedule.edit_all',$schedule->id)}}"><img src="{{asset('/img/modify20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Edit')}}</font></font></a></span>
                        @elseif($schedule->pattern == 3)
                        <span class="nowrap-grn "><a href="{{route('frontend.schedule.edit_repeat',$schedule->id)}}"><img src="{{asset('/img/modify20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Edit')}}</font></font></a></span>
                        @endif
                    </span>
                    <span class="menu_item"><span class="nowrap-grn "><a id="confirm_delete" href="javascript:void(0)"><img src="{{asset('img/delete20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Delete')}}</font></font></a></span></span>
                @endif
                @if($schedule->pattern == 1)
                    <span class="menu_item"><span class="nowrap-grn "><a href="{{route('frontend.schedule.create',['schedule_id'=>$schedule->id])}}"><img src="{{asset('img/reuse20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Reuse')}} </font></font></a></span></span>
                @elseif($schedule->pattern == 2)
                    <span class="menu_item"><span class="nowrap-grn "><a href="{{route('frontend.schedule.create_all_day',['schedule_id'=>$schedule->id])}}"><img src="{{asset('img/reuse20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Reuse')}} </font></font></a></span></span>
                @elseif($schedule->pattern == 3)
                    <span class="menu_item"><span class="nowrap-grn "><a href="{{route('frontend.schedule.create_repeat',['schedule_id'=>$schedule->id])}}"><img src="{{asset('img/reuse20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Reuse')}} </font></font></a></span>
                    </span>
                @endif
               @if(in_array(\Auth::guard('member')->user()->id,$schedule->member()->pluck('id')->toArray()))
                <span class="menu_item"><span class="nowrap-grn "><a href="/schedule/leave/{{$schedule->id}}"><img src="{{asset('/img/out_schedule20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Leave')}} </font></font></a></span></span>
               @else
                <span class="menu_item"><span class="nowrap-grn "><a href="/schedule/participate/{{$schedule->id}}"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt="">{{trans('base.Attend')}}  </a></span></span>
               @endif
                <span class="menu_item">
                <script src="{!!asset('/js/display_options.js')!!}" type="text/javascript"></script>
                <link href="{!!asset('assets/css/display_options.css')!!}" rel="stylesheet" type="text/css">
                </span>
            </div>
            <div class="clear_both_0px"></div>
         </div>
         <div class="cover" style="display: none; width: 1903px; height: 2043px;" id="background"></div>
         <div style="display:none;">
            <font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><input type="submit" value="Vâng "></font></font>
            <font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><input type="button" value="không" onclick="closeReportRelationConfirmWindow();"></font></font>
         </div>
         <script type="text/javascript" src="{{asset('/js/yahoo-min.js')}}"></script>
         <script type="text/javascript" src="{{asset('/js/treeview.js')}}"></script>
         <!--menubar_end-->
         <!--view-->
         <script language="javascript" text="text/javascript" src="{!!asset('/js/star.js')!!}"></script>
         <script language="javascript" text="text/javascript">
            var options = {url: '/star/ajax_request?',
                           csrf_ticket: '4074830fe66b251b311813bed0f3394d',
                           list_id: 'event_list'               };
            
            var obj_star_list = new GRN_StarList(options);
         </script>
         <div class="detail_title_grn">
            <div id="event_list">
               <span class="title_mark_schedule_grn"></span>
               <h2 class="schedule inline_block_grn">
                   @if(!is_null($schedule->menu))
                   <span class="event_color{{explode(';#',$schedule->menu)[1]}}_grn">{{explode(';#',$schedule->menu)[0]}}</span>
                   @endif
                   {!!$schedule->title!!}
               </h2>
            </div>
         </div>
         <div class="js_customization_schedule_header_space"></div>
         <table class="viewTable-grn">
            <tbody>
               <tr id="date_and_time_row_grn">
                  <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Date_and_time')}}</font></font></th>
                  <td>
                      <span class="schedule_text_noticeable_grn">
                          <font style="vertical-align: inherit;">
                          @if($schedule->pattern == 2 || $schedule->pattern == 4)
                          <font style="vertical-align: inherit;">{!!date('D, F d, Y',strtotime($schedule->start_date))!!} -</font>       
                          <font style="vertical-align: inherit;">{!!date('D, F d, Y',strtotime($schedule->end_date))!!}</font>    
                          @elseif($schedule->pattern == 1)
                          <font style="vertical-align: inherit;">{!!date('D, F d, Y h:i A',strtotime($schedule->start_date))!!} -</font>       
                          <font style="vertical-align: inherit;">{!!date('D, F d, Y h:i A',strtotime($schedule->end_date))!!}</font> 
                          @else
                          {!!date('D, F d, Y',strtotime($schedule->start_date))!!}      
                          <span class="mLeft15">{!!date('h:i A',strtotime($schedule->start_date))!!}&nbsp; - &nbsp;{!!date('h:i A',strtotime($schedule->end_date))!!}</span>
                          @endif
                      </span>
                  </td>
               </tr>
               @if($schedule->pattern == 3)
               <tr>
                  <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Repeats')}}</font></font></th>
                  <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Every')}} {!!trans('base.'.$schedule->type_repeat)!!} </font><span class="mLeft15"><font style="vertical-align: inherit;">{!!date('d/m/Y',strtotime($schedule->start_repeat))!!} </font></font><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> - {!!date('d/m/Y',strtotime($schedule->end_repeat))!!}</font></font></span>
                  </td>
               </tr>
               @endif
               @if(!is_null($schedule->company_name))
                <tr valign="top">
                    <th nowrap="">{{trans('base.Company_information')}}</th>
                    <td>
                        <table class="address" cellpadding="3">
                            <colgroup>
                                <col width="5%">
                                <col width="95%">
                            </colgroup>
                            <tbody><tr valign="top">
                                <th nowrap="">{{trans('base.Company_name')}}</th>
                                <td>{{$schedule->company_name}}</td>
                            </tr>
                            <tr valign="top">
                                <th nowrap="">{{trans('base.Address')}}</th>
                                <td>{{$schedule->physical_address}}</td>
                            </tr>
                            <tr valign="top">
                                <th nowrap="">{{trans('base.Company_phone_number')}}</th>
                                <td>{{$schedule->company_telephone_number}}</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
               @endif
               <tr valign="top">
                  <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Facilities')}}</font></font></th>
                  <td>
                      <div class='schedule_member_base_grn'>
                            @foreach($schedule->equipment as $key=>$val)
                            <span class="facility-grn">
                                <a href="javascript:void(0)">
                                    <img src="{!!asset('/img/facility20.gif')!!}" border="0" alt="">{{$val->name}}
                                </a>
                            </span>
                            @endforeach   
                      </div>
                  </td>
               </tr>
               <!--GTM-1677-->
               <!--End GTM-1677-->
               <tr valign="top" id="attendees_row_grn">
                  <th nowrap="">
                     <span class="schedule_view_member_list"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Attendees')}} ({{count($schedule->member)}} {{trans('base.member')}})</font></font></span>
                  </th>
                  <td>
                     <div class="schedule_member_base_grn">
                        @foreach($schedule->member as $key=>$val)   
                        @if($schedule->uid == $val->id)
                        <span class="user-grn"><a href="#"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$val->full_name}}</font></font></a></span>
                        @else
                        <span class="user-grn"><a href="#"><img src="{{asset('/img/user20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$val->full_name}}</font></font></a></span>
                        @endif
                        @endforeach
                     </div>
                  </td>
               </tr>
               <tr valign="top" id="notes_row_grn">
                  <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{trans('base.Notes')}}</font></font></th>
                  <td style="max-width:500px;">
                    <pre>
                        {{$schedule->memo}}
                    </pre>
                  </td>
               </tr>
               @if(count($schedule->files) > 0) 
                    <tr valign="top">
                         <th nowrap="">{{trans('base.Attachments')}}}}</th>
                         <td>
                             @foreach($schedule->files as $key=>$value)
                             <a class="with_lang" target="_blank" href="{{asset($value->link)}}"><img src="{{asset('/img/disk20.gif')}}" border="0" alt="">{{$value->name}}</a>&nbsp;&nbsp;({{$value->size}})<br>
                             @endforeach                  
                         </td>
                     </tr>
               @endif
               @if($schedule->pattern == 4)
               <tr valign="top">
                    <th nowrap="">Percent</th>
                    <td>
                        {{$schedule->percent}} %               
                    </td>
                </tr>
                @endif
            </tbody>
         </table>
         <div class="viewTableSubInfo-grn">
            <span class="nowrap-grn mRight15"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
            {{trans('base.Registrant')}}
            </font></font><a href="#"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{!!$schedule->created_by->full_name!!}</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> {!!date('D , F d, Y h:i A',strtotime($schedule->created_at))!!}
            </font></font></span>
            <span class="nowrap-grn"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
            {{trans('base.Updater')}}
            </font></font><a href="#"><img src="{{asset('/img/loginuser20.gif')}}" border="0" alt=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">@if(is_null($schedule->update_person)) {!!$schedule->created_by->full_name!!} @else {!!$schedule->updater->full_name!!} @endif</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> &nbsp;{!!date('D , F d, Y h:i A',strtotime($schedule->updated_at))!!}
            </font></font></span>
         </div>
      </div>
   </div>
</div>
<div id="modal_confirm_delete" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <form action="{{route('frontend.schedule.destroy',$schedule->id)}}" method="post">
                {!! method_field('delete') !!}
                {!! csrf_field() !!}
                <div class="modal-header flex-column">
                    <div class="icon-boxs">
                            <i class="material-icons">&#xE5CD;</i>
                    </div>						
                    <h4 class="modal-title w-100">{{ trans('base.Are_you_sure?') }}</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>{{trans('base.Do_you_really_want_to_delete _these_records?_This_process_cannot_be_undone')}}.</p>
                    @if($schedule->pattern == 3)
                    <div class="mBottom3"><span class="radiobutton_base_grn"><input type="radio" name="apply" id="3" value="this" checked=""><label for="3" onmouseover="this.style.color='#ff0000'" onmouseout="this.style.color=''" style="color: rgb(255, 0, 0);">{{ trans('base.This_appointment_only') }}({{date('D, F d,Y',strtotime($schedule->start_date))}})</label></span></div>
                    <div class="mBottom3"><span class="radiobutton_base_grn"><input type="radio" name="apply" id="4" value="after"><label for="4" onmouseover="this.style.color='#ff0000'" onmouseout="this.style.color=''" style="color: rgb(255, 0, 0);">{{ trans('base.Appointments_on_and_after') }} {{date('D, F d,Y',strtotime($schedule->start_date))}}</label></span></div>
                    <div class="mBottom3"><span class="radiobutton_base_grn"><input type="radio" name="apply" id="5" value="all"><label class="attentionMessage-grn bold_grn" for="5" onmouseover="this.style.color='#ff0000'" onmouseout="this.style.color=''" style="color: rgb(255, 0, 0);">{{ trans('base.All_appointments') }}</label></span></div>
                    @endif
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('base.Cancel')}}</button>
                    <button type="submit" class="btn btn-danger">{{ trans('base.Delete') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>     
</body>
@stop
@section('script')
@parent
<script>
    $('#confirm_delete').click(function(){
       $('#modal_confirm_delete').modal('show');
    })
</script>
@stop
