<style>
   .sidebar-top li.active {
      box-shadow: rgb(0 0 0 / 40%) 0px 2px 4px, rgb(0 0 0 / 30%) 0px 7px 13px -3px, rgb(0 0 0 / 20%) 0px -3px 0px inset;
      border-radius: 10%;
   }
</style>
<div class="sidebar-top bg-white">
    <ul class="pd-0">
       <li class="@if(Route::currentRouteName() == 'home.view') active @endif">
          <a href="{{route('home.view')}}">
             <div class="">
                <img src="{{asset('/img/Portal.png')}}">
                <h6>Portal</h6>
             </div>
          </a>
       </li>
       <li class="@if(strpos(Route::currentRouteName(), 'schedule')) active @endif">
          <a href="{{route('frontend.schedule.index')}}?">
             <div class="">
                <img src="{{asset('/img/Scheduler.png')}}">
                <h6>Scheduler</h6>
             </div>
          </a>
       </li>
       
       <li class="@if(strpos(Route::currentRouteName(), 'todo')) active @endif">
          <a href="{!!route('frontend.todo.index')!!}">
             <div class="">
                <img src="{{asset('/img/To_do_list.png')}}">
                <h6>To-do List</h6>
             </div>
          </a>
       </li>
      
       <li class="@if(strpos(Route::currentRouteName(), 'project')) active @endif">
            <a href="{{route('frontend.project.index')}}">
               <div class="">
                  <img src="{{asset('/img/kaizen.png')}}">
                  <h6>Kaizen</h6>
               </div>
            </a>
       </li>
       <!--<li>
            <a href="{{route('frontend.project.index')}}">
               <div class="">
                  <img src="{{asset('/img/TPM.png')}}">
                  <h6>TPM</h6>
               </div>
            </a>
       </li>-->
       @if(\Auth::guard('member')->user()->level == \App\Member::LEVEL_ADMIN)
       <li class="@if(strpos(Route::currentRouteName(), 'dashboard.index') || strpos(Route::currentRouteName(), 'member') || strpos(Route::currentRouteName(), 'config') || strpos(Route::currentRouteName(), 'equipment') ||
            strpos(Route::currentRouteName(), 'department') || strpos(Route::currentRouteName(), 'position') || strpos(Route::currentRouteName(), 'level') || strpos(Route::currentRouteName(), 'slide')) active @endif">
            <a href="{!!route('frontend.dashboard.index')!!}">
               <div class="">
                  <img src="{{asset('/assets2/img/gear.png')}}">
                  <h6>System</h6>
               </div>
            </a>
       </li>
       @endif
    </ul>
    <div class="sidebar-bottom">
       <div class="button-off">
          <a class="sidebar-top-off on"><i class="fas fa-chevron-up"></i></a>
       </div>
    </div>
</div>
<script>
   $('.sidebar-top-off').click(function () {
        if ($(this).hasClass('on')) {
            $('.sidebar-top ul').hide(300);
            $(this).html('<i class="fas fa-chevron-down"></i>')
            $(this).removeClass('on');
        } else {
            $('.sidebar-top ul').show(300);
            $(this).html('<i class="fas fa-chevron-up"></i>');
            $(this).addClass('on');
        }
   })
    $(document).ready(function(){
        
        $('body').delegate('.dropdown','click',function(e){
             if(!$(this).parent().find('.dropdownMenu').hasClass('active')){
                $(this).parent().find('.dropdownMenu').addClass('active');
                 e.stopPropagation();
             }else{
                $(this).parent().find('.dropdownMenu').removeClass('active'); 
                 e.stopPropagation();
             }
        })
        
    })
</script>
