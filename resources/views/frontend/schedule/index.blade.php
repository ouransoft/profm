@extends('frontend.layouts.create_schedule')
@section('content')
<table class="global_navi_title" cellpadding="0" cellmargin="0" style="padding:0px;">
   <tbody>
      <tr>
         <td width="100%" valign="bottom" nowrap="">
            <div class="global_naviAppTitle-grn">
               <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="">{{trans('base.Scheduler')}}
            </div>
            <div class="global_navi-viewChange-grn">
               <ul>
                  <li><a class="global_naviBackTab-viewChange-grn viewChangeLeft-grn" href="{{route('frontend.schedule.group_day')}}">{{trans('base.Group_day')}}</a></li>
                  <li role="heading" aria-level="2" class="global_naviBack-viewChangeSelect-grn"><span>{{trans('base.Group_week')}}</span></li>
                  <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_day')}}">{{trans('base.Day')}}</a></li>
                  <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_week')}}">{{trans('base.Week')}}</a></li>
                  <li><a class="global_naviBackTab-viewChange-grn" href="{{route('frontend.schedule.personal_month')}}">{{trans('base.Month')}}</a></li>
                  <li><a class="global_naviBackTab-viewChange-grn viewChangeRight-grn" href="{{route('frontend.schedule.personal_year')}}">{{trans('base.Year')}}</a></li>
               </ul>
            </div>
         </td>
         <td align="right" valign="bottom" nowrap="">
         </td>
      </tr>
   </tbody>
</table>
<div class="mainarea">
   <div class="mainareaSchedule-grn">
      <script src="{{asset('/js/schedule.js')}}" type="text/javascript"></script>
      <!--menubar-->
      <div id="menu_part">
         <div id="smart_main_menu_part">
            <span class="menu_item">
            <a href="/schedule/create?bdate={{date('Y-m-d')}}&amp;uid={{\Auth::guard('member')->user()->id}}">
            <img src="{{asset('/img/write20.png')}}" border="0" alt="">{{trans('base.New')}}</a>
            </span>
            &nbsp;
         </div>
         <div id="smart_rare_menu_part" style="white-space:nowrap;">
            <div class="search_navi">
               <form name="search" action="/schedule/index?">
                  <input id="type_search" type="hidden" name="type_search" value="user">
                  <div class="searchboxChangeTarget-grn">
                     <div class="searchbox-grn">
                        <div id="searchbox-schedule" class="input-text-outer-cybozu">
                           <input class="input-text-cybozu" placeholder="Tìm kiếm theo tên thành viên" type="text" value="{{isset($_GET['search_text']) ? $_GET['search_text'] : ''}}" name="search_text" autocomplete="off" value="" onkeypress="if(event.keyCode == 13) search_submit(document.getElementById('type_search').value); return event.keyCode != 13;">
                           <button class="searchbox-submit-cybozu" type="submit"></button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <div class="clear_both_0px"></div>
      </div>
      <!--menubar_end-->
      <script language="JavaScript" type="text/javascript">
         <!--
         function __get_referer_bdate()
         {
             var referer_bdate = '';
             var param_elements = document.getElementsByName('bdate');
             for( var i = 0; i < param_elements.length; i++ )
             {
                 if (param_elements[i].getAttribute('value'))
                 {
                     referer_bdate = param_elements[i].getAttribute('value');
                     break;
                 }
             }
             return referer_bdate;
         }
         -->
      </script>
      <style type="text/css">
         .tableFixed{
         width:100%;
         table-layout:fixed;
         }
         .normalEvent {}
         .normalEventElement{}
         .showEventTitle{
         position:absolute;
         max-width: 300px;
         overflow:hidden;
         z-index: 20;
         }
         .hideEventTitle{}
         .hideEventTitle .normalEvent {
         overflow:hidden;
         vertical-align:top;
         }
         .userElement{
         overflow:hidden;
         }
         .nowrap_class{
         white-space:nowrap;
         padding:2px;
         overflow:hidden;
         }
      </style>
      <style type="text/css">
         .differ_tz_color{
         background-color:#FFDBDE;
         }
         .hide_event{
         display:none;
         }
      </style>
      <script type="text/javascript" language="javascript">
         var command_show_hide_absent_schedule_url = "{grn_pageurl page='schedule/command_show_hide_absent_schedule'}";
      </script>
      <form name="schedule/index" method="GET" action="/schedule/index?bdate={{$date_now}}&amp;uid=&amp;gid=15">
         <div class="margin_bottom">
            <table width="100%">
               <tbody>
                  <tr>
                     <td class="v-allign-middle" nowrap="nowrap">
                        <table cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr>
                                 <td nowrap="nowrap">
                                    <script language="JavaScript" type="text/javascript">
                                       function setUserGroups( group_obj ) {
                                           @foreach($departments as $key=>$val)
                                           group_obj.appendItem( new GRN_GroupItem( '{{$val->id}}', '{{$val->name}}', 'window.parent.clickUserGroup', false, '' ) );
                                        
                                        @endforeach
                                         
                                       
                                       }
                                       function clickUserGroup( gid, name, extra_param ) {
                                           document.getElementById( 'popup_group_list_iframe' ).style.display= 'none';
                                           updateDropdownButtonTitle( name );
                                           location.href = "/schedule/index?bdate={{$date_now}}&date=" + document.forms["schedule/index"].date.value + '&gid='+gid + '&p=' + extra_param;
                                       }
                                       
                                       function clickFacilityGroupTree( form_name, param ){
                                           document.getElementById( 'popup_facility_group_tree_iframe' ).style.display= 'none';
                                       
                                           if ( param['fagid'] != '0' && param['fagid'] != 'f' ) {
                                               if( param['fagid'] == 'r' || param['extra_param'] != 0 ){
                                                   updateDropdownButtonTitle( param['name'] );
                                               }
                                               else{
                                                   updateDropdownButtonTitle( param['name']);
                                               }
                                               changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                               location.href = "/schedule/index?bdate={{$date_now}}&date=" + document.forms["schedule/index"].date.value + '&gid=f' + param['fagid']+ '&p='+param['extra_param'];
                                           }else {
                                               updateDropdownButtonTitle( param['name'] );
                                               changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                               location.href = "/schedule/index?bdate={{$date_now}}&date=" + document.forms["schedule/index"].date.value + '&gid=f' + '&p='+param['extra_param'];
                                           }
                                       }
                                       
                                       function updateDropdownButtonTitle( newTitle )
                                       {
                                           var node = window.document.getElementById( 'dropdown_current_a' );
                                           node.innerHTML = unescape( newTitle );
                                           changeDropDownWidth( 'wrap_dropdown_facility_current' );
                                       }
                                       
                                    </script>
                                    <link href="{{asset('assets/css/fag_tree.css')}}" rel="stylesheet" type="text/css">
                                    <table id="group-select" border="0" cellspacing="0" cellpadding="0" class="wrap_dropdown_menu" style="width: 332px;">
                                       <tbody>
                                          <tr height="20">
                                             <td id="title" class="dropdown_menu_current" height="20" nowrap="">@if(isset($_GET['gid']) &&  $_GET['gid'] > 0 ) {{\App\Department::find($_GET['gid'])->name}} @elseif(isset($_GET['eid']) && !isset($_GET['gid'])) {{\App\Equipment::find($_GET['eid'])->name}} @else {{trans('base.Choose_by_department')}} @endif</td>
                                             <td id="user-button" class="dropdown_menu_user" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                <img src="{{asset('/img/user-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0">
                                             </td>
                                             <td id="facility-button" class="dropdown_menu_facility" style="margin:0px;paddig:0px;" valign="center" aligh="left" width="37" height="20">
                                                <img src="{{asset('/img/facility-dropdown30x20.gif')}}" style="margin:0px;paddig:0px;" border="0">
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <div id="user-popup" class="wrap_dropdown_option"></div>
                                    <div id="facility-popup" class="wrap_dropdown_option"></div>
                                    <div id="dummy-popup" class="wrap_dropdown_option"></div>
                                    <div></div>
                                    <div id="facility-popup-dummy_root" style="border:1px solid #000000; top:-10000px; left:-10000px; position:absolute; background-color:#FFFFFF; overflow:scroll; width:1px;height:1px;">
                                      <div id="facility-popup-dummy_tree_wrap_tree1" class="wrap_tree1">
                                        <div id="facility-popup-dummy_tree_wrap_tree2" class="wrap_tree2">
                                          <div id="facility-popup-dummy_tree"></div>
                                        </div>
                                      </div>
                                    </div>
                                    <script type="text/javascript">
                                       (function () {
                                       
                                           var group_select_id    = 'group-select';
                                           var title_id           = 'title';
                                           var user_button_id     = 'user-button';
                                           var facility_button_id = 'facility-button';
                                           var user_popup_id      = 'user-popup';
                                           var facility_popup_id  = 'facility-popup';
                                           var is_multi_view      = false;
                                       
                                           var dropdown = new GRN_DropdownMenu(
                                               group_select_id, title_id, user_button_id, facility_button_id,
                                               GRN_DropdownMenu.prototype.PreferOrganization,
                                               user_popup_id, facility_popup_id,
                                               clickOrganizationCallback, clickFacilityGroupCallback,
                                               "" );
                                       
                                           function updateTitle( title ) {
                                               var old_width = jQuery('#' + group_select_id).outerWidth();
                                               jQuery('#' + group_select_id).css( {'width':''} );
                                               jQuery('#' + title_id).html( title );
                                               if( old_width > jQuery('#' + group_select_id).outerWidth() ) {
                                                   jQuery('#' + group_select_id).css( { 'width': old_width + 'px'} );
                                               }
                                           }
                                       
                                           function clickOrganizationCallback( group_item ) {
                                               return function(){
                                                   updateTitle( group_item.name )
                                                   dropdown.organization.hide();
                                                   
             
                                                   location.href = "/schedule/index?"+ '&bdate=' + document.forms["schedule/index"].bdate.value + '&gid='+group_item.gid;
                                               }
                                           }
                                       
                                           function clickFacilityGroupCallback( node ) {
                                               if( node.extra_param ) { 
                                                   updateTitle( node.label );
                                               }
                                               else {
                                                   if( node.oid == 'f' ) {
                                                       updateTitle( '(All facilities)' );
                                                   }else{
                                                       updateTitle(  node.label );
                                                   }
                                               }
                                               dropdown.facility.hide();
                                       
                                               var oid = node.oid;
                                               window.location.href = "/schedule/index?"+ '&bdate=' + document.forms["schedule/index"].bdate.value + '&eid='+oid;
                                             }
                                           dropdown.initializeOrganization(
                                               new Array(
                                                   {!!$department!!} ) );
                                       
                                           var group_select_width = dropdown.organization.getWidth( jQuery('#' + title_id).outerWidth() );
                                           jQuery('#' + group_select_id).css( {'width': group_select_width +"px"} );
                                       
                                           dropdown.updateTitle = updateTitle;
                                       
                                           dropdown.initializeFacilityGroup( { 'page_name': "schedule/index",
                                                                               'ajax_path':'/schedule/json/accessible_facility_tree?',
                                                                               'csrf_ticket':'cd9933a8846474545be419350224e341',
                                                                               'callback':clickFacilityGroupCallback,
                                                                               'selectedOID':"",
                                                                               'title_width': jQuery('#' + title_id).outerWidth(),
                                                                               'node_info':
                                                                               [{!!$equipment!!}]
                                                                             });
                                       }());
                                       
                                    </script>
                                 </td>
                                 <td nowrap="nowrap"><a class="selectPulldownSub-grn"  id='search_some_member' href="javascript:void(0);"><img src="{{asset('/img/blankB16.png')}}" border="0" alt=""></a></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                     <td class="v-allign-middle" align="center">
                        <span class="displaydate">{{date('D, F d, Y',strtotime($date_now))}}</span>
                        <span class="viewSubCalendar-grn">
                        <span id="showIcon-grn" class="showIconOff-grn" title="Show calendar">
                        <span class="subCalendar-grn"></span>
                        </span>
                        </span>
                     </td>
                     <td class="v-allign-middle" nowrap="nowrap" align="right">
                        <div class="moveButtonBlock-grn">
                            <span class="moveButtonBase-grn"><a href="{{route('frontend.schedule.index',['bdate'=>$prev_week,'uid'=>isset($_GET['uid'])? $_GET['uid'] :'','gid'=>isset($_GET['gid'])? $_GET['gid'] :'','eid'=>isset($_GET['eid'])? $_GET['eid']:''])}}" title="{{trans('base.Previous_week')}}"><span class="moveButtonArrowLeftTwo-grn"></span></a></span><span class="moveButtonBase-grn"><a href="{{route('frontend.schedule.index',['bdate'=>date('Y-m-d',strtotime('-1 day',strtotime($date_now))),'gid'=>isset($_GET['gid'])? $_GET['gid']:'','uid'=>isset($_GET['uid'])? $_GET['uid'] :'','eid'=>isset($_GET['eid'])? $_GET['eid'] :''])}}" title="{{trans('base.Previous_day')}}"><span class="moveButtonArrowLeft-grn"></span></a></span><span class="moveButtonBase-grn" title=""><a href="{{route('frontend.schedule.index',['bdate'=>date('Y-m-d'),'gid'=>isset($_GET['gid'])? $_GET['gid'] : '','uid'=>isset($_GET['uid'])? $_GET['uid'] :'','eid'=>isset($_GET['eid'])? $_GET['eid'] :''])}}">{{trans('base.Today')}}</a></span><span class="moveButtonBase-grn"><a href="{{route('frontend.schedule.index',['bdate'=>date('Y-m-d',strtotime('+1 day',strtotime($date_now))),'gid'=>isset($_GET['gid'])? $_GET['gid'] :'','uid'=>isset($_GET['uid'])? $_GET['uid'] :'','eid'=>isset($_GET['eid'])? $_GET['eid']:''])}}" title="{{trans('base.Next_day')}}"><span class="moveButtonArrowRight-grn"></span></a></span><span class="moveButtonBase-grn"><a href="{{route('frontend.schedule.index',['bdate'=>$next_week,'gid'=>isset($_GET['gid'])? $_GET['gid']:'','uid'=>isset($_GET['uid'])? $_GET['uid'] :'','eid'=>isset($_GET['eid'])? $_GET['eid']:''])}}" title="{{trans('base.Next_week')}}"><span class="moveButtonArrowRightTwo-grn"></span></a></span>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <script language="javascript" type="text/javascript">
            <!--
            var grn_schedule_navi_command_on;
            
            var grn_schedule_navi_cal_url = "/api/command_navi_calendar_display?";
            
            var title_show_calendar = "Show calendar";
            var title_hide_calendar = "Hide calendar";
            
            function grn_schedule_navi_cal()
            {
                var icon_tag = window.document.getElementById( 'showIcon-grn' );
                if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOff-grn'))
                {
                    jQuery('#schedule_calendar').show();
            
                    jQuery('#wait_image').show();
                    if(window.document.getElementById( 'subCalendar-grn-image' ))
                    {
                        jQuery('#subCalendar-grn-image').hide();
                    }
                    
                    grn_schedule_navi_command_on = true;
                    grn_schedule_send_req('on');
                    
                    YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOff-grn');
                    YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOn-grn');
                    
                    icon_tag.setAttribute("title", title_hide_calendar);
                }
                else
                {
                    if(YAHOO.util.Dom.hasClass('showIcon-grn', 'showIconOn-grn'))
                    {
                        jQuery('#schedule_calendar').hide();
            
                        grn_schedule_navi_command_on = false;
                        grn_schedule_send_req('off');
                        
                        YAHOO.util.Dom.removeClass('showIcon-grn', 'showIconOn-grn');
                        YAHOO.util.Dom.addClass('showIcon-grn', 'showIconOff-grn');
                        
                        icon_tag.setAttribute("title", title_show_calendar);
                    }
                }
            }
            
            function grn_schedule_send_req(navi_cal_display_flag)
            {
                var post_body = jQuery.param({navi_cal_display_flag:navi_cal_display_flag});
                post_body += '&csrf_ticket=cd9933a8846474545be419350224e341';
                var oj = new jQuery.ajax({
                        url         : grn_schedule_navi_cal_url,
                        type        : 'POST',
                        data        : post_body,
                        complete    : grn_schedule_onloaded
                    });
            }
            
            function grn_schedule_onloaded(jqXHR)
            {
                var headers = jqXHR.getAllResponseHeaders();
                var regex = /X-Cybozu-Error/i;
                if( headers.match( regex ) )
                {
                    document.body.innerHTML = jqXHR.responseText;
                    return false;
                }
            
                jQuery('#wait_image').hide();
                if(window.document.getElementById( 'subCalendar-grn-image' ))
                {
                    jQuery('#subCalendar-grn-image').show();
                }
            
                if (grn_schedule_navi_command_on)
                {
                    jQuery('#navi_cal_label_on').show();
                    jQuery('#navi_cal_label_off').hide();
                }
                else
                {
                    jQuery('#navi_cal_label_off').show();
                    jQuery('#navi_cal_label_on').hide();
                }
            }
            
            YAHOO.util.Event.addListener( window.document.getElementById( 'showIcon-grn' ), 'click', grn_schedule_navi_cal);
            //-->
            
         </script>
         <center>
            <img src="{{asset('/img/spinner.gif')}}" id="wait_image" name="wait_image" style="display:none">
            <div id="schedule_calendar" style="display:none">
               {!!$calendar_html!!}
            </div>
         </center>
         <script language="javascript" type="text/javascript">
            <!--
            function doMoveCalednar( move_date, onComplete )
            {
                var url = "/api/schedule_navi_calendar?location=schedule%2Findex&p=&search_text=&uid=&gid=&event=&event_date=&vwdate={{$date_now}}"+'&cndate='+move_date;
                ajax = new jQuery.ajax({
                    url:                url,
                    type:               'GET',
                    async:               true,
                    success: function(result) {
                        jQuery('#schedule_calendar').html(result);
                    },
                    complete:           onComplete
                });
            }
            
            //-->
            
         </script>
         <div class="clear_both_1px">&nbsp;</div>
         <table id="schedule_groupweek" class="calendar scheduleWrapper scheduleWrapperGroupWeek group_week  group_week_calendar js_customization_schedule_view_type_GROUP_WEEK">
            <colgroup>
               <col class="group_week_calendar_column_first">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
               <col class="group_week_calendar_column_common">
            </colgroup>
            <tbody id="event_list">
               {!!$html!!}                               
            </tbody>
         </table>
         @if(!is_null($members))
         <div class="paginate-schedule d-inline-block w-100 mt-2">
            {{$members->appends($_GET)->links()}}
         </div>
         @endif
         <input type="hidden" name="bdate" value="{{$date_now}}">
      </form>
   </div>
   <script src="{{asset('/js/selector-min.js')}}" type="text/javascript"></script>
   <div class="modal fade modalbox" id="modal_search_member" data-backdrop="static" aria-modal="true" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding-bottom:0px;">
                    <h5 class="modal-title">Tìm kiếm thành viên</h5>
                    <a href="javascript:void(0);" data-dismiss="modal" class="close-attendees"><i class='icon-cross' style="font-size:20px;color:#333;"></i></a>
                </div>
                <div class="modal-body">
                    <div class="input-group rounded" style="border: 1px solid #aaaaaa!important;">
                        <input type="search" id="input_search_member" class="form-control rounded" placeholder="Tìm nhân viên" aria-label="Search" aria-describedby="search-addon">
                        <button type="button" class="search-member search-position input-group-text border-0" id="search-addon" style="background:#fd8113;border-radius: 0px;">
                            <i class='icon-search4 text-white'></i>
                        </button>
                    </div>
                    <div class="form-group my-2">
                        <select id="select_attendees" class="select2" data-placeholder='Tất cả'>
                            <option></option>
                            {!!$department_html!!}
                        </select>
                    </div>
                    <div class='row'>
                        <div class="card col-md-6">
                            <h6 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">Danh sách thành viên</h6>
                            <div class="border user-list">
                                <ul class="listview member-list pd0">
                                    @foreach($list_member as $key=>$value)
                                    <li class="d-flex align-items-center px-1 li-add">
                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                            <i class='icon-user'></i>
                                        </span>
                                        <div class="d-flex align-items-end justify-content-between w-100">
                                            <span class="attendees-name">{{$value->full_name}} ({{$value->login_id}})</span>
                                            <span class="text-warning add-member-btn" data-id='{{$value->id}}' data-name='{{$value->full_name}} ({{$value->login_id}})' data-department_id="{{$value->department_id}}">
                                                <i class='icon-plus3'></i>
                                            </span>
                                        </div>
                                    </li> 
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="card col-md-6">
                            <h6 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">Danh sách đã chọn</h6>
                            <div class="border user-list">
                                <ul class="listview member-choose pd0">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-1">
                        <button type="button" class="submit-member btn btn-primary mr-1 mb-1" data-dismiss="modal">Tìm kiếm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
   $('body').delegate('.button-next','click',function(){
       var date = $(this).data('next');
       var member_id = $('#select_member').val();
       $.ajax({
              type: "POST",
              url: '/api/next-schedule',
              data: {date:date,member_id:member_id},
              success: function(response){
                   $('.table-schedule').html(response.html);
                   $('#pagination').html(`
                   <a href="#" class="button-prev" data-prev='`+response.start+`'><i class="fas fa-caret-left"></i></a>
                   <a href="#" class="button-next" data-next='`+response.end+`'><i class="fas fa-caret-right"></i></a>
                   `);
               }
       });   
   });
   $('body').delegate('.button-prev','click',function(){
       var date = $(this).data('prev');
       var member_id = $('#select_member').val();
       $.ajax({
              type: "POST",
              url: '/api/prev-schedule',
              data: {date:date,member_id:member_id},
              success: function(response){
                   $('.table-schedule').html(response.html);
                   $('#pagination').html(`
                   <a href="#" class="button-prev" data-prev='`+response.start+`'><i class="fas fa-caret-left"></i></a>
                   <a href="#" class="button-next" data-next='`+response.end+`'><i class="fas fa-caret-right"></i></a>
                   `);
               }
       });   
   });
   $('#select_member').change(function(){
       var date = $('.button-prev').data('prev');
       var member_id = $(this).val();
       $.ajax({
              type: "POST",
              url: '/api/change-schedule',
              data: {date:date,member_id:member_id},
              success: function(response){
                   $('.table-schedule').html(response.html);
                   $('#list_todolist').html(response.list_html);
               }
       });
   })
   $('.add-todolist').click(function(){
       $('#modal_create_todolist').modal('show');
   })
   $('#create_todolist').submit(function(e){
       e.preventDefault();
       $.ajax({
               type: "POST",
               url: '/api/create-todolist',
               data: new FormData(this),
               contentType: false,
               processData: false,
               dataType: 'json',
               success: function(response){
                   var notifier = new Notifier();
                   var notification = notifier.notify("success", "Thêm thành công");
                   notification.push();
                   $('#list_todolist').prepend(response.html);
               }
           });   
   })
   $('body').delegate('.check-todolist','click',function(){
       if ($('input[name="todolist_id"]:checked').val() === undefined) {
           var notifier = new Notifier();
           var notification = notifier.notify("info", "Cần chọn công việc trước khi thao tác");
           notification.push();
       }else{
           var todolist_id = [];
           $('.check').each(function () {
               if ($(this).is(':checked')) {
                   todolist_id.push($(this).val());
               }
           })   
           $.ajax({
               url: '/api/change-status-todolist',
               method: 'POST',
               data: {todolist_id: todolist_id},
               success: function (response) {
                   var notifier = new Notifier();
                   var notification = notifier.notify("success", "Cập nhật thành công");
                   notification.push();
                  $('#list_todolist').html(response.html);
               }
           });
       }
   });
   $('#search_some_member').click(function(){
       $('#modal_search_member').modal('show');
   })
   $('body').delegate('.add-member-btn','click',function(){
        $('.member-choose').append(`<li class="d-flex align-items-center px-1">
                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                            <i class="icon-user"></i>
                                        </span>
                                        <div class="d-flex align-items-end justify-content-between w-100">
                                            <span class="attendees-name">`+$(this).data('name')+`</span>
                                            <span class="text-warning remove-member-btn" data-id='`+$(this).data('id')+`' data-name='`+$(this).data('name')+`'>
                                                <i class="icon-minus3"></i>
                                            </span>
                                        </div>
                                    </li>`);
        $(this).parent().parent().remove();
   });
   $('body').delegate('.remove-member-btn','click',function(){
        $('.member-list').append(`<li class="d-flex align-items-center px-1">
                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                            <i class="icon-user"></i>
                                        </span>
                                        <div class="d-flex align-items-end justify-content-between w-100">
                                            <span class="attendees-name">`+$(this).data('name')+`</span>
                                            <span class="text-warning add-member-btn" data-id='`+$(this).data('id')+`' data-name='`+$(this).data('name')+`'>
                                                <i class="icon-plus3"></i>
                                            </span>
                                        </div>
                                    </li>`);
        $(this).parent().parent().remove();
   });
   function convertViToEn(str, toUpperCase = false) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng
        str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư

        return toUpperCase ? str.toUpperCase() : str;
    }
   $('body').delegate('#input_search_member','keyup',function(){
        var keyword = $(this).val();
        if($('#select_attendees').val() == ''){
            var department_id = [];
        }else{
            var department_id = $('#select_attendees').val().split(",");
        }
        var convert_keyword = convertViToEn($(this).val());
        if(keyword != '' || department_id.length > 0){
            $('.add-member-btn').each(function(){
                if((($(this).data('name').indexOf(keyword) != -1 || convertViToEn($(this).data('name')).indexOf(convert_keyword) != -1)) &&
                        (department_id.includes(String($(this).data('department_id'))) || department_id.length == 0)){
                    $(this).parent().parent().removeClass('d-none');
                }else{
                    $(this).parent().parent().addClass('d-none');
                }
            });
        }else{
            $('.li-add').removeClass('d-none');
        }
   })
   $('body').delegate('#select_attendees','change',function(){
        if($(this).val() == ''){
            var department_id = [];
        }else{
            var department_id = $(this).val().split(",");
        }
        var keyword = $('#input_search_member').val();
        var convert_keyword = convertViToEn($(this).val());
        if(keyword != '' || department_id.length > 0){
            $('.add-member-btn').each(function(){
                if((($(this).data('name').indexOf(keyword) != -1 || convertViToEn($(this).data('name')).indexOf(convert_keyword) != -1) || keyword == '') &&
                    department_id.includes(String($(this).data('department_id')))){
                    $(this).parent().parent().removeClass('d-none');
                }else{
                    $(this).parent().parent().addClass('d-none');
                }
            });
        }else{
            $('.li-add').removeClass('d-none');
        }
   })
   $('body').delegate('.submit-member','click',function(){
         if($('.remove-member-btn').length){
            var uids = [];
            $('.remove-member-btn').each(function(){
                uids.push(parseInt($(this).data('id')));
            });
            var url = window.location.href.split('?')[0]+'?uid='+uids.join(',');
            window.location = url;
         }
   })
   function formatResult(node) {
        var level = 0;
        if(node.element !== undefined){
          level = (node.element.className);
          if(level.trim() !== ''){
            level = (parseInt(level.match(/\d+/)[0]));
          }
        }
        var $result = $('<span style="padding-left:' + (20 * level) + 'px;">' + node.text + '</span>');
        return $result;
     };
     $(".select2").select2({
        allowClear: true,
        templateResult: formatResult,
      });
</script>
@stop