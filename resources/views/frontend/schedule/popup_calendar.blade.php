<!DOCTYPE html>
  <html lang="en">
<head>
<meta name="robots" content="noindex, nofollow, noarchive">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="Pragma" content="no-cache">
<title></title>
<link href="{!!asset('/assets/css/std.css')!!}" rel="stylesheet" type="text/css">
<link href="{!!asset('/assets/css/font-en.css')!!}" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
body {
  background: url("");
  min-width: 16.5em;
  _height: 15.5em;
  margin: 0;
  padding: 0;
}
table {
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}
table.cal a {
  display: block;
  width: 100%;
  height: 100%;
}
* html table.cal th,
* html table.cal td {
  line-height: 1.2;
}
table.cal a:hover {
  background-color: #c6cbeb;
}
-->
</style>
<script language="JavaScript" type="text/javascript">
<!--
function Close()
{
    window.parent.cb_ui_select_date_display_calendar("{!!$close!!}");
}
function SetDate(nYear, nMonth, nDay, prefix)
{

    window.parent.cb_ui_select_date_init_date_select( "{{$form_name}}", nYear, nMonth, nDay, prefix);
    var select_form_name = window.parent.document.forms["schedule\/{{$form}}"]; // GTM-1605
 
 // GTM-1605
 if(select_form_name !== "undefined")
    window.parent.cb_ui_select_date_reset_year_range( select_form_name, prefix);
 // end GTM-1605

     window.parent.change_enddate();;
 
    Close();
}
//-->
</script>

</head>
<body>
<div class="popup_calendar">

<form name="{{$form_name}}" method="GET" action="/schedule/popup_calendar?script_name=cb_ui_select_date_init_date_select&amp;script_name2=cb_ui_select_date_display_calendar&amp;on_change=change_enddate%28%29%3B">

<table style="width:300px">
 <tr>
  <td colspan="3" align="center" valign="top" nowrap><span class="nowrap-grn "><a  href="javascript:Close();"><img src="{{asset('/img/close20.gif')}}" border="0" alt="">Close</a></span></td>
 </tr>
 <tr>
  <td nowrap align="left" style="width:5%">
       <input type="button" value=" &lt;&lt; " onClick='location.href="/schedule/popup_calendar?date={{$prev_month}}&amp;prefix={{$pre}}&amp;form_name={{$form_name}}"'>
     </td>
  <td nowrap align="center" style="width:90%">
   <script lang="javascript" type="text/javascript">
<!--
var wday_name = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
var openIds = new Array(0);
//-->
</script>
<script lang="javascript" type="text/javascript" src="{!!asset('/js/select_date.js')!!}"></script>
<script language="javascript" type="text/javascript"><!--
var g_arrayHolidays = new Array();
g_arrayHolidays[0] = "2011-01-01";
g_arrayHolidays[1] = "2011-01-10";
g_arrayHolidays[2] = "2011-02-11";
g_arrayHolidays[3] = "2011-03-21";
g_arrayHolidays[4] = "2011-04-29";
g_arrayHolidays[5] = "2011-05-03";
g_arrayHolidays[6] = "2011-05-04";
g_arrayHolidays[7] = "2011-05-05";
g_arrayHolidays[8] = "2011-07-18";
g_arrayHolidays[9] = "2011-09-19";
g_arrayHolidays[10] = "2011-09-23";
g_arrayHolidays[11] = "2011-10-10";
g_arrayHolidays[12] = "2011-11-03";
g_arrayHolidays[13] = "2011-11-23";
g_arrayHolidays[14] = "2011-12-23";
g_arrayHolidays[15] = "2012-01-01";
g_arrayHolidays[16] = "2012-01-02";
g_arrayHolidays[17] = "2012-01-09";
g_arrayHolidays[18] = "2012-02-11";
g_arrayHolidays[19] = "2012-03-20";
g_arrayHolidays[20] = "2012-04-29";
g_arrayHolidays[21] = "2012-04-30";
g_arrayHolidays[22] = "2012-05-03";
g_arrayHolidays[23] = "2012-05-04";
g_arrayHolidays[24] = "2012-05-05";
g_arrayHolidays[25] = "2012-07-16";
g_arrayHolidays[26] = "2012-09-17";
g_arrayHolidays[27] = "2012-09-22";
g_arrayHolidays[28] = "2012-10-08";
g_arrayHolidays[29] = "2012-11-03";
g_arrayHolidays[30] = "2012-11-23";
g_arrayHolidays[31] = "2012-12-23";
g_arrayHolidays[32] = "2012-12-24";
g_arrayHolidays[33] = "2013-01-01";
g_arrayHolidays[34] = "2013-01-14";
g_arrayHolidays[35] = "2013-02-11";
g_arrayHolidays[36] = "2013-03-20";
g_arrayHolidays[37] = "2013-04-29";
g_arrayHolidays[38] = "2013-05-03";
g_arrayHolidays[39] = "2013-05-04";
g_arrayHolidays[40] = "2013-05-05";
g_arrayHolidays[41] = "2013-05-06";
g_arrayHolidays[42] = "2013-07-15";
g_arrayHolidays[43] = "2013-09-16";
g_arrayHolidays[44] = "2013-09-23";
g_arrayHolidays[45] = "2013-10-14";
g_arrayHolidays[46] = "2013-11-03";
g_arrayHolidays[47] = "2013-11-04";
g_arrayHolidays[48] = "2013-11-23";
g_arrayHolidays[49] = "2013-12-23";
g_arrayHolidays[50] = "2014-01-01";
g_arrayHolidays[51] = "2014-01-13";
g_arrayHolidays[52] = "2014-02-11";
g_arrayHolidays[53] = "2014-03-21";
g_arrayHolidays[54] = "2014-04-29";
g_arrayHolidays[55] = "2014-05-03";
g_arrayHolidays[56] = "2014-05-04";
g_arrayHolidays[57] = "2014-05-05";
g_arrayHolidays[58] = "2014-05-06";
g_arrayHolidays[59] = "2014-07-21";
g_arrayHolidays[60] = "2014-09-15";
g_arrayHolidays[61] = "2014-09-23";
g_arrayHolidays[62] = "2014-10-13";
g_arrayHolidays[63] = "2014-11-03";
g_arrayHolidays[64] = "2014-11-23";
g_arrayHolidays[65] = "2014-11-24";
g_arrayHolidays[66] = "2014-12-23";
g_arrayHolidays[67] = "2015-01-01";
g_arrayHolidays[68] = "2015-01-12";
g_arrayHolidays[69] = "2015-02-11";
g_arrayHolidays[70] = "2015-03-21";
g_arrayHolidays[71] = "2015-04-29";
g_arrayHolidays[72] = "2015-05-03";
g_arrayHolidays[73] = "2015-05-04";
g_arrayHolidays[74] = "2015-05-05";
g_arrayHolidays[75] = "2015-05-06";
g_arrayHolidays[76] = "2015-07-20";
g_arrayHolidays[77] = "2015-09-21";
g_arrayHolidays[78] = "2015-09-22";
g_arrayHolidays[79] = "2015-09-23";
g_arrayHolidays[80] = "2015-10-12";
g_arrayHolidays[81] = "2015-11-03";
g_arrayHolidays[82] = "2015-11-23";
g_arrayHolidays[83] = "2015-12-23";

var g_arrayWorkdays = new Array();

function cb_ui_select_date_init_date_select( form_name, year, month, day, prefix)
{
    var year_select = document.forms[form_name].elements[prefix+"year"];
    if(year_select == undefined) return false;
    var month_select = document.forms[form_name].elements[prefix+"month"];
    if(month_select == undefined) return false;
    var day_select = document.forms[form_name].elements[prefix+"day"];
    if(day_select == undefined) return false;

    var year_options = year_select.options;
    var month_options = month_select.options;
    var day_options = day_select.options;
    var select_year = 0;

    for(var i = 0 ; i <= year_options.length - 1 ; i++)
    {
        if(year_options[i].value == year)
        {
            year_options[i].selected = true;
            select_year = 1;
        }
    }

    if( ! select_year)
    {
        if(year_options[0].value > year)
        {
            var o = document.createElement( "OPTION" );
            year_options.add(o);
            for(var i = year_options.length - 1 ; i > 0 ; i --)
            {
                var dst = year_options[i];
                var src = year_options[i - 1];
                dst.value = src.value;
                dst.text = src.text;
                dst.selected = false;
            }
            year_options[0].value = year;
            year_options[0].text = year + ' year';
            year_options[0].selected = true;
        }
        else
        {
            var o = document.createElement( "OPTION" );
            o.value = year;
            o.text = year + ' year';
            o.selected = true;
            year_options.add( o );
        }
    }

    for(var i = 0 ; i <= month_options.length - 1 ; i++)
    {
        if(month_options[i].value == month)
        {
            month_options[i].selected = true;
        }
    }

    var start_date = new Date(year, month - 1, 1);
    var wday = start_date.getDay();
    var last_day = cb_ui_select_date_get_last_day(year, month);
    var offset = 0;
    if( day_options.length > 0 && day_options[0].value < 1 )
    {
        offset = 1;
        day_options[0].className = "";
        day_options[0].text = "--";
    }

    for(i = 0 ; i < last_day ; i ++)
    {
        day_option = i + 1;
        opt_idx = i + offset;

        if(opt_idx >= day_options.length)
        {
            cb_ui_select_date_add_option(day_options, day_option);
        }
        day_options[opt_idx].value = day_option;

        if(wday == 0)
        {
            day_options[opt_idx].className = "s_date_sunday";
        }
        else if(wday == 6)
        {
            day_options[opt_idx].className = "s_date_saturday";
        }
        else
        {
            day_options[opt_idx].className = "";
        }

        if(g_arrayHolidays != null && g_arrayHolidays.length > 0)
        {
            if(cb_ui_select_date_check_holiday(g_arrayHolidays, year, month, day_option))
            {
                day_options[opt_idx].className = "s_date_holiday";
            }
        }

        day_options[opt_idx].text = day_option + "(" + wday_name[wday] + ")";
        if(day_options[opt_idx].value == day)
        {
            day_options[opt_idx].selected = true;
        }

        wday ++;
        if(wday > 6)
        {
            wday = 0;
        }
    }
    while((last_day+offset) < day_options.length)
    {
        if(day_options.remove)
        {
            day_options.remove(day_options.length - 1);
        }
        else
        {
            day_options[day_options.length - 1] = null;
        }
    }

    if ( year == "1970" && month == "1" && day_options[offset].value == "1")
    {
        if(day_options.remove)
        {
            day_options.remove(offset);
            day_options.remove(offset);
        }
        else
        {
            day_options[offset] = null;
            day_options[offset] = null;
        }
    }
      
}
var G_yearUnit = "";

function open_iframe (form_name, prefix, frame_src)
{
    if (form_name == null || form_name == "")
    {
        var select_year  = document.getElementById(prefix+"year");
        var select_month = document.getElementById(prefix+"month");
        var select_day   = document.getElementById(prefix+"day");
    }
    else
    {
        form = document.forms[form_name];
        var select_year  = form.elements[prefix+"year"];
        var select_month = form.elements[prefix+"month"];
        var select_day   = form.elements[prefix+"day"];
    }

    if(!select_year.disabled)
    {
        var year = select_year.options[select_year.selectedIndex].value;
        var month = select_month.options[select_month.selectedIndex].value;
        var day = select_day.options[select_day.selectedIndex].value;
        frame_src = frame_src + '&date=' + year + '-' + month + '-' + day;

        var id = form_name+prefix+"SetDateCal";
        for(i=0; i < openIds.length; i++)
        {
            if (openIds[i] != id)
            {
                e = document.getElementById(openIds[i]);
                if( e && e.style )
                {
                    if( e.style.display == "" )
                    {
                        e.style.display = "none";
                    }
                }
            }
        }
        var f = document.getElementById(id);
        if(f && f.src)
        {
            f.src = frame_src;
        }
        cb_ui_select_date_display_calendar(id);
        openIds = new Array(id);
        if( typeof parent.update_back_step == 'function' )
        {
            parent.update_back_step();
        }
    }
}
//-->
</script>
<select id="popup_year" name="popup_year"onChange="popup_init(this.form)">
    {!!$year_html!!}
</select>
/<select id="popup_month" name="popup_month" onChange="popup_init(this.form)">
    {!!$month_html!!}
</select>
<script language="javascript" type="text/javascript"><!--

function reinit_day()
{
    var _prefix = "popup_";
    var _form = document.forms["{{$form_name}}"];
    var _select_day = _form.elements[_prefix + "day"];
    if(_select_day != null)
    {
        var _day = _select_day.options[_select_day.selectedIndex].value;

        cb_ui_select_date_init_day(_form, _prefix);

        for(i = 0 ; i < _select_day.options.length ; i ++)
        {
            if(_select_day.options[i].value == _day) 
            {
                _select_day.options[i].selected = true;
            }
        }
    }
}

if (window.addEventListener)
{
    window.addEventListener('load', reinit_day, false);
}
else if (window.attachEvent)
{
    window.attachEvent('onload', reinit_day);
}

//-->
</script>
   <input type="hidden" name="date" value="" />
   <input type="hidden" name="form_name" value="{{$form_name}}" />
   <input type="hidden" name="prefix" value="{{$pre}}" />
   <input name="script_name" type="hidden" value="cb_ui_select_date_init_date_select" />
   <input name="script_name2" type="hidden" value="cb_ui_select_date_display_calendar" />
   <input name="on_change" type="hidden" value="change_enddate();" />

   <script type="text/javascript">
     function popup_init(form) {
       var year = document.getElementById("popup_year").value;
       var month = parseInt(document.getElementById("popup_month").value);
       if(10 > month) month = "0" + month;

       var date = year + "-" + month + "-01";
       form["date"].value = date;
       form.submit();
     }
   </script>
   
  </td>
  <td nowrap align="right" style="width:5%"><input type="button" value=" &gt;&gt; " onClick='location.href="/schedule/popup_calendar?date={{$next_month}}&amp;prefix={{$pre}}&amp;form_name={{$form_name}}"'></td>
 </tr>
</table>

<table class="cal" style="width:300px" border="0" cellspacing="0" cellpadding="2">
 <tr class="popup_calendar_band_grn">
  <th class="popup_calendar_band_sunday_grn">Sun</th>
  <th>Mon</th>
  <th>Tue</th>
  <th>Wed</th>
  <th>Thu</th>
  <th>Fri</th>
  <th class="popup_calendar_band_saturday_grn">Sat</th>
 </tr>
 {!!$html!!}
</table>
</form>
</div>