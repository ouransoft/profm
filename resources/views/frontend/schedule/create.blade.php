@extends('frontend.layouts.create_schedule')
@section('content')
<div class="global_navi" role="heading" aria-level="2" style="border:none;" style="border:none;margin-bottom: 20px;">
    <a href="@if(Session::get('redirect_route')) {{route(Session::get('redirect_route'))}} @endif" style="height: 34px;vertical-align: middle;line-height: 50px;display: inline-block;padding: 0px 20px;font-size: 20px;border-right: 1px solid #333;margin-top: 10px;line-height: 4px;color: #333;">
        <img src="{{asset('/img/schedule20.gif')}}" border="0" alt="" style="margin-bottom: 10px;width: 30px;">
        {{trans('base.Scheduler')}}
   </a>
   <span class="globalNavi-item-last-grn" style="padding: 0 15px;top: 6px;font-size: 20px;color: #fe7701;font-weight: 400;">
        {{trans('base.New_appointment')}}
   </span>
</div>
<div class="mainarea ">
   <div class="mainareaSchedule-grn">
      <script src="{{asset('/js/schedules.js')}}" type="text/javascript"></script>
      <div>
         <div><span class="bold">{{date('D, F d, Y',strtotime($date))}}</span><a href="javascript:void(0);display_on_off('display_day_open:display_swith_image_open:display_swith_image_close')"><span id="display_swith_image_open" class="mLeft10">{{trans('base.Show_day_planner')}}<img src="{{asset('/img/addressee_on20.gif')}}" border="0" alt="{{trans('base.Show_day_planner')}}" title="{{trans('base.Show_day_planner')}}"></span><span id="display_swith_image_close" class="mLeft10" style="display:none;">{{trans('base.Hide_day_planner')}}<img src="{{asset('/img/addressee_off20.gif')}}" border="0" alt="{{trans('base.Hide_day_planner')}}<" title="{{trans('base.Hide_day_planner')}}<"></span></a></div>
      </div>
      <div id="display_day_open" style="display:none;">
         <table class="day_table group_day_calendar">
            {!!$html!!}
         </table>
      </div>
      <h2 style="display:inline-block;" class="schedule">{{trans('base.New_appointment')}}</h2>
      <script language="JavaScript" type="text/javascript">
         function change_enddate()
         {
             form = document.forms["schedule/add"];
             cb_ui_select_date_change_enddate( form );
         }

         //-->
      </script>
      <form class="js_customization_form" name="schedule/add" id="schedule/add" method="post" action="/schedule/store?">
         <input type="hidden" name="pattern" value="1">
         <input type="hidden" name="_token" value="{{ csrf_token() }}" />
         <input type="hidden" name="allow_file_attachment" value="1">
         <script language="JavaScript" type="text/javascript">
            <!--
            function add_menu_submit(t)
            {
                f = document.forms["schedule/add"];
                grn.page.schedule.add.grn_onsubmit_common( f );
                f.target = '_self';
                var tab_item = f.elements['tab'];
                if (t == 'normal')
                {
                    f.action = '/schedule/add?';
                    if( tab_item )
                        tab_item.value = 'add';
                    f.submit();
                }
                else if (t == 'banner')
                {
                    f.action = '/schedule/banner_add?';
                    if( tab_item )
                        tab_item.value = 'add';
                    f.submit();
                }
                else if (t == 'repeat')
                {
                    f.action = '/schedule/repeat_add?';
                    if( tab_item ){
                        tab_item.value = 'add';
                      f.submit();
                    }
                }
            }

            function insert_values( f, flg )
            {
                var form = document.forms[ "schedule/add" ];

                jQuery.each(jQuery("form[name=\"schedule/add\"] input[type=\"text\"]"), function(index, value){
                    var new_input = document.createElement("input");
                    new_input.setAttribute("type", "hidden");
                    new_input.setAttribute("name", value.name);
                    new_input.setAttribute("value", value.value);

                    f.appendChild( new_input );
                } );

                jQuery.each(jQuery("form[name=\"schedule/add\"] input[type=\"hidden\"]"), function(index, value){
                    var new_input = document.createElement("input");
                    new_input.setAttribute("type", "hidden");
                    new_input.setAttribute("name", value.name);
                    new_input.setAttribute("value", value.value);

                    f.appendChild( new_input );
                } );

                var src_private = form.elements["private"];
                if(src_private != null)
                {
                    var new_input;
                    for( var i = 0; i < src_private.length; i++ )
                    {
                        if(src_private[i].checked){
                            new_input = document.createElement("input");
                            new_input.setAttribute("type", "hidden");
                            new_input.setAttribute("name", "private");
                            new_input.setAttribute("value", src_private[i].value);

                            f.appendChild( new_input );
                        }
                    }
                }

                var facility_list = grn.component.member_select_list.get_instance("sITEM");
                var selected_items = '';
                if (facility_list){
                    if(flg === 'repeat') {
                        selected_items = facility_list.getRepeatableSelectedFacilitiesValues().join(":");
                    }
                    if(flg === 'adjust'){
                        selected_items = facility_list.getValues().join(":");
                    }
                }
                if(0 < selected_items.length)
                {
                    var el = f.elements["selected_users_sITEM"];
                    if( el )
                    {
                        el.value = selected_items;
                    }
                    else
                    {
                        var new_input = document.createElement("input");
                        new_input.setAttribute("type", "hidden");
                        new_input.setAttribute("name", "selected_users_sITEM");
                        new_input.setAttribute("value", selected_items);

                        f.appendChild(new_input);
                    }
                }

                var src_start_hour = form.elements["start_hour"];
                var src_start_hour_new_input = document.createElement("input");
                src_start_hour_new_input.setAttribute("type", "hidden");
                src_start_hour_new_input.setAttribute("name", src_start_hour.name);
                src_start_hour_new_input.setAttribute("value", src_start_hour.value);
                   f.appendChild(src_start_hour_new_input);

                var src_start_minute = form.elements["start_minute"];
                var src_start_minute_new_input = document.createElement("input");
                src_start_minute_new_input.setAttribute("type", "hidden");
                src_start_minute_new_input.setAttribute("name", src_start_minute.name);
                src_start_minute_new_input.setAttribute("value", src_start_minute.value);
                   f.appendChild(src_start_minute_new_input);

                var src_end_hour = form.elements["end_hour"];
                var src_end_hour_new_input = document.createElement("input");
                src_end_hour_new_input.setAttribute("type", "hidden");
                src_end_hour_new_input.setAttribute("name", src_end_hour.name);
                src_end_hour_new_input.setAttribute("value", src_end_hour.value);
                   f.appendChild(src_end_hour_new_input);

                   //-----
                var src_end_minute = form.elements["end_minute"];
                var src_end_minute_new_input = document.createElement("input");
                src_end_minute_new_input.setAttribute("type", "hidden");
                src_end_minute_new_input.setAttribute("name", src_end_minute.name);
                src_end_minute_new_input.setAttribute("value", src_end_minute.value);
                   f.appendChild(src_end_minute_new_input);

                var src_start_year = form.elements["start_year"];
                var src_start_year_new_input = document.createElement("input");
                src_start_year_new_input.setAttribute("type", "hidden");
                src_start_year_new_input.setAttribute("name", src_start_year.name);
                src_start_year_new_input.setAttribute("value", src_start_year.value);
                   f.appendChild(src_start_year_new_input);

                var src_start_month = form.elements["start_month"];
                var src_start_month_new_input = document.createElement("input");
                src_start_month_new_input.setAttribute("type", "hidden");
                src_start_month_new_input.setAttribute("name", src_start_month.name);
                src_start_month_new_input.setAttribute("value", src_start_month.value);
                   f.appendChild(src_start_month_new_input);

                var src_start_day = form.elements["start_day"];
                var src_start_day_new_input = document.createElement("input");
                src_start_day_new_input.setAttribute("type", "hidden");
                src_start_day_new_input.setAttribute("name", src_start_day.name);
                src_start_day_new_input.setAttribute("value", src_start_day.value);
                   f.appendChild(src_start_day_new_input);

                var src_end_year = form.elements["end_year"];
                var src_end_year_new_input = document.createElement("input");
                src_end_year_new_input.setAttribute("type", "hidden");
                src_end_year_new_input.setAttribute("name", src_end_year.name);
                src_end_year_new_input.setAttribute("value", src_end_year.value);
                   f.appendChild(src_end_year_new_input);

                var src_end_month = form.elements["end_month"];
                var src_end_month_new_input = document.createElement("input");
                src_end_month_new_input.setAttribute("type", "hidden");
                src_end_month_new_input.setAttribute("name", src_end_month.name);
                src_end_month_new_input.setAttribute("value", src_end_month.value);
                   f.appendChild(src_end_month_new_input);

                var src_end_day = form.elements["end_day"];
                var src_end_day_new_input = document.createElement("input");
                src_end_day_new_input.setAttribute("type", "hidden");
                src_end_day_new_input.setAttribute("name", src_end_day.name);
                src_end_day_new_input.setAttribute("value", src_end_day.value);
                   f.appendChild(src_end_day_new_input);

                var src_menu = form.elements["menu"];
                var src_menu_new_input = document.createElement("input");
                src_menu_new_input.setAttribute("type", "hidden");
                src_menu_new_input.setAttribute("name", src_menu.name);
                src_menu_new_input.setAttribute("value", src_menu.value);
                   f.appendChild(src_menu_new_input);

                var src_title = form.elements["title"];
                var src_title_new_input = document.createElement("input");
                src_title_new_input.setAttribute("type", "hidden");
                src_title_new_input.setAttribute("name", src_title.name);
                src_title_new_input.setAttribute("value", src_title.value);
                   f.appendChild(src_title_new_input);

                var src_memo = form.elements["memo"];
                var src_memo_new_input = document.createElement("input");
                src_memo_new_input.setAttribute("type", "hidden");
                src_memo_new_input.setAttribute("name", src_memo.name);
                src_memo_new_input.setAttribute("value", src_memo.value);
                   f.appendChild(src_memo_new_input);

                var tab_new_input = document.createElement("input");
                tab_new_input.setAttribute("type", "hidden");
                tab_new_input.setAttribute("name", 'tab');
                tab_new_input.setAttribute("value", 'add');
                var tab = f.appendChild(tab_new_input);

                var csrf_ticket_new_input = document.createElement("input");
                csrf_ticket_new_input.setAttribute("type", "hidden");
                csrf_ticket_new_input.setAttribute("name", 'csrf_ticket');
                csrf_ticket_new_input.setAttribute("value", 'csrf_ticket');
                var csrf_ticket = form.elements["csrf_ticket"];
                   f.appendChild(csrf_ticket_new_input);


            }

            function check_facility( f )
            {
                insert_values(f, 'repeat');
                var facility_select = grn.component.facility_add.get_instance("facility_select");
                if ( ! facility_select){
                    return true;
                }
                var facility_list = facility_select.facilityList.getList();
                for (var i = 0; i < facility_list.length; i++){
                    var facility = jQuery(facility_list[i]);
                    if( facility.attr("data-repeat") === "0" || facility.attr("approval") === "1" )
                    {
                        after_check_facility(f);
                        return true;
                    }
                }


                after_check_facility(f);
                return false;
            }

            function after_check_facility(f)
            {
                var private_select = grn.component.member_add.get_instance("private_select");
                if (private_select){
                    private_select.prepareSubmit();
                }
                var member_select = grn.component.member_add.get_instance("member_select");
                if (member_select) {
                    member_select.prepareSubmit();
                }
                var facility_select = grn.component.facility_add.get_instance("facility_select");
                if (facility_select) {
                    facility_select.prepareSubmit();
                }

                var form = document.forms['schedule/add'];
                var el = form.elements['selected_users_sUID'];
                if( f.elements[el.name] )
                {
                    f.elements[el.name].value = el.value;
                }
                else
                {
                    let new_input = document.createElement("input");
                    new_input.setAttribute("type","hidden");
                    new_input.setAttribute("name",el.name);
                    new_input.setAttribute("value",el.value);
                    f.appendChild(new_input);
                }
                el = form.elements['selected_users_p_sUID'];
                console.log(el);
                if( el )
                {
                    if( f.elements[el.name] )
                    {
                        f.elements[el.name].value = el.value;
                    }
                    else
                    {
                        let new_input_el = document.createElement("input");
                        new_input_el.setAttribute("type","hidden");
                        new_input_el.setAttribute("name",el.name);
                        new_input_el.setAttribute("value",el.value);
                        f.appendChild(new_input_el);
                    }
                }
            }
            //-->
         </script>
         <input type="hidden" name="tab" value="">
         <input type="hidden" name="bdate" value="2020-12-23">
         <input type="hidden" name="uid" value="58">
         <input type="hidden" name="gid" value="f">
         <input type="hidden" name="referer_key" value="">
         <div class="tab">
            <span class="tab_left_on"></span>
            <span class="tab_on">
            <span class="tab_text_noimage"> {{trans('base.Regular')}}
            </span>  </span>
            <span class="tab_right_on"></span>
         </div>
         <div class="tab">
            <span class="tab_left_off"></span>
            <span class="tab_off">
            <span class="tab_text_noimage"><a href="{{route('frontend.schedule.create_all_day',$_GET)}}">  {{trans('base.All_day')}}
            </a></span>  </span>
            <span class="tab_right_off"></span>
         </div>
         <div class="tab">
            <span class="tab_left_off"></span>
            <span id="tab-repeat-schedule" class="tab_off">
            <span class="tab_text_noimage">
            <a href="{{route('frontend.schedule.create_repeat',$_GET)}}">{{trans('base.Repeating')}}</a>
            </span>
            </span>
            <span class="tab_right_off"></span>
         </div>
         <div class="tab_menu_end">&nbsp;</div>
         <div class="clear_both_1px">&nbsp;</div>
         <div class="js_customization_schedule_header_space"></div>
         <table class="std_form" id="main_table" style="border-collapse: separate;">
            <tr valign="top">
               <th nowrap>{{trans('base.Date_and_time')}}</th>
               <td>
                  <table>
                     <tr>
                        <td nowrap>
                           <script lang="javascript" type="text/javascript">
                              <!--
                              var wday_name = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
                              var openIds = new Array(0);
                              //-->
                           </script>
                           <script lang="javascript" type="text/javascript" src="{!!asset('/js/select_date.js')!!}"></script>
                           <script language="javascript" type="text/javascript"><!--
                              var g_arrayHolidays = new Array();
                              g_arrayHolidays[0] = "2011-01-01";
                              g_arrayHolidays[1] = "2011-01-10";
                              g_arrayHolidays[2] = "2011-02-11";
                              g_arrayHolidays[3] = "2011-03-21";
                              g_arrayHolidays[4] = "2011-04-29";
                              g_arrayHolidays[5] = "2011-05-03";
                              g_arrayHolidays[6] = "2011-05-04";
                              g_arrayHolidays[7] = "2011-05-05";
                              g_arrayHolidays[8] = "2011-07-18";
                              g_arrayHolidays[9] = "2011-09-19";
                              g_arrayHolidays[10] = "2011-09-23";
                              g_arrayHolidays[11] = "2011-10-10";
                              g_arrayHolidays[12] = "2011-11-03";
                              g_arrayHolidays[13] = "2011-11-23";
                              g_arrayHolidays[14] = "2011-12-23";
                              g_arrayHolidays[15] = "2012-01-01";
                              g_arrayHolidays[16] = "2012-01-02";
                              g_arrayHolidays[17] = "2012-01-09";
                              g_arrayHolidays[18] = "2012-02-11";
                              g_arrayHolidays[19] = "2012-03-20";
                              g_arrayHolidays[20] = "2012-04-29";
                              g_arrayHolidays[21] = "2012-04-30";
                              g_arrayHolidays[22] = "2012-05-03";
                              g_arrayHolidays[23] = "2012-05-04";
                              g_arrayHolidays[24] = "2012-05-05";
                              g_arrayHolidays[25] = "2012-07-16";
                              g_arrayHolidays[26] = "2012-09-17";
                              g_arrayHolidays[27] = "2012-09-22";
                              g_arrayHolidays[28] = "2012-10-08";
                              g_arrayHolidays[29] = "2012-11-03";
                              g_arrayHolidays[30] = "2012-11-23";
                              g_arrayHolidays[31] = "2012-12-23";
                              g_arrayHolidays[32] = "2012-12-24";
                              g_arrayHolidays[33] = "2013-01-01";
                              g_arrayHolidays[34] = "2013-01-14";
                              g_arrayHolidays[35] = "2013-02-11";
                              g_arrayHolidays[36] = "2013-03-20";
                              g_arrayHolidays[37] = "2013-04-29";
                              g_arrayHolidays[38] = "2013-05-03";
                              g_arrayHolidays[39] = "2013-05-04";
                              g_arrayHolidays[40] = "2013-05-05";
                              g_arrayHolidays[41] = "2013-05-06";
                              g_arrayHolidays[42] = "2013-07-15";
                              g_arrayHolidays[43] = "2013-09-16";
                              g_arrayHolidays[44] = "2013-09-23";
                              g_arrayHolidays[45] = "2013-10-14";
                              g_arrayHolidays[46] = "2013-11-03";
                              g_arrayHolidays[47] = "2013-11-04";
                              g_arrayHolidays[48] = "2013-11-23";
                              g_arrayHolidays[49] = "2013-12-23";
                              g_arrayHolidays[50] = "2014-01-01";
                              g_arrayHolidays[51] = "2014-01-13";
                              g_arrayHolidays[52] = "2014-02-11";
                              g_arrayHolidays[53] = "2014-03-21";
                              g_arrayHolidays[54] = "2014-04-29";
                              g_arrayHolidays[55] = "2014-05-03";
                              g_arrayHolidays[56] = "2014-05-04";
                              g_arrayHolidays[57] = "2014-05-05";
                              g_arrayHolidays[58] = "2014-05-06";
                              g_arrayHolidays[59] = "2014-07-21";
                              g_arrayHolidays[60] = "2014-09-15";
                              g_arrayHolidays[61] = "2014-09-23";
                              g_arrayHolidays[62] = "2014-10-13";
                              g_arrayHolidays[63] = "2014-11-03";
                              g_arrayHolidays[64] = "2014-11-23";
                              g_arrayHolidays[65] = "2014-11-24";
                              g_arrayHolidays[66] = "2014-12-23";
                              g_arrayHolidays[67] = "2015-01-01";
                              g_arrayHolidays[68] = "2015-01-12";
                              g_arrayHolidays[69] = "2015-02-11";
                              g_arrayHolidays[70] = "2015-03-21";
                              g_arrayHolidays[71] = "2015-04-29";
                              g_arrayHolidays[72] = "2015-05-03";
                              g_arrayHolidays[73] = "2015-05-04";
                              g_arrayHolidays[74] = "2015-05-05";
                              g_arrayHolidays[75] = "2015-05-06";
                              g_arrayHolidays[76] = "2015-07-20";
                              g_arrayHolidays[77] = "2015-09-21";
                              g_arrayHolidays[78] = "2015-09-22";
                              g_arrayHolidays[79] = "2015-09-23";
                              g_arrayHolidays[80] = "2015-10-12";
                              g_arrayHolidays[81] = "2015-11-03";
                              g_arrayHolidays[82] = "2015-11-23";
                              g_arrayHolidays[83] = "2015-12-23";

                              var g_arrayWorkdays = new Array();

                              function cb_ui_select_date_init_date_select( form_name, year, month, day, prefix)
                              {
                                  var year_select = document.forms[form_name].elements[prefix+"year"];
                                  if(year_select == undefined) return false;
                                  var month_select = document.forms[form_name].elements[prefix+"month"];
                                  if(month_select == undefined) return false;
                                  var day_select = document.forms[form_name].elements[prefix+"day"];
                                  if(day_select == undefined) return false;

                                  var year_options = year_select.options;
                                  var month_options = month_select.options;
                                  var day_options = day_select.options;
                                  var select_year = 0;

                                  for(var i = 0 ; i <= year_options.length - 1 ; i++)
                                  {
                                      if(year_options[i].value == year)
                                      {
                                          year_options[i].selected = true;
                                          select_year = 1;
                                      }
                                  }

                                  if( ! select_year)
                                  {
                                      if(year_options[0].value > year)
                                      {
                                          var o = document.createElement( "OPTION" );
                                          year_options.add(o);
                                          for(var i = year_options.length - 1 ; i > 0 ; i --)
                                          {
                                              var dst = year_options[i];
                                              var src = year_options[i - 1];
                                              dst.value = src.value;
                                              dst.text = src.text;
                                              dst.selected = false;
                                          }
                                          year_options[0].value = year;
                                          year_options[0].text = year + ' year';
                                          year_options[0].selected = true;
                                      }
                                      else
                                      {
                                          var o = document.createElement( "OPTION" );
                                          o.value = year;
                                          o.text = year + ' year';
                                          o.selected = true;
                                          year_options.add( o );
                                      }
                                  }

                                  for(var i = 0 ; i <= month_options.length - 1 ; i++)
                                  {
                                      if(month_options[i].value == month)
                                      {
                                          month_options[i].selected = true;
                                      }
                                  }

                                  var start_date = new Date(year, month - 1, 1);
                                  var wday = start_date.getDay();
                                  var last_day = cb_ui_select_date_get_last_day(year, month);
                                  var offset = 0;
                                  if( day_options.length > 0 && day_options[0].value < 1 )
                                  {
                                      offset = 1;
                                      day_options[0].className = "";
                                      day_options[0].text = "--";
                                  }

                                  for(i = 0 ; i < last_day ; i ++)
                                  {
                                      day_option = i + 1;
                                      opt_idx = i + offset;

                                      if(opt_idx >= day_options.length)
                                      {
                                          cb_ui_select_date_add_option(day_options, day_option);
                                      }
                                      day_options[opt_idx].value = day_option;

                                      if(wday == 0)
                                      {
                                          day_options[opt_idx].className = "s_date_sunday";
                                      }
                                      else if(wday == 6)
                                      {
                                          day_options[opt_idx].className = "s_date_saturday";
                                      }
                                      else
                                      {
                                          day_options[opt_idx].className = "";
                                      }

                                      if(g_arrayHolidays != null && g_arrayHolidays.length > 0)
                                      {
                                          if(cb_ui_select_date_check_holiday(g_arrayHolidays, year, month, day_option))
                                          {
                                              day_options[opt_idx].className = "s_date_holiday";
                                          }
                                      }

                                      day_options[opt_idx].text = day_option + "(" + wday_name[wday] + ")";
                                      if(day_options[opt_idx].value == day)
                                      {
                                          day_options[opt_idx].selected = true;
                                      }

                                      wday ++;
                                      if(wday > 6)
                                      {
                                          wday = 0;
                                      }
                                  }
                                  while((last_day+offset) < day_options.length)
                                  {
                                      if(day_options.remove)
                                      {
                                          day_options.remove(day_options.length - 1);
                                      }
                                      else
                                      {
                                          day_options[day_options.length - 1] = null;
                                      }
                                  }

                                  if ( year == "1970" && month == "1" && day_options[offset].value == "1")
                                  {
                                      if(day_options.remove)
                                      {
                                          day_options.remove(offset);
                                          day_options.remove(offset);
                                      }
                                      else
                                      {
                                          day_options[offset] = null;
                                          day_options[offset] = null;
                                      }
                                  }

                              }
                              var G_yearUnit = "";

                              function open_iframe (form_name, prefix, frame_src)
                              {
                                  if (form_name == null || form_name == "")
                                  {
                                      var select_year  = document.getElementById(prefix+"year");
                                      var select_month = document.getElementById(prefix+"month");
                                      var select_day   = document.getElementById(prefix+"day");
                                  }
                                  else
                                  {
                                      form = document.forms[form_name];
                                      var select_year  = form.elements[prefix+"year"];
                                      var select_month = form.elements[prefix+"month"];
                                      var select_day   = form.elements[prefix+"day"];
                                  }

                                  if(!select_year.disabled)
                                  {
                                      var year = select_year.options[select_year.selectedIndex].value;
                                      var month = select_month.options[select_month.selectedIndex].value;
                                      var day = select_day.options[select_day.selectedIndex].value;
                                      frame_src = frame_src + '&date=' + year + '-' + month + '-' + day;

                                      var id = form_name+prefix+"SetDateCal";
                                      for(i=0; i < openIds.length; i++)
                                      {
                                          if (openIds[i] != id)
                                          {
                                              e = document.getElementById(openIds[i]);
                                              if( e && e.style )
                                              {
                                                  if( e.style.display == "" )
                                                  {
                                                      e.style.display = "none";
                                                  }
                                              }
                                          }
                                      }
                                      var f = document.getElementById(id);
                                      if(f && f.src)
                                      {
                                          f.src = frame_src;
                                      }

                                      cb_ui_select_date_display_calendar(id);
                                      openIds = new Array(id);
                                      if( typeof parent.update_back_step == 'function' )
                                      {
                                          parent.update_back_step();
                                      }
                                  }
                              }
                              //-->
                           </script>
                           <select id="start_month" name="start_month" onChange="cb_ui_select_date_init_day(this.form, 'start_');change_enddate();">
                           {!!isset($start_month_html)? $start_month_html : $month_html!!}
                           </select>
                           <select id="start_day" name="start_day" onChange="change_enddate();">
                           {!!isset($start_day_html)? $start_day_html : $day_html!!}
                           </select>
                           <select id="start_year" name="start_year"onChange="cb_ui_select_date_init_day(this.form, 'start_');cb_ui_select_date_reset_year_range(this.form, 'start_');change_enddate();">
                           {!!isset($start_year_html)? $start_year_html : $year_html!!}
                           </select>
                           <script language="javascript" type="text/javascript"><!--
                              function reinit_day()
                              {
                                  var _prefix = "start_";
                                  var _form = document.forms["schedule/add"];
                                  var _select_day = _form.elements[_prefix + "day"];
                                  if(_select_day != null)
                                  {
                                      var _day = _select_day.options[_select_day.selectedIndex].value;

                                      cb_ui_select_date_init_day(_form, _prefix);

                                      for(i = 0 ; i < _select_day.options.length ; i ++)
                                      {
                                          if(_select_day.options[i].value == _day)
                                          {
                                              _select_day.options[i].selected = true;
                                          }
                                      }
                                  }
                              }

                              if (window.addEventListener)
                              {
                                  window.addEventListener('load', reinit_day, false);
                              }
                              else if (window.attachEvent)
                              {
                                  window.attachEvent('onload', reinit_day);
                              }

                              //-->
                           </script>
                           <a name="" style="cursor: pointer"><img src="{!!asset('/img/calendar20.gif')!!}" border="0" align="absmiddle" style="cursor:hand;" onClick='open_iframe("schedule/add", "start_", "/schedule/popup_calendar?prefix=start_&form_name=schedule/add&on_change=change_enddate();");' title="To select a date in the past three years or more, use the date selection calendar." ></a>
                           <iframe id="schedule/addstart_SetDateCal" frameborder="no" scrolling="no" style="display:none; position:absolute; width:308px; height:17.5em;z-index:10;" src=""></iframe>&nbsp;
                           <select id="start_hour" name="start_hour"   >
                           {!! isset($start_hour_html)? $start_hour_html : $hour_html!!}
                           </select>
                           <select id="start_minute" name="start_minute"  >
                           {!!isset($start_minute_html)? $start_minute_html : $minute_html!!}
                           </select>
                           <span id="applied_timezone" class="span_timezone" style='display:none;'><span id='span_timezone'>(UTC+07:00) VietNam</span></span><script src="{!!asset('/js/time_selector.js')!!}" type="text/javascript"></script><script language="JavaScript" type="text/javascript">var grn_space_url = '/grn/space?';</script><img src="{!!asset('/img/event.gif')!!}" id="time_selector" name="time_selector" align="absmiddle" height="20" width="20" style="cursor: pointer;" title="You can specify the period with the start time and the ending time." onclick="grn_show_hide('time_selector', '/schedule/popup_time_selector?', 'schedule/add', 'start_hour', 'end_hour', '0');"> -
                           <p class="half">
                              <select id="end_month" name="end_month" onChange="cb_ui_select_date_init_day(this.form, 'end_');">
                              {!!isset($end_month_html)? $end_month_html : $month_html!!}
                              </select>
                              <select id="end_day" name="end_day">
                              {!!isset($end_day_html)? $end_day_html : $day_html!!}
                              </select>
                              <select id="end_year" name="end_year"onChange="cb_ui_select_date_init_day(this.form, 'end_');cb_ui_select_date_reset_year_range(this.form, 'end_');">
                              {!!isset($end_year_html)? $end_year_html : $year_html!!}
                              </select>
                              <script language="javascript" type="text/javascript"><!--
                                 function reinit_day()
                                 {
                                     var _prefix = "end_";
                                     var _form = document.forms["schedule/add"];
                                     var _select_day = _form.elements[_prefix + "day"];
                                     if(_select_day != null)
                                     {
                                         var _day = _select_day.options[_select_day.selectedIndex].value;

                                         cb_ui_select_date_init_day(_form, _prefix);

                                         for(i = 0 ; i < _select_day.options.length ; i ++)
                                         {
                                             if(_select_day.options[i].value == _day)
                                             {
                                                 _select_day.options[i].selected = true;
                                             }
                                         }
                                     }
                                 }
                                 if (window.addEventListener)
                                 {
                                     window.addEventListener('load', reinit_day, false);
                                 }
                                 else if (window.attachEvent)
                                 {
                                     window.attachEvent('onload', reinit_day);
                                 }

                                 //-->
                              </script><a name="" style="cursor: pointer"><img src="{!!asset('/img/calendar20.gif')!!}" border="0" align="absmiddle" style="cursor:hand;" onClick='open_iframe("schedule/add", "end_", "/schedule/popup_calendar?prefix=end_&form_name=schedule/add&on_change=");' title="To select a date in the past three years or more, use the date selection calendar." ></a>
                              <iframe id="schedule/addend_SetDateCal" frameborder="no" scrolling="no" style="display:none; position:absolute; width:308px; height:17.5em;z-index:10;" src=""></iframe>&nbsp;
                              <select id="end_hour" name="end_hour"   >
                              {!!isset($end_hour_html)? $end_hour_html : $hour_html!!}
                              </select>
                              <select id="end_minute" name="end_minute"  >
                              {!!isset($end_minute_html)? $end_minute_html : $minute_html!!}
                              </select>
                              <span id="applied_end_timezone" class="span_timezone" style='display:none;'><span id='span_end_timezone'>(UTC+07:00) VietNam</span></span>
                           <div>
                           <span id="span_time_span" class="margin_top sub_explanation"><span class="attention" id="validate_date" style="display:none;">{{ trans('base.Time_is_empty') }}</span><span class="attention" id="invalid_date" style="display:none;">{{ trans('base.The_end_time_must_follow_the_start_time') }}.</span></span></div>
                           
                           <script type="text/javascript" src="{!!asset('/js/schedule_tz.js')!!}" ></script>
                           <div class="overlay" style="display:none;" id="background"></div>
                           <div id="timezone_dialog" class="msgbox center" style="width:auto;display:none;">
                              <div id="timezone_dialog_buttons" class="command">
                                 <span id="apply_timezone" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="GRN_ScheduleSelectTimezone.applyTimezone();"  data-auto-disable=><a href="javascript:void(0);" role="button">{{trans('base.Apply')}}</a></span><span id="cancel_timezone" class="button_grn_js button1_normal_grn" onclick="GRN_ScheduleSelectTimezone.closeTimezoneDialog();"  data-auto-disable=><a href="javascript:void(0);" role="button">{{trans('base.Cancel')}}</a></span>
                              </div>
                           </div>
                           <input type='hidden' id='timezone' name='timezone' value='Asia/Tokyo' />
                           <input type='hidden' id='end_timezone' name='end_timezone' value='Asia/Tokyo' />
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr>
               <th nowrap>{{trans('base.Subject')}}</th>
               <td>
                  <select menu="schedule_menu" name="menu" class="hidden">
                     @if(isset($schedule))
                     <option value="{{$schedule->menu}}">{{explode(';#',$schedule->menu)[0]}}</option>
                     @endif
                     <option value="">-----</option>
                     <option value="Cuộc họp;#5">Cuộc họp</option>
                     <option value="Hội thảo;#2">Hội thảo</option>
                     <option value="Đào tạo;#1">Đào tạo</option>
                     <option value="Công tác;#7">Công tác</option>
                     <option value="Ngày lễ;#6">Ngày lễ</option>
                     <option value="Phỏng vấn;#3">Phỏng vấn</option>
                     <option value="Gặp gỡ;#4">Gặp gỡ</option>
                  </select>
                   &nbsp;<input type="text" id="subject" name="title" value="@if(isset($schedule)) {{$schedule->title}} @endif">
                   <span class="attention margin_top sub_explanation" id="validate_subject" style="display:none;">{{trans('base.Subject_is_empty')}}.</span>
               </td>
            </tr>
            <tr valign="top">
               <th nowrap>{{trans('base.Attendees')}}</th>
               <td>
                  <style type="text/css">
                     #spinner-loading-CGID {
                     background-image: url( {{asset('/img/spinner.gif')}});
                     }
                  </style>
                  <script src="{!!asset('/js/pubsub.js')!!}" type="text/javascript"></script>
                  <script src="{!!asset('/js/member_add.js')!!}" type="text/javascript"></script>
                  <script src="{!!asset('/js/member_select_list.js')!!}" type="text/javascript"></script>
                  <script src="{!!asset('/js/pulldown_menu.js')!!}" type="text/javascript"></script>
                  <script language="JavaScript" type="text/javascript">
                     <!--
                     new grn.component.member_add.MemberAdd("member_select", "schedule/add", ["sUID"], "CID",
                             {
                                 categorySelectUrl: '/api/ajax_user_add_select_by_group',
                                 searchBoxOptions: {"is_use":true,"id_searchbox":"user","url":"/api/search_members_by_keyword","append_post_data":[]},
                                 pulldownPartsOptions: {"is_use":true,"pulldown_id":"CID_pulldown"},
                                 appId: "schedule",
                                 isCalendar: true,
                                 showGroupRole: true,
                                 includeOrg: "1",
                                 accessPlugin: true,
                                 accessPluginEncoded: "YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjI6e3M6NjoiYWN0aW9uIjthOjI6e2k6MDtzOjQ6InJlYWQiO2k6MTtzOjM6ImFkZCI7fXM6MTI6InNlc3Npb25fbmFtZSI7czoxMjoic2NoZWR1bGUvYWRkIjt9fQ==",
                                 pluginSessionName: "schedule/add",
                                 pluginDataName: "access_plugin",
                                 addOrgWithUsers: false,
                                 showOmitted: false,
                                 useCandidateSupportParts: false,
                                 operatorAddName: "",
                                 selectAllUsersInSearch: false            }
                     );

                     //-->
                  </script>
                  <input type="hidden" name="selected_users_sUID" value="">
                  <input type="hidden" name="selected_groups_sUID" value="">
                  <input type="hidden" name="selected_roles_sUID" value="">
                  <input type="hidden" name="for_redirect_at_non_command_page[]" value="">
                  <table class="table_plain_grn selectlist_base_grn">
                     <tr>
                        <td class="vAlignTop-grn" style="padding-left:0">
                           <table class="table_plain_grn">
                              <tr>
                                 <td class="buttonSlectOrder-grn">
                                    <div id="sUID_order_top" class="mBottom10">
                                       <a class="order_top_grn"
                                          aria-label="Move to top"
                                          title="Move to top"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="sUID_order_up" class="mBottom10">
                                       <a class="order_up_grn"
                                          aria-label="Move up"
                                          title="Move up"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="sUID_order_down" class="mBottom10">
                                       <a class="order_down_grn"
                                          aria-label="Move down"
                                          title="Move down"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="sUID_order_bottom" class="mBottom10">
                                       <a class="order_bottom_grn"
                                          aria-label="Move to bottom"
                                          title="Move to bottom"
                                          href="javascript:void(0)"></a>
                                    </div>
                                 </td>
                                 <td class="item_select">
                                    <div class="selectlist_area_grn">
                                       <span id="spinner_selectlist_sUID" style="display:none;position:absolute;z-index:1;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                       <div id="selectlist_base_selectlist_sUID" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn" tabindex="0" style="">
                                          <ul id="ul_selectlist_sUID">
                                             @if(isset($schedule))
                                             @foreach($schedule->member as $key=>$val)
                                             <li
                                                id="selectlist_sUID_member_user_58"
                                                class="selectlist_sUID"
                                                data-value="{{$val->id}}"
                                                data-type="user"
                                                data-name="{{$val->full_name}}"
                                                data-code="brown"                    data-id="{{$val->id}}"
                                                data-group-path=""
                                                data-url="">
                                                <span class="@if($schedule->uid == $val->id )selectlist_user_login_grn @else selectlist_user_grn @endif" aria-label="Login user"
                                                   title="Login user"></span>
                                                <span class="selectlist_text_grn">{{$val->full_name}}</span>
                                             </li>
                                             @endforeach
                                             @else
                                             <li
                                                id="selectlist_sUID_member_user_{{$member->id}}"
                                                class="selectlist_sUID"
                                                data-value="{{$member->id}}"
                                                data-type="user"
                                                data-name="{{$member->full_name}}"
                                                data-code="brown" data-id="{{$member->id}}"
                                                data-group-path=""
                                                data-url="">
                                                @if($member->id != \Auth::guard('member')->user()->id)
                                                <span class="selectlist_user_grn"></span>
                                                @else
                                                <span class="selectlist_user_login_grn" aria-label="Login user"
                                                   title="Login user"></span>
                                                @endif
                                                <span class="selectlist_text_grn">{{$member->full_name}}</span>
                                             </li>
                                             @endif
                                          </ul>
                                          <span id="spinner_scroll_selectlist_sUID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                       </div>
                                    </div>
                                    <div class="textSub-grn mTop5"><a id="select_all_selectlist_sUID" class="mRight20" href="javascript:void(0);">{{trans('base.Select_all')}}</a><a id="un_select_all_selectlist_sUID" style="display:none" class="mRight20" href="javascript:void(0);">{{ trans('base.Clear_all') }}</a></div>
                                 </td>
                              </tr>
                           </table>
                        </td>
                        <td class="vAlignTop-grn item_right_left">
                           <div class="buttonSelectMove-grn">
                              <div class="mBottom15">
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_add_sUID" style="padding-left:0;" href="javascript:void(0);">
                                 <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">{{trans('base.Add')}}</span>
                                 </a>
                                 </span>
                              </div>
                              <div>
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_rmv_sUID" style="padding-right:0;" href="javascript:void(0);">
                                 <span class="aButtonText-grn">{{trans('base.Remove')}}</span><span class="icon-buttonArrowRightBehind-grn"></span>
                                 </a>
                                 </span>
                              </div>
                           </div>
                        </td>
                        <td class="vAlignTop-grn">
                           <div class="mTop3 mBottom7 clearFix-cybozu">
                              <script src="{!!asset('/js/search_box.js')!!}" type="text/javascript"></script>
                              <div class="search_navi">
                                 <div class="searchbox-grn">
                                    <div id="searchbox-cybozu-user"
                                       class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                       <input class="input-text-cybozu prefix-grn" style="width:246px;" type="text"
                                          id="keyword_user" name="" autocomplete="off"
                                          value="{{trans('base.User_search')}}"
                                          maxlength="">
                                       <button id="searchbox-submit-user" class="searchbox-submit-cybozu" type="button"
                                          title="Search"
                                          aria-label="Search"></button>
                                    </div>
                                    <div class="clear_both_0px"></div>
                                 </div>
                              </div>
                           </div>
                           <!-- category select -->
                           <link href="{!!asset('assets/css/pulldown_menu.css')!!}" rel="stylesheet" type="text/css">
                           <div class="mBottom7 nowrap-grn">
                              <dl id="CID_pulldown" class="selectmenu_grn selectmenu_base_grn">
                                 <dt><a href="javascript:void(0)" class="nowrap-grn"><span></span><span class="selectlist_selectmenu_item_grn pulldown_head"></span><span class="pulldownbutton_arrow_down_grn mLeft3"></span></a></dt>
                                 <dd>
                                    <div class="pulldown_menu_grn" style="display: none;">
                                       <ul>
                                          <li ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">{{trans('base.All')}}</span></a></li>
                                          @foreach($departments as $key=>$val)
                                          <li data-value="{{$val->id}}" ><a href="javascript:void(0)" @if($val->level > 1) style='padding-left:{{($val->level -1) * 30}}px' @endif><span class="vAlignMiddle-grn">{{$val->name}} </span></a></li>
                                          @endforeach
                                       </ul>
                                    </div>
                                 </dd>
                              </dl>
                           </div>
                           <!-- category select -->
                           <div class="selectlist_area_grn">
                              <span id="spinner_selectlist_CID" style="display:none;position:absolute;z-index:1;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                              <div id="selectlist_base_selectlist_CID" aria-multiselectable="true" class="selectlist_grn selectlist_r_grn" tabindex="0" style="">
                                 <ul id="ul_selectlist_CID">
                                 </ul>
                                 <span id="spinner_scroll_selectlist_CID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                              </div>
                           </div>
                           <div class="textSub-grn mTop5">
                              <a id="select_all_selectlist_CID" class="mRight20" href="javascript:void(0);">{{trans('base.Select_all')}}</a><a id="un_select_all_selectlist_CID" style="display:none" class="mRight20" href="javascript:void(0);">{{ trans('base.Clear_all') }}</a>    <script language="JavaScript" type="text/javascript">
                                 <!--
                                 var popupMember_CID_count = 0;
                                 function popupMember_CID(Aform)
                                 {
                                     if ( ! popupMember_CID_count || document.getElementById('form_CID[]') == null)
                                     {
                                         var p = document.getElementById('div_CID[]');
                                         var c = document.createElement('DIV');
                                         p.style.display = "";
                                         p.appendChild(c);
                                         c.innerHTML = '<iframe id="form_CID[]" name="form_CID[]" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';
                                     }

                                     popupWin("","user_list",1034,675,0,0,0,1,0,1);
                                     var url = '/grn/popup_user_list?';
                                     var html = '<!DOCTYPE html><html lang="en"><body><form method="post" target="user_list" action="' + url + '">';
                                     html += '<input type="hidden" name="system_display" value="0">';
                                     var form_frame = document.getElementById('form_CID[]').contentWindow.document;
                                     var instance = grn.component.member_select_list.get_instance("CID");
                                     var values = instance.getSelectedUsersValues();
                                     html += '<input type="hidden" name="cid" value="' + values.join(':') + '">';
                                     html += '</form></body></html>';
                                     form_frame.write( html );
                                     form_frame.close();
                                     form_frame.forms[0].submit();
                                     popupMember_CID_count = 1;
                                     if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                                     {
                                         update_back_step();
                                     }
                                     return true;
                                 }
                                 //-->
                              </script>
                              <div id="div_CID[]" style="display:none; position:absolute; width:0em; height:0em;">&nbsp;</div>

                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr valign="top">
               <th nowrap>{{trans('base.Shared_with')}}</th>
               <td>
                  <style type="text/css">
                     #spinner-loading-p_CGID {
                     background-image: url({{asset('/img/spinner.gif')}});
                     }
                  </style>
                  <script language="JavaScript" type="text/javascript">
                     <!--
                     compo = new grn.component.member_add.MemberAdd("private_select", "schedule/add", ["p_sUID"], "p_CID",
                             {
                                 categorySelectUrl: '/api/ajax_user_add_select_by_group',
                                 searchBoxOptions: {"is_use":true,"id_searchbox":"private_menu","url":"/api/search_members_by_keyword","append_post_data":[]},
                                 pulldownPartsOptions: {"is_use":true,"pulldown_id":"p_CID_pulldown"},
                                 appId: "schedule",
                                 isCalendar: false,
                                 showGroupRole: true,
                                 includeOrg: "1",
                                 accessPlugin: true,
                                 accessPluginEncoded: "YToyOntzOjQ6Im5hbWUiO3M6ODoic2NoZWR1bGUiO3M6NjoicGFyYW1zIjthOjI6e3M6NjoiYWN0aW9uIjthOjE6e2k6MDtzOjQ6InJlYWQiO31zOjEyOiJzZXNzaW9uX25hbWUiO3M6MTc6InNjaGVkdWxlL2FkZC92aWV3Ijt9fQ==",
                                 pluginSessionName: "private_menu",
                                 pluginDataName: "access_plugin_private_menu",
                                 addOrgWithUsers: false,
                                 showOmitted: false,
                                 useCandidateSupportParts: false,
                                 operatorAddName: "",
                                 selectAllUsersInSearch: false            }
                     );

                     //-->
                  </script>
                  <input type="hidden" name="selected_users_p_sUID" value="">
                  <input type="hidden" name="selected_groups_p_sUID" value="">
                  <input type="hidden" name="selected_roles_p_sUID" value="">
                  <table class="table_plain_grn selectlist_base_grn">
                     <tr>
                        <td class="vAlignTop-grn" style="padding-left:0">
                           <table class="table_plain_grn">
                              <tr>
                                 <td class="buttonSlectOrder-grn">
                                    <div id="p_sUID_order_top" class="mBottom10">
                                       <a class="order_top_grn"
                                          aria-label="Move to top"
                                          title="Move to top"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="p_sUID_order_up" class="mBottom10">
                                       <a class="order_up_grn"
                                          aria-label="Move up"
                                          title="Move up"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="p_sUID_order_down" class="mBottom10">
                                       <a class="order_down_grn"
                                          aria-label="Move down"
                                          title="Move down"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="p_sUID_order_bottom" class="mBottom10">
                                       <a class="order_bottom_grn"
                                          aria-label="Move to bottom"
                                          title="Move to bottom"
                                          href="javascript:void(0)"></a>
                                    </div>
                                 </td>
                                 <td class="item_select">
                                    <div class="selectlist_area_grn">
                                       <span id="spinner_selectlist_p_sUID" style="display:none;position:absolute;z-index:1;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                       <div id="selectlist_base_selectlist_p_sUID" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn" tabindex="0" style="">
                                          <ul id="ul_selectlist_p_sUID">
                                             @if(isset($schedule))
                                                @foreach($schedule->seen as $key=>$val)
                                                <li
                                                   id="selectlist_p_sUID_member_user_{{$val->id}}"
                                                   class="selectlist_p_sUID"
                                                   data-value="{{$val->id}}"
                                                   data-type="user"
                                                   data-name="{{$val->full_name}}"
                                                   data-code="brown"                    data-id="{{$val->id}}"
                                                   data-group-path=""
                                                   data-url="">
                                                   <span class="@if($schedule->uid == $val->id )selectlist_user_login_grn @else selectlist_user_grn @endif" aria-label="Login user"
                                                      title="Login user"></span>
                                                   <span class="selectlist_text_grn">{{$val->full_name}}</span>
                                                </li>
                                                @endforeach
                                             @endif
                                          </ul>
                                          <span id="spinner_scroll_selectlist_p_sUID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                       </div>
                                    </div>
                                    <div class="textSub-grn mTop5"><a id="select_all_selectlist_p_sUID" class="mRight20" href="javascript:void(0);">{{trans('base.Select_all')}}</a><a id="un_select_all_selectlist_p_sUID" style="display:none" class="mRight20" href="javascript:void(0);">{{trans('base.Clear_all')}}</a></div>
                                 </td>
                              </tr>
                           </table>
                        </td>
                        <td class="vAlignTop-grn item_right_left">
                           <div class="buttonSelectMove-grn">
                              <div class="mBottom15">
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_add_p_sUID" style="padding-left:0;" href="javascript:void(0);">
                                 <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">{{trans('base.Add')}}</span>
                                 </a>
                                 </span>
                              </div>
                              <div>
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_rmv_p_sUID" style="padding-right:0;" href="javascript:void(0);">
                                 <span class="aButtonText-grn">{{trans('base.Remove')}}</span><span class="icon-buttonArrowRightBehind-grn"></span>
                                 </a>
                                 </span>
                              </div>
                           </div>
                        </td>
                        <td class="vAlignTop-grn">
                           <div class="mTop3 mBottom7 clearFix-cybozu">
                              <div class="search_navi">
                                 <div class="searchbox-grn">
                                    <div id="searchbox-cybozu-private_menu"
                                       class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                       <input class="input-text-cybozu prefix-grn" style="width:246px;" type="text"
                                          id="keyword_private_menu" name="" autocomplete="off"
                                          value="{{trans('base.User_search')}}"
                                          maxlength="">
                                       <button id="searchbox-submit-private_menu" class="searchbox-submit-cybozu" type="button"
                                          title="Search"
                                          aria-label="Search"></button>
                                    </div>
                                    <div class="clear_both_0px"></div>
                                 </div>
                              </div>
                           </div>
                           <!-- category select -->
                           <div class="mBottom7 nowrap-grn">
                              <dl id="p_CID_pulldown" class="selectmenu_grn selectmenu_base_grn">
                                 <dt>
                                    <a href="javascript:void(0)" class="nowrap-grn">
                                    <span>
                                    </span>
                                    <span class="selectlist_selectmenu_item_grn pulldown_head">
                                    </span>
                                    <span class="pulldownbutton_arrow_down_grn mLeft3"></span>
                                    </a>
                                 </dt>
                                 <dd>
                                    <div class="pulldown_menu_grn" style="display: none;">
                                       <ul>
                                          <li ><a href="javascript:void(0)"><span class="vAlignMiddle-grn">{{trans('base.All')}}</span></a></li>
                                          @foreach($departments as $key=>$val)
                                          <li data-value="{{$val->id}}" ><a href="javascript:void(0)" @if($val->level > 1) style='padding-left:{{($val->level -1) * 30}}px' @endif><span class="vAlignMiddle-grn">{{$val->name}} </span></a></li>
                                          @endforeach
                                       </ul>
                                    </div>
                                 </dd>
                              </dl>
                           </div>
                           <!-- category select -->
                           <div class="selectlist_area_grn">
                              <span id="spinner_selectlist_p_CID" style="display:none;position:absolute;z-index:1;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                              <div id="selectlist_base_selectlist_p_CID" aria-multiselectable="true" class="selectlist_grn selectlist_r_grn" tabindex="0" style="">
                                 <ul id="ul_selectlist_p_CID">
                                 </ul>
                                 <span id="spinner_scroll_selectlist_p_CID" style="display:none;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                              </div>
                           </div>
                           <div class="textSub-grn mTop5">
                              <a id="select_all_selectlist_p_CID" class="mRight20" href="javascript:void(0);">{{trans('base.Select_all')}}</a><a id="un_select_all_selectlist_p_CID" style="display:none" class="mRight20" href="javascript:void(0);">{{ trans('base.Clear_all') }}</a>    <script language="JavaScript" type="text/javascript">
                                 <!--
                                 var popupMember_p_CID_count = 0;
                                 function popupMember_p_CID(Aform)
                                 {
                                     if ( ! popupMember_p_CID_count || document.getElementById('form_CID[]') == null)
                                     {
                                         var p = document.getElementById('div_CID[]');
                                         var c = document.createElement('DIV');
                                         p.style.display = "";
                                         p.appendChild(c);
                                         c.innerHTML = '<iframe id="form_CID[]" name="form_CID[]" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';
                                     }

                                     popupWin("","user_list",1034,675,0,0,0,1,0,1);
                                     var url = '/grn/popup_user_list?';
                                     var html = '<!DOCTYPE html><html lang="en"><body><form method="post" target="user_list" action="' + url + '">';
                                     html += '<input type="hidden" name="system_display" value="0">';
                                     var form_frame = document.getElementById('form_CID[]').contentWindow.document;
                                     var instance = grn.component.member_select_list.get_instance("p_CID");
                                     var values = instance.getSelectedUsersValues();
                                     html += '<input type="hidden" name="cid" value="' + values.join(':') + '">';
                                     html += '</form></body></html>';
                                     form_frame.write( html );
                                     form_frame.close();
                                     form_frame.forms[0].submit();
                                     popupMember_p_CID_count = 1;
                                     if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                                     {
                                         update_back_step();
                                     }
                                     return true;
                                 }
                                 //-->
                              </script>
                              <div id="div_CID[]" style="display:none; position:absolute; width:0em; height:0em;">&nbsp;</div>
                              <a href="javascript:void(0);" onClick="popupMember_p_CID();"></a>
                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr valign="top">
               <th nowrap>{{trans('base.Facilities')}}</th>
               <td>
                  <script src="{!!asset('/js/facility_add.js')!!}" type="text/javascript"></script>
                  <script language="JavaScript" type="text/javascript">
                     <!--
                     jQuery(function () {
                         new grn.component.facility_add.FacilityAdd("facility_select", "schedule/add", "schedule/add", "sITEM", "cITEM",
                                 {
                                     "idSearchBox": "facilities",
                                     "searchUrl": '/api/search_facility',
                                     "categorySelectUrl": 'schedule/json/accessible_facility',
                                     "initUsingPurpose": "",
                                     "csrfTicket": "1153e7462753d2c3cb7aacab2f545d8e"
                                     });
                         });
                     //-->
                  </script>
                  <input type="hidden" name="selected_users_sITEM" value="">
                  <table class="table_plain_grn selectlist_base_grn">
                     <tr>
                        <td class="vAlignTop-grn" style="padding-left: 0;">
                           <table class="table_plain_grn">
                              <tr>
                                 <td class="buttonSlectOrder-grn">
                                    <div id="sITEM_order_up" class="mBottom10">
                                       <a class="order_up_grn"
                                          aria-label="Move up"
                                          title="Move up"
                                          href="javascript:void(0)"></a>
                                    </div>
                                    <div id="sITEM_order_down" class="mBottom10">
                                       <a class="order_down_grn"
                                          aria-label="Move down"
                                          title="Move down"
                                          href="javascript:void(0)"></a>
                                    </div>
                                 </td>
                                 <td class="item_select">
                                    <div class="selectlist_base_grn">
                                       <div id="selectlist_base_selectlist_sITEM" aria-multiselectable="true" class="selectlist_grn selectlist_l_grn focus_grn"
                                          style="height:151px;" tabindex="0">
                                          <span id="spinner_selectlist_sITEM"
                                             style="display:none;position:absolute;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                          <ul id="ul_selectlist_sITEM">
                                              @if(isset($schedule))
                                              @foreach($schedule->equipment as $key=>$val)
                                                <li id="selectlist_sITEM_member_facility_{{$val->id}}" class="selectlist_sITEM"
                                                   data-value="{{$val->id}}" data-approval="0" data-repeat="1" data-ancestors="" data-code="{{$val->name}}" data-name="{{$val->name}}">
                                                   <span class="selectlist_facility_grn"></span>
                                                   <span class="selectlist_text2_grn">{{$val->name}}</span>
                                                </li>
                                               @endforeach
                                              @endif
                                          </ul>
                                       </div>
                                    </div>
                                    <div class="textSub-grn mTop5">
                                       <a id="select_all_selectlist_sITEM" class="mRight20"
                                          href="javascript:void(0);">{{trans('base.Select_all')}}</a>
                                       <a id="un_select_all_selectlist_sITEM" style="display:none" class="mRight20"
                                          href="javascript:void(0);">{{trans('base.Clear_all')}}</a>
                                    </div>
                                 </td>
                              </tr>
                           </table>
                        </td>
                        <td class="vAlignTop-grn item_right_left">
                           <div class="buttonSelectMove-grn">
                              <div class="mBottom15">
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_add_cITEM" style="padding-left:0;" href="javascript:void(0);">
                                 <span class="icon-buttonArrowLeft-grn"></span><span class="aButtonText-grn">{{trans('base.Add')}}</span>
                                 </a>
                                 </span>
                              </div>
                              <div>
                                 <span class="aButtonStandard-grn">
                                 <a role="button" id="btn_rmv_cITEM" style="padding-right:0;" href="javascript:void(0);">
                                 <span class="aButtonText-grn">{{trans('base.Delete')}}</span><span class="icon-buttonArrowRightBehind-grn"></span>
                                 </a>
                                 </span>
                              </div>
                           </div>
                        </td>
                        <td class="vAlignTop-grn">
                           <div class="mTop3 mBottom7 clearFix-schedule">
                              <div class="search_navi">
                                 <div class="searchbox-grn">
                                    <div id="searchbox-cybozu-facilities"
                                       class="input-text-outer-cybozu searchbox-keyword-area searchbox_keyword_grn">
                                       <input class="input-text-cybozu prefix-grn" style="width:246px;" type="text"
                                          id="keyword_facilities" name="" autocomplete="off"
                                          value="{{trans('base.Facility_search')}}"
                                          maxlength="">
                                       <button id="searchbox-submit-facilities" class="searchbox-submit-cybozu" type="button"
                                          title="Search"
                                          aria-label="Search"></button>
                                    </div>
                                    <div class="clear_both_0px"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear_both_0px"></div>
                           <div class="mBottom7">
                              <link href="{{asset('assets/css/fag_tree.css')}}" rel="stylesheet" type="text/css">
                              <dl id="group-select" class="selectmenu_grn selectmenu_base_grn">
                                 <dt><a id="facility-button" href="javascript:void(0)" class="nowrap-grn"><span></span><span id="dropdown_title" class="selectlist_selectmenu_item_grn pulldown_head">{{trans('base.All_facilities')}}</span><span id="dropdown_arrow_image" class="pulldownbutton_arrow_down_grn mLeft3"></span><input type="hidden" name="pulldown_head" value="0"></a></dt>
                              </dl>
                              <div id="user-popup" class="wrap_dropdown_option"></div>
                              <div id="facility-popup" class="wrap_dropdown_option"></div>
                              <div id="dummy-popup" class="wrap_dropdown_option"></div>
                              <div></div>
                              <div id="facility-popup-dummy_root" style="border:1px solid #000000; top:-10000px; left:-10000px; position:absolute; background-color:#FFFFFF; overflow:scroll; width:1px;height:1px;">
                                 <div id="facility-popup-dummy_tree_wrap_tree1" class="wrap_tree1">
                                    <div id="facility-popup-dummy_tree_wrap_tree2" class="wrap_tree2">
                                       <div id="facility-popup-dummy_tree"></div>
                                    </div>
                                 </div>
                              </div>
                              <script type="text/javascript">
                                 (function () {

                                     var group_select_id    = 'group-select';
                                     var title_id           = 'dropdown_title';
                                     var user_button_id     = 'user-button';
                                     var facility_button_id = 'facility-button';
                                     var user_popup_id      = 'user-popup';
                                     var facility_popup_id  = 'facility-popup';

                                     var dropdown = new GRN_DropdownMenu(
                                         group_select_id, title_id, user_button_id, facility_button_id,
                                         GRN_DropdownMenu.prototype.PreferFacilityGroup,
                                         user_popup_id, facility_popup_id,
                                         clickOrganizationCallback, clickFacilityGroupCallback,
                                         "" );

                                     function updateTitle( title ) {
                                         var group_select = jQuery("#" + group_select_id);
                                         var old_width = parseInt(group_select.outerWidth());
                                         group_select.css("width", "");
                                         jQuery("#" + title_id).html( title );
                                         if( old_width > parseInt(group_select.outerWidth()) ) {
                                             group_select.css("width", old_width + "px");
                                         }
                                     }

                                     function clickOrganizationCallback( group_item ) {
                                         return function(){
                                             updateTitle( group_item.name );
                                             dropdown.organization.hide();
                                             location.href = "/index?"+ '&bdate=' + document.forms["schedule/add"].bdate.value + '&gid='+group_item.gid;
                                         }
                                     }

                                     function clickFacilityGroupCallback( node ) {
                                         node.tree.fagTree.removeSpecialNode( 's' );
                                         if( node.extra_param ) { //よく使う施設グループ or 最近選択した施設グループ
                                             updateTitle( node.label );
                                         }
                                         else {
                                             if( node.oid == 'f' ) {
                                                 updateTitle( '(All facilities)' );
                                             }else{
                                                 updateTitle(  node.label );
                                             }
                                         }
                                         dropdown.facility.hide();

                                         var oid = node.oid;
                                         if( oid.substr(0, 1) == 'x' ) {
                                             oid = oid.substr( 1 );
                                         }
                                         grn.base.namespace("grn.component.facility_add");
                                         if (typeof grn.component.facility_add.get_instance === "function") {
                                             var facility_select = grn.component.facility_add.get_instance("facility_select");
                                             if (facility_select) {
                                                 var facility_group_id = oid ? oid : 0;
                                                 facility_select.changeCategory(facility_group_id);
                                                 return;
                                             }
                                         }
                                         ChangeITEM( "schedule/add", {'fagid': oid } );
                                     }

                                     dropdown.initializeOrganization(
                                         new Array(

                                                  ) );

                                     var icon_size = 37;
                                     var min_width = 200;

                                     var group_select = jQuery("#" + group_select_id);
                                     var current_width = group_select.outerWidth();

                                     if ( current_width  < min_width ){
                                         group_select.css( "width", (min_width + icon_size) + "px" );
                                     }

                                     var title_width  = group_select.outerWidth();

                                     dropdown.initializeFacilityGroup( { 'page_name': "schedule/add",
                                                                         'ajax_path':'/schedule/json/accessible_facility_tree?',
                                                                         'csrf_ticket':'1153e7462753d2c3cb7aacab2f545d8e',
                                                                         'callback':clickFacilityGroupCallback,
                                                                         'selectedOID':"x",
                                                                         'title_width': title_width,
                                                                         'node_info':
                                                                         [{!!$equipment!!}]
                                                                       });

                                     YAHOO.namespace("global");
                                     YAHOO.global.dropdown = dropdown;
                                     YAHOO.global.updateTitle = updateTitle;
                                 }());

                              </script>
                           </div>
                           <div class="mBottom5">
                              <div class="selectlist_base_grn">
                                 <div id="selectlist_base_selectlist_cITEM" aria-multiselectable="true" class="selectlist_grn selectlist_r_grn"
                                    style="height:90px;" tabindex="0">
                                    <span id="spinner_selectlist_cITEM"
                                       style="display:none;position:absolute;"><img src="{{asset('/img/spinner.gif')}}" border="0" alt=""></span>
                                    <ul id="ul_selectlist_cITEM">
                                       {!!$equipment_html!!}
                                    </ul>
                                 </div>
                              </div>
                              <div class="textSub-grn mTop5">
                                 <a id="select_all_selectlist_cITEM" class="mRight20"
                                    href="javascript:void(0);">{{trans('base.Select_all')}}</a>
                                 <a id="un_select_all_selectlist_cITEM" style="display:none" class="mRight20"
                                    href="javascript:void(0);">{{trans('base.Clear_all')}}</a>
                                 <script language="JavaScript" type="text/javascript">
                                    <!--
                                    var popupFacilityList_count = 0;
                                    function popupFacilityList(Aform)
                                    {
                                     if ( ! popupFacilityList_count)
                                     {

                                       var p = document.getElementById('div_cITEM');
                                       var c = document.createElement('DIV');
                                       p.style.display = "";
                                       p.appendChild(c);
                                       c.innerHTML = '<iframe id="form_cITEM" name="form_cITEM" frameborder="no" scrolling="no" style="position:absolute; width:0em; height:0em;" src=""></iframe>';

                                     }

                                        popupWin("","facility_list",1034,675,0,0,0,1,0,1);

                                     var form_frame = window.frames['form_cITEM'].document;
                                     var url = '/schedule/popup_facility_list?';
                                     var html = '<!DOCTYPE html><html><body lang="{$html_tag_lang|escape}"><form method="post" target="facility_list" action="' + url + '">';
                                     html += '<input type="hidden" name="system_display" value="0">';


                                        var instance = grn.component.member_select_list.get_instance("cITEM");
                                        var values = instance.getSelectedUsersValues();
                                        html += jQuery.map(values, function(value, count){
                                            if ( value.length > 0 && isFinite( value ) ) {
                                                return '<input type="hidden" name="cid[' + (count) + ']" value="' + value + '">'
                                            }else{
                                                return '';
                                            }
                                        }).join();


                                     html += '</form></body></html>';
                                     form_frame.write( html );
                                     form_frame.close();
                                     form_frame.forms[0].submit();
                                     popupfacilityList_count = 1;
                                     if( !grn.browser.chrome && !grn.browser.safari && typeof update_back_step == 'function' )
                                     {
                                        update_back_step();
                                     }
                                     return true;
                                    }
                                    //-->
                                 </script>
                                 <div id="div_cITEM" style="display:none; position:absolute; width:0; height:0;">&nbsp;</div>
                              </div>
                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr>
               <th nowrap>{{trans('base.Availability')}}</th>
               <td>
                  
                  <div id="form_frame" style="display:none; position:absolute; width:0em; height:0em;">&nbsp;</div>
                  <script language="JavaScript" type="text/javascript">
                     var form_name = document.getElementById('schedule/add');

                  </script>
                  <span class="aButtonHighlight-grn">
                  <a href="javascript:void(0);" onClick="javascript:openConfirm(form_name, 'add');">
                  <span class="icon-blankB-grn"></span>
                  <span class="aButtonText-grn">{{trans('base.Check_availability_of_attendees_and_facilities')}}</span>
                  </a>
                  </span>
                  <script src="{!!asset('/js/adjust_link.js')!!}" type="text/javascript"></script>
               </td>
            </tr>
            <tr valign="top">
               <th>{{trans('base.Company_information')}}</th>
               <td>
                  <a href="javascript:display_on_off('address_form')">{{trans('base.Add_company_information')}}▼</a>
                  <table class="address" id="address_form" style="display:none;">
                     <colgroup>
                        <col width="5%">
                        <col width="95%">
                     </colgroup>
                     <tr valign="top">
                        <th nowrap>{{trans('base.Company_name')}}</th>
                        <td><input  type="text" name="company_name" size="52" maxlength="100" onKeyPress="return event.keyCode != 13;"></td>
                     </tr>
                     <tr valign="top">
                        <th nowrap>{{trans('base.Address')}}</th>
                        <td><input  type="text" name="physical_address" size="52" maxlength="65535" onKeyPress="return event.keyCode != 13;"></td>
                     </tr>
                     <tr valign="top">
                        <th nowrap>{{trans('base.Company_phone_number')}}</th>
                        <td><input  type="text" name="company_telephone_number" size="50" maxlength="100" onKeyPress="return event.keyCode != 13;"></td>
                     </tr>
                  </table>
               </td>
            </tr>
            <tr valign="top">
               <th nowrap>{{trans('base.Notes')}}</th>
               <td><textarea id="textarea_id" name="memo" class="autoexpand" wrap="virtual" role="form" cols="65" rows="5" style="border: 1px solid #999;white-space: pre-wrap; min-height: 95px; height: 121px;">{{isset($schedule)? $schedule->memo : ''}}</textarea></td>
            </tr>
            <tr valign="top">
               <th nowrap>{{trans('base.Attachments')}}</th>
               <td>
                  <div id="html5_content">
                     <div style="margin-top:3px;">
                        <div id="drop_" class="drop">
                           Drop files here.
                        </div>
                        <div class="file_input_div">
                           {{trans('base.Attach_files')}}
                           <input type="file" class="file_html5 file_input_hidden" name="file[]" size="40" id="file_upload" multiple style='display:inline-block;'>
                        </div>
                     </div>
                     <div class="clear_both_0px"></div>
                     <div id="upload_message" >
                     </div>
                     <table id="upload_table" class="attachment_list_base_grn">
                        <tbody id='list_file_upload'>
                           <tr>
                              <td></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="clear_both_0px"></div>
               </td>
            </tr>
            <tr>
               <td class="form_category_title_td_grn" colspan="2">
                  <div class="form_category_title_grn">{{trans('base.Advanced_settings')}}</div>
               </td>
            </tr>
            <tr>
               <th>{{trans('base.Visibility')}}</th>
               <td class="d-flex">
                    <span class="radiobutton_base_grn d-flex align-items-center">
                        <input type="radio" name="private" id="1" value="0" onclick="display_off('private_select')" checked >
                        <label class="mb-0" for="1" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">{{trans('base.Public')}}</label>
                    </span>
                    <span class="radiobutton_base_grn d-flex align-items-center">
                        <input type="radio" name="private" id="2" value="1" onclick="display_off('private_select')" >
                        <label class="mb-0" for="2" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">{{trans('base.Private')}}</label>
                    </span>
               </td>
            </tr>
            <tr>
                <th>Lịch công ty</th>
                <td>
                    <span class="radiobutton_base_grn d-flex align-items-center">
                        <input type="checkbox" id="pd" name="public_dashboard" value="1">
                        <label class="mb-0" for="pd" onMouseOver="this.style.color='#ff0000'" onMouseOut="this.style.color=''">Hiển thị</label>
                    </span>
                </td>
            </tr>
            <tr>
               <td></td>
               <td>
                  <script src="{!!asset('/js/add.js')!!}" type="text/javascript"></script>
                  <script language="JavaScript" type="text/javascript">
                     var check_add = false;
                     var browser_type = "chrome";
                     var browser_ver_major = "87";
                     var allow_file_attachment = true;
                     var is_ios = false;
                     var is_android = false;
                     var handle_type = '';
                     function _submitNormal(f,ajax_page)
                     {
                         url_redirect = '/schedule/view?';
                         grn.component.button("#schedule_submit_button").showSpinner();
                         grn.component.button("#schedule_submit_button_top").showSpinner();

                         var request = new grn.component.ajax.request({
                                     url: ajax_page,
                                     dataType: "json",
                                     data: jQuery(f).serialize(),
                                     method: "post"
                                 }
                         );

                         request.on('beforeShowError', function (event, jqXHR) {
                             if (typeof jqXHR.responseJSON !== "undefined") {
                                 var json_obj = jqXHR.responseJSON;
                                 if (typeof json_obj.validation !== "undefined" && JSON.parse(json_obj.validation) == false) {
                                     event.preventDefault();
                                     jQuery('#show_error').css({display: ''});
                                     window.scrollTo(0, 0);
                                 }
                             }
                         });

                         request.send()
                                 .done(function (data, textStatus, jqXHR) {
                                     // remove ajax flag element
                                     removeAjaxElement(f);
                                     // remove refresh status dialog flag element
                                     removeShowRefreshDialogFlag(f);

                                     var json_obj = grn_parseJson(jqXHR.responseText);
                                     if (json_obj.conflict_facility == 1) {
                                         showYN(jqXHR, function (except_date) {
                                             //my_callback
                                             (function ($) {

                                                 jQuery("#hfExcept").val(except_date);
                                                 check_add = false;
                                                 if (grn.base.isNamespaceDefined("grn.page.schedule.add")) {
                                                     grn.page.schedule.add.schedule_submit(form_name, ajax_page);
                                                 }
                                             })(jQuery);
                                         });
                                     } else {

                                         var link = json_obj.link;
                                         window.location = link;

                                     }
                                 })
                                 .fail(function () {
                                     // remove ajax flag element
                                     removeAjaxElement(f);
                                     // remove refresh status dialog flag element
                                     removeShowRefreshDialogFlag(f);

                                     grn.component.button("#schedule_submit_button").hideSpinner();
                                     grn.component.button("#schedule_submit_button_top").hideSpinner();
                                     check_add = false;
                                 });
                     }

                     function _submitUpload(f,ajax_page)
                     {
                         url_redirect = '/schedule/view?';
                         grn.component.button("#schedule_submit_button").showSpinner();
                         grn.component.button("#schedule_submit_button_top").showSpinner();
                         var request = new grn.component.ajax.request({
                             url: ajax_page,
                             dataType: "json",
                             data: jQuery(f).serialize(),
                             method: "post"
                         });

                         request.send()
                                 .done( function(data, textStatus, jqXHR){
                                 removeAjaxElement(f);
                                 // remove refresh status dialog flag element

                                 removeShowRefreshDialogFlag(f);

                                 if(typeof grn_parseJson(jqXHR.responseText) === "object")
                                 {
                                     var json_obj = grn_parseJson(jqXHR.responseText);
                                     if(json_obj.code)
                                     {
                                         grn.component.button("#schedule_submit_button").hideSpinner();
                                         grn.component.button("#schedule_submit_button_top").hideSpinner();
                                         if(!json_obj.validation)
                                         {
                                             grn.component.error_handler.show(jqXHR);
                                         }
                                         else
                                         {
                                             jQuery('#show_error').css({display:''});
                                             window.scrollTo(0,0);
                                         }
                                         check_add = false;
                                     }
                                     else
                                     {
                                         if (json_obj.conflict_facility == 1) {
                                             showYN(jqXHR, function (except_date) {
                                                 //my_callback
                                                 (function ($) {

                                                     jQuery("#hfExcept").val(except_date);
                                                     check_add = false;
                                                     if (grn.base.isNamespaceDefined("grn.page.schedule.add")) {
                                                         grn.page.schedule.add.schedule_submit(form_name, ajax_page);
                                                     }
                                                 })(jQuery);
                                             });
                                         } else {

                                             var link = json_obj.link;
                                             window.location = link;

                                         }
                                     }
                                 }
                                 else
                                 {
                                     setAjaxElement(f);
                                     setShowRefreshDialogFlag(f);
                                     _submitNormal(f,ajax_page);
                                 }
                             })
                             .fail(function() {
                                 // remove ajax flag element
                                 removeAjaxElement(f);
                                 // remove refresh status dialog flag element
                                 removeShowRefreshDialogFlag(f);

                                 grn.component.button("#schedule_submit_button").hideSpinner();
                                 grn.component.button("#schedule_submit_button_top").hideSpinner();
                                 check_add = false;
                             });
                     }

                     /**
                      * This function is called only from schedule/add.js.
                      *
                      * @param {HTMLElement} f
                      * @param {string} ajax_page
                      *
                      */
                     function _submit(f,ajax_page)
                     {
                         if (((browser_type == 'msie' && browser_ver_major < 10) || is_ios || is_android) && allow_file_attachment)
                         {
                             _submitNormal(f,ajax_page);
                         }
                         else
                         {
                            _submitUpload(f,ajax_page);

                         }
                     }

                     function showYN(request, my_callback) {
                         var s = request.responseText;
                         if (typeof s != 'undefined') {
                             var ob_json = grn_parseJson(s);
                             var title;
                             var msg;
                             var html_rows = "";
                             var html_row_header = "";
                             var rows_length = 0;
                             var rows_limit = 0;
                             var html_row_more = "";
                             var is_show_more_text = false;
                             var event_except = "";
                             (function ($) {
                                 var events = ob_json.conflict_events;
                                 if (events != null && events.length > 0) {
                                     rows_length = events.length;
                                     rows_limit = events.length;
                                     if (events.length > 5) {//Limit 5 events are displayed.
                                         rows_limit = 5;
                                         is_show_more_text = true;
                                     }

                                     html_row_header = "<tr><th class='nowrap-grn' style='width:30px;'>Date</th><th class='nowrap-grn'>Conflicting facility </th></tr>";
                                     for (var i = 0; i < rows_limit; i++) {
                                         html_rows += "<tr><td class='nowrap-grn'><span class='icon_list_style_grn icon_inline_grn'></span><span>" + events[i].setdatetime + "</span></td><td><span>" + events[i].col_facility + "</span></td></tr>";
                                         event_except += ";" + events[i].col_setdatetime;
                                     }
                                     for (var j = rows_limit; j < events.length; j++) {
                                         event_except += ";" + events[j].col_setdatetime;
                                     }
                                 }
                             })(jQuery);

                             if (is_show_more_text == true) {
                                 html_row_more = "<div class='mTop7'>...&nbsp;" +
                                 "" +
                                 (rows_length - rows_limit) +
                                 "&nbsp;more appointments with conflicting facilities exist.</div>";
                             }
                             msg = "<div class='mBottom10'><span class='icon_attention_grn attentionMessage-grn bold_grn'>One or more facilities are conflicting in the following appointments:</span></div>";
                             msg += "<table class='table_grn table_list_1_grn'>" + html_row_header + html_rows + "</table>" + html_row_more;
                             msg += "<div class='border-partition-common-dot-grn'></div>";
                             if (ob_json.conflict_all == 1) {// all event are conflict
                                 msg += "<div class='bold_grn mTop15'>All appointments have conflicting facilities. No appointment can be added.</div>";
                             }
                             else {
                                 if (handle_type == '' || handle_type == 'add') // event is added
                                 {
                                     msg += "<div class='bold_grn mTop15'>Do you want to add only appointments with no conflicting facilities?</div>";
                                 }
                                 else    // modify
                                 {
                                     msg += "<div class='bold_grn mTop15'>Do you want to edit only appointments with no conflicting facilities?<br />Appointments with conflicting facilities are deleted, not edited.</div>";
                                 }
                             }

                             var msgboxButtonType = (ob_json.conflict_all) ? GRN_MsgBoxButtons.ok : GRN_MsgBoxButtons.yesno;

                             GRN_MsgBox.show(msg, title, msgboxButtonType, {
                                 ui: [],
                                 caption: {
                                     yes: 'Yes',
                                     no: 'No',
                                     ok: 'Back'
                                 },
                                 main_button: {ui: grn.component.button.UI.MAIN, autoDisable: true},
                                 callback: function (result, form) {
                                     check_add = false;
                                     grn.component.button("#schedule_submit_button").hideSpinner();
                                     grn.component.button("#schedule_submit_button_top").hideSpinner();
                                     if (result == GRN_MsgBoxResult.yes && typeof my_callback != 'undefined') {
                                         my_callback(event_except);
                                     } else {
                                         jQuery("#hfExcept").empty();
                                         if (result == GRN_MsgBoxResult.ok) {
                                             GRN_MsgBox.close();
                                         }
                                     }
                                 }
                             });
                         }
                     }

                     function toggle(id)
                     {
                         var loading = document.getElementById(id + '_loading');
                         var button = document.getElementById(id + '_button');

                         if (!loading)
                         {
                             loading.innerHTML = '';
                             button.disabled = false;
                             isLoading = false;
                         }
                         else
                         {
                             loading.innerHTML = '<img src="' + G.spinnerImage + '" />';
                             button.disabled = true;
                             isLoading = true;
                         }
                     }

                     function setAjaxElement(f)
                     {
                         if( document.getElementById('use_ajax') ) return false;

                         var element = document.createElement("input");
                         element.setAttribute("type", "hidden");
                         element.setAttribute("id", "use_ajax");
                         element.setAttribute("name", "use_ajax");
                         element.setAttribute("value", "1");
                         f.appendChild(element);
                         return true;
                     }

                     function removeAjaxElement(f)
                     {
                         var use_ajax = document.getElementById('use_ajax');
                         if(use_ajax)
                         {
                             f.removeChild(use_ajax);
                         }
                     }

                     function setShowRefreshDialogFlag(f)
                     {
                         if (jQuery("#attendance_check_dialog").is(":visible"))
                         {
                             if( document.getElementById('show_refresh_status_dialog') ) return;

                             var element = document.createElement("input");
                             element.setAttribute("type", "hidden");
                             element.setAttribute("id", "show_refresh_status_dialog");
                             element.setAttribute("name", "show_refresh_status_dialog");
                             element.setAttribute("value", "1");
                             f.appendChild(element);
                         }
                     }

                     function removeShowRefreshDialogFlag(f)
                     {
                         var show_refresh_status_dialog = document.getElementById('show_refresh_status_dialog');
                         if(show_refresh_status_dialog)
                         {
                             f.removeChild(show_refresh_status_dialog);
                         }
                     }
                     //-->
                  </script>
                  <div class="mTop10 buttonArea-grn">
                     <script>
                        function update_back_step()
                        {
                            grn.page.schedule.add.update_back_step();
                        }
                     </script>
                     <span id="schedule_submit_button" class="button_grn_js button1_main_grn  button1_r_margin2_grn" onclick="if (!grn.component.button(this).isDisabled()) grn.page.schedule.add.schedule_submit('schedule/add', '/schedule/store');"  >
                        <a href="javascript:void(0);" role="button">{{trans('base.Add')}}</a></span>
                            <span id="schedule_button_cancel" class="button_grn_js button1_normal_grn" onclick="grn.page.schedule.add.schedule_cancel('back');return false;"  >
                        <a href="javascript:void(0);" role="button">{{trans('base.Cancel')}}</a>
                     </span>
                  </div>
               </td>
            </tr>
         </table>
      </form>
      <script type="text/javascript" src="{{asset('/js/timezone_info.js')}}"></script>
      <script type="text/javascript" src="{{asset('/js/schedule_add.js')}}"></script>
   </div>
</div>
@stop
@section('script')
@parent
<script src="{{asset('/js/dist/schedule.js')}}" type="text/javascript"></script>
<script>
     $(document).on('change', '#file_upload', function () {
          var file_data = $(this).prop('files');
          var form_data = new FormData();
          for(let i=0;i<file_data.length;i++){
            form_data.append('file[]', file_data[i]);
          }
          form_data.append('type', {{\App\File::TYPE_SCHEDULE}});
          $.ajax({
                url: '/api/upload',
                type: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == 'true'){
                        if(response.success == 'true'){
                            response.data.forEach(element =>
                            $('#list_file_upload').append(`<tr>
                                                                <td>
                                                                    <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                </td>
                                                                <td>`+element.name+` (`+element.size+` Mb)</td>
                                                            </tr>`)
                            )
                        }
                    }
                }
           });
     })
</script>
<script language="JavaScript" type="text/javascript">
   jQuery(function () {
       new grn.js.page.schedule.Add();
   });
</script>
@stop
