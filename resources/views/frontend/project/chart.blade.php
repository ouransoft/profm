<div style="display: inline-flex;width:100%">
    <div style="width: 33%;display:block;">
        <canvas id="income" width="300" height="400" style="margin: 0 auto"></canvas>
        <p style="text-align: center;margin-bottom: 40px">Biểu đồ thống kê đề án theo tháng</p>
    </div> 
    <div style="width: 33%;display:block;text-align: center;">
        <canvas id="countries" width="300" height="400" style="margin: 0 auto"></canvas>
        <p style="text-align: center;margin-bottom: 40px">Biểu đồ thống kê đề án theo trạng thái</p>
    </div>
    <div style="width: 33%;display:block;text-align: center;">
        <canvas id="status_chart" width="300" height="400" style="margin: 0 auto"></canvas>
        <p style="text-align: center;margin-bottom: 40px">Biểu đồ thống kê đề án theo cấp độ</p>
    </div>
</div>
<script>
    var labels1=[]; 
    var datas1 =[];
    var labels2=[]; 
    var datas2 =[];
    @foreach($label1 as $val)
        labels1.push('{!!$val!!}');
    @endforeach
    @foreach($data1 as $key=>$val1)
        datas1.push('{!!$val1!!}');
    @endforeach
    var labels2=[]; 
    var datas2=[];
    @foreach($label2 as $val)
        labels2.push('{!!$val!!}');
    @endforeach
    @foreach($data2 as $key=>$val2)
        datas2.push('{!!$val2!!}');
    @endforeach
    var barData = {
        labels : labels1,
        datasets : [
            {   
                label: 'Đề án',
                fillColor : "#48A497",
                strokeColor : "#48A4D1",
                data : datas1,
                backgroundColor:'#48A4D1'
            },
        ]
    }
    var income = document.getElementById("income").getContext("2d");
    var myBarChart = new Chart(income, {
        type: 'bar',
        data: barData,
        options: { 
            responsive: false,
            maintainAspectRatio: false,
        }
        });
    
    var barData1 = {
        labels : labels2,
        datasets : [
            {
                label: 'Đề án',
                fillColor : "#48A497",
                strokeColor : "#48A4D1",
                data : datas2,
                backgroundColor:'#48A4D1'
            },
        ]
    }
    var income1 = document.getElementById("status_chart").getContext("2d");
    var myBarChart = new Chart(income1, {
        type: 'bar',
        data: barData1,
        options: { 
            responsive: false,
            maintainAspectRatio: false,
        }
    });
    var pieData =  {
        labels: [
            'Chờ duyệt',
            'Trả về',
            'Đã duyệt'
        ],
        datasets:[{
                data: [{{$status_pending}}, {{$status_return}}, {{$status_approved}}],
                backgroundColor:['#3a4048','#da2f69','#26d847']
            }
            
        ]
    }

    // pie chart options
    var pieOptions = {
        segmentShowStroke : true,
        animateScale : false,
        legend: {
            display: true,
            labels: {
                fontColor: '#333',
                render: 'label'
            }
        },
        responsive: false,
        maintainAspectRatio: false,
    }
    var countries= document.getElementById("countries").getContext("2d");
    countries.width = 400;
    countries.height = 400;
    var myPieChart = new Chart(countries, {
        type: 'pie',
        data: pieData,
        options: pieOptions,
    });
    
</script>

