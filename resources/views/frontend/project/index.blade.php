@extends('frontend.layouts.project')
@section('content')
<style>
    .project-content {
        background: none;
    }
    .project-content .content {
        margin: 0 10px;
        padding:0px;
    }
    .project-content .card {
        margin-top: 3px;
    }
    .project-content .content {
        box-shadow: rgb(0 0 0 / 25%) 0px 0.0625em 0.0625em, rgb(0 0 0 / 25%) 0px 0.125em 0.5em, rgb(255 255 255 / 10%) 0px 0px 0px 1px inset;
    }
</style>
<div class="project-content" id="pjax-container">
    <div class="content">
        <div class="top-sidebar bg-white">
            <div class="top-left-sidebar" style="width:85%;float:left">
                <ul style="padding-left:13px;">
                    <li><input class='checkbox-action-project' type="checkbox" id='check_all' value='all'></li>
                    <li><a href="javascript:void(0)" class='export-member-project' title="Xuất file excel"><img  src="{!!asset('assets2/img/excel.png')!!}"></a></li>
                    <li><a href="javascript:void(0)" class="save-project" title="Lưu lại"><img  src="{!!asset('assets2/img/save (2).png')!!}"></a></li>
                    @if(!$keyword)
                    <li><a href="javascript:void(0)" class="remove-project" title='Xóa'><img  src="{!!asset('assets2/img/trash (2).png')!!}"></a></li>
                    @endif
                    @if($keyword =='draft')
                    <li><a href="submit-project" title='Gửi'><img src="{!!asset('assets2/img/submit.png')!!}"></a></li>
                    @endif
                </ul>
            </div>
            <div class="top-right-sidebar">
                @if(count($records) > 0)
                <div style="display:flex" id="show_page_project">
                    <p style="padding-top:15px;padding-right:10px">{{$records->firstItem()}}-{!!$records->lastItem()!!} {{trans('base.among')}} {!!$records->total()!!}</p>
                    <a  href="{{$records->previousPageUrl()}}" class='forward-page-project' @if($records->currentPage() == 1) disabled="disabled" @endif><img src="{!!asset('assets2/img/left-arrow.png')!!}"></a>
                    <a  href="{{$records->nextPageUrl()}}" class='next-page-project' @if($records->currentPage() == $records->lastPage()) disabled="disabled" @endif><img src="{!!asset('assets2/img/forward.png')!!}"></a>
                </div>
                @endif
            </div>
        </div>
        <div style="overflow-y: scroll;width:100%;height:825px;padding:0px">
            <table class="table table-content" style="width:100%;margin-top:0px">
                <tbody id="records-project">
                    @foreach($records as $key=>$record)
                    <tr class='project-detail'>
                        <td colspan="1" class="td-checkbox pl-5"><input style="width:16px;height:20px;" type="checkbox" value="{{$record->id}}" name="project_id" class='check'></td>
                        <td class='inhert' colspan="1" class="link-project" data-href="@if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0) {!!route('frontend.project.edit',$record->id)!!} @else {!!route('frontend.project.view',$record->id)!!} @endif">
                            {!!$record->getStatus!!}
                        </td>
                        <td colspan="5" class="link-project" data-href="@if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0) {!!route('frontend.project.edit',$record->id)!!} @else {!!route('frontend.project.view',$record->id)!!} @endif"><a @if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0) href="{!!route('frontend.project.edit',$record->id)!!}" @else href="{!!route('frontend.project.view',$record->id)!!}" @endif><span class="font-weight-normal">{{$record->name}}</span></a></td>
                        <td style="width:130px;font-size: 14px;" class="link-project" data-href="@if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0) {!!route('frontend.project.edit',$record->id)!!} @else {!!route('frontend.project.view',$record->id)!!} @endif"><span>{{date('d',strtotime($record->created_at))}} tháng {{date('m',strtotime($record->created_at))}}</span></td>                               
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/project.js') !!}"></script>
<script>
    $('.link-project').click(function(){
        window.location.href = $(this).attr('data-href');
    })
</script>
@stop