<div class="sidebar-project shadow">
    <div class="button-create">
        <a href="{{route('frontend.project.create')}}" class="add-project"><i class="fas fa-plus"></i> {{trans('base.NewPJ')}}</a>
    </div>
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical" style='position: relative;border-top:1px solid #dcdcdc;padding-top: 10px;'>
        <a class="nav-link load-page  @if(Route::currentRouteName() == 'frontend.project.list' && !isset($_GET['keyword_project'])) active @endif" href="{!!route('frontend.project.list')!!}"><span class="icons icons-list-dark"></span>Danh sách đề án<span>{!!\App\Project::where('is_deleted',0)->count()!!}</span></a>
        @if(in_array(\Auth::guard('member')->user()->level,[\App\Member::LEVEL_D1,\App\Member::LEVEL_D2]))
        <a class="nav-link load-page  @if(Route::currentRouteName() == 'frontend.project.list' && isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'pending') active @endif" href="{!!route('frontend.project.list',['keyword_project'=>'pending'])!!}"><span class="icons icons-approve-dark"></span>Đề án cần phê duyệt</a>
        @endif
        @if(\Auth::guard('member')->user()->level == \App\Member::LEVEL_TK)
        <a class="nav-link load-page  @if(Route::currentRouteName() == 'frontend.project.list' && isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'consider') active @endif" href="{!!route('frontend.project.list',['keyword_project'=>'consider'])!!}"><span class="icons icons-approve-dark"></span>Xem xét cấp độ <span>{!!\App\Project::where('is_deleted',0)->where('consider',1)->count()!!}</span></a>
        @endif
        <a class="nav-link load-page  @if(Route::currentRouteName() == 'frontend.project.index' && !isset($_GET['keyword'])) active @endif" href="{!!route('frontend.project.index')!!}"><span class="icons icons-user-dark"></span>Đề án cá nhân<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'save') active @endif" href="{!!route('frontend.project.index',['keyword'=>'save'])!!}"><span class="icons icons-save-dark"></span>{{trans('base.Save')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_saved',1)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'send') active @endif" href="{!!route('frontend.project.index',['keyword'=>'send'])!!}"><span class="icons icons-send-dark"></span>{{trans('base.Send')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status','>',0)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'draft') active @endif" href="{!!route('frontend.project.index',['keyword'=>'draft'])!!}"><span class="icons icons-draft-dark"></span>{{trans('base.Draft')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status',0)->where('is_deleted',0)->count()!!}</span></a>
        <a class="nav-link load-page chart-list" data-href="{{route('frontend.project.chart')}}" href="javascript:void(0)"><span class="icons icons-chart-dark"></span>Biểu đồ thống kê</a>
        <a class="nav-link load-page @if(isset($_GET['keyword']) && $_GET['keyword'] == 'remove') active @endif" href="{!!route('frontend.project.index',['keyword'=>'remove'])!!}"><span class="icons icons-delete-dark"></span>{{trans('base.Trash')}}<span>{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',1)->count()!!}</span></a>
    </div>
    <div class="text-sidebar">
        <h6><span class="icons icons-medal"></span>{{trans('base.Your_ranking')}}</h6>
        <ul class="first-ul">
            <li><span class="icons icons-user" style="position:relative"></span>{{trans('base.Personal')}} ( @if($position_rank != 0){{$position_rank}} @else Chưa có đề án @endif)</li>
            <li><span class="icons icons-user-group" style="position:relative"></span>{{trans('base.Group')}} ( @if($position_team_rank != 0){{$position_team_rank}} @else Chưa có đề án @endif)</li>
        </ul>
    </div>
    <div class="text-sidebar">
        <h6><span class="orange">TOP TEAMS</span> of Month</h6>
        <ul style="position:relative">
            @foreach($rank_team as $key=>$val)
            <li>{{$key + 1}}. Tem {{$val->name}} <span class="float-right mr-3">{{$val->count}} {{trans('base.project')}}</span></li>
            @endforeach
        </ul>
    </div>
    <div class="text-sidebar">
        <h6><span class="orange">TOP USER</span> of Quarter </h6>
        <ul style="position:relative">
            @foreach($rank_quarter as $key=>$val)
            <li>{{$key + 1}}. <span class="full-name-member">{{\App\Member::find($val->id)->full_name}}</span> <span class="float-right mr-3">{{$val->count}} {{trans('base.project')}}</span></li>
            @endforeach
        </ul>
    </div>
</div>