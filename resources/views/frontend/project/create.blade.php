@extends('frontend.layouts.project')
@section('content')
<div class="project-content">
    <div class="content project-content">
        <div class='title-project'>
            <h4 style="margin-top:25px">{{trans('base.IMPORT_NEW_IMPROVED_PROJECTS_INFORMATION')}}</h4>
            <a class='helper-question' href="javascript:void(0)" style="color:#333;position: absolute;right: 16px;top: 32px; font-size: 22px;}"><img src='{{asset('/img/information.png')}}' style='width: 27px;margin-bottom: 6px;margin-right: 8px;'>{{trans('base.FAQ')}}</a>
        </div>
        <div class="form-create">
            <form method="POST" action="{{route('frontend.project.store')}}" id="FrmCreateProject">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <div class="row" style="margin-bottom: 30px;">
                    <div class="notification-member col-md-6">
                        <div class="row" id='info_member' style="border: 1px solid;margin-left: 0px;border-radius: 4px;">
                            <div class="avatar-member">
                                <div class="img-member">
                                    <img src="@if(is_null(\Auth::guard('member')->user()->file())){!!asset('assets2/img/man.png')!!} @else /{!!\Auth::guard('member')->user()->file()->link!!} @endif" style="width: 100%;">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <h3 class="mb-0">{!!\Auth::guard('member')->user()->full_name!!}</h3>
                                <p>{{trans('base.Employee_code')}}: <span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                <p>{{trans('base.Position')}}: <span>@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif</span></p>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input mt-1" id="check_member" style="width:22px;height:22px;">
                            <label class="form-check-label" for="check_member" style="margin: 4px 0px 0px 14px;">{{trans('base.Where_to_enter_for_someone_else')}}</label>
                        </div>
                        <div class="form-group row" style="margin-top: 25px;">
                            <label for="inputPassword" class="pl-3 col-form-label">{{trans('base.Select_code_employee')}}</label>
                            <div class="col-sm-6 mt-1">
                                <select class="form-control select-search choose-member" name="member_id" data-placeholder="{{trans('base.Select_code_employee')}}" disabled>
                                    {!!$member_html!!}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-md-12 col-form-label pl-3">{{trans('base.SUBJECT_NAME')}}</label>
                    <div class="col-sm-12">
                        <input class="form-control" name="name" type="text" required="" id="subject_name"> 
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">{{trans('base.BEFORE')}}</span> {{trans('base.IMPROVE')}}: </label>
                    <div class="col-md-6">
                        <textarea class="form-control" id="before_content" name="before_content">{!!old('before_content')!!}</textarea>
                    </div>
                    <div class="col-md-6 div-image">
                        <div class="file-input file-input-ajax-new">
                            <div class="input-group file-caption-main">
                                <div class="input-group-btn input-group-append">
                                    <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                        <input type="file" class="upload-images" multiple="multiple" name="before_upload[]" data-type ="{{\App\File::TYPE_IMAGE_PROJECT_BEFORE}}" data-fouc="">
                                    </div>
                                </div>
                            </div>
                            <div class="file-preview">
                                <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="image-link" >
                        <input type="hidden" name="image_before_id" class="image-id" >
                        <input type="hidden" class="image-name" >
                    </div>
                    <div id="html5_content" style="padding-left:15px;">
                        <div style="margin-top:3px;">
                           <div id="drop_" class="drop">
                              Drop files here.
                           </div>
                           <div class="file_input_div">
                              Thêm file đính kèm trước cải tiến
                              <input type="file" class="file_html5 file_input_hidden file_upload" data-type="{{\App\File::TYPE_FILE_PROJECT_BEFORE}}" name="file[]" size="40" multiple="" style="display:inline-block;">
                           </div>
                        </div>
                        <div class="clear_both_0px"></div>
                        <div id="upload_message">
                        </div>
                        <table id="upload_table" class="attachment_list_base_grn">
                           <tbody id="list_file_upload_before">
                              <tr>
                                 <td></td>
                              </tr>
                           </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">{{trans('base.AFTER')}}</span> {{trans('base.IMPROVE')}}: </label>
                    <div class="col-md-6">
                        <textarea class="form-control" id="after_content" name="after_content">{!!old('after_content')!!}</textarea>
                    </div>
                    <div class="col-md-6 div-image">
                        <div class="file-input file-input-ajax-new">
                            <div class="input-group file-caption-main">
                                <div class="input-group-btn input-group-append">
                                    <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                        <input type="file" class="upload-images" multiple="multiple" name="after_upload[]" data-type ="{{\App\File::TYPE_IMAGE_PROJECT_AFTER}}"  id ='image_after' data-fouc="">
                                    </div>
                                </div>
                            </div>
                            <div class="file-preview ">
                                 <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">

                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="image-link" >
                        <input type="hidden" name="image_after_id" class="image-id" >
                        <input type="hidden" class="image-name" >
                    </div>
                    <div id="html5_content" style="padding-left:15px;">
                        <div style="margin-top:3px;">
                           <div id="drop_" class="drop">
                              Drop files here.
                           </div>
                           <div class="file_input_div">
                              Thêm file đính kèm sau cải tiến
                              <input type="file" class="file_html5 file_input_hidden file_upload" name="file[]" size="40" data-type="4" id="file_upload" multiple="" style="display:inline-block;">
                           </div>
                        </div>
                        <div class="clear_both_0px"></div>
                        <div id="upload_message">
                        </div>
                        <table id="upload_table" class="attachment_list_base_grn">
                           <tbody id="list_file_upload_after">
                              <tr>
                                 <td></td>
                              </tr>
                           </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                        <div class="radio">
                            <label for="check_personal"><input type="radio" name="type" id="check_personal" value="1" checked style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">{{trans('base.PERSONAL_PROJECT')}}</span></label>
                        </div>
                    </div>
                    <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                        <div class="radio">
                            <label for="check_team"><input type="radio" name="type" id="check_team" value="2" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">{{trans('base.GROUP_PROJECT')}}</span></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <p class="col-md-12">Loại đề án</p>
                    <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                        <div class="radio">
                            <label for="check_1"><input type="radio" name="pattern" id="check_1" value="1" checked style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">ĐỀ ÁN LÀM NGAY</span></label>
                        </div>
                    </div>
                    <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                        <div class="radio">
                            <label for="check_2"><input type="radio" name="pattern" id="check_2" value="2" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">ĐỀ ÁN CẦN ĐƯỢC PHÊ DUYỆT</span></label>
                        </div>
                    </div>
                </div>
                @if(\Auth::guard('member')->user()->level < 3)
                <div class="row">
                    <p class="col-md-12">Chọn người duyệt đề án</p>
                    <div class="form-group col-md-6">
                        <select class="form-control select-search choose-member-approved" name="member_approved_id" data-placeholder="{{trans('base.Choose_member')}}">
                            {!!$member_approved_html!!}
                        </select>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <button class="btn btn-success submit-form" type="button"><span class="icons icons-send-white"></span>  {{trans('base.SEND')}}</button>
                    <button class="btn btn-secondary" name='draft' value='1' title="{{trans('base.Draft')}}"><span class="icons icons-draft-white"></span>Nháp</button>
                    <a href="{{route('frontend.project.index')}}" class="btn btn-danger"><span class="icons icons-cancel-white"></span>{{trans('base.Back')}}</a>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="overlay"></div>
<div class="modal fade" id="modal_helper_question" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel"><span>Hỏi đáp</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!!\App\Config::first()->content!!}
            </div>

        </div>
    </div>
</div>
<script
<script src="{!! asset('assets/global_assets/js/plugins/forms/selects/select2.min.js') !!}"></script>
<script src="{!! asset('assets2/js/project.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script type="text/javascript">
        $('#check_member').change(function(){
            if($(this).is(':checked')){
                $('.choose-member').prop('disabled', false);
            }else{
                 $(".choose-member").val("");
                 $(".choose-member").select2();
                 $('.choose-member').prop('disabled', 'disabled');
                 $('#info_member').html(`<div style='padding-left: 0px'>
                                            <div class="avatar-member">
                                                <img src="@if(is_null(\Auth::guard('member')->user()->file())){!!asset('assets2/img/man.png')!!} @else /{!!\Auth::guard('member')->user()->file()->link!!} @endif" style="width: 100%;">
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <h3>{!!\Auth::guard('member')->user()->full_name!!}</h3>
                                            <p>{{trans('base.Employee_code')}}: <span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                            <p>{{trans('base.Position')}}: <span>(@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif)</span></p>
                                            
                                        </div>`);
            }
        })
        $('#check_level').change(function(){
            if($(this).is(':checked')){
                $('.choose-level').prop('disabled', false);
            }else{
                 $('.choose-level').prop('disabled', 'disabled');
            }
        })
        $('.choose-member').change(function(){
            var member_id = $(this).val();
            $.ajax({
                url:'/api/getInfoMember',
                method:'POST',
                data:{member_id:member_id},
                success: function(response){
                    $('#info_member').html(`<div style='padding-left: 0px'>
                                                <div class="avatar-member">
                                                    <img src="`+response.member.avatar+`" style="width: 100%;">
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <h3>`+response.member.full_name+`</h3>
                                                <p>{{trans('base.Employee_code')}}: <span>`+response.member.login_id+`</span></p>
                                                <p>{{trans('base.Position')}}: <span>`+response.member.position_name+`</span></p>
                                                
                                            </div>`);
                }
            })
        })
        $.ajaxSetup ({
            cache: false
        });
        ClassicEditor
                .create( document.querySelector( '#before_content' ),{
                    removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                    image: {}
                } )
                .then( editor => {
                    before_content = editor;
                } )
                .catch( error => {
                    console.error( error );
        } );
        ClassicEditor
                .create( document.querySelector( '#after_content' ),{
                    removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
                    image: {}
                } )
                .then(editor => {
                      descriptionEditor1 = editor;
                      if($('#after_content').hasClass('disabled')){
                            descriptionEditor1.isReadOnly = true;
                      }
                    })
                .catch( error => {
                    console.error( error );
        });
        $('.submit-form').click(function(e){
            e.preventDefault();
            var name = $('#subject_name').val();
             $.ajax({
                url:'/api/checkProject',
                method:'POST',
                data:{name:name},
                success: function(response){
                    if(before_content.getData() == '' ||  $('input[name="name"]').val() == ''){
                        var notifier = new Notifier();
                        var notification = notifier.notify("info", "Nhập đầy đủ thông tin trước khi gửi");
                        notification.push();
                    }else if($('.choose-member-approved').val() == ''){
                        var notifier = new Notifier();
                        var notification = notifier.notify("info", "Cần chọn người duyệt đề án trước khi gửi");
                        notification.push();
                    }else if(response.success === 'true'){
                        var notifier = new Notifier();
                        var notification = notifier.notify("info", "Tên đề tài đã tồn tại");
                    }else{
                         $('#FrmCreateProject').submit();
                    }
                }
            })
        })
        $('.helper-question').click(function(){
            $('#modal_helper_question').modal('show');
        })
        $('input[name="pattern"]').change(function(){
            if($(this).val() == 2){
                $('#after_content').attr('disabled',true);
                descriptionEditor1.isReadOnly = true;
                $('#image_after').attr('disabled',true);
                $('#image_after').attr('data-fouc','disabled');
            }else{
                $('#after_content').attr('disabled',false);
                descriptionEditor1.isReadOnly = false;
                $('#image_after').attr('disabled',false);
                $('#image_after').attr('data-fouc','');
            }
        })
        $(document).on('change', '.file_upload', function () {
            var file_data = $(this).prop('files');
            var type = $(this).data('type');
            var form_data = new FormData();
            for(let i=0;i<file_data.length;i++){
                form_data.append('file[]', file_data[i]);
            }
            form_data.append('type',type);
            form_data.append('format',2);
            $.ajax({
                    url: '/api/upload-files',
                    type: 'POST',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function(response){
                        if(response.success == 'true'){
                            if(type == {{\App\File::TYPE_FILE_PROJECT_BEFORE}}){
                                response.data.forEach(element =>
                                $('#list_file_upload_before').append(`<tr>
                                                                        <td>
                                                                            <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                        </td> 
                                                                        <td>`+element.name+` (`+element.size+` Mb)</td>
                                                                    </tr>`)
                                );
                            }else{
                                response.data.forEach(element =>
                                $('#list_file_upload_after').append(`<tr>
                                                                        <td>
                                                                            <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                        </td>
                                                                        <td>`+element.name+` (`+element.size+` Mb)</td>
                                                                    </tr>`)
                                );
                            }
                        }
                    }
            });
        })
</script>
@stop