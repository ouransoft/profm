@extends('frontend.layouts.project')
@section('content')
<style>
    .project-content {
        background: none;
    }
    .project-content .content {
        margin: 0 10px;
        padding:0px;
    }
    .project-content .card {
        margin-top: 3px;
    }
    .project-content .content {
        box-shadow: rgb(0 0 0 / 25%) 0px 0.0625em 0.0625em, rgb(0 0 0 / 25%) 0px 0.125em 0.5em, rgb(255 255 255 / 10%) 0px 0px 0px 1px inset;
    }
</style>
<div class="project-content">
    <div class="content">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        {{trans('base.Search_filters')}}
                        <i class="icon-circle-down2"></i>
                    </button>
                </h5>
            </div>
            <div id="collapseOne" class="collapse @if(count($_GET) > 0) show @endif" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <form id="search_project" method='GET' action='{{route('frontend.project.list')}}'>
                        <input type="hidden" name="keyword_project" value="@if(isset($_GET['keyword_project'])){{$_GET['keyword_project']}}@endif">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control search-full_name" name="full_name" value='@if(isset($_GET['full_name'])){{$_GET['full_name']}}@endif' placeholder="Tên thành viên">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control search-status select-search" name="status" data-placeholder="Chọn trạng thái">
                                            {!!$status_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control search-level select-search" name="level" data-placeholder="Chọn cấp độ">
                                            {!!$level_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control" placeholder="Ngày bắt đầu" onfocus="(this.type='date')" type="text" name="start_date" value='@if(isset($_GET['start_date'])){{$_GET['start_date']}}@endif'>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control" placeholder="Ngày kết thúc" onfocus="(this.type='date')" type="text" name="end_date" value='@if(isset($_GET['end_date'])){{$_GET['end_date']}}@endif'>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="row">
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary btn-search">{{trans('base.Search')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="top-sidebar">
            <div class="top-left-sidebar" style="width:85%;float:left">
                <ul>
                    <li><input class='checkbox-action-project' type="checkbox" id='check_all' value='all'></li>
                    <li><a href="javascript:void(0)" class='export-project' title="Xuất file excel"><img  src="{!!asset('assets2/img/excel.png')!!}"></a></li>
                    <li><a href="javascript:void(0)" class="save-project" title="Lưu lại"><img  src="{!!asset('assets2/img/save (2).png')!!}"></a></li>
                </ul>
            </div>
            <div class="top-right-sidebar">
                @if(count($records) > 0)
                <div id="show_page_project">
                    <p style="padding-top:15px;padding-right:10px">{{$records->firstItem()}}-{!!$records->lastItem()!!} {{trans('base.among')}} {!!$records->total()!!}</p>
                    <a href="{{$records->previousPageUrl()}}" class='forward-page-project' @if($records->currentPage() == 1) disabled="disabled" @endif><img src="{!!asset('assets2/img/left-arrow.png')!!}"></a>
                    <a href="{{$records->nextPageUrl()}}" class='next-page-project' @if($records->currentPage() == $records->lastPage()) disabled="disabled" @endif><img src="{!!asset('assets2/img/forward.png')!!}"></a>
                </div>
                @endif
            </div>
        </div>
        <div style="overflow-y: scroll;width:100%;height:825px;">
            <table class="table table-content" style="width:100%;margin-top:0px">
                <tbody id="records-project">
                    @foreach($records as $key=>$record)
                    <tr class='project-detail'>
                        <td colspan="1" class="td-checkbox"><input style="width:16px;height:20px;" type="checkbox" value="{{$record->id}}" name="project_id" class='check'></td>
                        <td class='inhert' colspan="1" class="link-project" data-href="{!!route('frontend.project.view',$record->id)!!}">
                            {!!$record->getStatus!!}
                        </td>
                        <td colspan="1" style="width: 200px;font-weight: 500" class="link-project" data-href="{!!route('frontend.project.view',$record->id)!!}">
                            {!!$record->member->full_name!!} ({!!$record->member->login_id!!})
                        </td>
                        <td colspan="5" class="link-project" data-href="{!!route('frontend.project.view',$record->id)!!}"><a  href="{!!route('frontend.project.view',$record->id)!!}"><span>{{$record->name}}</span></a></td>
                        <td style="width:130px;font-size: 14px;" class="link-project" data-href="{!!route('frontend.project.view',$record->id)!!}"><span>{{date('d',strtotime($record->created_at))}} tháng {{date('m',strtotime($record->created_at))}}</span></td>                               
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class='show-chart'>
    </div>
</div>
<div class="modal fade" id="modal_choose_report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Chọn mẫu xuất file báo cáo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="frmExport">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="row" style="margin-bottom:20px;">
                            <div class="col-md-2">Bắt đầu</div>
                            <div class="col-md-4">
                                <input type="date" class="form-control" name="start_date" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="col-md-1">đến</div>
                            <div class="col-md-4">
                                <input type="date" class="form-control" name="end_date" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                        <div class="row" style="margin-bottom:20px;">
                            <div class="col-md-2">Phòng</div>
                            <div class="col-md-4 select-full">
                                <select class="form-control select-search" name="department_id" data-placeholder="{{trans('base.All')}}">
                                    {!!$department_html!!}
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export1"><input type="radio" name="type" id="export1" value="1" checked style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo cá nhân không có đề tài</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export2"><input type="radio" name="type" id="export2" value="2" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo xếp loại nhóm</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export3"><input type="radio" name="type" id="export3" value="3" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo xếp loại cá nhân theo quý</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export4"><input type="radio" name="type" id="export4" value="4" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo tổng hợp đề tài</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export5"><input type="radio" name="type" id="export4" value="5" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo cá nhân không có đề án theo tháng</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="export5"><input type="radio" name="type" id="export4" value="6" style="width:22px;height:22px!important;"><span style="margin-bottom: 13px;position: absolute;left: 50px;">Mẫu báo cáo xếp thứ tự nhóm</span></label>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="button" class='submit-export'>Xuất báo cáo</button>
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_update_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">THAY ĐỔI<span class="orange"> CẤP ĐỘ</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmUpdateLevel'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' id="project_update_id">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="col-md-2">Cấp độ</label>
                                <div class="col-md-6 select-full">
                                    <select name="level" class="select-search form-control" id="select_level" data-placeholder="Không cấp độ">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12 ">
                                <label class="col-md-2">Lí do</label>
                                <div class="form-group col-md-12">
                                    <textarea class='form-control' name='reason_update' rows="10" required="" oninvalid="this.setCustomValidity('Enter Reason Here')" oninput="this.setCustomValidity('')"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn btn-submit submit-update-project'> Cập nhật </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/datatables.min.js') !!}"></script>
<script src="{!! asset('assets2/js/datatables_basic.js') !!}"></script>
<script src="{!! asset('assets2/js/zoom.js') !!}"></script>
<script src="{!! asset('assets2/js/photoswipe.min.js') !!}"></script>
<script src="{!! asset('assets2/js/photoswipe-ui-default.min.js') !!}"></script>
<script>
    $('#zoomBtn').click(function() {
$('.zoom-btn-sm').toggleClass('scale-out');
if (!$('.zoom-card').hasClass('scale-out')) {
$('.zoom-card').toggleClass('scale-out');
}
});
$('.zoom-btn-sm').click(function() {
var btn = $(this);
var card = $('.zoom-card');
if ($('.zoom-card').hasClass('scale-out')) {
$('.zoom-card').toggleClass('scale-out');
}
if (btn.hasClass('zoom-btn-person')) {
card.css('background-color', '#d32f2f');
} else if (btn.hasClass('zoom-btn-doc')) {
card.css('background-color', '#fbc02d');
} else if (btn.hasClass('zoom-btn-tangram')) {
card.css('background-color', '#388e3c');
} else if (btn.hasClass('zoom-btn-report')) {
card.css('background-color', '#1976d2');
} else {
card.css('background-color', '#7b1fa2');
}
});
$('#check_all').change(function () {
if ($(this).is(":checked")) {
$('.check').each(function () {
$(this).prop('checked', true);
});
} else {
$('.check').each(function () {
$(this).prop('checked', false);
});
}
});
$('body').delegate('.check', 'change', function(){
var member_id = [];
$('.check').each(function () {
if ($(this).is(':checked')) {
member_id.push($(this).val());
}
$('#check_all').attr('value', member_id.join(','));
})
})
        $('#frmReason').submit(function(e){
e.preventDefault();
var form = $(this);
$.ajax({
url:"/api/return-project",
        method:"POST",
        data:form.serialize(),
        success: function(response){
        if (response.success == true){
        $('#modal_return_project').modal('hide');
        var notifier = new Notifier();
        var notification = notifier.notify("success", "Đề án trả về thành công");
        notification.push();
        setTimeout(function(){ location.reload(); }, 2000);
        }
        }
})
        });
$('body').delegate('.send-project', 'click', function(){
var project_id = $(this).data('project_id');
$('#send_project_id').val(project_id);
$('#modal_send_project').modal('show');
})
        $('#frmSend').submit(function(e){
e.preventDefault();
var form = $(this);
$.ajax({
url:"/api/send-project",
        method:"POST",
        data:form.serialize(),
        success: function(response){
        if (response.success == true){
        $('#modal_send_project').modal('hide');
        var notifier = new Notifier();
        var notification = notifier.notify("success", "Duyệt đề án thành công");
        notification.push();
        setTimeout(function(){ location.reload(); }, 2000);
        }
        }
})
        });
$('body').delegate('.save-project', 'click', function(){
if ($('input[type="checkbox"]:checked').val() === undefined){
var notifier = new Notifier();
var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
notification.push();
} else{
var project_id = 0;
if ($('#check_all').is(":checked")){
project_id = 'all';
} else{
project_id = $('#check_all').attr('value');
}
$.ajax({
url: '/api/save-project',
        method: 'POST',
        data: {project_id: project_id},
        success: function (response) {
        var notifier = new Notifier();
        var notification = notifier.notify("success", "Lưu thành công");
        notification.push();
        $('input[type=checkbox]').prop('checked', false);
        }
});
}
})
        $('.chart-list').click(function(){
$('.load-page').removeClass('active');
$(this).addClass('active');
$('.content').attr('style', 'display:none');
$(".show-chart").attr('style', 'display:block;text-align:center;');
var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
$(".show-chart").html(ajax_load).load($(this).data('href'));
})
        $('body').delegate('.submit-export', 'click', function(){
if ($('input[type="checkbox"]:checked').val() !== undefined){
project_id = $('#check_all').attr('value');
} else{
project_id = '';
}
$input = $('<input type="hidden" name="project_id" value ="' + project_id + '"/>');
$('#frmExport').append($input);
$.ajax({
url: '/api/export-project',
        method: 'POST',
        data: $("#frmExport").serialize(),
        success: function (response) {
        window.location.href = response.href;
        }
});
})
        $('body').delegate('.export-project', 'click', function(){
$('#modal_choose_report').modal('show');
})
        $('body').delegate('.modal-return-project', 'click', function(){
$('#project_id').val($(this).data('project_id'));
$('#modal_return_project').modal('show');
})
        $('body').delegate('.update-level-project', 'click', function(){
project_id = $(this).data('project_id');
$.ajax({
url: '/api/getSelectLevel',
        method: 'POST',
        data: {project_id: project_id},
        success: function (response) {
        $('#select_level').html(response.html);
        $('#project_update_id').val(project_id);
        $('#modal_update_project').modal('show');
        }
});
})
        $('#frmUpdateLevel').submit(function(e){
e.preventDefault();
$.ajax({
url: '/api/update-level-project',
        method: 'POST',
        data: $("#frmUpdateLevel").serialize(),
        success: function (response) {
        var notifier = new Notifier();
        var notification = notifier.notify("success", "Cập nhật thành công");
        notification.push();
        $('#modal_update_project').modal('hide');
        $("#frmUpdateLevel")[0].reset();
        $('#modal_return_project').modal('hide');
        }
})
        })
        $('body').delegate('.show-comment', 'click', function(){
var comment = $(this).data('comment');
$('.content-comment').html(comment);
$('#modal_show_commment').modal('show');
})
 $('.link-project').click(function(){
        window.location.href = $(this).attr('data-href');
    })
</script>
@stop