@extends('frontend.layouts.admin')
@section('content')
<body class="page-body">
    <div class="page-project" style="margin:0px;">
        @include('frontend.project.sidebar')
        <div class="project-content">
            <div class="content project-content">
                @if($record->status != \App\Project::STATUS_ACTIVE)
                <h4 style="margin-top:25px">{{trans('base.EDITING_IMPROVED_PROJECT_INFORMATION')}}</h4>
                @else
                <h4 style="margin-top:25px">THÔNG TIN ĐỀ ÁN CẢI TIẾN</h4>
                @endif
                <div class="form-create">
                    <form method="POST" action="{{route('frontend.project.update',$record->id)}}" id="FrmUpdateProject">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="row" style="margin-bottom: 30px;">
                            <div class="notification-member col-md-6">
                                <div class="row" id='info_member' style="border: 1px solid;margin-left: 0px;border-radius: 4px;">
                                    <div class="col-md-3" style='padding-left: 0px'>
                                        <div class="img-member">
                                        <img src="@if(is_null(\Auth::guard('member')->user()->file())){!!asset('assets2/img/man.png')!!} @else /{!!\Auth::guard('member')->user()->file()->link!!} @endif" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <h3>{!!\Auth::guard('member')->user()->full_name!!}</h3>
                                        <p>{{trans('base.Employee_code')}}: <span>{!!\Auth::guard('member')->user()->login_id!!}</span></p>
                                        <p>{{trans('base.Position')}}: <span>@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" check-member>
                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-1 col-form-label">{{trans('base.SUBJECT_NAME')}}</label>
                            <div class="col-sm-11">
                                <input class="form-control" name="name" type="text" value='{{$record->name}}' @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">{{trans('base.BEFORE')}}</span> {{trans('base.IMPROVE')}}: </label>
                            <div class="col-md-6">
                                <textarea class="form-control @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif" id="before_content" name="before_content">{!!$record->before_content!!}</textarea>
                            </div>
                            <div class="col-md-6 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                                <input type="file" data-type="{{\App\File::TYPE_IMAGE_PROJECT_BEFORE}}" class="upload-images @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disable @endif" multiple="multiple" name="before_upload[]" data-fouc="" @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                        <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="image-link" value="{{$record->before_images ? implode(',',$record->before_images()->pluck('link')->toArray()) : ''}}">
                                <input type="hidden" name="image_before_id" class="image-id" value="{{$record->before_images ? implode(',',$record->before_images()->pluck('id')->toArray()) : ''}}">
                                <input type="hidden" class="image-name" value="{{$record->before_images ? implode(',',$record->before_images()->pluck('name')->toArray()) : ''}}">
                            </div>
                            <div id="html5_content" style="padding-left:15px;">
                                <div style="margin-top:3px;">
                                <div id="drop_" class="drop">
                                    Drop files here.
                                </div>
                                <div class="file_input_div">
                                    Thêm file đính kèm trước cải tiến
                                    <input type="file" class="file_html5 file_input_hidden file_upload" data-type="{{\App\File::TYPE_FILE_PROJECT_BEFORE}}" name="file[]" size="40" multiple="" style="display:inline-block;">
                                </div>
                                </div>
                                <div class="clear_both_0px"></div>
                                <div id="upload_message">
                                </div>
                                <table id="upload_table" class="attachment_list_base_grn">
                                <tbody id="list_file_upload_before">
                                    @if(count($record->before_files) > 0) 
                                        @foreach($record->before_files as $key=>$value)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="{{$value->id}}">
                                            </td>
                                            <td>{{$value->name}} ({{$value->size}} Mb) </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">{{trans('base.AFTER')}}</span> {{trans('base.IMPROVE')}}: </label>
                            <div class="col-md-6">
                                <textarea class="form-control @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif" id="after_content" name="after_content">{!!$record->after_content!!}</textarea>
                            </div>
                            <div class="col-md-6 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                                <input type="file" data-type="{{\App\File::TYPE_IMAGE_PROJECT_AFTER}}" class="upload-images @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disable @endif" multiple="multiple" name="after_upload[]" data-fouc="" @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                        <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="image-link" value="{{$record->after_images ? implode(',',$record->after_images()->pluck('link')->toArray()) : ''}}">
                                <input type="hidden" name="image_after_id" class="image-id" value="{{$record->after_images ? implode(',',$record->after_images()->pluck('id')->toArray()) : ''}}">
                                <input type="hidden" class="image-name" value="{{$record->after_images ? implode(',',$record->after_images()->pluck('name')->toArray()) : ''}}">
                            </div>
                            <div id="html5_content" style="padding-left:15px;">
                                <div style="margin-top:3px;">
                                <div id="drop_" class="drop">
                                    Drop files here.
                                </div>
                                <div class="file_input_div">
                                    Thêm file đính kèm sau cải tiến
                                    <input type="file" class="file_html5 file_input_hidden file_upload" name="file[]" size="40" data-type="4" id="file_upload" multiple="" style="display:inline-block;">
                                </div>
                                </div>
                                <div class="clear_both_0px"></div>
                                <div id="upload_message">
                                </div>
                                <table id="upload_table" class="attachment_list_base_grn">
                                <tbody id="list_file_upload_after">
                                    @if(count($record->after_files) > 0) 
                                        @foreach($record->after_files as $key=>$value)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="{{$value->id}}">
                                            </td>
                                            <td>{{$value->name}} ({{$value->size}} Mb)</td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <p>Loại đề án</p>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="check_1"><input type="radio" name="pattern" id="check_1" value="1" checked style="width:22px;height:22px!important;" @if($record->pattern ==  1) checked @endif><span style="margin-bottom: 13px;position: absolute;left: 50px;">ĐỀ ÁN LÀM NGAY</span></label>
                                </div>
                            </div>
                            <div class="form-check col-md-12" style="margin-left: 0px;padding-left:15px;">
                                <div class="radio">
                                    <label for="check_2"><input type="radio" name="pattern" id="check_2" value="2" style="width:22px;height:22px!important;" @if($record->pattern ==  2) checked @endif><span style="margin-bottom: 13px;position: absolute;left: 50px;">ĐỀ ÁN CẦN ĐƯỢC PHÊ DUYỆT</span></label>
                                </div>
                            </div>
                        </div>
                        @if($record->status == 0 && \Auth::guard('member')->user()->level < 3)
                        <div class="row">
                            <p class="col-md-12">Chọn người duyệt đề án</p>
                            <div class="form-group col-md-6">
                                <select class="form-control select-search choose-member-approved" name="member_approved_id" data-placeholder="{{trans('base.Choose_member')}}">
                                    {!!$member_approved_html!!}
                                </select>
                            </div>
                        </div>
                        @endif
                        @if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0)
                        <div class="form-group">
                            @if($record->status == 0)
                            <button class="btn btn-primary" type="submit" name="draft" value="1"><span class="icons icons-save-white"></span>  {{trans('base.Save')}}</button>
                            @endif
                            <button class="btn btn-success submit-form" type="submit" name="draft" value="0">@if($record->status != 0) <span class="icons icons-edit"></span> {{trans('base.Edits')}} @else <span class="icons icons-send-white"></span> {{trans('base.SEND')}} @endif</button>
                            <a href="{{route('frontend.project.index')}}" class="btn btn-danger"><span class="icons icons-cancel-white"></span>{{trans('base.Back')}}</a>
                            <!--<a href="#" style="padding: 11px 15px 15px;border: 1px solid #a6a5a5;"><img style="width:24px;" src="/public/assets2/img/trash.png"></a>-->
                        </div>
                        @if($record->status != 0)
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-left"><span class="orange">{{trans('base.Reason_for_return')}}: </label>
                            <div class="col-md-12">
                                <textarea class="form-control" readonly="">{{$record->reason}}</textarea>
                            </div>
                        </div>
                        @endif
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
@stop
@section('script')
@parent
<script src="{!! asset('assets/global_assets/js/plugins/forms/selects/select2.min.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script src="{!! asset('ckfinder/ckfinder.js') !!}"></script>
<script>
    $('#check_member').change(function(){
        if($(this).is(':checked')){
            $('.choose-member').prop('disabled', false);
        }else{
             $('.choose-member').prop('disabled', 'disabled');
        }
    })
    $('#check_level').change(function(){
        if($(this).is(':checked')){
            $('.choose-level').prop('disabled', false);
        }else{
             $('.choose-level').prop('disabled', 'disabled');
        }
    })
    $.ajaxSetup ({
        cache: false
    });
    $('.load-page').click(function(){
        $('.load-page').removeClass('active');
        $(this).addClass('active');
       
        $(".project-content").load($(this).data('href')); 
    })
    ClassicEditor
            .create( document.querySelector( '#before_content' ),{
		removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
		image: {}
	    } )
            .then( editor => {
                before_content = editor;
            } )
            .catch( error => {
                console.error( error );
    } );
    ClassicEditor
            .create( document.querySelector( '#after_content' ) ,{
		removePlugins: ['CKFinderUploadAdapter', 'CKFinder', 'EasyImage', 'Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed'],
		image: {}
	    } )
            .then(editor => {
                  console.log(editor);
                  descriptionEditor = editor;
                  if($('#after_content').hasClass('disabled')){
                        descriptionEditor.isReadOnly = true;
                  }
                })
            .catch( error => {
                console.error( error );
    } );
    $('.submit-form').click(function(e){
        e.preventDefault();
        var name = $('#subject_name').val();
         $.ajax({
            url:'/api/checkProject',
            method:'POST',
            data:{name:name},
            success: function(response){
                if(before_content.getData() == '' ||  $('input[name="name"]').val() == ''){
                    var notifier = new Notifier();
                    var notification = notifier.notify("info", "Nhập đầy đủ thông tin trước khi gửi");
                    notification.push();
                }else if($('.choose-member-approved').val() == ''){
                    var notifier = new Notifier();
                    var notification = notifier.notify("info", "Cần chọn người duyệt đề án trước khi gửi");
                    notification.push();
                }else{
                     $('#FrmUpdateProject').submit();
                }
            }
        })
    })
    $(document).on('change', '.file_upload', function () {
            var file_data = $(this).prop('files');
            var type = $(this).data('type');
            var form_data = new FormData();
            for(let i=0;i<file_data.length;i++){
                form_data.append('file[]', file_data[i]);
            }
            form_data.append('type',type);
            form_data.append('format',2);
            $.ajax({
                    url: '/api/upload-files',
                    type: 'POST',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function(response){
                        if(response.success == 'true'){
                            if(type == {{\App\File::TYPE_FILE_PROJECT_BEFORE}}){
                                response.data.forEach(element =>
                                $('#list_file_upload_before').append(`<tr>
                                                                        <td>
                                                                            <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                        </td> 
                                                                        <td>`+element.name+` (`+element.size+` Mb)</td>
                                                                    </tr>`)
                                );
                            }else{
                                response.data.forEach(element =>
                                $('#list_file_upload_after').append(`<tr>
                                                                        <td>
                                                                            <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                        </td>
                                                                        <td>`+element.name+` (`+element.size+` Mb)</td>
                                                                    </tr>`)
                                );
                            }
                        }
                    }
            });
        })
</script>
@stop