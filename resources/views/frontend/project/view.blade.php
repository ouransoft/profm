@extends('frontend.layouts.project')
@section('content')
<link href="{!!asset('assets2/css/default-skin.css')!!}" rel="stylesheet">
<link href="{!!asset('assets2/css/photoswipe.css')!!}" rel="stylesheet">
<div class="project-content" style="background: none;">
    <div class="content" style="padding:40px 80px;">
        <div class="">
            <!-- multistep form -->
            <div id="msform">
                <!-- progressbar -->
                <div class="progresswrap">
                <ul id="progressbar">
                    @foreach(\App\Project::Progress_arr as $key=>$val)
                    @if($progress >= $key && \App\LogApproved::where('project_id',$record->id)->where('progress',$key)->first() && \App\LogApproved::where('project_id',$record->id)->where('progress',$key)->first()->status == 2)
                    <li class="active" ><a href="javascript:void(0)" class="history" data-progress="{{$key}}">{{trans('base.'.$val)}}</a></li>
                    @elseif($progress >= $key && \App\LogApproved::where('project_id',$record->id)->where('progress',$key)->first() && \App\LogApproved::where('project_id',$record->id)->where('progress',$key)->first()->status == 0) 
                    <li class="error"><a href="javascript:void(0)" class="history" data-progress="{{$key}}">{{trans('base.'.$val)}}</a></li>
                    @elseif($progress >= $key)
                    <li class="active"><a href="javascript:void(0)">{{trans('base.'.$val)}}</a></li>
                    @else
                    <li><a href="javascript:void(0)">{{trans('base.'.$val)}}</a></li>
                    @endif
                    @endforeach
                </ul>
            </div>
            <fieldset class="content-box-shadow">
                <div class="row" style="width:100%">
                    @if($member_approved && $member_approved->member_id == \Auth::guard('member')->user()->id && $progress < 3)
                        <a href="javascript:void(0)" class="btn btn-return" data-toggle="modal" data-target="#modal_return_project">{{trans('base.Return')}}</a>
                        @if(\Auth::guard('member')->user()->level == 3)
                        <a href="javascript:void(0)" class="change-approved btn btn-approved" style="right:150px" data-project_id="{{$record->id}}">Chuyển người duyệt</a>
                        @endif
                        <a href="javascript:void(0)" class="send-project btn btn-approved" data-project_id="{{$record->id}}">{{trans('base.Approve_project')}}</a>
                    @endif
                    @if(($progress == 3 && $member_approved && $member_approved->member_id == \Auth::guard('member')->user()->id))
                        <a href="javascript:void(0)" class="btn btn-approved" data-toggle="modal" data-target="#modal_assign_project">Giao việc</a>
                    @endif
                    @if($record->consider == 1 && \Auth::guard('member')->user()->level == \App\Member::LEVEL_TK)
                        <a href="javascript:void(0)" class="btn btn-approved" data-toggle="modal" data-target="#modal_consider_project">Chọn cấp độ</a>
                    @endif
                    @if($member_project && $member_project->status == 0)
                        <a href="javascript:void(0)" class="btn btn-report" data-toggle="modal" data-target="#modal_report_project">Báo cáo kết quả</a>
                    @endif
                    @if(($progress == 5 || ($progress == 6 && \App\LogApproved::where('project_id',$record->id)->where('progress',6)->first()->status == 1)) && \Auth::guard('member')->user()->level == 3)
                        <a href="javascript:void(0)" class="btn btn-unapproved" data-toggle="modal" data-target="#modal_unapproved_project">{{trans('base.Return')}}</a>
                        <a href="javascript:void(0)" class="btn btn-approved" data-toggle="modal" data-target="#modal_complete_project">Duyệt</a>
                    @endif
                </div>
                <h2 class="fs-title" style="width:100%;padding-top: 20px;">{{$record->name}}</h2>
                <div class="row" style="width: 100%;padding:0 30px;">
                    <h3 class="fs-subtitle"><span class="orange"><u>{{trans('base.BEFORE')}} {{trans('base.IMPROVE')}}</u></span></h3>
                    <div class="content-project">
                        {!!$record->before_content!!}
                    </div>
                    <div id="demo-test-gallery" class="demo-gallery" style="width:100%">
                        @if(is_array($record->before_images) || is_object($record->before_images))
                            @foreach($record->before_images as $key=>$val)
                            <a href="{{asset($val->link)}}" data-size="1600x1600" data-med="{{asset($val->link)}}" data-med-size="1024x1024" data-author="" class="demo-gallery__img--main">
                                <img src="{{asset($val->link)}}" alt="" class="left45" />
                            </a>
                            @endforeach
                        @endif
                    </div>
                    <span class="extension--icon-pulse--learn"></span>
                    @if(count($record->before_files) > 0)
                    <div>
                        <p class='bold text-left'>File đính kèm:</p>
                        @foreach($record->before_files as $key=>$value)
                            <p class='text-left'><a href='{{asset($value->link)}}'> {{$value->name}} ({{$value->size}})</a></p>
                        @endforeach
                    </div>
                    @endif
                </div>
                @if(!is_null($record->after_content))
                <div class="row" style="width: 100%;padding:0 30px;">
                    <h3 class="fs-subtitle"><span class="orange"><u>{{trans('base.AFTER')}} {{trans('base.IMPROVE')}}</u></span></h3>
                    <div class="content-project">
                        {!!$record->after_content!!}
                    </div>
                    <div id="demo-test-gallery" class="demo-gallery" style="width:100%">
                         @if(is_array($record->after_images) || is_object($record->after_images))
                            @foreach($record->after_images as $key=>$val)
                            <a href="{{asset($val->link)}}" data-size="1600x1600" data-med="{{asset($val->link)}}" data-med-size="1024x1024" data-author="" class="demo-gallery__img--main">
                                <img src="{{asset($val->link)}}" alt="" class="left45" />
                            </a>
                            @endforeach
                        @endif
                    </div>
                    <span class="extension--icon-pulse--learn"></span>
                    @if(count($record->after_files) > 0)
                    <div>
                        <p class='bold text-left'>File đính kèm:</p>
                        @foreach($record->after_files as $key=>$value)
                            <p class='text-left'><a href='{{asset($value->link)}}'> {{$value->name}} ({{$value->size}})</a></p>
                        @endforeach
                    </div>
                    @endif
                </div>
                @endif
                @if(count($list_member_project) > 0)
                    @foreach($list_member_project as $key=>$val)
                        <div class="row" style="width: 100%;padding:0 30px;">
                            <h3 class="fs-subtitle"><span class="orange">Người báo cáo: {{$val->member->full_name}}</span></h3> 
                            <h3 class="fs-subtitle"><span class="orange">Nội dung:</span></h3>
                            <div class="content-project">
                                {!!$val->after_content!!}
                            </div>
                            @if($val->images)
                            <div id="demo-test-gallery" class="demo-gallery" style="width:100%">
                                @foreach($val->images as $key=>$val)
                                <a href="{{asset($val->link)}}" data-size="1600x1600" data-med="{{asset($val->link)}}" data-med-size="1024x1024" data-author="" class="demo-gallery__img--main">
                                    <img src="{{asset($val->link)}}" alt="" class="left45" />
                                </a>
                                @endforeach
                            </div>
                            @endif
                            <span class="extension--icon-pulse--learn"></span>
                        </div>
                    @endforeach
                @endif
            </fieldset>
        </div>
    </div>  
</div>
</div>
<div class="modal fade" id="modal_return_project" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">{{trans('base.Reason_for_return')}}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmReason'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value='{{$record->id}}'>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='reason' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='submit-return-project'><i class="icon-reply" style="color:#fff"></i> {{trans('base.Return')}} </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_unapproved_project"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">{{trans('base.Reason_for_return')}}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmReasonUnapproved'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value='{{$record->id}}'>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='reason' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='submit-return-project'><i class="icon-reply" style="color:#fff"></i> {{trans('base.Return')}} </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_show_commment"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">COMMENT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <p class="content-comment"></p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_send_project" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">DUYỆT ĐỀ ÁN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmSend'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' id="send_project_id">
                        @if(\Auth::guard('member')->user()->level == 2)
                        <div class="row">
                            <p class="col-md-12 row">Chọn cấp tiếp theo duyệt đề án</p>
                            <div class="form-group col-md-12 row">
                                <select class="form-control select-search choose-member-approved" name="member_approved_id" data-placeholder="{{trans('base.Choose_member')}}">
                                    {!!$member_approved_html!!}
                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <p class="col-md-12 row">Comment</p>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='comment' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        @if(\Auth::guard('member')->user()->level == \App\Member::LEVEL_D2)
                        <div class="row">
                            <label style="display: flex">
                                <input type="checkbox" name="consider" class="form-control" value="1" style="margin-top:5px;margin-right:8px;">Xem xét đề án cấp độ
                            </label>
                        </div>
                        @endif
                        <div class='text-center'>
                            <button type="button" class='submit-send-project' name="type" value="1">{{trans('base.Approve_project')}} </button>
                            @if(\Auth::guard('member')->user()->level == \App\Project::STATUS_ACTIVE)
                            <button type="button" class='btn submit-send-project' name="type" value="2">{{trans('base.Completed')}} </button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_complete_project" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">DUYỆT ĐỀ ÁN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmComleteProject'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value="{{$record->id}}">
                        <div class="row">
                            <p class="col-md-12 row">Comment</p>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='comment' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='submit-complete-project'>{{trans('base.Completed')}} </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_change_approved" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Chuyển người duyệt đề án</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmChangeApproved'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value="{{$record->id}}">
                        <div class="row">
                            <p class="col-md-12 row">Chọn người duyệt đề án</p>
                            <div class="form-group col-md-12 row">
                                <select class="form-control select-search choose-change-approved" name="member_approved_id" data-placeholder="{{trans('base.Choose_member')}}">
                                    {!!$member_approved_html!!}
                                </select>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn btn-success'>Gửi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_consider_project" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Xem xét đề án cấp độ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmConsiderProject'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value="{{$record->id}}">
                        <div class="row form-group">
                            <select class="select form-control" name="level" data-placeholder='Chọn cấp độ'>
                                {!!$level_html!!}
                            </select>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn btn-success'>{{trans('base.Save')}} </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_assign_project" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:1000px!important;" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Giao việc</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmAssign'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value="{{$record->id}}">
                        <div class="list-member">
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label>Nhân viên</label>
                                    <select class="form-control select-search mySelect" name="member_id[]" data-placeholder="Chọn thành viên">
                                        {!!$member_html!!}
                                    </select>
                                </div>
                                <div class="form-group col-md-6 row">
                                    <label class="">Công việc</label>
                                    <textarea class="form-control" name="work[]"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center' style="margin-top:20px;">
                            <button type="button" class='add-member btn-primary btn'>Thêm thành viên</button>
                            <button type="submit" class='submit-send-project' name="type" value="1">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_show_history" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Lịch sử</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid content-history">
                    
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_report_project" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Báo cáo kết quả</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="frmReportProject">
                        <input type="hidden" name="project_id" value="{{$record->id}}">
                        <div class="form-group row">
                            <label class="col-md-12 required control-label text-left text-semibold"><span class="orange">Nội dung: </label>
                            <div class="col-md-12" style="margin-bottom: 10px">
                                <textarea class="form-control" id="after_content" name="after_content">{!!old('after_content')!!}</textarea>
                            </div>
                            <div class="col-md-12 div-image">
                                <div class="file-input file-input-ajax-new">
                                    <div class="input-group file-caption-main">
                                        <div class="input-group-btn input-group-append">
                                            <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">{{trans('base.Choose_an_image')}}</span>
                                                <input type="file" class="upload-images" multiple="multiple" name="after_upload[]" data-type="{{\App\File::TYPE_IMAGE_PROJECT_REPORT}}" data-fouc="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="file-preview ">
                                         <div class=" file-drop-zone" ondrop="dropHandler(event);"  ondragover="return false">

                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="image-link">
                                <input type="hidden" name="image_after_id" class="image-id" >
                                <input type="hidden" class="image-name">
                            </div>
                        </div>
                        <div class='text-center' style="margin-top:20px;">
                            <button type="submit" class='submit-report-project'>Gửi báo cáo</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_show_history" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Lịch sử</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid content-history">
                    
                </div>
            </div>

        </div>
    </div>
</div>
<div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
      <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
      </div>
      <div class="pswp__ui pswp__ui--hidden">
        <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
                <div class="pswp__preloader__icn">
                  <div class="pswp__preloader__cut">
                    <div class="pswp__preloader__donut"></div>
                  </div>
                </div>
            </div>
        </div>
                    <!-- <div class="pswp__loading-indicator"><div class="pswp__loading-indicator__line"></div></div> -->
        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip">
                                <!-- <a href="#" class="pswp__share--facebook"></a>
                                <a href="#" class="pswp__share--twitter"></a>
                                <a href="#" class="pswp__share--pinterest"></a>
                                <a href="#" download class="pswp__share--download"></a> -->
            </div>
        </div>
        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
        <div class="pswp__caption">
          <div class="pswp__caption__center">
          </div>
        </div>
      </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/zoom.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script src="{!! asset('assets2/js/photoswipe.min.js') !!}"></script>
<script src="{!! asset('assets2/js/photoswipe-ui-default.min.js') !!}"></script>
<script>
$('#check_member').change(function () {
    if ($(this).is(':checked')) {
        $('.choose-member').prop('disabled', false);
    } else {
        $('.choose-member').prop('disabled', 'disabled');
    }
})
$('#check_level').change(function () {
    if ($(this).is(':checked')) {
        $('.choose-level').prop('disabled', false);
    } else {
        $('.choose-level').prop('disabled', 'disabled');
    }
})
$('#frmReason').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: "/api/return-project",
        method: "POST",
        data: form.serialize(),
        success: function (response) {
            if (response.success == true) {
                $('#modal_return_project').modal('hide');
                var notifier = new Notifier();
                var notification = notifier.notify("success", "Đề án trả về thành công");
                notification.push();
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        }
    })
});
$('body').delegate('.send-project', 'click', function () {
    var project_id = $(this).data('project_id');
        $('#send_project_id').val(project_id);
        $('#modal_send_project').modal('show');
    })
    $('.submit-send-project').click(function(){
        if($('.choose-member-approved').val() == ''){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn cấp tiếp theo duyệt đề án");
            notification.push();
        }else{
            if($(this).attr('name')) {
                $(this).parents('form').append(
                    $("<input type='hidden'>").attr( { 
                        name: $(this).attr('name'), 
                        value: $(this).attr('value') })
                );
            }
            $('#frmSend').submit();
        }
    });
    $('#frmSend').submit(function (e) {
        e.preventDefault();
        var form = $(this);
    $.ajax({
            url: "/api/send-project",
            method: "POST",
            data: form.serialize(),
        success: function (response) {
                if (response.success == true) {
                    $('#modal_send_project').modal('hide');
            var notifier = new Notifier();
            var notification = notifier.notify("success", "Duyệt đề án thành công");
            notification.push();
            setTimeout(function () {
                       window.location.href = response.href;
            }, 2000);
        }
            }
        })
    });
    $('#frmComleteProject').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "/api/complete-project",
            method: "POST",
            data: form.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $('#modal_complete_project').modal('hide');
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Duyệt đề án thành công");
                    notification.push();
                    setTimeout(function () {
                       window.location.href = '{!!route('frontend.project.list')!!}';
                    }, 2000);
                }
            }
})
    })
$('body').delegate('.save-project', 'click', function () {
    var project_id = $(this).data('project_id');
    $.ajax({
        url: '/api/save-project',
        method: 'POST',
        data: {project_id: project_id},
        success: function (response) {
            var notifier = new Notifier();
            var notification = notifier.notify("success", "Lưu thành công");
            notification.push();
        }
    });
})
    $('body').delegate('.show-comment', 'click', function () {
        var comment = $(this).data('comment');
        $('.content-comment').html(comment);
        $('#modal_show_commment').modal('show');
    })
    $('body').delegate('.add-member','click',function(){
        $('.list-member').append(`<div class="row">
                                    <div class="form-group col-md-5">
                                        <label>Nhân viên</label>
                                        <select class="form-control select-search mySelect" data-placeholder="Chọn thành viên" name='member_id[]'>
                                            {!!$member_html!!}
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 row">
                                        <label class="">Công việc</label>
                                        <textarea class="form-control" name="work[]"></textarea>
                                    </div>
                                    <div class="form-group col-md-1">
                                     <button class="btn btn-danger btn-delete-member">Xóa</button>
                                    </div>
                                  </div>`);
        $('.select-search').select2();
    })
    $('#frmConsiderProject').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "/api/update-level-project",
            method: "POST",
            data: form.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $('#modal_consider_project').modal('hide');
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Cập nhật cấp độ đề án thành công");
                    notification.push();
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        })
    })
    $('body').delegate('.btn-delete-member','click',function(){
        $(this).parents('.row').remove();
    })
    $('#frmAssign').submit(function(e){
        e.preventDefault();
        var form = $(this);
    $.ajax({
            url: "/api/send-assign",
            method: "POST",
            data: form.serialize(),
        success: function (response) {
                if (response.success == true) {
                    $('#modal_assign_project').modal('hide');
            var notifier = new Notifier();
                    var notification = notifier.notify("success", "Giao việc thành công");
            notification.push();
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
        }
            }
})
    })
    $('.history').click(function(){
        var progress = $(this).data('progress');
        $.ajax({
            url: "/api/get-history",
            method: "POST",
            data: {progress:progress,project_id:{{$record->id}}},
            success: function (response) {
                if (response.success == true) {
                    $('.content-history').html(response.html);
                    $('#modal_show_history').modal('show');
                }
            }
        })
    })
    ClassicEditor
            .create( document.querySelector( '#after_content' ) )
            .then( editor => {
                after_content = editor; // Save for later use.
            } )
            .catch( error => {
                console.error( error );
    } );
$('#frmReportProject').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "/api/report-project",
            method: "POST",
            data: form.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $('#modal_report_project').modal('hide');
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Gửi báo cáo thành công");
                    notification.push();
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        })
})
    $('#frmReasonUnapproved').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "/api/unapproved-project",
            method: "POST",
            data: form.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $('#modal_unapproved_project').modal('hide');
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Báo cáo đề án trả về thành công");
                    notification.push();
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        })
    })
    $('.change-approved').click(function(){
        $('#modal_change_approved').modal('show');
    })
    $('#frmChangeApproved').submit(function(e){
        e.preventDefault();
        var form = $(this);
        if($('.choose-change-approved').val() == ''){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn người duyệt đề án");
            notification.push();
        }else{
            $.ajax({
                url: "/api/change-member-approved",
                method: "POST",
                data: form.serialize(),
                success: function (response) {
                    if (response.success == true) {
                        $('#modal_change_approved').modal('hide');
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Chuyển người duyệt thành công");
                        notification.push();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            })
        }
    })
</script>
<script>
    /* 
Orginal Page: http://thecodeplayer.com/walkthrough/jquery-multi-step-form-with-progress-bar 

*/
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			scale = 1 - (1 - now) * 0.2;
			left = (now * 50)+"%";
			opacity = 1 - now;
			current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			scale = 0.8 + (1 - now) * 0.2;
			left = ((1-now) * 50)+"%";
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		easing: 'easeInOutBack'
	});
});

</script>
<script type="text/javascript">
    (function() {

        var initPhotoSwipeFromDOM = function(gallerySelector) {

                var parseThumbnailElements = function(el) {
                    var thumbElements = el.childNodes,
                        numNodes = thumbElements.length,
                        items = [],
                        el,
                        childElements,
                        thumbnailEl,
                        size,
                        item;

                    for(var i = 0; i < numNodes; i++) {
                        el = thumbElements[i];

                        // include only element nodes 
                        if(el.nodeType !== 1) {
                          continue;
                        }

                        childElements = el.children;

                        size = el.getAttribute('data-size').split('x');

                        // create slide object
                        item = {
                                        src: el.getAttribute('href'),
                                        w: parseInt(size[0], 10),
                                        h: parseInt(size[1], 10),
                                        author: el.getAttribute('data-author')
                        };

                        item.el = el; // save link to element for getThumbBoundsFn

                        if(childElements.length > 0) {
                          item.msrc = childElements[0].getAttribute('src'); // thumbnail url
                          if(childElements.length > 1) {
                              item.title = childElements[1].innerHTML; // caption (contents of figure)
                          }
                        }


                                var mediumSrc = el.getAttribute('data-med');
                        if(mediumSrc) {
                        size = el.getAttribute('data-med-size').split('x');
                        // "medium-sized" image
                        item.m = {
                                src: mediumSrc,
                                w: parseInt(size[0], 10),
                                h: parseInt(size[1], 10)
                        };
                        }
                        // original image
                        item.o = {
                                src: item.src,
                                w: item.w,
                                h: item.h
                        };

                        items.push(item);
                    }

                    return items;
                };

                // find nearest parent element
                var closest = function closest(el, fn) {
                    return el && ( fn(el) ? el : closest(el.parentNode, fn) );
                };

                var onThumbnailsClick = function(e) {
                    e = e || window.event;
                    e.preventDefault ? e.preventDefault() : e.returnValue = false;

                    var eTarget = e.target || e.srcElement;

                    var clickedListItem = closest(eTarget, function(el) {
                        return el.tagName === 'A';
                    });

                    if(!clickedListItem) {
                        return;
                    }

                    var clickedGallery = clickedListItem.parentNode;

                    var childNodes = clickedListItem.parentNode.childNodes,
                        numChildNodes = childNodes.length,
                        nodeIndex = 0,
                        index;

                    for (var i = 0; i < numChildNodes; i++) {
                        if(childNodes[i].nodeType !== 1) { 
                            continue; 
                        }

                        if(childNodes[i] === clickedListItem) {
                            index = nodeIndex;
                            break;
                        }
                        nodeIndex++;
                    }

                    if(index >= 0) {
                        openPhotoSwipe( index, clickedGallery );
                    }
                    return false;
                };

                var photoswipeParseHash = function() {
                        var hash = window.location.hash.substring(1),
                    params = {};

                    if(hash.length < 5) { // pid=1
                        return params;
                    }

                    var vars = hash.split('&');
                    for (var i = 0; i < vars.length; i++) {
                        if(!vars[i]) {
                            continue;
                        }
                        var pair = vars[i].split('=');  
                        if(pair.length < 2) {
                            continue;
                        }           
                        params[pair[0]] = pair[1];
                    }

                    if(params.gid) {
                        params.gid = parseInt(params.gid, 10);
                    }

                    return params;
                };

                var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                    var pswpElement = document.querySelectorAll('.pswp')[0],
                        gallery,
                        options,
                        items;

                        items = parseThumbnailElements(galleryElement);

                    // define options (if needed)
                    options = {

                        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                        getThumbBoundsFn: function(index) {
                            // See Options->getThumbBoundsFn section of docs for more info
                            var thumbnail = items[index].el.children[0],
                                pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                rect = thumbnail.getBoundingClientRect(); 

                            return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                        },

                        addCaptionHTMLFn: function(item, captionEl, isFake) {
                                        if(!item.title) {
                                                captionEl.children[0].innerText = '';
                                                return false;
                                        }
                                        captionEl.children[0].innerHTML = item.title +  '<br/><small>Photo: ' + item.author + '</small>';
                                        return true;
                        },

                    };


                    if(fromURL) {
                        if(options.galleryPIDs) {
                                // parse real index when custom PIDs are used 
                                // https://photoswipe.com/documentation/faq.html#custom-pid-in-url
                                for(var j = 0; j < items.length; j++) {
                                        if(items[j].pid == index) {
                                                options.index = j;
                                                break;
                                        }
                                }
                            } else {
                                options.index = parseInt(index, 10) - 1;
                            }
                    } else {
                        options.index = parseInt(index, 10);
                    }

                    // exit if index not found
                    if( isNaN(options.index) ) {
                        return;
                    }



                        var radios = document.getElementsByName('gallery-style');
                        for (var i = 0, length = radios.length; i < length; i++) {
                            if (radios[i].checked) {
                                if(radios[i].id == 'radio-all-controls') {

                                } else if(radios[i].id == 'radio-minimal-black') {
                                        options.mainClass = 'pswp--minimal--dark';
                                        options.barsSize = {top:0,bottom:0};
                                                options.captionEl = false;
                                                options.fullscreenEl = false;
                                                options.shareEl = false;
                                                options.bgOpacity = 0.85;
                                                options.tapToClose = true;
                                                options.tapToToggleControls = false;
                                }
                                break;
                            }
                        }

                    if(disableAnimation) {
                        options.showAnimationDuration = 0;
                    }

                    // Pass data to PhotoSwipe and initialize it
                    gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);

                    // see: http://photoswipe.com/documentation/responsive-images.html
                        var realViewportWidth,
                            useLargeImages = false,
                            firstResize = true,
                            imageSrcWillChange;

                        gallery.listen('beforeResize', function() {

                                var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
                                dpiRatio = Math.min(dpiRatio, 2.5);
                            realViewportWidth = gallery.viewportSize.x * dpiRatio;


                            if(realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200 ) {
                                if(!useLargeImages) {
                                        useLargeImages = true;
                                        imageSrcWillChange = true;
                                }

                            } else {
                                if(useLargeImages) {
                                        useLargeImages = false;
                                        imageSrcWillChange = true;
                                }
                            }

                            if(imageSrcWillChange && !firstResize) {
                                gallery.invalidateCurrItems();
                            }

                            if(firstResize) {
                                firstResize = false;
                            }

                            imageSrcWillChange = false;

                        });

                        gallery.listen('gettingData', function(index, item) {
                            if( useLargeImages ) {
                                item.src = item.o.src;
                                item.w = item.o.w;
                                item.h = item.o.h;
                            } else {
                                item.src = item.m.src;
                                item.w = item.m.w;
                                item.h = item.m.h;
                            }
                        });

                    gallery.init();
                };

                // select all gallery elements
                var galleryElements = document.querySelectorAll( gallerySelector );
                for(var i = 0, l = galleryElements.length; i < l; i++) {
                        galleryElements[i].setAttribute('data-pswp-uid', i+1);
                        galleryElements[i].onclick = onThumbnailsClick;
                }

                // Parse URL and open gallery if it contains #&pid=3&gid=1
                var hashData = photoswipeParseHash();
                if(hashData.pid && hashData.gid) {
                        openPhotoSwipe( hashData.pid,  galleryElements[ hashData.gid - 1 ], true, true );
                }
        };

        initPhotoSwipeFromDOM('.demo-gallery');


})();

</script>
@stop