@extends('frontend.home.home')
@section('content')
<div class="content project-content">
    <h4 style="margin-top:25px">{{trans('base.manage_position')}}</h4>
    <div class="form-create">
        <form id='postData' data-model='position'>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 text-right">{{trans('base.Position')}}</label>
                <div class="col-sm-6">
                    <input class="form-control" name="name" type="text"> 
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-5"></div>
                <div class="col-md-7 col-sm-8 col-sm-offset-4 btn-group button-group">
                    <a href="/home" class="btn btn-sm btn-default">{{trans('base.Back')}}</a>
                    <button type="button" class="btn btn-sm btn-primary btn-add">{{trans('base.Add')}}</button>
                </div>
            </div>
        </form>
        <table class="table datatable-basic table-bordered" id="dataTables-example" style="margin:0px">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{trans('base.Position')}}</th>
                    <th>{{trans('base.Action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($records as $key=>$record)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$record->name}}</td>
                    <td>
                        <a href="javascript:void(0)" title="{!! trans('base.edit') !!}" class="success edit-data" data-id='{{$record->id}}' data-model='position'><i class="icon-pencil"></i></a>
                        <form action="{!! route('frontend.position.destroy', ['id' => $record->id]) !!}" method="POST" style="display: inline-block;margin:0px;">
                            {!! method_field('DELETE') !!}
                            {!! csrf_field() !!}
                             <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-id='{{$record->id}}' data-action="delete_position">
                                <i class="icon-close2"></i>
                            </a>                
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modal_check_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">Danh sách thành viên chưa đổi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <h5>Bạn cần đổi chức vụ các thành viên trước khi xóa </h5>
                    <div id='list_member_check'>
                        
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $('[data-action="delete_position"]').click(function (e) {
        var elm = this;
        var id = $(this).data('id');
        $.ajax({
            url:'/api/check-delete-position',
            method:'POST',
            data:{id:id},
            success: function(response){
                if(response.success == 'false'){
                    $('#list_member_check').html(response.html);
                    $('#modal_check_delete').modal('show');
                }else{
                     bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
                        if (result === true) {
                            $(elm).parents('form').submit();
                        }
                    });
                }
            }
        })
    });
</script>
@stop