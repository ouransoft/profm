@extends('frontend.home.home')
@section('content')
<div class="project-content" style="padding: 15px;">
    <div class="project-content">
        <div class="d-flex mt-2">
            <div class="d-flex mr-2 rounded" style="background: rgb(163, 163, 163); width: 50%">
                @if(count($rank_team) > 0)
                <div class="col-md-7 px-0">
                    <h2 class="ml-3 mb-3 text-light font-weight-bold">{{$rank_team[0]->name}}</h2>
                    <div class="ml-3 d-flex">
                        <i class="icon-users text-light pr-3 ml-2" style="font-size: 26px"></i>
                        <h3 class="mr-5 text-light">{{\App\Member::where('department_id',$rank_team[0]->id)->count()}}</h3>
                    </div>
                </div>
                <div class="col-md-5 d-flex align-items-center justify-content-center">
                    <h1 class="text-light text-center" style="font-size: 66px; z-index:100">{{$rank_team[0]->count}}</h1>
                    <div style="position: absolute; right: 0; bottom: 0;">
                        <i class="icon-trophy2 text-muted pr-3 ml-2" style="font-size: 100px"></i>
                    </div>
                </div>
                @else
                <div class="col-md-7 px-0">
                    <h2 class="ml-3 mb-3 text-light font-weight-bold">TEAM</h2>
                    <div class="ml-3 d-flex">
                        <i class="icon-users text-light pr-3 ml-2" style="font-size: 26px"></i>
                        <h3 class="mr-5 text-light">0</h3>
                        <i class="icon-newspaper text-light pr-3 ml-2" style="font-size: 26px"></i>
                        <h3 class="text-light">0</h3>
                    </div>
                </div>
                <div class="col-md-5 d-flex align-items-center justify-content-center">
                    <h1 class="text-light text-center" style="font-size: 66px; z-index:100">0</h1>
                    <div style="position: absolute; right: 0; bottom: 0;">
                        <i class="icon-trophy2 text-muted pr-3 ml-2" style="font-size: 100px"></i>
                    </div>
                </div>
                @endif
            </div>
            <div class="ml-2 rounded position-relative" style="background: rgb(54, 175, 49); width: 50%">
                <div style="position: absolute; right: 0; bottom: 0;">
                    <i class="icon-medal2 pr-3 ml-2" style="font-size: 100px; color:rgba(80, 80, 80, 0.623)"></i>
                </div>
                @foreach($rank_quarter as $key=>$val)
                <div class="d-flex py-2">
                    <div class="col-md-8 d-flex">
                        <span class="text-light" style="font-size: 24px; line-height: normal">{{++$key}}</span>
                        <i class="icon-user text-light pr-3 ml-2" style="font-size: 24px"></i>
                        <div class="d-flex flex-column">
                            <h6 class="mb-0 text-light">{{$val->full_name}} ({{$val->login_id}})</h6>
                        </div>
                    </div>
                    <div class="col-md-4 department-action d-flex">
                        <a class="d-flex justify-content-center mr-3 text-light">
                            <i class="icon-calendar text-light pr-3 ml-2" style="font-size: 24px"></i>
                            <span style="font-size: 24px;line-height: 26px;">{{$val->count}}</span>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Bộ lọc tìm kiếm
                        <i class="icon-circle-down2"></i>
                    </button>
                </h5>
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                <div class="card-body">
                    <form id="search_project" method="GET" action="{{route('frontend.member.index')}}">
                        <input type="hidden" name="keyword_project" value="">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row form-group">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control search-full_name" @if(isset($_GET['full_name'])) value="{{$_GET['full_name']}}" @endif name="full_name" value="" placeholder="Tên thành viên">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control search-status select2" name="department_id" data-placeholder="Chọn phòng ban">
                                            <option></option>
                                            {!!$department_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control search-level select-search" name="level" data-placeholder="Chọn chức vụ">
                                            {!!$position_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="row">
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary btn-search">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="table-content table-member-content" style="margin-top: 0px;">
            <div class="control-table row">
                <div class="top-left-sidebar col-md-4">
                    <ul class="action-member pl-0">
                        <li><input type="checkbox" id="check_all" value="all"></li>
                        <li><a href="javascript:void(0)" class="export-member" title="Xuất file excel"><img src="/assets2/img/excel.png"></a></li>
                        <li><a href="javascript:void(0)" class="modal-create-member" title="Tạo mới thành viên"><i class="icon-user-plus" style="font-size: 30px;color: green;"></i></a></li>
                        <li><a href="javascript:void(0)" class="modal-edit-member" title="Chỉnh sửa thông tin thành viên"><i class="icon-pencil" style="font-size: 24px;color: #333;"></i></a></li>
                        <li><a href="javascript:void(0)" class="remove-member" title="Xóa thành viên"><i class="icon-bin" style="font-size: 24px;color: red;"></i></a></li>
                    </ul>
                </div>

                <div class="col-md-3 text-center" id="show_page_member" style="display:inline-block">
                    @if(count($records) > 0)
                    <p id="page_member" class="pl-0" style="display: inline-block">{{$records->firstItem()}}-{!!$records->lastItem()!!} {{trans('base.among')}} {!!$records->total()!!}</p>
                    <a style="padding-top:15px" href="{{$records->url($records->currentPage() - 1)}}" class='forward-page-member'><img src="{!!asset('assets2/img/left-arrow.png')!!}"></a>
                    <a style="padding-top:15px" href="@if($records->lastPage() == $records->currentPage()){{$records->url($records->currentPage())}} @else {{$records->url($records->currentPage() + 1)}} @endif" class='next-page-member'><img src="{!!asset('assets2/img/forward.png')!!}"></a>
                     @endif
                </div>

                <div class="col-md-5 text-right" style="bottom: -12px;">
                    <a href="{{route('frontend.member.index')}}" class="list-member rounded border border-dark"><i class="icon-user-check"></i> <span style="font-size: 13px;">D.S THÀNH VIÊN</span></a>
                    <a href="{{route('frontend.member.history')}}" class="list-member-remove bg-light rounded border border-dark"><i class="icon-user-minus"></i> <span style="font-size: 13px;">D.S THÀNH VIÊN ĐÃ XÓA</span></a>
                </div>
            </div>
            <table class="table table-member" style="margin-top:5px;">
                <tbody id="records-member">
                    @foreach($records as $key=>$record)
                    <tr class="py-0">
                        <td class="border-0 pt-0 pb-1"><input type="checkbox" value="{{$record->id}}" name="member_id" class="check"></td>
                        <td class="border-0 pt-0 pb-1">
                            <div class="row d-flex rounded py-1" style="background: rgb(218, 218, 218); z-index:100">
                                <div class="col-md-6 d-flex">
                                    <span class="icons icons-user-dark" style="float: none;margin-right: 5px; width: 32px !important"></span>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-0">{{$record->full_name}}</h6>
                                        <span style="line-height: normal">{{$record->login_id}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right department-action d-flex justify-content-end">
                                    <a class="d-flex justify-content-center mr-3" style="font-size: 22px"><span class="icons icons-todo-list" title="Số lượng công việc" style="float: none;margin-right: 5px; width: 26px !important"></span>{{count($record->membertodo)}}</a>
                                    <a class="d-flex justify-content-center mr-3" style="font-size: 22px"><span class="icons icons-quarter" title="Số lượng lịch trình" style="float: none;margin-right: 5px; width: 26px !important"></span>{{count($record->schedule)}}</a>
                                    <a class="d-flex justify-content-center mr-3" style="font-size: 22px"><span class="icons icons-project" title="Số lượng đề án" style="float: none;margin-right: 5px; width: 26px !important"></span>{{count($record->project)}}</a>
                                    <a class=" staff-m d-flex justify-content-center mr-3" style="font-size: 22px;margin-top:15px;"><i class="icon-circle-down2"></i></a>
                                </div>
                            </div>
                            <div class="extend-staff row border border-top-0 border-right border-left-0 border-bottom rounded-bottom bg-light position-relative" style="height: 0; overflow: hidden">
                                
                                <div>
                                <h5 class="ml-2 my-4">Thông tin chi tiết</h5>
                                <p style="margin-left:15px">- Họ tên: {{$record->full_name}}</p>
                                <p style="margin-left:15px">- ID: {{$record->login_id}}</p>
                                <p style="margin-left:15px">- Chức vụ: {{$record->position ? $record->position->name : ''}}</p>
                                <p style="margin-left:15px">- Nhóm: {{$record->department ? $record->department->name : ''}}</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>       
<div class="modal fade" id="modal_create_member" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="padding-bottom:0px">
            <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Thêm</span> thành viên</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="container-fluid">
              <form method="post" id='create_member' action='{{route('frontend.member.store')}}'>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <div class="row">
                  <div class="col-md-5">
                      <div class="form-group  row">
                          <lable class="col-md-12">Phòng</lable>
                          <div class='col-md-12 select-full'>
                            <select class="form-control select2" id="validate_department_id" name='department_id' data-placeholder='Tất cả'>
                                <option></option>
                                {!!$department_html!!}
                            </select>
                            <div class="invalid-feedback" id="error_department"></div>
                          </div>
                      </div>
                      <div class="form-group  row">
                            <lable class="col-md-12 ">Chức vụ</lable>
                            <div class="col-md-12 select-full">
                                <select class="form-control select" name='position_id' data-placeholder="Tất cả">
                                    {!!$position_html!!}
                                </select>
                            </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-12">ID*</lable>
                          <div class="col-md-12">
                            <input class="form-control check-ID" type="text" name='login_id' id="validate_login_id" required="">
                            <div class="invalid-feedback" id="error_login"></div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group  row">
                          <lable class="col-md-12">Level*</lable>
                          <div class="col-md-12">
                              <select class="form-control select" data-placeholder="Tất cả" style="width:100%;" name='level' id="validate_level" required="">
                                    <option></option>
                                    <option value='1'>Nhân viên (Nộp đề án)</option>
                                    <option value='2'>Tổ trưởng (Duyệt lần 1)</option>
                                    <option value='3'>Trưởng phòng (Duyệt lần 2)</option>
                                    <option value='4'>Thư ký</option>
                                    <option value='5'>Admin</option>
                            </select>
                          </div>
                          <div class="invalid-feedback" id="error_level"></div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-12">Họ tên*</lable>
                          <div class="col-md-12">
                            <input class="form-control" type="text" name='full_name' id="validate_full_name" required="">
                            <div class="invalid-feedback" id="error_full_name"></div>
                          </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-12">Mật khẩu*</lable>
                          <div class="col-md-12 ">
                            <input class="form-control password" type="password" name='password' id="validate_password" required="">
                            <div class="invalid-feedback" id="error_password"></div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-3">
                       <div class="image-avatar" style="background-image:url('{!!asset('assets2/img/background-avatar.jpg')!!}')">
                       <div id="yourBtn" onclick="getFile()">Chọn ảnh</div>
                       <div style='height: 0px;width: 0px; overflow:hidden;'><input id="upfile" type="file" value="upload" onchange="sub(this)" /></div>
                       <input type='hidden' class='image_data' name='avatar'>
                      </div>
                  </div>
              </div>
              <div class='row'>
                  <div class='col-md-12 form-group'>
                       <label>Ghi chú</label>
                       <textarea class='form-control' rows="6" name='note'></textarea>
                  </div>
              </div>
            </form>
          </div>
      </div>
      <div class="modal-footer">
        <div class='row' style="width:100%;">
            <div class='col-md-6'>
                <button type="button" class="btn btn-primary submit-create-member" style="color:#fff;height: 40px;"><i class='icon-user-plus' style="color: #fff"></i>Thêm nhân viên</button>
            </div>
            <div class='col-md-6'>
                <div class='form-upload' style="display:flex;">
                    <div style="width: 70%;">Upload file DS định dạng *xlsx <a href="/public/file/List member.xlsx"><p>Download file mẫu</p></a></div>
                     <div id="upload_excel" onclick="getFile1()">UPLOAD</div>
                     <div style='height: 0px;width: 0px; overflow:hidden;'><input id="upfile1" type="file" /></div>
                 </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_edit_member" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="padding-bottom:0px">
            <h5 class="modal-title" id="exampleModalLabel"><span class="orange">Sửa</span> thành viên</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="container-fluid">
              <form method="post" id='update_member'>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
              <input type="hidden" name="id" id="update_member_id" />
                <div class="row">
                  <div class="col-md-5">
                      <div class="form-group row">
                          <lable class="col-md-12">Phòng</lable>
                          <div class="col-md-12 select-full">
                            <select class="form-control edit-department" name='department_id' data-placeholder='Tất cả' >
                            </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-12">Chức vụ</lable>
                          <div class="col-md-12 select-full">
                            <select class="form-control edit-position select-search" name='position_id' data-placeholder="Tất cả">
                            </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-12">ID</lable>
                          <div class='col-md-12'>
                            <input class="form-control edit-login_id" type="text" name='login_id'>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                       <div class="form-group row">
                          <lable class="col-md-12">Level</lable>
                          <div class='col-md-12'>
                                <select class="form-control edit-level select2" name='level'>
                                </select>
                           </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-12">Họ tên</lable>
                          <div class='col-md-12'>
                                <input class="form-control edit-full_name" type="text" name='full_name'>
                          </div>
                      </div>
                      <div class="form-group row">
                          <lable class="col-md-12">Mật khẩu</lable>
                          <div class='col-md-12'>
                                <input class="form-control edit-password" type="password" name='password'>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-3">
                       <div class="image-avatar" style="background-image:url('{!!asset('assets2/img/background-avatar.jpg')!!}')">
                       <div id="yourBtn" onclick="getFile()">Chọn ảnh</div>
                       <div style='height: 0px;width: 0px; overflow:hidden;'><input id="upfile" type="file" value="upload" onchange="sub(this)" /></div>
                       <input type='hidden' class='image_data' name='avatar'>
                      </div>
                  </div>
              </div>
              <div class='row'>
                  <div class='col-md-12 form-group'>
                       <label>Ghi chú</label>
                       <textarea class='form-control edit-note' rows="6" name='note'></textarea>
                  </div>
              </div>
            </form>
          </div>
      </div>
      <div class="modal-footer">
        <div class='row' style="width:100%;">
            <div class='col-md-2' id="forward_member">
                <!--<a type="button" class="btn btn-primary forward-member" style="background: orange;color:#fff"><i class='icon-arrow-left7' style="color: #fff"></i> Foward</a>-->
            </div>
            <div class='col-md-7 text-center'>
                <button type="button" class="btn btn-primary submit-edit-member"><i class='icon-pencil' style="color: #fff;margin-right: 8px;"></i>Sửa thông tin</button>
            </div>
            <div class='col-md-2' id="next_member">
                <!--<a type="button" class="btn btn-primary next-member" style="background: orange;color:#fff">Next <i class='icon-arrow-right7' style="color: #fff"></i></a>-->
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@stop
@section('script')
@parent
@if (Session::has('error'))
<script>
    var notifier = new Notifier();
    var notification = notifier.notify("warning", "{{session('error')}}");
    notification.push();
</script>
@endif
<script>
    function formatResult(node) {
        var level = 0;
        if(node.element !== undefined){
          level = (node.element.className);
          if(level.trim() !== ''){
            level = (parseInt(level.match(/\d+/)[0]));
          }
        }
        var $result = $('<span style="padding-left:' + (20 * level) + 'px;">' + node.text + '</span>');
        return $result;
     };
    $('body').delegate('.staff-m','click',function (){
        if($(this).hasClass('active')){
            $('.staff-m').removeClass('active')
            $('.extend-staff').removeClass('active');
        }else{
            $('.staff-m').removeClass('active')
            $('.extend-staff').removeClass('active');
            $(this).addClass('active');
            $(this).parents('td').find('.extend-staff').addClass('active');
        }
    })
    $('#check_all').change(function () {
        if ($(this).is(":checked")) {
            $('.check').each(function () {
                $(this).prop('checked', true);
            });
        } else {
            $('.check').each(function () {
                $(this).prop('checked', false);
            });
        }
    });
    $('body').delegate('.check','change',function(){
        var member_id = [];
        $('.check').each(function () {
            if ($(this).is(':checked')) {
                $(this).parent().parent().attr('style','background:#d8d8d8');
                member_id.push($(this).val());
            }else{
                $(this).parent().parent().attr('style','');
            }
            $('#check_all').attr('value',member_id.join(','));
        })
    })
    $('body').delegate('.modal-edit-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
        }
        $.ajax({
            url: '/api/edit-list-member',
            method: 'POST',
            data: {member_id: member_id},
            dataType: 'json',
            success: function (response) {
                    $('.edit-part').html(response.part_html);
                    $('.edit-department').html(response.department_html);
                    $('.edit-groups').html(response.groups_html);
                    $('.edit-team').html(response.team_html);
                    $('.edit-position').html(response.position_html);
                    $('.edit-login_id').val(response.login_id);
                    $('.edit-email').val(response.email);
                    $('.edit-level').html(response.level_html);
                    $('#update_member_id').val(response.id);
                    $('.edit-full_name').val(response.full_name);
                    $('.edit-note').val(response.note)
                    if(response.avatar !== null){
                        $('.image-avatar').attr('style','background-image:url('+response.avatar+')');
                        $('.image_data').val(response.avatar);
                    }else{
                        $('.image-avatar').attr('style','background-image:url(/public/assets2/img/background-avatar.jpg)');
                        $('.image_data').val('');
                    }
                    if(response.next_id != null){
                        $('#next_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.next_id+' style="background: orange;color:#fff">Next <i class="icon-arrow-right7" style="color: #fff"></i></a>')
                    }
                    $(".edit-department").select2({
                        placeholder: 'Select an option',
                        width: "300px",
                        templateResult: formatResult,
                    });
                    $('#modal_edit_member').modal('show');
            }
        });
    });
    $('body').delegate('.remove-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
        }
        var elm = this;
        bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
            if (result === true) {
                $.ajax({
                    url: '/api/remove-member',
                    method: 'POST',
                    data: {member_id: member_id},
                    success: function (response) {
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Xóa thành công");
                        notification.push();
                        setTimeout(function(){
                            window.location.href = window.location.href
                        }, 2000);
                    }
                });
            }
        });
    })
    $('body').delegate('.detail-member','click',function(){
            $.ajax({
            url: '/api/edit-detail-member',
            method: 'POST',
            data: {member_id: $(this).data('member_id')},
            dataType: 'json',
            success: function (response) {
                    $('.edit-part').html(response.part_html);
                    $('.edit-department').html(response.department_html);
                    $('.edit-groups').html(response.groups_html);
                    $('.edit-team').html(response.team_html);
                    $('.edit-position').html(response.position_html);
                    $('.edit-login_id').val(response.login_id);
                    $('#update_member_id').val(response.id);
                    $('.edit-level').html(response.level_html);
                    $('.edit-full_name').val(response.full_name);
                    $('.edit-email').val(response.email);
                    $('.edit-note').val(response.note)
                   if(response.avatar !== null){
                        $('.image-avatar').attr('style','background-image:url('+response.avatar+')');
                        $('.image_data').val(response.avatar);
                    }else{
                        $('.image-avatar').attr('style','background-image:url(/public/assets2/img/background-avatar.jpg)');
                        $('.image_data').val('');
                    }
                    if(response.next !== null){
                        $('#next_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.next+' style="background: orange;color:#fff">Next <i class="icon-arrow-right7" style="color: #fff"></i></a>');
                    }else{
                        $('#next_member').html('');
                    }
                    if(response.prev !== null){
                        $('#forward_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.prev+' style="background: orange;color:#fff"><i class="icon-arrow-left7" style="color: #fff"></i> Foward</a>');
                    }else{
                        $('#forward_member').html('');
                    }
                    $('#modal_edit_member').modal('show');
            }
        });
    });
    $('body').delegate('.restore-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/restore-member',
                method: 'POST',
                data: {member_id: member_id},
                success: function (response) {
                    $('#records-member').html(response.html);
                    $('#show_page_member').html(response.show_page);
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Khôi phục thành viên thành công");
                    notification.push();
                }
            });
        }
    })
    $('body').delegate('.export-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/export-member',
                method: 'POST',
                data: {member_id: member_id},
                success: function (response) {
                    window.location.href=response.href;
                }
            });
        }
    })
    $('#upfile1').change(function(){
        var form_data = new FormData();
        form_data.append("files", $(this)[0].files[0]);
        var form = $(this);
        $.ajax({
                type: "POST",
                url: '/api/import-member',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == 'true'){
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Thêm thành viên thành công");
                        notification.push();
                        setTimeout(function(){ location.reload(); }, 2000);
                    }else{
                        var notifier = new Notifier();
                        var notification = notifier.notify("info", response.message);
                        notification.push();
                        setTimeout(function(){ location.reload(); }, 4000);
                    }
                }
            });
    })
    $('.check-ID').blur(function(){
        var id=$(this).val();
        $.ajax({
                type: "POST",
                url: '/api/checkid-member',
                data: {id:id},
                success: function(response){
                        if(response.success == 'true'){
                           $('#error_login').html('ID login đã tồn tại')
                        }else{
                           $('#error_login').html('');
                        }
                    }
                });
    });

  $(".select2").select2({
    placeholder: 'Select an option',
    width: "300px",
    templateResult: formatResult,
  });
</script>

@stop
