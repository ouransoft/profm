@extends('frontend.home.home')
@section('content')
<div class="project-content" style="padding: 15px;">
    <div class="project-content">
    <div class="d-flex mt-2">
            <div class="d-flex mr-2 rounded" style="background: rgb(163, 163, 163); width: 50%">
                @if(count($rank_team) > 0)
                <div class="col-md-7 px-0">
                    <h2 class="ml-3 mb-3 text-light font-weight-bold">{{$rank_team[0]->name}}</h2>
                    <div class="ml-3 d-flex">
                        <i class="icon-users text-light pr-3 ml-2" style="font-size: 26px"></i>
                        <h3 class="mr-5 text-light">{{\App\Member::where('department_id',$rank_team[0]->id)->count()}}</h3>
                    </div>
                </div>
                <div class="col-md-5 d-flex align-items-center justify-content-center">
                    <h1 class="text-light text-center" style="font-size: 66px; z-index:100">{{$rank_team[0]->count}}</h1>
                    <div style="position: absolute; right: 0; bottom: 0;">
                        <i class="icon-trophy2 text-muted pr-3 ml-2" style="font-size: 100px"></i>
                    </div>
                </div>
                @else
                <div class="col-md-7 px-0">
                    <h2 class="ml-3 mb-3 text-light font-weight-bold">TEAM</h2>
                    <div class="ml-3 d-flex">
                        <i class="icon-users text-light pr-3 ml-2" style="font-size: 26px"></i>
                        <h3 class="mr-5 text-light">0</h3>
                        <i class="icon-newspaper text-light pr-3 ml-2" style="font-size: 26px"></i>
                        <h3 class="text-light">0</h3>
                    </div>
                </div>
                <div class="col-md-5 d-flex align-items-center justify-content-center">
                    <h1 class="text-light text-center" style="font-size: 66px; z-index:100">0</h1>
                    <div style="position: absolute; right: 0; bottom: 0;">
                        <i class="icon-trophy2 text-muted pr-3 ml-2" style="font-size: 100px"></i>
                    </div>
                </div>
                @endif
            </div>
            <div class="ml-2 rounded position-relative" style="background: rgb(54, 175, 49); width: 50%">
                <div style="position: absolute; right: 0; bottom: 0;">
                    <i class="icon-medal2 pr-3 ml-2" style="font-size: 100px; color:rgba(80, 80, 80, 0.623)"></i>
                </div>
                @foreach($rank_quarter as $key=>$val)
                <div class="d-flex py-2">
                    <div class="col-md-8 d-flex">
                        <span class="text-light" style="font-size: 24px; line-height: normal">{{++$key}}</span>
                        <i class="icon-user text-light pr-3 ml-2" style="font-size: 24px"></i>
                        <div class="d-flex flex-column">
                            <h6 class="mb-0 text-light">{{$val->full_name}} ({{$val->login_id}})</h6>
                        </div>
                    </div>
                    <div class="col-md-4 department-action d-flex">
                        <a class="d-flex justify-content-center mr-3 text-light">
                            <i class="icon-calendar text-light pr-3 ml-2" style="font-size: 24px"></i>
                            <span style="font-size: 24px;line-height: 26px;">{{$val->count}}</span>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Bộ lọc tìm kiếm
                        <i class="icon-circle-down2"></i>
                    </button>

                </h5>
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                <div class="card-body">
                    <form id="search_project" method="GET" action="{{route('frontend.member.history')}}">
                        <input type="hidden" name="keyword_project" value="">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row form-group">
                                    <div class="col-md-12 select-full">
                                        <input class="form-control search-full_name" name="full_name" value="" placeholder="Tên thành viên">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control search-status select2" name="department_id" data-placeholder="Chọn phòng ban">
                                            <option></option>
                                            {!!$department_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row form-group">
                                    <div class="col-md-12 select-full">
                                        <select class="form-control search-level select-search" name="level" data-placeholder="Chọn chức vụ">
                                            {!!$position_html!!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="row">
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-primary btn-search">Tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="table-content table-member-content" style="margin-top:0px">
            <div class="control-table row">
                <div class="top-left-sidebar col-md-4 text-left">
                    <ul class="action-member pl-0">  
                        <li><input type="checkbox" id="check_all" value="all"></li>
                        <li><a href="#"><img src="/assets2/img/excel.png"></a></li>
                        <li><a href="javascript:void(0)" class="restore-member"><i class="icon-undo2" style="font-size: 30px;color: green;"></i></a></li>
                    </ul>
                </div>

                <div class="col-md-2 text-center" id="show_page_member" style="display:flex">
                    @if(count($records) > 0)
                    <p id="page_member" class="pl-0">{{$records->firstItem()}}-{!!$records->lastItem()!!} {{trans('base.among')}} {!!$records->total()!!}</p>
                    <a style="padding-top:15px" href="{{$records->url($records->currentPage() - 1)}}" class='forward-page-member'><img src="{!!asset('assets2/img/left-arrow.png')!!}"></a>
                    <a style="padding-top:15px" href="@if($records->lastPage() == $records->currentPage()){{$records->url($records->currentPage())}} @else {{$records->url($records->currentPage() + 1)}} @endif" class='next-page-member'><img src="{!!asset('assets2/img/forward.png')!!}"></a>
                     @endif
                </div>

                <div class="col-md-6 text-right" style="bottom: -12px;">
                    <a href="{{route('frontend.member.index')}}" class="list-member bg-light rounded border border-dark"><i class="icon-user-check"></i> <span style="font-size: 13px;">D.S THÀNH VIÊN</span></a>
                    <a href="{{route('frontend.member.history')}}" class="list-member-remove rounded border border-dark"><i class="icon-user-minus"></i> <span style="font-size: 13px;">D.S THÀNH VIÊN ĐÃ XÓA</span></a>
                </div>
            </div>
            <table class="table table-member" style="margin-top:5px;">
                <tbody id="records-member">
                @foreach($records as $key=>$record)
                    <tr class="py-0">
                        <td class="border-0 pt-0 pb-1"><input type="checkbox" value="{{$record->id}}" name="member_id" class="check"></td>
                        <td class="border-0 pt-0 pb-1">
                            <div class="row d-flex rounded py-1" style="background: rgb(218, 218, 218); z-index:100">
                                <div class="col-md-6 d-flex">
                                    <span class="icons icons-user-dark" style="float: none;margin-right: 5px; width: 32px !important"></span>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-0">{{$record->full_name}}</h6>
                                        <span style="line-height: normal">{{$record->login_id}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right department-action d-flex justify-content-end">
                                    <a class="d-flex justify-content-center mr-3" style="font-size: 22px"><span class="icons icons-todo-list" title="Số lượng công việc" style="float: none;margin-right: 5px; width: 26px !important"></span>{{count($record->membertodo)}}</a>
                                    <a class="d-flex justify-content-center mr-3" style="font-size: 22px"><span class="icons icons-quarter" title="Số lượng lịch trình" style="float: none;margin-right: 5px; width: 26px !important"></span>{{count($record->schedule)}}</a>
                                    <a class="d-flex justify-content-center mr-3" style="font-size: 22px"><span class="icons icons-project" title="Số lượng đề án" style="float: none;margin-right: 5px; width: 26px !important"></span>{{count($record->project)}}</a>
                                    <a class=" staff-m d-flex justify-content-center mr-3" style="font-size: 22px;margin-top:15px;"><i class="icon-circle-down2"></i></a>
                                </div>
                            </div>
                            <div class="extend-staff row border border-top-0 border-right border-left-0 border-bottom rounded-bottom bg-light position-relative" style="height: 0; overflow: hidden">
                                
                                <div>
                                <h5 class="ml-2 my-4">Thông tin chi tiết</h5>
                                <p style="margin-left:15px">- Họ tên: {{$record->full_name}}</p>
                                <p style="margin-left:15px">- ID: {{$record->login_id}}</p>
                                <p style="margin-left:15px">- Chức vụ: {{$record->position ? $record->position->name : ''}}</p>
                                <p style="margin-left:15px">- Vị trí: {{$record->department ? $record->department->name : ''}}</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>       
@stop
@section('script')
@parent
<script>
    function formatResult(node) {
        var level = 0;
        if(node.element !== undefined){
          level = (node.element.className);
          if(level.trim() !== ''){
            level = (parseInt(level.match(/\d+/)[0]));
          }
        }
        var $result = $('<span style="padding-left:' + (20 * level) + 'px;">' + node.text + '</span>');
        return $result;
     };
    $('.staff-m').click(function (){
        $('.extend-staff').toggleClass('active');
    })
    $('#check_all').change(function () {
        if ($(this).is(":checked")) {
            $('.check').each(function () {
                $(this).prop('checked', true);
            });
        } else {
            $('.check').each(function () {
                $(this).prop('checked', false);
            });
        }
    });
    $('body').delegate('.check','change',function(){
        var member_id = [];
        $('.check').each(function () {
            if ($(this).is(':checked')) {
                $(this).parent().parent().attr('style','background:#d8d8d8');
                member_id.push($(this).val());
            }else{
                $(this).parent().parent().attr('style','');
            }
            $('#check_all').attr('value',member_id.join(','));
        })
    })
    $('body').delegate('.modal-edit-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
        }
        $.ajax({
            url: '/api/edit-list-member',
            method: 'POST',
            data: {member_id: member_id},
            dataType: 'json',
            success: function (response) {
                    $('.edit-part').html(response.part_html);
                    $('.edit-department').html(response.department_html);
                    $('.edit-groups').html(response.groups_html);
                    $('.edit-team').html(response.team_html);
                    $('.edit-position').html(response.position_html);
                    $('.edit-login_id').val(response.login_id);
                    $('.edit-email').val(response.email);
                    $('.edit-level').html(response.level_html);
                    $('#update_member_id').val(response.id);
                    $('.edit-full_name').val(response.full_name);
                    $('.edit-note').val(response.note)
                    if(response.avatar !== null){
                        $('.image-avatar').attr('style','background-image:url('+response.avatar+')');
                        $('.image_data').val(response.avatar);
                    }else{
                        $('.image-avatar').attr('style','background-image:url(/public/assets2/img/background-avatar.jpg)');
                        $('.image_data').val('');
                    }
                    if(response.next_id != null){
                        $('#next_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.next_id+' style="background: orange;color:#fff">Next <i class="icon-arrow-right7" style="color: #fff"></i></a>')
                    }
                    $('#modal_edit_member').modal('show');
            }
        });
    });
    $('body').delegate('.remove-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
        }
        var elm = this;
        bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
            if (result === true) {
                $.ajax({
                    url: '/api/remove-member',
                    method: 'POST',
                    data: {member_id: member_id},
                    success: function (response) {
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Xóa thành công");
                        notification.push();
                        setTimeout(function(){
                            window.location.hrer = location.href;
                        }, 2000);
                    }
                });
            }
        });
    })
    $('body').delegate('.detail-member','click',function(){
            $.ajax({
            url: '/api/edit-detail-member',
            method: 'POST',
            data: {member_id: $(this).data('member_id')},
            dataType: 'json',
            success: function (response) {
                    $('.edit-part').html(response.part_html);
                    $('.edit-department').html(response.department_html);
                    $('.edit-groups').html(response.groups_html);
                    $('.edit-team').html(response.team_html);
                    $('.edit-position').html(response.position_html);
                    $('.edit-login_id').val(response.login_id);
                    $('#update_member_id').val(response.id);
                    $('.edit-level').html(response.level_html);
                    $('.edit-full_name').val(response.full_name);
                    $('.edit-email').val(response.email);
                    $('.edit-note').val(response.note)
                   if(response.avatar !== null){
                        $('.image-avatar').attr('style','background-image:url('+response.avatar+')');
                        $('.image_data').val(response.avatar);
                    }else{
                        $('.image-avatar').attr('style','background-image:url(/public/assets2/img/background-avatar.jpg)');
                        $('.image_data').val('');
                    }
                    if(response.next !== null){
                        $('#next_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.next+' style="background: orange;color:#fff">Next <i class="icon-arrow-right7" style="color: #fff"></i></a>');
                    }else{
                        $('#next_member').html('');
                    }
                    if(response.prev !== null){
                        $('#forward_member').html('<a type="button" class="btn btn-primary detail-member" data-member_id='+response.prev+' style="background: orange;color:#fff"><i class="icon-arrow-left7" style="color: #fff"></i> Foward</a>');
                    }else{
                        $('#forward_member').html('');
                    }
                    $('#modal_edit_member').modal('show');
            }
        });
    });
   
    $('body').delegate('.restore-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/restore-member',
                method: 'POST',
                data: {member_id: member_id},
                success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Khôi phục thành viên thành công");
                    notification.push();
                    setTimeout(function(){
                            window.location.href = window.location.href
                    }, 2000);
                }
            });
        }
    })
    $('body').delegate('.export-member','click',function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn thành viên trước khi thao tác");
            notification.push();
        }else{
            var member_id = 0;
            if($('#check_all').is(":checked")){
                member_id = 'all';
            }else{
                member_id = $('#check_all').attr('value');
            }
            $.ajax({
                url: '/api/export-member',
                method: 'POST',
                data: {member_id: member_id},
                success: function (response) {
                    window.location.href=response.href;
                }
            });
        }
    })
    $('.search-part,.search-department,.search-groups,.search-team').change(function(){
         $('#search_member').submit();
    })
    $("#search_member").submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
               type: "POST",
               url: '/api/search-member',
               data: form.serialize(),
               success: function(response){
                    if(response.success == true){
                        $('#records-member').html(response.html);
                        $('#show_page_member').html(response.show_page);
                    }
                }
        });
    });
    $('#upfile1').change(function(){
        var form_data = new FormData();
        form_data.append("files", $(this)[0].files[0]);
        var form = $(this);
        $.ajax({
                type: "POST",
                url: '/api/import-member',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == 'true'){
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Thêm thành viên thành công");
                        notification.push();
                        setTimeout(function(){ location.reload(); }, 2000);
                    }else{
                        var notifier = new Notifier();
                        var notification = notifier.notify("info", response.message);
                        notification.push();
                        setTimeout(function(){ location.reload(); }, 4000);
                    }
                }
            });
    })
    $('.check-ID').blur(function(){
        var id=$(this).val();
        $.ajax({
                type: "POST",
                url: '/api/checkid-member',
                data: {id:id},
                success: function(response){
                    if(response.success == 'true'){
                           $('#error_login').html('ID login đã tồn tại')
                        }
                    }
                });
    });
    $(".select2").select2({
    placeholder: 'Select an option',
    width: "300px",
    templateResult: formatResult,
  });
  $('body').delegate('.staff-m','click',function (){
        if($(this).hasClass('active')){
            $('.staff-m').removeClass('active')
            $('.extend-staff').removeClass('active');
        }else{
            $('.staff-m').removeClass('active')
            $('.extend-staff').removeClass('active');
            $(this).addClass('active');
            $(this).parents('td').find('.extend-staff').addClass('active');
        }
    })
</script>
@stop
