@extends('frontend.home.home')
@section('content')
<div class="content project-content">
    <h4>{{trans('base.Manage_typical_images')}}</h4>
    <div class="form-create">
        <form id='postData' data-model='slide'>
            <div class="form-group row">
                <label class="col-md-2 required control-label text-right text-semibold">{{trans('base.Title')}}</label>
                <div class="col-lg-9">
                    <input class='form-control' name='title'>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 required control-label text-right text-semibold" for="images">{{trans('base.Image')}}</label>
                <div class="col-lg-9 div-image">
                    <div class="file-input file-input-ajax-new">
                        <div class="input-group file-caption-main">
                            <div class="input-group-btn input-group-append">
                                <div tabindex="500" class="btn btn-primary btn-file"><i class="icon-folder-open"></i>&nbsp; <span class="hidden-xs">{{trans('base.Choose')}}</span>
                                    <input type="file" id="images" class="upload-image" multiple="multiple" name="file_upload[]" data-fouc="" data-type="{{\App\File::TYPE_SLIDE}}">
                                </div>
                            </div>
                            <div class="file-caption form-control kv-fileinput-caption" tabindex="500">
                            </div>
                        </div>
                        <div class="file-preview ">
                            <div class=" file-drop-zone">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="image-link" >
                    <input type="hidden" name="image" class="image-id">
                    <input type="hidden" class="image-name">
                </div>
            </div>
            <div class="form-group row" style="padding-top:10px;">
                <div class="col-md-5"></div>
                <div class="col-md-7 col-sm-8 col-sm-offset-4 btn-group button-group">
                    <a href="/home" class="btn btn-sm btn-default">{{trans('base.Back')}}</a>
                    <button type="button" class="btn btn-sm btn-primary btn-add">{{trans('base.Add')}}</button>
                </div>
            </div>
        </form>
        <table class="table datatable-basic table-bordered" id="dataTables-example" style="margin:0px">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{trans('base.Title')}}</th>
                    <th>{{trans('base.Action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($records as $key=>$record)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$record->title}}</td>
                    <td>
                        <a href="javascript:void(0)" title="{!! trans('base.edit') !!}" class="success edit-data" data-id='{{$record->id}}' data-model='slide'><i class="icon-pencil"></i></a>
                        <form action="{!! route('frontend.slide.destroy', ['id' => $record->id]) !!}" method="POST" style="display: inline-block;margin:0px;">
                            {!! method_field('DELETE') !!}
                            {!! csrf_field() !!}
                            <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-action="delete">
                                <i class="icon-close2"></i>
                            </a>              
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
@section('script')
@parent
@stop