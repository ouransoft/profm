@extends('frontend.home.home')
@section('content')
<link rel="stylesheet" href="{!!asset('assets2/dashboard/css/master_style.css')!!}">
<div class="content project-content mt-5">
    <div class="row">
        <div class="col-xl-4 col-md-6 col-12">
            <div class="small-box pull-up bg-info">
                <div class="inner">
                <h3 class="text-white">{{$total_schedule}}</h3>
                <p>Lịch trình</p>
                </div>
                <div class="icon">
                    <i class="icon-calendar text-white" style="font-size: 50px"></i>
                </div>
                <a href="#" class="small-box-footer"></a>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 col-12">
            <div class="small-box pull-up bg-warning">
                <div class="inner">
                  <h3 class="text-white">{{$total_todo}}</h3>
                  <p>Todo-list</p>
                </div>
                <div class="icon">
                    <i class="icon-list3 text-white" style="font-size: 50px"></i>
                </div>
                <a href="#" class="small-box-footer"></a>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 col-12">
            <div class="small-box pull-up bg-success">
                <div class="inner">
                  <h3 class="text-white">{{$total_project}}</h3>
                  <p>Đề án</p>
                </div>
                <div class="icon">
                    <i class="icon-list-unordered text-white" style="font-size: 50px"></i>
                </div>
                <a href="#" class="small-box-footer"></a>
            </div>
        </div>
        <div class="col-xl-4 col-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Biểu đồ thống kê đề án theo trạng thái</h4>
                </div>
                <div class="box-body p-0">
                    <div id="donut"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Biểu đồ thống kê đề án theo tháng</h4>
                </div>
                <div class="box-body p-0">
                    <div id="ticketoverview"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jQuery 3 -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js')}}"></script>

<!-- fullscreen -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/screenfull/screenfull.js')}}"></script>

<!-- jQuery ui -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/jquery-ui/jquery-ui.js')}}"></script>

<!-- popper -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/popper/dist/popper.min.js')}}"></script>

<!-- Slimscroll -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- FastClick -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>

<!-- Sparkline -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- apexcharts -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/apexcharts-bundle/irregular-data-series.js')}}"></script>
<script src="{{asset('assets2/dashboard/assets/vendor_components/apexcharts-bundle/dist/apexcharts.js')}}"></script>

<!-- This is data table -->
<script src="{{asset('assets2/dashboard/assets/vendor_components/datatable/datatables.min.js')}}"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('assets2/dashboard/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js')}}"></script>

<!-- Bx-code admin dashboard demo (This is only for demo purposes) -->
<script src="{{asset('assets2/dashboard/js/pages/dashboard-2.js')}}"></script>
<script>
    var optionDonut = {
        colors : ['#0055c9','#fc9208', '#7c096a'],
        chart: {
                type: 'donut',
                width: '100%'
        },
        dataLabels: {
              enabled: true,
        },
        plotOptions: {
              pie: {
                donut: {
                      size: '55%',
                },
                offsetY: 20,
              },
              stroke: {
                colors: undefined
              }
        },
        series: [{{implode(',',$status_project)}}],
        labels: ['Chờ duyệt', 'Trả về', 'Đã duyệt'],
        legend: {
              position: 'bottom',
              offsetY: 0
        }
    }
    var donut = new ApexCharts(
      document.querySelector("#donut"),
      optionDonut
    )
    donut.render();
    
   var options = {
        chart: {
            height: 395,
            type: 'bar',
        },
        colors : ['#4029a7','#128c80', '#f3000b'],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false,
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        series: [{
            name: 'Số lượng',
            data: [{{implode(',',$data_chart_bar)}}]
        }],
        xaxis: {
            categories: ['T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12'],
        },
        fill: {
            opacity: 1

        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val + "  Đề án"
                }
            }
        }
    }

    var chart = new ApexCharts(
        document.querySelector("#ticketoverview"),
        options
    );

    chart.render();
</script>
@stop
