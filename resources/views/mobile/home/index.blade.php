<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>I-VIBO</title>
    <meta name="description" content="kaizen,5S">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" href="{{\App\Config::first()->favicon}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{!!asset('mobile/assets/img/icon/192x192.png')!!}">
    <link rel="stylesheet" href="{!!asset('mobile/assets/css/style.css')!!}">
    <link rel="manifest" href="__manifest.json">
</head>
<body class="bg-white">
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <div id="appCapsule" class="pt-0" style="height: 100vh;padding-top:56px;">
        <div class="login-form mt-1">
            <div class="section">
                <img src="{!!asset('mobile/assets/img/logo.jpg')!!}" alt="image" class="form-image">
            </div>
            <div class="section mt-1">
                <h1>Get started</h1>
                <h4>Fill the form to log in</h4>
            </div>
            <div class="section mt-1 mb-5">
                <form action="{!!route('loginMember')!!}" method="POST">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" class="form-control" name='login_id' id="login_id1" placeholder="Company ID" autocomplete="off">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="password" class="form-control" name='password' id="password1" placeholder="Password" autocomplete="off">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-button-group" style="position: initial;padding:0px;">
                        <button type="submit" class="btn btn-primary btn-block btn-lg">Log in</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Jquery -->
    <script src="{{asset('mobile/assets/js/lib/jquery-3.4.1.min.js')}}"></script>
    <!-- Bootstrap-->
    <script src="{{asset('mobile/assets/js/lib/popper.min.js')}}"></script>
    <script src="{{asset('mobile/assets/js/lib/bootstrap.min.js')}}"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="{{asset('mobile/assets/js/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
    <!-- jQuery Circle Progress -->
    <script src="{{asset('mobile/assets/js/plugins/jquery-circle-progress/circle-progress.min.js')}}"></script>
    <!-- Base Js File -->
    <script src="{{asset('mobile/assets/js/base.js')}}"></script>
</body>
</html>
