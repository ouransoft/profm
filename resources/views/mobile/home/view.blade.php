@extends('mobile.layouts.admin')
@section('content')
<style>
  .header-home {
    background: #000;
    height: 140px;
    border-bottom-right-radius: 35px;
    display: block;
    position: relative;
  }

  .content-header-home {
    display: inline-block;
    height: 100%;
    padding: 15px;
  }

  .img-logo img {
    display: block;
    width: 50%;
    height: auto;
  }

  .content-header-home p {
    margin: 0px;
    color: #fff;
  }

  .info-member {
    position: absolute;
    bottom: 2px;
    right: 7px;
    font-size: 58px;
    color: #fff;
  }

  .schedule {
    padding: 10px;
  }

  .schedule-content {
    box-shadow: rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset;
    padding: 15px;
    border-radius: 10px;
  }

  .icon-schedule {
    font-size: 40px;
    color: #333;
  }

  .schedule-middle span {
    font-size: 16px;
    font-weight: 600;
  }

  .top-module img {
    width: 45px;
    height: auto;
  }

  .card {
    width: 100%;
    margin: 5px 0px;
    padding: 10px;
    box-shadow: rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset;
  }

  .top-module ion-icon {
    font-size: 22px;
    color: orange;
  }

  .content-module {
    line-height: 20px;
    padding: 5px 10px;
    background: #f6e9d8;
    border-radius: 10px;
    margin-top: 6px;
  }

  .content-module span {
    font-size: 12px;
    color: #333;
  }

  .content-module h3 {
    margin: 0px;
  }
</style>
<div id="appCapsule" style="padding:0px;padding-bottom:56px">
  <div class="header-home">
    <div class="content-header-home">
      <div class="img-logo">
        <img src="/img/i-vibo.png" />
      </div>
      <p>CHÀO MỪNG</p>
      <p>{{\Auth::guard('member')->user()->full_name}} ({{\Auth::guard('member')->user()->login_id}})</p>
    </div>
    <a href="{{route('frontend.member.profile')}}" class="info-member">
      <ion-icon name="information-circle-outline"></ion-icon>
    </a>
  </div>
  <div class="content-home">
    <div class="schedule">
      <div class="schedule-content">
        <div class="schedule-header d-flex">
          <div class="d-block w-50">
            @if(is_null($schedule))
            <span class="d-block">Lịch trình hôm nay</span>
            <span class="d-block">Thứ {{date('w')}} (<span class="text-warning">Ngày {{date('d')}} tháng {{date('m')}}</span>)</span>
            @else
            <span class="d-block">Lịch trình sắp tới</span>
            <span class="d-block">{{$schedule->date}} (<span class="text-warning">{{$schedule->day}}</span>)</span>
            @endif
          </div>
          <div class="d-block w-50 text-right">
            <a href="javascript:void(0)" class="icon-schedule">
              <ion-icon name="calendar-outline"></ion-icon>
            </a>
          </div>
        </div>
        @if(is_null($schedule))
        <div class="schedule-middle text-center">
          <span>BẠN KHÔNG CÓ LỊCH TRÌNH NÀO</span>
        </div> 
        @else
        <div class="schedule-middle text-center text-dark">
          <a href="{{route('frontend.schedule.view',$schedule->id)}}">
            <span>{{$schedule->title}}</span>
          </a>
        </div>
        @endif
      </div>
    </div>
    <div class="row" style="margin:0px">
      <div class="col-6">
        <a href="{{route('frontend.schedule.index')}}">
          <div class="card">
            <div class="top-module">
              <img src="img/Scheduler.png">
              <ion-icon class="float-right" name="create-outline"></ion-icon>
            </div>
            <div class="content-module">
              <h3>Lịch trình</h3>
              <span>{{$count_schedule}} sự kiện</span>
            </div>
          </div>
        </a>
      </div>
      <div class="col-6">
        <a href="{{route('frontend.todo.index')}}">
          <div class="card">
            <div class="top-module">
              <img src="img/To_do_list.png">
              <ion-icon class="float-right" name="create-outline"></ion-icon>
            </div>
            <div class="content-module">
              <h3>Công việc</h3>
              <span>{{$count_todolist}} công việc</span>
            </div>
          </div>
        </a>
      </div>
      <div class="col-6">
        <a href="{{route('frontend.project.index')}}">
          <div class="card">
            <div class="top-module">
              <img src="img/kaizen.png">
              <ion-icon class="float-right" name="create-outline"></ion-icon>
            </div>
            <div class="content-module">
              <h3>KAIZEN</h3>
              <span>{{$count_todolist}} đề án</span>
            </div>
          </div>
        </a>
      </div>
      @if(\Auth::guard('member')->user()->can('tpm-index'))
      <div class="col-6">
        <a href="{{route('frontend.tpm.dashboard')}}">
          <div class="card">
            <div class="top-module">
              <img src="img/TPM.png">
              <ion-icon class="float-right" name="create-outline"></ion-icon>
            </div>
            <div class="content-module">
              <h3>TPM SYSTEM</h3>
              @if($count_action_tpm > 0)
              <span>{{$count_action_tpm}} công việc chờ</span>
              @endif
            </div>
          </div>
        </a>
      </div>
      @endif
      <!-- <div class="col-6">
        <a href="">
          <div class="card">
            <div class="top-module text-center">
              <img src="img/analytics.png">
            </div>
            <div class="content-module text-center">
              <h3>THỐNG KÊ</h3>
            </div>
          </div>
        </a>
      </div> -->
      @if(\Auth::guard('member')->user()->can('trouble-report'))
      <div class="col-6">
        <a href="{{route('frontend.trouble.report')}}">
          <div class="card" style="background:red">
            <div class="top-module text-center">
              <img src="img/warning.png">
            </div>
            <div class="content-module text-center" style="background:#fe9a41">
              <h3>BÁO CÁO SỰ CỐ</h3>
            </div>
          </div>
        </a>
      </div>
      @endif
    </div>
  </div>
</div>
@stop
@section('script')
@parent
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js"></script>
@stop