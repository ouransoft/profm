@extends('mobile.layouts.admin')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('frontend.schedule.index')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">{{trans('base.Apointerment_details')}}</div>
</div>
<div id="appCapsule" class="header-active">
    <div class="section full">
        <div class="py-2 d-flex justify-content-center">
            @if($schedule->pattern == 1)
            <a href="{{route('frontend.schedule.edit',$schedule->id)}}" type="button" class="btn btn-text-primary w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="create-outline" class="mx-0"></ion-icon>
                <span>{{ trans('base.Edit') }}</span>
            </a>
            @elseif($schedule->pattern == 2)
            <a href="{{route('frontend.schedule.edit_all',$schedule->id)}}" type="button" class="btn btn-text-primary w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="create-outline" class="mx-0"></ion-icon>
                <span>{{ trans('base.Edit') }}</span>
            </a>
            @elseif($schedule->pattern == 3)
            <a href="{{route('frontend.schedule.edit_repeat',$schedule->id)}}" type="button" class="btn btn-text-primary w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="create-outline" class="mx-0"></ion-icon>
                <span>{{ trans('base.Edit') }}</span>
            </a>
            @else
            <a href="{{route('frontend.schedule.edit_task',$schedule->id)}}" type="button" class="btn btn-text-primary w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="create-outline" class="mx-0"></ion-icon>
                <span>Edit</span>
            </a>
            @endif
            <a id="confirm_delete" href="javascript:void(0)" data-target="#DialogIconedButton" data-toggle="modal" type="button" class="btn btn-text-danger w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="close" class="mx-0"></ion-icon>
                <span>{{ trans('base.Delete') }}</span>
            </a>
            @if($schedule->pattern == 1)
            <a href="{{route('frontend.schedule.create',['schedule_id'=>$schedule->id])}}" type="button" class="btn btn-text-success w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="reload" class="mx-0"></ion-icon>
                <span>{{ trans('base.Reuse') }}</span>
            </a>
            @elseif($schedule->pattern == 2)
            <a href="{{route('frontend.schedule.create_all_day',['schedule_id'=>$schedule->id])}}" type="button" class="btn btn-text-success w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="reload" class="mx-0"></ion-icon>
                <span>{{ trans('base.Reuse') }}</span>
            </a>
            @elseif($schedule->pattern == 3)
            <a href="{{route('frontend.schedule.create_repeat',['schedule_id'=>$schedule->id])}}" type="button" class="btn btn-text-success w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="reload" class="mx-0"></ion-icon>
                <span>{{ trans('base.Reuse') }}</span>
            </a>
            @endif
            @if(in_array(\Auth::guard('member')->user()->id,$schedule->member()->pluck('id')->toArray()))
            <a href="/schedule/leave/{{$schedule->id}}" type="button" class="btn btn-text-warning w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="person-remove" class="mx-0"></ion-icon>
                <span>{{ trans('base.Leave') }}</span>
            </a>
            @else
            <a href="/schedule/participate/{{$schedule->id}}" type="button" class="btn btn-text-warning w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="person-add" class="mx-0"></ion-icon>
                <span>{{ trans('base.Attend') }}</span>
            </a>
            @endif
        </div>
        <div class="wide-block pt-2">
            <h3 class="font-weight-normal mb-1">
                <div class="chip chip-info chip-height rounded schedule-title-tag">
                    <span class="px-1 font-weight-normal">Cuộc họp</span>
                </div>
                <span class="text-warning">{{$schedule->title}}</span>
            </h3>
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="calendar" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>{{ trans('base.Date_and_time') }}</span>
                    </label>
                    @if($schedule->pattern == 2 || $schedule->pattern == 4)
                    <span class="pl-3 fs-16">{!!date('D, F d, Y',strtotime($schedule->start_date))!!} - {!!date('D, F d, Y',strtotime($schedule->end_date))!!}</span>
                    @elseif($schedule->pattern == 1)
                    <span class="pl-3 fs-16">{!!date('D, F d, Y h:i A',strtotime($schedule->start_date))!!} - {!!date('D, F d, Y h:i A',strtotime($schedule->end_date))!!}</span>
                    @else
                    <span class="pl-3 fs-16">{!!date('h:i A',strtotime($schedule->start_date))!!}&nbsp; - &nbsp;{!!date('h:i A',strtotime($schedule->end_date))!!}</span>
                    @endif
                </div>
            </div>
            @if($schedule->pattern == 3)
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="reload" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>{{ trans('base.Repeats') }}</span>
                    </label>
                    <span class="pl-3 fs-16">{{ trans('base.Every') }} {!! trans('base.'.$schedule->type_repeat) !!} {!!date('d/m/Y',strtotime($schedule->start_repeat))!!} - {!!date('d/m/Y',strtotime($schedule->end_repeat))!!}</span>
                </div>
            </div>
            @endif
            @if(count($schedule->equipment) > 0)
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="construct" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>{{ trans('base.Facilities') }}</span>
                    </label>
                    <div class="pl-3 fs-16">
                        {{implode(',',$schedule->equipment()->pluck('name')->toArray())}}
                    </div>
                </div>
            </div>
            @endif
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="person" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>{{ trans('base.Attendees') }} ({{count($schedule->member)}} {{ trans('base.Member') }})</span>
                    </label>
                    <div class="pl-3 pr-2 fs-16">
                        <span class="mb-0 font-weight-normal">{{implode(',',$schedule->member()->pluck('full_name')->toArray())}}</span>
                    </div>
                </div>
            </div>
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="document" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>{{ trans('base.Notes') }}</span>
                    </label>
                    <div class="content pl-3">
                        {{$schedule->memo}}
                    </div>
                    <div class="pl-3 fs-16"></div>
                </div>
            </div>
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="document" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>File đính kèm</span>
                    </label>
                    <div class="content pl-3">
                        @if(count($schedule->files) > 0) 
                            @foreach($schedule->files as $key=>$value)
                                <a href="/{{$value->link}}"><span class="pl-3 fs-16">{{$value->name}} ({{$value->size}})</span></a>
                            @endforeach
                        @endif
                    </div>
                    <div class="pl-3 fs-16"></div>
                </div>
            </div>
        </div>
        <div class="pt-1 pb-2 px-3">
            <div class="fs-14">
                <div class="d-flex align-items-center text-muted">
                    <ion-icon name="time" class="pr-05 text-warning fs-18"></ion-icon>
                    <span>{{ trans('base.Registrant') }}</span>
                    <ion-icon name="person" class="fs-18 pl-1 pr-05 text-success"></ion-icon>
                    <a href="#">{!!$schedule->created_by->full_name!!}</a>
                </div>
                <span class="pl-92">{!!date('D , F d, Y h:i A',strtotime($schedule->created_at))!!}</span>
                <div class="d-flex align-items-center text-muted mt-1 flex-wrap">
                    <ion-icon name="time" class="pr-05 text-warning fs-18"></ion-icon>
                    <span class="pr-2">{{ trans('base.Updater') }}</span>
                    <ion-icon name="person" class="fs-18 pl-05 pr-05 text-success"></ion-icon>
                    <a href="#">@if(is_null($schedule->update_person)) {!!$schedule->created_by->full_name!!} @else {!!$schedule->updater->full_name!!} @endif</a>
                </div>
                <span class="pl-92">{!!date('D , F d, Y h:i A',strtotime($schedule->updated_at))!!}</span>
            </div>
        </div>
        <div class="modal fade dialogbox" id="modal_confirm_delete" data-backdrop="static" tabindex="-1" role="dialog" aria-modal="true" style="display: none;">
            <div class="modal-dialog" role="document">
                <form action="{{route('frontend.schedule.destroy',$schedule->id)}}" method="post">
                    {!! method_field('delete') !!}
                    {!! csrf_field() !!}
                    <div class="modal-content">
                        <div class="modal-icon text-danger">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </div>
                        <div class="modal-header">
                            <h5 class="modal-title">{{ trans('base.Notification') }}</h5>
                        </div>
                        <div class="modal-body">
                            {{ trans('base.Do_you_really_want_to_delete _these_records?_This_process_cannot_be_undone') }}
                            @if($schedule->pattern == 3)
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" id="customRadio1" name="apply" class="custom-control-input" value="this" checked="">
                                <label class="custom-control-label" for="customRadio1">{{ trans('base.This_appointment_only') }} {{date('D, F d,Y',strtotime($schedule->start_date))}}</label>
                            </div>
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" id="customRadio2" name="apply" class="custom-control-input" value="after">
                                <label class="custom-control-label" for="customRadio2">{{ trans('base.Appointments_on_and_after') }} {{date('D, F d,Y',strtotime($schedule->start_date))}}</label>
                            </div>
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" id="customRadio3" name="apply" class="custom-control-input" value="all">
                                <label class="custom-control-label" for="customRadio3">{{ trans('base.All_appointments') }} {{date('D, F d,Y',strtotime($schedule->start_date))}} - {{date('D, F d,Y',strtotime($schedule->end_date))}}</label>
                            </div>
                            @endif
                        </div>
                        <div class="modal-footer">
                            <div class="btn-inline">
                                <button type="submit" class="btn btn-text-danger">
                                    <ion-icon name="trash-outline" role="img" class="md hydrated" aria-label="trash outline"></ion-icon>
                                    {{ trans('base.Delete') }}
                                </button>
                                <a href="javascript:void(0)" class="btn btn-text-primary" data-dismiss="modal">
                                    <ion-icon name="share-outline" role="img" class="md hydrated" aria-label="share outline"></ion-icon>
                                    {{ trans('base.Cancel') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $('#confirm_delete').click(function(){
       $('#modal_confirm_delete').modal('show');
    })
</script>
@stop
