@extends('mobile.layouts.admin')
@section('content')
<div id="appCapsule" class="header-active">
    <div class="section full">
        <h5 class='pl-3'>Lịch trình đã bị xóa</h5>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $('#confirm_delete').click(function(){
       $('#modal_confirm_delete').modal('show');
    })
</script>
@stop