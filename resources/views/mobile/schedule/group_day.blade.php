@extends('mobile.layouts.admin')
@section('content')
<div class="extraHeader p-0">
    <ul class="nav nav-tabs style1" role="tablist">
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="{{route('frontend.schedule.index')}}" role="tab">
                Personal week
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="{{route('frontend.schedule.group_day')}}" role="tab">
                Group day
            </a>
        </li>
    </ul>
</div>

<!-- App Capsule -->
<div id="appCapsule" class="extra-header-active">
    <div class="section full">
        <div class="wide-block py-2 d-flex justify-content-center">
            <a href="#" class="btn btn-text-warning">
                <ion-icon name="chevron-back-outline" class="mr-0"></ion-icon>
            </a>
            <a type="button" class="btn btn-text-warning shadowed">Today</a>
            <a href="#" class="btn btn-text-warning">
                <ion-icon name="chevron-forward-outline" class="mr-0"></ion-icon>
            </a>
        </div>
    </div>
    <div class="tab-content">
        <!-- Day tab -->
        <div class="tab-pane fade active show" id="day" role="tabpanel">
            <div class="section full mt-1">
                <div class="section-title">
                    <h3>Wed, March 24, 2021</h3>
                </div>
            </div>
            <div class="wide-block tab-pane fade active show" id="week" role="tabpanel">
                <div class="section px-0 mt-3 mb-3">
                    <div class="card">
                        <div class="card-body py-1">
                            <ul class="listview flush transparent">
                                <li>
                                    <div class="px-0 item card-title d-flex justify-content-between">
                                        <img src="{{asset('mobile/assets/img/sample/avatar/avatar7.jpg')}}" alt="image" class="image">
                                        <h4 class="in"> Phạm Đức </h4>
                                        <a href="#" class="text-warning"><ion-icon name="create-outline"></ion-icon></a> 
                                    </div>
                                </li>
                                <li class="py-2">
                                    <a href="#" class="item px-0">
                                        <div>
                                            <div class="in text-dark">
                                                <span>01:10 PM-03:05 PM</span>
                                            </div>
                                            <div class="text-muted mt-1 pb-1 d-flex flex-row">
                                                <h4 class="mb-0">All day 20/3</h4>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-2">
                                    <a href="#" class="item px-0">
                                        <div>
                                            <div class="in text-dark">
                                                <span>01:10 PM-03:05 PM</span>
                                            </div>
                                            <div class="text-muted mt-1 pb-1 d-flex flex-row">
                                                <h4 class="mb-0">Cập nhật cash</h4>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card mt-2">
                        <div class="card-body py-1">
                            <ul class="listview flush transparent image-listview">
                                <li>
                                    <div class="px-0 item card-title d-flex justify-content-between">
                                        <img src="{{asset('mobile/assets/img/sample/avatar/avatar7.jpg')}}" alt="image" class="image">
                                        <h4 class="in"> Ngô Lương Lộc </h4>
                                        <a href="#" class="text-warning"><ion-icon name="create-outline"></ion-icon></a> 
                                    </div>
                                </li>
                                <li class="py-2">
                                    No Appointment
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card mt-2">
                        <div class="card-body py-1">
                            <ul class="listview flush transparent image-listview">
                                <li>
                                    <div class="px-0 item card-title d-flex justify-content-between">
                                        <img src="{{asset('mobile/assets/img/sample/avatar/avatar3.jpg')}}" alt="image" class="image">
                                        <h4 class="in"> Nguyễn Tuấn Anh </h4>
                                        <a href="#" class="text-warning"><ion-icon name="create-outline"></ion-icon></a> 
                                    </div>
                                </li>
                                <li class="py-2">
                                    <a href="#" class="item px-0">
                                        <div>
                                            <div class="in text-dark">
                                                <span>11:10 AM-03:05 PM</span>
                                            </div>
                                            <div class="text-muted mt-1 pb-1 d-flex flex-row">
                                                <h4 class="mb-0">Test</h4>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- * Day tab -->

    </div>
</div>
@stop
@section('script')
@parent
@stop