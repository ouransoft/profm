@extends('mobile.layouts.admin')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('frontend.schedule.index')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">{{ trans('base.Create_Schedule') }}</div>
</div>
<div id="appCapsule" class="header-active">
    <div class="section full my-2">
        <div class="section-title">
            <h2 class="text-warning">{{ trans('base.New_appointment') }}</h2>
        </div>
        <div class="wide-block">
            <div class="py-3 d-flex justify-content-center">
                <a href="{{route('frontend.schedule.create')}}" type="button" class="btn btn-text-warning shadowed w-31 mx-1 px-1 active">{{ trans('base.Regular') }}</a>
                <a href="{{route('frontend.schedule.create_all_day')}}" type="button" class="btn btn-text-warning shadowed w-31 mx-1 px-1">{{ trans('base.All_day') }}</a>
                <a href="{{route('frontend.schedule.create_repeat')}}" type="button" class="btn btn-text-warning shadowed w-31 mx-1 px-1">{{ trans('base.Repeating') }}</a>
            </div>
            <form action="{{route('frontend.schedule.store')}}" method="POST">
                <input type='hidden' name='menu' id='menu_schedule' value="@if(isset($schedule) && !is_null($schedule->menu)) {{$schedule->menu}} @endif">
                <input type='hidden' name='pattern' value="1">
                <input type='hidden' id='selected_users_sITEM' name='selected_users_sITEM' value="@if(isset($schedule)) {{implode(':',$schedule->equipment()->pluck('id')->toArray())}} @endif">
                <input id="selected_users_sUID" type="hidden" name="selected_users_sUID" value="@if(isset($schedule)) {{implode(':',$schedule->member()->pluck('id')->toArray())}} @else {{\Auth::guard('member')->user()->id}} @endif">
                <input type='hidden' id='selected_users_p_sUID' name='selected_users_p_sUID' value="@if(isset($schedule)) {{implode(':',$schedule->seen()->pluck('id')->toArray())}} @endif">
                <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="reservation"><h4>{{ trans('base.Date_and_time') }}</h4></label>
                        <div class="fieldset__wrapper d-flex form-group">
                            <input id="" name="start_date" class="fieldset__input form-control datepicker w-60" value="@if(isset($schedule)) {{date('d/m/Y',strtotime($schedule->start_date))}} @else {{date('d/m/Y')}} @endif" type="text" placeholder="{{ trans('base.Start_date') }}">
                            <input type="text" name="start_time" class="bs-timepicker form-control w-35" placeholder="--:--" value="@if(isset($schedule) && $schedule->none_time == 0) {{date('H:i',strtotime($schedule->start_date))}} @endif" />
                        </div>
                        <div class="fieldset__wrapper d-flex form-group">
                            <input id="" name="end_date" class="fieldset__input form-control datepicker w-60" value="@if(isset($schedule)) {{date('d/m/Y',strtotime($schedule->end_date))}} @else {{date('d/m/Y')}} @endif" type="text" placeholder="{{ trans('base.End_date') }}">
                            <input type="text" name="end_time" class="bs-timepicker form-control w-35" placeholder="--:--" value="@if(isset($schedule) && $schedule->none_time == 0) {{date('H:i',strtotime($schedule->end_date))}} @endif" />
                        </div>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>{{ trans('base.Subject') }}</h4></label>
                        <div class="d-flex flex-row">
                            <dl id="order" class="dropdown w-25 mb-0">
                                <dt class="form-control select-center d-flex align-items-center justify-content-center px-1 w-100" style="overflow: hidden">
                                    @if(isset($schedule) && !is_null($schedule->menu))
                                    <a href="#" class="text-dark d-flex justify-content-start align-items-center" style="font-size: 13px;">
                                        <div class="chip chip-info rounded-0 menu-color{{explode(';#',$schedule->menu)[1]}}" style="height: 16px; width: 16px;">
                                            <span class="chip-label"></span>
                                        </div>
                                        {{explode(';#',$schedule->menu)[0]}}
                                    </a>
                                    @else
                                    <a href="#" class="text-dark d-flex justify-content-start align-items-center" style="font-size: 13px;">-----</a>
                                    @endif
                                </dt>
                                <dd>
                                    <ul class="ul-drop" style="z-index: 1000; display: none;">
                                        <li value=""><a href="#" class="pl-1 text-dark">-----</a></li>
                                        <li value="Cuộc họp;#5">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-info rounded-0 menu-color5" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Cuộc họp</span>
                                            </a>
                                        </li>
                                        <li value="Hội thảo;#2">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-primary rounded-0 menu-color2" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Hội thảo</span>
                                            </a>
                                        </li>
                                        <li value="Đào tạo;#1">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-primary rounded-0 menu-color1" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Đào tạo</span>
                                            </a>
                                        </li>
                                        <li value="Công tác;#7">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-warning rounded-0 menu-color7" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Công tác</span>
                                            </a>
                                        </li>
                                        <li value="Ngày lễ;#6">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-info rounded-0 menu-color6" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Ngày lễ</span>
                                            </a>
                                        </li>
                                        <li value="Phỏng vấn;#3">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-danger rounded-0 menu-color3" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Phỏng vấn</span>
                                            </a>
                                        </li>
                                        <li value="Gặp gỡ;#4">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-success rounded-0 menu-color4" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Gặp gỡ</span>
                                            </a>
                                        </li>
                                    </ul>
                                </dd>
                            </dl>
                            <input type="text" name='title' class="form-control w-75 ml-1" id="subject" required="" value="@if(isset($schedule)) {{$schedule->title}} @endif">
                        </div>
                    </div>
                </div>
                <div class="form-group boxed pt-0">
                    <div class="input-wrapper">
                        <label class="label" for="attendees"><h4>{{ trans('base.Attendees') }}</h4></label>
                        <div class="card my-2" id="attendees">
                            <h4 class="attendees-select bg-warning mb-0 py-2 px-2 text-light rounded-top d-flex align-items-center justify-content-between" data-toggle="modal" data-target="#insert-attendees">
                                {{ trans('base.list_attendees') }}
                                <ion-icon class="" name="create-outline" style="font-size:24px"></ion-icon>
                            </h4>
                            <div class="border user-list">
                                <ul class="listview attendees-final">
                                    @if(isset($schedule))
                                        @foreach($schedule->member as $key=>$val)
                                        <li class="d-flex align-items-center px-1" data-attendee_id="{{$val->id}}">
                                            <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                                <ion-icon name="person-outline" role="img" class="md hydrated" aria-label="person outline"></ion-icon>
                                            </span>
                                            <div class="d-flex align-items-end justify-content-between w-100">
                                                <span class="attendees-name" style="font-size: 16px;">{{$val->full_name}}</span>
                                                <span class="text-warning remove-attendees-btn" style="font-size: 22px;">
                                                    <ion-icon name="remove-outline" role="img" class="md hydrated" aria-label="remove outline"></ion-icon>
                                                </span>
                                            </div>
                                        </li>
                                        @endforeach
                                    @else
                                        <li class="d-flex align-items-center px-1" data-attendee_id="{{\Auth::guard('member')->user()->id}}">
                                            <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                                <ion-icon name="person-outline" role="img" class="md hydrated" aria-label="person outline"></ion-icon>
                                            </span>
                                            <div class="d-flex align-items-end justify-content-between w-100">
                                                <span class="attendees-name" style="font-size: 16px;">{{\Auth::guard('member')->user()->full_name}}</span>
                                                <span class="text-warning remove-attendees-btn" style="font-size: 22px;">
                                                    <ion-icon name="remove-outline" role="img" class="md hydrated" aria-label="remove outline"></ion-icon>
                                                </span>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="modal fade modalbox" id="insert-attendees" data-backdrop="static" tabindex="-1" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-warning">{{ trans('base.Add_attendees') }}</h5>
                                        <a href="javascript:;" data-dismiss="modal" class="close-attendees">{{ trans('base.Close') }}</a>
                                    </div>
                                    <div class="modal-body">
                                        <div class="input-group rounded">
                                            <input type="search" id='input_search_attendees' class="form-control rounded" placeholder="{{ trans('base.Staff_search') }}" aria-label="Search" aria-describedby="search-addon"/>
                                            <button type='button' class="search-attendees search-position input-group-text border-0" id="search-addon">
                                                <ion-icon name="search-outline" role="img" class="md hydrated text-warning" aria-label="search outline"></ion-icon>
                                            </button>
                                        </div>
                                        <div class="form-group my-2">
                                            <select id='select_attendees' class="custom-select">
                                                <option selected value=''>Tất cả</option>
                                                @foreach($departments as $key=>$department)
                                                <option value='{{$department->id}}'>{{$department->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <h4 class="attendees-add-all text-warning mt-2 d-flex align-items-center">
                                            <ion-icon name="add-outline" style="font-size: 20px;"></ion-icon>
                                            Thêm tất cả
                                        </h4>
                                        <div class="card">
                                            <h4 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">{{ trans('base.list_staff') }}</h4>
                                            <div class="border user-list">
                                                <ul class="listview attendees-list">
                                                    @foreach($members as $key=>$val)
                                                    <li class="d-flex align-items-center px-1" data-attendee_id="{{$val->id}}">
                                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                                            <ion-icon name="person-outline"></ion-icon>
                                                        </span>
                                                        <div class="d-flex align-items-end justify-content-between w-100">
                                                            <span class="attendees-name" style="font-size: 16px;">{{$val->full_name}}</span>
                                                            <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                                                <ion-icon name="add-outline"></ion-icon>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <h4 class="attendees-remove-all text-warning mt-2 d-flex align-items-center">
                                            <ion-icon name="remove-outline" style="font-size: 20px;"></ion-icon>
                                            Xóa tất cả
                                        </h4>
                                        <div class="card mt-1">
                                            <h4 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">{{ trans('base.list_attendees') }}</h4>
                                            <div class="border user-list">
                                                <ul class="listview attendees-invite">

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card mt-1">
                                            <button type="button" class="submit-attendees btn btn-primary mr-1 mb-1" data-dismiss="modal">{{ trans('base.Submit') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed pt-0">
                    <div class="input-wrapper">
                        <label class="label" for="shared"><h4>{{ trans('base.Shared_with') }}</h4></label>
                        <div class="card my-2" id="shared">
                            <h4 class="shared-select bg-warning mb-0 py-2 px-2 text-light rounded-top d-flex align-items-center justify-content-between" data-toggle="modal" data-target="#insert-shared">
                                {{ trans('base.list_shared') }}
                                <ion-icon name="create-outline" style="font-size:24px"></ion-icon>
                            </h4>
                            <div class="border user-list">
                                <ul class="listview shared-final">
                                    @if(isset($schedule))
                                        @foreach($schedule->seen as $key=>$val)
                                        <li class="d-flex align-items-center px-1" data-share_id ="{{$val->id}}">
                                            <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                                <ion-icon name="person-outline"></ion-icon>
                                            </span>
                                            <div class="d-flex align-items-end justify-content-between w-100">
                                                <span class="shared-name" style="font-size: 16px;">{{$val->full_name}}</span>
                                                <span class="text-warning add-shared-btn" style="font-size: 22px;">
                                                    <ion-icon name="add-outline"></ion-icon>
                                                </span>
                                            </div>
                                        </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="modal fade modalbox" id="insert-shared" data-backdrop="static" tabindex="-1" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-warning">{{ trans('base.Add_shared_staff') }}</h5>
                                        <a href="javascript:;" data-dismiss="modal" class="close-shared">{{ trans('base.Close') }}</a>
                                    </div>
                                    <div class="modal-body">
                                        <div class="input-group rounded">
                                            <input type="search" id='input_share' class="form-control rounded" placeholder="{{ trans('base.User_search') }}" aria-label="Search" aria-describedby="search-addon"/>
                                            <button class="search-position search-share input-group-text border-0" id="search-addon">
                                                <ion-icon name="search-outline" role="img" class="md hydrated text-warning" aria-label="search outline"></ion-icon>
                                            </button>
                                        </div>
                                        <div class="form-group my-2">
                                            <select class="custom-select" id='select_share'>
                                                <option selected value=''>Tất cả</option>
                                                @foreach($departments as $key=>$department)
                                                    <option value='{{$department->id}}'>{{$department->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <h4 class="shared-add-all text-warning mt-2 d-flex align-items-center">
                                            <ion-icon name="add-outline" style="font-size: 20px;"></ion-icon>
                                            Thêm tất cả
                                        </h4>
                                        <div class="card">
                                            <h4 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">{{ trans('base.list_staff') }}</h4>
                                            <div class="h220 border user-list">
                                                <ul class="listview shared-list">
                                                    @foreach($members as $key=>$val)
                                                    <li class="d-flex align-items-center px-1" data-share_id ="{{$val->id}}">
                                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                                            <ion-icon name="person-outline"></ion-icon>
                                                        </span>
                                                        <div class="d-flex align-items-end justify-content-between w-100">
                                                            <span class="shared-name" style="font-size: 16px;">{{$val->full_name}}</span>
                                                            <span class="text-warning add-shared-btn" style="font-size: 22px;">
                                                                <ion-icon name="add-outline"></ion-icon>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <h4 class="shared-remove-all text-warning mt-2 d-flex align-items-center">
                                            <ion-icon name="remove-outline" style="font-size: 20px;"></ion-icon>
                                            Xóa tất cả
                                        </h4>
                                        <div class="card mt-1">
                                            <h4 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">{{ trans('base.Shared_staff_list') }}</h4>
                                            <div class="border user-list">
                                                <ul class="listview shared-invite">

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card mt-1">
                                            <button type="button" class="submit-shared btn btn-primary mr-1 mb-1" data-dismiss="modal">{{ trans('base.Submit') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed pt-0">
                    <div class="input-wrapper">
                        <label class="label" for="facilities"><h4>{{ trans('base.Facilities') }}</h4></label>
                        <div class="card my-2" id="facilities">
                            <h4 class="facilities-select bg-warning mb-0 py-2 px-2 text-light rounded-top d-flex align-items-center justify-content-between" data-toggle="modal" data-target="#insert-facilities">
                                {{ trans('base.list_facilities') }}
                                <ion-icon name="create-outline" style="font-size:24px"></ion-icon>
                            </h4>
                            <div class="border user-list">
                                <ul class="listview facilities-final">
                                    @if(isset($schedule) && count($schedule->equipment) > 0)
                                    @foreach($equipments as $key=>$val)
                                        <li class="d-flex align-items-center px-1" data-equipment_id="{{$val->id}}">
                                            <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                                <ion-icon name="person-outline"></ion-icon>
                                            </span>
                                            <div class="d-flex align-items-end justify-content-between w-100">
                                                <span class="facilities-name" style="font-size: 16px;">{{$val->name}}</span>
                                                <span class="text-warning add-facilities-btn" style="font-size: 22px;">
                                                    <ion-icon name="add-outline"></ion-icon>
                                                </span>
                                            </div>
                                        </li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="modal fade modalbox" id="insert-facilities" data-backdrop="static" tabindex="-1" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-warning">{{ trans('base.Add_facilities') }}</h5>
                                        <a href="javascript:;" data-dismiss="modal" class="close-facilities">{{ trans('base.Close') }}</a>
                                    </div>
                                    <div class="modal-body">
                                        <div class="input-group rounded">
                                            <input type="search" class="form-control rounded" placeholder="{{ trans('base.Facility_search') }}" aria-label="Search" aria-describedby="search-addon"/>
                                            <button class="search-position input-group-text border-0" id="search-addon">
                                                <ion-icon name="search-outline" role="img" class="md hydrated text-warning" aria-label="search outline"></ion-icon>
                                            </button>
                                        </div>
                                        <div class="form-group my-2">
                                            <select class="custom-select" id="exampleFormControlSelect1">
                                                <option selected>Tất cả</option>
                                                <option>Kết quả tìm kiếm</option>
                                            </select>
                                        </div>
                                        <h4 class="facilities-add-all text-warning mt-2 d-flex align-items-center">
                                            <ion-icon name="add-outline" style="font-size: 20px;"></ion-icon>
                                            Thêm tất cả
                                        </h4>
                                        <div class="card">
                                            <h4 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">{{ trans('base.list_facilities') }}</h4>
                                            <div class="h220 border user-list">
                                                <ul class="listview facilities-list">
                                                    @foreach($equipments as $key=>$val)
                                                    <li class="d-flex align-items-center px-1" data-equipment_id="{{$val->id}}">
                                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                                            <ion-icon name="person-outline"></ion-icon>
                                                        </span>
                                                        <div class="d-flex align-items-end justify-content-between w-100">
                                                            <span class="facilities-name" style="font-size: 16px;">{{$val->name}}</span>
                                                            <span class="text-warning add-facilities-btn" style="font-size: 22px;">
                                                                <ion-icon name="add-outline"></ion-icon>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <h4 class="facilities-remove-all text-warning mt-2 d-flex align-items-center">
                                            <ion-icon name="remove-outline" style="font-size: 20px;"></ion-icon>
                                            Xóa tất cả
                                        </h4>
                                        <div class="card mt-1">
                                            <h4 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">{{ trans('base.list_facilities') }}</h4>
                                            <div class="border user-list">
                                                <ul class="listview facilities-invite">

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card mt-1">
                                            <button type="button" class="submit-facilities btn btn-primary mr-1 mb-1" data-dismiss="modal">{{ trans('base.Submit') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group-boxed">
                    <div class="input-wrapper">
                        <a href="javascript:;" class="text-dark" type="button" data-toggle="collapse" data-target="#accordion1" aria-expanded="false">
                            <h4 class="d-flex align-items-center">
                                <span>{{ trans('base.Add_company_information') }}</span>
                                <ion-icon class="text-warning" name="caret-down-outline"></ion-icon>
                            </h4>
                        </a>
                        <div id="accordion1" class="accordion-body collapse">
                            <div class="accordion-content">
                                <div class="wide-block border-0 pb-2">
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="label" for="company-name">{{ trans('base.Company_name') }}</label>
                                            <input type="text" name='company_name' value="@if(isset($schedule)) {{$schedule->company_name}} @endif" class="form-control" id="company-name" placeholder="Enter company name">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="label" for="address">{{ trans('base.Address') }}</label>
                                            <input type="text" name='physical_address' class="form-control" value="@if(isset($schedule)) {{$schedule->physical_address}} @endif" id="address" placeholder="Enter address">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="label" for="company-phone-number">{{ trans('base.Company_phone_number') }}</label>
                                            <input type="text" name='company_telephone_number' class="form-control" value="@if(isset($schedule)) {{$schedule->company_telephone_number}} @endif" id="company-phone-number" placeholder="Enter company phone number">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label"><h4>{{ trans('base.Notes') }}</h4></label>
                        <textarea name='memo' rows="2" class="form-control">@if(isset($schedule)) {{$schedule->memo}} @endif</textarea>
                    </div>
                </div>
                <div class="form-group boxed py-3">
                    <div class="input-wrapper">
                        <label class="label" for="attachments">
                            <h4 class="d-flex align-items-center">
                                <span>{{ trans('base.Attachments') }}</span>
                                <ion-icon class="text-warning" name="attach-outline" style="font-size: 22px;"></ion-icon>
                            </h4>
                        </label>
                        <input id="attachments" type="file" multiple>
                        <div id='list_file_upload'>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed pt-0">
                    <div class="input-wrapper">
                        <label class="label"><h4>{{ trans('base.Visibility') }}</h4></label>
                        <div class="d-flex">
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" id="public" name="private" class="custom-control-input" value='0' @if(isset($schedule) && $schedule->private == 1) @else checked @endif>
                                <label class="custom-control-label pl-0" for="public">{{ trans('base.Public') }}</label>
                            </div>
                            <div class="custom-control custom-radio mb-1 ml-2">
                                <input type="radio" id="private" name="private" class="custom-control-input" value='1' @if(isset($schedule) && $schedule->private == 1) checked @endif>
                                <label class="custom-control-label pl-0" for="private">{{ trans('base.Private') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed pt-0">
                    <div class="input-wrapper">
                        <label class="label"><h4>Lịch công ty</h4></label>
                        <div class="d-flex">
                            <div class="custom-control custom-radio mb-1">
                                <input type="checkbox" id="pd" name="public_dashboard" class="custom-control-input" value='1'>
                                <label class="custom-control-label pl-0" for="pd">Hiển thị</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center py-3">
                    <button type="button" class="btn btn-primary mr-2 mb-1 w-25" id="submit_form">{{ trans('base.Add') }}</button>
                    <a href="{{route('frontend.schedule.index')}}" class="btn btn-warning mr-1 mb-1 w-25">{{ trans('base.Cancel') }}</a>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $(".dropdown dt a").click(function () {
        $(".dropdown dd ul").toggle();
    });

    $(".dropdown dd ul li a").click(function () {
        var text = $(this).html();
        $(".dropdown dt a").html(text);
        $(".dropdown dd ul").hide();
        if (order != getSelectedValue("order")) {
            order = getSelectedValue("order");
        }
    });

    function getSelectedValue(id) {
        return $("#" + id).find("dt a span.value").html();
    }

    $('body').delegate(".attendees-select", "click", function () {
        var staff = $(".attendees-list").children();
        var attendees = $(".attendees-invite").children();
        var value = $(".attendees-final").children();
        $(".attendees-invite").append(value);

        $(".close-attendees").click(function () {
            $(".attendees-list").html(staff);
            $(".attendees-invite").html(attendees);
            $(".attendees-final").html(value);
        });
    })

    $('body').delegate(".attendees-add-all", "click", function () {
        var value = $(".attendees-list").children();
        var enable = $(".attendees-list").html();
        if (enable != "") {
            $.each(value, function () {
                var name = $(this).find(".attendees-name").text();
                $(".attendees-invite").append('<li class="d-flex align-items-center px-1">' +
                        '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                        '<ion-icon name="person-outline"></ion-icon>' +
                        '</span>' +
                        '<div class="d-flex align-items-end justify-content-between w-100">' +
                        '<span class="attendees-name" style="font-size: 16px;">' + name + '</span>' +
                        '<span class="text-warning remove-attendees-btn" style="font-size: 22px;">' +
                        '<ion-icon name="remove-outline"></ion-icon>' +
                        '</span>' +
                        '</div>' +
                        '</li>');
            });
            $(".attendees-list").html("");
        }
    });

    $('body').delegate(".attendees-remove-all", "click", function () {
        var value = $(".attendees-invite").children();
        var enable = $(".attendees-invite").html();
        if (enable != "") {
            $.each(value, function () {
                var name = $(this).find(".attendees-name").text();
                $(".attendees-list").append('<li class="d-flex align-items-center px-1">' +
                        '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                        '<ion-icon name="person-outline"></ion-icon>' +
                        '</span>' +
                        '<div class="d-flex align-items-end justify-content-between w-100">' +
                        '<span class="attendees-name" style="font-size: 16px;">' + name + '</span>' +
                        '<span class="text-warning add-attendees-btn" style="font-size: 22px;">' +
                        '<ion-icon name="add-outline"></ion-icon>' +
                        '</span>' +
                        '</div>' +
                        '</li>');
            });
            $(".attendees-invite").html("");
        }
    });

    $('body').delegate(".add-attendees-btn", "click", function () {
        var name = $(this).parent().find(".attendees-name").text();
        $(this).parent().parent().remove();
        $(".attendees-invite").append('<li class="d-flex align-items-center px-1" data-attendee_id='+$(this).parents('li').data('attendee_id')+'>' +
                '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                '<ion-icon name="person-outline"></ion-icon>' +
                '</span>' +
                '<div class="d-flex align-items-end justify-content-between w-100">' +
                '<span class="attendees-name" style="font-size: 16px;">' + name + '</span>' +
                '<span class="text-warning remove-attendees-btn" style="font-size: 22px;">' +
                '<ion-icon name="remove-outline"></ion-icon>' +
                '</span>' +
                '</div>' +
                '</li>');
    });

    $('body').delegate(".remove-attendees-btn", "click", function () {
        var name = $(this).parent().find(".attendees-name").text();
        $(this).parent().parent().remove();
        $(".attendees-list").append('<li class="d-flex align-items-center px-1" data-attendee_id='+$(this).parents('li').data('attendee_id')+'>' +
                '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                '<ion-icon name="person-outline"></ion-icon>' +
                '</span>' +
                '<div class="d-flex align-items-end justify-content-between w-100">' +
                '<span class="attendees-name" style="font-size: 16px;">' + name + '</span>' +
                '<span class="text-warning add-attendees-btn" style="font-size: 22px;">' +
                '<ion-icon name="add-outline"></ion-icon>' +
                '</span>' +
                '</div>' +
                '</li>');
    });

    $('body').delegate(".submit-attendees", "click", function () {
        var value = $(".attendees-invite").children();
        var data = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $('#selected_users_sUID').val(data);
        $(".attendees-final").append(value);
    })

    // *Attendees event

    // Shared with event

    $('body').delegate(".shared-select", "click", function () {
        var staff = $(".shared-list").children();
        var shared = $(".shared-invite").children();
        var value = $(".shared-final").children();
        $(".shared-invite").append(value);

        $(".close-shared").click(function () {
            $(".shared-list").html(staff);
            $(".shared-invite").html(shared);
            $(".shared-final").html(value);
        });
    })

    $('body').delegate(".shared-add-all", "click", function () {
        var value = $(".shared-list").children();
        var enable = $(".shared-list").html();
        if (enable != "") {
            $.each(value, function () {
                var name = $(this).find(".shared-name").text();
                $(".shared-invite").append('<li class="d-flex align-items-center px-1">' +
                        '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                        '<ion-icon name="person-outline"></ion-icon>' +
                        '</span>' +
                        '<div class="d-flex align-items-end justify-content-between w-100">' +
                        '<span class="shared-name" style="font-size: 16px;">' + name + '</span>' +
                        '<span class="text-warning remove-shared-btn" style="font-size: 22px;">' +
                        '<ion-icon name="remove-outline"></ion-icon>' +
                        '</span>' +
                        '</div>' +
                        '</li>');
            });
            $(".shared-list").html("");
        }
    });

    $('body').delegate(".shared-remove-all", "click", function () {
        var value = $(".shared-invite").children();
        var enable = $(".shared-invite").html();
        if (enable != "") {
            $.each(value, function () {
                var name = $(this).find(".shared-name").text();
                $(".shared-list").append('<li class="d-flex align-items-center px-1">' +
                        '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                        '<ion-icon name="shared-outline"></ion-icon>' +
                        '</span>' +
                        '<div class="d-flex align-items-end justify-content-between w-100">' +
                        '<span class="shared-name" style="font-size: 16px;">' + name + '</span>' +
                        '<span class="text-warning remove-shared-btn" style="font-size: 22px;">' +
                        '<ion-icon name="remove-outline"></ion-icon>' +
                        '</span>' +
                        '</div>' +
                        '</li>');
            });
            $(".shared-invite").html("");
        }
    });

    $('body').delegate(".add-shared-btn", "click", function () {
        var name = $(this).parent().find(".shared-name").text();
        $(this).parent().parent().remove();
        $(".shared-invite").append('<li class="d-flex align-items-center px-1" data-share_id="'+$(this).parents('li').data('share_id')+'">' +
                '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                '<ion-icon name="person-outline"></ion-icon>' +
                '</span>' +
                '<div class="d-flex align-items-end justify-content-between w-100">' +
                '<span class="shared-name" style="font-size: 16px;">' + name + '</span>' +
                '<span class="text-warning remove-shared-btn" style="font-size: 22px;">' +
                '<ion-icon name="remove-outline"></ion-icon>' +
                '</span>' +
                '</div>' +
                '</li>');
    });

    $('body').delegate(".remove-shared-btn", "click", function () {
        var name = $(this).parent().find(".shared-name").text();
        $(this).parent().parent().remove();
        $(".shared-list").append('<li class="d-flex align-items-center px-1" data-share_id="'+$(this).parents('li').data('share_id')+'">' +
                '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                '<ion-icon name="person-outline"></ion-icon>' +
                '</span>' +
                '<div class="d-flex align-items-end justify-content-between w-100">' +
                '<span class="shared-name" style="font-size: 16px;">' + name + '</span>' +
                '<span class="text-warning add-shared-btn" style="font-size: 22px;">' +
                '<ion-icon name="add-outline"></ion-icon>' +
                '</span>' +
                '</div>' +
                '</li>');
    });

    $('body').delegate(".submit-shared", "click", function () {
        var value = $(".shared-invite").children();
        var data = $('.shared-invite li').map(function() {
            return $(this).data('share_id');
        }).get().join(':');
        $('#selected_users_p_sUID').val(data);
        $(".shared-final").append(value);
    })

    // *Shared with event

    // Facilities event

    $('body').delegate(".facilities-select", "click", function () {
        var staff = $(".facilities-list").children();
        var facilities = $(".facilities-invite").children();
        var value = $(".facilities-final").children();
        $(".facilities-invite").append(value);

        $(".close-facilities").click(function () {
            $(".facilities-list").html(staff);
            $(".facilities-invite").html(facilities);
            $(".facilities-final").html(value);
        });
    })

    $('body').delegate(".facilities-add-all", "click", function () {
        var value = $(".facilities-list").children();
        var enable = $(".facilities-list").html();
        if (enable != "") {
            $.each(value, function () {
                var name = $(this).find(".facilities-name").text();
                $(".facilities-invite").append('<li class="d-flex align-items-center px-1">' +
                        '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                        '<ion-icon name="person-outline"></ion-icon>' +
                        '</span>' +
                        '<div class="d-flex align-items-end justify-content-between w-100">' +
                        '<span class="facilities-name" style="font-size: 16px;">' + name + '</span>' +
                        '<span class="text-warning remove-facilities-btn" style="font-size: 22px;">' +
                        '<ion-icon name="remove-outline"></ion-icon>' +
                        '</span>' +
                        '</div>' +
                        '</li>');
            });
            $(".facilities-list").html("");
        }
    });

    $('body').delegate(".facilities-remove-all", "click", function () {
        var value = $(".facilities-invite").children();
        var enable = $(".facilities-invite").html();
        if (enable != "") {
            $.each(value, function () {
                var name = $(this).find(".facilities-name").text();
                $(".facilities-list").append('<li class="d-flex align-items-center px-1">' +
                        '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                        '<ion-icon name="facilities-outline"></ion-icon>' +
                        '</span>' +
                        '<div class="d-flex align-items-end justify-content-between w-100">' +
                        '<span class="facilities-name" style="font-size: 16px;">' + name + '</span>' +
                        '<span class="text-warning remove-facilities-btn" style="font-size: 22px;">' +
                        '<ion-icon name="remove-outline"></ion-icon>' +
                        '</span>' +
                        '</div>' +
                        '</li>');
            });
            $(".facilities-invite").html("");
        }
    });

    $('body').delegate(".add-facilities-btn", "click", function () {

        var name = $(this).parent().find(".facilities-name").text();
        $(this).parent().parent().remove();
        $(".facilities-invite").append('<li class="d-flex align-items-center px-1" data-equipment_id="'+$(this).parents('li').data('equipment_id')+'">' +
                '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                '<ion-icon name="person-outline"></ion-icon>' +
                '</span>' +
                '<div class="d-flex align-items-end justify-content-between w-100">' +
                '<span class="facilities-name" style="font-size: 16px;">' + name + '</span>' +
                '<span class="text-warning remove-facilities-btn" style="font-size: 22px;">' +
                '<ion-icon name="remove-outline"></ion-icon>' +
                '</span>' +
                '</div>' +
                '</li>');
    });

    $('body').delegate(".remove-facilities-btn", "click", function () {
        var name = $(this).parent().find(".facilities-name").text();
        $(this).parent().parent().remove();
        $(".facilities-list").append('<li class="d-flex align-items-center px-1" data-equipment_id="'+$(this).parents('li').data('equipment_id')+'">' +
                '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                '<ion-icon name="person-outline"></ion-icon>' +
                '</span>' +
                '<div class="d-flex align-items-end justify-content-between w-100">' +
                '<span class="facilities-name" style="font-size: 16px;">' + name + '</span>' +
                '<span class="text-warning add-facilities-btn" style="font-size: 22px;">' +
                '<ion-icon name="add-outline"></ion-icon>' +
                '</span>' +
                '</div>' +
                '</li>');
    });

    $('body').delegate(".submit-facilities", "click", function () {
        var value = $(".facilities-invite").children();
        var data = $('.facilities-invite li').map(function() {
            return $(this).data('equipment_id');
        }).get().join(':');
        $('#selected_users_sITEM').val(data);
        $(".facilities-final").append(value);
    })

    // *Facilities event

    var fileInput = document.getElementById('attachments');
    var listFile = document.getElementById('list_file');

    fileInput.onchange = function () {
        var files = Array.from(this.files);
        files = files.map(file => file.name);
        listFile.innerHTML = files.join('<br/>');
    }

    setTimeout(() => {
        notification('notification-welcome', 5000);
    }, 2000);
    $('.datepicker').pickadate({
         formatSubmit: 'yyyy-mm-dd',
         format: 'dd/mm/yyyy',
         onOpen: undefined,
    })
    $(function () {
       $('.bs-timepicker').timepicker();
    });
    $('.ul-drop li').click(function(){
        $('#menu_schedule').val($(this).attr('value'));
    })
    $('.search-attendees').click(function(){
        var keyword = $('#input_search_attendees').val();
        var department_id = $('#select_attendees').val();
        var member_id = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-attendee_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.attendees-list').html(html);
                }
        });
    })
    $('#select_attendees').change(function(){
        var keyword = $('#input_search_attendees').val();
        var department_id = $(this).val();
        var member_id = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-attendee_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.attendees-list').html(html);
                }
        });
    })
    $('.search-attendees').click(function(){
        var keyword = $('#input_search_attendees').val();
        var department_id = $('#select_attendees').val();
        var member_id = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-attendee_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.attendees-list').html(html);
                }
        });
    })
    $('#select_attendees').change(function(){
        var keyword = $('#input_search_attendees').val();
        var department_id = $(this).val();
        var member_id = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-attendee_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.attendees-list').html(html);
                }
        });
    })
    $('.search-share').click(function(){
        var keyword = $('#input_search_share').val();
        var department_id = $('#select_share').val();
        var member_id = $('.shared-invite li').map(function() {
            return $(this).data('share_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-share_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.shared-list').html(html);
                }
        });
    })
    $('#select_share').change(function(){
        var keyword = $('#input_search_share').val();
        var department_id = $(this).val();
        var member_id = $('.share-invite li').map(function() {
            return $(this).data('share_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-share_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.shared-list').html(html);
                }
        });
    })
     
     $(document).on('change', '#attachments', function () {
        var file_data = $(this).prop('files');
          var form_data = new FormData();
          for(let i=0;i<file_data.length;i++){
            form_data.append('file[]', file_data[i]);
          }
          form_data.append('type', {{\App\File::TYPE_SCHEDULE}});
          $.ajax({
                url: '/api/upload-files',
                type: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == 'true'){
                        if(response.success == 'true'){
                            response.data.forEach(element =>
                                $('#list_file_upload').append(`<div class="custom-control custom-checkbox mb-1">
                                    <input type="checkbox" class="custom-control-input" checked="checked" name="files[]" id="customCheckb1" value="`+element.id+`">
                                    <label class="custom-control-label" for="customCheckb1">`+element.name+` (`+element.size+`)</label>
                                </div>`)
                            )
                        }
                    }
                }
           });
     })
     $('#submit_form').click(function(e){
         e.preventDefault();
         $this = $(this);
         var start_date = $('input[name="start_date_submit"]').val();
         var start_time = $('input[name="start_time"]').val() || '00:00';
         var end_date = $('input[name="end_date_submit"]').val();
         var end_time = $('input[name="end_time"]').val() || '00:00';
         start_date_value = new Date(start_date+' '+start_time);
         end_date_value = new Date(end_date+' '+end_time);
         var check = 1;
         if(start_date_value.getTime() > end_date_value.getTime()){
            $('#notification').html('Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc');
            toastbox('toast-info',3000);
            check = 0;
         }
         if($('#subject').val() == ''){
            $('#notification').html('Tiêu đề trống');
            toastbox('toast-info',3000);
            check = 0;
         }
         if(check == 1){
             $this.parents('form').submit();
         }

     })
</script>
@stop
