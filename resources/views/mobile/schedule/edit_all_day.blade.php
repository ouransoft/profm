@extends('mobile.layouts.admin')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('frontend.schedule.index')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">{{ trans('base.Edit_schedule') }}</div>
</div>
<div id="appCapsule" class="header-active">
    <div class="section full my-2">
        <div class="section-title">
            <h2 class="text-warning">{{ trans('base.Edit_appointment') }}</h2>
        </div>
        <div class="wide-block">
            <form action="{{route('frontend.schedule.update',$schedule->id)}}" method="POST">
                <input type='hidden' name='menu' id='menu_schedule' value='{{$schedule->menu}}'>
                <input type='hidden' name='pattern' value="2">
                <input id="selected_users_sUID" type="hidden" name="selected_users_sUID" value="{{implode(':',$schedule->member->pluck('id')->toArray())}}">
                <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="reservation"><h4>{{ trans('base.Date') }}</h4></label>
                        <div class="fieldset__wrapper d-flex form-group">
                            <input id="" name="start_date" class="fieldset__input form-control datepicker w-60" value="{{date('d/m/Y',strtotime($schedule->start_date))}}" type="text" placeholder="Start date">
                        </div>
                        <div class="fieldset__wrapper d-flex form-group">
                            <input id="" name="end_date" class="fieldset__input form-control datepicker w-60" value="{{date('d/m/Y',strtotime($schedule->end_date))}}" type="text" placeholder="End date">
                        </div>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>{{ trans('base.Subject') }}</h4></label>
                        <div class="d-flex flex-row">
                            <dl id="order" class="dropdown w-25 mb-0">
                                <dt class="form-control select-center d-flex align-items-center justify-content-center px-1 w-100" style="overflow: hidden">
                                    @if(is_null($schedule->menu))
                                    <a href="#" class="text-dark d-flex justify-content-start align-items-center" style="font-size: 13px;">-----</a>
                                    @else
                                    <a href="#" class="text-dark d-flex justify-content-start align-items-center" style="font-size: 13px;">{{explode(';#',$schedule->menu)[0]}}</a>
                                    @endif
                                </dt>
                                <dd>
                                    <ul class="ul-drop" style="z-index: 1000; display: none;">
                                        <li value=""><a href="#" class="pl-1 text-dark">-----</a></li>
                                        <li value="Cuộc họp;#5">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-info rounded-0 menu-color5" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Cuộc họp</span>
                                            </a>
                                        </li>
                                        <li value="Hội thảo;#2">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-primary rounded-0 menu-color2" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Hội thảo</span>
                                            </a>
                                        </li>
                                        <li value="Đào tạo;#1">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-primary rounded-0 menu-color1" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Đào tạo</span>
                                            </a>
                                        </li>
                                        <li value="Công tác;#7">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-warning rounded-0 menu-color7" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Công tác</span>
                                            </a>
                                        </li>
                                        <li value="Ngày lễ;#6">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-info rounded-0 menu-color6" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Ngày lễ</span>
                                            </a>
                                        </li>
                                        <li value="Phỏng vấn;#3">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-danger rounded-0 menu-color3" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Phỏng vấn</span>
                                            </a>
                                        </li>
                                        <li value="Gặp gỡ;#4">
                                            <a href="#" class="d-flex align-items-center pl-1">
                                                <div class="chip chip-success rounded-0 menu-color4" style="height: 16px; width: 16px;">
                                                    <span class="chip-label"></span>
                                                </div>
                                                <span class="text-dark text-truncate">Gặp gỡ</span>
                                            </a>
                                        </li>
                                    </ul>
                                </dd>
                            </dl>
                            <input type="text" name='title' value='{{$schedule->title}}' class="form-control w-75 ml-1" id="subject" required="">
                        </div>
                    </div>
                </div>
                <div class="form-group boxed pt-0">
                    <div class="input-wrapper">
                        <label class="label" for="attendees"><h4>{{ trans('base.Attendees') }}</h4></label>
                        <div class="card my-2" id="attendees">
                            <h4 class="attendees-select bg-warning mb-0 py-2 px-2 text-light rounded-top d-flex align-items-center justify-content-between" data-toggle="modal" data-target="#insert-attendees">
                                {{ trans('base.list_attendees') }}
                                <ion-icon class="" name="create-outline" style="font-size:24px"></ion-icon>
                            </h4>
                            <div class="border user-list">
                                <ul class="listview attendees-final">
                                    @foreach($schedule->member as $key=>$val)
                                    <li class="d-flex align-items-center px-1" data-attendee_id="{{$val->id}}">
                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                            <ion-icon name="person-outline" role="img" class="md hydrated" aria-label="person outline"></ion-icon>
                                        </span>
                                        <div class="d-flex align-items-end justify-content-between w-100">
                                            <span class="attendees-name" style="font-size: 16px;">{{$val->full_name}}</span>
                                            <span class="text-warning remove-attendees-btn" style="font-size: 22px;">
                                                <ion-icon name="remove-outline" role="img" class="md hydrated" aria-label="remove outline"></ion-icon>
                                            </span>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="modal fade modalbox" id="insert-attendees" data-backdrop="static" tabindex="-1" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-warning">{{ trans('base.Add_attendees') }}</h5>
                                        <a href="javascript:;" data-dismiss="modal" class="close-attendees">{{ trans('base.Close') }}</a>
                                    </div>
                                    <div class="modal-body">
                                        <div class="input-group rounded">
                                            <input type="search" id='input_search_attendees' class="form-control rounded" placeholder="{{ trans('base.Staff_search') }}" aria-label="Search" aria-describedby="search-addon"/>
                                            <button type='button' class="search-attendees search-position input-group-text border-0" id="search-addon">
                                                <ion-icon name="search-outline" role="img" class="md hydrated text-warning" aria-label="search outline"></ion-icon>
                                            </button>
                                        </div>
                                        <div class="form-group my-2">
                                            <select id='select_attendees' class="custom-select">
                                                <option selected value=''>Tất cả</option>
                                                @foreach($departments as $key=>$department)
                                                <option value='{{$department->id}}'>{{$department->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <h4 class="attendees-add-all text-warning mt-2 d-flex align-items-center">
                                            <ion-icon name="add-outline" style="font-size: 20px;"></ion-icon>
                                            Thêm tất cả
                                        </h4>
                                        <div class="card">
                                            <h4 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">{{ trans('base.list_staff') }}</h4>
                                            <div class="border user-list">
                                                <ul class="listview attendees-list">
                                                    @foreach($members as $key=>$val)
                                                    <li class="d-flex align-items-center px-1" data-attendee_id="{{$val->id}}">
                                                        <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                                            <ion-icon name="person-outline"></ion-icon>
                                                        </span>
                                                        <div class="d-flex align-items-end justify-content-between w-100">
                                                            <span class="attendees-name" style="font-size: 16px;">{{$val->full_name}}</span>
                                                            <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                                                <ion-icon name="add-outline"></ion-icon>
                                                            </span>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <h4 class="attendees-remove-all text-warning mt-2 d-flex align-items-center">
                                            <ion-icon name="remove-outline" style="font-size: 20px;"></ion-icon>
                                            Xóa tất cả
                                        </h4>
                                        <div class="card mt-1">
                                            <h4 class="bg-warning mb-0 py-2 pl-2 text-light rounded-top">{{ trans('base.list_attendees') }}</h4>
                                            <div class="border user-list">
                                                <ul class="listview attendees-invite">

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card mt-1">
                                            <button type="button" class="submit-attendees btn btn-primary mr-1 mb-1" data-dismiss="modal">{{ trans('base.Submit') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group-boxed">
                    <div class="input-wrapper">
                        <a href="javascript:;" class="text-dark" type="button" data-toggle="collapse" data-target="#accordion1" aria-expanded="false">
                            <h4 class="d-flex align-items-center">
                                <span>{{ trans('base.Add_company_information') }}</span>
                                <ion-icon class="text-warning" name="caret-down-outline"></ion-icon>
                            </h4>
                        </a>
                        <div id="accordion1" class="accordion-body collapse">
                            <div class="accordion-content">
                                <div class="wide-block border-0 pb-2">
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="label" for="company-name">{{ trans('base.Company_name') }}</label>
                                            <input type="text" name='company_name' value="{{$schedule->company_name}}" class="form-control" id="company-name" placeholder="Enter company name">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="label" for="address">{{ trans('base.Address') }}</label>
                                            <input type="text" name='physical_address' value="{{$schedule->physical_address}}" class="form-control" id="address" placeholder="Enter address">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="label" for="company-phone-number">{{ trans('base.Company_phone_number') }}</label>
                                            <input type="text" name='company_telephone_number' value="{{$schedule->company_telephone_number}}" class="form-control" id="company-phone-number" placeholder="Enter company phone number">
                                            <i class="clear-input">
                                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                            </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label"><h4>{{ trans('base.Notes') }}</h4></label>
                        <textarea name='memo' rows="2" class="form-control">{{$schedule->memo}}</textarea>
                    </div>
                </div>
                <div class="form-group boxed py-3">
                    <div class="input-wrapper">
                        <label class="label" for="attachments">
                            <h4 class="d-flex align-items-center">
                                <span>{{ trans('base.Attachments') }}</span>
                                <ion-icon class="text-warning" name="attach-outline" style="font-size: 22px;"></ion-icon>
                            </h4>
                        </label>
                        <input id="attachments" type="file" multiple>
                        <div id='list_file_upload'>
                            @if(count($schedule->files) > 0)
                            @foreach($schedule->files as $key=>$value)
                             <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" checked="checked" name="upload_fileids[]" id="customCheckb1" value="{{$value->id}}">
                                <label class="custom-control-label" for="customCheckb1">{{$value->name}} ({{$value->size}})</label>
                             </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group boxed pt-0">
                    <div class="input-wrapper">
                        <label class="label"><h4>{{ trans('base.Visibility') }}</h4></label>
                        <div class="d-flex justify-content-center">
                            <div class="custom-control custom-radio mb-1 mr-3">
                                <input type="radio" id="public" name="private" class="custom-control-input" value='0' @if($schedule->private == 0) checked @endif>
                                <label class="custom-control-label" for="public">{{ trans('base.Public') }}</label>
                            </div>
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" id="private" name="private" class="custom-control-input" value='1' @if($schedule->private == 1) checked @endif>
                                <label class="custom-control-label" for="private">{{ trans('base.Private') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed pt-0">
                    <div class="input-wrapper">
                        <label class="label"><h4>Lịch công ty</h4></label>
                        <div class="d-flex justify-content-between">
                            <div class="custom-control custom-radio mb-1">
                                <input type="checkbox" id="pd" name="public_dashboard" class="custom-control-input" value='1'>
                                <label class="custom-control-label pl-0" for="pd">Hiển thị</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center py-3">
                    <button type="submit" class="btn btn-primary mr-2 mb-1 w-25 px-0" id="submit_form">{{ trans('base.Update') }}</button>
                    <a href="{{route('frontend.schedule.index')}}" class="btn btn-warning mr-1 mb-1 w-25">{{ trans('base.Cancel') }}</a>
                </div>
            </form>
        </div>

    </div>
</div>
@stop
@section('script')
@parent
<script>
    $(".dropdown dt a").click(function () {
        $(".dropdown dd ul").toggle();
    });

    $(".dropdown dd ul li a").click(function () {
        var text = $(this).html();
        $(".dropdown dt a").html(text);
        $(".dropdown dd ul").hide();
        if (order != getSelectedValue("order")) {
            order = getSelectedValue("order");
        }
    });

    function getSelectedValue(id) {
        return $("#" + id).find("dt a span.value").html();
    }

    $('body').delegate(".attendees-select", "click", function () {
        var staff = $(".attendees-list").children();
        var attendees = $(".attendees-invite").children();
        var value = $(".attendees-final").children();
        $(".attendees-invite").append(value);

        $(".close-attendees").click(function () {
            $(".attendees-list").html(staff);
            $(".attendees-invite").html(attendees);
            $(".attendees-final").html(value);
        });
    })

    $('body').delegate(".attendees-add-all", "click", function () {
        var value = $(".attendees-list").children();
        var enable = $(".attendees-list").html();
        if (enable != "") {
            $.each(value, function () {
                var name = $(this).find(".attendees-name").text();
                $(".attendees-invite").append('<li class="d-flex align-items-center px-1">' +
                        '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                        '<ion-icon name="person-outline"></ion-icon>' +
                        '</span>' +
                        '<div class="d-flex align-items-end justify-content-between w-100">' +
                        '<span class="attendees-name" style="font-size: 16px;">' + name + '</span>' +
                        '<span class="text-warning remove-attendees-btn" style="font-size: 22px;">' +
                        '<ion-icon name="remove-outline"></ion-icon>' +
                        '</span>' +
                        '</div>' +
                        '</li>');
            });
            $(".attendees-list").html("");
        }
    });

    $('body').delegate(".attendees-remove-all", "click", function () {
        var value = $(".attendees-invite").children();
        var enable = $(".attendees-invite").html();
        if (enable != "") {
            $.each(value, function () {
                var name = $(this).find(".attendees-name").text();
                $(".attendees-list").append('<li class="d-flex align-items-center px-1">' +
                        '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                        '<ion-icon name="person-outline"></ion-icon>' +
                        '</span>' +
                        '<div class="d-flex align-items-end justify-content-between w-100">' +
                        '<span class="attendees-name" style="font-size: 16px;">' + name + '</span>' +
                        '<span class="text-warning add-attendees-btn" style="font-size: 22px;">' +
                        '<ion-icon name="add-outline"></ion-icon>' +
                        '</span>' +
                        '</div>' +
                        '</li>');
            });
            $(".attendees-invite").html("");
        }
    });

    $('body').delegate(".add-attendees-btn", "click", function () {
        var name = $(this).parent().find(".attendees-name").text();
        $(this).parent().parent().remove();
        $(".attendees-invite").append('<li class="d-flex align-items-center px-1" data-attendee_id='+$(this).parents('li').data('attendee_id')+'>' +
                '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                '<ion-icon name="person-outline"></ion-icon>' +
                '</span>' +
                '<div class="d-flex align-items-end justify-content-between w-100">' +
                '<span class="attendees-name" style="font-size: 16px;">' + name + '</span>' +
                '<span class="text-warning remove-attendees-btn" style="font-size: 22px;">' +
                '<ion-icon name="remove-outline"></ion-icon>' +
                '</span>' +
                '</div>' +
                '</li>');
    });

    $('body').delegate(".remove-attendees-btn", "click", function () {
        var name = $(this).parent().find(".attendees-name").text();
        $(this).parent().parent().remove();
        $(".attendees-list").append('<li class="d-flex align-items-center px-1" data-attendee_id='+$(this).parents('li').data('attendee_id')+'>' +
                '<span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">' +
                '<ion-icon name="person-outline"></ion-icon>' +
                '</span>' +
                '<div class="d-flex align-items-end justify-content-between w-100">' +
                '<span class="attendees-name" style="font-size: 16px;">' + name + '</span>' +
                '<span class="text-warning add-attendees-btn" style="font-size: 22px;">' +
                '<ion-icon name="add-outline"></ion-icon>' +
                '</span>' +
                '</div>' +
                '</li>');
    });

    $('body').delegate(".submit-attendees", "click", function () {
        var value = $(".attendees-invite").children();
        var data = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $('#selected_users_sUID').val(data);
        $(".attendees-final").append(value);
    })

    // *Attendees event

    var fileInput = document.getElementById('attachments');
    var listFile = document.getElementById('list_file');

    fileInput.onchange = function () {
        var files = Array.from(this.files);
        files = files.map(file => file.name);
        listFile.innerHTML = files.join('<br/>');
    }

    setTimeout(() => {
        notification('notification-welcome', 5000);
    }, 2000);
    $('.datepicker').pickadate({
         formatSubmit: 'yyyy-mm-dd',
         format: 'dd/mm/yyyy',
         onOpen: undefined,
    })
    $(function () {
       $('.bs-timepicker').timepicker();
    });
    $('.ul-drop li').click(function(){
        $('#menu_schedule').val($(this).attr('value'));
    })
    $('.search-attendees').click(function(){
        var keyword = $('#input_search_attendees').val();
        var department_id = $('#select_attendees').val();
        var member_id = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-attendee_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.attendees-list').html(html);
                }
        });
    })
    $('#select_attendees').change(function(){
        var keyword = $('#input_search_attendees').val();
        var department_id = $(this).val();
        var member_id = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-attendee_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.attendees-list').html(html);
                }
        });
    })
    $('.search-attendees').click(function(){
        var keyword = $('#input_search_attendees').val();
        var department_id = $('#select_attendees').val();
        var member_id = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-attendee_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.attendees-list').html(html);
                }
        });
    })
    $('#select_attendees').change(function(){
        var keyword = $('#input_search_attendees').val();
        var department_id = $(this).val();
        var member_id = $('.attendees-invite li').map(function() {
            return $(this).data('attendee_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-attendee_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.attendees-list').html(html);
                }
        });
    })
    $('.search-share').click(function(){
        var keyword = $('#input_search_share').val();
        var department_id = $('#select_share').val();
        var member_id = $('.shared-invite li').map(function() {
            return $(this).data('share_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-share_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.shared-list').html(html);
                }
        });
    })
    $('#select_share').change(function(){
        var keyword = $('#input_search_share').val();
        var department_id = $(this).val();
        var member_id = $('.share-invite li').map(function() {
            return $(this).data('share_id');
        }).get().join(':');
        $.ajax({
               type: "POST",
               url: '/api/search-attendees',
               data: {keyword:keyword,member_id:member_id,department_id:department_id},
               success: function(response){
                    html = '';
                    $.each( response.data, function( key, value ) {
                        html +=`<li class="d-flex align-items-center px-1" data-share_id="`+value.id+`">
                                    <span class="d-flex align-items-end text-warning mx-2" style="font-size: 20px;">
                                        <ion-icon name="person-outline"></ion-icon>
                                    </span>
                                    <div class="d-flex align-items-end justify-content-between w-100">
                                        <span class="attendees-name" style="font-size: 16px;">`+value.full_name+`</span>
                                        <span class="text-warning add-attendees-btn" style="font-size: 22px;">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </span>
                                    </div>
                                </li>`;
                    });
                    $('.shared-list').html(html);
                }
        });
    })
    $(document).on('change', '#attachments', function () {
        var file_data = $(this).prop('files');
          var form_data = new FormData();
          for(let i=0;i<file_data.length;i++){
            form_data.append('file[]', file_data[i]);
          }
          form_data.append('type', {{\App\File::TYPE_SCHEDULE}});
          $.ajax({
                url: '/api/upload-files',
                type: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response){
                    if(response.success == 'true'){
                        if(response.success == 'true'){
                            response.data.forEach(element =>
                                $('#list_file_upload').append(`<div class="custom-control custom-checkbox mb-1">
                                    <input type="checkbox" class="custom-control-input" checked="checked" name="files[]" id="customCheckb1" value="`+element.id+`">
                                    <label class="custom-control-label" for="customCheckb1">`+element.name+` (`+element.size+`)</label>
                                </div>`)
                            )
                        }
                    }
                }
           });
     })
     $('#submit_form').click(function(e){
         e.preventDefault();
         $this = $(this);
         var start_date = $('input[name="start_date_submit"]').val();
         var end_date = $('input[name="end_date_submit"]').val();
         start_date_value = new Date(start_date);
         end_date_value = new Date(end_date);
         var check = 1;
         if(start_date_value.getTime() > end_date_value.getTime()){
            $('#notification').html('Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc');
            toastbox('toast-info',3000);
            check = 0;
         }
         if($('#subject').val() == ''){
            $('#notification').html('Tiêu đề trống');
            toastbox('toast-info',3000);
            check = 0;
         }
         if(check == 1){
             $this.parents('form').submit();
         }

     })
</script>
@stop
