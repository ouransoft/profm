<!DOCTYPE html>
<html lang="en">
    @include('mobile/layouts/__head')
    <body>
    <a href="{{route('frontend.schedule.create')}}" class="button btn-warning insert-fixed-btn show px-1">
        <ion-icon name="add" role="img" class="md hydrated" aria-label="arrow up outline"></ion-icon>
    </a>
<!-- App Capsule -->
<div id="appCapsule" class="py-0 fixed-scroll" style="background-color: #000">
    <div class="section full schedule-calendar">
        <div class="wide-block calendar-bg d-flex justify-content-center">
            <div id="calendar"></div>
            <div class="modal fade dialogbox" id="accordion" data-backdrop="static" tabindex="-1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">{{ trans('base.Search') }}</h5>
                        </div>
                        <form action='{{route('frontend.schedule.index')}}' method="get">
                            <div class="modal-body text-left mb-2">
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <input id='tags' type="text" list="languages" name='keyword' class="form-control search-area" placeholder="{{ trans('base.Enter_staff_name') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-inline">
                                    <button type="button" class="btn btn-text-secondary" data-dismiss="modal">{{ trans('base.Close') }}</button>
                                    <button type="submit" class="btn btn-text-primary">{{ trans('base.Search') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section full" id="schedule-workbench">
        <div class="wide-block border-0 pr-0 pl-1">
            <div class="bar-cover">
                <div class="bg-dark bar"></div>
            </div>
            <div class="section-title fs-22 pl-0">
                <span class="py-1 pl-2 pr-3" id="title_weekend">{{strtoupper(date('l',strtotime($date_now))).' '.date('d',strtotime($date_now))}}</span>
            </div>
            <!-- timeline -->
            <div class="block-out border-top"></div>
            <div class="timeline timed pt-4 pb-5" id="list_schedule">
                @foreach($schedules as $key=>$schedule)
                <div class="item mb-4">
                    <a href="{{route('frontend.schedule.view',$schedule->id)}}">
                    @if($schedule->pattern == 2)
                    <div class="d-flex flex-column time time-area mr-2">
                        <span class="fs-16 text-muted">[All day]</span>
                    </div>
                    @else
                    <div class="d-flex flex-column time time-area mr-2">
                        <span class="fs-16 pb-2 text-muted">{{date('H:i A',strtotime($schedule->start_date))}}</span>
                        <span class="fs-16 text-muted">{{date('H:i A',strtotime($schedule->end_date))}}</span>
                    </div>
                    @endif
                    <div class="dot {{((!is_null($schedule->menu)) ?  'bg-color'.explode(';#',$schedule->menu)[1] : 'bg-white')}}"></div>
                    <div class="content">
                        <h4 class="title fs-16">{{$schedule->title}}</h4>
                        <span class="text-muted fs-16">{{$schedule->memo}}</span>
                    </div>
                    </a>
                </div>
                @endforeach
            </div>
            <!-- * timeline -->
        </div>
    </div>
</div>
@include('mobile/layouts/__footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
    $.ajaxSetup({
        headers: {
          'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
        }
    });
    $( function() {
    $( "#tags" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "/api/getListName",
          method:"post",
          data: {
            term: request.term
          },
          success: function(data) {
             response(data);
             
          }
        } )
    }
  } );
    });
  
  !function() {

  var today = moment();

  function Calendar(selector, events) {
    this.el = document.querySelector(selector);
    this.events = events;
    this.current = moment().date(1);
    this.draw();
    var current = document.querySelector('.today');
    if(current) {
      var self = this;
//      window.setTimeout(function() {
//        self.openDay(current);
//      }, 500);
      $('.today').parent('.week').addClass('active');
    }
  }

  Calendar.prototype.draw = function() {
    //Create Header
    this.drawHeader();
    this.drawHead();
    //Draw Month
    this.drawMonth();

    //this.drawLegend();
  }

  Calendar.prototype.drawHeader = function() {
    var self = this;
    if(!this.header) {
      this.header = createElement('div', 'header');
      this.header.className = 'header';

      this.title = createElement('h1');

      var left = createElement('div', 'left');
      left.addEventListener('click', function() { self.prevMonth(); });

      var right = createElement('div', 'right');
      right.addEventListener('click', function() { self.nextMonth(); });

      var left = createElement('div', 'left');
      left.addEventListener('click', function() { self.prevMonth(); });

      var search = createElement('ion-icon', 'search fs-28 text-light');
    //   search.addEventListener('click', function() {});

      //Append the Elements
      this.header.appendChild(this.title);
      this.header.appendChild(left);
      this.header.appendChild(right);
      this.header.appendChild(search);
      this.el.appendChild(this.header);
    }

    this.title.innerHTML = this.current.format('MMMM YYYY');
  }

  Calendar.prototype.drawHead = function() {
      var self = this;
      if(!this.head) {
      this.head = createElement('div', 'head');
      this.head.className = 'head';
      this.el.appendChild(this.head);
      this.head.innerHTML = `<div class="week">
                                <div class="weekday">
                                    <div class="day-name">Sun</div>
                                </div>
                                <div class="weekday ">
                                    <div class="day-name">Mon</div>
                                </div>
                                <div class="weekday ">
                                    <div class="day-name">Tue</div>
                                </div>
                                <div class="weekday">
                                    <div class="day-name">Wed</div>
                                </div>
                                <div class="weekday">
                                    <div class="day-name">Thu</div>
                                </div>
                                <div class="weekday">
                                    <div class="day-name">Fri</div>
                                </div>
                                <div class="weekday">
                                    <div class="day-name">Sat</div>
                                </div>
                            </div>`;
     }
  }
  Calendar.prototype.drawMonth = function() {
    var self = this;
    if(this.events.length > 0){
        this.events.forEach(function(ev) {
         ev.date = self.current.clone().date(Math.random() * (29 - 1) + 1);
        });
    }
    if(this.month) {
      this.oldMonth = this.month;
      this.oldMonth.className = 'month out ' + (self.next ? 'next' : 'prev');
      this.oldMonth.addEventListener('webkitAnimationEnd', function() {
        self.oldMonth.parentNode.removeChild(self.oldMonth);
        self.month = createElement('div', 'month');
        self.backFill();
        self.currentMonth();
        self.fowardFill();
        self.el.appendChild(self.month);
        window.setTimeout(function() {
          self.month.className = 'month in ' + (self.next ? 'next' : 'prev');
        }, 16);
      });
    } else {
        this.month = createElement('div', 'month');
        this.el.appendChild(this.month);
        this.backFill();
        this.currentMonth();
        this.fowardFill();
        this.month.className = 'month new';
    }
  }

  Calendar.prototype.backFill = function() {
    var clone = this.current.clone();
    var dayOfWeek = clone.day();

    if(!dayOfWeek) { return; }

    clone.subtract('days', dayOfWeek+1);

    for(var i = dayOfWeek; i > 0 ; i--) {
      this.drawDay(clone.add('days', 1));
    }
  }

  Calendar.prototype.fowardFill = function() {
    var clone = this.current.clone().add('months', 1).subtract('days', 1);
    var dayOfWeek = clone.day();

    if(dayOfWeek === 6) { return; }

    for(var i = dayOfWeek; i < 6 ; i++) {
      this.drawDay(clone.add('days', 1));
    }
  }

  Calendar.prototype.currentMonth = function() {
    var clone = this.current.clone();

    while(clone.month() === this.current.month()) {
      this.drawDay(clone);
      clone.add('days', 1);
    }
  }

  Calendar.prototype.getWeek = function(day) {
    if(!this.week || day.day() === 0) {
      local = new Date(day);
      start_day_week = moment(local).format('MM/DD/YYYY');
      end_day_week = moment(start_day_week).add(6, 'days').format('MM/DD/YYYY');
      date_now = moment().format('MM/DD/YYYY');
      if(start_day_week < date_now && date_now < end_day_week){
          this.week = createElement('div', 'week active');
      }else{
          this.week = createElement('div', 'week');
      }
      this.month.appendChild(this.week);
    }
  }

  Calendar.prototype.drawDay = function(day) {
    var self = this;
    this.getWeek(day);
    //Outer Day
    var outer = createElement('div', this.getDayClass(day));
    //outer.addEventListener('click', function() {
    //  self.openDay(this);
    //});
    //Day Name
    //Day Number
    if(day.format('d') == 0){
        var number = createElement('div', 'day-number sunday', day.format('DD'));
    }else{
        var number = createElement('div', 'day-number', day.format('DD'));
    }
    outer.setAttribute("data-date", day.format('YYYY-MM-DD'));
    //Events
    var events = createElement('div', 'day-events');
    var all_day_events = createElement('div', 'all-day-events');
    this.drawEvents(day, events);
    outer.appendChild(number);
    outer.appendChild(events);
    this.week.appendChild(outer);
    var event_all_day = createElement('div', 'day-events');
    if(day.format('d') == 6){
        this.drawAllDayEvents(day, all_day_events);
        event_all_day.appendChild(all_day_events);
        this.week.appendChild(event_all_day);
    }
  }

  Calendar.prototype.drawEvents = function(day, element) {
    if(day.month() === this.current.month() && this.events.length > 0) {
        var todaysEvents = this.events.reduce(function(memo, ev) {
            if(ev.pattern != 2){ 
                var local = new Date(day);
                var current_day = moment(local).format('MM/DD/YYYY');
                var current_date = new Date(current_day);
                var start_date = new Date(ev.start_date);
                var end_date = new Date(ev.end_date);
                if(current_date.getTime() >= start_date.getTime() && current_date.getTime() <= end_date.getTime()) {
                  memo.push(ev);
                }
            }
            return memo;
          }, []);

      todaysEvents.forEach(function(ev) {
        var evSpan = createElement('span', ev.color);
        element.appendChild(evSpan);
      });
    }else{
       var todaysEvents = this.events.reduce(function(memo) {
            return memo;
          }, []);
    }
  }
  Calendar.prototype.drawAllDayEvents = function(day, element) {
    var datas = this.events;
    var todaysEvents = this.events.reduce(function(memo, ev,index,datas){
        if(ev.pattern == 2){
            var local = new Date(day);
            var start_date = moment(ev.start_date).format("YYYY-MM-DD");
            var end_date = moment(ev.end_date).format("YYYY-MM-DD");
            var start_day =  moment(local).day(0).format("YYYY-MM-DD");
            var end_day = moment(local).day(7).format("YYYY-MM-DD");
            if(start_date <= end_day && end_date >= start_day) {
               memo.push(ev);
            }
        }
        return memo;
      }, []);
    todaysEvents.forEach(function(ev) {
        var local = new Date(day);
        var start_date = moment(ev.start_date);
        var end_date = moment(ev.end_date);
        var start_day =  moment(local).day(0);
        var end_day = moment(local).day(7);
        var evDiv =  createElement('div','allday');
        var sub_start = start_date.diff(start_day, 'days') + 1;
        var sub_end = end_date.diff(end_day, 'days') + 1;
        if(sub_start > 0) {
            var evSpan = createElement('span','col_'+sub_start);
            evDiv.appendChild(evSpan);
        }else{
            sub_start = 0;
        }
        if(sub_end >= 0){
            var evSpan = createElement('span', ev.color+' col_'+ (7 - sub_start));
            evDiv.appendChild(evSpan); 
        }else{
            var evSpan1 = createElement('span', ev.color+ ' col_'+(7 - sub_start + sub_end));
            var evSpan2 = createElement('span','col_'+(-sub_start));
            evDiv.appendChild(evSpan1);
            evDiv.appendChild(evSpan2);
        }
        element.appendChild(evDiv);
    });
  }

  Calendar.prototype.getDayClass = function(day) {
    classes = ['day'];
    if(day.month() !== this.current.month()) {
      classes.push('other');
    } else if (today.isSame(day, 'day')) {
      classes.push('today');
    }
    return classes.join(' ');
  }

  Calendar.prototype.openDay = function(el) {
    var details, arrow;
    var dayNumber = +el.querySelectorAll('.day-number')[0].innerText || +el.querySelectorAll('.day-number')[0].textContent;
    var day = this.current.clone().date(dayNumber);

    var currentOpened = document.querySelector('.details');

    //Check to see if there is an open detais box on the current row
    if(currentOpened && currentOpened.parentNode === el.parentNode) {
      details = currentOpened;
      arrow = document.querySelector('.arrow');
    } else {
      //Close the open events on differnt week row
      //currentOpened && currentOpened.parentNode.removeChild(currentOpened);
      if(currentOpened) {
        currentOpened.addEventListener('webkitAnimationEnd', function() {
          currentOpened.parentNode.removeChild(currentOpened);
        });
        currentOpened.addEventListener('oanimationend', function() {
          currentOpened.parentNode.removeChild(currentOpened);
        });
        currentOpened.addEventListener('msAnimationEnd', function() {
          currentOpened.parentNode.removeChild(currentOpened);
        });
        currentOpened.addEventListener('animationend', function() {
          currentOpened.parentNode.removeChild(currentOpened);
        });
        currentOpened.className = 'details out';
      }

      //Create the Details Container
      details = createElement('div', 'details in');

      //Create the arrow
      var arrow = createElement('div', 'arrow');

      //Create the event wrapper

      details.appendChild(arrow);
      el.parentNode.appendChild(details);
    }
    var todaysEvents = this.events.reduce(function(memo, ev){
      var local = new Date(day);
      var current_day = moment(local).format('MM/DD/YYYY');
      var current_date = new Date(current_day);
      var start_date = new Date(ev.start_date);
      var end_date = new Date(ev.end_date);
      if(current_date.getTime() >= start_date.getTime() && current_date.getTime() <= end_date.getTime()) {
        memo.push(ev);
      }
      return memo;
    }, []);

    this.renderEvents(todaysEvents, details);

    arrow.style.left = el.offsetLeft - el.parentNode.offsetLeft + 27 + 'px';
  }

  Calendar.prototype.renderEvents = function(events, ele) {
    //Remove any events in the current details element
    var currentWrapper = ele.querySelector('.events');
    var wrapper = createElement('div', 'events in' + (currentWrapper ? ' new' : ''));

    events.forEach(function(ev) {
      var div = createElement('div', 'event');
      var square = createElement('div', 'event-category ' + ev.color);
      var span = createElement('span', '', ev.eventName);

      div.appendChild(square);
      div.appendChild(span);
      wrapper.appendChild(div);
    });

    if(!events.length) {
      var div = createElement('div', 'event empty');
      var span = createElement('span', '', 'No Events');

      div.appendChild(span);
      wrapper.appendChild(div);
    }

    if(currentWrapper) {
      currentWrapper.className = 'events out';
      currentWrapper.addEventListener('webkitAnimationEnd', function() {
        currentWrapper.parentNode.removeChild(currentWrapper);
        ele.appendChild(wrapper);
      });
      currentWrapper.addEventListener('oanimationend', function() {
        currentWrapper.parentNode.removeChild(currentWrapper);
        ele.appendChild(wrapper);
      });
      currentWrapper.addEventListener('msAnimationEnd', function() {
        currentWrapper.parentNode.removeChild(currentWrapper);
        ele.appendChild(wrapper);
      });
      currentWrapper.addEventListener('animationend', function() {
        currentWrapper.parentNode.removeChild(currentWrapper);
        ele.appendChild(wrapper);
      });
    } else {
      ele.appendChild(wrapper);
    }
  }

  /*Calendar.prototype.drawLegend = function() {
    var legend = createElement('div', 'legend');
    var calendars = this.events.map(function(e) {
      return e.calendar + '|' + e.color;
    }).reduce(function(memo, e) {
      if(memo.indexOf(e) === -1) {
        memo.push(e);
      }
      return memo;
    }, []).forEach(function(e) {
      var parts = e.split('|');
      var entry = createElement('span', 'entry ' +  parts[1], parts[0]);
      legend.appendChild(entry);
    });
    this.el.appendChild(legend);
  }*/

  Calendar.prototype.nextMonth = function() {
    this.current.add('months', 1);
    $this = this;
    var local = new Date(this.current);
    var current_day = moment(local).format('YYYY-MM-DD');
    $.ajax({
            url: '/api/get-schedule-by-month',
            method: 'POST',
            data: {date: current_day},
            success: function (response) {
                $this.events = response.data;
                $this.next = true;
                $this.draw();
            }
        })

  }

  Calendar.prototype.prevMonth = function() {
    this.current.subtract('months', 1);
    $this = this;
    var local = new Date(this.current);
    var current_day = moment(local).format('YYYY-MM-DD');
    $.ajax({
            url: '/api/get-schedule-by-month',
            method: 'POST',
            data: {date: current_day},
            success: function (response) {
                $this.events = response.data;
                $this.next = true;
                $this.draw();
            }
        })
  }

  window.Calendar = Calendar;

  function createElement(tagName, className, innerText) {
    var ele = document.createElement(tagName);
    if(className) {
      ele.className = className;
    }
    if(innerText) {
      ele.innderText = ele.textContent = innerText;
    }
    return ele;
  }
}();

!function() {
  var data = {!!$list_schedule_mobile!!};



  function addDate(ev) {

  }

  var calendar = new Calendar('#calendar', data);

}();
$('.search').attr('name','search');

$('.search').attr('data-toggle','modal');
$('.search').attr('data-target','#accordion');
$('#list_schedule').outerHeight($(window).height() - $('#calendar .header').outerHeight() - $('#calendar .head').outerHeight() - $('#calendar .week').outerHeight() - $('.appBottomMenu').outerHeight() - $('.bar-cover').outerHeight() - $('.section-title').outerHeight());
$('body').delegate('.day','click',function(){
    member_id = {!!$member_id!!}
    $('.day').removeClass('active');
    $('.week').removeClass('active');
    $(this).addClass('active');
    $(this).parent('.week').addClass('active');
    var date = $(this).data('date');
    if($(".bar-cover").hasClass('active')){
        $(".month").animate({height:$('.week.active').height()},300);
        setTimeout(function(){ $(".week.active").addClass('fix') }, 250);
    }else{
        $(".month").animate({height:$('.week.active').height()},300);
        setTimeout(function(){ $(".week.active").addClass('fix') }, 250);
        $(".bar-cover").addClass('active');
    }
    $.ajax({
        url: '/api/load-schedule-day',
        method: 'POST',
        data: {date: date,member_id:member_id},
        success: function (response) {
            $('#title_weekend').html(response.title);
            $('#list_schedule').html(response.html);
        }
    })
})

function autoHeightAnimate(element, time){
  var curHeight = element.height(), // Get Default Height
      autoHeight = element.css('height', 'auto').height(); // Get Auto Height
      element.height(curHeight); // Reset to Default Height
      element.stop().animate({ height: autoHeight+curHeight }, time); // Animate to Auto Height
}
$('body').delegate(".bar-cover","click", function(){
    if($(this).hasClass('active')){
        $(this).removeClass('active');
        autoHeightAnimate($(".month"),300)
        setTimeout(function(){ $(".week.active").removeClass('fix') }, 250);
    }else{
        $(this).addClass('active');
        $(".month").animate({height:$('.week.active').height()},300);
        setTimeout(function(){ $(".week.active").addClass('fix') }, 250);
    }
});
document.addEventListener('swiped-right', function(e) {
    $('.left').click(); // the element that was swiped
});
document.addEventListener('swiped-left', function(e) {
    $('.right').click(); // the element that was swiped
});

document.addEventListener('swiped-up', function(e) {
    if(!$(".bar-cover").hasClass('active')){
        $(".month").animate({height:$('.week.active').height()},300);
        setTimeout(function(){ $(".week.active").addClass('fix') }, 250);
        $(".bar-cover").addClass('active');
    }
});
document.addEventListener('swiped-down', function(e) {
    if($('#list_schedule').scrollTop() === 0){
        if($(".bar-cover").hasClass('active')){
            $(".bar-cover").removeClass('active');
            autoHeightAnimate($(".month"),200)
            setTimeout(function(){ $(".week.active").removeClass('fix') }, 150);
        }
    }
});
</script>
</body>
</html>
