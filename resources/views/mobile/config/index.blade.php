@extends('mobile.home.home')
@section('content')
<div id="appCapsule">
    <div class="content project-content">
        <a class="next-sidebar sidebar-left"><i style="padding: 0px 10px 0px 2px;font-size: 22px;margin-top: 4px;" class="icon-arrow-right6"></i> <h4 >Quản lý cấu hình website</h4></a>
        <div class="form-create">
            <form method="POST" action="{{route('frontend.config.update')}}">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <div class="form-group row">
                    <label class="col-md-2 required control-label text-left text-semibold">Logo website:</label>
                    <div class="col-md-6 div-image">
                        <div class="file-input file-input-ajax-new">
                            <div class="input-group file-caption-main">
                                <div class="input-group-btn input-group-append">
                                    <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">Chọn ảnh</span>
                                        <input type="file" class="upload-image" multiple="multiple" name="image_upload[]" data-fouc="">
                                    </div>
                                </div>
                            </div>
                            <div class="file-preview ">
                                <div class=" file-drop-zone">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="image" class="image_data" value="{{$record->image}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 required control-label text-left text-semibold">Favicon: </label>
                    <div class="col-md-6 div-image">
                        <div class="file-input file-input-ajax-new">
                            <div class="input-group file-caption-main">
                                <div class="input-group-btn input-group-append">
                                    <div tabindex="500" class="btn btn-primary btn-file"><span class="hidden-xs">Chọn ảnh</span>
                                        <input type="file" class="upload-image" multiple="multiple" name="favicon_upload[]" data-fouc="">
                                    </div>
                                </div>
                            </div>
                            <div class="file-preview ">
                                <div class=" file-drop-zone">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="favicon" class="image_data" value="{{$record->favicon}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label text-left">Tên trang website</label>
                    <div class="col-sm-6">
                        <input class="form-control" name="title" type="text" value="{{$record->title}}"> 
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 required control-label text-left text-semibold">Hỏi đáp </label>
                    <div class="col-md-6">
                        <textarea class="form-control" id="content_question" name="content">{!!$record->content!!}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <button class="btn btn-submit" type="submit" style="background:#fd7700;"><img src="http://de-an.local/assets2/img/send.png">  Lưu lại</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script>
ClassicEditor
        .create(document.querySelector('#content_question'))
        .catch(error => {
            console.error(error);
        });
</script>
@if (Session::has('success'))
<script>
    var notifier = new Notifier();
    var notification = notifier.notify("success", "Cập nhật thành công");
    notification.push();
</script>
@endif
@stop