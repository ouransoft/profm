@extends('mobile.layouts.admin')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('frontend.todo.index')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">Chi tiết công việc</div>
</div>
<!-- * App Header -->

<!-- Search Component -->
<div id="search" class="appHeader">
    <form class="search-form">
        <div class="form-group searchbox">
            <input type="text" class="form-control" placeholder="Search...">
            <i class="input-icon">
                <ion-icon name="search-outline"></ion-icon>
            </i>
            <a href="javascript:;" class="ml-1 close toggle-searchbox">
                <ion-icon name="close-circle"></ion-icon>
            </a>
        </div>
    </form>
</div>
<!-- * Search Component -->

<!-- App Capsule -->
<div id="appCapsule" class="header-active">
    <div class="section full my-2">
        <div class="py-2 d-flex justify-content-around">
            @if(isset($membertodo) && $membertodo->join == 0)
            <a href="{{route('frontend.todo.join',$record->id)}}" type="button" class="btn btn-primary mx-1 px-1 d-flex align-items-center">
                <ion-icon name="person-add-outline"></ion-icon>
                <span>Tham gia</span>
            </a>
            <a href="javascipt:void(0)" data-toggle="modal" data-target="#modal_unjoin_todo" type="button" class="unjoin-todo btn btn-danger mx-1 px-1 d-flex align-items-center">
                <ion-icon name="person-remove-outline"></ion-icon>
                <span>Không tham gia</span>
            </a>
            @endif
            @if($check == true)
            <a href="javascript:void(0)" class="btn btn-success change-status" data-id="{{$record->id}}" style="color:#333;"><ion-icon name="checkbox-outline"></ion-icon>  Cập nhật</a>
            @endif
            @if($record->created_by == \Auth::guard('member')->user()->id)
            <a href="{{route('frontend.todo.edit',$record->id)}}" type="button" class="btn btn-primary w-25 mx-1 px-1 d-flex align-items-center">
                <ion-icon name="create-outline" class="mx-0"></ion-icon>
                <span>{{ trans('base.Edit') }}</span>
            </a>
            @if($record->status != \App\Todo::STATUS_COMPLETE )
            <form action="{{route('frontend.todo.destroy',['id' => $record->id])}}" method="POST" class="btn btn-danger w-25 mx-1 px-1 d-flex align-items-center">
                    {!! method_field('DELETE') !!}
                    {!! csrf_field() !!}
                    <a type="button" class="btn-danger" data-action="delete" style="display:contents">
                         <ion-icon name="close" class="mx-0"></ion-icon> {{ trans('base.Delete') }}
                    </a>
            </form>
            @endif
            @endif
        </div>
        <div class="wide-block">
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="reader-outline" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>Tiêu đề</span>
                    </label>
                    <span class="pl-3 fs-16">{{$record->title}}</span>
                </div>
            </div>
            @if(!is_null($record->purpose))
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="help-outline" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>Mục đích</h4>
                    </label>
                    <span class="pl-3 fs-16">{{$record->purpose}}</span>
                </div>
            </div>
            @endif
            @if(!is_null($record->address))
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="location-outline" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>Địa điểm</span>
                    </label>
                    <span class="pl-3 fs-16">{{$record->address}}</span>
                </div>
            </div>
            @endif
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="time-outline" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>Bắt đầu</span>
                    </label>
                    <span class="pl-3 fs-16">{{$record->start_date()}}</span>
                </div>
            </div>
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="time-outline" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>Kết thúc</span>
                    </label>
                    <span class="pl-3 fs-16">{{$record->end_date()}}</span>
                </div>
            </div>
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="person-outline" class="fs-18 pr-1 text-warning"></ion-icon>
                        <span>Giao cho ({{count($record->member)}} {{ trans('base.Member') }})</span>
                    </label>
                    <div class="pl-3 pr-2 fs-16">
                        @if(count($record->member) > 0)
                        <span class="mb-0 font-weight-normal">{{implode(',',$record->member()->pluck('full_name')->toArray())}}</span>
                        <div class="d-flex">
                            @foreach($record->member as $key=>$val)
                            <div class="chip chip-media mb-05 mr-1">
                                <img src="{{$val->avatar}}" alt="avatar">
                            </div>
                            @endforeach
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="bar-chart-outline" class="fs-18 pr-1 text-warning d-flex align-items-center"></ion-icon>
                        <span>Trạng thái</span>
                    </label>
                    <div class='pl-3 pr-2 fs-16'>
                        <span class="badge badge-color{{$record->status}}">{{$record->nameStatus()}}</span>
                    </div>
                </div>
            </div>
            <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="bar-chart-outline" class="fs-18 pr-1 text-warning d-flex align-items-center"></ion-icon>
                        <span>Mức độ</span>
                    </label>
                    <div class='pl-3 pr-2 fs-16'>
                        <span class="badge badge-priority{{$record->priority}}">{{$record->namePriority()}}</span>
                    </div>
                </div>
            </div>
            <div class="form-group boxed">
                <div class="input-wrapper not-empty">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="document-text-outline" class="fs-18 pr-1 text-warning d-flex align-items-center"></ion-icon>
                        <span>Nội dung</span>
                    </label>
                    <span class="pl-3 fs-16">{{$record->content}}</span>
                </div>
            </div>
            <div class="form-group boxed">
                <div class="input-wrapper not-empty">
                    <label class="label fs-16 d-flex align-items-center">
                        <ion-icon name="document-text-outline" class="fs-18 pr-1 text-warning d-flex align-items-center"></ion-icon>
                        <span>Ghi chú</span>
                    </label>
                    <span class="pl-3 fs-16">{{$record->note}}</span>
                </div>
            </div>
            <div class="form-group boxed py-3">
                <div class="input-wrapper">
                    <label class="label fs-16 d-flex align-items-center" for="attachments">
                        <span class="d-flex align-items-center">
                            <ion-icon class="text-warning md hydrated" name="attach-outline" style="font-size: 22px;" role="img" aria-label="attach outline"></ion-icon>
                            <span>File đính kèm</span>
                        </span>
                    </label>
                    @if(count($record->files) > 0) 
                        @foreach($record->files as $key=>$value)
                            <a href="/{{$value->link}}"><span class="pl-3 fs-16">{{$value->name}} ({{$value->size}})</span></a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title mb-1 pl-0">
                <h3 class="mb-0">Bình luận</h3>
            </div>
            <div class="pt-2 pb-0">
                <!-- comment block -->
                <div class="comment-block ui comments">
                    @foreach($comments as $key=>$comment)
                        <!--item -->
                        <div class="item" id="comment{{$comment->id}}">
                            <div class="avatar">
                                <img src="{{$comment->member ? $comment->member->avatar : asset('img/user30.png')}}" alt="avatar" class="imaged w32 rounded">
                            </div>
                            <div class="in">
                                <div class="comment-header">
                                    <h4 class="title">{{$comment->member ? $comment->member->full_name : ''}}</h4>
                                    <span class="time">{{\App\Helpers\StringHelper::time_ago($comment->created_at)}}</span>
                                </div>
                                <div class="text d-flex align-items-center justify-content-between">
                                    <span>{{$comment->comment}}</span>
                                    @if ($comment->member->id == $membertodo->member_id)
                                        <ion-icon name="ellipsis-horizontal" data-toggle="modal" data-target="#deleteCommentSheet{{$comment->id}}" style="font-size: 18px"></ion-icon>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- * item -->
                        <div class="modal fade action-sheet inset" id="deleteCommentSheet{{$comment->id}}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <ul class="action-button-list">
                                            <li>
                                                <a data-comment_id="{{$comment->id}}" href="javascript:void(0)" class="btn btn-list delete-comment" data-dismiss="modal">
                                                    <span class="text-danger">Xóa tin nhắn</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- * comment block -->
            </div>
        </div>

        <div class="divider mt-2 mb-0"></div>

        <div class="section mt-0">
            <div class="pt-1 pb-2">
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <textarea name="content" id="comment" class="form-control h-auto" placeholder="Viết bình luận..."></textarea>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="mt-1">
                    <button id="send-mess-btn" class="btn btn-primary btn-lg btn-block">
                        Gửi
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_unjoin_todo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom:0px">
                <h5 class="modal-title" id="exampleModalLabel">LÝ DO<span class="orange"> KHÔNG THAM GIA</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" action="{{route('frontend.todo.unjoin')}}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='id' value='{{$record->id}}'>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='reason' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='btn btn-success'><ion-icon name="send-outline" class="md hydrated"></ion-icon> GỬI </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade dialogbox" id="todo-checkbox" data-backdrop="static" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cập nhật tiến độ công việc</h5>
            </div>
            <form method="post" action="{{route('frontend.todo.changeStatus')}}" id="frmChangStatus">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <input type='hidden' name='id' id="todo_id">
                <div class="modal-body mb-1">
                        <div class="form-group">
                            <select class="custom-select" id="select_status" name="progress" data-placeholder="Chọn tiến độ">

                            </select>
                        </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn btn-text-secondary" data-dismiss="modal">Thoát</a>
                        <button type="sunmit" class="btn btn-text-primary">Cập nhật</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $(document).ready(function(){
     $('.popover-dismiss').popover({
      trigger: 'focus'
    })
   });
    $('textarea[name="content"]').keypress(
        function(e){
            var comment = $(this).val();
            var todo_id = {{$record->id}}
            if (e.keyCode == 13) {
                if ($(this).index() == 0) {
                    console.log(comment);
                    $.ajax({
                        url:'/api/send-comment',
                        method:'POST',
                        data:{comment:comment,todo_id:todo_id},
                        success: function(response){
                            $('textarea').val('');
                            $('.ui.comments').prepend(response.html);
                        }
                    })
                }
            }
    });
    $('#send-mess-btn').click(function(){
        var comment = $('#comment').val();
        var todo_id = {{$record->id}}
        if ($('#comment').index() == 0) {
            $.ajax({
                url:'/api/send-comment',
                method:'POST',
                data:{comment:comment,todo_id:todo_id},
                success: function(response){
                    $('textarea').val('');
                    $('.ui.comments').prepend(response.html);
                }
            })
        }
    })
    $('body').delegate('.delete-comment','click',function(){
        var comment_id = $(this).data('comment_id');
        console.log(comment_id);
        $.ajax({
            url:'/api/delete-comment',
            method:'POST',
            data:{comment_id:comment_id},
            success: function(response){
                $('#'+response.id).remove();
            }
        })
    })
    $('body').delegate('.change-status', 'click', function (e) {
        var id = $(this).data('id');
        $.ajax({
            url: '/api/getSelectStatus',
            method: 'POST',
            data: {id: id},
            success: function (response) {
                $('#select_status').html(response.html);
                $('#todo_id').val(id);
                $('#todo-checkbox').modal('show');
            }
        })
    })
     $('[data-action="delete"]').click(function (e) {
        var elm = this;
        bootbox.confirm("Bạn có chắc chắn muốn xóa?", function (result) {
            if (result === true) {
                $(elm).parents('form').submit();
            }
        });
    });
</script>
@stop
