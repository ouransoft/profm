@extends('mobile.layouts.admin')
@section('content')
<link rel="stylesheet" href="https://www.jqueryscript.net/demo/Searchable-Multi-select-jQuery-Dropdown/jquery.dropdown.css">
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('frontend.todo.index')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">Chỉnh sửa công việc</div>
</div>
<!-- * App Header -->

<!-- Search Component -->
<div id="search" class="appHeader">
    <form class="search-form">
        <div class="form-group searchbox">
            <input type="text" class="form-control" placeholder="Search...">
            <i class="input-icon">
                <ion-icon name="search-outline"></ion-icon>
            </i>
            <a href="javascript:;" class="ml-1 close toggle-searchbox">
                <ion-icon name="close-circle"></ion-icon>
            </a>
        </div>
    </form>
</div>
<!-- * Search Component -->
<!-- App Capsule -->
<div id="appCapsule" class="header-active">
    <div class="section full my-2">
        <div class="wide-block">
            <form action="{{route('frontend.todo.update',$record->id)}}" method="POST">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>Tiêu đề <span class="text-danger">*</span></h4></label>
                        <input type="text" class="form-control" name="title" id="title" required="" value='{{$record->title}}'>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>Mục đích</h4></label>
                        <input type="text" class="form-control" name="purpose" id="reason" value='{{$record->purpose}}'>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>Địa điểm</h4></label>
                        <input type="text" class="form-control" name="address" id="place" value='{{$record->address}}'>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>Bắt đầu</h4></label>
                        <div style="display: flex">
                            <input id="" name="start_date" class="fieldset__input form-control datepicker" value="{{date('d/m/Y',strtotime($record->start_date))}}" type="text" placeholder="Start date">
                            <div class="input-group clockpicker">
                                <input type="text" class="form-control" autocomplete="off" readonly="" name="start_time" value="{{date('H:i',strtotime($record->start_date))}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>Kết thúc</h4></label>
                        <div style="display: flex">
                            <input id="" name="end_date" class="fieldset__input form-control datepicker" value="{{date('d/m/Y',strtotime($record->end_date))}}" type="text" placeholder="End date">
                            <div class="input-group clockpicker">
                                <input type="text" class="form-control" autocomplete="off" readonly="" name="end_time" value="{{date('H:i',strtotime($record->end_date))}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>Giao cho</h4></label>
                        <div class="demo">
                            <select style="display:none"  name="member_id[]" multiple>
                                {!!$member_html_mobile!!}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="subject"><h4>Mức độ</h4></label>
                        <select class="custom-select" id="level" name="priority">
                            {!!$priority_html_mobile!!}
                        </select>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper not-empty">
                        <label class="label"><h4>Nội dung</h4></label>
                        <textarea rows="2" class="form-control" name="content">{{$record->content}}</textarea>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper not-empty">
                        <label class="label"><h4>Ghi chú</h4></label>
                        <textarea rows="2" class="form-control" name="note">{{$record->note}}</textarea>
                    </div>
                </div>
                <div class="form-group boxed py-3">
                    <div class="input-wrapper">
                        <label class="label" for="attachments">
                            <h4 class="d-flex align-items-center">
                                <span>File đính kèm</span>
                                <ion-icon class="text-warning md hydrated" name="attach-outline" style="font-size: 22px;" role="img" aria-label="attach outline"></ion-icon>
                            </h4>
                        </label>
                        <input id="attachments" type="file" multiple="" name="name_file">
                        <table id="upload_table" class="attachment_list_base_grn">
                            <tbody id='list_file_upload'>
                                @if(count($record->files) > 0) 
                                    @foreach($record->files as $key=>$value)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="{{$value->id}}">
                                            {{$value->name}} ({{$value->size}})
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="d-flex justify-content-center py-3">
                    <button type="submit" class="btn btn-primary mr-2 mb-1 w-25">{{ trans('base.Update') }}</button>
                    <button type="button" class="btn btn-warning mr-1 mb-1 w-25">{{ trans('base.Cancel') }}</button>
                </div>
            </form>
        </div>

    </div>
</div>
@stop
@section('script')
@parent
<script src="https://www.jqueryscript.net/demo/Searchable-Multi-select-jQuery-Dropdown/jquery.dropdown.js"></script>
<script>
    $('.demo').dropdown({
        init: 'noop',
        choice:function () {},
        extendProps: []
    });
    $('.datepicker').pickadate({
         formatSubmit: 'yyyy-mm-dd',
         format: 'dd/mm/yyyy',
         onOpen: undefined,
    })
    $('.clockpicker').clockpicker({
        placement: 'bottom', // clock popover placement
        align: 'right',       // popover arrow align
        autoclose: true,    // auto close when minute is selected
        vibrate: true        // vibrate the device when dragging clock hand
    });
    $(document).on('change', '#attachments', function () {
        var file_data = $(this).prop('files');
        var form_data = new FormData();
        for(let i=0;i<file_data.length;i++){
        form_data.append('file[]', file_data[i]);
        }
        form_data.append('type',{{\App\File::TYPE_TODO}});
        $.ajax({
            url: '/api/upload',
            type: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(response){
                if(response.success == 'true'){
                    if(response.success == 'true'){
                        response.data.forEach(element =>
                        $('#list_file_upload').append(`<tr>
                                                            <td>
                                                                <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                `+element.name+` (`+element.size+` Mb)
                                                            </td>
                                                        </tr>`)
                        )
                    }
                }
            }
        });
     })
</script>
@stop
