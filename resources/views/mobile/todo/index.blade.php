@extends('mobile.layouts.admin')
@section('content')
<link rel="stylesheet" href="{!!asset('assets2/dashboard/css/master_style.css')!!}">
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('home.view')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">{{ trans('base.To_do_list') }}</div>
</div>
<!-- * App Header -->

<!-- Search Component -->
<div id="search" class="appHeader">
    <form class="search-form">
        <div class="form-group searchbox">
            <input type="text" class="form-control" placeholder="Search...">
            <i class="input-icon">
                <ion-icon name="search-outline"></ion-icon>
            </i>
            <a href="javascript:;" class="ml-1 close toggle-searchbox">
                <ion-icon name="close-circle"></ion-icon>
            </a>
        </div>
    </form>
</div>
<!-- * Search Component -->

<div class="extraHeader p-0">
    <ul class="nav nav-tabs style1 mx-1" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#uncompleted" role="tab">
                {{ trans('base.Uncomplete') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#completed" role="tab">
                {{ trans('base.Completed') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#statistical" role="tab">
                Thống kê
            </a>
        </li>
    </ul>
</div>
 <a href="{{route('frontend.todo.create')}}" class="button btn-warning insert-fixed-btn show px-1">
    <ion-icon name="add" role="img" class="md hydrated" aria-label="arrow up outline"></ion-icon>
</a>
<!-- App Capsule -->
<div id="appCapsule" class="extra-header-active">
    <div class="tab-content pb-2">
        <div class="tab-pane fade active show" id="uncompleted" role="tabpanel">
            <div class="section mb-3">
                @foreach($records as $key=>$record)
                <div class="card mt-2 mb-0">
                    <div class="pt-0 pb-0 my-2 timeline time-padding load-view mborder-color-{{$record->priority}}" data-href='{{route('frontend.todo.view',$record->id)}}'>
                        <div class="item mb-1">
                            <div class="content d-flex pr-1">
                                <a href="{{route('frontend.todo.view',$record->id)}}" class="item font-weight-normal text-warning" style="font-size: 18px;">{{$record->title}}</a>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="pr-1">
                                <div class="content">
                                    <span class="title d-flex align-items-center">
                                        <ion-icon name="time" class="icon-schedule md hydrated text-info" role="img" aria-label="time"></ion-icon>
                                        <span class="mb-0 pl-1 font-weight-normal fs-14">
                                            <p class="mb-0 pr-05">{{date('d/m/Y',strtotime($record->start_date))}}</p>
                                        </span>
                                    </span>
                                </div>
                                <div class="content">
                                    <span class="title d-flex align-items-center">
                                        <ion-icon name="person" class="icon-schedule md hydrated text-success" role="img" aria-label="person"></ion-icon>
                                        <span class="mb-0 pl-1 font-weight-normal fs-14">{{implode(',',$record->member()->take(2)->pluck('full_name')->toArray())}} @if(count($record->member) > 2)+{{count($record->member) - 2}} @endif</span>
                                    </span>
                                    <div class="d-flex pl-3">
                                        @foreach($record->member as $key=> $result)
                                        @if($key < 5)
                                        <div class="chip chip-media mb-05 mr-1">
                                            <img src="{{$result->avatar}}" alt="avatar">
                                        </div>
                                        @endif
                                        @endforeach
                                        @if(count($record->member) > 5)+{{count($record->member) - 5}} @endif
                                    </div>
                                </div>
                            </div>
                            <div id="dart{{$record->id}}" class="circle-progress dart-size px-1 py-1 align-self-start mr-1"><canvas width="120" height="120" style="height: 40px; width: 40px;"></canvas>
                                <div class="in">
                                    <div class="text">
                                        <h4 class="value">@if($record->status == \App\Todo::STATUS_COMPLETE) 100% @else {{number_format($record->progress())}}% @endif</h4>
                                    </div>
                                </div>
                            </div>
                            <script>
                                $('#dart{!!$record->id!!}').circleProgress({
                                    value: {{$record->status == \App\Todo::STATUS_COMPLETE ? 1 : number_format($record->progress(),1)/100}},
                                    size: 40, // do not delete this
                                    fill: {
                                        gradient: ["#1E74FD", "#592BCA"]
                                    },
                                    animation: {
                                        duration: 2000
                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="completed" role="tabpanel">
            <div class="section full">
                <div class="wide-block py-2 d-flex justify-content-around">
                    <a href="#" type="button" class="btn btn-text-warning shadowed delete-all"><ion-icon name="trash-outline" class="mx-1"></ion-icon>{{ trans('base.Delete_all') }}</a>
                </div>
            </div>
            <div class="section mt-3 mb-3">
                @foreach($historys as $key=>$history)
                <div class="card mt-1 mb-0">
                    <div class="pt-0 pb-0 my-2 timeline time-padding load-view mborder-color-{{$history->priority}}" data-href='{{route('frontend.todo.view',$history->id)}}'>
                        <div class="item mb-1">
                            <div class="content d-flex pr-1">
                                <a href="{{route('frontend.todo.view',$history->id)}}" class="item font-weight-normal text-warning" style="font-size: 18px;">{{$history->title}}</a>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="pr-1">
                                <div class="content">
                                    <span class="title d-flex align-items-center">
                                        <ion-icon name="time" class="icon-schedule md hydrated text-info" role="img" aria-label="time"></ion-icon>
                                        <span class="mb-0 pl-1 font-weight-normal fs-14">
                                            <p class="mb-0 pr-05">{{date('d/m/Y',strtotime($history->start_date))}}</p>
                                        </span>
                                    </span>
                                </div>
                                <div class="content">
                                    <span class="title d-flex align-items-center">
                                        <ion-icon name="person" class="icon-schedule md hydrated text-success" role="img" aria-label="person"></ion-icon>
                                        <span class="mb-0 pl-1 font-weight-normal fs-14">{{implode(',',$history->member()->take(2)->pluck('full_name')->toArray())}} @if(count($history->member) > 2)+{{count($history->member) - 2}} @endif</span>
                                    </span>
                                    <div class="d-flex pl-3">
                                        @foreach($history->member as $key=> $result)
                                        @if($key < 5)
                                        <div class="chip chip-media mb-05 mr-1">
                                            <img src="{{$result->avatar}}" alt="avatar">
                                        </div>
                                        @endif
                                        @endforeach
                                        @if(count($history->member) > 5)+{{count($history->member) - 5}} @endif
                                    </div>
                                </div>
                            </div>
                            <div id="dart{{$history->id}}" class="circle-progress dart-size px-1 py-1 align-self-start mr-1"><canvas width="120" height="120" style="height: 40px; width: 40px;"></canvas>
                                <div class="in">
                                    <div class="text">
                                        <h4 class="value">@if($history->status == \App\Todo::STATUS_COMPLETE) 100% @else {{number_format($history->progress())}}% @endif</h4>
                                    </div>
                                </div>
                            </div>
                            <script>
                                $('#dart{!!$history->id!!}').circleProgress({
                                    value: {{$history->status == \App\Todo::STATUS_COMPLETE ? 1 : number_format($history->progress(),1)/100}},
                                    size: 40, // do not delete this
                                    fill: {
                                        gradient: ["#1E74FD", "#592BCA"]
                                    },
                                    animation: {
                                        duration: 2000
                                    }
                                });
                            </script>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade" id="statistical" role="tabpanel">
            <div class="section mt-3 mb-3">
                <div class="box">
                    <div class="listview-title mt-1 px-0 mx-4 border-bottom border-secondary"><h4 class="mb-0">Tìm kiếm</h4></div>
                    <div class="col-xl-12 col-md-12 col-12 mt-2">
                        <form id='form_search' method="GET" action='' style="width:100%">
                            <div class="d-flex justify-content-around">
                                <div class="d-flex align-items-center person-search">
                                    <input type="radio" id="radio_member" name="search" value='personal' checked="">
                                    <h4 class="mb-0 mx-1">Cá nhân</h4>
                                    <ion-icon name="person-outline" class="text-warning"></ion-icon>
                                </div>
                                <div class="d-flex align-items-center person-search">
                                    <input type="radio" name="search">
                                    <h4 class="mb-0 mx-1">Nhóm</h4>
                                    <ion-icon name="people-outline" class="text-warning"></ion-icon>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mt-1 mb-3 form-group">
                                <select class='form-control custom-select w-75 select' name='member_id' data-placeholder='--Chọn--'>
                                    {!!$member_html!!}
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-md-3 col-3">
                        <div class="small-box pull-up bg-info">
                            <div class="inner py-0 px-0">
                                <h3 class="text-white mb-0" id="count_week">{{$count_week}}</h3>
                                <p class="mb-0">Theo tuần</p>
                            </div>
                            <div class="icon">
                                <i class="icon-calendar text-white" style="font-size: 50px"></i>
                            </div>
                            <a href="#" class="small-box-footer"> </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 col-3">
                        <div class="small-box pull-up bg-danger">
                            <div class="inner py-0 px-0">
                            <h3 class="text-white mb-0" id="count_month">{{$count_month}}</h3>
                            <p class="mb-0">Theo tháng</p>
                            </div>
                            <div class="icon">
                                <i class="icon-calendar text-white" style="font-size: 50px"></i>
                            </div>
                            <a href="#" class="small-box-footer"></a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 col-3">
                        <div class="small-box pull-up bg-warning">
                            <div class="inner py-0 px-0">
                            <h3 class="text-white mb-0" id="count_quarter">{{$count_quarter}}</h3>
                            <p class="mb-0">Theo Quý</p>
                            </div>
                            <div class="icon">
                                <i class="icon-calendar text-white" style="font-size: 50px"></i>
                            </div>
                            <a href="#" class="small-box-footer"> </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 col-3">
                        <div class="small-box pull-up bg-success">
                            <div class="inner py-0 px-0">
                            <h3 class="text-white mb-0" id="count_year">{{$count_year}}</h3>
                            <p class="mb-0">Theo năm</p>
                            </div>
                            <div class="icon">
                                <i class="icon-calendar text-white" style="font-size: 50px"></i>
                            </div>
                            <a href="#" class="small-box-footer"> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12 pd-0">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title mb-0">Biểu đồ thống kê toàn bộ</h4>
                        </div>
                        <div class="box-body p-0 chart">
                            <div id="donut"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12 mt-3 pd-0">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title mb-0">Danh sách công việc chưa hoàn thành</h4>
                        </div>
                        <ul id="list_todo" class="listview link-listview flush transparent mb-1">
                            @foreach($list_todo as $key=>$todo)
                            <li>
                                <a href="{{route('frontend.todo.view',$todo->id)}}" class="item">
                                    <div class="in">
                                        <div>
                                            {{$todo->title}}
                                            <footer class="pt-1">
                                                <ion-icon name="calendar-clear-outline"></ion-icon> {{$todo->end_date}}
                                            </footer>
                                        </div>
                                    </div>
                                    <span class="badge badge-priority{{$todo->priority}}">{{$todo->namePriority()}}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * Uncompleted tab -->

    <!-- Modal -->
    <div class="modal fade dialogbox" id="todo-checkbox" data-backdrop="static" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cập nhật tiến độ công việc</h5>
                </div>
                <form method="post" action="{{route('frontend.todo.changeStatus')}}" id="frmChangStatus">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <input type='hidden' name='id' id="todo_id">
                    <div class="modal-body mb-1">
                        <div class="form-group">
                            <select class="custom-select" id="select_status" name="progress" data-placeholder="Chọn tiến độ">
                                
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-inline">
                            <a href="#" class="btn btn-text-secondary" data-dismiss="modal">CLOSE</a>
                            <button type="sunmit" class="btn btn-text-primary">SEND</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- *Modal -->
</div>
<script src="{{asset('assets2/dashboard/assets/vendor_components/apexcharts-bundle/irregular-data-series.js')}}"></script>
<script src="{{asset('assets2/dashboard/assets/vendor_components/apexcharts-bundle/dist/apexcharts.js')}}"></script>
@stop
@section('script')
@parent
<script>
    var optionDonut = {
        colors : ['#e4ae0f','#007bff','#037f03','#ff4a52'],
        chart: {
                type: 'donut',
                width: '100%',
                events: {
                    dataPointSelection: function(event, chartContext, config) {
                        var time = $('.select-filler-chart').val();
                        var search = $('input[type="radio"]:checked').val();
                        if(search == 'department'){
                            var id = $('select[name="department_id"]').val();
                        }else{
                            var id = $('select[name="member_id"]').val();
                        }
                        var pattern = config.dataPointIndex;
                        $.ajax({
                            url:'/api/get-list-todo',
                            method:'POST',
                            data:{time:time,search:search,id:id,pattern:pattern},
                            success:function(response){

                            }
                        })
                    }
                }
        },
        dataLabels: {
              enabled: true,
        },
        plotOptions: {
              pie: {
                donut: {
                      size: '55%',
                },
                offsetY: 20,
              },
              stroke: {
                colors: undefined
              }
        },
        series: [{{implode(',',$status_todo)}}],
        labels: ['Chưa hoàn thành','Hoàn thành trước hạn', 'Hoàn thành đúng hạn','Hoàn thành sau hạn'],
        legend: {
              position: 'bottom',
              offsetY: 0
        }
      }

    var donut = new ApexCharts(
        document.querySelector("#donut"),
        optionDonut
    )
    $('.select-fillter').change(function(e){
        e.preventDefault();
        callAjax();
    })
    donut.render();


    function callAjax(){
        $.ajax({
            url:'/api/get-statistical-todo',
            method:'POST',
            data: $('#form_search').serialize(),
            success:function(response){
                $('#count_week').html(response.count_week);
                $('#count_month').html(response.count_month);
                $('#count_quarter').html(response.count_quater);
                $('#count_year').html(response.count_year);
                if(response.list_todo_mobile_html != ''){
                    $('#list_todo').html(response.list_todo_mobile_html);
                }
                else{
                    $('#list_todo').html('<li><a href="javascript:void(0)">Không có dữ liệu</a></li>');
                }
                donut.updateSeries(response.status_todo);
            }
        })
    }

    $(document).ready(function(){
        $('.popover-dismiss').popover({
            trigger: 'focus'
        })
    });
    $('body').delegate('.change-status', 'click', function (e) {
        var id = $(this).data('id');
        $.ajax({
            url: '/api/getSelectStatus',
            method: 'POST',
            data: {id: id},
            success: function (response) {
                $('#select_status').html(response.html);
                $('#todo_id').val(id);
                $('#todo-checkbox').modal('show');
            }
        })
    })
    $('body').delegate('.delete-todo','click',function(){
        var id = $(this).data('id');
        $this = $(this);
        $.ajax({
            url: '/api/delete-todo',
            method: 'POST',
            data: {id: id},
            success: function (response) {
                $this.parents('.card').remove();
            }
        })
    })
    $('body').delegate('.delete-all','click',function(){
        $.ajax({
            url: '/api/delete-todo-all',
            method: 'POST',
            success: function (response) {
                $('.card-history').remove();
            }
        })
    })
    $('.load-view').click(function(){
        window.location.href = $(this).data('href');
    })
    $('.select-filler-chart').click(function(){
        var time = $(this).val();
        var search = $('input[type="radio"]:checked').val();
        if(search == 'department'){
            var id = $('select[name="department_id"]').val();
        }else{
            var id = $('select[name="member_id"]').val();
        }
        $.ajax({
            url:'/api/get-chart-todo',
            method:'POST',
            data:{time:time,search:search,id:id},
            success:function(response){
                donut.updateSeries(response.status_todo)
            }
        })
    })
    $('.select-fillter').html('{!!$member_html!!}');
    $('input[type="radio"]').click(function(){
        if ($('#radio_member').is(':checked'))
        {
            $('.select').html('{!!$member_html!!}');
            callAjax();
            // $('.select').val('');
        }else{
            $('.select').html('{!!$department_html!!}');
        }
    });
    $('body').delegate('.select','change',function(){
           callAjax(); 
    })
</script>
@stop
