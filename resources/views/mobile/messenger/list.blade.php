@extends('mobile.layouts.admin')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('home.view')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">{{ trans('base.Messenger') }}</div>
</div>
<div id="appCapsule">
    <ul class="listview image-listview">
        @foreach($all_messages as $key=>$value)
        <li class="{{$value['seen'] == 0 ? 'active' : ''}}">
            <a href="{{route('frontend.messenger.detail',$value['id'])}}" class="item">
                <img src="{{$value['avatar']}}" alt="image" class="image">
                <div class="in">
                    <div>
                        {{$value['name']}}
                        <footer class="message-hidden">{!!$value['message']!!}</footer>
                    </div>
                    <span class="text-muted">{{$value['time']}}</span>
                </div>
            </a>
        </li>
        @endforeach
    </ul>
</div>
@stop
@section('script')
@parent
<script>
    var channel = pusher.subscribe('chat-message');
    channel.bind('send-message', function (data) {
        if($('#private_id').val() === data.to){
            $.ajax({
                headers: {
                    'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                },
                url: '/api/get-messenger',
                method: 'POST',
                success: function (response) {
                    if (response.error === false) {
                        $('.listview').html(response.html);
                    }
                }
            });
        }
    });
</script>
@stop
