<html lang="en">
    @include('mobile/layouts/__head')
    <body>
        <div class="appHeader bg-warning text-light">
            <div class="left">
                <a href="{{route('frontend.messenger.index')}}" class="headerButton">
                    <ion-icon name="chevron-back-outline"></ion-icon>
                </a>
            </div>
            <div class="pageTitle">@if($group->name == '') {{$group->member()->first()->full_name}} @else {{$group->name}} @endif</div>
        </div>
        <div id="appCapsule">
            @foreach($messages as $key=>$value)
                @if($value->from == \Auth::guard('member')->user()->id)
                    @if($value->type == 1)
                        <div class="message-item user">
                            <div class="content">
                                <div class="bubble">
                                    {!!$value->message!!}
                                </div>
                                <div class="footer">{{date('h:i a',strtotime($value->created_at))}}</div>
                            </div>
                        </div>
                    @elseif($value->type == 2)
                        <div class="message-item user">
                            <div class="content">
                                <div class="bubble">
                                    <img src='{!!asset($value->message)!!}' class='imaged w160'>
                                </div>
                                <div class="footer">{{date('h:i a',strtotime($value->created_at))}}</div>
                            </div>
                        </div>
                    @else
                        <div class="message-item user">
                            <div class="content">
                                <div class="bubble">
                                    <a href='{{asset($value->message)}}'>{!!$value->file_name!!}</a>
                                </div>
                                <div class="footer">{{date('h:i a',strtotime($value->created_at))}}</div>
                            </div>
                        </div>
                    @endif
                @else
                     @if($value->type == 1)
                        <div class="message-item">
                            <img src="{{$value->membersend ? $value->membersend->avatar : '/assets2/img/gear.png'}}" alt="avatar" class="avatar">
                            <div class="content">
                                <div class="bubble">
                                    {!!$value->message!!}
                                </div>
                                <div class="footer">{{date('h:i a',strtotime($value->created_at))}}</div>
                            </div>
                        </div>
                    @elseif($value->type == 2)
                        <div class="message-item">
                            <img src="{{$value->membersend ? $value->membersend->avatar : '/assets2/img/gear.png'}}" alt="avatar" class="avatar">
                            <div class="content">
                                <div class="bubble">
                                    <img src='{!!asset($value->message)!!}' class='imaged w160'>
                                </div>
                                <div class="footer">{{date('h:i a',strtotime($value->created_at))}}</div>
                            </div>
                        </div>
                    @else
                        <div class="message-item">
                            <img src="{{$value->membersend ? $value->membersend->avatar : '/assets2/img/gear.png'}}" alt="avatar" class="avatar">
                            <div class="content">
                                <div class="bubble">
                                    <a href='{{asset($value->message)}}'>{!!$value->file_name!!}</a>
                                </div>
                                <div class="footer">{{date('h:i a',strtotime($value->created_at))}}</div>
                            </div>
                        </div>
                    @endif
                @endif
            @endforeach
        </div>
        <div class="modal fade action-sheet inset" id="addActionSheet" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Share</h5>
                    </div>
                    <div class="modal-body">
                        <ul class="action-button-list">
                            <li>
                                <label class="btn btn-list">
                                    <input type="file" class="upload-image" style="display:none;btn btn-list" multiple="" accept="image/*" capture="camera">
                                    <span>
                                        <ion-icon name="camera-outline"></ion-icon>
                                        Take a photo
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label class="btn btn-list">
                                    <input type="file" class="upload-image" style="display:none;btn btn-list" multiple="" accept="image/*">
                                    <span>
                                        <ion-icon name="image-outline"></ion-icon>
                                        Upload from Gallery
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label class="btn btn-list">
                                    <input type="file" class="upload-file" style="display:none;btn btn-list" multiple="" accept=".doc,.docx,.xlsx,.csv,.pdf,.xls" capture="">
                                    <span>
                                        <ion-icon name="document-outline"></ion-icon>
                                        Documents
                                    </span>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="chatFooter">
            <form style="margin-bottom:0px;" id="frmMessenger">
                <input type='hidden' name='type' value='1' id="type" />
                <input type='hidden' name='my_id' value='{{\Auth::guard('member')->user()->id}}' />
                <input type='hidden' name='reply_id' value='0' id="reply_id" />
                <input type='hidden' name='type_file' value='1' />
                <input type='hidden' name='receiver_id' value='{{$group->id}}' id="receiver_id" />
                <a href="javascript:void(0);" class="btn btn-icon btn-secondary rounded" data-toggle="modal" data-target="#addActionSheet">
                    <ion-icon name="add"></ion-icon>
                </a>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="text" name="message" autocomplete='off' id='content_message' class="form-control" placeholder="Type a message...">
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                </div>
                <button type="submit" class="btn btn-icon btn-warning rounded">
                    <ion-icon name="send"></ion-icon>
                </button>
            </form>
        </div>
        <script src="{!!asset('assets2/js/pusher.min.js')!!}"></script>
        <script src="{{asset('mobile/assets/js/lib/jquery-3.4.1.min.js')}}"></script>
        <!-- Bootstrap-->
        <script src="{{asset('mobile/assets/js/lib/popper.min.js')}}"></script>
        <script src="{{asset('mobile/assets/js/lib/bootstrap.min.js')}}"></script>
        <!-- Ionicons -->
        <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
        <!-- Owl Carousel -->
        <script src="{{asset('mobile/assets/js/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
        <!-- jQuery Circle Progress -->
        <script src="{{asset('mobile/assets/js/plugins/jquery-circle-progress/circle-progress.min.js')}}"></script>
        <!-- Base Js File -->
        
         <script src="{{asset('mobile/assets/js/custom.js')}}"></script>
    </body>
</html>
<script>
    function scrollToBottomFunc() {
        $('#appCapsule').animate({
            scrollTop: $('#appCapsule')[0].scrollHeight
        }, 50);
    }
    scrollToBottomFunc();
    var my_id = {!!\Auth::guard('member')->user()->id!!};
    var receiver = {!!$group->id!!};
    Pusher.logToConsole = true;
    var pusher = new Pusher('5f84d2a344876b9fea04', {
        cluster: 'ap1',
        forceTLS: true
    });
    var channel = pusher.subscribe('chat-message');
    channel.bind('send-message', function (data) {
        if (my_id == data.from) {
            if(data.type == 3){
                $('#appCapsule').append(`
                    <div class="message-item user">
                        <div class="content">
                            <div class="bubble">
                                <a href="`+data.message+`" download>`+data.file_name+`</a>
                            </div>
                            <div class="footer">{{ date('h:i a') }}</div>
                        </div>
                    </div>
                `);
            }else if(data.type == 2){
                console.log(data);
                $('#appCapsule').append(`
                    <div class="message-item user">
                        <div class="content">
                            <div class="bubble">
                                <img src="{!!asset('`+data.message+`')!!}" class="imaged w160">
                            </div>
                            <div class="footer">{{ date('h:i a') }}</div>
                        </div>
                    </div>
                `);
            }else{
                $('#appCapsule').append(`
                    <div class="message-item user">
                        <div class="content">
                            <div class="bubble">
                                `+data.message+`
                            </div>
                            <div class="footer">{{ date('h:i a') }}</div>
                        </div>
                    </div>
                `);
            }
        }else if(data.member_ids.includes(my_id) && receiver == data.group_id ){
            if(data.type == 3){
                $('#appCapsule').append(`
                    <div class="message-item">
                        <img src="`+data.avatar+`" alt="avatar" class="avatar">
                        <div class="content">
                            <div class="bubble">
                                <a href="`+data.message+`" download>`+data.file_name+`</a>
                            </div>
                            <div class="footer">{{ date('h:i a') }}</div>
                        </div>
                    </div>
                `);
            }else if(data.type == 2){
                $('#appCapsule').append(`
                    <div class="message-item">
                        <img src="`+data.avatar+`" alt="avatar" class="avatar">
                        <div class="content">
                            <div class="bubble">
                                <img src="{!!asset('`+data.message+`')!!}" class="imaged w160">
                            </div>
                            <div class="footer">{{ date('h:i a') }}</div>
                        </div>
                    </div>
                `);
            }else{
                $('#appCapsule').append(`
                    <div class="message-item">
                        <img src="`+data.avatar+`" alt="avatar" class="avatar">
                        <div class="content">
                            <div class="bubble">
                                `+data.message+`
                            </div>
                            <div class="footer">{{ date('h:i a') }}</div>
                        </div>
                    </div>
                `);
            }
        }else if(my_id === data.to && receiver != data.from){
            if($('#count-message').html() === ''){
                 $('#count-message').html(1);
            }else{
                 $('#count-message').html(parseInt($('#count-message').html()) + 1);
        }
        }
        $('#appCapsule').scrollTop($('#appCapsule')[0].scrollHeight);
    });
    $(document).on('submit', '#frmMessenger', function (e) {
        e.preventDefault();
        var message = $('#content_message').val();
        if (message != '') {
            $(this).val('');
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                data: $('#frmMessenger').serialize() ,
                success: function (data) {
                    scrollToBottomFunc();
                    $('#content_message').val('');
                }
            })
        }
    });
    $('body').delegate('.upload-file','change', function(){
        var file = $(this).prop('files');
        var from = {!!\Auth::guard('member')->user()->id!!}
        var receiver_id = {{$group->id}};
        for(let i=0;i<file.length;i++){
            var form_data = new FormData();
            form_data.append('type_file','file');
            form_data.append('file', file[i]);
            form_data.append('my_id', {{\Auth::guard('member')->user()->id}});
            form_data.append('type', 1);
            form_data.append('reply_id', 0);
            form_data.append('receiver_id', receiver_id);
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                headers: {
                        'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                    },
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    $('#addActionSheet').modal('hide');
                }
            })
        }
    });
    $('body').delegate('.upload-image','change', function(){
        var file = $(this).prop('files');
        var from = {!!\Auth::guard('member')->user()->id!!}
        var receiver_id = {{$group->id}};
        for(let i=0;i<file.length;i++){
            var form_data = new FormData();
            form_data.append('type_file','image');
            form_data.append('image', file[i]);
            form_data.append('my_id', {{\Auth::guard('member')->user()->id}});
            form_data.append('type', 1);
            form_data.append('reply_id', 0);
            form_data.append('receiver_id', receiver_id);
            $.ajax({
                url: '/api/send-message',
                method: 'POST',
                headers: {
                        'Authorization': 'Bearer ' + $('meta[name="api_token"]').attr('content'),
                    },
                data: form_data,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (response) {
                    $('#addActionSheet').modal('hide');
                }
            })
        }
    });
</script>

