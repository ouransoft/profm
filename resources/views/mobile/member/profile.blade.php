@extends('mobile.layouts.admin')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="pageTitle">
        {{ trans('base.Profile') }}
    </div>
</div>
<div id="search" class="appHeader">
    <form class="search-form">
        <div class="form-group searchbox">
            <input type="text" class="form-control" placeholder="Search...">
            <i class="input-icon">
                <ion-icon name="search-outline"></ion-icon>
            </i>
            <a href="javascript:;" class="ml-1 close toggle-searchbox">
                <ion-icon name="close-circle"></ion-icon>
            </a>
        </div>
    </form>
</div>
<div id="appCapsule">
    <form action="{{route('frontend.member.update',\Auth::guard('member')->user()->id)}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="section full mt-2 mb-2">
            <div class="section-title">Ảnh đại diện</div>
            <div class="wide-block pb-2 pt-2">
                <div class="custom-file-upload">
                    <input type="file" id="fileuploadInput"  name='file' accept=".png, .jpg, .jpeg">
                    @if(is_null(\Auth::guard('member')->user()->avatar))
                    <label for="fileuploadInput">
                        <span>
                            <strong>
                                <ion-icon name="cloud-upload-outline"></ion-icon>
                                <i>Chọn ảnh</i>
                            </strong>
                        </span>
                    </label>
                    @else
                    <label for="fileuploadInput" class="file-uploaded" style="background-image: url('{{\Auth::guard('member')->user()->file() ? \Auth::guard('member')->user()->file()->link : ''}}');">
                        
                    </label>
                    @endif
                </div>
            </div>
        </div>
        <div class="section full mt-2 mb-2">
            <div class="section-title">Thông tin cá nhân</div>
            <div class="wide-block pb-1 pt-2">
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="name5">{{ trans('base.Full_name') }}</label>
                        <input type="text" class="form-control" name='full_name' value="{{\Auth::guard('member')->user()->full_name}}" id="name5" placeholder="Enter your full name">
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="email5">E-mail</label>
                        <input type="email" class="form-control" name='email' value="{{\Auth::guard('member')->user()->email}}" id="email5" placeholder="Nhập Email">
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                        <div class="invalid-feedback">Nhập email</div>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="phone5">{{ trans('base.Phone') }}</label>
                        <input type="tel" class="form-control" name='phone' id="phone5" value="{{\Auth::guard('member')->user()->phone}}" placeholder="Nhập số điện thoại">
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="address5">{{ trans('base.Address') }}</label>
                        <textarea id="address5" rows="2" name='address' class="form-control">{{\Auth::guard('member')->user()->note}}</textarea>
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="name5">Company ID</label>
                        <input type="text" class="form-control" name='login_id' value="{{\Auth::guard('member')->user()->login_id}}" id="name5" placeholder="Nhập mã ID">
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                </div>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <label class="label" for="password5">{{ trans('base.New_password') }}</label>
                        <input type="password" class="form-control" name='password' id="password5" placeholder="Nhập password">
                        <i class="clear-input">
                            <ion-icon name="close-circle"></ion-icon>
                        </i>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-warning mr-1 mb-1">{{ trans('base.Update') }}</button>
            <a href="{{route('home.view')}}" class="btn btn-secondary mr-1 mb-1">{{ trans('base.Cancel') }}</a>
        </div>
    </form>
</div>
<div id="notification-13" class="notification-box">
    <div class="notification-dialog ios-style bg-success">
        <div class="notification-content">
            <div class="in">
                <h3 class="subtitle">Thông báo</h3>
                <div class="text">
                    Cập nhật thành công
                </div>
            </div>
            <div class="right">
                <span>just now</span>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
@if (Session::has('success'))
<script>
    notification('notification-13', 2500)
</script>
@endif
@stop
