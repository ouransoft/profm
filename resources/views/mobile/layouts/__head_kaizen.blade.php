<head>
    <meta name="api_token" content="{{\Auth::guard('member')->user() ? \Auth::guard('member')->user()->api_token : ''}}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>I-VIBO</title>
    <meta name="description" content="Kaizen change good">
    <meta name="keywords" content="kaizen,5S" />
    <link rel="icon" href="{{\App\Config::first()->favicon}}">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/classic.css')}}">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/classic.date.css')}}">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/timepicker.min.css')}}">
    <script src="{!!asset('assets2/js/chart.min.js')!!}"></script>
    <script src="{{asset('mobile/assets/js/lib/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('mobile/assets/js/plugins/jquery-circle-progress/circle-progress.min.js')}}"></script>
    <link rel="manifest" href="__manifest.json">
    <!--css kaizen -->
    <link rel="stylesheet" href="{!!asset('assets2/css/jquery.transfer.css')!!}">
    <link href="{!!asset('assets2/css/fonts/etline-font.min.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/fonts/fontawesome/all.min.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/fonts/pe-icon-7-stroke.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/fonts/themify-icons.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/components.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/plugins/slick/slick.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/bootstrap.min.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/icomoon/styles.min.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/styles.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/select2.min.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/default-skin.css')!!}" rel="stylesheet">
    <link href="{!!asset('assets2/css/photoswipe.css')!!}" rel="stylesheet">
    <link href="{!!asset('mobile/kaizen/css/kaizen.css')!!}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/custom.css')}}">
</head>