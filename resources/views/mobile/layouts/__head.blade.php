<head>
    <meta name="api_token" content="{{\Auth::guard('member')->user() ? \Auth::guard('member')->user()->api_token : ''}}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>I-VIBO</title>
    <meta name="description" content="Kaizen change good">
    <meta name="keywords" content="kaizen,5S" />
    <link rel="icon" href="{{\App\Config::first()->favicon}}">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/classic.css')}}">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/classic.date.css')}}">
    <link rel="stylesheet" href="{{asset('mobile/assets/css/timepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets2/css/jquery-clockpicker.css')}}">
    <script src="{{asset('mobile/assets/js/lib/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('mobile/assets/js/plugins/jquery-circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('assets2/js/bootstrap-clockpicker.min.js')}}"></script>
</head>