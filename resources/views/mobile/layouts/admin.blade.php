<!DOCTYPE html>
<html lang="en">
    @include('mobile/layouts/__head')
    <body>
        <div id="toast-info" class="toast-box toast-top bg-info">
            <div class="in">
                <div class="text" id="notification">
                </div>
            </div>
            <button type="button" class="btn btn-sm btn-text-light close-button">OK</button>
        </div>
        @include('mobile/layouts/__header')
        @yield('content')
        <!-- Footer -->
        @include('mobile/layouts/__footer')      
    </body>
    @yield('script')   
    
</html>

