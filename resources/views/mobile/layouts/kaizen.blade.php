<!DOCTYPE html>
<html lang="en">
    @include('mobile/layouts/__head_kaizen')
    <body>
        @include('mobile/layouts/__header')
        @yield('content')
        <!-- Footer -->
        @include('mobile/layouts/__footer')      
    </body>
    @yield('script')   
    
</html>

