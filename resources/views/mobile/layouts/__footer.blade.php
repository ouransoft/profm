<style>
    .img-logo-sidebar img{
        margin: 0 auto;
        display: block;
        width: 70%;
        height: auto;
    }
    .img-avatar img{
        width: 120px;
        height: 120px;
        object-fit: cover;
    }
    .full-name{
        text-align: center;
        margin: 0 15px;
        padding: 15px 0;
        border-bottom: 2px solid;
        font-size:18px;
    }
    .image-listview{
        list-style: none;
        padding-left: 15px;
    }
    .img-logo-sidebar{
        margin-top: 15px;
    }
</style>
<div class="appBottomMenu">
    <a href="{!!route('home.view')!!}" class="item @if(\Route::currentRouteName() == 'home.view')active @endif">
        <div class="col">
            <ion-icon name="home-outline"></ion-icon>
        </div>
        Trang chủ
    </a>
    <a href="{!!route('frontend.messenger.index')!!}" class="item @if(\Route::currentRouteName() == 'frontend.messenger.index')active @endif">
        <div class="col">
            <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
            <span class="badge badge-danger" id='count-message'>@if($count_message > 0){!! $count_message !!}@endif</span>
        </div>
        Tin nhắn
    </a>
    <a href="{!!route('frontend.notification.index')!!}" class="item @if(\Route::currentRouteName() == 'frontend.notification.index')active @endif">
        <div class="col">
            <ion-icon name="notifications-outline"></ion-icon>
            <span class="badge badge-danger">@if (count(Auth::guard('member')->user()->unreadNotifications)){{count(Auth::guard('member')->user()->unreadNotifications)}}@endif</span>
        </div>
        Thông báo
    </a>
    <a href="javascript:void(0);" class="item" data-toggle="modal" data-target="#sidebarPanel">
        <div class="col">
            <ion-icon name="menu-outline"></ion-icon>
        </div>
        Công cụ
    </a>
</div>
<!-- * App Bottom Menu -->
<!-- App Sidebar -->
<div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="margin-top:18px">
        <div class="modal-content">
            <div class="modal-body p-0">
                <!-- profile box -->
                <div class="">
                    <div class="img-logo-sidebar">
                        <img src="/img/i-vibo1.png">
                    </div>
                    <div class="img-avatar text-center">
                        <img src="/{{\Auth::guard('member')->user()->file() ? \Auth::guard('member')->user()->file()->link : 'img/no_avatar.png'}}" alt="image" class="imaged rounded">
                    </div>
                    <h4 class="full-name">{{\Auth::guard('member')->user()->full_name}} ({{\Auth::guard('member')->user()->login_id}})</h4>
                </div>
                <!-- * profile box -->
                <ul class="flush transparent image-listview mt-2">
                    <li>
                        <a href="{!!route('home.view')!!}" class="item">
                            <div class="mr-2">
                                <img src="{!!asset('mobile/assets/upload/Portal.png')!!}" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Portal
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{!!route('frontend.schedule.index')!!}" class="item">
                            <div class="mr-2">
                                <img src="{!!asset('mobile/assets/upload/Scheduler.png')!!}" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Scheduler
                            </div>
                        </a>
                    </li>
                    <!--<li>
                        <a href="#" class="item">
                            <div class="mr-2">
                                <img src="{!!asset('mobile/assets/upload/Cabinet.png')!!}" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Cabinet
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="item">
                            <div class="mr-2">
                                <img src="{!!asset('mobile/assets/upload/Time set.png')!!}" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Timeset
                            </div>
                        </a>
                    </li>-->
                    <li>
                        <a href="{{route('frontend.todo.index')}}" class="item">
                            <div class="mr-2">
                                <img src="{!!asset('mobile/assets/upload/To_do_list.png')!!}" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                To-do list
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('frontend.project.index')}}" class="item">
                            <div class="mr-2">
                                <img src="{!!asset('mobile/assets/upload/kaizen.png')!!}" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                Kaizen
                            </div>
                        </a>
                    </li>
                    @if(\Auth::guard('member')->user()->can('tpm-index'))
                    <li>
                        <a href="{{route('frontend.tpm.dashboard')}}" class="item">
                            <div class="mr-2">
                                <img src="{!!asset('img/TPM.png')!!}" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                TPM Systems
                            </div>
                        </a>
                    </li>
                    @endif
                    <!--<li class="multi-level">
                        <a href="#" class="item">
                            <div class="mr-2">
                                <img src="{!!asset('/assets2/img/gear.png')!!}" alt="image" class="imaged w36">
                            </div>
                            <div class="in">
                                System
                            </div>
                        </a>
                        <ul class="listview link-listview">
                            <li>
                                <a href="{{route('frontend.member.index')}}" class="item">
                                    Quản lí thành viên
                                </a>
                            </li>
                            <li>
                                <a href="{{route('frontend.config.index')}}" class="item">
                                   Quản lý hệ thống
                                </a>
                            </li>
                        </ul>
                    </li>-->
                </ul>
            </div>
            <!-- sidebar buttons -->
            <div class="sidebar-buttons">
                <!-- <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                        {!!trans('base.language')!!}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item @if(session('locale') == 'vi') active @endif" href="{{route('frontend.language.change',['locale'=>'vi'])}}">{{trans('base.Vietnamese')}}</a>
                        <a class="dropdown-item @if(session('locale') == 'en') active @endif" href="{{route('frontend.language.change',['locale'=>'en'])}}">{{trans('base.English')}}</a>
                    </div>
                </div> -->
                <a href="{!!route('logoutMember')!!}" class="button">
                    Đăng xuất
                    <ion-icon name="log-out-outline" style="font-size: 25px;margin-left: 10px;"></ion-icon>
                </a>
            </div>
            <!-- * sidebar buttons -->
        </div>
    </div>
</div>
<!-- * App Sidebar -->

<!-- ///////////// Js Files ////////////////////  -->
<!-- Jquery -->
<!-- Bootstrap-->
<script src="{!!asset('assets2/js/bootbox.min.js')!!}"></script>
<script src="{{asset('mobile/assets/js/lib/popper.min.js')}}"></script>
<script src="{{asset('mobile/assets/js/lib/bootstrap.min.js')}}"></script>
<!-- Ionicons -->
<script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
<!-- Owl Carousel -->
<script src="{{asset('mobile/assets/js/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- jQuery Circle Progress -->
<!-- Base Js File -->
<script src="{!!asset('assets2/js/pusher.min.js')!!}"></script>
<script src="{{asset('mobile/assets/js/custom.js')}}"></script>
<script src="{{asset('mobile/assets/js/picker.js')}}"></script>
<script src="{{asset('mobile/assets/js/picker.date.js')}}"></script>
<script src="{{asset('mobile/assets/js/timepicker.min.js')}}"></script>
<script src="{{asset('mobile/assets/js/swiped-events.js')}}"></script>
<script src="{{asset('mobile/assets/js/base.js')}}"></script>
<input type='hidden' value='{{isset(\Auth::guard('member')->user()->id)? \Auth::guard('member')->user()->id : ''}}' id="private_id" />
