<script src="{!!asset('assets2/js/bootbox.min.js')!!}"></script>
<script src="{{asset('mobile/assets/js/lib/popper.min.js')}}"></script>
<script src="{{asset('mobile/assets/js/lib/bootstrap.min.js')}}"></script>
<!-- Ionicons -->
<script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
<!-- Owl Carousel -->
<script src="{{asset('mobile/assets/js/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- jQuery Circle Progress -->
<!-- Base Js File -->
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script src="{{asset('mobile/assets/js/custom.js')}}"></script>
<script src="{{asset('mobile/assets/js/picker.js')}}"></script>
<script src="{{asset('mobile/assets/js/picker.date.js')}}"></script>
<script src="{{asset('mobile/assets/js/timepicker.min.js')}}"></script>
<script src="{{asset('mobile/assets/js/swiped-events.js')}}"></script>
<script src="{{asset('mobile/assets/js/base.js')}}"></script>
<input type='hidden' value='{{isset(\Auth::guard('member')->user()->id)? \Auth::guard('member')->user()->id : ''}}' id="private_id" />
