@extends('mobile.layouts.kaizen')
@section('content')
<link href="{{ asset('mobile/assets/css/jquery.lighter.css') }}" rel="stylesheet" type="text/css" />
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('frontend.project.list')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">Chi tiết đề án</div>
</div>
<body class="page-body">
    <div class="extraHeader p-0">
        <div class="form-wizard-section">
            @foreach(\App\Project::Progress_arr as $key=>$val)
                @if($progress >= $key && \App\LogApproved::where('project_id',$record->id)->where('progress',$key)->first() && \App\LogApproved::where('project_id',$record->id)->where('progress',$key)->first()->status == 2)
                <a href="javascript:void(0)" data-progress="{{$key}}" class="history button-item active">
                    <strong>{{$key}}</strong>
                    <p>{{trans('base.'.$val)}}</p>
                </a>
                @elseif($progress >= $key && \App\LogApproved::where('project_id',$record->id)->where('progress',$key)->first() && \App\LogApproved::where('project_id',$record->id)->where('progress',$key)->first()->status == 0) 
                <a href="javascript:void(0)" data-progress="{{$key}}" class="button-item error history">
                    <strong>{{$key}}</strong>
                    <p>{{trans('base.'.$val)}}</p>
                </a>
                @elseif($progress >= $key)
                <a href="javascript:void(0)" class="button-item active">
                    <strong>{{$key}}</strong>
                    <p>{{trans('base.'.$val)}}</p>
                </a>
                @else
                <a href="javascript:void(0)" class="button-item">
                    <strong>{{$key}}</strong>
                    <p>{{trans('base.'.$val)}}</p>
                </a>
                @endif
            @endforeach
        </div>
    </div>
    <div id="appCapsule" class="extra-header-active">
        <div class="section mb-2 mt-2 full">
            <div class="wide-block pt-2 pb-2">
                    <h2 class="fs-title" style="width:100%;text-align: center;margin-bottom: 16px;">{{$record->name}}</h2>
                    <h6 style="font-size:1rem;"><span class="orange">{{trans('base.BEFORE')}}</span><span> {{trans('base.IMPROVE')}}</span></h6>
                    <p>
                        {!!$record->before_content!!}
                    </p>
                    @if($record->before_images)
                        @foreach($record->before_images as $key=>$val)
                            <img src="{{asset($val->link)}}" style="width:45%;padding:10px;">
                        @endforeach
                    @endif
                    @if(count($record->before_files) > 0)
                    <div>
                        <p class='bold text-left'>File đính kèm:</p>
                        @foreach($record->before_files as $key=>$value)
                            <p class='text-left'><a href='{{asset($value->link)}}'> {{$value->name}} ({{$value->size}})</a></p>
                        @endforeach
                    </div>
                    @endif
                    <span class="extension--icon-pulse--learn"></span>
                    @if(!is_null($record->after_content))
                    <h6 style="font-size:1rem;"><span class="orange">{{trans('base.AFTER')}}</span> <span>{{trans('base.IMPROVE')}}</span></h6>
                    <p>
                     {!!$record->after_content!!}
                    </p>
                    @if($record->after_images)
                        @foreach($record->after_images as $key=>$val)
                            <img src="{{asset($val->link)}}" style="width:45%;padding:10px;">
                        @endforeach
                    @endif
                    @if(count($record->after_files) > 0)
                    <div>
                        <p class='bold text-left'>File đính kèm:</p>
                        @foreach($record->after_files as $key=>$value)
                            <p class='text-left'><a href='{{asset($value->link)}}'> {{$value->name}} ({{$value->size}})</a></p>
                        @endforeach
                    </div>
                    @endif
                    <span class="extension--icon-pulse--learn"></span>
                    @endif
                    @if(count($list_member_project) > 0)
                        @foreach($list_member_project as $key=>$val)
                        <h3 class="fs-subtitle"><span class="orange">Người báo cáo: {{$val->member->full_name}}</span></h3> 
                        <h3 class="fs-subtitle"><span class="orange">Nội dung:</span></h3>
                        <div class="content-project">
                            {!!$val->after_content!!}
                        </div>
                        @if($val->images)
                        <div id="demo-test-gallery" class="demo-gallery" style="width:100%">
                            @foreach($val->images as $key=>$val)
                            <img src="{{asset($val->link)}}" style="width:45%;padding:10px;">
                            @endforeach
                        </div>
                        @endif
                        <span class="extension--icon-pulse--learn"></span>
                        @endforeach
                    @endif
               
            </div>
        </div>
    </div>
</body>
@if(($member_approved && $member_approved->member_id == \Auth::guard('member')->user()->id && $progress < 3) || ($progress == 3 && $member_approved && $member_approved->member_id == \Auth::guard('member')->user()->id) || ($progress == 4 || ($progress == 6 && \App\LogApproved::where('project_id',$record->id)->where('progress',6)->first()->status == 0))  && in_array(\Auth::guard('member')->user()->id,$record->memberproject->pluck('member_id')->toArray()) || (($progress == 5 || ($progress == 6 && \App\LogApproved::where('project_id',$record->id)->where('progress',6)->first()->status == 1)) && $record->member->department_id == \Auth::guard('member')->user()->department_id && \Auth::guard('member')->user()->level == 3))
<div class="fab-button animate bottom-right dropdown">
    <a href="#" class="fab" data-toggle="dropdown">
        <ion-icon name="add-outline"></ion-icon>
    </a>
    <div class="dropdown-menu">
        @if($member_approved && $member_approved->member_id == \Auth::guard('member')->user()->id && $progress < 3)
            <a href="javascript:void(0)" class="dropdown-item danger" data-toggle="modal" data-target="#modal_return_project">
                <ion-icon name="arrow-undo-outline"></ion-icon>
                <p>{{trans('base.Return')}}</p>
            </a>
        @if(\Auth::guard('member')->user()->level == 3)
            <a href="javascript:void(0)" class="change-approved dropdown-item success" data-project_id="{{$record->id}}">
                <ion-icon name="arrow-forward-circle-outline"></ion-icon>
                <p>Chuyển người duyệt</p>
            </a>
        @endif
            <a href="javascript:void(0)" class="dropdown-item send-project success" data-project_id="{{$record->id}}">
                <ion-icon name="arrow-redo-outline"></ion-icon>
                <p>{{trans('base.Approve_project')}}</p>
            </a>
        @endif
        @if(($progress == 3 && $member_approved && $member_approved->member_id == \Auth::guard('member')->user()->id))
            <a href="javascript:void(0)" class="dropdown-item primary" data-toggle="modal" data-target="#modal_assign_project">
                <ion-icon name="arrow-redo-outline"></ion-icon>
                <p>Giao việc</p>
            </a>
        @endif
        @if(($progress == 4 || ($progress == 6 && \App\LogApproved::where('project_id',$record->id)->where('progress',6)->first()->status == 0))  && in_array(\Auth::guard('member')->user()->id,$record->memberproject->pluck('member_id')->toArray()))
            <a href="javascript:void(0)" class="dropdown-item primary" data-toggle="modal" data-target="#modal_report_project">
                <ion-icon name="arrow-redo-outline"></ion-icon>
                <p>Báo cáo kết quả</p>
            </a>
        @endif
        @if(($progress == 5 || ($progress == 6 && \App\LogApproved::where('project_id',$record->id)->where('progress',6)->first()->status == 1)) && $record->member->department_id == \Auth::guard('member')->user()->department_id && \Auth::guard('member')->user()->level == 3)
            <a href="javascript:void(0)" class="dropdown-item danger" data-toggle="modal" data-target="#modal_unapproved_project">
                <ion-icon name="arrow-undo-outline"></ion-icon>
                <p>{{trans('base.Return')}}</p>
            </a>
            <a href="{{route('frontend.project.approved',$record->id)}}" class="dropdown-item success" data-project_id="{{$record->id}}">
                <ion-icon name="arrow-redo-outline"></ion-icon>
                <p>Hoàn thành</p>
            </a>
        @endif
    </div>
</div>
@endif
<div class="modal fade" id="modal_return_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">LÝ DO<span class="orange"> TRẢ VỀ</span></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmReason'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value='{{$record->id}}'>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='reason' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='submit-return-project'><i class="icon-reply" style="color:#fff"></i> Trả về </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_send_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">DUYỆT ĐỀ ÁN</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid pd-0">
                    <form method="post" id='frmSend'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' id="send_project_id">
                        @if(\Auth::guard('member')->user()->level == 2)
                        <div class="row">
                            <p class="col-md-12 row">Chọn cấp tiếp theo duyệt đề án</p>
                            <div class="form-group" style="width:100%;">
                                <select class="form-control select choose-member-approved" name="member_approved_id" data-placeholder="{{trans('base.Choose_member')}}">
                                    {!!$member_approved_html!!}
                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <p class="col-md-12">Comment</p>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class='form-control' name='comment' rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="button" class='submit-send-project btn btn-success' name="type" value="1">{{trans('base.Approve_project')}} </button>
                            @if(\Auth::guard('member')->user()->level == \App\Project::STATUS_ACTIVE)
                            <button type="button" class='submit-send-project btn btn-danger' name="type" value="2">{{trans('base.Completed')}} </button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_change_approved" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">CHUYỂN NGƯỜI DUYỆT</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmSend'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' id="send_project_id">
                        <div class="row">
                            <p class="col-md-12 row">Chọn người duyệt đề án</p>
                            <div class="form-group" style="width:100%;">
                                <select class="form-control select choose-member-approved" name="member_approved_id" data-placeholder="{{trans('base.Choose_member')}}">
                                    {!!$member_approved_html!!}
                                </select>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="button" class='submit-send-project btn btn-success' name="type" value="1">Gửi </button>
                           
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_show_history" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Lịch sử</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid content-history pd-0">
                    
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_assign_project" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Giao việc</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id='frmAssign'>
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <input type='hidden' name='project_id' value="{{$record->id}}">
                    <div class="list-member">
                        <div class="row">
                            <div class="form-group col-md-12" style="width: 100%;">
                                <label>Nhân viên</label>
                                <select class="form-control select-search mySelect" name="member_id[]" data-placeholder="Chọn thành viên">
                                    {!!$member_html!!}
                                </select>
                            </div>
                            <div class="form-group col-md-12" style="width: 100%;">
                                <label class="">Công việc</label>
                                <textarea class="form-control" name="work[]"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class='text-center' style="margin-top:20px;">
                        <button type="button" class='add-member btn-primary btn'>Thêm thành viên</button>
                        <button type="submit" class='submit-send-project btn-success btn' name="type" value="1">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_report_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Báo cáo kết quả</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid pd-0">
                    <form id="frmReportProject">
                        <input type="hidden" name="project_id" value="{{$record->id}}">
                        <div class="form-group">
                            <label class="required control-label text-left text-semibold"><span class="orange">Nội dung </label>
                            <div style="margin-bottom: 10px;width: 100%">
                                <textarea class="form-control" placeholder="Nhập nội dung báo cáo" id="after_content" name="after_content" rows="5">{!!old('after_content')!!}</textarea>
                            </div>
                            <div class="col-12 p-0">
                                <div class="div-image">
                                    <span class="btn btn-warning btn-file h-100 pl-1 pr-1">
                                        Hình ảnh<input type="file" accept="image/*" class="upload-img" capture data-type ="{{\App\File::TYPE_IMAGE_PROJECT_REPORT}}">
                                    </span>
                                    <input type='hidden' class='image_data' name='image_after_id'>
                                    <ul class="list-img">
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class='text-center' style="margin-top:20px;">
                            <button type="submit" class='submit-report-project btn btn-primary'>Gửi báo cáo</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_unapproved_project" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">{{trans('base.Reason_for_return')}}</span></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form method="post" id='frmReasonUnapproved'>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <input type='hidden' name='project_id' value='{{$record->id}}'>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <textarea class='form-control' name='reason' rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='text-center'>
                            <button type="submit" class='submit-return-project'><i class="icon-reply" style="color:#fff"></i> {{trans('base.Return')}} </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{{ asset('mobile/assets/js/jquery.lighter.js') }}" type="text/javascript"></script>
<script src="{!! asset('assets2/js/select2.min.js') !!}"></script>
<script src="{!! asset('assets2/js/custom.js') !!}"></script>
<script src="{!! asset('assets2/js/zoom.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script src="{!!asset('assets2/js/Notifier.min.js')!!}"></script>
<script>
$('body').delegate('.upload-img','change',function(){
    $this = $(this);
    var file_data = $(this).prop('files')[0];
    var form_data = new FormData();
    var $input = $this.parents('.div-image').find('.image_data');
    form_data.append('file[]', file_data);
    form_data.append('type',$(this).data('type'));
    $.ajax({
        url: '/api/upload',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
            if (response.success == "true") {
                var data = response.data[0];
                if($input.val() != ''){
                    file_id = $input.val().split(',');
                }else{
                    file_id = [];
                }
                file_id.push(data.id);
                $input.val(file_id);
                $this.parent().parent().find('.list-img').append('<li><a href="/'+data.link+'" data-lighter><img src="/'+data.link+'"/></a></li>');
            } else {
                alert('File upload không hợp lệ');
            }

        }
    });
})
$('#check_member').change(function () {
    if ($(this).is(':checked')) {
        $('.choose-member').prop('disabled', false);
    } else {
        $('.choose-member').prop('disabled', 'disabled');
    }
})
$('#check_level').change(function () {
    if ($(this).is(':checked')) {
        $('.choose-level').prop('disabled', false);
    } else {
        $('.choose-level').prop('disabled', 'disabled');
    }
})
$('body').delegate('.send-project', 'click', function () {
    var project_id = $(this).data('project_id');
    $('#send_project_id').val(project_id);
    $('#modal_send_project').modal('show');
})
$('#frmReason').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: "/api/return-project",
        method: "POST",
        data: form.serialize(),
        success: function (response) {
            if (response.success == true) {
                $('#modal_return_project').modal('hide');
                
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        }
    })
});
$('.submit-send-project').click(function(){
        
            if($(this).attr('name')) {
                $(this).parents('form').append(
                    $("<input type='hidden'>").attr( { 
                        name: $(this).attr('name'), 
                        value: $(this).attr('value') })
                );
            }
            $('#frmSend').submit();
        
});
$('#frmSend').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: "/api/send-project",
        method: "POST",
        data: form.serialize(),
        success: function (response) {
            if (response.success == true) {
                $('#modal_send_project').modal('hide');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }
        }
    })
});
$('body').delegate('.save-project', 'click', function () {
    var project_id = $(this).data('project_id');
    $.ajax({
        url: '/api/save-project',
        method: 'POST',
        data: {project_id: project_id},
        success: function (response) {
            var notifier = new Notifier();
            var notification = notifier.notify("success", "Lưu thành công");
            notification.push();
        }
    });
})
$('.history').click(function(){
        var progress = $(this).data('progress');
        $.ajax({
            url: "/api/get-history",
            method: "POST",
            data: {progress:progress,project_id:{{$record->id}}},
            success: function (response) {
                if (response.success == true) {
                    $('.content-history').html(response.html);
                    $('#modal_show_history').modal('show');
                }
            }
        })
    })
$('#frmAssign').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "/api/send-assign",
            method: "POST",
            data: form.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $('#modal_assign_project').modal('hide')
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        })
    })
$('#frmReportProject').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "/api/report-project",
            method: "POST",
            data: form.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $('#modal_report_project').modal('hide');
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        })
    })
    $('#frmReasonUnapproved').submit(function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "/api/unapproved-project",
            method: "POST",
            data: form.serialize(),
            success: function (response) {
                if (response.success == true) {
                    $('#modal_unapproved_project').modal('hide');
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        })
    })
    $('.change-approved').click(function(){
        $('#modal_change_approved').modal('show');
    })
    $('#frmChangeApproved').submit(function(e){
        e.preventDefault();
        var form = $(this);
        if($('.choose-change-approved').val() == ''){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn người duyệt đề án");
            notification.push();
        }else{
            $.ajax({
                url: "/api/change-member-approved",
                method: "POST",
                data: form.serialize(),
                success: function (response) {
                    if (response.success == true) {
                        $('#modal_change_approved').modal('hide');
                        
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            })
        }
    })
    $('body').delegate('.add-member','click',function(){
        $('.list-member').append(`<div class="row">
                                    <div class="form-group col-md-5">
                                        <label>Nhân viên</label>
                                        <select class="form-control select-search mySelect" data-placeholder="Chọn thành viên" name='member_id[]'>
                                            {!!$member_html!!}
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="">Công việc</label>
                                        <textarea class="form-control" name="work[]"></textarea>
                                    </div>
                                    <div class="form-group col-md-1">
                                     <button class="btn btn-danger btn-delete-member" type="button">Xóa</button>
                                    </div>
                                  </div>`);
        $('.select-search').select2();
    })
    $('body').delegate('.btn-delete-member','click',function(){
        $(this).parents('.row').remove();
    })
</script>
@stop