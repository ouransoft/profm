<div class="modal fade panelbox panelbox-right" id="PanelRight" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header mb-0">
                <h2 class="modal-title">Kaizen</h2>
                <a href="javascript:;" data-dismiss="modal" class="panel-close" style="line-height: initial;color: #333">
                    <ion-icon name="close-outline"></ion-icon>
                </a>
            </div>
            <div class="modal-body p-0">
                <ul class="listview list-view-project transparent image-listview">
                    <li  @if(Route::currentRouteName() == 'frontend.project.list' && !isset($_GET['keyword_project'])) class="active" @endif>
                        <a href="{!!route('frontend.project.list')!!}" class="item">
                            <div class="icon-box bg-warning">
                                <ion-icon name="list-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Danh sách đề án</div>
                                @if(\App\Project::where('is_deleted',0)->count() > 0)
                                <span class="badge badge-danger">{!!\App\Project::where('is_deleted',0)->count()!!}</span>
                                @endif
                            </div>
                        </a>
                    </li>
                    <li  @if(Route::currentRouteName() == 'frontend.project.list' && isset($_GET['keyword_project']) && $_GET['keyword_project'] == 'pending') class="active" @endif>
                          <a href="{!!route('frontend.project.list',['keyword_project'=>'pending'])!!}" class="item">
                            <div class="icon-box bg-warning">
                                <ion-icon name="checkbox-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Đề án cần phê duyệt</div>
                               
                            </div>
                        </a>
                    </li>
                    <li @if(Route::currentRouteName() == 'frontend.project.index' && !isset($_GET['keyword'])) class="active" @endif>
                        <a href="{!!route('frontend.project.index')!!}" class="item">
                            <div class="icon-box bg-warning">
                               <ion-icon name="person-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Đề án cá nhân</div>
                                @if(\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',0)->count() > 0)
                                <span class="badge badge-danger"> {!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',0)->count()!!}</span>
                                @endif
                            </div>
                        </a>
                    </li>
                    <li @if(isset($_GET['keyword']) && $_GET['keyword'] == 'save') class="active" @endif>
                        <a href="{!!route('frontend.project.index',['keyword'=>'save'])!!}" class="item">
                            <div class="icon-box bg-warning">
                                <ion-icon name="save-outline"></ion-icon>
                            </div>
                            <div class="in">
                                Lưu lại
                                @if(\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_saved',1)->where('is_deleted',0)->count() > 0)
                                <span class="badge badge-danger"> {!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_saved',1)->where('is_deleted',0)->count()!!}</span>
                                @endif
                            </div>
                        </a>
                    </li>
                    <li @if(isset($_GET['keyword']) && $_GET['keyword'] == 'send') class="active" @endif>
                        <a href="{!!route('frontend.project.index',['keyword'=>'send'])!!}" class="item">
                            <div class="icon-box bg-warning">
                               <ion-icon name="send-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Đã gửi</div>
                                @if(\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status','>',0)->where('is_deleted',0)->count() > 0)
                                 <span class="badge badge-danger">{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status','>',0)->where('is_deleted',0)->count()!!}</span>
                                @endif
                            </div>
                        </a>
                    </li>
                    <li @if(isset($_GET['keyword']) && $_GET['keyword'] == 'draft') class="active" @endif>
                        <a href="{!!route('frontend.project.index',['keyword'=>'draft'])!!}" class="item">
                            <div class="icon-box bg-warning">
                                <ion-icon name="bookmark-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Nháp</div>
                                @if(\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status',0)->where('is_deleted',0)->count() > 0)
                                <span class="badge badge-danger">{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('status',0)->where('is_deleted',0)->count()!!}</span>
                                @endif
                            </div>
                        </a>
                    </li>
                    <li @if(isset($_GET['keyword']) && $_GET['keyword'] == 'draft') class="active" @endif>
                        <a href="javascript:void(0)" class="item chart-list" data-href="{{route('frontend.project.chart')}}">
                            <div class="icon-box bg-warning">
                               <ion-icon name="stats-chart-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Biểu đồ thống kê</div>
                            </div>
                        </a>
                    </li>
                    <li @if(isset($_GET['keyword']) && $_GET['keyword'] == 'remove') class="active" @endif>
                        <a href="{!!route('frontend.project.index',['keyword'=>'remove'])!!}" class="item">
                            <div class="icon-box bg-warning">
                                <ion-icon name="trash-outline"></ion-icon>
                            </div>
                            <div class="in">
                                <div>Thùng rác</div>
                                @if(\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',1)->count() > 0)
                                <span class="badge badge-danger">{!!\App\Project::where('member_id',\Auth::guard('member')->user()->id)->where('is_deleted',1)->count()!!}</span>
                                @endif
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>