@extends('mobile.layouts.kaizen')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('home.view')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">Đề án</div>
    <div class="right">
        <a href="javascript:void(0)" class="headerButton" data-toggle="modal" data-target="#PanelRight">
            <ion-icon name="menu-outline" role="img" class="md hydrated" ></ion-icon>
        </a>
    </div>
</div>
<body class="page-body">
    <a href="{{route('frontend.project.create')}}" class="button btn-warning insert-fixed-btn show px-1">
        <ion-icon name="add" role="img" class="md hydrated" aria-label="arrow up outline"></ion-icon>
    </a>
    <div id="appCapsule">
        <div class="row" style="margin:0px;width:100%;">
            <div class="col-md-12 project-content" style="padding:0px;">
                <ul class="listview image-listview list-project">
                    @foreach($records as $key=>$record)
                    <li>
                        <a @if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0) href="{!!route('frontend.project.edit',$record->id)!!}" @else href="{!!route('frontend.project.view',$record->id)!!}" @endif class="item">
                            {!!$record->getStatusMobile!!}
                            <div class="in">
                                {{$record->name}}
                                <span style="float:right;font-size:12px">{{date('d',strtotime($record->created_at))}}/{{date('m',strtotime($record->created_at))}}</span>
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class='show-chart'>
            </div>
        </div>
    </div>
</body>
@include('mobile.project.sidebar')
@stop
@section('script')
@parent
<script src="{!!asset('assets2/js/select2.min.js')!!}"></script>
<script src="{!!asset('assets2/js/Notifier.min.js')!!}"></script>
<script src="{!! asset('assets2/js/project.js') !!}"></script>
<script src="{!! asset('assets2/js/custom.js') !!}"></script>
<script>
    $('.chart-list').click(function(){
            $('.project-content').attr('style', 'display:none');
            $(".show-chart").attr('style', 'display:block;text-align:center;');
            var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
            $(".show-chart").html(ajax_load).load($(this).data('href'));
    })
    $('.list-view-project li').click(function(){
           $('.list-view-project li').removeClass('active');
           $(this).addClass('active');        
    })
</script>
@stop