@extends('mobile.layouts.kaizen')
@section('content')
<link href="{{ asset('mobile/assets/css/jquery.lighter.css') }}" rel="stylesheet" type="text/css" />
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('frontend.project.index')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">Chỉnh sửa đề án</div>
</div>
<body class="page-body">
    <div id="appCapsule">
        <div class="row" style="margin:0px;width:100%">
            @include('mobile/project/sidebar')
            <div class="col-md-12" style="padding:0px;">
                <div class="content project-content">
                    <div class="form-create">
                        <form method="POST" action="{{route('frontend.project.update',$record->id)}}">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            <div class="row">
                                <div class="notification-member col-md-6">
                                    <div class="row" id='info_member' style="border: 1px solid;margin: 0px;border-radius: 4px;">
                                        <div class="col-5" style='padding-left: 0px'>
                                            <div class="img-member">
                                                <img src="{!!\Auth::guard('member')->user()->avatar!!}">
                                            </div>
                                        </div>
                                        <div class="col-7">
                                            <h3 class="mb-0" style="margin-top: 16px;">{!!\Auth::guard('member')->user()->full_name!!}</h3>
                                            <p>Chức vụ: <span>@if(\Auth::guard('member')->user()->position){{\Auth::guard('member')->user()->position->name}} @endif</span></p>
                                            <p>Bộ phận: <span>@if(\Auth::guard('member')->user()->part) {!!\Auth::guard('member')->user()->part->name!!} @endif</span></p>
                                            <p>Team: <span>@if(\Auth::guard('member')->user()->team) {!!\Auth::guard('member')->user()->team->name!!} @endif</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class=" col-form-label font-weight-bold">TÊN ĐỀ TÀI</label>
                                <div>
                                    <input class="form-control" name="name" type="text" value='{{$record->name}}' @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif> 
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-form-label text-left font-weight-bold"><span class="orange">TRƯỚC</span> CẢI TIẾN: </label>
                                <div class="w-100 mb-1">
                                    <textarea class="form-control" @if($record->status != \App\Project::STATUS_CANCEL && $record->status != 0) disabled @endif placeholder="Nhập nội dung trước cải tiến ..." style="width: 100%!important;" rows="5" id="before_content" name="before_content">{!!$record->before_content!!}</textarea>
                                </div>
                                <div class="col-12 p-0">
                                    <div class="div-image">
                                        <span class="btn btn-warning btn-file h-100 pl-1 pr-1">
                                            Hình ảnh<input type="file" accept="image/*" class="upload-img" capture data-type ="{{\App\File::TYPE_IMAGE_PROJECT_BEFORE}}">
                                        </span>
                                        <input type='hidden' class='image_data' name='image_before_id'>
                                        <ul class="list-img">
                                            @foreach($record->before_images as $key=>$value)
                                            <li><a href="/{{$value->link}}" data-lighter><img src="/{{$value->link}}"/></a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group boxed py-3 pb-0">
                                    <div class="input-wrapper">
                                        <label class="label" for="before_attachments">
                                            <h4 class="d-flex align-items-center">
                                                <span>File đính kèm</span>
                                                <ion-icon class="text-warning md hydrated" name="attach-outline" style="font-size: 22px;" role="img" aria-label="attach outline"></ion-icon>
                                            </h4>
                                        </label>
                                        <input id="before_attachments" type="file" multiple="" data-type="{{\App\File::TYPE_FILE_PROJECT_BEFORE}}">
                                        <div id="list_file_upload_before">
                                            <table>
                                                <tbody>
                                                @if(count($record->before_files) > 0) 
                                                    @foreach($record->before_files as $key=>$value)
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="{{$value->id}}">{{$value->name}} ({{$value->size}} Mb)
                                                            </td> 
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            <table>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <label class="col-form-label text-left font-weight-bold"><span class="orange">SAU</span> CẢI TIẾN: </label>
                                <div class="w-100 mb-1">
                                    <textarea class="form-control" placeholder="Nhập nội dung sau cải tiến" style="width: 100%!important;" rows="5" id="after_content" name="after_content">{!!old('after_content')!!}</textarea>
                                </div>
                                <div class="col-12 p-0">
                                    <div class="div-image">
                                        <span class="btn btn-warning btn-file h-100 pl-1 pr-1">
                                            Hình ảnh<input type="file" accept="image/*" class="upload-img" capture data-type ="{{\App\File::TYPE_IMAGE_PROJECT_AFTER}}">
                                        </span>
                                        <input type='hidden' class='image_data' name='image_after_id'>
                                        <ul class="list-img">
                                            @foreach($record->after_images as $key=>$value)
                                                <li><a href="/{{$value->link}}" data-lighter><img src="/{{$value->link}}"/></a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group boxed py-3 pb-0">
                                    <div class="input-wrapper">
                                        <label class="label" for="after_attachments">
                                            <h4 class="d-flex align-items-center">
                                                <span>File đính kèm</span>
                                                <ion-icon class="text-warning md hydrated" name="attach-outline" style="font-size: 22px;" role="img" aria-label="attach outline"></ion-icon>
                                            </h4>
                                        </label>
                                        <input id="after_attachments" type="file" multiple="" data-type="{{\App\File::TYPE_FILE_PROJECT_AFTER}}">
                                        <div id="list_file_upload_after">
                                            <table>
                                                <tbody>
                                                @if(count($record->after_files) > 0) 
                                                    @foreach($record->after_files as $key=>$value)
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="{{$value->id}}">{{$value->name}} ({{$value->size}} Mb)
                                                            </td> 
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-check" style="padding-left:0px;">
                                    <div class="radio">
                                        <label for="check_personal"><input type="radio" name="type" id="check_personal" value="1" @if($record->type == 1) checked @endif style="width:22px;height:22px!important;"><span style="margin-bottom: 12px;position: absolute;left: 32px;">ĐỀ ÁN CÁ NHÂN</span></label>
                                    </div>
                                </div>
                                <div class="form-check" style="padding-left:0px;">
                                    <div class="radio">
                                        <label for="check_team"><input type="radio" name="type" id="check_team" value="2" @if($record->type == 2) checked @endif style="width:22px;height:22px!important;"><span style="margin-bottom: 12px;position: absolute;left: 32px;">ĐỀ ÁN NHÓM</span></label>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <p class="font-weight-bold">Loại đề án</p>
                                <div class="form-check " style="padding-left:0px;">
                                    <div class="radio">
                                        <label for="check_1"><input type="radio" name="pattern" id="check_1" value="1" checked style="width:22px;height:22px!important;" @if($record->pattern ==  1) checked @endif><span style="margin-bottom: 12px;position: absolute;left: 32px;">ĐỀ ÁN LÀM NGAY</span></label>
                                    </div>
                                </div>
                                <div class="form-check" style="padding-left:0px;">
                                    <div class="radio">
                                        <label for="check_2"><input type="radio" name="pattern" id="check_2" value="2" style="width:22px;height:22px!important;" @if($record->pattern ==  2) checked @endif><span style="margin-bottom: 12px;position: absolute;left: 32px;">ĐỀ ÁN CẦN ĐƯỢC PHÊ DUYỆT</span></label>
                                    </div>
                                </div>
                            </div>
                        @if($record->status == 0 && \Auth::guard('member')->user()->level < 3)
                        <div>
                            <p class="mb-0 font-weight-bold">Chọn người duyệt đề án</p>
                            <div class="form-group" style="width:100%">
                                <select class="form-control select-search choose-member-approved" name="member_approved_id" data-placeholder="{{trans('base.Choose_member')}}">
                                    {!!$member_approved_html!!}
                                </select>
                            </div>
                        </div>
                        @endif
                        @if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0)
                        <div class="form-group">
                            @if($record->status == 0)
                            <button class="btn btn-submit btn-primary" type="submit" name="draft" value="1" style="background:#fd7700;"><ion-icon name="save-outline"></ion-icon>{{trans('base.Save')}}</button>
                            @endif
                            <button class="btn btn-submit" type="submit" name="draft" value="0" style="background:#fd7700;color:#fff;padding:0 10px">@if($record->status != 0) <ion-icon name="create-outline"></ion-icon>Chỉnh sửa @else <ion-icon name="send-outline"></ion-icon>  {{trans('base.SEND')}} @endif</button>
                            <!--<a href="#" style="padding: 11px 15px 15px;border: 1px solid #a6a5a5;"><img style="width:24px;" src="/public/assets2/img/trash.png"></a>-->
                            <a href="{{route('frontend.project.index')}}" class="btn btn-submit btn-danger"><ion-icon name="close-outline"></ion-icon>Quay lại</a>
                        </div>
                        @if($record->status != 0)
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-left font-weight-bold"><span class="orange">Lí do trả về: </label>
                            <div class="col-md-12">
                                <textarea class="form-control" readonly="">{{$record->reason}}</textarea>
                            </div>
                        </div>
                        @endif
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
@stop
@section('script')
@parent
<script src="{{ asset('mobile/assets/js/jquery.lighter.js') }}" type="text/javascript"></script>
<script src="{!! asset('assets2/js/select2.min.js') !!}"></script>
<script src="{!! asset('assets2/js/project.js') !!}"></script>
<script src="{!! asset('assets2/js/custom.js') !!}"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<script>
$('body').delegate('.upload-img','change',function(){
    $this = $(this);
    var file_data = $(this).prop('files')[0];
    var form_data = new FormData();
    var $input = $this.parents('.div-image').find('.image_data');
    form_data.append('file[]', file_data);
    form_data.append('type',$(this).data('type'));
    $.ajax({
        url: '/api/upload',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response) {
            if (response.success == "true") {
                var data = response.data[0];
                if($input.val() != ''){
                    file_id = $input.val().split(',');
                }else{
                    file_id = [];
                }
                file_id.push(data.id);
                $input.val(file_id);
                $this.parent().parent().find('.list-img').append('<li><a href="/'+data.link+'" data-lighter><img src="/'+data.link+'"/></a></li>');
            } else {
                alert('File upload không hợp lệ');
            }

        }
    });
})
$('#check_member').change(function(){
if ($(this).is(':checked')){
$('.choose-member').prop('disabled', false);
} else{
$('.choose-member').prop('disabled', 'disabled');
}
})
        $('#check_level').change(function(){
if ($(this).is(':checked')){
$('.choose-level').prop('disabled', false);
} else{
$('.choose-level').prop('disabled', 'disabled');
}
})
        $.ajaxSetup ({
        cache: false
        });
$('.load-page').click(function(){
$('.load-page').removeClass('active');
$(this).addClass('active');
var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
$(".project-content").html(ajax_load).load($(this).data('href'));
})
if($('#after_content').hasClass('disabled')){
    $(this).attr('disabled','true')
} 
$('.select-search').select2();
$('.submit-form').click(function(e){
        e.preventDefault();
        var name = $('#subject_name').val();
         $.ajax({
            url:'/api/checkProject',
            method:'POST',
            data:{name:name},
            success: function(response){
                if($('#before_content').val() == '' ||  $('input[name="name"]').val() == ''){
                    var notifier = new Notifier();
                    var notification = notifier.notify("info", "Nhập đầy đủ thông tin trước khi gửi");
                    notification.push();
                }else if($('.choose-member-approved').val() == ''){
                    var notifier = new Notifier();
                    var notification = notifier.notify("info", "Cần chọn người duyệt đề án trước khi gửi");
                    notification.push();
                }else{
                     $('#FrmUpdateProject').submit();
                }
            }
        })
    })
    $(document).on('change', '#before_attachments , #after_attachments', function () {
            var file_data = $(this).prop('files');
            var type = $(this).data('type');
            var form_data = new FormData();
            for(let i=0;i<file_data.length;i++){
                form_data.append('file[]', file_data[i]);
            }
            form_data.append('type',type);
            form_data.append('format',2);
            $.ajax({
                    url: '/api/upload-files',
                    type: 'POST',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function(response){
                        if(response.success == 'true'){
                            if(type == {{\App\File::TYPE_FILE_PROJECT_BEFORE}}){
                                response.data.forEach(element =>
                                $('#list_file_upload_before').append(`<tr>
                                                                        <td>
                                                                            <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                        </td> 
                                                                        <td>`+element.name+` (`+element.size+` Mb)</td>
                                                                    </tr>`)
                                );
                            }else{
                                response.data.forEach(element =>
                                $('#list_file_upload_after').append(`<tr>
                                                                        <td>
                                                                            <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="files[]" value="`+element.id+`">
                                                                        </td>
                                                                        <td>`+element.name+` (`+element.size+` Mb)</td>
                                                                    </tr>`)
                                );
                            }
                        }
                    }
            });
        })
</script>
@stop