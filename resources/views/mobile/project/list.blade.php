@extends('mobile.layouts.kaizen')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('home.view')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">Đề án</div>
    <div class="right">
        <a href="javascript:void(0)" class="headerButton" data-toggle="modal" data-target="#PanelRight">
            <ion-icon name="menu-outline" role="img" class="md hydrated" ></ion-icon>
        </a>
    </div>
</div>
<body class="page-body">
    <a href="{{route('frontend.project.create')}}" class="button btn-warning insert-fixed-btn show px-1">
        <ion-icon name="add" role="img" class="md hydrated" aria-label="arrow up outline"></ion-icon>
    </a>
    <div id="appCapsule">
        <div class="row" style="margin:0px;width:100%;">
            <div class="col-md-12 project-content" style="padding:0px;">
                <ul class="listview image-listview list-project">
                    @foreach($records as $key=>$record)
                    <li>
                        <a @if($record->status == \App\Project::STATUS_CANCEL || $record->status == 0) href="{!!route('frontend.project.edit',$record->id)!!}" @else href="{!!route('frontend.project.view',$record->id)!!}" @endif class="item">
                            {!!$record->getStatusMobile!!}
                            <div class="in">
                                <div>
                                    {{$record->member->full_name}}({{$record->member->login_id}})
                                    <footer>{{$record->name}}</footer>
                                </div>
                                <span style="float:right;font-size:12px">{{date('d',strtotime($record->created_at))}}/{{date('m',strtotime($record->created_at))}}</span>
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class='show-chart'>
            </div>
        </div>
    </div>
</body>
@include('mobile.project.sidebar')
@stop
@section('script')
@parent
<script src="{!! asset('assets2/js/datatables.min.js') !!}"></script>
<script src="{!! asset('assets2/js/datatables_basic.js') !!}"></script>
<script src="{!!asset('assets2/js/popper.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootstrap.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/parallax/parallax.js')!!}"></script>
<script src="{!!asset('assets2/js/scripts.js')!!}"></script>
<script src="{!!asset('assets2/js/select2.min.js')!!}"></script>
<script src="{!!asset('assets2/plugins/owl.carousel/owl.carousel.min.js')!!}"></script>
<script src="{!!asset('assets2/js/Notifier.min.js')!!}"></script>
<script src="{!!asset('assets2/js/bootbox.min.js')!!}"></script>
<script src="{!!asset('assets2/js/main.js')!!}" id="_mainJS" data-plugins="load"></script>
<script src="{!!asset('assets2/js/custom.js')!!}"></script>
<script>
    $('#check_all').change(function () {
        if ($(this).is(":checked")) {
            $('.check').each(function () {
                $(this).prop('checked', true);
            });
        } else {
            $('.check').each(function () {
                $(this).prop('checked', false);
            });
        }
    });
    $('body').delegate('.check', 'change', function(){
        var member_id = [];
        $('.check').each(function () {
            if ($(this).is(':checked')) {
                member_id.push($(this).val());
            }
            $('#check_all').attr('value', member_id.join(','));
        })
    })
    $('body').delegate('.next-list-project', 'click', function(){
        $.ajax({
                url: '/api/next-list-project',
                method: 'POST',
                success: function (response){
                    $('#records_list_project').html(response.html);
                    $('#show_list_project').html(response.show_page);
                }
        });
    });
    $('body').delegate('.forward-list-project', 'click', function(){
        $.ajax({
                url: '/api/forward-list-project',
                method: 'POST',
                success: function (response){
                    $('#records_list_project').html(response.html);
                    $('#show_list_project').html(response.show_page);
                }
        });
    });
    $('.link-list').click(function(){
        $('.table-member-content').attr('style', 'display:block');
        $('.link-list').removeClass('orange');
        $('.chart-list').removeClass('orange');
        $(this).addClass('orange');
        $(".show-chart").attr('style', 'display:none');
        var keyword_project = $(this).data('keyword_project');
        $.ajax({
        url:"/api/getListProject",
                type:"POST",
                data:{keyword_project:keyword_project},
                success: function (response){
                if (keyword_project == 'all'){
                window.location.href = '{!!route('frontend.project.list')!!}';
                }
                $('#records_list_project').html(response.html);
                $('#show_list_project').html(response.show_page);
                }
        })
    })
    $('select[name="month"],select[name="year"]').change(function(){
        var month = $('select[name="month"]').val();
        var year = $('select[name="year"]').val();
        var keyword = $('.link-list.orange').data('keyword');
        $.ajax({
        url:"/api/getListProject",
                type:"POST",
                data:{month:month, year:year, keyword:keyword},
                success: function (response){
                $('#records_list_project').html(response.html);
                $('#show_list_project').html(response.show_page);
                }
        })
    })
    $('body').delegate('.send-project', 'click', function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        } else{
            var project_id = 0;
            if ($('#check_all').is(":checked")){
            project_id = 'all';
            } else{
            project_id = $('#check_all').attr('value');
            }
            $.ajax({
            url: '/api/send-project',
                    method: 'POST',
                    data: {project_id: project_id},
                    success: function (response) {
                    setTimeout(function(){ location.reload(); }, 200);
                    $('input[type=checkbox]').prop('checked', false);
                    }
            });
        }
    })
    $('body').delegate('.return-project', 'click', function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        } else{
            var project_id = 0;
            if ($('#check_all').is(":checked")){
                project_id = 'all';
            } else{
                project_id = $('#check_all').attr('value');
            }
            $.ajax({
                    url: '/api/return-project',
                    method: 'POST',
                    data: {project_id: project_id},
                    success: function (response) {
                        var notifier = new Notifier();
                        var notification = notifier.notify("success", "Đề án trả về thành công");
                        notification.push();
                        $('input[type=checkbox]').prop('checked', false);
                    }
            });
        }
    })
    $('body').delegate('.save-project', 'click', function(){
        if ($('input[type="checkbox"]:checked').val() === undefined){
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        } else{
            var project_id = 0;
            if ($('#check_all').is(":checked")){
                project_id = 'all';
            } else{
                project_id = $('#check_all').attr('value');
            }
            $.ajax({
                    url: '/api/save-project',
                    method: 'POST',
                    data: {project_id: project_id},
                    success: function (response) {
                    var notifier = new Notifier();
                    var notification = notifier.notify("success", "Lưu thành công");
                    notification.push();
                    $('input[type=checkbox]').prop('checked', false);
                }
            });
        }
    })
    $('.chart-list').click(function(){
        $('#PanelRight').modal('hide');
        $('.project-content').attr('style', 'display:none');
        $(".show-chart").attr('style', 'display:block;text-align:center;');
        var ajax_load = "<img src='/public/assets2/img/small-loading.gif' alt='loading...' />";
        $(".show-chart").html(ajax_load).load($(this).data('href'));
    })
    $('body').delegate('.export-project', 'click', function(){
        if ($('input[type="checkbox"]:checked').val() === undefined) {
            var notifier = new Notifier();
            var notification = notifier.notify("info", "Cần chọn đề án trước khi thao tác");
            notification.push();
        } else{
            var project_id = 0;
            if ($('#check_all').is(":checked")){
            project_id = 'all';
            } else{
            project_id = $('#check_all').attr('value');
            }
            $.ajax({
            url: '/api/export-project',
                    method: 'POST',
                    data: {project_id: project_id},
                    success: function (response) {
                    window.location.href = response.href;
                    }
            });
        }
    })
    $('.list-view-project li').click(function(){
       $('.list-view-project li').removeClass('active');
       $(this).addClass('active');        
    })
</script>
@stop