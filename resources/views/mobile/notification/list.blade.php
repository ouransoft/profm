@extends('mobile.layouts.admin')
@section('content')
<div class="appHeader bg-warning text-light">
    <div class="left">
        <a href="{{route('home.view')}}" class="headerButton">
            <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle">{{ trans('base.Notification') }}</div>
</div>
<div class="extraHeader p-0">
    <ul class="nav nav-tabs lined" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#not_seen" role="tab" aria-selected="true">
                Chưa xem
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#seen" role="tab" aria-selected="false">
                Đã xem
            </a>
        </li>
    </ul>
</div>
<div id="appCapsule" class='extra-header-active'>
    <div class="tab-content">
        <div class="tab-pane fade active show" id="not_seen" role="tabpanel">
            <div class="section full">
                <ul class="listview image-listview">
                    @foreach( \Auth::guard('member')->user()->unreadNotifications as $val)
                    <li>
                        <a href="javascript:void(0)" style="width:100%" data-id="{{$val->id}}" data-link="{{$val->data['link']}}" class="item seen-notification">
                            <img src="/assets2/img/img_avatar.png" alt="image" class="image">
                            <div class="in">
                                <div>
                                    {{$val->data['full_name']}}
                                    <footer>{{$val->data['content']}}</footer>
                                </div>
                                <span class="text-muted">{{$val->data['time']}}({{date('d/m',strtotime($val->created_at))}})</span>
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="tab-pane fade" id="seen" role="tabpanel">
            <div class="section full">
                <ul class="listview image-listview">
                    @foreach(\Auth::guard('member')->user()->Notifications->where('read_at','<>',NULL) as $val)
                    <li>
                        <a href="javascript:void(0)" style="width:100%" data-id="{{$val->id}}" data-link="{{$val->data['link']}}" class="item seen-notification">
                            <img src="/assets2/img/img_avatar.png" alt="image" class="image">
                            <div class="in">
                                <div>
                                    {{$val->data['full_name']}}
                                    <footer>{{$val->data['content']}}</footer>
                                </div>
                                <span class="text-muted">{{$val->data['time']}}({{date('d/m',strtotime($val->created_at))}})</span>
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $('body').delegate('.seen-notification', 'click', function () {
        var id = $(this).data('id');
        var link = $(this).data('link');
        $.ajax({
            url: '/api/seen-notification',
            method: 'POST',
            data: {id: id, _token: '{!! csrf_token() !!}'},
            success: function (response) {
                if (response.error == false) {
                    window.location.href = link;
                }
            }
        });
    });
</script>
@stop
