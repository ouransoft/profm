@extends('mobile.home.home')
@section('content')
<div id="appCapsule">
    <div class="content project-content">
        <a class="next-sidebar sidebar-left"><i style="padding: 0px 10px 0px 2px;font-size: 22px;margin-top: 4px;" class="icon-arrow-right6"></i> <h4>Quản lý slide</h4></a>
        <div class="form-create">
            <form id='postData' data-model='slide'>
                <div class="form-group row">
                    <label class="col-md-2 required control-label text-left text-semibold">Tiều đề</label>
                    <div class="col-lg-9">
                        <input class='form-control' name='title'>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 required control-label text-left text-semibold" for="images">Hình ảnh</label>
                    <div class="col-lg-9 div-image">
                        <div class="file-input file-input-ajax-new">
                            <div class="input-group file-caption-main">
                                <div class="input-group-btn input-group-append">
                                    <div tabindex="500" class="btn btn-primary btn-file"><i class="icon-folder-open"></i>&nbsp; <span class="hidden-xs">Chọn</span>
                                        <input type="file" id="images" class="upload-images" multiple="multiple" name="file_upload[]" data-fouc="">
                                    </div>
                                </div>
                            </div>
                            <div class="file-preview ">
                                <div class=" file-drop-zone">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="image" class="image_data">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-5"></div>
                    <div class="col-md-7 col-sm-8 col-sm-offset-4 btn-group button-group">
                        <a href="/home" class="btn btn-sm btn-default">Quay lại</a>
                        <button type="button" class="btn btn-sm btn-primary btn-add">Thêm</button>
                    </div>
                </div>
            </form>
            <table class="table datatable-basic table-bordered" style="margin:0px">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tiêu đề</th>
                        <th>Tác vụ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($records as $key=>$record)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$record->title}}</td>
                        <td>
                            <a href="javascript:void(0)" title="{!! trans('base.edit') !!}" class="success edit-data" data-id='{{$record->id}}' data-model='slide'><i class="icon-pencil"></i></a>
                            <form action="{!! route('frontend.slide.destroy', ['id' => $record->id]) !!}" method="POST" style="display: inline-block;margin:0px;">
                                {!! method_field('DELETE') !!}
                                {!! csrf_field() !!}
                                <a title="{!! trans('base.delete') !!}" class="delete text-danger" data-action="delete">
                                    <i class="icon-close2"></i>
                                </a>              
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
@stop