<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller {

    public function __construct() {

    }

    public function loginMember(Request $request) {
        $input = [
            'login_id' => $request->get('login_id'),
            'password' => $request->get('password'),
            'is_deleted'=>0,
        ];
        if (Auth::guard('member')->attempt($input,true)) {
            $member = Auth::guard('member')->user();
            $member['session_id'] = session()->getId();
            $member['api_token'] = $member->createToken('MyApp')->accessToken;
            $member->save();
            //gửi tin nhắn hệ thống
            if(count(\App\Group::where('type',99)->get()) == 0){
                $group = new \App\Group();
                $group->name = "Tin nhắn hệ thống";
                $group->type = 99;
                $group->save();
                $group->member()->attach(\Auth::guard('member')->user()->id);
            }else{
                $group = \App\Group::where('type',99)->first();
                if(count(\App\GroupMember::where('group_id',$group->id)->where('member_id',\Auth::guard('member')->user()->id)->get()) == 0){
                    $group->member()->attach(\Auth::guard('member')->user()->id);
                    $data = new \App\GroupMessage();
                    $data->from = 0;
                    $data->group_id = $group->id;
                    $data->message = "Chào mừng bạn đến với I-VIBO Chats System";
                    $data->save();
                }
            }
            return Redirect::route('home.view');
        }
        return Redirect::route('home.index')->with('error', 'Đăng nhập không thành công');
    }
    public function logoutMember() {
        if(\Auth::guard('member')->user()){
            \Auth::guard('member')->user()->tokens()->delete();
        }
        \Auth::guard('member')->logout();
        return Redirect::route('home.index');
    }

}
