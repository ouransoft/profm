<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ProjectRepository;
use Illuminate\Support\Facades\Session;
use Repositories\LogApprovedRepository;
use Repositories\LogSavedRepository;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\DB;
use Repositories\LevelRepository;
use Repositories\MemberProjectRepository;
use Mail;
use Repositories\ToDoRepository;
use Stevebauman\Purify\Facades\Purify;
use Repositories\FileRepository;

class ProjectController extends Controller
{
     public function __construct(FileRepository $fileRepo,ToDoRepository $todoRepo,MemberProjectRepository $memberprojectRepo,LevelRepository $levelRepo,MemberRepository $memberRepo,ProjectRepository $projectRepo,LogApprovedRepository $logapprovedRepo,LogSavedRepository $logsavedRepo) {
        $this->projectRepo = $projectRepo;
        $this->logapprovedRepo = $logapprovedRepo;
        $this->logsavedRepo = $logsavedRepo;
        $this->memberRepo = $memberRepo;
        $this->levelRepo = $levelRepo;
        $this->memberprojectRepo = $memberprojectRepo;
        $this->todoRepo = $todoRepo;
        $this->fileRepo = $fileRepo;
    }
    public function save(Request $request){
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $project_id = $this->projectRepo->all()->pluck('id')->toArray();
        }else{
            $project_id = explode(',',$project_arr);
        }
        $this->projectRepo->save($project_id);
        foreach($project_id as $val){
            $log['member_id']= \Auth::guard('member')->user()->id;
            $log['project_id'] = $val;
            $this->logsavedRepo->create($log);
        }
        return response()->json(array('success'=>true));
    }
    public function send(Request $request){
        $project_id = $request->get('project_id');
        $project = $this->projectRepo->find($project_id);
        $log['member_id']= $request->get('member_approved_id');
        $log['project_id'] = $project_id;
        if(!$request->get('member_approved_id')){
            $level = 3;
            $log['member_id'] = \Auth::guard('member')->user()->id;
            $log['progress'] = 4;
            $log['status'] = 1;
            $this->logapprovedRepo->create($log);
        }else{
            $log['progress'] = 3;
            $log['status'] = 1;
            $log['level'] = 3;
            $this->logapprovedRepo->create($log);
            $level = 2;
        }
        $this->projectRepo->send($project_id,$level);
        if($request->get('comment')){
            $log['comment'] = $request->get('comment');
        }
        if($request->get('type') == 1){
            $log['progress'] = $level;
        }else{
            $log['progress'] = 3;
            $log['status'] = 2;
            $this->logapprovedRepo->create($log);
            $log['progress'] = 6;
            $this->logapprovedRepo->create($log);
        }
        \App\LogApproved::where('progress',$level)->where('project_id',$project_id)->update(['status'=>2,'comment'=>$request->get('comment')]);
        $record = $this->projectRepo->find($project_id);
        if($request->get('consider')){
            $this->projectRepo->update(['consider'=>$request->get('consider')],$project_id);
        }
        if($record->status < \App\Project::STATUS_ACTIVE){
            $this->memberRepo->NotificateMember($request->get('member_approved_id'), ['content' => 'Có đề án mới', 'link' => '/project/view/' . $project_id]);
            $this->memberRepo->NotificateMember($project->member_id, ['content' => 'Đề án đã được duyệt', 'link' => '/project/view/' . $project_id]);
        }
        if($request->get('type') == 1 && $level == 3){
            return response()->json(array('success'=>true,'href'=>route('frontend.project.view',$project_id)));
        }else{
            return response()->json(array('success'=>true,'href'=>route('frontend.project.list',['keyword_project'=>'pending'])));
        }
    }
    public function destroy(Request $request){
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $project_id = $this->projectRepo->getByMember(\Auth::guard('member')->user()->id)->pluck('id')->toArray();
        }else{
            $project_id = explode(',',$project_arr);
        }
        if($this->projectRepo->checkDraft($project_id) > 0){
            return response()->json(array('success'=>'false'));
        }
        if(\Auth::guard('member')->user()){
            $this->projectRepo->remove($project_id);
        }
        return response()->json(array('success'=>'true'));
    }
    public function returnProject(Request $request){
        $input = $request->all();
        $project_id = $request->get('project_id');
        $input['status'] = \App\Project::STATUS_CANCEL;
        $this->projectRepo->update($input,$project_id);
        $project = $this->projectRepo->find($project_id);
        $member = $project->logapproved()->where('level',2)->first();
        if($member){
          $this->memberRepo->NotificateMember($member->member_id, ['content' => 'Đề án trả về', 'link' => '/project/view/' . $project_id]);  
        }
        \App\LogApproved::where('project_id',$project_id)->where('progress',\Auth::guard('member')->user()->level)->update(['status'=>0,'reason'=>$input['reason']]);
        $this->memberRepo->NotificateMember($project->member_id, ['content' => 'Đề án trả về', 'link' => '/project/edit/' . $project_id]);
        return response()->json(array('success'=>true));
    }
    function htmlToPlainText($str){
        $str = html_entity_decode($str, ENT_QUOTES | ENT_XML1, 'UTF-8');
        $str = htmlspecialchars_decode($str);
        $str = html_entity_decode($str);
        $str = strip_tags($str);
        return $str;
    }
    public function export(Request $request)
    {
        $input = $request->all();
        if($input['type'] == 1){
            $href = $this->projectRepo->exportForm1($request);
        }elseif($input['type'] == 2){
            $href = $this->projectRepo->exportForm2($request);
        }elseif($input['type'] == 3){
            $href = $this->projectRepo->exportForm3($request);
        }elseif($input['type'] == 4){
            $href = $this->projectRepo->exportForm4($request);
        }elseif($input['type'] == 5){
            $href = $this->projectRepo->exportForm5($request);
        }else{
            $href = $this->projectRepo->exportForm6($request); 
        }
        return response()->json(['success' => 'true', 'href' => $href]);
        
    }
    public function exportProject(Request $request)
    {
        $project_arr = $request->get('project_id');
        if($project_arr == 'all'){
            $ids = $this->projectRepo->getAll()->pluck('id')->toArray();
        }else{
            $ids = explode(',',$project_arr);
        }
        $records = $this->projectRepo->export($ids);
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(70);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(70);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'Tên đề tài')
            ->setCellValue('C1', 'Trước cải tiến')
            ->setCellValue('D1', 'Sau cải tiến')   
            ->setCellValue('E1', 'Cấp độ')
            ->setCellValue('F1', 'Ngày');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:F1" )->applyFromArray($styleArray);
        $bold_range = ['A1', 'B1', 'C1', 'D1','E1','F1'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 2;
        $no = 1;
        foreach ($records as $key=>$record)
        {
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level = '---';
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record->name)
                ->setCellValue('C' . $rows, $this->htmlToPlainText($record->before_content))
                ->setCellValue('D' . $rows, $this->htmlToPlainText($record->after_content))
                ->setCellValue('E' . $rows, $level)
                ->setCellValue('F' . $rows, $record->created_at()) 
                ;
            $rows++;
            $styleArray = array(
                'borders' => array(
                    'top' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."F" . $rows . "")->applyFromArray($styleArray);
        }
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Bao_cao_luong_lai_xe_ngay_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/De_an_'.$timestamp.'.xls');
        $href = 'http://' . \Request::server('SERVER_NAME') . '/file/De_an_' . $timestamp . '.xls';
        return response()->json(['success' => 'false', 'href' => $href]);
    }
    public function searchProject(Request $request){
        Session::put('list_page',1);
        $search = $request->all();
        $records = $this->projectRepo->search($search);
        $html='';
        foreach($records as $key=>$record){
            if($record->levels){
                $level = $record->levels->name;
            }else{
                $level ='';
            }
            if($record->status == \App\Project::STATUS_CANCEL){
                $status = '<span class="badge badge-danger">Trả về</span>';
            }elseif($record->status < \App\Project::STATUS_ACTIVE ){
                $status = '<span class="badge badge-secondary">Chờ duyệt</span>';
            }else{
                $status = '<span class="badge badge-success">Đã duyệt</span>';
            }
            $html .='<tr>
                        <td class="middle"><input type="checkbox" value="'.$record->id.'" name="member_id" class="check"></td>
                        <td  class="middle">'.++$key.'</td>
                        
                        <td  class="middle">'.$record->member->full_name.'</td>
                        <td  class="middle">'.$record->member->department->name.'</td>
                        <td><a href="'.route('frontend.project.view',$record->id).'">'.$record->name.'</a></td>
                        <td  class="middle"><span class="badge badge-danger">'.$level.'</span></td>
                        <td  class="middle">'.$status.'</td>
                        <td  class="middle">'.$record->created_at().'</td>
                    </tr>';
        }
        $start = 1;
        if(Session::get('_list_pages') == 0){
            $start = 0;
        }
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_list_pages').' trong số '.Session::get('_list_count').'</p>';
        if(session('list_page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-list-project"><img src="/public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_list_count') > session('_list_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-list-project"><img src="/public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
    public function updateLevel(Request $request){
        $project = $this->projectRepo->find($request->get('project_id'));
        $this->projectRepo->update(['level'=>$request->get('level'),'consider'=>2],$project->id);
        return response()->json(array('success'=>true));
    }
    public function getSelectLevel(Request $request){
        $project_id = $request->get('project_id');
        $record = $this->projectRepo->find($project_id);
        $html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all(),$record->level);
        return response()->json(array('html'=>$html));
    }
    public function checkName(Request $request){
        if($this->projectRepo->checkName($request->name)){
            return response()->json(array('success'=>'true'));
        }
    }
    public function addAssign(Request $request){
        $input = Purify::clean($request->all());
        \App\LogApproved::where('project_id',$input['project_id'])->where('progress',4)->update(['status'=>2]);
        foreach($input['member_id'] as $key=>$val){
            //send notification
            $this->memberRepo->NotificateMember($val,['content' => 'Đã giao cho bạn 1 công việc ', 'link' => '/project/view/' . $input['project_id']]);
            // create todo
            $input_todo['created_by'] = \Auth::guard('member')->user()->id;
            $input_todo['title'] = $input['work'][$key];
            $input_todo['start_date'] = date('Y-m-d');
            $input_todo['end_date'] = date('Y-m-d');
            $input_todo['priority'] = 3;
            $todo = $this->todoRepo->create($input_todo);
            // create relationship member_todo
            $input_todo['member_id']= [];
            $input_todo['member_id'][] = $val;
            $todo->member()->attach($input_todo['member_id']);
            // create relationship member_project
            $detail['member_id'] = $val;
            $detail['todo_id'] = $todo->id;
            $detail['project_id'] = $input['project_id'];
            $this->memberprojectRepo->create($detail);
            //create log project
            $log['member_id'] = $val;
            $log['project_id'] = $input['project_id'];
            $log['progress'] = 5;
            $this->logapprovedRepo->create($log);
            // send email notification
            $member = $this->memberRepo->find($val);
            if(!is_null($member->email)){
                Mail::send('mail', array('title'=>$todo->title,'content'=>$todo->content,'link'=>(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/todo/view/'.$todo->id), function($message) use ($member){
                    $message->to($member->email, 'Ouransoft')->subject('Thông báo giao việc từ '.\Auth::guard('member')->user()->full_name);
                });
            }
        }
        return response()->json(array('success'=>true));
    }
    public function getHistory(Request $request){
        $input = $request->all();
        $logapproved = $this->logapprovedRepo->getByProgress($input['progress'],$input['project_id']);
        $member = $this->memberRepo->find($logapproved->member_id);
        switch ($input['progress']) {
            case 1:
                $html = '<p>- Người tạo: '.$member->full_name.'</p>
                         <p>- Phòng: '.$member->department->name.'</p>
                         <p>- Chức vụ: '.($member->position ? $member->position->name : '').'</p>
                         <p>- Ngày tạo: '.$logapproved->updated_at().'</p>';
                break;
            case 2:
                if($logapproved->status == 0){
                    $html = '<p>- Người trả về: '.$member->full_name.'</p>
                             <p>- Phòng: '.$member->department->name.'</p>
                             <p>- Chức vụ: '.($member->position ? $member->position->name : '').'</p>
                             <p>- Lí do: '.$logapproved->reason.'</p>';
                }else{
                    $html = '<p>- Người duyệt: '.$member->full_name.'</p>
                             <p>- Phòng: '.$member->department->name.'</p>
                             <p>- Chức vụ: '.($member->position ? $member->position->name : '').'</p>
                             <p>- Ngày duyệt: '.$logapproved->updated_at().'</p>';
                    if(!is_null($logapproved->comment)){
                        $html .= '<p>- Comment: '.$logapproved->comment.'</p>';
                    }
                }
                break;
            case 3:
                if($logapproved->status == 0){
                    $html = '<p>- Người trả về: '.$member->full_name.'</p>
                             <p>- Phòng: '.$member->department->name.'</p>
                             <p>- Chức vụ: '.($member->position ? $member->position->name : '').'</p>
                             <p>- Lí do: '.$logapproved->reason.'</p>';
                }else{
                    $html = '<p>- Người duyệt: '.$member->full_name.'</p>
                             <p>- Phòng: '.$member->department->name.'</p>
                             <p>- Chức vụ: '.($member->position ? $member->position->name : '').'</p>
                             <p>- Ngày duyệt: '.$logapproved->updated_at().'</p>';
                    if(!is_null($logapproved->comment)){
                        $html .= '<p>- Comment: '.$logapproved->comment.'</p>';
                    }
                }
                break;
            case 4:
                $project = $this->projectRepo->find($input['project_id']);
                $html = '';
                foreach($project->memberproject as $key=>$val){
                    $html .= '<p>- '.$val->member->full_name.': '.$val->todo->title.'   (<a target="_blank" href="'.route('frontend.todo.view',$val->todo_id).'" class=""><u>Chi tiết công việc</u></a>)</p>';
                }
                break;
            case 5:
                $logapproveds = $this->logapprovedRepo->getListByProgress($input['progress'],$input['project_id']);
                $html = '';
                foreach($logapproveds as $key=>$logapproved){
                    $html .= '<h5>- Người báo cáo: '.$logapproved->member->full_name.'</h5>
                              <p>- Phòng: '.$logapproved->member->department->name.'</p>
                              <p>- Chức vụ: '.($logapproved->member->position ? $logapproved->member->position->name : '').'</p>
                              <p>- Ngày gửi: '.$logapproved->updated_at().'</p>';
                }
                
                break;
            case 6:
                $logapproved = $this->logapprovedRepo->getByProgress($input['progress'],$input['project_id']);
                $member = $this->memberRepo->find($logapproved->member_id);
                if($logapproved->status == 0){
                    $html = '<p>- Người trả về: '.$member->full_name.'</p>
                             <p>- Phòng: '.$member->department->name.'</p>
                             <p>- Chức vụ: '.($member->position ? $member->position->name : '').'</p>
                             <p>- Lí do: '.$logapproved->reason.'</p>';
                }else{
                    $html = '<p>- Người duyệt: '.$member->full_name.'</p>
                             <p>- Phòng: '.$member->department->name.'</p>
                             <p>- Chức vụ: '.($member->position ? $member->position->name : '').'</p>
                             <p>- Ngày duyệt: '.$logapproved->updated_at().'</p>';
                            if(!is_null($logapproved->comment)){
                                $html .= '<p>- Comment: '.$logapproved->comment.'</p>';
                            }     
                }
                break;
        }
        return response()->json(array('success'=>true,'html'=>$html));
    }
    public function report(Request $request){
        $input = $request->all();
        $input['status']= 1;
        $member_project = \App\MemberProject::where('member_id',\Auth::guard('member')->user()->id)->where('project_id',$input['project_id'])->first();
        $this->fileRepo->resetRelationshipID($member_project->id,[\App\File::TYPE_IMAGE_PROJECT_REPORT]);
        if($input['image_after_id']){
            $image_after_ids = explode(',',$input['image_after_id']);
            foreach($image_after_ids as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$member_project->id],$val);
            }
        }
        $this->fileRepo->deleteFileNull();
        unset($input['image_after_id']);
        \App\MemberProject::where('member_id',\Auth::guard('member')->user()->id)->where('project_id',$input['project_id'])->update($input);
        \App\LogApproved::where('member_id',\Auth::guard('member')->user()->id)->where('project_id',$input['project_id'])->where('progress',5)->update(['status'=>2]);
        $todo_id = $member_project->todo_id;
        \App\MemberTodo::where('member_id',\Auth::guard('member')->user()->id)->where('todo_id',$todo_id)->update(['join'=>1,'progress'=>100]);
        \App\Todo::find($todo_id)->update(['status'=>3]);
        $check = \App\LogApproved::where('project_id',$input['project_id'])->where('status',1)->get();
        $logapproved = $this->logapprovedRepo->getByProgress(3,$input['project_id']);
        if(count($check) == 0){
            $check1 = $this->logapprovedRepo->checkProgress(6,$input['project_id']);
            if(count($check1) == 0){
                $log['member_id']= $logapproved->member_id;
                $log['project_id'] = $input['project_id'];
                $log['status'] = 1;
                $log['progress'] = 6;
                $this->logapprovedRepo->create($log);
            }else{
                $this->logapprovedRepo->updateByProgress(6,$input['project_id'],['status'=>1]);
            }
        }
        $this->memberRepo->NotificateMember($logapproved->member_id,['content' => 'Đã gửi báo cáo kết quả đề án ', 'link' => '/project/view/' . $input['project_id']]);  
        return response()->json(array('success'=>true));
    }
    public function unapproved(Request $request){
        $input = Purify::clean($request->all());
        $check = $this->logapprovedRepo->checkProgress(6,$input['project_id']);
        if(count($check) > 0){
            $this->logapprovedRepo->updateByProgress(6,$input['project_id'],['status'=>0,'reason'=>$input['reason']]);
        }else{
            $log['reason'] = $input['reason'];
            $log['member_id'] = \Auth::guard('member')->user()->id;
            $log['project_id'] = $input['project_id'];
            $log['progress'] = 6;
            $log['status'] = 0;
            $logapproved = $this->logapprovedRepo->create($log);
        }
        $project = $this->projectRepo->find($input['project_id']);
        $project->memberproject()->update(['status'=>0]);
        $member_ids = $project->memberproject()->pluck('member_id')->toArray();
        $this->memberRepo->NotificateMembers($member_ids,['content' => 'Kết quả đề án bị trả về ', 'link' => '/project/view/' . $input['project_id']]);  
        return response()->json(array('success'=>true));
    }
    public function changeMemberApproved(Request $request){
        $input = $request->all();
        \App\LogApproved::where('project_id',$input['project_id'])->where('progress',3)->update(['member_id'=>$input['member_approved_id']]);
        $this->memberRepo->NotificateMember($input['member_approved_id'], ['content' => 'Đã chuyển quyền duyệt đề án cho bạn', 'link' => '/project/view/' . $input['project_id']]);
        return response()->json(array('success'=>true));
    }
    public function complete(Request $request){
        $check = $this->logapprovedRepo->checkProgress(6,$request->get('project_id'));
        if(count($check) > 0){
            $this->logapprovedRepo->updateByProgress(6,$request->get('project_id'),['status'=>2,'comment'=>$request->get('comment')]);
        }else{
            $log['member_id'] = \Auth::guard('member')->user()->id;
            $log['project_id'] = $request->get('project_id');
            $log['progress'] = 6;
            $log['status'] = 2;
            $log['comment'] = $request->get('comment');
            $logapproved = $this->logapprovedRepo->create($log);
        }
        $project = $this->projectRepo->find($request->get('project_id'));
        $member_ids = $project->memberproject()->pluck('member_id')->toArray();
        $this->memberRepo->NotificateMembers($member_ids,['content' => 'Kết quả đề án đã được duyệt', 'link' => '/project/view/' . $request->get('project_id')]);  
        return response()->json(array('success'=>true));
    }
}
