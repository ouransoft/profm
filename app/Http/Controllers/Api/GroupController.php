<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\GroupRepository;
use App\Repositories\MemberRepository;
use Stevebauman\Purify\Facades\Purify;

class GroupController extends Controller
{
    public function __construct(GroupRepository $groupRepo,MemberRepository $memberRepo) {
        $this->groupRepo = $groupRepo;
        $this->memberRepo = $memberRepo;
    }
    public function store(Request $request){
        $input = Purify::clean($request->all());
        $validator = \Validator::make($input, $this->groupRepo->validateCreate());
        if ($validator->fails()) {
            return response()->json(['error' => true]);
        }
        $group = $this->groupRepo->create($input);
        $input['member_id'][]=\Auth::guard('member')->user()->id;
        $group->member()->attach($input['member_id']);
        return response()->json(['error' => false]);
    }
    public function getMessage(Request $request){
        $input = Purify::clean($request->all());
        $my_id = $input['from'];
        $recever_id = $input['to'];
        $messages = \App\GroupMessage::where('group_id',$recever_id)->orderBy('created_at','ASC')->get();
        $group = $this->groupRepo->find($recever_id);
        $html = '<div class="chat-box show">
            <div class="chat-head">
                <h6>'.$group->name.'</h6>
                <div class="more">
                    <span class="close-mesage"><i class="fa fa-times"></i></span>
                </div>
            </div>
            <div class="chat-list">
                <ul class="ps-container ps-theme-default ps-active-y">';
        foreach($messages as $key=>$val){
            if($val->from == $my_id){
                $html .='<li class="me">
                            <div class="chat-thumb"><img src="/assets2/img/img_avatar.png" alt="" class="avatar-img"></div>
                            <div class="notification-event">
                                <span class="chat-message-item">
                                    '.$val->message.'
                                </span>
                                <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">'.date('d/m, h:i a',strtotime($val->created_at)).'</time></span>
                            </div>
                        </li>';
            }else{
                $member = $this->memberRepo->find($val->from);
                $html .='<li class="you">
                            <div class="chat-thumb"><img src="/assets2/img/img_avatar.png" alt="" class="avatar-img"></div>
                            <div class="notification-event">
                                <span class="notification-date">'.$member->full_name.'</span>
                                <span class="chat-message-item">
                                    '.$val->message.'
                                </span>
                                <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">'.date('d/m , h:i a',strtotime($val->created_at)).'</time></span>
                            </div>
                        </li>';
            }
        }
        $html .='<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; height: 290px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 215px;"></div></div>
                </ul>
                <div class="text-box">
                    <input type="hidden" name="receiver_id" value="'.$recever_id.'" id="receiver_id" />
                    <input type="hidden" name="my_id" value="'.$my_id.'" id="my_id" />
                    <input type="hidden" name="type" value="2" id="type_message" />
                    <label class="custom-file-upload">
                        <input type="file" class="upload-file"/>
                        <i class="fa fa-cloud-upload-alt"></i>
                    </label>
                    <textarea placeholder="Tin nhắn..." name="message" id="content_message"></textarea>
                </div>
            </div>
        </div>
    </div>';
        return response()->json(['error' => false,'html'=>$html]);
    }
}
