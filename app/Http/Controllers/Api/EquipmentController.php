<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\EquipmentRepository;
use Stevebauman\Purify\Facades\Purify;

class EquipmentController extends Controller
{
    public function __construct(EquipmentRepository $equipmentRepo) {
        $this->equipmentRepo= $equipmentRepo;
    }
    public function getList(Request $request){
        $equipments = \App\Equipment::where('id',$request->gid)->get();
        $list = new \stdClass;
        foreach($equipments as $key=>$val){
            $object1 = new \stdClass;
            $object1->approval = 0;
            $object1->checkrepeat = 1;
            $object1->code = $val->name;
            $object1->displayName = $val->name;
            $object1->forignKey = $val->name;
            $object1->id = $val->id;
            $object1->name = $val->name;
            $object1->type = "facility";
            $list->$key = $object1;
        }
        $object = new \stdClass;
        $object->list = $list;
        $object->offset = count((array)$list);
        $object->total = count((array)$list);
        return response()->json($object);
    }
}
