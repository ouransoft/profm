<?php

namespace App\Http\Controllers\Api;

use App\Helpers\StringHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use \Pusher\Pusher;
use Repositories\GroupRepository;
use Symfony\Component\Console\Helper\Helper;
use Stevebauman\Purify\Facades\Purify;
use Repositories\FileRepository;

class MemberController extends Controller
{
    public function __construct(MemberRepository $memberRepo,GroupRepository $groupRepo,FileRepository $fileRepo) {
        $this->memberRepo = $memberRepo;
        $this->groupRepo = $groupRepo;
        $this->fileRepo = $fileRepo;
    }
    public function seenNotification(Request $request){
        \App\Notification::where('id',$request->id)
            ->update(['read_at'=>date('Y-m-d H:i:s')]);
        return response()->json(['error' => false]);
    }
    public function seenNotificationAll(){
        \Auth::guard('member')->user()->unreadNotifications->markAsRead();
        return response()->json(['error' => false]);
    }
    public function editList(Request $request) {
        $member_arr = $request->get('member_id');
        if($member_arr == 'all'){
            $member_id = $this->memberRepo->getAll()->pluck('id')->toArray();
        }else{
            $member_id = explode(',',$member_arr);
        }
        Session::put('member_id', $member_id);
        $member = $this->memberRepo->find(session('member_id')[0]);
        $department_html = \App\Helpers\StringHelper::getSelectDepartmentChildren(0,$member->department_id);
        $position_html = \App\Helpers\StringHelper::getSelectHtml(\App\Position::get(),$member->position_id);
        $id = $member->id;
        $level_html='';
        $level_name = ['Nhân viên (Nộp đề án)','Tổ trưởng (Duyệt lần 1)','Trưởng phòng (Duyệt lần 2)','Thư ký','Admin'];
        for($i=1;$i<6;$i++){
            if($member->level == $i){
                $level_html .='<option value="'.$i.'" selected>'.$level_name[$i-1].'</option>';
            }else{
                $level_html .='<option value="'.$i.'">'.$level_name[$i-1].'</option>';
            }          
        }
        if(count(session('member_id')) > 1){
            $next_id = session('member_id')[1];
        }else{
            $next_id = null;
        }
        return response()->json(array('department_html'=>$department_html,'position_html'=>$position_html,
            'login_id'=>$member->login_id,'email'=>$member->email,'full_name'=>$member->full_name,'note'=>$member->note,'id'=>$id,'next_id'=>$next_id,'avatar'=>$member->avatar,'level_html'=>$level_html));

    }
    public function editDetail(Request $request) {
        $member_id = $request->get('member_id');
        $member = $this->memberRepo->find($member_id);
        $department_html = \App\Helpers\StringHelper::getSelectDepartmentChildren(0,$member->department_id);
        dd($department_html);
        $position_html = \App\Helpers\StringHelper::getSelectHtml(\App\Position::get(),$member->position_id);
        $level_html='';
        $level_name = ['Nhân viên (Nộp đề án)','Tổ trưởng (Duyệt lần 1)','Trưởng phòng (Duyệt lần 2)','Thư ký','Admin'];
        for($i=1;$i<6;$i++){
            if($member->level == $i){
                $level_html .='<option value="'.$i.'" selected>'.$level_name[$i-1].'</option>';
            }else{
                $level_html .='<option value="'.$i.'">'.$level_name[$i-1].'</option>';
            }
                    
        }
        $index = array_search($member_id, Session::get('member_id'));
        if($index !== false && $index > 0 ) {
            $prev = session('member_id')[$index-1];
        }else{
            $prev = null;
        }
        if($index !== false && $index < count(Session::get('member_id'))-1){
            $next = session('member_id')[$index+1];
        }else{
            $next = null;
        }
        return response()->json(array('department_html'=>$department_html,'position_html'=>$position_html,
            'login_id'=>$member->login_id,'email'=>$member->email,'full_name'=>$member->full_name,'note'=>$member->note,'id'=>$member_id,'prev'=>$prev,'next'=>$next,'avatar'=>$member->avatar,'level_html'=>$level_html));

    }
    public function update(Request $request) {
        $input = Purify::clean($request->all());
        if($input['password'] == ''){
            unset($input['password']);
        }else{
            $input['password'] = bcrypt($input['password']);
        }
        $this->memberRepo->update($input, $input['id']);
        return response()->json(array('success' => true));
    }
     public function destroy(Request $request) {
        $member_arr = $request->get('member_id');
        if($member_arr == 'all'){
            $member_id = $this->memberRepo->getAll()->pluck('id')->toArray();
        }else{
            $member_id = explode(',',$member_arr);
        }
        $this->memberRepo->remove($member_id);
        return response()->json(array('success'=>true));
    }
    public function listMember(){
        Session::put('page',1);
        $records = $this->memberRepo->getIndex(10);
        $html = $this->getHtml($records);
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
    public function listMemberRemove(){
        $records = $this->memberRepo->getIndexRemove(10);
        $html = $this->getHtml($records);
        $start = ((Session::get('page')-1) * 10) + 1;
        $show_page ='<p id="page_member">'.$start.'-'.Session::get('_pages').' trong số '.Session::get('_count').'</p>';
        if(session('page') > 1){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="forward-page-member"><img src="public/assets2/img/left-arrow.png"></a>';
        }
        if(session('_count') > session('_pages')){
            $show_page .= '<a style="padding-top:15px" href="javascript:void(0)" class="next-page-member"><img src="public/assets2/img/forward.png"></a>';
        }
        return response()->json(array('html'=>$html,'show_page'=>$show_page));
    }
    public function restoreMember(Request $request){
        $member_arr = $request->get('member_id');
        if($member_arr == 'all'){
            $member_id = $this->memberRepo->getAllRemove()->pluck('id')->toArray();
        }else{
            $member_id = explode(',',$member_arr);
        }
        $this->memberRepo->restore($member_id);
        return response()->json(array('success'=>true));
    }
    public function checkPassword(Request $request){
        $password = $request->get('password');
        if(Hash::check($password, \Auth::guard('member')->user()->password)) {
            return response()->json(array('success'=>'true'));
        }else{
            return response()->json(array('success'=>'false'));
        }
    }
    public function resetPassword(Request $request){
        $password = $request->get('password');
        $input['password'] = bcrypt($password);
        $this->memberRepo->update($input,\Auth::guard('member')->user()->id);
        return response()->json(array('success'=>'true'));
    }
    public function getInfo(Request $request){
        $member_id = $request->get('member_id');
        if($member_id){
            $member = $this->memberRepo->find($member_id);
        }else{
            $member = \Auth::guard('member')->user();
        }
        $member->position_name = $member->position ? $member->position->name : '';
        return response()->json(array('member'=>$member));
    }
    public function getInfoMobile(Request $request){
        $member_id = $request->get('member_id');
        if($member_id){
            $member = $this->memberRepo->find($member_id);
        }else{
            $member = \Auth::guard('member')->user();
        }
        $html='<div class="col-5" style="padding-left:0px;">
                    <div class="img-member">
                        <img src="'.$member->avatar.'">
                    </div>
                </div>
                <div class="col-7">
                    <h3>'.$member->full_name.'</h3>
                    <p>Mã nhân viên: <span>'.$member->login_id.'</span></p>
                    <p>Chức vụ: <span>'.($member->position ? $member->position->name : '').'</span></p>
                </div>';
        return response()->json(array('html'=>$html));
    }
    public function import(Request $request)
    {
        if ($_FILES['files']) {
            // get file extension
            $extension = pathinfo($_FILES['files']['name'], PATHINFO_EXTENSION);
            if ($extension == 'csv') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } elseif ($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }
            // file path
            $spreadsheet = $reader->load($_FILES['files']['tmp_name']);
            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            $member_ID=[];
            foreach ($allDataInSheet as $key => $row) {
                //Lấy dứ liệu từ dòng thứ 4
                if ($key > 1 && !is_null($row['B']) && !is_null($row['E'])) {
                    $input['department_id'] = ($row['A'] == NULL || is_null(\App\Department::where('name',$row['A'])->first())) ? NULL : \App\Department::where('name',$row['A'])->first()->id;
                    $input['position_id'] = ($row['B'] == NULL || is_null(\App\Position::where('name',$row['B'])->first())) ? NULL : \App\Position::where('name',$row['B'])->first()->id;
                    $input['login_id'] = $row['C'];
                    $input['level'] = $row['D'];
                    $input['full_name'] = $row['E'];
                    $input['password'] = bcrypt(123456);
                    $input['avatar']='/public/img/avatar.png';
                    $check = $this->memberRepo->checkID($input['login_id']);
                    if(!$check){
                        $this->memberRepo->create($input);
                        unset($input);
                    }else{
                        $member_ID[] = $input['login_id'];
                    }
                }
            }
        }
        if(count($member_ID) > 0){
            $message = 'ID login đã tồn tại: ';
            foreach($member_ID as $key=>$val){
                $message .= '<p>'.$val.'</p>';
            }
            return response()->json(array('success'=>'false','message'=>$message));
        }else{
            return response()->json(array('success'=>'true'));
        }
    }
    public function export(Request $request)
    {
        $member_arr = $request->get('member_id');
        if($member_arr == 'all'){
            $ids = $this->memberRepo->getAll()->pluck('id')->toArray();
        }else{
            $ids = explode(',',$member_arr);
        }
        $records = $this->memberRepo->export($ids);
        $a = Carbon::now();

        $lastQuarter = $a->quarter;
        foreach($records as $key=>$record){
            $record->project_month = \App\Project::where('member_id',$record->id)->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->whereDate('approved_at','<',date('Y-m-d'))->whereDate('approved_at','>',date('Y-m-d',strtotime("-1 months")))->count();
            $record->project_quarter = \App\Project::where('member_id',$record->id)->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->where(DB::raw('QUARTER(approved_at)'), $lastQuarter)->whereYear('approved_at',date('Y'))->count();
            $record->total = \App\Project::where('member_id',$record->id)->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->count();
        }
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'STT')->mergeCells("A1:A2")
            ->setCellValue('B1', 'Bộ phận')->mergeCells("B1:B2")
            ->setCellValue('C1', 'Phòng')->mergeCells("C1:C2")
            ->setCellValue('D1', 'Tổ nhóm')->mergeCells("D1:D2")
            ->setCellValue('E1', 'Team')->mergeCells("E1:E2")
            ->setCellValue('F1', 'ID')->mergeCells("F1:F2")
            ->setCellValue('G1', 'Họ tên')->mergeCells("G1:G2")
            ->setCellValue('H1', 'Chức vụ')->mergeCells("H1:H2")
            ->setCellValue('I1', 'Level')->mergeCells("I1:I2")
            ->setCellValue('J1', 'Thông tin đề án')->mergeCells("J1:L1")
            ->setCellValue('J2', 'Tháng hiện tại')
            ->setCellValue('K2', 'Quý hiện tại')
            ->setCellValue('L2', 'Tổng số đề án');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A1:L2" )->applyFromArray($styleArray);
        $bold_range = ['A1:A2', 'B1:B2', 'C1:C2', 'D1:D2','E1:E2','F1:F2','G1:G2','H1:H2','I1:I2','J1:L1','J2','K2','L2'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $spreadsheet->getActiveSheet()->getStyle('D2:H2')->getFont()->setSize(8);
        $rows = 2;
        $no = 1;
        foreach ($records as $key=>$record)
        {
            $rows++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, $key + 1)
                ->setCellValue('B' . $rows, $record->part ? $record->part->name : '')
                ->setCellValue('C' . $rows, $record->department ? $record->department->name : '')
                ->setCellValue('D' . $rows, $record->groups ? $record->groups->name : '')
                ->setCellValue('E' . $rows, $record->team ? $record->team->name : '')
                ->setCellValue('F' . $rows, $record->login_id)
                ->setCellValue('G' . $rows, $record->full_name)
                ->setCellValue('H' . $rows, $record->position ? $record->position->name : '')
                ->setCellValue('I' . $rows, $record->level)
                ->setCellValue('J' . $rows, $record->project_month)
                ->setCellValue('K' . $rows, $record->project_quarter)
                ->setCellValue('L' . $rows, $record->total);
            //$rows++;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."L" . $rows . "")->applyFromArray($styleArray);
        }
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Bao_cao_luong_lai_xe_ngay_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/Thanh_vien_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/Thanh_vien_' . $timestamp . '.xls';
        return response()->json(['success' => 'false', 'href' => $href]);
    }
    public function getAllMessage(Request $request){
        $user_id = $request->get('id');
        $groups = \App\GroupMember::where('member_id',$user_id)->pluck('group_id')->toArray();
        $all_messages_record = \App\GroupMessage::whereIn('group_id',$groups)->orderBy('created_at','desc')->pluck('group_id')->toArray(); // lấy ra tất cả id của tin nhắn trong các nhóm tìm được
        $all_messages_record = array_unique($all_messages_record); // lọc id trùng lặp - lấy ra id duy nhất
        $all_messages = array();
        foreach($all_messages_record as $key => $record){
            if(\App\Group::where('id',$record)->first()->type == 99){
                $all_messages[$key]['avatar'] = '\assets2\img\gear.png';
                $all_messages[$key]['name'] = 'Tin nhắn hệ thống';
            }else if(\App\Group::where('id',$record)->first()->name != null){
                $all_messages[$key]['avatar'] = \App\Group::where('id',$record)->first()->avatar;
                $all_messages[$key]['name'] = \App\Group::where('id',$record)->first()->name;
            }else{
                $receiver = \App\GroupMember::where('group_id',$record)->where('member_id','!=',$user_id)->first();
                $all_messages[$key]['avatar'] = \App\Member::where('id',$receiver->member_id)->first()->avatar;
                $all_messages[$key]['name'] = $receiver->name;
            }
            $all_messages[$key]['message'] = \App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->message;
            $all_messages[$key]['time'] = \App\Helpers\StringHelper::time_ago(\App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->created_at);
            $all_messages[$key]['id'] = $record;
            $all_messages[$key]['type'] = \App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->type;
            $seen = \App\ReadMessage::where('message_id',\App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->id)
                                            ->where('member_id',\Auth::guard('member')->user()->id)
                                            ->where('group_id',$record)
                                            ->get();
            if(count($seen)>0){
                $all_messages[$key]['seen'] = $seen[0]->seen;
            }else{
                $all_messages[$key]['seen'] = 1;
            }
        }
        return response()->json(['error' => false,'messages'=>$all_messages]);
    }
    public function getMessenger(){
        $id = \Auth::guard('member')->user()->id;
        $member_message = \DB::table('member')->leftjoin('messages','member.id','=','messages.to')->select('member.id','member.full_name')->where('member.id','<>',$id)->where('member.id','<>',1)
                        ->groupBy('member.id','member.full_name')->get();
        foreach($member_message as $key=>$val){
            $my_id = $id;
            $recever_id = $val->id;
            $message = \App\Message::where(function ($query) use ($my_id, $recever_id) {
                                                $query->where('from', $recever_id)->where('to', $my_id);
                                            })->orWhere(function ($query) use ($recever_id, $my_id) {
                                                $query->where('from', $my_id)->where('to', $recever_id);
                                            })->orderBy('created_at','DESC')->first();
            $member_message[$key]->message = $message ? $message->message :'';
            $member_message[$key]->created = $message ? date('d/m, h:i a',strtotime($message->created_at)) :'';
            $member_message[$key]->created_at = $message ? $message->created_at :'';
            $member_message[$key]->is_read = ( $message && $message->is_read == 0 && $message->to == $my_id) ? 0 : 1 ;
            $member_message[$key]->from = \App\Member::find($val->id) ? \App\Member::find($val->id)->id : null;
        }
        $member_message = collect($member_message)->sortBy('created_at')->reverse();
        $html='';
        foreach( $member_message as $val){
            $image = !is_null(\App\Member::find($val->from)) ? (!is_null(\App\Member::find($val->from)->avatar) ? \App\Member::find($val->from)->avatar : '/assets2/img/img_avatar.png' ) : '/assets2/img/img_avatar.png';
            if($val->is_read == 0){
                $seen='active-message';
            }else{
                $seen='';
            }
            if(strpos($val->message, '</a>') == true){
                $val->message='Đã gửi 1 file';
            };
            $html .= ' <li class="'.$seen.'">
                            <a href="'.route('frontend.messenger.detail',$val->id).'" class="item">
                                <img src="'.$image.'" alt="image" class="image">
                                <div class="in">
                                    <div>
                                        '.$val->full_name.'
                                        <footer>'.mb_convert_encoding(substr($val->message,0,40), 'UTF-8', 'UTF-8').'</footer>
                                    </div>
                                    <span class="text-muted">'.$val->created.'</span>
                                </div>
                            </a>
                       </li>';
        }
        return response()->json(['error' => false,'html'=>$html]);
    }
    public function getMessage(Request $request){
        $input = $request->all();
        $all_messages = \App\GroupMessage::where('group_id',$input['link'])->get();
        $messages = array();
        $group_type = 1;
        // dd($all_messages);
        foreach($all_messages as $key => $message){
            if($message->from == 0){
                $messages[$key]['from'] = "";
                $messages[$key]['avatar'] = "";
            }else{
                $group_member = \App\GroupMember::where('member_id',$message->from)->where('group_id',$input['link'])->first();
                $messages[$key]['from'] = $group_member ? $group_member->name : '';
                $messages[$key]['avatar'] = \App\Member::where('id',$message->from)->first()->avatar;
            }
            if($message->reply_id > 0){
                $reply = \App\GroupMessage::find($message->reply_id);
                $messages[$key]['reply_message'] = $reply->message;
                $messages[$key]['reply_name'] = $reply->membersend->full_name;
            }
            $messages[$key]['reply_id'] = $message->reply_id;
            $messages[$key]['id'] = $message->from;
            $messages[$key]['message_id'] = $message->id;
            $messages[$key]['file_name'] = $message->file_name;
            $messages[$key]['message'] = $message->message;
            $messages[$key]['time'] = date('h:i A',strtotime($message->created_at));
            $messages[$key]['date'] = date('d-m-Y',strtotime($message->created_at));
            $messages[$key]['type'] = $message->type;
            $messages[$key]['seen'] = 1;
            $member_in_messages = \App\ReadMessage::where('group_id',$input['link'])->where('message_id',$message->id)->where('member_id','!=',\Auth::guard('member')->user()->id)->where('seen',1)->get();
            $messages[$key]['member_seen'] = array();
            foreach($member_in_messages as $member_in_message){
                $messages[$key]['member_seen'][] = [
                    'avatar' => \App\Member::where('id',$member_in_message->member_id)->first()->avatar,
                    'name' => \App\Member::where('id',$member_in_message->member_id)->first()->full_name
                ];
            }
        }
        if($input['type'] == 1){
            $seen = \App\ReadMessage::where('message_id',\App\GroupMessage::where('group_id',$message->group_id)->orderBy('created_at', 'desc')->first()->id)
                                        ->where('member_id',\Auth::guard('member')->user()->id)
                                        ->where('group_id',$message->group_id)
                                        ->where('seen',1)
                                        ->get();
            $unread = \App\ReadMessage::where('message_id',\App\GroupMessage::where('group_id',$message->group_id)->orderBy('created_at', 'desc')->first()->id)
                                        ->where('member_id',\Auth::guard('member')->user()->id)
                                        ->where('group_id',$message->group_id)
                                        ->where('seen',0)
                                        ->get();
            $length = count($messages);
            if(count($unread)>0){
                $group_unread = \App\Group::where('id',$message->group_id)->first();
                $group_unread->readMessage()->detach(\Auth::guard('member')->user()->id);
                if(count($seen)>0){
                    $group_seen = \App\Group::where('id',$message->group_id)->first();
                    $group_seen->readMessage()->async([\Auth::guard('member')->user()->id => ['message_id'=>$all_messages[$length-1]->id, 'seen'=>1]]);
                }else{
                    $group_seen = \App\Group::where('id',$message->group_id)->first();
                    $group_seen->readMessage()->attach(\Auth::guard('member')->user()->id,['message_id'=>$all_messages[$length-1]->id, 'seen'=>1]);
                }
            }
        }
        $group_id = $input['link'];
        $to_member = \App\GroupMember::where('group_id',$input['link'])->where('member_id','!=',\Auth::guard('member')->user()->id)->first();
        $to_type = 0; // Group
        $to_member_id = 0;
        $avatar = "";
        $to_member_phone = "";
        $to_member_email = "";
        $to_member_address = "";
        if($input['type'] == 0){
            $group_name = \App\Member::where('id',$input['link'])->first()->full_name;
            $to_member_id = $input['link'];
            $avatar = \App\Member::where('id',$input['link'])->first()->avatar;
            $to_member_phone = \App\Member::where('id',$to_member_id)->first()->phone;
            $to_member_email = \App\Member::where('id',$to_member_id)->first()->email;
            $to_member_address = \App\Member::where('id',$to_member_id)->first()->address;
             $members = [];
        }else{
            $group = \App\Group::find($group_id);
            $members = $group->member;
            foreach($members as $key=>$member){
                $member->position_name = $member->position ? $member->position->name : '';
            }
            $group_type = \App\Group::where('id',$input['link'])->first()->type;
            if($group_type == 99){
                $group_name = "Tin nhắn hệ thống";
                $to_type = 0;
                $avatar = "\assets2\img\gear.png";
            }else{
                $group_name = \App\Group::where('id',$input['link'])->first()->name; // lấy tên của group
                if($group_name == ""){ // Nếu rỗng thì lấy group_name theo name trong GroupMember
                    $group_name = $to_member->name;
                    $to_type = 1; // Member
                    $to_member_id = $to_member->member_id;
                }
                $avatar = \App\Group::where('id',$input['link'])->first()->avatar;
                if($avatar == null){
                    $avatar = \App\Member::where('id',$to_member_id)->first()->avatar;
                    $to_member_phone = \App\Member::where('id',$to_member_id)->first()->phone;
                    $to_member_email = \App\Member::where('id',$to_member_id)->first()->email;
                    $to_member_address = \App\Member::where('id',$to_member_id)->first()->address;
                }
            }
        }
        $member_name = \App\Member::where('id',\Auth::guard('member')->user()->id)->first()->full_name;
        $count_message = \App\ReadMessage::where('member_id',\Auth::guard('member')->user()->id)->where('seen',0)->count();
        return response()->json(['count_message'=>$count_message,'error' => false,'members'=>$members,'html'=>$messages,'member_name'=>$member_name,'group_id'=>$group_id,'group_type'=>$group_type,'group_name'=>$group_name,'type'=>$input['type'],'avatar'=>$avatar,'to_type'=>$to_type,'to_member'=>$to_member_id,'to_member_phone'=>$to_member_phone,'to_member_email'=>$to_member_email,'to_member_address'=>$to_member_address]);
    }
    public function sendMessage(Request $request)
    {
        $config = ['HTML.Allowed' => 'div,b,a[href],img[src]'];
        $input = Purify::clean($request->all(),$config);
        $group_id = \App\Group::where('id',$input['receiver_id'])->first();
        $data = new \App\GroupMessage();
        $message = "";
        $files = "";
        $file_name = "";
        $size = 0;
        $ext = "";
        $group_id = $input['receiver_id'];
        if($input['type'] == 0){
            $member_name = \App\Member::where('id',\Auth::guard('member')->user()->id)->first()->full_name;
            $reveiver_name = \App\Member::where('id',$input['receiver_id'])->first()->full_name;
            $group = new \App\Group();
            $group->name = "";
            $group->save();
            $group->member()->attach($input['receiver_id']);
            $group->member()->sync([\Auth::guard('member')->user()->id => ['name' => $member_name], $input['receiver_id'] => ['name' => $reveiver_name]]);
            $group_id = $group->id;
        }
        if($input['type_file'] == 'image'){
            $data->type = 2;
            $destinationPath = 'chat/message/images';
            $image = $request['image'];
            $files = $destinationPath . '/' . $image->getClientOriginalName();
            $file_name = time()."_".$image->getClientOriginalName();
            $image->move($destinationPath, $file_name);
            $message = $destinationPath . '/' . $file_name;
        }else if($input['type_file'] == 'file'){
            $data->type = 3;
            $destinationPath = 'chat/message/files';
            $message = array();
            $file = $request['file'];
            $files = $destinationPath . '/' . $file->getClientOriginalName();
            $size = $file->getClientSize();
            $file_name = $file->getClientOriginalName();
            if ($size > 0) {
                $size = (int) $size;
                $base = log($size) / log(1024);
                $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');
                $size =  round(pow(1024, $base - floor($base)), 2) . $suffixes[floor($base)];
            }
            $link_file_name = time().'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $link_file_name);
            $data->file_name = $file_name;
            $data->size = $size;
            $data->extension = pathinfo($files, PATHINFO_EXTENSION);
            $message = $destinationPath . '/' . $link_file_name;
        }else{
            $message = $input['message'];
        }
        $member = $this->memberRepo->find($input['my_id']);
        $avatar = $member->avatar;
        $data->from = $input['my_id'];
        $data->group_id = $group_id;
        if(isset($input['reply_id'])){
            $data->reply_id = $input['reply_id'];
        }else{
            $data->reply_id = 0;
        }
        $data->message = $message;
        $data->save();
        // chỉnh sửa bảng read_message
        $member_receives = \App\GroupMember::where('group_id',$group_id)->where('member_id','!=',\Auth::guard('member')->user()->id)->get();
        $group = \App\Group::find($group_id);
        if($input['type'] == 0){
            $group->readMessage()->attach($member_receives[0]->member_id, ['message_id'=>$data->id,'seen'=>0]);
        }else{
            foreach($member_receives as $member_receive){
                $unread_message = \App\ReadMessage::where('member_id',$member_receive->member_id)
                                                    ->where('group_id',$group_id)
                                                    ->where('seen',0)
                                                    ->get();
                if(count($unread_message)>0){
                    $group->readMessage()->sync([$member_receive->member_id => ['message_id'=>$data->id,'seen'=>0]]);
                }else{
                    $group->readMessage()->attach($member_receive->member_id, ['message_id'=>$data->id,'seen'=>0]);
                }
            }
        }
        //
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
        );
        $from_name = \App\Member::where('id',$data->from)->first()->full_name;
        $member_ids = $group->member()->pluck('id')->toArray();
        if(isset($input['reply_id']) && $input['reply_id'] > 0){
            $reply = \App\GroupMessage::find($input['reply_id']);
            $reply_message = $reply->message;
            $reply_name = $reply->membersend->full_name;
        }else{
            $reply_message='';
            $reply_name='';
        }
        $data = ['reply_id'=>$data->reply_id,'reply_message'=>$reply_message,'reply_name'=>$reply_name,'member_ids'=>json_encode($member_ids),'avatar'=>$avatar,'from' => $data->from,'from_name'=>$from_name,'time'=>date('h:i A'),'date'=>'time','date'=>date('d-m'),'group_id' => $group_id,'message'=>$data->message,'message_id'=>$data->id,'file_name'=>$file_name,'size'=>$data->size,'ext'=>$data->extension,'type'=>$data->type];
        $pusher->trigger('chat-message', 'send-message', $data);
        return response()->json(['success'=>'true']);
    }
    public function updateInfo(Request $request){
        $input = $request->all();
        if(\Auth::guard('member')->user()->file() && \Auth::guard('member')->user()->file()->id != $input['file_id']){
            $this->fileRepo->deleteByMember(\Auth::guard('member')->user()->id);
        }
        if($input['file_id'] && (!\Auth::guard('member')->user()->file() || $input['file_id'] != \Auth::guard('member')->user()->file()->id)){
            $this->fileRepo->update(['relationship_id'=>\Auth::guard('member')->user()->id],$input['file_id']);
        }
        $this->memberRepo->update($input,\Auth::guard('member')->user()->id);
        return response()->json(['success'=>'true']);
    }
    public function checkID(Request $request){
        $ID = $request->get('id');
        $check = $this->memberRepo->checkID($ID);
        if($check){
            return response()->json(['success'=>'true']);
        }else{
            return response()->json(['success'=>'false']);
        }
    }
    public function seachMemberByKeyword(Request $request){
        $input = Purify::clean($request->all());
        $members = $this->memberRepo->searchByKeyword($input['keyword']);
        $members_info = new \stdClass();
        foreach($members as $key=>$val){
            $object1 = new \stdClass();
            $object1->displayName = $val->full_name;
            $object1->foreignKey = $val->full_name;
            $object1->id = $val->id;
            $object1->type = 'user';
            $object1->isInvalidUser = false;
            $object1->isLoginUser = false;
            $object1->isNotUsingApp = false;
            $members_info->$key = $object1;
        }
        return response()->json(array('members_info'=>$members_info));
    }
    public function searchMemberByName(Request $request){
        $input = Purify::clean($request->all());
        if(isset($input['uids'])){
            $members = $this->memberRepo->whereArray($input['uids'])->get();
        }else{
            $members = $this->memberRepo->searchMember($request->get('p'));
        }
        $user = new \stdClass();
        foreach($members as $key=>$val){
            $user1 = new \stdClass();
            $user1->_id = $val->id;
            $user1->col_display_name = $val->full_name;
            $user1->col_display_name_language = 5;
            $user1->col_foreign_key = $val->full_name;
            $user1->col_nickname = "";
            $user1->col_normalized_sort_key = "";
            $user1->col_valid = null;
            $user1->icon_path = "";
            $user1->logged_in = false;
            $user1->url = "";
            $user->$key = $user1;
        }
        $facility = new \stdClass();
        $org = new \stdClass();
        $data = new \stdClass();
        $data->user = $user;
        $data->facility = $facility;
        $data->org = $org;
        return response()->json($data);
    }
    public function getUserForMobile(Request $request){
        if($request->get('gid')){
            $department_id = $request->get('gid');
            $members = $this->memberRepo->getByDepartment($department_id);
        }else{
            $members = $this->memberRepo->searchByKeyword($request->get('keyword'));
        }
        $data = new \stdClass();
        $list = new \stdClass();
        foreach($members as $key=>$val){
            $user1 = new \stdClass();
            $user1->id = "".$val->id."";
            $user1->displayName = $val->full_name;
            $user1->type = "user";
            $user1->foreignKey = $val->full_name;
            $user1->isNotUsingApp = false;
            $user1->isLoginUser = false;
            $user1->image = "";
            $list->$key = $user1;
        }
        $data->list= $list;
        $data->offset= count($members);
        $data->total= count($members);
        return response()->json($data);
    }
    public function searchAttendees(Request $request){
        $members = $this->memberRepo->searchMemberMobile($request->all());
        return response()->json(array('data'=>$members));
    }
    public function updateToken(Request $request){
        $this->memberRepo->update(['device_token'=>$request->get('device_token')],\Auth::guard('member')->user()->id);
        return response()->json(['success'=>'true']);
    }
    public function getListName(Request $request){
        $member = \App\Member::where('full_name','like','%' .$request->get('term'). '%')->get();
        if(count($member) > 0){
            $data = $member->pluck('full_name')->toArray();
        }else{
            $data = [];
        }
        return response()->json($data);
    }
    public function getListHtml(){
        $members = $this->memberRepo->getAllNotMe();
        $html = \App\Helpers\StringHelper::getSelectMemberOptionsMobile($members);
        return response()->json(['html'=>$html]);
    }
}
