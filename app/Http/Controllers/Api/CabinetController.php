<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stevebauman\Purify\Facades\Purify;

class CabinetController extends Controller
{
    public function addFile(Request $request){
        $targetDir = "/public/file/";
        $allowTypesFile = array('pdf', 'docx','doc','xls','jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG','xlsx');
        $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
        $fileName = basename($_FILES['file']['name']);
        $html ='';
        if(strlen($fileName)!=0){
            $fileType = pathinfo($fileName);
            // Check whether file type is valid
            if(in_array($fileType['extension'], $allowTypesFile)){
                move_uploaded_file($_FILES['file']['tmp_name'],'file/'.$fileName);
                $link = $targetDir . $fileName;
                $size = \App\Helpers\StringHelper::ConvertSizeFile($_FILES['file']['size']);
                $name = $fileName;
                $html .='<tr>
                            <td valign="middle">
                               <input type="hidden" name="link[]" value="'.$link .'">
                               <input type="hidden" name="file_name[]" value="'.$name .'">
                               <input type="hidden" name="size[]" value="'.$size .'">
                            </td>
                            <td style="white-space: normal;">'.$name.'</td>
                            <td>'.$size.'</td>
                            <td>
                                <input type="text" style="width:100%;" name="subject[]">
                            </td>
                            <td style="white-space: nowrap;">
                            <select name="max_version[]">
                                <option value="0" selected="">(None)</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="-1">Unlimited</option>
                             </select> 
                             latest versions
                            </td>
                            <td>
                                <textarea id="description[]" name="memo[]" class="autoexpand" wrap="virtual" style="width:100%;" rows="5" onfocus="expandTextarea(this);"></textarea>
                            </td>
                        </tr>';
            }
        }
        return response()->json(array('success' => 'true','html'=>$html));
    }
}
