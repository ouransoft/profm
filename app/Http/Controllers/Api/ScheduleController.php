<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ScheduleRepository;
use Repositories\EquipmentRepository;
use App\Repositories\MemberRepository;
use Repositories\FileRepository;
use Repositories\ToDoRepository;
use Repositories\DepartmentRepository;
use Stevebauman\Purify\Facades\Purify;

class ScheduleController extends Controller
{
     public function __construct(DepartmentRepository $departmentRepo,ToDoRepository $todoRepo,FileRepository $fileRepo,MemberRepository $memberRepo,EquipmentRepository $equipmentRepo,ScheduleRepository $scheduleRepo) {
        $this->scheduleRepo = $scheduleRepo;
        $this->equipmentRepo = $equipmentRepo;
        $this->memberRepo = $memberRepo;
        $this->fileRepo = $fileRepo;
        $this->todoRepo = $todoRepo;
        $this->departmentRepo = $departmentRepo;
    }

    public function getListInfo(Request $request){
        $input = $request->all();
        $member_ids = explode(',',$input['member_id']);
        $member_arr = \App\Member::whereIn('id',$member_ids)->pluck('full_name')->toArray();
        $member_html = '';
        foreach($member_arr as $key=>$val){
            $member_html .='<span class="badge badge-secondary mr10">'.$val.'</span>';
        }
        if($input['equipment_id'] != ''){
            $equipment_ids = explode(',',$input['equipment_id']);
            $equipment_arr = \App\Equipment::whereIn('id',$equipment_ids)->pluck('name')->toArray();
            $equipment_html = '';
            foreach($equipment_arr as $key=>$val){
                $equipment_html .='<span class="badge badge-secondary mr10">'.$val.'</span>';
            }
        }else{
            $equipment_html = '';
        }
        return response()->json(array('member_html'=>$member_html,'equipment_html'=>$equipment_html));
    }
    public function check(Request $request){
        $input = $request->all();
        $member_ids = explode(',',$input['member_id']);
        $member_arr = \App\Member::whereIn('id',$member_ids)->get();
        $member = [];
        $schedule=[];
        foreach($member_arr as $key=>$val){
            $object1 = new \stdClass();
            $object1->id = ++$key;
            $object1->title = $val->full_name;
            $member[] = $object1;
            foreach($val->schedule as $k=>$value){
                $object2 = new \stdClass();
                $object2->start = date('Y-m-d H:i',strtotime($value->start_date));
                $object2->end = date('Y-m-d H:i',strtotime($value->end_date));
                $object2->title = $value->work;
                $object2->resourceId = $key;
                $schedule[] = $object2;
            }
        }
        $day = date('d',strtotime($input['date']));
        $month = date('m',strtotime('-1 months',strtotime($input['date'])));
        $year = date('Y',strtotime($input['date']));
        return response()->json(array('member'=>$member,'schedule'=>$schedule,'day'=>$day,'month'=>$month,'year'=>$year));
    }
    public function checkDuplicate(Request $request){
        $input = $request->all();
        $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_date'] . ' ' . $input['start_time']));
        $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_date'] . ' ' . $input['end_time']));
        $member_ids = explode(',',$input['member_id']);
        $member_arr = \App\Member::whereIn('id',$member_ids)->get();
        $equipment_ids = explode(',',$input['member_id']);
        $equipment_arr = \App\Equipment::whereIn('id',$equipment_ids)->get();
        $schedule = [];
        foreach($member_arr as $key=>$val){
            foreach($val->schedule as $k=>$value){
                if(strtotime($value->start_date) < strtotime($input['end_date']) && strtotime($value->end_date) > strtotime($input['start_date']) ){
                    $object1 = new \stdClass();
                    $object1->start = date('Y-m-d H:i',strtotime($value->start_date));
                    $object1->end = date('Y-m-d H:i',strtotime($value->end_date));
                    $object1->work = $value->work;
                    $object1->name = $val->full_name;
                    $schedule[] = $object1;
                }
            }
        }
        foreach($equipment_arr as $key=>$val){
            foreach($val->schedule as $k=>$value){
                if(strtotime($value->start_date) < strtotime($input['end_date']) && strtotime($value->end_date) > strtotime($input['start_date']) ){
                    $object2 = new \stdClass();
                    $object2->start = date('Y-m-d H:i',strtotime($value->start_date));
                    $object2->end = date('Y-m-d H:i',strtotime($value->end_date));
                    $object2->work = $value->work;
                    $object2->name = $val->name;
                    $schedule[] = $object2;
                }
            }
        }
        if(count($schedule) > 0){
            $html = '';
            foreach($schedule as $key=>$val){
                $html .='<tr>
                            <td>'.$val->name.'</td>
                            <td>'.$val->work.'</td>
                            <td>'.date('d/m/Y H:i',strtotime($val->start)).'</td>
                            <td>'.date('d/m/Y H:i',strtotime($val->end)).'</td>
                         </tr>';
            }
            return response()->json(array('success'=>'true','html'=>$html));
        }else{
            return response()->json(array('success'=>'false'));
        }

    }
    public function getUserSelect(Request $request){
        if($request->get('gid')){
            $department = $this->departmentRepo->find($request->get('gid'));
            $department_ids = $department->getIds();
            $members = $this->memberRepo->getByDepartment($department_ids);
        }else{
            $members = $this->memberRepo->getAll();
        }
        $users_info = new \stdClass();
        foreach($members as $key=>$val){
            $object1 = new \stdClass();
            $object1->displayName = $val->full_name;
            $object1->foreignKey = $val->full_name;
            $object1->id = $val->id;
            $object1->type = 'user';
            $object1->isInvalidUser = false;
            if($val->id == \Auth::guard('member')->user()->id){
                $object1->isLoginUser = true;
            }else{
                $object1->isLoginUser = false;
            }
            $object1->isNotUsingApp = false;
            $users_info->$key = $object1;
        }
        return response()->json(array('users_info'=>$users_info));
    }
    public function uploadedFiles(Request $request){

    }
    public function addFile(Request $request){
        $targetDir = "/schedule/";
        $allowTypesFile = array('pdf', 'docx','doc','xls','jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG','xlsx');
        $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
        $fileName = basename($_FILES['file']['name']);
        if(strlen($fileName)!=0){
            $fileType = pathinfo($fileName);
            // Check whether file type is valid
            if(in_array($fileType['extension'], $allowTypesFile)){
                move_uploaded_file($_FILES['file']['tmp_name'],'schedule/'.$fileName);
                $file['link'] = $targetDir . $fileName;
                $file['size'] = \App\Helpers\StringHelper::ConvertSizeFile($_FILES['file']['size']);
                $file['name'] = $fileName;
                $record = $this->fileRepo->create($file);
            }
        }
        $html ='';
        $html .='<tr>
                    <td>
                        <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="upload_fileids[]" value="'.$record->id.'">
                    </td>
                    <td>'.$record->name.' ('.$record->size.')</td>
                </tr>';
        return response()->json(array('success' => 'true','html'=>$html));
    }
    public function addFileMobile(Request $request){
        $targetDir = "/schedule/";
        $allowTypesFile = array('pdf', 'docx','doc','xls','jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG','xlsx');
        $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
        $fileName = basename($_FILES['file']['name']);
        if(strlen($fileName)!=0){
            $fileType = pathinfo($fileName);
            // Check whether file type is valid
            if(in_array($fileType['extension'], $allowTypesFile)){
                move_uploaded_file($_FILES['file']['tmp_name'],'schedule/'.$fileName);
                $file['link'] = $targetDir . $fileName;
                $file['size'] = \App\Helpers\StringHelper::ConvertSizeFile($_FILES['file']['size']);
                $file['name'] = $fileName;
                $record = $this->fileRepo->create($file);
            }
        }
        $html ='';
        $html .='<div class="custom-control custom-checkbox mb-1">
                    <input type="checkbox" class="custom-control-input" checked="checked" name="upload_fileids[]" id="customCheckb1" value="'.$record->id.'">
                    <label class="custom-control-label" for="customCheckb1">'.$record->name.' ('.$record->size.')</label>
                </div>';
        return response()->json(array('success' => 'true','html'=>$html));
    }

    public function seachFacility(Request $request){
        $input = $request->all();
        $equipments = $this->equipmentRepo->search($input['search_text']);
        $data=[];
        foreach($equipments as $key=>$val){
            $object = new \stdClass();
            $object->ancestors = '';
            $object->approval = '0';
            $object->code = $val->name;
            $object->name = $val->name;
            $object->repeat = 1;
            $data[] = $object;
        }
        return $data;
    }
    public function ajaxEventList(Request $request){
        $date = [];
        $members = [];
        $member_ids=[];
        $events = new \stdClass();
        $object = new \stdClass();
        $object->calendarWeekStart = "0";
        $object->facilityPlacement = "after_subject";
        $object->maxTime = 19;
        $object->minTime = 8;
        //
        $begin_date = $request->get('begin_date');
        if($request->get('number_of_days') == 1){
            $object1 = new \stdClass();
            $object1->text = date('D, d F',strtotime($begin_date));
            $object1->text_full = date('D, F d, Y',strtotime($begin_date));
            $object1->type= "s_date_".strtolower(date('l',strtotime($begin_date)));
            $object1->value = $begin_date;
            $date[] = $object1;
        }else{
            for ($i = 0; $i < 7; $i++) {
                $object1 = new \stdClass();
                $object1->value = date('Y-m-d', strtotime($begin_date." +" . $i . " days"));
                $object1->text = date('D, d F',strtotime($object1->value));
                $object1->text_full = date('D, F d, Y',strtotime($object1->value));
                $object1->type= "s_date_".strtolower(date('l',strtotime($object1->value)));
                $date[]=$object1;
            }

        }
        $object->dates = $date;
        //start list member
        if($request->get('uid') != ''){
            $member = $this->memberRepo->find($request->get('uid'));
            $objec = new \stdClass();
            $objec->id = $member->id;
            $objec->name= $member->full_name;
            $objec->type = "user";
            $objec->url = "#";
            $members[] = $objec;
            $member_ids[]=$member->id;;
        }
        if($request->get('gid') != 'login'){
            if($request->get('gid') == 'selected' && $request->get('members') != 'empty' && $request->get('members')){
                foreach($request->get('members') as $val){
                    $val = json_decode($val);
                    $object2 = new \stdClass();
                    if($val->type == 'user'){
                        $member = $this->memberRepo->find($val->id);
                        $object2->name= $member->full_name;
                        $object2->type = "user";
                    }else{
                         $member = $this->equipmentRepo->find($val->id);
                         $object2->name= $member->name;
                         $object2->type = "facility";
                    }
                    $object2->colorId = 0;
                    $object2->id = $member->id;
                    $object2->selected = $val->selected;
                    $object2->url = "#";
                    $object2->user_status = "active";
                    $members[] = $object2;
                    $member_ids[]=$member->id;;
                }
            }elseif($request->get('eid')){
                    $facility = $this->equipmentRepo->find($request->get('eid'));
                    $object2 = new \stdClass();
                    $object2->id = $facility->id;
                    $object2->name= $facility->name;
                    $object2->type = "facility";
                    $object2->url = "";
                    $members[] = $object2;
            }else{
                $department = $this->departmentRepo->find($request->get('gid'));
                if($department){
                    $department_id = $department->getIds();
                    $list_members = $this->memberRepo->getByDepartment($department_id);
                    foreach($list_members as $member){
                        $object2 = new \stdClass();
                        $object2->colorId = 0;
                        $object2->id = $member->id;
                        $object2->name= $member->full_name;
                        $object2->selected = true;
                        $object2->type = "user";
                        $object2->url = "#";
                        $object2->user_status = "active";
                        $members[] = $object2;
                        $member_ids[]=$member->id;;
                    }
                }
            }
        }elseif($request->get('gid') == 'login'){
            $object2 = new \stdClass();
            $object2->colorId = 0;
            $object2->id = \Auth::guard('member')->user()->id;
            $object2->name= \Auth::guard('member')->user()->full_name;
            $object2->selected = true;
            $object2->type = "user";
            $object2->url = "#";
            $object2->user_status = "active";
            $member_ids[]=\Auth::guard('member')->user()->id;
            $members[] = $object2;
        }
        //end list member
        $events->banner=[];
        $by_date =[];
        $banner = [];
        if($request->get('number_of_days') == 7){
            for ($i = 0; $i < 7; $i++) {
                $object4 = new \stdClass();
                $object4->calendar = null;
                $object4->date  = date('Y-m-d', strtotime($begin_date." +" . $i . " days"));
                $object4->date_type = "s_date_".strtolower(date('l',strtotime($object4->date)));
                //start list event by day
                $schedules = $this->scheduleRepo->getAllByMemberDay($member_ids,$object4->date);
                $todos = $this->todoRepo->getListByMember(\Auth::guard('member')->user()->id,$object4->date);
                $normal1 = [];
                $all_day = [];
                foreach($schedules as $schedule){
                    $normal = new \stdClass();
                    $normal->id = $schedule->id;
                    if(in_array(\Auth::guard('member')->user()->id,$schedule->member()->pluck('id')->toArray()) || $schedule->private == 0){
                        $normal->data = $schedule->title .''. (count($schedule->equipment) > 0 ? ' ['.implode(',',$schedule->equipment()->pluck('name')->toArray()).']' : '');
                        $normal->private = 0;
                    }else{
                        $normal->data = 'Đã có lịch';
                        $normal->private = 1;
                    }
                    $normal->detail = $schedule->title;
                    $normal->end_date = date('Y-m-d H:i:s',strtotime($schedule->end_date));
                    if($schedule->none_time != 1){
                         $normal->end_time = date('Y-m-d',strtotime($schedule->end_date));
                         $normal->showtime = date('h:i A',strtotime($schedule->start_date));
                         $normal->conflict = 'false';
                    }else{
                         unset($normal->private);
                    }
                    $normal->faci_items = '';
                    $normal->facility_name = $schedule->equipment ? implode(',',$schedule->equipment()->pluck('name')->toArray()) : '';
                    $normal->menu = '';
                    $normal->menu_color = '';
                    $normal->start_date = date('Y-m-d H:i:s',strtotime($schedule->start_date));
                    if($schedule->pattern == 1){
                        $normal->type = 'normal';
                    }else{
                        $normal->type = 'share_repeat';
                        if($schedule->type_repeat == 'week' || $schedule->type_repeat == 'weekday'){
                            $normal->type_repeat = 'week';
                        }elseif($schedule->type_repeat == 'day'){
                            $normal->type_repeat = 'day';
                        }else{
                            $normal->type_repeat = 'month';
                        }
                    }
                    $normal->uid = $schedule->uid;
                    if($schedule->none_time == 1){
                         $all_day[] = $normal;
                    }else{
                         $normal1[] = $normal;
                    }

                }
                $list_todos = [];
                foreach($todos as $val){
                    $todo = new \stdClass();
                    $todo->tid = $val->id;
                    $todo->cid = $val->member_id;
                    $todo->title = $val->title;
                    $todo->priority = $val->priority;
                    $list_todos[] = $todo;
                }
                if(count($normal1) > 0 || count($all_day) > 0){
                    $event1 = new \stdClass();
                    $event1->normal = $normal1;
                    $event1->allday = $all_day;
                }else{
                   $event1=[];
                }
                $object4->events = $event1;
                $object4->todos = $list_todos;
                $by_date[]=$object4;
            }
            $schedules1 = $this->scheduleRepo->getByMemberAllAboutDay($member_ids,$begin_date,date('Y-m-d',strtotime(" +6 days",strtotime($begin_date))));
                foreach($schedules1 as $schedule){
                    $all = new \stdClass();
                    if(is_null($schedule->menu)){
                        if($schedule->pattern == 2){
                            $all->data = $schedule->title;
                        }else{
                            $all->data = $schedule->title.' ('.$schedule->percent.' %)';
                        }
                    }else{
                        $all->data = explode(';#',$schedule->menu)[0].":".$schedule->title;
                    }
                    $all->detail = $schedule->title;
                    $all->end_date = date('Y-m-d H:i:s',strtotime($schedule->end_date));
                    $all->id = $schedule->id;
                    $all->menu = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[0];
                    $all->menu_color = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[1];
                    $all->start_date = date('Y-m-d H:i:s',strtotime($schedule->start_date));
                    $all->type = 'share_banner';
                    $all->uid = $schedule->uid;
                    $banner[] = $all;
            }
        }else{
            $object4 = new \stdClass();
            $object4->calendar = null;
            $object4->date  = $begin_date;
            $object4->date_type = "s_date_".strtolower(date('l',strtotime($begin_date)));
            //start list event by day
            if(!$request->get('eid')){
                $schedules = $this->scheduleRepo->getAllByMemberDay($member_ids,$begin_date);
            }else{
                $schedules = $this->scheduleRepo->getByEquipmentDay([$request->get('eid')],$begin_date);
            }
            $todos = $this->todoRepo->getListByMember(\Auth::guard('member')->user()->id,$begin_date);
            $normal1 = [];
            $all_day = [];
            foreach($schedules as $schedule){
                $normal = new \stdClass();
                $normal->id = $schedule->id;
                if(in_array(\Auth::guard('member')->user()->id,$schedule->member()->pluck('id')->toArray()) || $schedule->private == 0){
                    $normal->data = $schedule->title .''. (count($schedule->equipment) > 0 ? ' ['.implode(',',$schedule->equipment()->pluck('name')->toArray()).']' : '');
                    $normal->private = 0;
                }else{
                    $normal->data = 'Đã có lịch';
                    $normal->private = 1;
                }
                $normal->detail = $schedule->title;
                $normal->end_date = date('Y-m-d H:i:s',strtotime($schedule->end_date));
                if($schedule->none_time != 1){
                     $normal->end_time = date('Y-m-d',strtotime($schedule->end_date));
                     $normal->showtime = date('h:i A',strtotime($schedule->start_date));
                     $normal->conflict = 'false';
                }else{
                    unset($normal->private);
                }
                $normal->faci_items = '';
                $normal->facility_name = $schedule->equipment ? implode(',',$schedule->equipment()->pluck('name')->toArray()) : '';
                $normal->menu = '';
                $normal->menu_color = '';
                $normal->start_date = date('Y-m-d H:i:s',strtotime($schedule->start_date));
                if($schedule->pattern == 1){
                    $normal->type = 'normal';
                }else{
                    $normal->type = 'share_repeat';
                    if($schedule->type_repeat == 'week' || $schedule->type_repeat == 'weekday'){
                        $normal->type_repeat = 'week';
                    }elseif($schedule->type_repeat == 'day'){
                        $normal->type_repeat = 'day';
                    }else{
                        $normal->type_repeat = 'month';
                    }
                }
                $normal->uid = $schedule->uid;
                if($schedule->none_time == 1){
                    $all_day[] = $normal;
                }else{
                    $normal1[] = $normal;
                }
            }
            if(count($normal1) > 0 || count($all_day) > 0){
                $event1 = new \stdClass();
                $event1->normal = $normal1;
                $event1->allday = $all_day;
            }else{
               $event1=[];
            }
            $list_todos = [];
            foreach($todos as $val){
                $todo = new \stdClass();
                $todo->tid = $val->id;
                $todo->cid = $val->member_id;
                $todo->title = $val->title;
                $todo->priority = $val->priority;
                $list_todos[] = $todo;
            }
            $object4->events = $event1;
            $object4->todos = $list_todos;
            $by_date[]=$object4;
            $schedules1 = $this->scheduleRepo->getByMemberAllDay($member_ids,$object4->date);
            foreach($schedules1 as $schedule){
                $all = new \stdClass();
                if(is_null($schedule->menu)){
                     if($schedule->pattern == 2){
                         $all->data = $schedule->title;
                     }else{
                          $all->data = $schedule->title.' ('.$schedule->percent.' %)';
                     }
                }else{
                    $all->data = explode(';#',$schedule->menu)[0].":".$schedule->title;
                }
                $all->detail = $schedule->title;
                $all->end_date = date('Y-m-d H:i:s',strtotime($schedule->end_date));
                $all->id = $schedule->id;
                $all->menu = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[0];
                $all->menu_color = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[1];
                $all->start_date = date('Y-m-d H:i:s',strtotime($schedule->start_date));
                $all->type = 'share_banner';
                $all->uid = $schedule->uid;
                $banner[] = $all;
            }
        }
        $events->banner=$banner;
        //end list event by day
        $events->by_date=$by_date;
        $object->events = $events;
        $object->members = $members;
        return response()->json($object);
    }
    public function loadSchedule(Request $request){
        if($request->get('bdate')){
            $date_now = $request->get('bdate');
        }else{
            $date_now = date('Y-m-d');
        }
        if($request->get('action') == 'next'){
            $date_now = date('Y-m-d', strtotime(" +" . $request->get('day') . " days",strtotime($date_now)));
        }else{
            $date_now = date('Y-m-d', strtotime(" -" . $request->get('day') . " days",strtotime($date_now)));
        }
        if(!is_null(\Auth::guard('member')->user()->department)){
            $department_id = \Auth::guard('member')->user()->department->getIds();
        }else{
            $department_id= [];
        }
        $members = $this->memberRepo->getByDepartment($department_id,\Auth::guard('member')->user()->id);
         $html ='<colgroup>
                    <col class="group_week_calendar_column_first">
                    <col class="group_week_calendar_column_common">
                    <col class="group_week_calendar_column_common">
                    <col class="group_week_calendar_column_common">
                    <col class="group_week_calendar_column_common">
                    <col class="group_week_calendar_column_common">
                    <col class="group_week_calendar_column_common">
                    <col class="group_week_calendar_column_common">
                 </colgroup>
                 <tr>
                 <td class="s_domain_week">
                    <span class="domain">(UTC+07:00) VietNam</span>
                 </td>';
        for($i = 0; $i < 7; $i++) {
             $date = date('Y-m-d', strtotime(" +" . $i . " days",strtotime($date_now)));
             if($i == 0){
                $html .='<td class="s_date_'.strtolower(date('l',strtotime($date))).'_week" align="center">&nbsp;<a href="/schedule/group_day?bdate='.$date.'">'.date('D, d M',strtotime($date)).'</a><input type="hidden" id="day_start" value="'.date('Y-m-d',strtotime($date)).'" /></td>';
             }else{
                $html .='<td class="s_date_'.strtolower(date('l',strtotime($date))).'_week" align="center">&nbsp;<a href="/schedule/group_day?bdate='.$date.'">'.date('D, d M',strtotime($date)).'</a></td>';
             }
        }
        if(is_null(\Auth::guard('member')->user()->file())){
            $avatar = '<div class="profileImageUser-grn"></div>';
        }else{
            $avatar = '<div class="user_photo_grn" style="background-image: url('.\Auth::guard('member')->user()->file()->link.');" aria-label=""></div>';
        }
        $html .='</tr><tr class="js_customization_schedule_user_id_'.\Auth::guard('member')->user()->id.'">
                    <td valign="top" class="calendar_rb_week userBox">
                        <div class="userElement profileImageBase-grn profileImageBaseSchedule-grn">
                           <dl>
                              <dt>
                                 <a >
                                    <div class="profileImage-grn">
                                       <div class="profileImageFrame-grn">
                                          '.$avatar.'
                                       </div>
                                    </div>
                                 </a>
                              </dt>
                              <dd><a >'.\Auth::guard('member')->user()->full_name.'</a></dd>
                           </dl>
                           <div class="clear_both_0px"></div>
                        </div>
                        <div class="shortcut_box_full"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?bdate='.$date_now.'"><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">Day</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.$date_now.'"><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">Week</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month?bdate='.$date_now.'"><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">Month</a></span></div>
                        <div class="shortcut_box_short" style="display:none"><span class="nowrap-grn schedule_userbox_item_grn"><a href="/schedule/personal_day?bdate='.$date_now.'"><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.$date_now.'"><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a href="/schedule/personal_month?bdate='.$date_now.'"><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month"></a></span></div>
                     </td>';
        for($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', strtotime(" +" . $i . " days",strtotime($date_now)));
            $schedules = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                            ->whereDate('schedule.start_date', '<=', $date)->whereDate('schedule.end_date', '>=', $date)->whereNotIn('pattern',['2','4'])->get();
            $html .= '<td valign="top" class="s_user_week normalEvent" rel="/schedule/simple_add?bdate='.$date_now.'">
                        <div class="addEvent">
                           <a title="Add" href="/schedule/create?bdate='.$date.'&amp;uid='.\Auth::guard('member')->user()->id.'&amp;">
                              <div class="iconWrite-grn"></div>
                           </a>
                        </div>
                        <div class="js_customization_schedule_date_'.$date.'"></div>
                        <div class="groupWeekInfo"></div>';
            $todos = $this->todoRepo->getListByDate(\Auth::guard('member')->user()->id, $date);
            foreach ($todos as $todo) {
                $html .= '<div class="schedule_todo normalEventElement">
                                <img src="/assets/mobile/img/todoPersonalInSchedule16.png" border="0" alt=""><a href="' . route('frontend.todo.view', $todo->id) . '">' . $todo->title . '</a>
                            </div>';
            }     
            foreach($schedules as $schedule){
             $menu = !is_null($schedule->menu) ? '<span class="event_color' . explode(';#', $schedule->menu)[1] . '_grn">' . explode(';#', $schedule->menu)[0] . '</span>' : '';
             $html .= '<div class="share normalEventElement   group_week_calendar_item">
                           <div class="listTime"><a href="/schedule/view/'.$schedule->id.'&amp;bdate='.$date_now.'">'.date('h:i A',strtotime($schedule->start_date)).'-'.date('h:i A',strtotime($schedule->end_date)).'</a></div>
                           <div class="groupWeekEventTitle"><a href="/schedule/view/'.$schedule->id.'&amp;bdate='.$date_now.'">'.$menu.' '.$schedule->title.' </a></div>
                        </div>';
            }
        }
        $html .='</td></tr>';
        $date = date('D, F d,Y',strtotime($date_now));
        return response()->json(array('html'=>$html,'date'=>$date));
    }
    public function naviCalendar(Request $request){
        return $this->scheduleRepo->showCalendar($request->get('cndate'),$request->get('location'));
    }
     public function naviCalendarDisplay(Request $request){
        return 1;
    }
    public function getMoreMember(Request $request){
        $data = new \stdClass();
        $data->is_need_scroll = true;
        $member_info = new \stdClass();
        $members = $this->memberRepo->getByDepartment([$request->get('s_oid')]);
        foreach($members as $key=>$member){
            $object = new \stdClass();
            $object->displayName = $member->full_name;
            $object->foreignKey = "";
            $object->group_path = "";
            $object->id = strval($member->id);
            $object->isNotUsingApp = false;
            $object->type = "user";
            $object->s_oid = strval($member->department_id);
            $object->url = "";
            $member_info->$key = $object;
        }
        $data->members_info = $member_info;
        return response()->json($data);
    }
   public function naviCalendarMonth(Request $request){
        return $this->scheduleRepo->showCalendarMonth($request->get('cndate'),$request->get('location'));
    }
    public function getScheduleByMonth(Request $request){
        $start_date = $request->get('date');
        $end_date = date('Y-m-t',strtotime($start_date));
        $schedules_all_mobile = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                                ->where(function($query) use ($start_date,$end_date) {
                                    $query->whereBetween(\DB::raw('DATE(start_date)'), array($start_date, $end_date))
                                          ->orwhereBetween(\DB::raw('DATE(end_date)'), array($start_date, $end_date));
                                })->get();
        $list_schedule_mobile = [];
        foreach ($schedules_all_mobile as $key => $val) {
            $object = new \stdClass();
            $object->calendar = 'Work';
            if(!is_null($val->menu) && $val->menu != ''){
                $object->color = 'bg-color'.explode(';#',$val->menu)[1];
            }else{
                $object->color = 'bg-white';
            }
            if($val->pattern == 2){
               $object->pattern = 2;
               $object->eventName ='[All day] '.$val->title;
            }else{
               $object->pattern = 1;
               $object->eventName = $val->title;
            }
            $object->start_date = date('m/d/Y',strtotime($val->start_date));
            $object->end_date = date('m/d/Y',strtotime($val->end_date));
            $list_schedule_mobile[] = $object;
        }
        return response()->json(array('data'=>$list_schedule_mobile));
    }
    public function getScheduleByDay(Request $request){
        $date = $request->get('date');
        $schedules_all_mobile = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', $request->get('member_id'))
                                ->whereDate('start_date','<=',$date)->where('end_date','>=',$date)->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
        $list_schedule_mobile = [];
        $html = '';
        foreach ($schedules_all_mobile as $key => $val) {
            if(!is_null($val->menu)){
                $color = 'bg-color'.explode(';#',$val->menu)[1];
            }else{
                $color = 'bg-white';
            }
            if($val->pattern == 2){
                $html .='<div class="item mb-4">
                        <a href="'.route('frontend.schedule.view',$val->id).'">
                        <div class="d-flex flex-column time time-area mr-2">
                            <span class="fs-16 text-muted">[All day]</span>
                        </div>
                        <div class="dot '.$color.'"></div>
                        <div class="content">
                            <h4 class="title fs-16">'.$val->title.'</h4>
                            <span class="text-muted fs-16">'.$val->memo.'</span>
                        </div>
                        </a>
                    </div>';
            }else{
                $html .='<div class="item mb-4">
                        <a href="'.route('frontend.schedule.view',$val->id).'">
                        <div class="d-flex flex-column time time-area mr-2">
                            <span class="fs-16 pb-2 text-muted">'.date('H:i A',strtotime($val->start_date)).'</span>
                            <span class="fs-16 text-muted">'.date('H:i A',strtotime($val->end_date)).'</span>
                        </div>
                        <div class="dot '.$color.'"></div>
                        <div class="content">
                            <h4 class="title fs-16">'.$val->title.'</h4>
                            <span class="text-muted fs-16">'.$val->memo.'</span>
                        </div>
                        </a>
                    </div>';
            }
        }
        $title = strtoupper(date('l',strtotime($request->get('date')))).' '.date('d',strtotime($request->get('date')));
        return response()->json(array('html'=>$html,'title'=>$title));
    }
}
