<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CommentRepository;
use App\Repositories\MemberTodoRepository;
use App\Repositories\MemberRepository;
use Repositories\EquipmentRepository;
use Repositories\ScheduleRepository;
use Repositories\ToDoRepository;
use Repositories\NotificationScheduleRepository;
use Repositories\ProjectRepository;
use Mail;
use stdClass;
use Hash;
use Illuminate\Support\Carbon;
use \Pusher\Pusher;
use Repositories\FileRepository;
use Repositories\LogApprovedRepository;
use Repositories\LevelRepository;

class MobileController extends Controller
{
    public function __construct(LevelRepository $levelRepo,LogApprovedRepository $logapprovedRepo,MemberRepository $memberRepo,ScheduleRepository $scheduleRepo,ToDoRepository $todoRepo,FileRepository $fileRepo,MemberTodoRepository $membertodoRepo, CommentRepository $commentRepo, EquipmentRepository $equipmentRepo, NotificationScheduleRepository $notificationSchRepo, ProjectRepository $projectRepo) {
        $this->memberRepo = $memberRepo;
        $this->scheduleRepo = $scheduleRepo;
        $this->todoRepo = $todoRepo;
        $this->fileRepo = $fileRepo;
        $this->membertodoRepo = $membertodoRepo;
        $this->commentRepo = $commentRepo;
        $this->equipmentRepo = $equipmentRepo;
        $this->notificationSchRepo = $notificationSchRepo;
        $this->projectRepo = $projectRepo;
        $this->logapprovedRepo = $logapprovedRepo;
        $this->levelRepo= $levelRepo;
    }
    public function login(Request $request){
        $input = [
            'login_id' => $request->get('login_id'),
            'password' => $request->get('password'),
            'is_deleted'=>0,
        ];
        if (\Auth::guard('member')->attempt($input,true)) {
            $member = \Auth::guard('member')->user();
            $member['session_id'] = session()->getId();
            $member['api_token'] = $member->createToken('MyApp')->accessToken;
            $member->save();
            $data = new stdClass();
            $data->id = $member->id;
            $data->login_id = $member->login_id;
            $data->avatar = $member->avatar;
            $data->token = $member->api_token;
            $data->full_name = $member->full_name;
            $data->phone = $member->phone;
            $data->position_name = $member->position ? $member->position->name : 'Chưa cập nhật';
            $data->department_name = $member->department ? $member->department->name : 'Chưa cập nhật';
            $data->email = $member->email;
            $data->address = $member->address;
            return response()->json(array('status'=>1,'msg'=>'Đăng nhập thành công','data'=>$data));
        }else{
            return response()->json(array('status'=>0,'msg'=>'Đăng nhập không thành công'));
        }
    }
    public function dashboard(Request $request){
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $count_schedule = count($member->schedule);
        $count_project = count($member->project);
        $count_todolist = count($member->membertodo);
        $data = $this->scheduleRepo->getRecently($member->id);
        $schedule = new stdClass();
        $schedule->title = $data->title;
        $schedule->day = date('d',strtotime($data->start_date)).' tháng '.date('m',strtotime($data->start_date));
        $schedule->date = \App\Helpers\StringHelper::weekendDay(date('w',strtotime($data->start_date)));
        $schedule->time = date('H:i',strtotime($data->start_date));
       return response()->json(array('success'=>true,'schedule'=>$schedule,'count_schedule'=>$count_schedule,'count_todolist'=>$count_todolist,'count_project'=>$count_project));
    }
    public function searchEquipment(Request $request){
        $name = $request->get('name');
        $equip_ids = $request->get('equip_id');
        $equips = $this->equipmentRepo->searchEquipApp($name,$equip_ids);
        return response()->json($equips);
    }
    public function searchMember(Request $request){
        $name = $request->get('name');
        return response()->json(['success'=>true]);
        $member_ids = $request->get('member_id');
        $members = $this->memberRepo->searchMemberApp($name,$member_ids);
        return response()->json($members);
    }
    public function notification(Request $request){
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $notifications = $member->unreadNotifications;
        $data = [];
        foreach($notifications as $val){
            $notification = new stdClass();
            $notification->creator = $val->data['full_name'];
            $notification->content = $val->data['content'];
            $notification->date = date('d/m',strtotime($val->created_at));
            $data[] = $notification;
        }
        return response()->json(array('success'=>true,'data'=>$data));
    }
    public function info(Request $request){
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $data = new stdClass();
        $data->full_name = $member->full_name;
        $data->avatar = $member->avatar;
        $data->email = $member->email;
        $data->phone = $member->phone;
        $data->address = $member->address;
        return response()->json(array('success'=>true,'data'=>$data));
    }
    public function infoUpdate(Request $request){
        return response()->json($request->avatar);
        $input = $request->all();
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        if(isset($_FILES['avatar'])){
           
            $targetDir = "/upload/images/";
            $allowTypes = array('jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG');
            $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
            $fileName = basename($_FILES['avatar']['name']);
            if(strlen($fileName)!=0){
                $fileType = pathinfo($fileName);
                $fileName = time().'_'.$fileName;
                if(in_array($fileType['extension'], $allowTypes)){
                    // Upload file to server
                    move_uploaded_file($_FILES['avatar']['tmp_name'],'upload/images/'.$fileName);
                }else{
                    return response()->json(array('success' => 'false','message'=>'Chỉ được upload file ảnh'));
                }
                $targetFilePath = $targetDir . $fileName;
                $input['avatar'] = $targetFilePath;
            }
        }
        $this->memberRepo->update($input,$member->id);
        return response()->json(array('success'=>true,'msg'=>'Cập nhật thông tin thành công'));
    }
    public function updatePassword(Request $request){
        $input = $request->all();
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        if(!Hash::check($input['password'], $member->password)){
            return response()->json(array('success'=>false));
        }else{
            $this->memberRepo->update(['password'=>bcrypt($input['new_password'])],$member->id);
            return response()->json(array('success'=>true,'msg'=>'Cập nhật mật khẩu thành công'));
        }
    }
    public function todolist(Request $request){
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $uncompleted = $member->todo()->whereIn('todo.status',[1,2])->select('id','title','priority','start_date','end_date')->orderBy('created_at','DESC')->get();
        foreach($uncompleted as $key=>$value){
            $value->members = $value->member()->select('id','full_name','avatar')->get();
            $value->progress =  number_format($value->progress());
        }
        $completed = $member->todo()->where('todo.status',3)->select('id','title','priority','start_date','end_date')->orderBy('created_at','DESC')->get();
        foreach($completed as $key=>$value){
            $value->members = $value->member()->select('id','full_name','avatar')->get();
            $value->progress =  number_format($value->progress());
        }
        return response()->json(array('success'=>true,'uncompleted'=>$uncompleted,'completed'=>$completed));
    }
    public function deleteTodolist(Request $request){
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $todo_id = $request->get('id');
        $todo = $this->todoRepo->find($todo_id);
        if($member->id == $todo->created_by){
            $todo->delete();
            return response()->json(array('success'=>true,'msg'=>'Xóa thành công'));
        }else{
            return response()->json(array('success'=>false));
        }
    }
    public function editProgress(Request $request){
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $todo = $this->todoRepo->find($request->get('todo_id'));
        $progress = $todo->member()->where('id',$member->id)->first()->pivot->progress;
        return response()->json(array('success'=>true,'progress'=>$progress));
    }
    public function updateProgress(Request $request){
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $todo = $this->todoRepo->find($request->get('todo_id'));
        if($request->get('progress') == 100){
            if(strtotime(date('Y-m-d H:i')) < strtotime(date('Y-m-d H:i',strtotime($todo->end_date)))){
                $this->membertodoRepo->updateRecord($member->id,$request->get('todo_id'),['progress'=>$request->get('progress'),'complete_date'=>date('Y-m-d H:i'),'pattern'=>1]);
            }elseif(strtotime(date('Y-m-d H:i')) == strtotime(date('Y-m-d H:i',strtotime($todo->end_date)))){
                $this->membertodoRepo->updateRecord($member->id,$request->get('todo_id'),['progress'=>$request->get('progress'),'complete_date'=>date('Y-m-d H:i'),'pattern'=>2]);
            }else{
               $this->membertodoRepo->updateRecord($member->id,$request->get('todo_id'),['progress'=>$request->get('progress'),'complete_date'=>date('Y-m-d H:i'),'pattern'=>3]);
            }
        }else{
            $this->membertodoRepo->updateRecord($member->id,$request->get('todo_id'),['progress'=>$request->get('progress')]);
        }
        if($todo->progress() == 100){
            if(strtotime(date('Y-m-d H:i')) == strtotime(date('Y-m-d H:i',strtotime($todo->end_date)))){
                $this->todoRepo->update(['status'=>3,'complete_date'=>$todo->end_date],$request->get('todo_id'));
            }else{
                $this->todoRepo->update(['status'=>3,'complete_date'=>date('Y-m-d H:i')],$request->get('todo_id'));
            }
        }
        return response()->json(array('success'=>true,'msg'=>'Cập nhật tiến độ công việc thành công'));
    }
    public function detailTodolist(Request $request){
        $token = $request->bearerToken();
        $id = $request->get('id');
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $todo = $this->todoRepo->find($id);
        $member_todo = \App\MemberTodo::where('todo_id',$id)->where('member_id',$this->memberRepo->getByToken($token)->id)->first();
        
        if(!$todo){
            return response()->json(array('success'=>false));
        }else{
            $data = $todo;
            $data->files = $todo->files()->select('link','name')->get();
            $data->members = $todo->member()->select('id','full_name','avatar')->get();
            $members_id = array(); 
            foreach($data->members as $record){
                array_push($members_id, $record->id);
            }
            $data->members_id = $members_id;
            $data->comment = $this->commentRepo->getByTodo($data->id);
            $data->progress =  number_format($data->progress());
            $data->join = $member_todo->join;
            $data->person_progress = $member_todo->progress;
            
            foreach($data->comment as $key => $cmt){
                $member = \App\Member::select('id','full_name','avatar')->find($cmt->member_id)->toArray();
                $cmt->member = $member;
            }
            return response()->json(array('success'=>true,'data'=>$data));
        }
    }
    public function joinTodolist(Request $request){
        $token = $request->bearerToken();
        $id = $request->get('id');
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        if($request->get('join') == 1){
            $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['join'=>1,'status'=>1]);
            $this::checkStatus($id);
            return response()->json(array('success'=>true));
        }else{
            $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['join'=>2]);
            $this::checkStatus($request->get('id'));
            return response()->json(array('success'=>true));
        }
        return response()->json(array('success'=>false));
    }
    public function checkStatus($todo_id){
        $count = \App\MemberTodo::where('todo_id',$todo_id)->count();
        $count_status_0 = \App\MemberTodo::where('todo_id',$todo_id)->where('join','>',0)->count();
        $count_status_2 = \App\MemberTodo::where('todo_id',$todo_id)->where('join',2)->count();
        if($count == $count_status_0 && $count_status_2 < $count){
            $this->todoRepo->update(['status'=>2],$todo_id);
        }else if($count_status_2 == $count){
            $this->todoRepo->update(['status'=>4],$todo_id);
        }else{
            $this->todoRepo->update(['status'=>1],$todo_id);
        }
    }
    public function updateTodoList(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $id=$input['id'];
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $todo = $this->todoRepo->find($input['id']);
        if(isset($input['status']) && $input['status'] == \App\ToDo::STATUS_COMPLETE){
            foreach($todo->member as $key=>$val){
                $this->membertodoRepo->updateRecord($val->id,$id,['progress'=>100]);
            }
        }
        if(isset($input['member_id']) && $input['member_id'] > 0){
            $todo->member()->sync($input['member_id']);
        }
        $this::checkStatus($todo->id);
        // Thêm file vào project
        $this->fileRepo->resetRelationshipID($todo->id,[\App\File::TYPE_TODO]);
        if(isset($input['files']) && count($input['files']) > 0){
            foreach($input['files'] as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$todo->id],$val);
            }
        }
        $this->fileRepo->deleteFileNull();
        $this->todoRepo->update($input,$id);
        return response()->json(array('success'=>true,'msg'=>'Cập nhật thông tin thành công'));
    }
    public function storeTodoList(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $input['created_by'] = \Auth::guard('member')->user()->id;
        if(is_null($input['priority'])){
            $input['priority'] = 3;
        }
        if(isset($input['file'])){
            $input['file'] = implode(',',$input['file']);
        }
        $todo = $this->todoRepo->create($input);
        if(isset($input['member_id']) && $input['member_id'] > 0){
            $todo->member()->attach($input['member_id']);
            if(in_array(\Auth::guard('member')->user()->id,$input['member_id'])){
                $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$todo->id,['join'=>1,'status'=>1]);
                $this::checkStatus($todo->id);
            }
            foreach($input['member_id'] as $val){
                if($val != \Auth::guard('member')->user()->id){
                    $this->memberRepo->NotificateMember($val,['content'=>'Bạn được giao 1 công việc','link'=>route('frontend.todo.view',$todo->id)]);
                    $member = $this->memberRepo->find($val);
                    if(!is_null($member->email)){
                        $email = $member->email;
                        $name = \Auth::guard('member')->user()->full_name;
                        Mail::send('mail', array('title'=>$todo->title,'content'=>$todo->content,'link'=>(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/todo/view/'.$todo->id), function($message) use ($email,$name){
                            $message->from('contact@ouransoft.vn', 'Ouransoft');
                            $message->to($email, 'Ouransoft')->subject('Thông báo giao việc từ '.$name);
                        });
                    }
                }
            }
        }
        // Thêm file vào project
        if(isset($input['files']) && count($input['files']) > 0){
            foreach($input['files'] as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$todo->id],$val);
            }
        }
        return response()->json(array('success'=>true,'msg'=>'Tạo mới thành công'));
    }

    public function schedule(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $schedules = $this->scheduleRepo->getAllByMember([$member->id],date('Y-m-d'));
        $data = [];
        foreach($schedules as $key=>$schedule){
            $object = new stdClass();
            $object->id= $schedule->id;
            $object->title = $schedule->title;
            $object->start_date = $schedule->start_date;
            $object->start_date = $schedule->start_date;
            $object->pattern =  $schedule->pattern;
            $object->end_date = $schedule->end_date;
            $object->menu_title = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[0];
            $object->menu_color = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[1];
            $data[] =  $object;
        }
        return response()->json(array('success'=>true,'data'=>$data));
    }
    
    public function searchSchedule(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $schedules = $this->scheduleRepo->getAllByMember([$input['member_id']],$input['date']);
        $data = [];
        foreach($schedules as $key=>$schedule){
            $object = new stdClass();
            $object->id= $schedule->id;
            $object->title = $schedule->title;
            $object->start_date = $schedule->start_date;
            $object->pattern =  $schedule->pattern;
            $object->end_date = $schedule->end_date;
            $object->menu_title = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[0];
            $object->menu_color = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[1];
            $data[] =  $object;
        }
        return response()->json(array('success'=>true,'data'=>$data));
    }
    
    public function sendText(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $group_id = $input['group_id'];
        $group = \App\Group::find($group_id);
        $files = "";
        $file_name = "";
        $size = 0;
        $ext = "";
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $data = new \App\GroupMessage();
        $avatar = $member->avatar;
        $name = $member->full_name;
        $data->type = 0;
        $data->from = $member->id;
        $data->group_id = $input['group_id'];
        $data->message = $input['content'];
        $data->save();
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
        );
        $from_name = \App\Member::where('id',$data->from)->first()->full_name;
        $member_ids = $group->member()->pluck('id')->toArray();
        $reply_message='';
        $reply_name='';
        $data = ['reply_id'=>$data->reply_id,'reply_message'=>$reply_message,'reply_name'=>$reply_name,'member_ids'=>json_encode($member_ids),'avatar'=>$avatar,'from' => $data->from,'from_name'=>$from_name,'time'=>date('h:i A'),'date'=>'time','date'=>date('d-m'),'group_id' => $group_id,'message'=>$data->message,'message_id'=>$data->id,'file_name'=>$file_name,'size'=>$data->size,'ext'=>$data->extension,'type'=>$data->type];
        $pusher->trigger('chat-message', 'send-message', $data);
        $object = new stdClass();
        $object->content = $input['content'];
        $userinfo = new stdClass();
        $userinfo->name = $name;
        $userinfo->avatar = $avatar;
        $object->userinfo = $userinfo;
        return response()->json(['success'=>'true','data'=>$object]);
    }
    public function sendFile(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $group_id = $input['group_id'];
        $group = \App\Group::find($group_id);
        $files = "";
        $file_name = "";
        $size = 0;
        $ext = "";
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $data = new \App\GroupMessage();
        $data->type = 2;
        $destinationPath = 'chat/message/images';
        $image = $request->file;
        $files = $destinationPath . '/' . $image->getClientOriginalName();
        $file_name = time()."_".$image->getClientOriginalName();
        $image->move($destinationPath, $file_name);
        $message = $destinationPath . '/' . $file_name;
        $avatar = $member->avatar;
        $name = $member->full_name;
        $data->from = $member->id;
        $data->group_id = $input['group_id'];
        $data->message = $message;
        $data->save();
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
        );
        $from_name = \App\Member::where('id',$data->from)->first()->full_name;
        $member_ids = $group->member()->pluck('id')->toArray();
        $reply_message='';
        $reply_name='';
        $data = ['reply_id'=>$data->reply_id,'reply_message'=>$reply_message,'reply_name'=>$reply_name,'member_ids'=>json_encode($member_ids),'avatar'=>$avatar,'from' => $data->from,'from_name'=>$from_name,'time'=>date('h:i A'),'date'=>'time','date'=>date('d-m'),'group_id' => $group_id,'message'=>$data->message,'message_id'=>$data->id,'file_name'=>$file_name,'size'=>$data->size,'ext'=>$data->extension,'type'=>$data->type];
        $pusher->trigger('chat-message', 'send-message', $data);
        $object = new stdClass();
        $object->file = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') .$destinationPath . '/' . $file_name;
        $userinfo = new stdClass();
        $userinfo->name = $name;
        $userinfo->avatar = $avatar;
        $object->userinfo = $userinfo;
        return response()->json(['success'=>'true','data'=>$object]);
    }
    public function detailSchedule(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $schedule_id = $input['id'];
        $schedule = \App\Schedule::find($schedule_id);
        // $schedule->member = $schedule->member ? $schedule->member()->pluck('full_name')->toArray() : [];
        $schedule->members = $schedule->member()->select('id','full_name')->get();
        $schedule->equipment = $schedule->equipment ? $schedule->equipment()->pluck('name')->toArray() : [];
        $schedule->menu_title = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[0];
        $schedule->menu_color = is_null($schedule->menu) ? '': explode(';#',$schedule->menu)[1];
        $schedule->created_by = $schedule->created_by;
        return response()->json(['success'=>'true','data'=>$schedule]);
    }
    public function deleteSchedule(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $schedule = $this->scheduleRepo->find($input['id']);
        if(isset($input['apply']) && $input['apply'] != 'this'){
            if($input['apply'] == 'after'){
                $record = $this->scheduleRepo->deleteAfter($schedule->event,$schedule->start_date);
                $this->scheduleRepo->updateBefore($schedule->event,$schedule->start_date);
            }else{
                $record = $this->scheduleRepo->deleteAll($schedule->event);
            }
        }else{
            \App\MemberSchedule::where('schedule_id',$input['id'])->delete();
            \App\EquipmentSchedule::where('schedule_id',$input['id'])->delete();
            $record = $this->scheduleRepo->delete($input['id']);
        }
        return response()->json(array('success'=>true,'msg'=>'Xóa thành công'));
    }
    
    public function leaveSchedule(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $schedule = $this->scheduleRepo->find($input['id']);
        $array = $schedule->member()->pluck('id')->toArray();
        $input['member_id'] = array_diff($array, [$member->id]);
        $schedule->member()->sync($input['member_id']);
        $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Đã không tham gia lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
        $this->notificationSchRepo->createNotification('Đã không tham gia lịch trình', $member->id, $input['member_id'], route('frontend.schedule.view', $schedule->id));
        return response()->json(array('success'=>true,'msg'=>'Không tham gia thành công'));
    }
    public function storeSchedule(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        if($input['pattern'] == 1 || $input['pattern'] == 3){
            if(is_null($input['start_hour']) && is_null($input['end_hour']) ){
                $input['start_hour'] = '00';
                $input['start_minute'] = '00';
                $input['end_hour'] = '23';
                $input['end_minute'] = '59';
                $input['none_time'] = 1;
            }
        }
        $schedule_id = 0;
        $equipment_name = [];
        if (count($input['selected_users_sITEM'])>1) {
            if ($input['selected_users_sITEM'] != '') {
                // $equipment_ids = explode(':', $input['selected_users_sITEM']);
                $equipment_ids = $input['selected_users_sITEM'];
                unset($equipment_ids[0]);
                $start_time = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $end_time = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                foreach ($equipment_ids as $key => $equipment_id) {
                    $eq_schedule = $this->scheduleRepo->getByEquipmentAllAboutTime($equipment_id, $start_time, $end_time);
                    if (count($eq_schedule) > 0) {
                        $equipment = $this->equipmentRepo->find($equipment_id);
                        $equipment_name[] = $equipment->name;
                    }
                }
                if (count($equipment_name) > 0) {
                    $equipment_name = implode(',', $equipment_name);
                    $object = new \stdClass();
                    $object->cause = "When reserving a facility, you must set the (time) period so that it does not overlap other appointments.";
                    $object->code = "GRN_SCHD_13208";
                    $object->counter_measure = '"Confirm the appointment for the following facility: "' . $equipment_name . '"."';
                    $object->diagnosis = '""The appointment at the following facility overlaps another appointment: ' . $equipment_name . '"."';
                    return response()->json($object);
                }
            }else{

            }
        }
        if ($input['selected_users_sUID'] == '') {
            $object = new \stdClass();
            $object->cause = "One or more attendees for this appointment are required to add or change the appointment.";
            $object->code = "GRN_SCHD_13021";
            $object->counter_measure = "Select one or more attendees";
            $object->diagnosis = "Attendee has not been specified";
            return response()->json($object);
        }
        
        if ($input['pattern'] == 3) {
            $equipment_conflic = [];
            if (count($input['selected_users_sITEM'])>0) {
                if ($input['selected_users_sITEM'] != '') {
                    // $equipment_ids = explode(':', $input['selected_users_sITEM']);
                    $equipment_ids = $input['selected_users_sITEM'];
                    unset($equipment_ids[0]);
                    $start_time = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                    $end_time = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                    foreach ($equipment_ids as $key => $equipment_id) {
                        $eq_schedules = $this->scheduleRepo->getByEquipmentAllAboutTime($equipment_id, $start_time, $end_time);
                        if ($eq_schedules) {
                            foreach ($eq_schedules as $key => $val) {
                                $equipment = $this->equipmentRepo->find($equipment_id);
                                $object1 = new \stdClass();
                                $object1->col_facility = $equipment->name;
                                $object1->col_setdatetime = strtotime($val->start_date);
                                $object1->setdatetime = date('D, d F', strtotime($val->start_date));
                                $equipment_conflic[] = $object1;
                            }
                        }
                    }
                    if (count($equipment_conflic) > 0) {
                        $object = new \stdClass();
                        $object->conflict_facility = 1;
                        $object->conflict_events = $equipment_conflic;
                        return response()->json($object);
                    }
                }
            }
            // start add schedule repeat
            $start_date = date('Y-m-d', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
            $end_date = date('Y-m-d', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day']));
            $date1 = new \DateTime($start_date);
            $date2 = new \DateTime($end_date);
            $day = $date1->diff($date2)->format("%a");
            $date_arr = [];
            if($day == 0){
                $time = new \DateTime($start_date);
                
                if ($input['type'] == 'week' && in_array($time->format("w"),$input['wday'])) {
                    $date_arr[] = $time->format("Y-m-d");
                }
                if (($time->format("w") != '0' && $time->format("w") != '6' && ($input['type'] == 'weekday')) || ($input['type'] == 'day')) {
                    $date_arr[] = $time->format("Y-m-d");
                }
                if ($input['type'] == 'month' && $time->format("d") == $input['day']) {
                    $date_arr[] = $time->format("Y-m-d");
                }
            }else{
                for ($i = 0; $i <= $day; $i++) {
                    $date1 = new \DateTime($start_date);
                    $time = $date1->modify('+' . $i . ' day');
                    if ($input['type'] == 'week' && in_array($time->format("w"),$input['wday'])) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                    if (($time->format("w") != '0' && $time->format("w") != '6' && ($input['type'] == 'weekday')) || ($input['type'] == 'day')) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                    if ($input['type'] == 'month' && $time->format("d") == $input['day']) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                }
            }
            if (count($date_arr) > 0) {
                $input['event'] = count(\App\Schedule::get()) + 1;
                $input['uid'] = $member->id;
                $input['type_repeat'] = $input['type'];
                if($input['type'] == 'month'){
                    $input['wday'] = $input['day'];
                }elseif($input['type'] == 'week'){
                    $input['wday'] = implode(',',$input['wday']);
                }else{
                    $input['wday'] = NULL;
                }
                $input['start_repeat'] = date('Y-m-d H:i:s', strtotime($start_date . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_repeat'] = date('Y-m-d H:i:s', strtotime($end_date . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                foreach ($date_arr as $key => $val) {
                    $input['start_date'] = date('Y-m-d H:i:s', strtotime($val . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                    $input['end_date'] = date('Y-m-d H:i:s', strtotime($val . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                    $schedule = $this->scheduleRepo->create($input);
                    // Thêm file vào schedule
                    if(isset($input['files']) && count($input['files']) > 0){
                        foreach($input['files'] as $key=>$val){
                            $this->fileRepo->update(['relationship_id'=>$schedule->id],$val);
                        }
                    }
                    if ($key == 0) {
                        $schedule_id = $schedule->id;
                    }
                    if ($input['selected_users_sUID'] != '') {
                        // $input['member_id'] = explode(':', $input['selected_users_sUID']);
                        $input['member_id'] = $input['selected_users_sUID'];
                        $emails = \App\Member::whereIn('id', $input['member_id'])->pluck('email')->toArray();
                        $schedule->member()->attach($input['member_id']);
                    }
                    if (count($input['selected_users_sITEM']) > 1) {
                        if ($input['selected_users_sITEM'] != '') {
                            // $input['equipment_id'] = explode(':', $input['selected_users_sITEM']);
                            $input['equipment_id'] = $input['selected_users_sITEM'];
                            unset($input['equipment_id'][0]);
                            $schedule->equipment()->attach($input['equipment_id']);
                        }
                    }
                    //SendNotificationJob::dispatch($input['title'], $input['memo'], $input['start_date'], $input['end_date'], $emails,$schedule->id)->delay(\Carbon\Carbon::now()->addSeconds(10));
                    }
                   if ($input['member_id']) {
                        if (($key = array_search($member->id, $input['member_id'])) !== false) {
                            unset($input['member_id'][$key]);
                        }
                        $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Bạn đã được thêm vào 1 lịch trình', 'link' => '/schedule/view/' . $schedule_id]);
                        $this->notificationSchRepo->createNotification('Bạn đã được thêm vào 1 lịch trình', $member->id, $input['member_id'], route('frontend.schedule.view', $schedule_id));
                   }
                   if (isset($input['selected_users_p_sUID']) && $input['selected_users_p_sUID'] != '') {
                        // $member_ids = explode(':', $input['selected_users_p_sUID']);
                        $member_ids = $input['selected_users_p_sUID'];
                        $input['member_id'] = $member_ids;
                        $schedule->seen()->attach($input['member_id']);
                        $this->memberRepo->NotificateMembers($member_ids, ['content' => 'Bạn được chia sẻ thông tin 1 lịch trình', 'link' => '/schedule/view/' . $schedule_id]);
                        $this->notificationSchRepo->createNotification('Bạn được chia sẻ thông tin 1 lịch trình', $member->id, $member_ids, route('frontend.schedule.view', $schedule_id));
                    }
            }else{
                $object = new \stdClass();
                $object->cause = "Cannot set because of the following reason. <ul><li>The combination of repeating condition and period is not correct</ul>";
                $object->code = "ORS_SC_13065";
                $object->counter_measure = "Confirm whether or not the date is correct.";
                $object->diagnosis = "Cannot use this repeating period.";
                return response()->json($object);
            }
            // end add schedule repeat
        } else {
            // start add schedule all day, normal
            if ($input['pattern'] == 1) {
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
            } elseif ($input['pattern'] == 2 || $input['pattern'] == 4) {
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day']));
            }
            $input['uid'] = $member->id;
            $input['event'] = count(\App\Schedule::get()) + 1;
            $schedule = $this->scheduleRepo->create($input);
            // Thêm file vào schedule
            if(isset($input['files']) && count($input['files']) > 0){
                foreach($input['files'] as $key=>$val){
                    $this->fileRepo->update(['relationship_id'=>$schedule->id],$val);
                }
            }
            $schedule_id = $schedule->id;
            if ($input['selected_users_sUID'] != '') {
                // $input['member_id'] = explode(':', $input['selected_users_sUID']);
                $input['member_id'] = $input['selected_users_sUID'];
                $emails = \App\Member::whereIn('id', $input['member_id'])->pluck('email')->toArray();
                $schedule->member()->attach($input['member_id']);
                if (($key = array_search($member->id, $input['member_id'])) !== false) {
                    unset($input['member_id'][$key]);
                }
                $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Bạn đã được thêm vào 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
                $this->notificationSchRepo->createNotification('Bạn đã được thêm vào 1 lịch trình', $member->id, $input['member_id'], route('frontend.schedule.view', $schedule->id));
            }
            if (isset($input['selected_users_p_sUID']) && $input['selected_users_p_sUID'] != '') {
                // $member_ids = explode(':', $input['selected_users_p_sUID']);
                $member_ids = $input['selected_users_p_sUID'];
                $input['member_id'] = $member_ids;
                $schedule->seen()->attach($input['member_id']);
                $this->memberRepo->NotificateMembers($member_ids, ['content' => 'Bạn được chia sẻ thông tin 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
                $this->notificationSchRepo->createNotification('Bạn được chia sẻ thông tin 1 lịch trình', $member->id, $member_ids, route('frontend.schedule.view', $schedule->id));
            }
            if (count($input['selected_users_sITEM'])>0) {
                if ($input['selected_users_sITEM'] != '') {
                    // $input['equipment_id'] = explode(':', $input['selected_users_sITEM']);
                    $input['equipment_id'] = $input['selected_users_sITEM'];
                    unset($input['equipment_id'][0]);
                    $schedule->equipment()->attach($input['equipment_id']);
                }
            }
            //$event_id = \App\Helpers\Calendar::insert($input['title'], '', $input['memo'], $input['start_date'], $input['end_date'], $emails);
            //$this->scheduleRepo->update(['event_id' => $event_id], $schedule->id);
            //end add schedule all day, normal
        }
        return response()->json(array('success'=>true,'msg'=>'Thêm mới thành công'));
    }
    
    public function updateSchedule(Request $request){
        $input = $request->all();
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        if($input['pattern'] == 1 || $input['pattern'] == 3){
            if(is_null($input['start_hour']) && is_null($input['end_hour'])){
                $input['start_hour'] = '00';
                $input['start_minute'] = '00';
                $input['end_hour'] = '23';
                $input['end_minute'] = '59';
                $input['none_time'] = 1;
            }else{
                $input['none_time'] = 0;
            }
        }
        $schedule = $this->scheduleRepo->find($input['id']);
        if ($input['pattern'] == 3 && $input['apply'] != 'this') {
            if ($input['apply'] == 'after') {
                $record = $this->scheduleRepo->find($input['id']);
                $start_time = date('Y-m-d H:i:s', strtotime($record->start_date));
                $start_date = date('Y-m-d', strtotime($record->start_date));
                $this->scheduleRepo->updateBefore($input['event'], date('Y-m-d', strtotime($record->start_date)));
                $this->scheduleRepo->deleteAfter($input['event'], date('Y-m-d', strtotime($record->start_date)));
            } else {
                $this->scheduleRepo->deleteAll($input['event']);
                $start_time = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $start_date = date('Y-m-d', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
            }
            $equipment_conflic = [];
            $end_time = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
            $end_date = date('Y-m-d', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day']));
            if (isset($input['selected_users_sITEM'])) {
                if ($input['selected_users_sITEM'] != '') {
                    // $equipment_ids = explode(':', $input['selected_users_sITEM']);
                    $equipment_ids = $input['selected_users_sITEM'];
                    unset($equipment_ids[0]);
                    foreach ($equipment_ids as $key => $equipment_id) {
                        $eq_schedules = $this->scheduleRepo->getByEquipmentAllAboutTime($equipment_id, $start_time, $end_time);
                        if ($eq_schedules) {
                            foreach ($eq_schedules as $key => $val) {
                                $equipment = $this->equipmentRepo->find($equipment_id);
                                $object1 = new \stdClass();
                                $object1->col_facility = $equipment->name;
                                $object1->col_setdatetime = strtotime($val->start_date);
                                $object1->setdatetime = date('D, d F', strtotime($val->start_date));
                                $equipment_conflic[] = $object1;
                            }
                        }
                    }
                    if (count($equipment_conflic) > 0) {
                        $object = new \stdClass();
                        $object->conflict_facility = 1;
                        $object->conflict_events = $equipment_conflic;
                        return response()->json($object);
                    }
                }
            }
            // start add schedule repeat
            $date1 = new \DateTime($start_date);
            $date2 = new \DateTime($end_date);
            $day = $date1->diff($date2)->format("%a") + 1;
            $date_arr = [];
            if($day == 0){
               $time = new \DateTime($start_date);
               if ($input['type'] == 'week' && in_array($time->format("w"),$input['wday'])) {
                    $date_arr[] = $time->format("Y-m-d");
                }
                if ($time->format("w") != '0' && $time->format("w") != '6' && (($input['type'] == 'weekday') || ($input['type'] == 'day'))) {
                    $date_arr[] = $time->format("Y-m-d");
                }
                if ($time->format("d") == $input['day'] && $input['type'] == 'month') {
                    $date_arr[] = $time->format("Y-m-d");
                }
            }else{
                for ($i = 0; $i <= $day; $i++) {
                    $date1 = new \DateTime($start_date);
                    $time = $date1->modify('+' . $i . ' day');
                    if ($input['type'] == 'week' && in_array($time->format("w"),$input['wday'])) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                    if ($time->format("w") != '0' && $time->format("w") != '6' && (($input['type'] == 'weekday') || ($input['type'] == 'day'))) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                    if ($input['type'] == 'month' && $time->format("d") == $input['day']) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                }
            }
            if (count($date_arr) > 0) {
                $input['event'] = count(\App\Schedule::get()) + 1;
                $input['uid'] = $member->id;
                $input['type_repeat'] = $input['type'];
                if($input['type'] == 'week'){
                    $input['wday'] = implode(',',$input['wday']);
                }
                $input['start_repeat'] = date('Y-m-d H:i:s', strtotime($start_date . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_repeat'] = date('Y-m-d H:i:s', strtotime($end_date . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                foreach ($date_arr as $key => $val) {
                    $input['start_date'] = date('Y-m-d H:i:s', strtotime($val . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                    $input['end_date'] = date('Y-m-d H:i:s', strtotime($val . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                    $schedule = $this->scheduleRepo->create($input);
                    if ($key == 0) {
                        $id = $schedule->id;
                    }
                    if ($input['selected_users_sUID'] != '') {
                        $input['member_id'] = explode(':', $input['selected_users_sUID']);
                        $emails = \App\Member::whereIn('id', $input['member_id'])->pluck('email')->toArray();
                        $schedule->member()->attach($input['member_id']);
                        if (($key = array_search($member->id, $input['member_id'])) !== false) {
                            unset($input['member_id'][$key]);
                        }
                        $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Cập nhật lại 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
                        $this->notificationSchRepo->createNotification('Cập nhật lại 1 lịch trình', $member->id, $input['member_id'], route('frontend.schedule.view', $schedule->id));
                    }
                    if (isset($input['selected_users_p_sUID']) && $input['selected_users_p_sUID'] != '') {
                        $member_ids = explode(':', $input['selected_users_p_sUID']);
                        $this->memberRepo->NotificateMembers($member_ids, ['content' => 'Bạn được chia sẻ thông tin 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
                        $this->notificationSchRepo->createNotification('Bạn được chia sẻ thông tin 1 lịch trình', $member->id, $member_ids, route('frontend.schedule.view', $schedule->id));
                    }
                    if (isset($input['selected_users_sITEM'])) {
                        if ($input['selected_users_sITEM'] != '') {
                            $input['equipment_id'] = explode(':', $input['selected_users_sITEM']);
                            $schedule->equipment()->attach($input['equipment_id']);
                        }
                    }
                    if (isset($input['private'])) {
                        if ($input['private'] == 1) {
                            $member_id = [];
                            $member_id[] = $member->id;
                            $input['member_id'] = $member_id;
                            $schedule->seen()->attach($input['member_id']);
                        }
                    }
                    if (isset($input['public_dashboard'])) {
                        $schedule->update($input);
                    }else{
                        $input['public_dashboard'] = "0";
                        $schedule->update($input);
                    }
                    //$event_id = \App\Helpers\Calendar::insert($input['title'], '', $input['memo'], $input['start_date'], $input['end_date'], $emails);
                    //$this->scheduleRepo->update(['event_id' => $event_id], $schedule->id);
                }
            }else{
                return response()->json(array('link' => '/schedule/create_repeat'));
            }

        } else {
            if ($input['pattern'] == 3 && $input['apply'] == 'this') {
                $schedule = $this->scheduleRepo->find($input['id']);
                $start_day = date('Y-m-d', strtotime($schedule->start_date));
                $end_day = date('Y-m-d', strtotime($schedule->end_date));
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($start_day . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($end_day . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                $input['event'] = count(\App\Schedule::get()) + 1;
                $input['pattern'] = 1;
                $input['wday']= NULL;
            } elseif ($input['pattern'] == 1) {
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
            } elseif ($input['pattern'] == 2 || $input['pattern'] == 4) {
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day']));
            }
            $input['update_person'] = $member->id;
            $input['update_at'] = date('Y-m-d');
            // dd($input);
            $this->scheduleRepo->update($input, $input['id']);
            // Thêm file vào schedule
            $this->fileRepo->resetRelationshipID($input['id'],[\App\File::TYPE_SCHEDULE]);
            if(isset($input['files']) && count($input['files']) > 0){
                foreach($input['files'] as $key=>$val){
                    $this->fileRepo->update(['relationship_id'=>$schedule->id],$val);
                }
            }
            $this->fileRepo->deleteFileNull();
            $schedule = $this->scheduleRepo->find($input['id']);
            if ($input['selected_users_sUID'] != '') {
                $input['member_id'] = explode(':', $input['selected_users_sUID']);
                $schedule->member()->sync($input['member_id']);
                if (($key = array_search($member->id, $input['member_id'])) !== false) {
                    unset($input['member_id'][$key]);
                }
                $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Bạn đã được thêm vào 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
            }
            if (isset($input['selected_users_sITEM'])) {
                if ($input['selected_users_sITEM'] != '') {
                    $input['equipment_id'] = explode(':', $input['selected_users_sITEM']);
                    $schedule->equipment()->sync($input['equipment_id']);
                }
            }elseif(count($schedule->equipment) > 0){
                $schedule->equipment()->detach();
            }
            if (isset($input['private'])) {
                if ($input['private'] == 2) {
                    $input['member_id'] = explode(',', $input['selected_users_p_sUID']);
                    $schedule->seen()->sync($input['member_id']);
                }
                if ($input['private'] == 1) {
                    $member_id = [];
                    $member_id[] = $member->id;
                    $input['member_id'] = $member_id;
                    $schedule->seen()->sync($input['member_id']);
                }
            }
            if(isset($input['public_dashboard'])){
                $schedule->update($input);
            }else{
                $input['public_dashboard'] = "0";
                $schedule->update($input);
            }
        }
        return response()->json(array('success'=>true,'msg'=>'Cập nhật thành công'));
    }

    public function statistical(Request $request){
        
        if($request->get('member_id')){
            $member_id = $request->get('member_id');
        }else{
            $member_id = \Auth::guard('member')->user()->id;
        }
        $data = array();
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');
        $lastQuarter = $now->quarter;
        if($request->get('search') == 'department'){
            $members = $this->memberRepo->getByDepartment([$request->get('department_id')]);
            if(count($members) > 0){
                $member_ids = $members->pluck('id')->toArray();
            }else{
                $member_ids = [];
            }
            $data['count_week'] = $this->membertodoRepo->countTodo($member_ids,'','',1,'week',TRUE);
            $data['count_month'] = $this->membertodoRepo->countTodo($member_ids,'','',1,'month',TRUE);
            $data['count_quarter'] = $this->membertodoRepo->countTodo($member_ids,'','',1,'quater',TRUE);
            $data['count_year'] = $this->membertodoRepo->countTodo($member_ids,'','',1,'year',TRUE);
            $data['list_todo'] = \App\Todo::join('member_todo','todo.id','=','member_todo.todo_id')->whereIn('todo.status',[1,2])->whereIn('member_todo.member_id',$member_ids)
                        ->select('todo.*')->groupBy('todo.id')->orderBy('todo.end_date','ASC')->pluck('todo.id');
            if($data['count_month'] == 0){
                $data['status_todo'] = [0,0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo($member_ids,2,0,1,'month',TRUE);
                $pattern1 = $this->membertodoRepo->countTodo($member_ids,3,1,1,'month',TRUE);
                $pattern2 = $this->membertodoRepo->countTodo($member_ids,3,2,1,'month',TRUE);
                $pattern3 = $this->membertodoRepo->countTodo($member_ids,3,3,1,'month',TRUE);
                $data['status_todo'] = [($pattern0 * 100)/$data['count_month'],($pattern1 * 100)/$data['count_month'], ($pattern2 * 100)/$data['count_month'],($pattern3 * 100)/$data['count_month']];
            }
        }else{
            $member = $this->memberRepo->find($member_id);
            $data['count_week'] = $this->membertodoRepo->countTodo([$member_id],'','',1,'week',FALSE);
            $data['count_month'] = $this->membertodoRepo->countTodo([$member_id],'','',1,'month',FALSE);
            $data['count_quarter'] = $this->membertodoRepo->countTodo([$member_id],'','',1,'quater',FALSE);
            $data['count_year'] = $this->membertodoRepo->countTodo([$member_id],'','',1,'year',FALSE);
            if($data['count_month'] == 0){
                $data['status_todo'] = [0,0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo([$member_id],2,0,1,'month',FALSE);
                $pattern1 = $this->membertodoRepo->countTodo([$member_id],3,1,1,'month',FALSE);
                $pattern2 = $this->membertodoRepo->countTodo([$member_id],3,2,1,'month',FALSE);
                $pattern3 = $this->membertodoRepo->countTodo([$member_id],3,3,1,'month',FALSE);
                $data['status_todo'] = [$pattern0,$pattern1,$pattern2,$pattern3];
            }
            $data['list_todo'] = \App\Todo::rightjoin('member_todo','todo.id','=','member_todo.todo_id')->whereIn('todo.status',[1,2])->where('member_todo.member_id',$member_id)
                        ->select('todo.*')->orderBy('todo.end_date','ASC')->groupBy('todo.id')->get();
        }
        $data['member_html'] = \App\Helpers\StringHelper::getSelectMemberOptions(\App\Member::where('is_deleted', 0)->get(),$member_id);
        if($request->get('department_id')){
            $data['department_html'] = \App\Helpers\StringHelper::getSelectOptions(\App\Department::where('level',\App\Department::max('level'))->get(),$request->get('department_id'));
        }else{
            $data['department_html'] = \App\Helpers\StringHelper::getSelectOptions(\App\Department::where('level',\App\Department::max('level'))->get());
        }

        return response()->json(array('success'=>true,'data'=>$data));
    }

    public function projectList(Request $request){
        $records = $this->projectRepo->getIndexByMember($request);
        foreach($records as $key=>$record){
            $logapproved = \App\LogApproved::where('project_id',$record->id)->orderBy('progress','DESC')->first();
            if($logapproved){
                if($logapproved->status == 1){
                        switch ($logapproved->progress){
                            case 2: 
                                $record->getStatusMobile = 'D1';
                                break;
                            case 3: 
                                $record->getStatusMobile = 'D2';
                                break;
                            case 4: 
                                $record->getStatusMobile = 'GV';
                                break;
                            case 5: 
                                $record->getStatusMobile = 'BC';
                                break;
                            case 6: 
                                $record->getStatusMobile = 'HT';
                                break;
                        }
                }elseif($logapproved->status == 0){
                     $record->getStatusMobile = 'TV';
                }else{
                    $record->getStatusMobile = 'HT';
                }
            }else{
                $record->getStatusMobile = 'TV';
            }
            $check = \App\LogApproved::where('project_id',$record->id)->where('status',1)->where('progress',5)->get();
            if(count($check) > 0){
                $record->getStatusMobile = 'BC';
            }
        }
        if($request->get('department_id')){
            $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get(),$request->get('department_id'));
        }else{
            $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get());
        }
        if($request->get('status')){
            $status_html = \App\Helpers\StringHelper::getSelectStatusProject(\App\Project::Progress_arr,$request->get('status'));
        }else{
            $status_html = \App\Helpers\StringHelper::getSelectStatusProject(\App\Project::Progress_arr);
        }
        if($request->get('level')){
            if($request->get('level') == 'KCD'){
                $level_html = 'Không cấp độ';
            }else{
                $level_html = 'Không cấp độ';
            }
            $level_html .= \App\Helpers\StringHelper::getSelectHtml(\App\Level::get(),$request->get('level'));
        }else{
            $level_html = 'Không cấp độ';
            $level_html .= \App\Helpers\StringHelper::getSelectHtml(\App\Level::get());  
        }
        return response()->json(array('success'=>true,'records'=>$records));
    }

    public function projectIndex(Request $request){
        if(isset($request['keyword'])){
            $records = $this->projectRepo->fillterMobile($request['keyword']);
            $keyword = $request['keyword'];
        }else{
            $records = $this->projectRepo->getIndex(15);
            $keyword='';
        }
        foreach($records as $key=>$record){
            $logapproved = \App\LogApproved::where('project_id',$record->id)->whereIn('status',[0,2])->orderBy('progress','DESC')->first();
            if($logapproved){
                if($logapproved->status == 2){
                    switch ($logapproved->progress){
                        case 1: 
                            $record->getStatusMobile = 'ĐN';
                            break;
                        case 2: 
                            $record->getStatusMobile = 'D1';
                            break;
                        case 3: 
                            $record->getStatusMobile = 'D2';
                            break;
                        case 4: 
                            $record->getStatusMobile = 'GV';
                            break;
                        case 5: 
                            $record->getStatusMobile = 'BC';
                            break;
                        case 6: 
                            $record->getStatusMobile = 'HT';
                            break;
                    }
                }else{
                    $record->getStatusMobile = 'TV';
                }
            }else{
                $record->getStatusMobile = 'N';
            }
        }
        return response()->json(array('success'=>true,'records'=>$records,'keyword'=>$keyword));

    }
    public function projectView(Request $request){
        $token = $request->bearerToken();
        $input = $request->all();
        $id = $input['id'];
        $records = $this->projectRepo->find($id);
        $member = $this->memberRepo->find($records->member_id);
        $records->member_html = \App\Helpers\StringHelper::getSelectMemberOptions($this->memberRepo->getAll());
        $records->logapproved = \App\LogApproved::where('project_id',$id)->whereIn('status',[0,2])->orderBy('progress','DESC')->first();
        $records->member_approved = \App\LogApproved::where('project_id',$id)->where('status',1)->first();
        $records->member_approved_html = \App\Helpers\StringHelper::getSelectMemberApprovedOptions(\App\Member::where('level',\App\Member::LEVEL_D2)->where('is_deleted',0)->get());
        if($records->logapproved){
            $check = \App\LogApproved::where('project_id',$id)->where('status',1)->where('progress',5)->get();
            if(count($check) == 0){
                $records->progress = $records->logapproved->progress;
            }else{
                $records->progress = 4;
            }
        }else{
            $records->progress = 0;
        }
        $records->member_project = \App\MemberProject::where('member_id',\Auth::guard('member')->user()->id)->where('project_id',$records->id)->first();
        $records->list_member_project = \App\MemberProject::where('project_id',$records->id)->where('status',1)->get();
        $records->level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->getAll());
        $image_before = [];
        $image_after = [];
        if($records->before_images){
            foreach($records->before_images as $key=>$val){
                $image_before[] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . ':8000/'.$val->link;
            }
        }
        if($records->after_images){
            foreach($records->after_images as $key=>$val){
                $image_after[] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . ':8000/'.$val->link;
            }
        }

        return response()->json(array('success'=>true,'records'=>$records,'image_before'=>$image_before,'image_after'=>$image_after));
    }

    public function projectMemberLevel(Request $request){
        $input = $request->all();

        $member_level = \App\Member::where('id',$input['id'])->first();
        if($member_level->level == 2){
            $member_approved = \App\Helpers\StringHelper::getSelectMemberApprovedApi(\App\Member::where('level',3)->where('is_deleted',0)->get());
        }else{
            $member_approved = \App\Helpers\StringHelper::getSelectMemberApprovedApi(\App\Member::whereIn('level',[2,3])->where('is_deleted',0)->get());
        }
        return response()->json(array('success'=>true,'member_approved'=>$member_approved, 'member_level'=>$member_level->level));
    }
    public function projectStore(Request $request){
        $input = $request->all();
        $token = $request->bearerToken();
        $member = $this->memberRepo->getByToken($token);
        if(is_null($member)){
            return response()->json(array('success'=>false));
        }
        $input['sort_date'] = date('Y-m-d H:i:s');
        if(isset($input['member_id'])){
            $member = $this->memberRepo->find($input['member_id']);
        }else{
            $input['member_id'] = $member->id;
        }
        if(isset($input['draft'])){
            $input['status'] = 0;
        }else{
            $input['status'] = $member->level;
        }
        $project = $this->projectRepo->create($input);
        if($input['status'] == \App\Project::STATUS_ACTIVE){
            $this->projectRepo->update(['approved_at'=>date('Y-m-d h:i:s'),'sort_date'=>date('Y-m-d h:i:s')],$project->id);
        }
        $log['status']= 2;
        $log['member_id']= $member->id;
        $log['project_id'] = $project->id;
        $log['level'] = $member->level;
        $log['progress'] = 1;
        $this->logapprovedRepo->create($log);
        if($member->level == \App\Member::LEVEL_D1){
             $log['progress'] = 2;
             $this->logapprovedRepo->create($log);
        }
        if($member->level == \App\Member::LEVEL_D2){
             $log_arr = ['2','3','6'];
                if($project->pattern == 1){
                    foreach($log_arr as $val){
                        $log['progress'] = $val;
                        $this->logapprovedRepo->create($log);
                    }
                }else{
                    foreach($log_arr as $val){
                        if($val != 6){
                            $log['progress'] = $val;
                            $this->logapprovedRepo->create($log);
                        }
                    }
                    $log_assign['member_id']= $member->id;
                    $log_assign['project_id'] = $project->id;
                    $log_assign['level'] = $member->level;
                    $log_assign['progress'] = 4;
                    $log_assign['status'] = 1;
                    $this->logapprovedRepo->create($log_assign);
                }
        }
        if($project->status > 0 && $project->status < \App\Project::STATUS_ACTIVE){
            $member_approved = $this->memberRepo->find($input['member_approved_id']);
            $log['status']= 1;
            $log['member_id']= $member_approved->id;
            $log['level'] = $member_approved->level;
            $log['progress'] = $member_approved->level;
            $this->logapprovedRepo->create($log);
            $this->memberRepo->NotificateMember($input['member_approved_id'], ['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
        }
        $targetDirFile = 'upload/files/';
        $allowTypesFile = array('pdf', 'docx','doc','xls','xlsx','pptx');
        if(count($input['files_before']) > 0){
            foreach($input['files_before'] as $key=>$val){
                $fileName = basename($val['name']);
                if(strlen($fileName)!=0){
                    $fileType = pathinfo($fileName);
                    if(in_array($fileType['extension'], $allowTypesFile)){
                        $link = $targetDirFile.time().'_'.$fileName;
                        move_uploaded_file($val['uri'],$link);
                        $file['link'] = $link;
                        $file['size'] =  number_format($val['size'] / 1048576,1);
                        $file['name'] = $fileName;
                        $file['type'] = \App\File::TYPE_FILE_PROJECT_BEFORE;
                        $file['format'] = \App\File::FORMAT_FILE;
                        $files = $this->fileRepo->create($file);
                        $this->fileRepo->update(['relationship_id'=>$project->id],$files->id);
                    }
                }
            }
        }
        if(count($input['files_after']) > 0){
            foreach($input['files_after'] as $key=>$val){
                $fileName = basename($val['name']);
                if(strlen($fileName)!=0){
                    $fileType = pathinfo($fileName);
                    if(in_array($fileType['extension'], $allowTypesFile)){
                        $link = $targetDirFile.time().'_'.$fileName;
                        move_uploaded_file($val['uri'],$link);
                        $file['link'] = $link;
                        $file['size'] =  number_format($val['size'] / 1048576,1);
                        $file['name'] = $fileName;
                        $file['type'] = \App\File::TYPE_FILE_PROJECT_BEFORE;
                        $file['format'] = \App\File::FORMAT_FILE;
                        $files = $this->fileRepo->create($file);
                        $this->fileRepo->update(['relationship_id'=>$project->id],$files->id);
                    }
                }
            }
        }
        $targetDir ='upload/images/';
        $allowTypesImage = array('jpg', 'png','jpeg','gif','PNG','JPG');
        if(count($input['image_before']) > 0){
            foreach($input['image_before'] as $key=>$val){
                $fileName = basename($val['name']);
                if(strlen($fileName)!=0){
                    $fileType = pathinfo($fileName);
                    if(in_array($fileType['extension'], $allowTypesImage)){
                        $link = $targetDir.time().'_'.$fileName;
                        move_uploaded_file($val['uri'],$link);
                        $file['link'] = $link;
                        $file['size'] = number_format($val['size'] / 1048576,1);
                        $file['name'] = $fileName;
                        $file['type'] = \App\File::TYPE_IMAGE_PROJECT_BEFORE;
                        $file['format'] = \App\File::FORMAT_IMAGE;
                        $image = $this->fileRepo->create($file);
                        $this->fileRepo->update(['relationship_id'=>$project->id],$image->id);
                    }
                }
            }
        }
        if(count($input['image_after']) > 0){
            foreach($input['image_after'] as $key=>$val){
                $fileName = basename($val['name']);
                if(strlen($fileName)!=0){
                    $fileType = pathinfo($fileName);
                    if(in_array($fileType['extension'], $allowTypesImage)){
                        $link = $targetDir.time().'_'.$fileName;
                        move_uploaded_file($val['uri'],$link);
                        $file['link'] = $link;
                        $file['size'] = number_format($val['size'] / 1048576,1);
                        $file['name'] = $fileName;
                        $file['type'] = \App\File::TYPE_IMAGE_PROJECT_AFTER;
                        $file['format'] = \App\File::FORMAT_IMAGE;
                        $image = $this->fileRepo->create($file);
                        $this->fileRepo->update(['relationship_id'=>$project->id],$image->id);
                    }
                }
            }
        }
        if ($project->id) {
            return response()->json(array('success'=>true,'msg'=>'Thêm mới thành công'));
        } else {
            return response()->json(array('success'=>false,'msg'=>'Thêm mới không thành công'));
        }
    }
}
