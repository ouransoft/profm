<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\NotificationScheduleRepository;
use Stevebauman\Purify\Facades\Purify;

class NotificationController extends Controller
{
    public function __construct(NotificationScheduleRepository $notiRepo) {
        $this->notiRepo = $notiRepo;
    }
    public function loadList(Request $request){
        $html = '<tr valign="top">
                    <th nowrap="">
                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Người gửi</font></font></div>
                    </th>
                    <th nowrap="">'.trans('base.Content').'</th>
                    <th nowrap="">'.trans('base.Created_at').'</th>
                 </tr>';
        if($request->get('status') == 0){
            foreach(\Auth::guard('member')->user()->unreadNotifications as $key=>$val){
                $html .='<tr valign="top">
                            <td nowrap="" style="padding-left:0px"><span class="selectlist_user_grn"></span>'.$val->data['full_name'].'</th>
                            <td nowrap="">
                                <a class="seen-notification" data-id="'.$val->id.'" data-link="'.$val->data['link'].'" href="javascript:void(0)">'.$val->data['content'].'</a>
                            </td>
                            <td nowrap="">'.date('d/m',strtotime($val->created_at)).' ('.$val->data['time'].')</th>
                        </tr>';
            }
        }else{
            foreach(\Auth::guard('member')->user()->Notifications->where('read_at','<>',NULL) as $key=>$val){
                $html .='<tr valign="top">
                            <td nowrap="" style="padding-left:0px"><span class="selectlist_user_grn"></span>'.$val->data['full_name'].'</th>
                            <td nowrap="">
                                <a class="seen-notification" data-id="'.$val->id.'" data-link="'.$val->data['link'].'" href="javascript:void(0)">'.$val->data['content'].'</a>
                            </td>
                            <td nowrap="">'.date('d/m',strtotime($val->created_at)).' ('.$val->data['time'].')</th>
                        </tr>';
            }
        }
        return response()->json(array('html'=>$html));
    }
}
