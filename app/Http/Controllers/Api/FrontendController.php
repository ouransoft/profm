<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\StringHelper;
use Illuminate\Support\Facades\Auth;
use Mail;
use Repositories\DepartmentRepository;
use Repositories\LevelRepository;
use Repositories\PositionRepository;
use App\Repositories\SlideRepository;
use App\Repositories\MemberRepository;
use Repositories\EquipmentRepository;
use Illuminate\Support\Facades\File; 
use Stevebauman\Purify\Facades\Purify;
use Repositories\FileRepository;

class FrontendController extends Controller {

    //
    public function __construct(FileRepository $fileRepo,EquipmentRepository $equipmentRepo,MemberRepository $memberRepo,SlideRepository $slideRepo,PositionRepository $positionRepo,DepartmentRepository $departmentRepo,LevelRepository $levelRepo) {
       $this->departmentRepo = $departmentRepo;
       $this->levelRepo = $levelRepo;
       $this->positionRepo = $positionRepo; 
       $this->slideRepo = $slideRepo;
       $this->memberRepo= $memberRepo;
       $this->equipmentRepo= $equipmentRepo;
       $this->fileRepo = $fileRepo;
    }
    public function addEquipment(Request $request){
        $input = Purify::clean($request->all());
        
        $this->equipmentRepo->create($input);
        return response()->json(array('success'=>true));
    }
    public function editEquipment(Request $request){
        $data = $this->equipmentRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateEquipment(Request $request){
        $input = Purify::clean($request->all());
        $this->equipmentRepo->update($input,$input['id']);
        return response()->json(array('success'=>true));
    }
    //
     public function addDepartment(Request $request){
        $input = Purify::clean($request->all());
        $check = $this->departmentRepo->checkName($input['name']);
        if ($check > 0) {
            return response()->json(array('success'=>false,'message'=>'Tên phòng ban đã tồn tại'));
        }else{
            
            $departpart = $this->departmentRepo->create($input);
            return response()->json(array('success'=>true,'id'=>$departpart->id,'level'=>$departpart->level));
        }
    }
    public function editDepartment(Request $request){
        $data = $this->departmentRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateDepartment(Request $request){
       $input = Purify::clean($request->all());
        $validator = \Validator::make($input, $this->departmentRepo->validateUpdate($input['id']));
        if ($validator->fails()) {
            return response()->json(array('success'=>false,'message'=>'Tên phòng ban đã tồn tại'));
        }else{
            $data = $this->departmentRepo->update($input,$input['id']);
            return response()->json(array('success'=>true));
        }
    }
    public function deleteChidrenDepartment(array $departments){
        $childs = [];
        foreach($departments as $department){
            if(count($department->children)){
                foreach($department->children as $key=>$val){
                    $childs[] = $val;
                }
                $department->children()->delete();
            }
        }
        if(count($childs) > 0)
             $this->deleteChidrenDepartment($childs);
        else
             return true;
   }
    public function deleteDepartment(Request $request){
        $record = \App\Department::find($request->get('id'));
        $array = [];
        if(count($record->children) > 0){
            foreach($record->children as $key=>$val){
                $array[] = $val;
            }
            $this::deleteChidrenDepartment($array);
            $record->children()->delete();
        }
        $record->delete();
        return response()->json(array('success'=>true));
    }
    //
     public function addLevel(Request $request){
        $input = Purify::clean($request->all());
        $validator = \Validator::make($input, $this->levelRepo->validateCreate());
        if ($validator->fails()) {
            return response()->json(array('success'=>false,'message'=>'Tên cấp độ đề án đã tồn tại'));
        }else{
            
            $level = $this->levelRepo->create($input);
            return response()->json(array('success'=>true));
        }
    }
    public function editLevel(Request $request){
        $data = $this->levelRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updateLevel(Request $request){
        $input = Purify::clean($request->all());
        $validator = \Validator::make($input, $this->levelRepo->validateUpdate($input['id']));
        if ($validator->fails()) {
            return response()->json(array('success'=>false,'message'=>'Tên cấp độ đề án đã tồn tại'));
        }else{
            $data = $this->levelRepo->update($input,$input['id']);
            return response()->json(array('success'=>true));
        }
    }
    //
    public function addPosition(Request $request){
        $input = Purify::clean($request->all());
        $check = $this->positionRepo->checkName($input['name']);
        if ($check > 0) {
            return response()->json(array('success'=>false,'message'=>'Tên chức vụ đã tồn tại'));
        }else{
            
            $position = $this->positionRepo->create($input);
            return response()->json(array('success'=>true));
        }
    }
    public function editPosition(Request $request){
        $data = $this->positionRepo->find($request->get('id'))->toArray();
        return response()->json(array('data'=>$data));
    }
    public function updatePosition(Request $request){
         $input = Purify::clean($request->all());
        $validator = \Validator::make($input, $this->positionRepo->validateUpdate($input['id']));
        if ($validator->fails()) {
            return response()->json(array('success'=>false,'message'=>'Tên chức vụ đã tồn tại'));
        }else{
            $data = $this->positionRepo->update($input,$input['id']);
            return response()->json(array('success'=>true));
        }
    }
    //
    public function addSlide(Request $request){
        $input = Purify::clean($request->all());
        $slide = $this->slideRepo->create($input);
        if(isset($input['image']) && $input['image'] != ''){
            $this->fileRepo->update(['relationship_id'=>$slide->id],$input['image']);
        }
        return response()->json(array('success'=>true));
    }
    public function editSlide(Request $request){
        $data = $this->slideRepo->find($request->get('id'));
        $data->image = $data->image()->first() ? $data->image()->first() : '';
        return response()->json(array('data'=>$data));
    }
    public function updateSlide(Request $request){
        $input = Purify::clean($request->all());
        $data = $this->slideRepo->update($input,$input['id']);
         $this->fileRepo->resetRelationshipID($input['id'],[\App\File::TYPE_SLIDE]);
         if(isset($input['image']) && $input['image'] != ''){
            $this->fileRepo->update(['relationship_id'=>$input['id']],$input['image']);
         }
         $this->fileRepo->deleteFileNull();
        return response()->json(array('success'=>true));
    }
    //
    public function upload(Request $request){
        $allowTypes = array('jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG','pdf', 'docx', 'xslx','doc','xls');
        $allowTypesImage = array('jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG');
        $allowTypesFile = array('pdf', 'docx', 'xslx','doc','xls');
        $data = [];
        foreach($_FILES['file']['name'] as $key=>$val){
            $fileName = basename($val);
            if(strlen($fileName)!=0){
                $fileType = pathinfo($fileName);
                if(in_array($fileType['extension'], $allowTypes)){
                    if(in_array($fileType['extension'], $allowTypesImage)){     
                        $targetDir = 'upload/images/';
                        $link = $targetDir.time().'_'.$fileName;              
                        $format = \App\File::FORMAT_IMAGE;
                    }elseif(in_array($fileType['extension'], $allowTypesFile)){
                        $targetDir = 'upload/files/';
                        $link = $targetDir.time().'_'.$fileName;
                        $format = \App\File::FORMAT_FILE;
                    }
                    move_uploaded_file($_FILES['file']['tmp_name'][$key],$link);
                    $file['link'] = $link;
                    $file['size'] = number_format($_FILES['file']['size'][$key] / 1048576,2);
                    $file['name'] = $fileName;
                    $file['type'] = $request->get('type');
                    $file['format'] = $format;
                    $data[] = $this->fileRepo->create($file);
                }else{
                    return response()->json(array('success' => 'false'));
                }
            }
        }
        return response()->json(array('success' => 'true','data'=>$data));
    }
    public function uploadImages(Request $request){
        $targetDir ='upload/images/';
        $allowTypesFile = array('jpg', 'png','jpeg','gif','PNG','JPG');
        $data = [];
        foreach($_FILES['file']['name'] as $key=>$val){
            $fileName = basename($val);
            if(strlen($fileName)!=0){
                $fileType = pathinfo($fileName);
                if(in_array($fileType['extension'], $allowTypesFile)){
                    $link = $targetDir.time().'_'.$fileName;
                    move_uploaded_file($_FILES['file']['tmp_name'][$key],$link);
                    $file['link'] = $link;
                    $file['size'] = number_format($_FILES['file']['size'][$key] / 1048576,1);
                    $file['name'] = $fileName;
                    $file['type'] = $request->get('type');
                    $file['format'] = \App\File::FORMAT_IMAGE;
                    $data[] = $this->fileRepo->create($file);
                }
            }
        }
        return response()->json(array('success' => 'true','data'=>$data));
    }

    public function uploadFiles(Request $request){
        $targetDir = 'upload/files/';
        $allowTypesFile = array('pdf', 'docx','doc','xls','xlsx','pptx');
        $data = [];
        foreach($_FILES['file']['name'] as $key=>$val){
            $fileName = basename($val);
            if(strlen($fileName)!=0){
                $fileType = pathinfo($fileName);
                if(in_array($fileType['extension'], $allowTypesFile)){
                    $link = $targetDir.time().'_'.$fileName;
                    move_uploaded_file($_FILES['file']['tmp_name'][$key],$link);
                    $file['link'] = $link;
                    $file['size'] = number_format($_FILES['file']['size'][$key] / 1048576,1);
                    $file['name'] = $fileName;
                    $file['type'] = $request->get('type');
                    $file['format'] = \App\File::FORMAT_FILE;
                    $data[] = $this->fileRepo->create($file);
                }
            }
        }
        return response()->json(array('success' => 'true','data'=>$data));
    }

    public function deleteFile(Request $request){
        $id = $request->get('id');
        $file = $this->fileRepo->find($id);
        File::delete("../".$file->link);
        $file->delete();
    }
    public function checkDeleteDepartment(Request $request){
        $id = $request->get('id');
        $department = $this->departmentRepo->find($id);
        if(count($department->member) > 0){
            $html = '';
            foreach($department->member as $key=>$member){
                $html .= '<p>- '.$member->full_name.' - '.$member->login_id.'</p>';
            }
            return response()->json(array('success' => 'false','html'=>$html));
        }else{
            return response()->json(array('success' => 'true'));
        }
    }
    public function checkDeletePosition(Request $request){
        $id = $request->get('id');
        $position = $this->positionRepo->find($id);
        if(count($position->member) > 0){
            $html = '';
            foreach($position->member as $key=>$member){
                $html .= '<p>- '.$member->full_name.' - '.$member->login_id.'</p>';
            }
            return response()->json(array('success' => 'false','html'=>$html));
        }else{
            return response()->json(array('success' => 'true'));
        }
    }
    public function addFile(Request $request){
        $targetDir = "/upload/files";
        $allowTypesFile = array('pdf', 'docx','doc','xls','xlsx');
        $data = [];
        foreach($_FILES['file']['name'] as $key=>$val){
            $fileName = basename($val);
            if(strlen($fileName)!=0){
                $fileType = pathinfo($fileName);
                if(in_array($fileType['extension'], $allowTypesFile)){
                    $link = 'upload/files/'.time().'_'.$fileName;
                    move_uploaded_file($_FILES['file']['tmp_name'][$key],$link);
                    $file['link'] = $link;
                    $file['size'] = \App\Helpers\StringHelper::ConvertSizeFile($_FILES['file']['size'][$key]);
                    $file['name'] = $fileName;
                    $file['type'] = $request->get('type');
                    $file['format'] = 2;
                    $data[] = $this->fileRepo->create($file);
                }
            }
        }
        return response()->json(array('success' => 'true','data'=>$data));
    }
    public function getInfoCompany(Request $request){
        if($request->key != '$2y$10$BN.5Y38A0B2ynxRxol4dYOoDgtUB9k1UMSbltFEaQ7tHnosUNF8.m'){
            return response()->json(array('success'=>false));
        }
        $count_member = \App\Member::count();
        $folder_size = 0;
        foreach( File::allFiles(public_path('upload/images')) as $file)
        {
            $folder_size += $file->getSize();
        }
        foreach( File::allFiles(public_path('upload/files')) as $file)
        {
            $folder_size += $file->getSize();
        }
        $total_todo = \App\ToDo::count();
        $total_schedule = \App\Schedule::count();
        $total_project = \App\Project::count();
        $folder_size = number_format($folder_size / 1048576,1);
        $config = \App\Config::first();
        $data = new \stdClass();
        $data->company_name = $config->company_name;
        $data->count = $count_member;
        $data->total_todo = $total_todo;
        $data->total_schedule = $total_schedule;
        $data->total_project = $total_project;
        $data->size = $folder_size;
        $data->link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME');
        $data->email = $config->email;
        $data->phone = $config->phone;
        return response()->json(array('data'=>$data));
    }
    public function getInfoDetail(Request $request){
        if($request->key != '$2y$10$BN.5Y38A0B2ynxRxol4dYOoDgtUB9k1UMSbltFEaQ7tHnosUNF8.m'){
            return response()->json(array('success'=>false));
        }
        $config = \App\Config::first();
        $start_date = date_create(date('Y-m-d',strtotime($config->created_at)));
        $now_date = date_create(date('Y-m-d'));
        $diff = date_diff($now_date,$start_date);
        $months = $diff->format("%m");
        if($months > 6){
            $months = 6;
        }else{
            $months =  $months;
        }
        $data = [];
        for($i=0;$i<$months;$i++){
            $object = new \stdClass();
            $month = date("m", strtotime("-".$i." months"));
            $year = date("Y", strtotime("-".$i." months"));
            $object->date = $month.'/'.$year;
            $object->count = \App\Member::whereMonth('created_at',$month)->whereYear('created_at',$year)->count();
            $object->total_todo = \App\ToDo::whereMonth('created_at',$month)->whereYear('created_at',$year)->count();
            $object->total_schedule = \App\Schedule::whereMonth('created_at',$month)->whereYear('created_at',$year)->count();
            $object->total_project = \App\Project::whereMonth('created_at',$month)->whereYear('created_at',$year)->count();
            $data[] = $object;
        }
        return response()->json(array('data'=>$data));
    }
}
