<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CommentRepository;
use Stevebauman\Purify\Facades\Purify;

class CommentController extends Controller
{
    public function __construct(CommentRepository $commentRepo){
        $this->commentRepo = $commentRepo;
    }
    public function send(Request $request){
        $input = Purify::clean($request->all());
        $input['member_id'] = \Auth::guard('member')->user()->id;
        $record = $this->commentRepo->create($input);

        //
        if (config('global.device') != 'pc') {
            $html = '<div class="item" id="comment'.$record->id.'">
                        <div class="avatar">
                            <img src="'.(is_null($record->member->avatar) ? asset('img/user30.png') : $record->member->avatar).'" alt="avatar" class="imaged w32 rounded">
                        </div>
                        <div class="in">
                            <div class="comment-header">
                                <h4 class="title">'.$record->member->full_name.'</h4>
                                <span class="time">'.\App\Helpers\StringHelper::time_ago($record->created_at).'</span>
                            </div>
                            <div class="text d-flex align-items-center justify-content-between">
                                <span>'.$record->comment.'</span>
                                <ion-icon name="ellipsis-horizontal" data-toggle="modal" data-target="#deleteCommentSheet'.$record->id.'" style="font-size: 18px"></ion-icon>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade action-sheet inset" id="deleteCommentSheet'.$record->id.'" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <ul class="action-button-list">
                                        <li>
                                            <a data-comment_id="'.$record->id.'" href="javascript:void(0)" class="btn btn-list delete-comment" data-dismiss="modal">
                                                <span class="text-danger">Xóa tin nhắn</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>';
        }else{
            $html = '<div class="comment" id="comment'.$record->id.'">
                        <div class="ui icon basic tiny buttons hide"><a class="ui button delete-comment" data-comment_id="'.$record->id.'" href="javascript:void(0)"><i class="icon-cross"></i></a></div>
                        <a class="avatar"><img class="ui avatar image" src="'.(is_null($record->member->avatar) ? asset('img/user30.png') : $record->member->avatar).'"></a>
                        <div class="content">
                            <a class="author">'.$record->member->full_name.'</a>
                            <div class="metadata">
                                <div class="date">'.\App\Helpers\StringHelper::time_ago($record->created_at).'</div>
                            </div>
                            <div class="text">'.$record->comment.'</div>
                            <div class="extra images"></div>
                        </div>
                    </div>';
            }
        //
        return response()->json(array('success' => 'true','html'=>$html));
    }
    public function delete(Request $request){
        $input = Purify::clean($request->all());
        $comment = $this->commentRepo->find($input['comment_id']);
        if(\Auth::guard('member')->user()->id == $comment->member_id){
            $this->commentRepo->delete($input['comment_id']);
        }
        return response()->json(array('success' => 'true','id'=>'comment'.$input['comment_id']));
    }
}
