<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ToDoRepository;
use App\Repositories\MemberTodoRepository;
use App\Repositories\MemberRepository;
use Carbon\Carbon;
use Stevebauman\Purify\Facades\Purify;

class ToDoController extends Controller
{
    public function __construct(ToDoRepository $todoRepo,MemberTodoRepository $membertodoRepo,MemberRepository $memberRepo) {
        $this->todoRepo = $todoRepo;
        $this->membertodoRepo = $membertodoRepo;
        $this->memberRepo = $memberRepo;
    }
    public function loadTodo(Request $request){
        $html = '<tr valign="top">
                    <th nowrap="">
                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tiêu đề</font></font></div>
                    </th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trạng thái</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Deadline</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Độ ưu tiên</font></font></th>
                 </tr>';
        $todos = $this->todoRepo->getByStatus($request->get('status'));
        foreach($todos as $key=>$val){
            $html .='<tr class="linetwo">
                        <td><span class="nowrap-grn "><a href="'.route('frontend.todo.view',$val->id).'">'.$val->title.'</a></span></td>
                        <td nowrap=""><span class="badge badge-color'.$val->status.'">'.$val->nameStatus().'</span></td>
                        <td nowrap="">'.$val->end_date().'</td>
                        <td nowrap="">
                            <span class="badge badge-priority'.$val->priority.'">'.$val->namePriority().'</span>
                        </td>
                     </tr>';
        }
        if($request->get('status') == 0){
            $check_list_html = '<a class="complete-list-todo" href="javascript:void(0)" style="height:28px!important;">Complete</a>';
        }else{
            $check_list_html = '<a class="delete-list-todo" href="javascript:void(0)" style="height:28px!important;">Delete</a>';
        }
        return response()->json(array('html'=>$html,'check_list_html'=>$check_list_html));
    }
    public function completeList(Request $request){
        $this->todoRepo->updateList($request->get('id'));
        $todos = $this->todoRepo->getByStatus(0);
        $html = '<tr valign="top">
                    <th nowrap=""></th>
                    <th nowrap="">
                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">To-do</font></font></div>
                    </th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Status</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Due date</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Priority</font></font></th>
                 </tr>';
        foreach($todos as $key=>$val){
            $html .='<tr class="linetwo">
                        <td nowrap=""><input type="checkbox" name="id[]" id="id[]" class="" value="'.$val->id.'"></td>
                        <td><span class="nowrap-grn "><a href="'.route('frontend.todo.view',$val->id).'">'.$val->title.'</a></span></td>
                        <td nowrap=""><span class="badge badge-danger">'.$val->nameStatus().'</span></td>
                        <td nowrap=""> '.$val->end_date().' </td>
                        <td nowrap=""><span class="badge badge-danger">'.$val->namePriority().'</span></td>
                     </tr>';
        }
        return response()->json(array('html'=>$html));
    }
    public function deleteList(Request $request){
        $this->todoRepo->deleteList($request->get('id'));
        $todos = $this->todoRepo->getByStatus(1);
        $html = '<tr valign="top">
                    <th nowrap=""></th>
                    <th nowrap="">
                       <div style="padding-top: 2px;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">To-do</font></font></div>
                    </th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Category</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Due date</font></font></th>
                    <th nowrap=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Priority</font></font></th>
                 </tr>';
        foreach($todos as $key=>$val){
            $category = $val->category ? $val->category->title : '' ;
            $due_date = is_null($val->due_date) ? 'None' : $val->due_date();
            $html .='<tr class="linetwo">
                        <td nowrap=""><input type="checkbox" name="id[]" id="id[]" class="" value="'.$val->id.'"></td>
                        <td><span class="nowrap-grn "><a href="'.route('frontend.todo.view',$val->id).'">'.$val->title.'</a></span></td>
                        <td nowrap=""> '.$category.' </td>
                        <td nowrap=""> '.$due_date.' </td>
                        <td nowrap="">
                             <span class="rank_star_base_grn">';
            for($i = 0;$i<$val->priority;$i++){
                $html .='<span class="rank_star_grn"></span>';
            }
            $html .= '</span></td></tr>';
        }
        return response()->json(array('html'=>$html));
    }
    public function getSelectStatus(Request $request){
        $record = $this->todoRepo->find($request->get('id'));
        $status_html = \App\Helpers\StringHelper::getSelectProgress(\App\ToDo::PROGRESS_ARR,$record->member()->where('id',\Auth::guard('member')->user()->id)->first()->pivot->progress);
        return response()->json(array('html'=>$status_html));
    }
    public function addFile(Request $request){
        $targetDir = "/todolist/";
        $allowTypesFile = array('pdf', 'docx','doc','xls','jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG','xlsx');
        $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
        $input = $request->all();
        if (config('global.device') != 'pc') {
            $html ='';
            $i = 0;
            foreach($input as $key => $files){
                $fileName = basename($_FILES[$key]['name']);
                if(strlen($fileName)!=0){
                    $fileType = pathinfo($fileName);
                    // Check whether file type is valid
                    if(in_array($fileType['extension'], $allowTypesFile)){
                        move_uploaded_file($_FILES[$key]['tmp_name'],'todolist/'.$fileName);
                        $file['link'] = $targetDir . $fileName;
                        $file['size'] = \App\Helpers\StringHelper::ConvertSizeFile($_FILES[$key]['size']);
                        $file['name'] = $fileName;
                    }
                }
                $html .='<div class="custom-control custom-checkbox mb-1">
                            <input type="checkbox" class="custom-control-input" checked="checked" name="upload_fileids[]" id="customCheckb'.$i.'">
                            <label class="custom-control-label" for="customCheckb'.$i.'">'.$file['name'].' ('.$file['size'].')</label>
                        </div>';
                $i++;
            }
        }else{
            $html ='';
            foreach($input as $key => $files){
                $fileName = basename($_FILES[$key]['name']);
                if(strlen($fileName)!=0){
                    $fileType = pathinfo($fileName);
                    // Check whether file type is valid
                    if(in_array($fileType['extension'], $allowTypesFile)){
                        move_uploaded_file($_FILES[$key]['tmp_name'],'todolist/'.$fileName);
                        $file['link'] = $targetDir . $fileName;
                        $file['size'] = \App\Helpers\StringHelper::ConvertSizeFile($_FILES[$key]['size']);
                        $file['name'] = $fileName;
                    }
                }
                $html .='<tr>
                            <td>
                                <input type="checkbox" class="upload_checkbox js_upload_file" checked="checked" name="upload_fileids[]">
                            </td>
                            <td>'.$file['name'].' ('.$file['size'].')</td>
                        </tr>';
            }
        }
        return response()->json(array('success' => 'true','html'=>$html));
    }
    public function destroy(Request $request){
        $record = $this->todoRepo->find($request->get('id'));
        $record->delete();
        return response()->json(array('success' => 'true'));
    }
     public function destroyAll(){
        $this->todoRepo->deleteAll();
        return response()->json(array('success' => 'true'));
    }
    public function getChart(Request $request){
        $input = $request->all();
        foreach($input as $key=>$val){
            if(!is_null($val)){
                $input[$key] = Purify::clean($val);
            }
        }
        if($input['search'] == 'department'){
            $members = $this->memberRepo->getByDepartment([$request->get('id')]);
            if(count($members) > 0){
                $member_ids = $members->pluck('id')->toArray();
            }else{
                $member_ids = [];
            }
            $total_todo = $this->membertodoRepo->countTodo($member_ids,'','','',$input['time'],TRUE);
            $pattern0 = $this->membertodoRepo->countTodo($member_ids,2,0,1,$input['time'],TRUE);
            $pattern1 = $this->membertodoRepo->countTodo($member_ids,3,1,1,$input['time'],TRUE);
            $pattern2 = $this->membertodoRepo->countTodo($member_ids,3,2,1,$input['time'],TRUE);
            $pattern3 = $this->membertodoRepo->countTodo($member_ids,3,3,1,$input['time'],TRUE);
            if($total_todo == 0){
                $status_todo = [0,0,0,0];
            }else{
                $status_todo = [$pattern0,$pattern1,$pattern2,$pattern3];
            }
        }else{
            $total_todo = $this->membertodoRepo->countTodo([$input['id']],'','','',$input['time'],TRUE);
            if($total_todo == 0){
                $status_todo = [0,0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo([$input['id']],2,0,1,$input['time'],FALSE);
                $pattern1 = $this->membertodoRepo->countTodo([$input['id']],3,1,1,$input['time'],FALSE);
                $pattern2 = $this->membertodoRepo->countTodo([$input['id']],3,2,1,$input['time'],FALSE);
                $pattern3 = $this->membertodoRepo->countTodo([$input['id']],3,3,1,$input['time'],FALSE);
                $status_todo = [$pattern0,$pattern1,$pattern2,$pattern3];
            }
        }
        return response()->json(array('success' => 'true','status_todo'=>$status_todo));
    }
    public function getTodo(Request $request){
        $records = $this->todoRepo->getIndex($request);
        $final_html = '';
        foreach($records as $key => $record){
            $member_html = '';
            foreach($record->member as $key=>$val){
                if($val->pivot->join == 1){
                    $member_html .= '<span class="badge badge-primary">'.$val->full_name.'</span>';
                }
                elseif($val->pivot->join == 0){
                    $member_html .= '<span class="badge badge-secondary">'.$val->full_name.'</span>';
                }
                else{
                    $member_html .= '<button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="'.$val->pivot->reason.'" >'.$val->full_name.'</button>';
                }
            }

            $warning_html = '';
            if($record->confirm == 2){
                $warning_html = '<div class="tooltip"><i class="fas fa-question-circle"></i>
                                    <span class="tooltiptext">Người giao việc đánh giá chưa hoàn thành</span>
                                </div>';
            }

            $filecheck_html = '';
            if($record->check == true){
                $filecheck_html = '<a href="javascript:void(0)" class="change-status" data-id="'.$record->id.'"><i class="icon-file-check"></i></a>';
            }

            $form_html = '';
            $updateicon_html = '';
            if(\Auth::guard("member")->user()->id == $record->created_by){
                if($record->status != \App\Todo::STATUS_COMPLETE){
                    $updateicon_html = '<a href="'.route("frontend.todo.edit",$record->id).'"><i class="icon-pencil"></i></a>';
                }
                $form_html = '<form action="'.route("frontend.todo.destroy",["id" => $record->id]).'" method="POST" style="display: inline-block">
                                '.method_field("DELETE").'
                                '.csrf_field().'
                                <a title="'.trans("base.delete").'" class="delete text-danger" data-action="delete">
                                    <i class="icon-bin"></i>
                                </a>
                            </form>';
            }

            $creator_html = '';
            if($record->creator == true){
                $creator_html .= $record->creator->full_name;
            }
            $final_html .= ' <tr>
                            <td  class="middle">'.($key + 1).'</td>
                            <td  class="middle"><a href="'.route("frontend.todo.view",$record->id).'">'.$record->title.'</a></td>
                            <td  class="middle">
                                '.$member_html.'
                            </td>
                            <td class="middle">'.$creator_html.'</td>
                            <td class="middle">'.$record->end_date().'</td>
                            <td class="middle">
                                <span class="badge badge-color'.$record->status.'">'.$record->nameStatus().'</span>
                                '.$warning_html.'
                            </td>
                            <td class="middle text-center">
                                <div class="progress-circle progress-'.number_format($record->progress()).'"><span>'.number_format($record->progress()).'</span></div>
                            </td>
                            <td class="middle"><span class="badge badge-priority'.$record->priority.'">'.$record->namePriority().'</span></td>
                            <td>
                                '.$filecheck_html.'
                                '.$updateicon_html.'
                                '.$form_html.'
                            </td>
                        </tr>';
        }
        return response()->json(array('success' => 'true', 'result' => $final_html));
    }
    public function getStatistical(Request $request){
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');
        $lastQuarter = $now->quarter;
        if($request->get('search') == 'department'){
            $members = $this->memberRepo->getByDepartment([$request->get('department_id')]);
            if(count($members) > 0){
                $member_ids = $members->pluck('id')->toArray();
            }else{
                $member_ids = [];
            }
            $count_week = $this->membertodoRepo->countTodo($member_ids,'','',1,'week',TRUE);
            $count_month = $this->membertodoRepo->countTodo($member_ids,'','',1,'month',TRUE);
            $count_quarter = $this->membertodoRepo->countTodo($member_ids,'','',1,'quater',TRUE);
            $count_year = $this->membertodoRepo->countTodo($member_ids,'','',1,'year',TRUE);
            $list_todo = \App\Todo::rightjoin('member_todo','todo.id','=','member_todo.todo_id')->whereIn('todo.status',[1,2])->whereIn('member_todo.member_id',$member_ids)
                        ->select('todo.*')->orderBy('todo.created_at','DESC')->groupBy('todo.id')->get();
            if(($request->time == 'month' && $count_month == 0) || ($request->time == 'week' && $count_week == 0) || ($request->time == 'quater' && $count_quarter == 0) || ($request->time == 'year' && $count_year == 0)){
                $status_todo = [0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo($member_ids,2,0,1,$request->time,TRUE);
                $pattern1 = $this->membertodoRepo->countTodo($member_ids,3,1,1,$request->time,TRUE);
                $pattern2 = $this->membertodoRepo->countTodo($member_ids,3,2,1,$request->time,TRUE);
                $pattern3 = $this->membertodoRepo->countTodo($member_ids,3,3,1,$request->time,TRUE);
                $status_todo = [$pattern0,$pattern1, $pattern2,$pattern3];
            }
        }else{
            $member_id = $request->get('member_id');
            $member = $this->memberRepo->find($member_id);
            $count_week = $this->membertodoRepo->countTodo([$member_id],'','',1,'week',FALSE);
            $count_month = $this->membertodoRepo->countTodo([$member_id],'','',1,'month',FALSE);
            $count_quarter = $this->membertodoRepo->countTodo([$member_id],'','',1,'quater',FALSE);
            $count_year = $this->membertodoRepo->countTodo([$member_id],'','',1,'year',FALSE);
            if(($request->time == 'month' && $count_month == 0) || ($request->time == 'week' && $count_week == 0) || ($request->time == 'quater' && $count_quarter == 0) || ($request->time == 'year' && $count_year == 0)){
                $status_todo = [0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo([$member_id],2,0,1,$request->time,FALSE);
                $pattern1 = $this->membertodoRepo->countTodo([$member_id],3,1,1,$request->time,FALSE);
                $pattern2 = $this->membertodoRepo->countTodo([$member_id],3,2,1,$request->time,FALSE);
                $pattern3 = $this->membertodoRepo->countTodo([$member_id],3,3,1,$request->time,FALSE);
                $status_todo = [$pattern0,$pattern1, $pattern2,$pattern3];
            }
            $list_todo = \App\Todo::rightjoin('member_todo','todo.id','=','member_todo.todo_id')->whereIn('todo.status',[1,2])->where('member_todo.member_id',$member_id)
                        ->select('todo.*')->orderBy('todo.created_at','DESC')->groupBy('todo.id')->get();
        }
        $list_todo_html = '';
        $list_todo_mobile_html = '';
        foreach($list_todo as $key=>$todo){
            $member_html = '';
            foreach($todo->member as $k=>$val){
                if($val->pivot->join == 1){
                    $member_html .= ' <span class="badge badge-primary">'.$val->full_name.'</span> ';
                }elseif($val->pivot->join == 0){
                    $member_html .= ' <span class="badge badge-secondary">'.$val->full_name.'</span> ';
                }else{
                    $member_html .= ' <button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="'.$val->pivot->reason.'" >'.$val->full_name.'</button> ';
                }
            }
            $list_todo_mobile_html .= '<li>
                                        <a href="'.route('frontend.todo.view',$todo->id).'" class="item">
                                            <div class="in">
                                                <div>
                                                    '.$todo->title.'
                                                    <footer class="pt-1">
                                                        <ion-icon name="calendar-clear-outline" role="img" class="md hydrated" aria-label="calendar clear outline"></ion-icon> '.date('d/m/Y h:i',strtotime($todo->end_date)).'
                                                    </footer>
                                                </div>
                                            </div>
                                            <span class="badge badge-priority3">'.$todo->namePriority().'</span>
                                        </a>
                                    </li>';
            $list_todo_html .= '<tr role="row">
                                    <td class="sorting_1">'.($key+1).'</td>
                                    <td><a href="'.route('frontend.todo.view',$todo->id).'">'.$todo->title.'</a></td>
                                    <td class="flex-column h-100">
                                        '.$member_html.'
                                    </td>
                                    <td><span class="badge badge-color'.$todo->status.'">'.$todo->nameStatus().'</span></td>
                                    <td><div class="progress-circle progress-'.number_format($todo->progress()).'"><span>'.number_format($todo->progress()).'</span></div></td>
                                    <td><span class="badge badge-priority'.$todo->priority.'">'.$todo->namePriority().'</span></td>
                                </tr>';
        }
        return response()->json(array('success' => 'true','list_todo_mobile_html'=>$list_todo_mobile_html,'status_todo'=>$status_todo,'count_week'=>$count_week,'count_month'=>$count_month,'count_quater'=>$count_quarter,'count_year'=>$count_year,'list_todo_html'=>$list_todo_html));
    }
     public function getList(Request $request){
        $input = $request->all();
        if($input['pattern'] == 0){
            $input['status'] = 2;
        }else{
            $input['status'] = 3;
        }
        if($input['search'] == 'department'){
            $members = $this->memberRepo->getByDepartment([$request->get('id')]);
            if(count($members) > 0){
                $member_ids = $members->pluck('id')->toArray();
            }else{
                $member_ids = [];
            }
            $list_todo = $this->todoRepo->getListByPattern($member_ids,$input['status'],$input['pattern'],$input['time'],TRUE);
        }else{
            $list_todo = $this->todoRepo->getListByPattern([$input['id']],$input['status'],$input['pattern'],$input['time'],FALSE);
        }
        $list_todo_html = '';
        foreach($list_todo as $key=>$todo){
            $member_html = '';
            foreach($todo->member as $k=>$val){
                if($val->pivot->join == 1){
                    $member_html .= ' <span class="badge badge-primary">'.$val->full_name.'</span> ';
                }elseif($val->pivot->join == 0){
                    $member_html .= ' <span class="badge badge-secondary">'.$val->full_name.'</span> ';
                }else{
                    $member_html .= ' <button style="height: 24px;" type="button" class="badge badge-danger popover-dismiss" data-toggle="popover" title="Lí do không tham gia" data-content="'.$val->pivot->reason.'" >'.$val->full_name.'</button> ';
                }
            }
            $list_todo_html .= '<tr role="row">
                                    <td class="sorting_1">'.($key+1).'</td>
                                    <td><a href="'.route('frontend.todo.view',$todo->id).'">'.$todo->title.'</a></td>
                                    <td class="flex-column h-100">
                                        '.$member_html.'
                                    </td>
                                    <td><span class="badge badge-color'.$todo->status.'">'.$todo->nameStatus().'</span></td>
                                    <td><div class="progress-circle progress-'.number_format($todo->progress()).'"><span>'.number_format($todo->progress()).'</span></div></td>
                                    <td><span class="badge badge-priority'.$todo->priority.'">'.$todo->namePriority().'</span></td>
                                </tr>';
        }
        $title = 'Các công việc ';
        if($input['pattern'] == 0){
            $title .='chưa hoàn thành';
        }elseif($input['pattern'] == 1){
            $title .=' hoàn thành trước hạn ';
        }elseif($input['pattern'] == 2){
            $title .=' hoàn thành đúng hạn ';
        }else{
            $title .=' hoàn thành sau hạn ';
        }
        switch ($input['time']){
            case 'week':
                $title .=' trong tuần';
                break;
            case 'month':
                $title .=' trong tháng';
                break;
            case 'quater':
                $title .=' trong quý';
                break;
            case 'year':
                $title .=' trong năm';
                break;
        }
        return response()->json(array('success' => 'true','list_todo_html'=>$list_todo_html,'title'=>$title));
    }
}
