<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ToDoRepository;
use App\Repositories\MemberRepository;
use App\Repositories\CommentRepository;
use App\Repositories\MemberTodoRepository;
use Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Jobs\SendMail;
use Stevebauman\Purify\Facades\Purify;
use Repositories\FileRepository;
use Illuminate\Support\Facades\File; 
use Repositories\DepartmentRepository;

class ToDoController extends Controller
{
    public function __construct(DepartmentRepository $departmentRepo,FileRepository $fileRepo,MemberTodoRepository $membertodoRepo,ToDoRepository $todoRepo,MemberRepository $memberRepo,CommentRepository $commentRepo){
        $this->todoRepo = $todoRepo;
        $this->memberRepo = $memberRepo;
        $this->commentRepo = $commentRepo;
        $this->membertodoRepo = $membertodoRepo;
        $this->fileRepo = $fileRepo;
        $this->departmentRepo = $departmentRepo;
    }
    public function index(Request $request) {
        $records = $this->todoRepo->getIndex($request);
        foreach($records as $key=>$record){
            if($record->status == 2 && \Auth::guard('member')->user()->membertodo()->where('todo_id',$record->id)->where('join',1)->count() > 0 && \Auth::guard('member')->user()->membertodo()->where('todo_id',$record->id)->where('join',1)->first()->progress < 100){
                $record->check = true;
            }else{
                $record->check = false;
            }
        }
        $historys = $this->todoRepo->getHistory($request);
        if($request->get('priority')){
            $priority_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::PRIORITY_ARR,$request->get('priority'));
        }else{
            $priority_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::PRIORITY_ARR);
        }

        //mobile
        if($request->get('member_id')){
            $member_id = $request->get('member_id');
        }else{
            $member_id = \Auth::guard('member')->user()->id;
        }
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');
        $lastQuarter = $now->quarter;
        if($request->get('search') == 'department'){
            $members = $this->memberRepo->getByDepartment([$request->get('department_id')]);
            if(count($members) > 0){
                $member_ids = $members->pluck('id')->toArray();
            }else{
                $member_ids = [];
            }
            $count_week = $this->membertodoRepo->countTodo($member_ids,'','',1,'week',TRUE);
            $count_month = $this->membertodoRepo->countTodo($member_ids,'','',1,'month',TRUE);
            $count_quarter = $this->membertodoRepo->countTodo($member_ids,'','',1,'quater',TRUE);
            $count_year = $this->membertodoRepo->countTodo($member_ids,'','',1,'year',TRUE);
            $list_todo = \App\Todo::join('member_todo','todo.id','=','member_todo.todo_id')->whereIn('todo.status',[1,2])->whereIn('member_todo.member_id',$member_ids)
                        ->select('todo.*')->groupBy('todo.id')->orderBy('todo.end_date','ASC')->pluck('todo.id');
            if($count_month == 0){
                $status_todo = [0,0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo($member_ids,2,0,1,'month',TRUE);
                $pattern1 = $this->membertodoRepo->countTodo($member_ids,3,1,1,'month',TRUE);
                $pattern2 = $this->membertodoRepo->countTodo($member_ids,3,2,1,'month',TRUE);
                $pattern3 = $this->membertodoRepo->countTodo($member_ids,3,3,1,'month',TRUE);
                $status_todo = [$pattern0,$pattern1,$pattern2,$pattern3];
            }
        }else{
            $member = $this->memberRepo->find($member_id);
            $count_week = $this->membertodoRepo->countTodo([$member_id],'','',1,'week',FALSE);
            $count_month = $this->membertodoRepo->countTodo([$member_id],'','',1,'month',FALSE);
            $count_quarter = $this->membertodoRepo->countTodo([$member_id],'','',1,'quater',FALSE);
            $count_year = $this->membertodoRepo->countTodo([$member_id],'','',1,'year',FALSE);
            if($count_month == 0){
                $status_todo = [0,0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo([$member_id],2,0,1,'month',FALSE);
                $pattern1 = $this->membertodoRepo->countTodo([$member_id],3,1,1,'month',FALSE);
                $pattern2 = $this->membertodoRepo->countTodo([$member_id],3,2,1,'month',FALSE);
                $pattern3 = $this->membertodoRepo->countTodo([$member_id],3,3,1,'month',FALSE);
                $status_todo = [$pattern0,$pattern1,$pattern2,$pattern3];
            }
            $list_todo = \App\Todo::rightjoin('member_todo','todo.id','=','member_todo.todo_id')->whereIn('todo.status',[1,2])->where('member_todo.member_id',$member_id)
                        ->select('todo.*')->orderBy('todo.end_date','ASC')->groupBy('todo.id')->get();
        }
        $member_html = \App\Helpers\StringHelper::getSelectMemberOptions(\App\Member::where('is_deleted', 0)->get(),$member_id);
        if($request->get('department_id')){
            $department_html = \App\Helpers\StringHelper::getSelectOptions(\App\Department::where('level',\App\Department::max('level'))->get(),$request->get('department_id'));
        }else{
            $department_html = \App\Helpers\StringHelper::getSelectOptions(\App\Department::where('level',\App\Department::max('level'))->get());
        }
        // *mobile
        if (config('global.device') != 'pc') {
            return view('mobile/todo/index', compact('records','historys','priority_html','member_html','department_html','count_week','count_month','count_quarter','count_year','status_todo','list_todo'));
        }else{
            return view('frontend/todo/index', compact('records','priority_html'));
        }
    }
    public function history(Request $request) {
        $records = $this->todoRepo->getHistory($request);
        if($request->get('priority')){
            $priority_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::PRIORITY_ARR,$request->get('priority'));
        }else{
            $priority_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::PRIORITY_ARR);
        }
        if (config('global.device') != 'pc') {
            return view('mobile/todo/history', compact('records'));
        }else{
            return view('frontend/todo/history', compact('records','priority_html'));
        }
    }
    public function statistical(Request $request) {
        if($request->get('member_id')){
            $member_id = $request->get('member_id');
        }else{
            $member_id = \Auth::guard('member')->user()->id;
        }
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');
        $lastQuarter = $now->quarter;
        if($request->get('search') == 'department'){
            $members = $this->memberRepo->getByDepartment([$request->get('department_id')]);
            if(count($members) > 0){
                $member_ids = $members->pluck('id')->toArray();
            }else{
                $member_ids = [];
            }
            $count_week = $this->membertodoRepo->countTodo($member_ids,'','',1,'week',TRUE);
            $count_month = $this->membertodoRepo->countTodo($member_ids,'','',1,'month',TRUE);
            $count_quarter = $this->membertodoRepo->countTodo($member_ids,'','',1,'quater',TRUE);
            $count_year = $this->membertodoRepo->countTodo($member_ids,'','',1,'year',TRUE);
            $list_todo = \App\Todo::join('member_todo','todo.id','=','member_todo.todo_id')->whereIn('todo.status',[1,2])->whereIn('member_todo.member_id',$member_ids)
                        ->select('todo.*')->groupBy('todo.id')->orderBy('todo.end_date','ASC')->pluck('todo.id');
            if($count_month == 0){
                $status_todo = [0,0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo($member_ids,2,0,1,'month',TRUE);
                $pattern1 = $this->membertodoRepo->countTodo($member_ids,3,1,1,'month',TRUE);
                $pattern2 = $this->membertodoRepo->countTodo($member_ids,3,2,1,'month',TRUE);
                $pattern3 = $this->membertodoRepo->countTodo($member_ids,3,3,1,'month',TRUE);
                $status_todo = [($pattern0 * 100)/$count_month,($pattern1 * 100)/$count_month, ($pattern2 * 100)/$count_month,($pattern3 * 100)/$count_month];
            }
        }else{
            $member = $this->memberRepo->find($member_id);
            $count_week = $this->membertodoRepo->countTodo([$member_id],'','',1,'week',FALSE);
            $count_month = $this->membertodoRepo->countTodo([$member_id],'','',1,'month',FALSE);
            $count_quarter = $this->membertodoRepo->countTodo([$member_id],'','',1,'quater',FALSE);
            $count_year = $this->membertodoRepo->countTodo([$member_id],'','',1,'year',FALSE);
            if($count_month == 0){
                $status_todo = [0,0,0,0];
            }else{
                $pattern0 = $this->membertodoRepo->countTodo([$member_id],2,0,1,'month',FALSE);
                $pattern1 = $this->membertodoRepo->countTodo([$member_id],3,1,1,'month',FALSE);
                $pattern2 = $this->membertodoRepo->countTodo([$member_id],3,2,1,'month',FALSE);
                $pattern3 = $this->membertodoRepo->countTodo([$member_id],3,3,1,'month',FALSE);
                $status_todo = [$pattern0,$pattern1,$pattern2,$pattern3];
            }
            $list_todo = \App\Todo::rightjoin('member_todo','todo.id','=','member_todo.todo_id')->whereIn('todo.status',[1,2])->where('member_todo.member_id',$member_id)
                        ->select('todo.*')->orderBy('todo.end_date','ASC')->groupBy('todo.id')->get();
        }
        $department = \App\Department::find(\Auth::guard('member')->user()->department_id);
        $department_ids = $department->getChildrenIds();
        $member_html = \App\Helpers\StringHelper::getSelectMemberOptions($this->memberRepo->getByDepartmentMe($department_ids,\Auth::guard('member')->user()->id),$member_id);
        if($request->get('department_id')){
            $department_html = \App\Helpers\StringHelper::getSelectOptions(\App\Department::where('level',\App\Department::max('level'))->get(),$request->get('department_id'));
        }else{
            $department_html = \App\Helpers\StringHelper::getSelectOptions(\App\Department::whereIn('id',$department_ids)->where('level',\App\Department::max('level'))->get());
        }
        return view('frontend/todo/statistical',compact('member_html','department_html','count_week','count_month','count_quarter','count_year','status_todo','list_todo'));
    }
    public function create() {
        $members = $this->memberRepo->getAll();
        $member_html = \App\Helpers\StringHelper::getSelectMemberOptions($members,[\Auth::guard('member')->user()->id]);
        $member_html_mobile = \App\Helpers\StringHelper::getSelectMemberOptionsMobile($members,[\Auth::guard('member')->user()->id]);
        $status_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::STATUS_ARR);
        $priority_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::PRIORITY_ARR,3);
        $priority_html_mobile = \App\Helpers\StringHelper::getSelectHtmlMobile(\App\ToDo::PRIORITY_ARR,3);
        if (config('global.device') != 'pc') {
            return view('mobile/todo/create',compact('priority_html_mobile','status_html','member_html_mobile'));
        }else{
            return view('frontend/todo/create',compact('priority_html','status_html','member_html'));
        }
    }
    public function store(Request $request) {
        $input = $request->all();
        foreach($input as $key=>$val){
            if(!is_null($val)){
                $input[$key] = Purify::clean($val);
            }
        }
        $validator = \Validator::make($input, $this->todoRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if (config('global.device') != 'pc') {
            $input['start_date'] = date('Y-m-d H:i',strtotime($input['start_date_submit'] .' '. $input['start_time']));
            $input['end_date'] = date('Y-m-d H:i',strtotime($input['end_date_submit'] .' '. $input['end_time']));
        }else{
            $input['start_date'] = date('Y-m-d H:i',strtotime($input['start_date'] .' '. $input['start_time']));
            $input['end_date'] = date('Y-m-d H:i',strtotime($input['end_date'] .' '. $input['end_time']));
        }
        $input['created_by'] = \Auth::guard('member')->user()->id;
        if(is_null($input['priority'])){
            $input['priority'] = 3;
        }
        if(isset($input['file'])){
            $input['file'] = implode(',',$input['file']);
        }
        $todo = $this->todoRepo->create($input);
        if(isset($input['member_id']) && $input['member_id'] > 0){
            $todo->member()->attach($input['member_id']);
            if(in_array(\Auth::guard('member')->user()->id,$input['member_id'])){
                $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$todo->id,['join'=>1,'status'=>1]);
                   $this::checkStatus($todo->id);
            }
            foreach($input['member_id'] as $val){
                if($val != \Auth::guard('member')->user()->id){
                    $this->memberRepo->NotificateMember($val,['content'=>'Bạn được giao 1 công việc','link'=>route('frontend.todo.view',$todo->id)]);
                    $member = $this->memberRepo->find($val);
                    if(!is_null($member->email)){
                        $email = $member->email;
                        $name = \Auth::guard('member')->user()->full_name;
                        Mail::send('mail', array('title'=>$todo->title,'content'=>$todo->content,'link'=>(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/todo/view/'.$todo->id), function($message) use ($email,$name){
                            $message->from('contact@ouransoft.vn', 'Ouransoft');
                            $message->to($email, 'Ouransoft')->subject('Thông báo giao việc từ '.$name);
                        });
                    }
                }
            }
        }
        // Thêm file vào project
        if(isset($input['files']) && count($input['files']) > 0){
            foreach($input['files'] as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$todo->id],$val);
            }
        }
        if ($todo) {
            return  redirect()->route('frontend.todo.index')->with('success','Tạo mới thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Tạo mới thất bại');
        }
    }
    public function view($id){
        $record = $this->todoRepo->find($id);
        if($record){
            $comments = $this->commentRepo->getByToDo($id);
            $membertodo = $this->membertodoRepo->getRecord(\Auth::guard('member')->user()->id,$id);
            if($record->status == 2 && \Auth::guard('member')->user()->membertodo()->where('todo_id',$record->id)->where('join',1)->count() > 0 && \Auth::guard('member')->user()->membertodo()->where('todo_id',$record->id)->where('join',1)->first()->progress < 100){
                $check = true;
            }else{
                $check = false;
            }
            if (config('global.device') != 'pc') {
                return view('mobile/todo/view',compact('record','comments','membertodo','check'));
            }else{
                return view('frontend/todo/view',compact('record','comments','membertodo','check'));
            }
        }else{
            if (config('global.device') != 'pc') {
                return view('mobile/todo/view_not_todo');
            }else{
                return view('frontend/todo/view_not_todo');
            }
        }
    }
    public function edit($id){
        $record = $this->todoRepo->find($id);
        $members = $this->memberRepo->getAll();
        if(count($record->member) > 0){
            $member_html = \App\Helpers\StringHelper::getSelectMemberOptions($members,$record->member()->pluck('id')->toArray());
        }else{
            $member_html = \App\Helpers\StringHelper::getSelectMemberOptions($members);
        }
        if(count($record->member) > 0){
            $member_html_mobile = \App\Helpers\StringHelper::getSelectMemberOptionsMobile($members,$record->member()->pluck('id')->toArray());
        }else{
            $member_html_mobile = \App\Helpers\StringHelper::getSelectMemberOptionsMobile($members);
        }
        $priority_html_mobile = \App\Helpers\StringHelper::getSelectHtmlMobile(\App\ToDo::PRIORITY_ARR);
        $status_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::STATUS_ARR,$record->status);
        $priority_html = \App\Helpers\StringHelper::getSelectHtml(\App\ToDo::PRIORITY_ARR,$record->priority);
        if (config('global.device') != 'pc') {
            return view('mobile/todo/edit',compact('record','priority_html_mobile','status_html','member_html_mobile'));
        }else{
            return view('frontend/todo/edit',compact('record','priority_html','status_html','member_html'));
        }
    }
    public function update(Request $request,$id){
        $input = $request->all();
        foreach($input as $key=>$val){
            if(!is_null($val)){
                $input[$key] = Purify::clean($val);
            }
        }
        if (config('global.device') != 'pc') {
            $input['start_date'] = date('Y-m-d H:i',strtotime($input['start_date_submit'] .' '. $input['start_time']));
            $input['end_date'] = date('Y-m-d H:i',strtotime($input['end_date_submit'] .' '. $input['end_time']));
        }else{
            $input['start_date'] = date('Y-m-d H:i',strtotime($input['start_date'] .' '. $input['start_time']));
            $input['end_date'] = date('Y-m-d H:i',strtotime($input['end_date'] .' '. $input['end_time']));
        }
        $todo = $this->todoRepo->find($id);
        if(isset($input['status']) && $input['status'] == \App\ToDo::STATUS_COMPLETE){
            foreach($todo->member as $key=>$val){
                $this->membertodoRepo->updateRecord($val->id,$id,['progress'=>100]);
            }
        }
        if(isset($input['member_id']) && $input['member_id'] > 0){
            $todo->member()->sync($input['member_id']);
        }
        $this::checkStatus($todo->id);
        // Thêm file vào project
        $this->fileRepo->resetRelationshipID($todo->id,[\App\File::TYPE_TODO]);
        if(isset($input['files']) && count($input['files']) > 0){
            foreach($input['files'] as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$todo->id],$val);
            }
        }
        $this->fileRepo->deleteFileNull();
        $this->todoRepo->update($input,$id);
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.todo.index')->with('success','Cập nhật thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Cập nhật thất bại');
        }
    }
    public function updateStatus(Request $request){
        $input = $request->all();
        foreach($input['id'] as $key=>$val){
            $input['status'] = 1;
            $input['com_date'] = date('Y-m-d');
            $this->todoRepo->update($input,$val);
        }
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.todo.index')->with('success','Cập nhật thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Cập nhật thất bại');
        }
    }
    public function destroy($id){
        $record = $this->todoRepo->find($id);
        $record->membertodo()->delete();
        if(count($record->files) > 0){
            foreach($record->files as $key=>$value){
                File::delete('./'.$value->link);
                $value->delete();
            }
        }
        $record->delete();
        if (config('global.device') != 'pc') {
            return  redirect()->route('frontend.todo.index')->with('success','Xóa thành công');
        }else{
            return  redirect()->route('frontend.todo.index')->with('error','Xóa không thành công');
        }
    }
    public function destroyMulti(Request $request){
        $input = $request->all();
        foreach($input['id'] as $key=>$val){
            $record = $this->todoRepo->find($val);
            $record->membertodo()->delete();
            $record->delete();
        }
        if (config('global.device') != 'pc') {
            return  redirect()->back()->with('success','Xóa thành công');
        }else{
            return  redirect()->back()->with('error','Xóa không thành công');
        }
    }
    public function destroyAll(){
        $this->todoRepo->deleteAll();
        if (config('global.device') != 'pc') {
            return  redirect()->back()->with('success','Xóa thành công');
        }else{
            return  redirect()->back()->with('error','Xóa không thành công');
        }
    }
    public function checkStatus($todo_id){
        $count = \App\MemberTodo::where('todo_id',$todo_id)->count();
        $count_status_0 = \App\MemberTodo::where('todo_id',$todo_id)->where('join','>',0)->count();
        $count_status_2 = \App\MemberTodo::where('todo_id',$todo_id)->where('join',2)->count();
        if($count == $count_status_0 && $count_status_2 < $count){
            $this->todoRepo->update(['status'=>2],$todo_id);
        }else if($count_status_2 == $count){
            $this->todoRepo->update(['status'=>4],$todo_id);
        }else{
            $this->todoRepo->update(['status'=>1],$todo_id);
        }
    }
    public function join($id){
        $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$id,['join'=>1,'status'=>1]);
        $this::checkStatus($id);
        return redirect()->back()->with('success','Tham gia công việc thành công');
    }
    public function unjoin(Request $request){
        $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['join'=>2,'reason'=>$request->reason]);
        $this::checkStatus($request->get('id'));
        return redirect()->route('frontend.todo.index')->with('success','Bỏ tham gia công việc thành công');
    }
    public function changeStatus(Request $request){
        $todo = $this->todoRepo->find($request->get('id'));
        if($request->get('progress') == 100){
            if(strtotime(date('Y-m-d H:i')) < strtotime(date('Y-m-d H:i',strtotime($todo->end_date)))){
                $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['progress'=>$request->get('progress'),'complete_date'=>date('Y-m-d H:i'),'pattern'=>1]);
            }elseif(strtotime(date('Y-m-d H:i')) == strtotime(date('Y-m-d H:i',strtotime($todo->end_date)))){
                $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['progress'=>$request->get('progress'),'complete_date'=>date('Y-m-d H:i'),'pattern'=>2]);
            }else{
               $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['progress'=>$request->get('progress'),'complete_date'=>date('Y-m-d H:i'),'pattern'=>3]);
            }
        }else{
            $this->membertodoRepo->updateRecord(\Auth::guard('member')->user()->id,$request->get('id'),['progress'=>$request->get('progress')]);
        }
        if($todo->progress() == 100){
            if(strtotime(date('Y-m-d H:i')) == strtotime(date('Y-m-d H:i',strtotime($todo->end_date)))){
                $this->todoRepo->update(['status'=>3,'complete_date'=>$todo->end_date],$request->get('id'));
            }else{
                $this->todoRepo->update(['status'=>3,'complete_date'=>date('Y-m-d H:i')],$request->get('id'));
            }
        }
        return  redirect()->back()->with('success','Cập nhật trạng thái thành công');
    }
    public function confirm($id){
        $todo = $this->todoRepo->find($id);
        if($todo->created_by == \Auth::guard('member')->user()->id){
            $this->todoRepo->update(['confirm'=>\App\Todo::CONFIRM_COMPLETE],$id);
            return redirect()->route('frontend.todo.history')->with('success','Xác nhận công việc thành công');
        }else{
            abort(403);
        }
    }
    public function unconfirm($id){
        $todo = $this->todoRepo->find($id);
        if($todo->created_by == \Auth::guard('member')->user()->id){
            $this->todoRepo->update(['confirm'=>\App\Todo::CONFIRM_CANCEL,'status'=>2],$id);
            foreach($todo->membertodo as $key=>$val){
                $this->membertodoRepo->updateRecord($val->member_id,$val->todo_id,['progress'=>0,'complete_date'=>NULL,'pattern'=>0]);
            }
            return redirect()->route('frontend.todo.history')->with('success','Trả về công việc thành công');
        }else{
            abort(403);
        }
    }

}
