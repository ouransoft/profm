<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ConfigRepository;
use Stevebauman\Purify\Facades\Purify;
use Repositories\FileRepository;

class ConfigController extends Controller
{
    public function __construct(ConfigRepository $configRepo,FileRepository $fileRepo) {
        $this->configRepo= $configRepo;
        $this->fileRepo = $fileRepo;
    }

    public function index(){
        $record = $this->configRepo->find(1);
        if (config('global.device') != 'pc') {
            return view('mobile/config/index', compact('record'));
        } else {
            return view('frontend/config/index', compact('record'));
        }
    }
    public function update(Request $request){
        $input = Purify::clean($request->all());
        $record = $this->configRepo->update($input,1);
        $this->fileRepo->resetRelationshipID(1,[\App\File::TYPE_LOGO,\App\File::TYPE_FAVICON]);
        if(isset($input['image']) && $input['image'] != ''){
            $this->fileRepo->update(['relationship_id'=>1],$input['image']);
        }
        if(isset($input['favicon']) && $input['favicon'] != ''){
            $this->fileRepo->update(['relationship_id'=>1],$input['favicon']);
        }
        $this->fileRepo->deleteFileNull();
        return redirect()->route('frontend.config.index')->with('success', 'Cập nhật thành công');
    }
}
