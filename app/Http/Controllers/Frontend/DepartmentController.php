<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stevebauman\Purify\Facades\Purify;

class DepartmentController extends Controller
{
    public function getChildren($parent_id,$selected = ''){
        $html = '';
        if(!$parent_id){
            return $html;
        }else{
            if(count(\App\Department::where('parent_id',$parent_id)->get()) > 0){
                $childs = \App\Department::where('parent_id',$parent_id)->get();
                foreach ($childs as $child) {
                    $ids = $child->getIds();
                    $members = \App\Member::whereIn('department_id',$ids)->where('is_deleted',0)->get();
                    $count_member = count($members);
                    $count_schedule = 0;
                    $count_project = 0;
                    $count_todo = 0;
                    foreach($members as $member){
                        $count_schedule += count($member->schedule);
                        $count_project += count($member->project);
                        $count_todo += count($member->membertodo);
                    }
                    $html .= '<li>
                                <a href="javascript:void(0)" class="'.(count($child->children) > 0 ? 'hidden-icon' : 'show-icon').'"><i class="'.(count($child->children) > 0 ? 'fas fa-chevron-circle-down' : 'fas fa-chevron-circle-right').'"></i></a>
                                <div class="department-detail-parent">
                                    <div class="department-detail">
                                        <div class="content-department">
                                            <span>'.$child->name.'</span>
                                        </div>
                                        <div class="department-action">
                                            <a><span class="icons icons-user-dark" style="float: none;top:4px;margin-right: 5px" title="Số lượng thành viên"></span>'.$count_member.'</a>
                                            <a><span class="icons icons-todo-list" style="float: none;top:4px;margin-right: 5px" title="Số lượng công việc"></span>'.$count_todo.'</a>
                                            <a><span class="icons icons-quarter" style="float: none;top:4px;margin-right: 5px" title="Số lượng lịch trình"></span>'.$count_schedule.'</a>
                                            <a><span class="icons icons-project" style="float: none;top:4px;margin-right: 5px" title="Số lượng đề án"></span>'.$count_project.'</a>
                                            <div class="action">
                                                <a href="javascript:void(0)" class="edit-department" data-id="'.$child->id.'"><i class="fas fa-edit"></i></a>
                                                <a href="javascript:void(0)" class="delete-department" data-id="'.$child->id.'"><i class="icon-bin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul '.(count($child->children) == 0 ? 'style="display:none;"' : '').'>';
                    $html .= $this::getChildren($child->id,$selected = '');
                    $html .=  '<li>
                                    <a href="javascript:void(0)" class="add-icon" data-parent_id ="'.$child->id.'" data-level="'.($child->level + 1).'">
                                        <i class="icon-plus-circle2"></i>
                                    </a>
                               </li>
                            </ul>
                        </li>';
                }
            }
            return $html;
        }
    }
    public function index(){
        $html = '';
        $records = \App\Department::where('parent_id',0)->get();
        foreach ($records as $record) {
             $ids = $record->getIds();
             $members = \App\Member::whereIn('department_id',$ids)->where('is_deleted',0)->get();
             $count_member = count($members);
             $count_schedule = 0;
             $count_project = 0;
             $count_todo = 0;
             foreach($members as $member){
                $count_schedule += count($member->schedule);
                $count_project += count($member->project);
                $count_todo += count($member->membertodo);
             }
             $html .= '<li>
                            <a href="javascript:void(0)" class="hidden-icon"><i class="fas fa-chevron-circle-down"></i></a>
                            <div class="department-detail-parent">
                                <div class="department-detail">
                                     <div class="content-department">
                                        <span style="width:40%">'.$record->name.'</span>
                                     </div>
                                     <div class="department-action">
                                        <a><span class="icons icons-user-dark" style="float: none;top:4px;margin-right: 5px" title="Số lượng thành viên"></span>'.$count_member.'</a>
                                        <a><span class="icons icons-todo-list" style="float: none;top:4px;margin-right: 5px" title="Số lượng công việc"></span>'.$count_todo.'</a>
                                        <a><span class="icons icons-quarter" style="float: none;top:4px;margin-right: 5px" title="Số lượng lịch trình"></span>'.$count_schedule.'</a>
                                        <a><span class="icons icons-project" style="float: none;top:4px;margin-right: 5px" title="Số lượng đề án"></span>'.$count_project.'</a>
                                        <div class="action">
                                            <a href="javascript:void(0)" class="edit-department" data-id="'.$record->id.'"><i class="fas fa-edit"></i></a>
                                            <a href="javascript:void(0)" class="delete-department" data-id="'.$record->id.'"><i class="icon-bin"></i></a>
                                        </div>
                                    </div>
                                 </div>
                            </div>
                            <ul>';
             $html .= $this::getChildren($record->id);              
             $html .=  '<li>
                            <a href="javascript:void(0)" class="add-icon" data-parent_id ="'.$record->id.'" data-level="'.($record->level + 1).'">
                                <i class="icon-plus-circle2"></i>
                            </a>
                        </li>
                    </ul>
                </li>';
        }
        $html .='<li>
                    <a href="javascript:void(0)" class="add-icon" data-parent_id ="0" data-level="1">
                       <i class="icon-plus-circle2"></i>
                    </a>
               </li>';
        if (config('global.device') != 'pc') {
            return view('mobile/department/index', compact('html'));
        } else {
            return view('frontend/department/index', compact('html'));
        }
    }
    public function destroy($id) {
        $record = \App\Department::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
