<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SlideRepository;
use Stevebauman\Purify\Facades\Purify;

class SlideController extends Controller
{
    public function __construct(SlideRepository $slideRepo) {
        $this->slideRepo = $slideRepo;
    }
    public function index(){
        $records = $this->slideRepo->all();
        if (config('global.device') != 'pc') {
            return view('mobile/slide/index', compact('records'));
        } else {
            return view('frontend/slide/index', compact('records'));
        }
    }
    public function destroy($id) {
        $record = \App\Slide::find($id);
        $record->delete();
        if ($record) {
            return redirect()->back()->with('success', 'Xóa thành công');
        } else {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
