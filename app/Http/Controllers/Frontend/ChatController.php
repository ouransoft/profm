<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pusher\Pusher;
use Stevebauman\Purify\Facades\Purify;

class ChatController extends Controller
{
    public function index(){
        $user_id = \Auth::guard('member')->user()->id;
        $groups = \App\GroupMember::where('member_id',$user_id)->pluck('group_id')->toArray();
        $all_messages_record = \App\GroupMessage::whereIn('group_id',$groups)->orderBy('created_at','desc')->pluck('group_id')->toArray(); // lấy ra tất cả id của tin nhắn trong các nhóm tìm được
        $all_messages_record = array_unique($all_messages_record); // lọc id trùng lặp - lấy ra id duy nhất
        $all_messages = array();
        $avatar = "";
        foreach($all_messages_record as $key => $record){
            if(\App\Group::where('id',$record)->first()->type == 99){
                $all_messages[$key]['avatar'] = '\assets2\img\gear.png';
                $all_messages[$key]['name'] = "Tin nhắn hệ thống";
            }
            else if(\App\Group::where('id',$record)->first()->name != null){
                $all_messages[$key]['avatar'] = \App\Group::where('id',$record)->first()->avatar;
                $all_messages[$key]['name'] = \App\Group::where('id',$record)->first()->name;
            }else{
                $receiver = \App\GroupMember::where('group_id',$record)->where('member_id','!=',$user_id)->first();
                $all_messages[$key]['avatar'] = \App\Member::where('id',$receiver->member_id)->first()->avatar;
                $all_messages[$key]['name'] = $receiver->name;
            }
            $all_messages[$key]['message'] = \App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->message;
            $all_messages[$key]['time'] = \App\Helpers\StringHelper::time_ago(\App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->created_at);
            $all_messages[$key]['id'] = $record;
            $all_messages[$key]['type'] = \App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->type;;
            $seen = \App\ReadMessage::where('message_id',\App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->id)
                                            ->where('member_id',\Auth::guard('member')->user()->id)
                                            ->where('group_id',$record)
                                            ->get();  
            if(count($seen)>0){
                $all_messages[$key]['seen'] = $seen[0]->seen;
            }else{
                $all_messages[$key]['seen'] = 1;
            }
        }
        if(\App\Group::where('id',$all_messages_record[0])->first()->type == 99){
            $group_name = "Tin nhắn hệ thống";
            $to_member_id = 0;
            $group_id = \App\Group::where('id',$all_messages_record[0])->first()->type;
            $to_type = 0;
            $avatar = '\assets2\img\gear.png';
        }else{
            $group_name = \App\Group::where('id',$all_messages_record[0])->first()->name;
            $to_member = \App\GroupMember::where('group_id',$all_messages_record[0])->where('member_id','!=',\Auth::guard('member')->user()->id)->first();
            $to_type = 0; // Group
            $to_member_id = $to_member->member_id;
            if($group_name == null){
                $group_name = $to_member->name;
                $to_type = 1; // Member
            }
            $group_id = $all_messages_record[0];
            // $messages = \App\GroupMessage::where('group_id',$all_messages_record[0])->get();
            $avatar = $all_messages[0]['avatar'];
        }
        $group = \App\Group::find($group_id);
        $group_type = \App\Group::where('id',$all_messages_record[0])->first()->type;
        $messages = \App\GroupMessage::where('group_id',$all_messages_record[0])->get();
        foreach($messages as $key=>$val){
            if($val->reply_id > 0){
                $val->reply = \App\GroupMessage::find($val->reply_id);
            }
        }
        return view('frontend.chat.view', compact('group','messages','user_id','all_messages','group_id','group_type','group_name','avatar','to_type','to_member_id'));
    }

    public function searchingMember(Request $request){
        $input = $request->all();
        $members = \App\Member::where('full_name','like','%'.$input['value'].'%')->get();
        $member_result = array();
        foreach($members as $key => $member){
            $link = $member->id;
            $type = 0; // lấy theo member_id
            $group_id = \App\GroupMember::where('member_id',$member->id)->pluck('group_id')->toArray(); // lấy ra các group của người tìm kiếm đc
            $current_member_group_id = \App\GroupMember::where('member_id',\Auth::guard('member')->user()->id)->whereIn('group_id',$group_id)->get(); // lấy ra các group của người dùng hiện tại
            if(count($current_member_group_id) > 0){
                foreach($current_member_group_id as $record){
                    if(\App\Group::where('id',$record->group_id)->first()->name == null){
                        $link = $record->group_id;
                        $type = 1; // lấy theo group_id
                    }
                }
            }
            $member_result[$key]['avatar'] = $member->avatar;
            $member_result[$key]['name'] = $member->full_name;
            $member_result[$key]['link'] = $link;
            $member_result[$key]['type'] = $type;
        }

        $groups = \App\GroupMember::where('member_id',\Auth::guard('member')->user()->id)->pluck('group_id')->toArray();
        $groups = \App\Group::whereIn('id',$groups)->where('name','!=',"")->where('name','like','%'.$input['value'].'%')->get();

        $group_result = array();
        foreach($groups as $key => $group){
            $group_result[$key]['avatar'] = $group->avatar;
            $group_result[$key]['name'] = $group->name;
            $group_result[$key]['link'] = $group->id;
            $group_result[$key]['type'] = 1;
        }
        return response()->json(['member_result'=>$member_result,'group_result'=>$group_result]);
    }
    // Tìm kiếm thành viên để thêm vào nhóm
    public function searchingMemberGroup(Request $request){
        $input = Purify::clean($request->all());
        if($input['name'] == ""){
            $member = \App\Member::where('id','!=',\Auth::guard('member')->user()->id)->where('is_deleted',0)->get();
            return response()->json(['member'=>$member]);
        }
        $member = \App\Member::where('id','!=',\Auth::guard('member')->user()->id)->where('full_name','like','%'.$input['name'].'%')->get();
        return response()->json(['member'=>$member]);
    }

    public function createGroup(Request $request){
        $input = Purify::clean($request->all());
        $input['id'] = explode(',',$input['id']);
        $group = new \App\Group();
        $destinationPath = 'chat/groups/image';
        $message = array();
        $file = $input['group_img'];
        if($file == null){
            $group->avatar = "assets2/img/team.png";
        }else{
            $files = $destinationPath . '/' . $file->getClientOriginalName();
            $file_name = $file->getClientOriginalName();
            $file->move($destinationPath, $file->getClientOriginalName());
            $group->avatar = $files;
        }
        $group->name = $input['group_name'];
        $group->save();
        foreach($input['id'] as $record){
            $member_name = \App\Member::where('id',$record)->first()->full_name;
            $group->member()->attach($record, ['name' => $member_name]);
            $group->save();
        }
        $message = 'Nhóm được tạo bởi '.\App\Member::where("id",$input["id"][0])->first()->full_name.' vào lúc '.date('h:i A d-m-Y', strtotime($group->created_at));
        \App\GroupMessage::create([
            'from' => 0,
            'group_id' => $group->id,
            'message' => $message
        ]);
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
        );
        $data = ['from' => 0,'date'=>date('d-m'),'group_id' => $group->id,'message'=>$message,];
        $pusher->trigger('chat-message', 'send-message', $data);
    }
    public function addMember(Request $request){
        $input = Purify::clean($request->all());
        $input['id'] = explode(',',$input['id']);
        $group = \App\Group::find($input['group_id']);
        $member = \App\Member::find($input['member_id']);
        foreach($input['id'] as $record){
            $member_name = \App\Member::where('id',$record)->first()->full_name;
            $group->member()->attach($record, ['name' => $member_name]);
        }
        $members = \App\Member::whereIn('id', $input['id'])->get();
        $member_name = implode(', ',$members->pluck('full_name')->toArray());
        $message = $member->full_name.' đã thêm '.$member_name.' vào nhóm lúc '.date('h:i A d-m-Y', strtotime($group->created_at));
        \App\GroupMessage::create([
            'from' => 0,
            'group_id' => $group->id,
            'message' => $message
        ]);
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );
        $pusher = new Pusher(
            '5f84d2a344876b9fea04', '20b454d858313e2615b9', '1085064', $options
        );
        $data = ['from' => 0,'date'=>date('d-m'),'group_id' => $group->id,'message'=>$message,];
        $pusher->trigger('chat-message', 'send-message', $data);
        foreach($members as $key=>$member){
            $member->position_name = $member->position ? $member->position->name : '';
        }
        return response()->json(['members'=>$members]);
    }
    public function getListMember(Request $request){
        $input = Purify::clean($request->all());
        $group = \App\Group::find($input['group_id']);
        $member_ids = $group->member()->pluck('id')->toArray();
        if(isset($input['keyword']) && $input['keyword'] != ""){
             $members = \App\Member::whereNotIn('id',$member_ids)->where('full_name','like','%'.$input['keyword'].'%')->get();
        }else{
             $members = \App\Member::whereNotIn('id',$member_ids)->get();
        }
        return response()->json(['members'=>$members]);
    }
    public function leaveGroup(Request $request){
        $input = Purify::clean($request->all());
        $group = \App\Group::find($input['group_id']);
        $group->member()->detach([$input['member_id']]);
        return response()->json(['success'=>true]);
    }
    public function deleteMessage(Request $request){
        $input = Purify::clean($request->all());
        $message = \App\GroupMessage::find($input['id']);
        $message->delete();
        return response()->json(['success'=>true]);
    }
}
