<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ProjectRepository;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Session;
use Repositories\LevelRepository;
use Repositories\LogApprovedRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Stevebauman\Purify\Facades\Purify;
use Repositories\FileRepository;

class ProjectController extends Controller {
    public function __construct(FileRepository $fileRepo,LogApprovedRepository $logapprovedRepo,MemberRepository $memberRepo,ProjectRepository $projectRepo,LevelRepository $levelRepo) {
        $this->projectRepo = $projectRepo;
        $this->memberRepo = $memberRepo;
        $this->levelRepo= $levelRepo;
        $this->logapprovedRepo = $logapprovedRepo;
        $this->fileRepo = $fileRepo;
    }
    public function index(){
        Session::put('p_page',1);
        if(isset($_GET['keyword'])){
            $records = $this->projectRepo->fillter($_GET['keyword'],15);
            $keyword = $_GET['keyword'];
        }else{
            $records = $this->projectRepo->getIndex(15);
            $keyword='';
            Session::forget('keyword');
        }
        foreach($records as $key=>$record){
            $logapproved = \App\LogApproved::where('project_id',$record->id)->whereIn('status',[0,2])->orderBy('progress','DESC')->first();
            if($logapproved){
                if($logapproved->status == 2){
                    switch ($logapproved->progress){
                        case 1: 
                            $record->getStatus = '<span class="badge badge-secondary">'.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                            $record->getStatusMobile = '<div class="icon-box bg-secondary">
                                                            <span>ĐN</span>
                                                        </div>';
                            break;
                        case 2: 
                            $record->getStatus = '<span class="badge badge-secondary">'.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                            $record->getStatusMobile = '<div class="icon-box bg-secondary">
                                                            <span>D1</span>
                                                        </div>';
                            break;
                        case 3: 
                            $record->getStatus = '<span class="badge badge-primary">'.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                            $record->getStatusMobile = '<div class="icon-box bg-primary">
                                                            <span>D2</span>
                                                        </div>';
                            break;
                        case 4: 
                            $record->getStatus = '<span class="badge badge-info">'.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                            $record->getStatusMobile = '<div class="icon-box bg-info">
                                                            <span>GV</span>
                                                        </div>';
                            break;
                        case 5: 
                            $record->getStatus = '<span class="badge badge-warning">'.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                            $record->getStatusMobile = '<div class="icon-box bg-warning">
                                                            <span>BC</span>
                                                        </div>';
                            break;
                        case 6: 
                            $record->getStatus = '<span class="badge badge-success">'.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                            $record->getStatusMobile = '<div class="icon-box bg-success">
                                                            <span>HT</span>
                                                        </div>';
                            break;
                    }
                }else{
                    $record->getStatus = '<span class="badge badge-danger">Trả về</span>'; 
                    $record->getStatusMobile = '<div class="icon-box bg-danger">
                                                            <span>TV</span>
                                                        </div>';
                }
            }else{
                $record->getStatus = '<span class="badge badge-secondary">Nháp</span>';
                $record->getStatusMobile = '<div class="icon-box badge-secondary">
                                                <span>N</span>
                                            </div>';
            }
        }
        if (config('global.device') != 'pc') {
            return view('mobile/project/index', compact('records','keyword'));
        } else {
            return view('frontend/project/index', compact('records','keyword'));
        }
    }
    public function listProject(Request $request){
        $records = $this->projectRepo->getIndexByMember($request);
        foreach($records as $key=>$record){
            $logapproved = \App\LogApproved::where('project_id',$record->id)->orderBy('progress','DESC')->first();
            if($logapproved){
                if($logapproved->status == 1){
                        switch ($logapproved->progress){
                            case 2: 
                                $record->getStatus = '<span class="badge badge-secondary">Chờ '.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                                $record->getStatusMobile = '<div class="icon-box bg-secondary">
                                                                <span>D1</span>
                                                            </div>';
                                break;
                            case 3: 
                                $record->getStatus = '<span class="badge badge-secondary">Chờ '.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                                $record->getStatusMobile = '<div class="icon-box bg-primary">
                                                                <span>D2</span>
                                                            </div>';
                                break;
                            case 4: 
                                $record->getStatus = '<span class="badge badge-secondary">Chờ '.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                                $record->getStatusMobile = '<div class="icon-box bg-info">
                                                                <span>GV</span>
                                                            </div>';
                                break;
                            case 5: 
                                $record->getStatus = '<span class="badge badge-secondary">Chờ '.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                                $record->getStatusMobile = '<div class="icon-box bg-warning">
                                                                <span>BC</span>
                                                            </div>';
                                break;
                            case 6: 
                                $record->getStatus = '<span class="badge badge-secondary">Chờ '.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                                $record->getStatusMobile = '<div class="icon-box bg-success">
                                                                <span>HT</span>
                                                            </div>';
                                break;
                        }
                }elseif($logapproved->status == 0){
                     $record->getStatus = '<span class="badge badge-danger">Trả về</span>'; 
                     $record->getStatusMobile = '<div class="icon-box bg-danger">
                                                                <span>TV</span>
                                                            </div>';
                }else{
                    $record->getStatus = '<span class="badge badge-primary">'.trans('base.'.\App\Project::Progress_arr[6]).'</span>';
                    $record->getStatusMobile = '<div class="icon-box bg-success">
                                                                <span>HT</span>
                                                            </div>';
                }
            }else{
                $record->getStatus = '<span class="badge badge-danger">Trả về</span>'; 
                $record->getStatusMobile = '<div class="icon-box bg-danger">
                                                            <span>TV</span>
                                                        </div>';
            }
            $check = \App\LogApproved::where('project_id',$record->id)->where('status',1)->where('progress',5)->get();
            if(count($check) > 0){
                $record->getStatus = '<span class="badge badge-secondary">Chờ '.trans('base.'.\App\Project::Progress_arr[$logapproved->progress]).'</span>';
                $record->getStatusMobile = '<div class="icon-box bg-warning">
                                                <span>BC</span>
                                            </div>';
            }
        }
        if($request->get('department_id')){
            $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get(),$request->get('department_id'));
        }else{
            $department_html = \App\Helpers\StringHelper::getSelectHtml(\App\Department::get());
        }
        if($request->get('status')){
            $status_html = \App\Helpers\StringHelper::getSelectStatusProject(\App\Project::Progress_arr,$request->get('status'));
        }else{
            $status_html = \App\Helpers\StringHelper::getSelectStatusProject(\App\Project::Progress_arr);
        }
        if($request->get('level')){
            if($request->get('level') == 'KCD'){
                $level_html = '<option></option><option value="KCD" selected>Không cấp độ</option>';
            }else{
                $level_html = '<option></option><option value="KCD">Không cấp độ</option>';
            }
            $level_html .= \App\Helpers\StringHelper::getSelectHtml(\App\Level::get(),$request->get('level'));
        }else{
            $level_html = '<option></option><option value="KCD">Không cấp độ</option>';
            $level_html .= \App\Helpers\StringHelper::getSelectHtml(\App\Level::get());  
        }
        if (config('global.device') != 'pc') {
            return view('mobile/project/list', compact('records','department_html','level_html'));
        } else {
            return view('frontend/project/list', compact('records','department_html','level_html','status_html'));
        }
    }
    public function create() {
        $members = $this->memberRepo->getAllNotMe();
        $member_html = \App\Helpers\StringHelper::getSelectCodeMemberOptions($members);
        if(\Auth::guard('member')->user()->level == 2){
            $member_approved_html = \App\Helpers\StringHelper::getSelectMemberApprovedOptions(\App\Member::where('level',3)->where('is_deleted',0)->get());
        }else{
            $member_approved_html = \App\Helpers\StringHelper::getSelectMemberApprovedOptions(\App\Member::whereIn('level',[2,3])->where('is_deleted',0)->get());
        }
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all());
        if (config('global.device') != 'pc') {
            return view('mobile/project/create', compact('member_html','level_html','member_approved_html'));
        } else {
            return view('frontend/project/create', compact('member_html','level_html','member_approved_html'));
        }
    }
    public function store(Request $request) {
        $input = Purify::clean($request->all());
        $input['sort_date'] = date('Y-m-d H:i:s');
        if(!isset($input['member_id'])){
            $input['member_id']= \Auth::guard('member')->user()->id;
        }
        $member = $this->memberRepo->find($input['member_id']);
        if(isset($input['draft'])){
            $input['status'] = 0;
        }else{
            $input['status'] = $member->level;
        }
        $project = $this->projectRepo->create($input);
        if($input['status'] == \App\Project::STATUS_ACTIVE){
            $this->projectRepo->update(['approved_at'=>date('Y-m-d h:i:s'),'sort_date'=>date('Y-m-d h:i:s')],$project->id);
        }
        $log['status']= 2;
        $log['member_id']= $member->id;
        $log['project_id'] = $project->id;
        $log['level'] = $member->level;
        $log['progress'] = 1;
        $this->logapprovedRepo->create($log);
        if($member->level == \App\Member::LEVEL_D1){
             $log['progress'] = 2;
             $this->logapprovedRepo->create($log);
        }
        if($member->level == \App\Member::LEVEL_D2){
             $log_arr = ['2','3','6'];
                if($project->pattern == 1){
                    foreach($log_arr as $val){
                        $log['progress'] = $val;
                        $this->logapprovedRepo->create($log);
                    }
                }else{
                    foreach($log_arr as $val){
                        if($val != 6){
                            $log['progress'] = $val;
                            $this->logapprovedRepo->create($log);
                        }
                    }
                    $log_assign['member_id']= $member->id;
                    $log_assign['project_id'] = $project->id;
                    $log_assign['level'] = $member->level;
                    $log_assign['progress'] = 4;
                    $log_assign['status'] = 1;
                    $this->logapprovedRepo->create($log_assign);
                }
        }
        if($project->status > 0 && $project->status < \App\Project::STATUS_ACTIVE){
            $member_approved = $this->memberRepo->find($input['member_approved_id']);
            $log['status']= 1;
            $log['member_id']= $member_approved->id;
            $log['level'] = $member_approved->level;
            $log['progress'] = $member_approved->level;
            $this->logapprovedRepo->create($log);
            $this->memberRepo->NotificateMember($input['member_approved_id'], ['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
        }
        if(isset($input['files']) && count($input['files']) > 0){
            foreach($input['files'] as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$project->id],$val);
            }
        }
        if($input['image_before_id']){
            $image_before_ids = explode(',',$input['image_before_id']);
            foreach($image_before_ids as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$project->id],$val);
            }
        }
        if($input['image_after_id']){
            $image_after_ids = explode(',',$input['image_after_id']);
            foreach($image_after_ids as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$project->id],$val);
            }
        }
        if ($project->id) {
            return redirect()->route('frontend.project.index')->with('success', 'Tạo mới thành công');
        } else {
            return redirect()->route('frontend.project.index')->with('error', 'Tạo mới thất bại');
        }
    }
    public function edit($id) {
        $record = $this->projectRepo->find($id);
        $members = $this->memberRepo->getAllNotMe();
        $member_html = \App\Helpers\StringHelper::getSelectCodeMemberOptions($members);
        $member_approved_html = \App\Helpers\StringHelper::getSelectMemberApprovedOptions(\App\Member::whereIn('level',[2,3])->where('is_deleted',0)->get());
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->all(),$record->level);
        if (config('global.device') != 'pc') {
            return view('mobile/project/update', compact('record','level_html','member_html','member_approved_html'));
        } else {
            return view('frontend/project/update', compact('record','level_html','member_html','member_approved_html'));
        }
    }
    public function update(Request $request, $id){
        $input = Purify::clean($request->all());
        $project = $this->projectRepo->find($id);
        if(!isset($input['draft'])){
            $input['status'] = \Auth::guard('member')->user()->level;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['sort_date'] = date('Y-m-d H:i:s');
            $member = $this->memberRepo->find($project->member_id);
            if($member->level == \App\Member::LEVEL_D1){
                if(isset($input['member_approved_id'])){
                    $log['status']= 2;
                    $log['member_id']= $member->id;
                    $log['project_id'] = $project->id;
                    $log['level'] = $member->level;
                    $log['progress'] = 1;
                    $this->logapprovedRepo->create($log);
                    $log['progress'] = 2;
                    $this->logapprovedRepo->create($log);
                    $member_approved = $this->memberRepo->find($input['member_approved_id']);
                    $log['status']= 1;
                    $log['member_id']= $member_approved->id;
                    $log['level'] = $member_approved->level;
                    $log['progress'] = $member_approved->level;
                    $this->logapprovedRepo->create($log);
                    $this->memberRepo->NotificateMember($input['member_approved_id'], ['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
                }else{
                    $this->logapprovedRepo->updateByProgress(3,$project->id,['status'=>1]);
                    $logapproved = $this->logapprovedRepo->getByProgress(3,$project->id);
                    $this->memberRepo->NotificateMember($logapproved->member_id,['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
                }
            }elseif($member->level == \App\Member::LEVEL_NV){
                if(isset($input['member_approved_id'])){
                    $log['status']= 2;
                    $log['member_id']= $member->id;
                    $log['project_id'] = $project->id;
                    $log['level'] = $member->level;
                    $log['progress'] = 1;
                    $this->logapprovedRepo->create($log);
                    $member_approved = $this->memberRepo->find($input['member_approved_id']);
                    $log['status']= 1;
                    $log['member_id']= $member_approved->id;
                    $log['level'] = $member_approved->level;
                    $log['progress'] = $member_approved->level;
                    $this->logapprovedRepo->create($log);
                    $this->memberRepo->NotificateMember($input['member_approved_id'], ['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
                }else{
                    if(count(\App\LogApproved::where('progress',2)->where('project_id',$project->id)->get()) > 0){
                        $this->logapprovedRepo->deleteByProject($id);
                        if($input['status'] == \App\Project::STATUS_ACTIVE){
                            $this->projectRepo->update(['approved_at'=>date('Y-m-d h:i:s'),'sort_date'=>date('Y-m-d h:i:s')],$project->id);
                        }
                        \App\LogApproved::where('progress',1)->where('project_id',$project->id)->update(['updated_at'=>date('Y-m-d H:i:s')]);
                        \App\LogApproved::where('progress',2)->where('project_id',$project->id)->update(['status'=>1]);
                        $logapproved = $this->logapprovedRepo->getByProgress(2,$project->id);
                        $this->memberRepo->NotificateMember($logapproved->member_id,['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
                    }else{
                        $logapproved = $this->logapprovedRepo->getByProgress(3,$project->id);
                        \App\LogApproved::where('progress',3)->where('project_id',$project->id)->update(['status'=>1]);
                        $this->memberRepo->NotificateMember($logapproved->member_id,['content' => 'Có đề án mới', 'link' => '/project/view/' . $project->id]);
                    }
                }
            }
        }
        $input['reason'] = NULL;
        // Thêm file vào project
        // Thêm file vào project
        $this->fileRepo->resetRelationshipID($project->id,[\App\File::TYPE_FILE_PROJECT_BEFORE,\App\File::TYPE_FILE_PROJECT_AFTER,\App\File::TYPE_IMAGE_PROJECT_BEFORE,\App\File::TYPE_IMAGE_PROJECT_AFTER]);
        if(isset($input['files']) && count($input['files']) > 0){
            foreach($input['files'] as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$project->id],$val);
            }
        }
        if($input['image_before_id']){
            $image_before_ids = explode(',',$input['image_before_id']);
            foreach($image_before_ids as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$project->id],$val);
            }
        }
        if($input['image_after_id']){
            $image_after_ids = explode(',',$input['image_after_id']);
            foreach($image_after_ids as $key=>$val){
                $this->fileRepo->update(['relationship_id'=>$project->id],$val);
            }
        }
        $this->fileRepo->deleteFileNull();
        $res = $this->projectRepo->update($input, $id);
        if ($res) {
            return redirect()->route('frontend.project.index')->with('success', 'Cập nhật thành công');
        } else {
            return redirect()->route('frontend.project.index')->with('error', 'Cập nhật thất bại');
        }
    }
    public function fillter($keyword){
        Session::put('p_page',1);
        $records = $this->projectRepo->fillter($keyword,10);
        return view('frontend/project/fillter',compact('records'));
    }
    public function view($id){
        $record = $this->projectRepo->find($id);
        $member = $this->memberRepo->find($record->member_id);
        $member_html = \App\Helpers\StringHelper::getSelectMemberOptions($this->memberRepo->getAll());
        $logapproved = \App\LogApproved::where('project_id',$id)->whereIn('status',[0,2])->orderBy('progress','DESC')->first();
        $member_approved = \App\LogApproved::where('project_id',$id)->where('status',1)->first();
        $member_approved_html = \App\Helpers\StringHelper::getSelectMemberApprovedOptions(\App\Member::where('level',\App\Member::LEVEL_D2)->where('is_deleted',0)->get());
        if($logapproved){
            $check = \App\LogApproved::where('project_id',$id)->where('status',1)->where('progress',5)->get();
            if(count($check) == 0){
                $progress = $logapproved->progress;
            }else{
                $progress = 4;
            }
        }else{
            $progress = 0;
        }
        $member_project = \App\MemberProject::where('member_id',\Auth::guard('member')->user()->id)->where('project_id',$record->id)->first();
        $list_member_project = \App\MemberProject::where('project_id',$record->id)->where('status',1)->get();
        $level_html = \App\Helpers\StringHelper::getSelectOptions($this->levelRepo->getAll());
        if (config('global.device') != 'pc') {
            return view('mobile/project/view', compact('record','level_html','logapproved','progress','member_html','member_approved','member_approved_html','member_project','list_member_project'));
        } else {
            return view('frontend/project/view', compact('record','level_html','logapproved','progress','member_html','member_approved','member_approved_html','member_project','list_member_project'));
        }
    }
    public function chart(){
        $label1 = [];
        $data1=[];
        $label2 = [];
        $data2=[];
        for($i=6;$i--;$i>0){
            $dt = Carbon::now();
            $dt->subMonth($i);
            $month = $dt->month;
            $year = $dt->year;
            $label1[] = "Tháng ".$dt->month;
            $data1[] = \App\Project::whereMonth('approved_at',$month)->whereYear('approved_at',$year)->where('is_destroy',0)->where('is_deleted',0)->count();
        }
        $status_pending = \App\Project::where('status','<',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->where('is_destroy',0)->count();
        $status_approved = \App\Project::where('status','=',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->where('is_destroy',0)->count();
        $status_return = \App\Project::where('status','=',\App\Project::STATUS_CANCEL)->where('is_deleted',0)->where('is_destroy',0)->count();
        $levels = $this->levelRepo->getAll();
        $label2[] = 'Không cấp độ';
        $data2[]= \App\Project::where('level',null)->where('is_destroy',0)->where('is_deleted',0)->where('status','>',0)->count();
        foreach($levels as $key=>$level){
            $label2[] = $level->name;
            $data2[]= \App\Project::where('level',$level->id)->where('is_destroy',0)->where('is_deleted',0)->where('status','>',0)->count();
        }
        if (config('global.device') != 'pc') {
            return view('mobile/project/chart',compact('label1','data1','status_pending','status_approved','status_return','label2','data2'));
        }else{
            return view('frontend/project/chart',compact('label1','data1','status_pending','status_approved','status_return','label2','data2'));
        }
    }
    public function approved($id){
        $check = $this->logapprovedRepo->checkProgress(6,$id);
        if(count($check) > 0){
            $this->logapprovedRepo->updateByProgress(6,$id,['status'=>2]);
        }else{
            $log['member_id'] = \Auth::guard('member')->user()->id;
            $log['project_id'] = $id;
            $log['progress'] = 6;
            $log['status'] = 2;
            $logapproved = $this->logapprovedRepo->create($log);
        }
        $project = $this->projectRepo->find($id);
        $member_ids = $project->memberproject()->pluck('member_id')->toArray();
        $this->memberRepo->NotificateMembers($member_ids,['content' => 'Kết quả đề án đã được duyệt', 'link' => '/project/view/' . $id]);  
        return redirect()->route('frontend.project.view',$id)->with('success', 'Duyệt đề án thành công');
    }
}
