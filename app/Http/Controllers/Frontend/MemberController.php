<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Stevebauman\Purify\Facades\Purify;
use Repositories\FileRepository;
class MemberController extends Controller
{
    public function __construct(MemberRepository $memberRepo,FileRepository $fileRepo) {
        $this->memberRepo = $memberRepo;
        $this->fileRepo = $fileRepo;
    }

    public function index(Request $request) {
        $records = $this->memberRepo->getIndex($request,10);
        if($request->get('department_id')){
            $department_html = \App\Helpers\StringHelper::getSelectDepartmentChildren(0,$request->get('department_id'));
        }else{
            $department_html = \App\Helpers\StringHelper::getSelectDepartmentChildren(0);
        }
        if($request->get('position_id')){
            $position_html = \App\Helpers\StringHelper::getSelectHtml(\App\Position::get(),$request->get('position_id'));
        }else{
            $position_html = \App\Helpers\StringHelper::getSelectHtml(\App\Position::get());
        }
        if (config('global.device') != 'pc') {
            return view('mobile/member/index', compact('records','department_html','position_html'));
        } else {
            return view('frontend/member/index', compact('records','department_html','position_html'));
        }
    }
    public function store(Request $request) {
        $input = Purify::clean($request->all());
        $validator = \Validator::make($input, $this->memberRepo->validateCreate());
        $input['password'] = bcrypt($input['password']);
        $input['avatar'] = '/img/avatar.png';
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $member = $this->memberRepo->create($input);
        if ($member) {
            return redirect()->route('frontend.member.index')->with('success', 'Tạo mới thành công');
        } else {
            return redirect()->route('frontend.member.index')->with('error', 'Tạo mới thất bại');
        }
    }
    public function update(Request $request, $id) {
        $input = Purify::clean($request->all());
        $validator = \Validator::make($input, $this->memberRepo->validateUpdate($id));
        if($request->file('file')){
            $targetDir = 'upload/images/';
            $allowTypesImage = array('jpg','png','jpeg','gif', 'PNG', 'GIF', 'JPG');
            $data = [];
            $fileName = basename($_FILES['file']['name']);
            if(strlen($fileName)!=0){
                $fileType = pathinfo($fileName);
                if(in_array($fileType['extension'], $allowTypesImage)){
                    $link = $targetDir.time().'_'.$fileName;
                    move_uploaded_file($_FILES['file']['tmp_name'],$link);
                    $file['link'] = $link;
                    $file['size'] = number_format($_FILES['file']['size'] / 1048576,1);
                    $file['name'] = $fileName;
                    $file['type'] = \App\File::TYPE_MEMBER;
                    $file['format'] = \App\File::FORMAT_IMAGE;
                    $file['relationship_id'] = \Auth::guard('member')->user()->id;
                    \App\File::where('type',\App\File::TYPE_MEMBER)->delete();
                    $this->fileRepo->create($file);
                }
            }
        }
        if (!$input['password']){
            unset($input['password']);
        }else{
            $password = $request->get('password');
            $input['password'] = bcrypt($password);
        }
        $res = $this->memberRepo->update($input, $id);
        return redirect()->back()->with('success', 'Cập nhật thành công');
    }
     public function profile(){
        return view('mobile/member/profile');
    }
    public function history(Request $request){
        $records = $this->memberRepo->getIndexRemove($request,10);
        $department_html = \App\Helpers\StringHelper::getSelectDepartmentChildren(0);
        $position_html = \App\Helpers\StringHelper::getSelectHtml(\App\Position::get());
        if (config('global.device') != 'pc') {
            return view('mobile/member/history', compact('records','department_html','position_html'));
        } else {
            return view('frontend/member/history', compact('records','department_html','position_html'));
        }
    }
}
