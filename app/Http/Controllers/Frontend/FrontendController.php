<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Repositories\SlideRepository;
use App\Repositories\MemberRepository;
use App\Schedule;
use Carbon\Carbon;
use Repositories\ToDoRepository;
use Repositories\DepartmentRepository;
use Repositories\EquipmentRepository;
use Repositories\NotificationScheduleRepository;
use Stevebauman\Purify\Facades\Purify;
use Repositories\ScheduleRepository;

class FrontendController extends Controller {
    public function __construct(ScheduleRepository $scheduleRepo,NotificationScheduleRepository $notificationSchRepo,EquipmentRepository $equipmentRepo,DepartmentRepository $departmentRepo,ToDoRepository $todoRepo,SlideRepository $slideRepo,MemberRepository $memberRepo) {
        $this->slideRepo = $slideRepo;
        $this->memberRepo = $memberRepo;
        $this->todoRepo = $todoRepo;
        $this->departmentRepo=$departmentRepo;
        $this->equipmentRepo = $equipmentRepo;
        $this->notificationSchRepo=$notificationSchRepo;
        $this->scheduleRepo = $scheduleRepo;
    }
    public function dashboard(){
        $members = $this->memberRepo->getAll();
        $total_todo = \App\Todo::count();
        $total_schedule = \App\Schedule::count();
        $total_project = \App\Project::count();
        $data_chart_bar = [];
        for($i=1;$i < 13;$i++){
            $data_chart_bar[] = \App\Project::whereMonth('approved_at',$i)->whereYear('approved_at',date('Y'))->where('is_destroy',0)->where('is_deleted',0)->count();
        }
        $status_pending = \App\Project::where('status','<',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->where('is_destroy',0)->count();
        $status_approved = \App\Project::where('status','=',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->where('is_destroy',0)->count();
        $status_return = \App\Project::where('status','=',\App\Project::STATUS_CANCEL)->where('is_deleted',0)->where('is_destroy',0)->count();
        $status_project = [$status_pending,$status_approved,$status_return];
        return view('frontend.dashboard.index',compact('total_todo','total_schedule','total_project','data_chart_bar','status_project'));
    }
    public function index() {
        $slides = $this->slideRepo->all();
        $date_now = date('Y-m-d');
        $schedules = DB::table('schedule')->get();
        $schedule_arr = array();
        for($i=0;$i<7;$i++){
            $first = Carbon::now();
            $current_day = date('d-m-Y',strtotime($first->addDays($i)));
            $next_day = date('d-m-Y',strtotime($first->addDays($i+1)));

            $schedule = DB::table('schedule')->where('public_dashboard',1)
                                            ->where('start_date','<=',date('Y-m-d',strtotime($current_day)))
                                            ->where('end_date','>=',date('Y-m-d',strtotime($current_day)))
                                            ->get();
            if(count($schedule) > 0){
                $schedule_arr[$i]['event'] = $schedule;
            }else{
                $schedule_arr[$i]['event'] = [""];
            }
            $schedule_arr[$i]['day'] = date('d-m-Y',strtotime($current_day));
            $schedule_arr[$i]['weekday'] = Carbon::now()->addDays($i)->dayOfWeek; // lấy số thứ tự của ngày trong tuần
        }
        $config = DB::table('config')->first();
        if (config('global.device') != 'pc') {
            return view('mobile/home/index');
        } else {
            return view('frontend/home/index',compact('config','schedule_arr','date_now','slides'));
        }
    }
    public function home(){
        return view('frontend/home/home');
    }
    public function create(){
        return view('frontend/home/create');
    }
    public function view(Request $request){
        if($request->get('bdate')){
            $date_now = $request->get('bdate');
        }else{
            $date_now = date('Y-m-d');
        }
        if(!is_null(\Auth::guard('member')->user()->department)){
            $department_id = \Auth::guard('member')->user()->department->getIds();
        }else{
            $department_id= [];
        }
        $members = $this->memberRepo->getByDepartment($department_id,\Auth::guard('member')->user()->id);
        $html ='<tr><td class="s_domain_week"><span class="domain">(UTC+07:00) VietNam</span></td>';
        for($i = 0; $i < 7; $i++) {
             $date = date('Y-m-d', strtotime(" +" . $i . " days",strtotime($date_now)));
             if($i == 0){
                $html .='<td class="s_date_'.strtolower(date('l',strtotime($date))).'_week" align="center">&nbsp;<a href="/schedule/group_day?bdate='.$date.'">'.date('l, d M',strtotime($date)).'</a><input type="hidden" id="day_start" value="'.date('Y-m-d',strtotime($date)).'" /></td>';
             }else{
                $html .='<td class="s_date_'.strtolower(date('l',strtotime($date))).'_week" align="center">&nbsp;<a href="/schedule/group_day?bdate='.$date.'">'.date('l, d M',strtotime($date)).'</a></td>';
             }
        }
        if(is_null(\Auth::guard('member')->user()->file())){
            $avatar = '<div class="profileImageUser-grn"></div>';
        }else{
            $avatar = '<div class="user_photo_grn" style="background-image: url(/'.\Auth::guard('member')->user()->file()->link.');" aria-label=""></div>';
        }
        $html .='</tr><tr class="js_customization_schedule_user_id_'.\Auth::guard('member')->user()->id.'">
                    <td valign="top" class="calendar_rb_week userBox">
                        <div class="userElement profileImageBase-grn profileImageBaseSchedule-grn">
                           <dl>
                              <dt>
                                 <a >
                                    <div class="profileImage-grn">
                                       <div class="profileImageFrame-grn">
                                          '.$avatar.'
                                       </div>
                                    </div>
                                 </a>
                              </dt>
                              <dd><a >'.\Auth::guard('member')->user()->full_name.'</a></dd>
                           </dl>
                           <div class="clear_both_0px"></div>
                        </div>
                        <div class="shortcut_box_full"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?bdate='.date('Y-m-d').'"><img src="/img/cal_pday20.gif" border="0" alt="'.trans('base.Day').'" title="'.trans('base.Day').'" class="small_link">'.trans('base.Day').'</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.date('Y-m-d').'"><img src="/img/cal_pweek20.gif" border="0" alt="'.trans('base.Week').'" title="'.trans('base.Week').'" class="small_link">'.trans('base.Week').'</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month?bdate='.date('Y-m-d').'"><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'" class="small_link">'.trans('base.Month').'</a></span></div>
                        <div class="shortcut_box_short" style="display:none"><span class="nowrap-grn schedule_userbox_item_grn"><a href="/schedule/personal_day?bdate='.date('Y-m-d').'"><img src="/img/cal_pday20.gif" border="0" alt="Day" title="'.trans('base.Day').'"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.date('Y-m-d').'"><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="'.trans('base.Week').'" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a href="/schedule/personal_month?bdate='.date('Y-m-d').'"><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'"></a></span></div>
                     </td>';
        $array_mobile = [];
        for($i = 0; $i < 7; $i++) {
            $object = new \stdClass();
            $date = date('Y-m-d', strtotime(" +" . $i . " days",strtotime($date_now)));
            $object->date = $date;
            $schedules = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                            ->whereDate('schedule.start_date', '<=', $date)->whereDate('schedule.end_date', '>=', $date)->whereNotIn('pattern',['2','4'])->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
            $object->schedule = $schedules;
            $todos = $this->todoRepo->getListByDate(\Auth::guard('member')->user()->id, $date);
                $html .= '<td valign="top" class="s_user_week normalEvent">
                        <div class="addEvent">
                           <a title="Add" href="/schedule/create?bdate=' . $date . '&amp;uid=' . \Auth::guard('member')->user()->id . '">
                              <div class="iconWrite-grn"></div>
                           </a>
                        </div>
                        <div class="js_customization_schedule_date_' . $date . '"></div>
                        <div class="groupWeekInfo"></div>';
            foreach ($todos as $todo) {
                $html .= '<div class="schedule_todo normalEventElement">
                               <img src="/assets/mobile/img/todoPersonalInSchedule16.png" border="0" alt=""><a href="' . route('frontend.todo.view', $todo->id) . '">' . $todo->title . '</a>
                          </div>';
            }

            foreach($schedules as $schedule){
            $menu = !is_null($schedule->menu) ? '<span class="event_color'.explode(';#',$schedule->menu)[1].'_grn">'.explode(';#',$schedule->menu)[0].'</span>' : '';
            if($schedule->pattern == 3){
                if($schedule->type_repeat == 'week' || $schedule->type_repeat == 'weekday'){
                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pweek20.gif" border="0" alt="'.trans('base.Week').'" title="'.trans('base.Week').'" class="small_link">';
                }elseif($schedule->type_repeat == 'day'){
                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pday20.gif" border="0" alt="'.trans('base.Day').'" title="'.trans('base.Day').'" class="small_link">';
                }else{
                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'" class="small_link">';
                }
             }else{
                $repeat = '';
             }
             $equipment_string = count($schedule->equipment) > 0 ? '<p>['.implode(',',$schedule->equipment()->pluck('name')->toArray()).']</p>' : '';
             $html .= '<div class="share normalEventElement   group_week_calendar_item">
                           <div class="listTime"><a href="'.route('frontend.schedule.view',$schedule->id).'">' .($schedule->none_time != 1 ? date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) : '' ). '</a></div>
                           <div class="'.($schedule->none_time != 1 ? 'groupWeekEventTitle' : 'groupWeekEventTitleAllday').'">
                              <a href="'.route('frontend.schedule.view',$schedule->id).'">'.$menu.' '.$schedule->title.' '.$equipment_string.' '.$repeat.'</a>
                           </div>
                        </div>';
            }
            $array_mobile[] = $object;
        }
        $html .='</td></tr>';
        $schedules_all = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                            ->whereDate('schedule.end_date', '>=', $date_now)->whereDate('schedule.start_date', '<=', date('Y-m-d', strtotime(" +6 days", strtotime($date_now))))->whereIn('pattern',['2','4'])->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
        foreach ($schedules_all as $key => $val) {
            $time = \App\Helpers\StringHelper::getAboutDay($date_now,$val->start_date,$val->end_date);
            $menu = is_null($val->menu) ? '' : '<span class="event_color' . explode(';#', $val->menu)[1] . '_grn">' . explode(';#', $val->menu)[0] . '</span>';
            if($val->pattern == 2){
                $html .= '<tr>
                            <td style="border-right:1px solid #C9C9C9"><br></td>
                            ' . ($time->first == 0 ? '' : '<td class="br_banner" colspan="' . $time->first . '"><br></td>'). '
                            <td class="s_banner normalEvent" colspan="' . $time->middle . '">
                            <div class="normalEventElement">'.((in_array(\Auth::guard('member')->user()->id,$val->member()->pluck('id')->toArray()) || $val->private == 0) ? '<a href="' . route('frontend.schedule.view', $val->id) . '"><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">' . $menu . ' ' . $val->title . '</a>' : '<a><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">Đã có lịch</a>' ).'</div></td>
                            ' . ($time->last == 0 ? '' : '<td class="br_banner" colspan="' . $time->last . '"><br></td>'). '
                          </tr>';
            }else{
                $html .= '<tr>
                            <td style="border-right:1px solid #C9C9C9"><br></td>
                            ' . ($time->first == 0 ? '' : '<td class="br_banner" colspan="' . $time->first . '"><br></td>'). '
                            <td class="s_task s_banner normalEvent" colspan="' . $time->middle . '">
                            <div><a href="' . route('frontend.schedule.view', $val->id) . '"><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">' . $menu . ' ' . $val->title . ' ('.$val->percent.'%)</a></div></td>
                            ' . ($time->last == 0 ? '' : '<td class="br_banner" colspan="' . $time->last . '"><br></td>'). '
                          </tr>';
            }
        }
        $departments = $this->departmentRepo->getAllStack(0);
        $equipments = $this->equipmentRepo->all();
        $equipment =[];
        $department = [];
        foreach($departments as $key=>$val){
            $object2 = new \stdClass();
            $object2->id = $val->id;
            $object2->name = $val->name;
            $object2->level = $val->level;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->type = "membership";
            $object2= json_encode($object2);
            $department[]=$object2;
        }
        foreach($equipments as $key=>$val){
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->count = "0";
            $object2->children = [];
            $object2= json_encode($object2);
            $equipment[]=$object2;
        }
        $department = implode(',',$department);
        $equipment = implode(',',$equipment);
        $todos = $this->todoRepo->getIndex($request);
        $notifications = \App\NotificationSchedule::where('to',\Auth::guard('member')->user()->id)->where('read_at',NULL)->orderBy('created_at','DESC')->get();
        $schedules_all_mobile = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                                ->where(function($query) {
                                    $query->whereBetween(DB::raw('DATE(start_date)'), array(date('Y-m-1'), date('Y-m-t')))
                                          ->orwhereBetween(DB::raw('DATE(end_date)'), array(date('Y-m-1'), date('Y-m-t')));
                                })->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
        $list_schedule_mobile = [];
        foreach ($schedules_all_mobile as $key => $val) {
            $object = new \stdClass();
            $object->calendar = 'Work';
            if(!is_null($val->menu)){
                $object->color = 'bg-color'.explode(';#',$val->menu)[1];
            }else{
                $object->color = 'bg-white';
            }
            if($val->pattern == 2){
               $object->pattern = 2;
               $object->eventName ='[All day] '.$val->title;
            }else{
               $object->pattern = 1;
               $object->eventName = $val->title;
            }
            $object->start_date = date('m/d/Y',strtotime($val->start_date));
            $object->end_date = date('m/d/Y',strtotime($val->end_date));
            $list_schedule_mobile[] = $object;
        }
        $list_schedule_mobile = json_encode($list_schedule_mobile);
        if (config('global.device') != 'pc') {
            $count_schedule = count(\Auth::guard('member')->user()->schedule);
            $count_project = count(\Auth::guard('member')->user()->project);
            $count_todolist = count(\Auth::guard('member')->user()->membertodo);
            $data = $this->scheduleRepo->getRecently(\Auth::guard('member')->user()->id);
            if($data){
                $schedule = new \stdClass();
                $schedule->title = $data->title;
                $schedule->id = $data->id;
                $schedule->day = 'Ngày ' .date('d',strtotime($data->start_date)).' tháng '.date('m',strtotime($data->start_date));
                $schedule->date = \App\Helpers\StringHelper::weekendDay(date('w',strtotime($data->start_date)));
                $schedule->time = date('H:i',strtotime($data->start_date));
            }else{
                $schedule = Null;
            }
            return view('mobile/home/view',compact('count_schedule','count_project','count_todolist','schedule'));
        } else {
            $list_member = $this->memberRepo->getAllNotMe();
            $department_html = \App\Helpers\StringHelper::getSelectDepartmentParent(0);
            return view('frontend/home/view',compact('date_now','html','todos','department','equipment','notifications','list_member','department_html'));
        }
    }
    public function changeLanguage($locale){
        if (in_array($locale, \Config::get('app.locales'))) {
          session(['locale' => $locale]);
        }
        return redirect()->back();
    }
    public function listMessenger(Request $request){
        $user_id = \Auth::guard('member')->user()->id;
        $groups = \App\GroupMember::where('member_id',$user_id)->pluck('group_id')->toArray();
        $all_messages_record = \App\GroupMessage::whereIn('group_id',$groups)->orderBy('created_at','desc')->pluck('group_id')->toArray(); // lấy ra tất cả id của tin nhắn trong các nhóm tìm được
        $all_messages_record = array_unique($all_messages_record); // lọc id trùng lặp - lấy ra id duy nhất
        $all_messages = array();
        $avatar = "";
        foreach($all_messages_record as $key => $record){
            if(\App\Group::where('id',$record)->first()->type == 99){
                $all_messages[$key]['avatar'] = '\assets2\img\gear.png';
                $all_messages[$key]['name'] = "Tin nhắn hệ thống";
            }
            else if(\App\Group::where('id',$record)->first()->name != null){
                $all_messages[$key]['avatar'] = \App\Group::where('id',$record)->first()->avatar;
                $all_messages[$key]['name'] = \App\Group::where('id',$record)->first()->name;
            }else{
                $receiver = \App\GroupMember::where('group_id',$record)->where('member_id','!=',$user_id)->first();
                $all_messages[$key]['avatar'] = \App\Member::where('id',$receiver->member_id)->first()->avatar;
                $all_messages[$key]['name'] = $receiver->name;
            }
            $all_messages[$key]['message'] = \App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->message;
            $all_messages[$key]['time'] = \App\Helpers\StringHelper::time_ago(\App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->created_at);
            $all_messages[$key]['id'] = $record;
            $all_messages[$key]['type'] = \App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->type;;
            $seen = \App\ReadMessage::where('message_id',\App\GroupMessage::where('group_id',$record)->orderBy('created_at', 'desc')->first()->id)
                                            ->where('member_id',\Auth::guard('member')->user()->id)
                                            ->where('group_id',$record)
                                            ->get();  
            if(count($seen)>0){
                $all_messages[$key]['seen'] = $seen[0]->seen;
            }else{
                $all_messages[$key]['seen'] = 1;
            }
        }
        if(\App\Group::where('id',$all_messages_record[0])->first()->type == 99){
            $group_name = "Tin nhắn hệ thống";
            $to_member_id = 0;
            $group_id = \App\Group::where('id',$all_messages_record[0])->first()->type;
            $to_type = 0;
            $avatar = '\assets2\img\gear.png';
        }else{
            $group_name = \App\Group::where('id',$all_messages_record[0])->first()->name;
            $to_member = \App\GroupMember::where('group_id',$all_messages_record[0])->where('member_id','!=',\Auth::guard('member')->user()->id)->first();
            $to_type = 0; // Group
            $to_member_id = $to_member->member_id;
            if($group_name == null){
                $group_name = $to_member->name;
                $to_type = 1; // Member
            }
            $group_id = $all_messages_record[0];
            // $messages = \App\GroupMessage::where('group_id',$all_messages_record[0])->get();
            $avatar = $all_messages[0]['avatar'];
        }
        $group = \App\Group::find($group_id);
        $group_type = \App\Group::where('id',$all_messages_record[0])->first()->type;
        $messages = \App\GroupMessage::where('group_id',$all_messages_record[0])->get();
        foreach($messages as $key=>$val){
            if($val->reply_id > 0){
                $val->reply = \App\GroupMessage::find($val->reply_id);
            }
        }
        return view('mobile/messenger/list', compact('group','messages','user_id','all_messages','group_id','group_type','group_name','avatar','to_type','to_member_id'));
    }
    
    public function detailMessenger($id){
        $group = \App\Group::find($id);
        $messages = \App\GroupMessage::where('group_id',$id)->get();
        return view('mobile/messenger/detail',compact('group','messages'));
    }
    
    public function listNotification(){
        return view('mobile/notification/list');
    }

}
