<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ScheduleRepository;
use Repositories\WorkRepository;
use Spatie\GoogleCalendar\Event;
use Repositories\EquipmentRepository;
use App\Repositories\MemberRepository;
use Repositories\DepartmentRepository;
use Repositories\FileRepository;
use Session;
use Repositories\NotificationScheduleRepository;
use Repositories\ToDoRepository;
use App\Jobs\SendNotificationJob;
use Symfony\Component\Process\Process;
use Stevebauman\Purify\Facades\Purify;

class ScheduleController extends Controller {

    public function __construct(ToDoRepository $todoRepo, NotificationScheduleRepository $notificationSchRepo, FileRepository $fileRepo, DepartmentRepository $departmentRepo, MemberRepository $memberRepo, EquipmentRepository $equipmentRepo, ScheduleRepository $scheduleRepo) {
        $this->scheduleRepo = $scheduleRepo;
        $this->equipmentRepo = $equipmentRepo;
        $this->memberRepo = $memberRepo;
        $this->departmentRepo = $departmentRepo;
        $this->fileRepo = $fileRepo;
        $this->todoRepo = $todoRepo;
        $this->notificationSchRepo = $notificationSchRepo;
    }

    public function index(Request $request) {
        Session::put('redirect_route','frontend.schedule.index');
        if ($request->get('bdate')) {
            $date_now = $request->get('bdate');
        } else {
            $date_now = date('Y-m-d');
        }
        if(is_array($date_now)){
            abort(404);
        }
        if ($request->get('uid')) {
            $members = \App\Member::whereIn('id', explode(',', $request->get('uid')))->paginate(10);
        } else {
            if ($request->get('gid') == 'search' || $request->get('type_search') == 'user') {
                $members = $this->memberRepo->searchByKeywordPaginate($request->get('search_text'));
            }elseif($request->get('gid') == 'all'){
                $members = $this->memberRepo->getPaginate(9);
            }elseif($request->get('gid')){
                    $department = $this->departmentRepo->find($request->get('gid'));
                        $department_id = $department->getIds();
                        $members = $this->memberRepo->getByDepartmentPaginate($department_id, \Auth::guard('member')->user()->id);
            }elseif(!$request->get('eid') && !$request->get('gid')) {
                if(!is_null(\Auth::guard('member')->user()->department)){
                    $department_id = \Auth::guard('member')->user()->department->getIds();
                }else{
                    $department_id= [];
                }
                $members = $this->memberRepo->getByDepartmentPaginate($department_id, \Auth::guard('member')->user()->id);
            }
        }
        $list_member = [];
        $list_member[] = \Auth::guard('member')->user();
        if (isset($members) && count($members) > 0) {
            foreach($members as $key=>$member){
                $list_member[] = $member;
            }
        }
        // start list weekend
        $html = '<tr><td class="s_domain_week"><span class="domain">(UTC+07:00) VietNam</span></td>';
        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', strtotime(" +" . $i . " days", strtotime($date_now)));
            $html .= '<td class="s_date_' . strtolower(date('l', strtotime($date))) . '_week" align="center">&nbsp;<a href="/schedule/group_day?bdate=' . $date . '">' . date('D, d F', strtotime($date)) . '</a></td>';
        }
        foreach ($list_member as $key => $member) {
            // start get list schedule normal, repeat
            if (is_null($member->file())) {
                $avatar = '<div class="profileImageUser-grn"></div>';
            } else {
                $avatar = '<div class="user_photo_grn" style="background-image: url(/' . $member->file()->link . ');" aria-label=""></div>';
            }
            $html .= '</tr><tr class="js_customization_schedule_user_id_' . $member->id . '">
                    <td valign="top" class="calendar_rb_week userBox">
                        <div class="userElement profileImageBase-grn profileImageBaseSchedule-grn">
                           <dl>
                              <dt>
                                 <a >
                                    <div class="profileImage-grn">
                                       <div class="profileImageFrame-grn">
                                          ' . $avatar . '
                                       </div>
                                    </div>
                                 </a>
                              </dt>
                              <dd><a >' . htmlentities($member->full_name) . '</a></dd>
                           </dl>
                           <div class="clear_both_0px"></div>
                        </div>
                        <div class="shortcut_box_full"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?uid=' . $member->id . '&bdate='.$date_now.'"><img src="/img/cal_pday20.gif" border="0" alt="'.trans('base.Day').'" title="'.trans('base.Day').'" class="small_link">'.trans('base.Day').'</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?uid=' . $member->id . '&bdate='.$date_now.'"><img src="/img/cal_pweek20.gif" border="0" alt="'.trans('base.Week').'" title="'.trans('base.Week').'" class="small_link">'.trans('base.Week').'</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month?uid=' . $member->id . '&bdate='.$date_now.'"><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'" class="small_link">'.trans('base.Month').'</a></span></div>
                        <div class="shortcut_box_short" style="display:none"><span class="nowrap-grn schedule_userbox_item_grn"><a href="/schedule/personal_day?uid=' . $member->id . '&bdate='.$date_now.'"><img src="/img/cal_pday20.gif" border="0" alt="'.trans('base.Day').'" title="'.trans('base.Day').'"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?uid=' . $member->id . '&bdate='.$date_now.'"><img src="/img/cal_pweek20.gif" border="0" alt="'.trans('base.Week').'" title="'.trans('base.Week').'" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a href="/schedule/personal_month?uid=' . $member->id . '&bdate='.$date_now.'"><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'"></a></span></div>

                     </td>';
            for ($i = 0; $i < 7; $i++) {
                $date = date('Y-m-d', strtotime(" +" . $i . " days", strtotime($date_now)));
                $schedules = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', $member->id)
                                ->whereDate('schedule.start_date', '<=', $date)->whereDate('schedule.end_date', '>=', $date)->whereNotIn('pattern',['2','4'])->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
                $todos = $this->todoRepo->getListByDate($member->id, $date);
                $html .= '<td valign="top" class="s_user_week normalEvent">
                        <div class="addEvent">
                           <a title="Add" href="/schedule/create?bdate=' . $date . '&amp;uid=' . $member->id . '">
                              <div class="iconWrite-grn"></div>
                           </a>
                        </div>
                        <div class="js_customization_schedule_date_' . $date . '"></div>
                        <div class="groupWeekInfo"></div>';
                foreach ($todos as $todo) {
                    $html .= '<div class="schedule_todo normalEventElement">
                                   <img src="/assets/mobile/img/todoPersonalInSchedule16.png" border="0" alt=""><a href="' . route('frontend.todo.view', $todo->id) . '">' . $todo->title . '</a>
                              </div>';
                }
                foreach ($schedules as $schedule) {
                    $menu = !is_null($schedule->menu) ? '<span class="event_color' . explode(';#', $schedule->menu)[1] . '_grn">' . explode(';#', $schedule->menu)[0] . '</span>' : '';
                    if(in_array(\Auth::guard('member')->user()->id,$schedule->member->pluck('id')->toArray()) || $schedule->private == 0){
                        $html .= '<div class="share normalEventElement   group_week_calendar_item">
                                  <div class="listTime"><a href="' . route('frontend.schedule.view', $schedule->id) . '">' .($schedule->none_time != 1 ? date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) : '' ). '</a></div>
                                  <div class="groupWeekEventTitle"><a href="' . route('frontend.schedule.view', $schedule->id) . '">'.($schedule->checkConflict($member->id) ? '<img src="/img/attention16.gif">' : '').''.$menu.' '.$schedule->title . ' </a></div>
                               </div>';
                    }else{
                        $html .= '<div class="share normalEventElement   group_week_calendar_item">
                                  <div class="listTime"><a>' .($schedule->none_time != 1 ? date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) : '' ). '</a></div>
                                  <div class="groupWeekEventTitle"><a>Đã có lịch</a></div>
                               </div>';
                    }
                }
            }
            $html .= '</td></tr>';
            // end get list schedule normal, repeat
            // start get list schedule all
            $schedules_alls = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', $member->id)
                            ->whereDate('schedule.end_date', '>=', $date_now)->whereDate('schedule.start_date', '<=', date('Y-m-d', strtotime(" +6 days", strtotime($date_now))))->whereIn('pattern',['2','4'])->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
            foreach ($schedules_alls as $key => $val) {
                $time = \App\Helpers\StringHelper::getAboutDay($date_now,$val->start_date,$val->end_date);
                $menu = is_null($val->menu) ? '' : '<span class="event_color' . explode(';#', $val->menu)[1] . '_grn">' . explode(';#', $val->menu)[0] . '</span>';
                if($val->pattern == 2){
                    $html .= '<tr>
                                <td style="border-right:1px solid #C9C9C9"><br></td>
                                ' . ($time->first == 0 ? '' : '<td class="br_banner" colspan="' . $time->first . '"><br></td>'). '
                                <td class="s_banner normalEvent" colspan="' . $time->middle . '">
                                <div class="normalEventElement">'.((in_array(\Auth::guard('member')->user()->id,$val->member()->pluck('id')->toArray()) || $val->private == 0) ? '<a href="' . route('frontend.schedule.view', $val->id) . '"><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">' . $menu . ' ' . $val->title . '</a>' : '<a><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">Đã có lịch</a>' ).'</div></td>
                                ' . ($time->last == 0 ? '' : '<td class="br_banner" colspan="' . $time->last . '"><br></td>'). '
                              </tr>';
                }else{
                    $html .= '<tr>
                                <td style="border-right:1px solid #C9C9C9"><br></td>
                                ' . ($time->first == 0 ? '' : '<td class="br_banner" colspan="' . $time->first . '"><br></td>'). '
                                <td class="s_task s_banner normalEvent" colspan="' . $time->middle . '">
                                <div><a href="' . route('frontend.schedule.view', $val->id) . '"><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">' . $menu . ' ' . $val->title . ' ('.$val->percent.'%)</a></div></td>
                                ' . ($time->last == 0 ? '' : '<td class="br_banner" colspan="' . $time->last . '"><br></td>'). '
                              </tr>';
                }
            }
            // end get list schedule all
        }
        // end list schedule by member
        for ($i = 1; $i <= 7; $i++) {
            $date_check_next = date('w', strtotime(" +" . $i . " days", strtotime($date_now)));
            $date_check_prev = date('w', strtotime(" -" . $i . " days", strtotime($date_now)));
            if ($date_check_next == '0') {
                $next_week = date('Y-m-d', strtotime(" +" . $i . " days", strtotime($date_now)));
            }
            if ($date_check_prev == '0') {
                $prev_week = date('Y-m-d', strtotime(" -" . $i . " days", strtotime($date_now)));
            }
        }
        $departments = $this->departmentRepo->getAllStack(0);
        $equipments = $this->equipmentRepo->all();
        $equipment = [];
        $department = [];
        $object2 = new \stdClass();
        $object2->id = 'all';
        $object2->name = 'Tất cả';
        $object2->level = 1;
        $object2->extra_param = "";
        if($request->get('gid') == 'all'){
            $object2->is_selected = "selected";
        }
        $object2->type = "membership";
        $object2 = json_encode($object2);
        $department[] = $object2;
        foreach ($departments as $key => $val) {
            $object2 = new \stdClass();
            $object2->id = $val->id;
            $object2->name = $val->name;
            $object2->level = $val->level;
            $object2->extra_param = "";
            $object2->type = "membership";
            if($request->get('gid') == $val->id){
                $object2->is_selected = "selected";
            }
            $object2 = json_encode($object2);
            $department[] = $object2;
        }
        foreach ($equipments as $key => $val) {
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->count = "0";
            $object2->children = [];
            $object2 = json_encode($object2);
            $equipment[] = $object2;
        }
        $department = implode(',', $department);
        $equipment = implode(',', $equipment);
        $calendar_html = $this->scheduleRepo->showCalendar(date('Y-m-01', strtotime($date_now)), 'schedule/index');
        $mobile_html = '';
        if(isset($_GET['uid']) && $_GET['uid'] > 0 && $_GET['uid'] != \Auth::guard('member')->user()->id){
            $member_id = $_GET['uid'];
        }else{
            $member_id = \Auth::guard('member')->user()->id;
        }
        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', strtotime(" +" . $i . " days", strtotime($date_now)));
            $schedules = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', $member_id)
                            ->whereDate('schedule.start_date', '<=', $date)->whereDate('schedule.end_date', '>=', $date)->get();
            $list_schedule_mobile = '';
            if (count($schedules) > 0) {
                foreach ($schedules as $schedule) {
                    $menu = !is_null($schedule->menu) ? '<span class="badge mr-1 bg-color' . explode(';#', $schedule->menu)[1] . ' text-light">' . explode(';#', $schedule->menu)[0] . '</span>' : '';
                    if ($schedule->pattern == 2) {
                        $list_schedule_mobile .= '<li class="py-2 pd-0">
                                                    <a href="' . route('frontend.schedule.view', $schedule->id) . '">
                                                       <div class="in">
                                                          <div class="d-flex flex-row pt-1">
                                                             <ion-icon name="alarm-outline" class="clock"></ion-icon>
                                                             <span class="d-inline-block text-truncate text-success" style="max-width: 250px;">'.date('d/m',strtotime($schedule->start_date)).'~'.date('d/m',strtotime($schedule->end_date)).'</span>
                                                          </div>
                                                          <div class="text-muted mt-1">
                                                            ' . $menu . '
                                                            <h4 class="mb-0">'.$schedule->title.'</h4>
                                                          </div>
                                                       </div>
                                                    </a>
                                                 </li>';
                    } else {
                        $list_schedule_mobile .= '<li class="py-2 pd-0">
                                                    <a href="' . route('frontend.schedule.view', $schedule->id) . '">
                                                       <div class="in">
                                                          <div class="in text-dark d-flex flex-row pt-1">
                                                             <ion-icon name="alarm-outline" class="clock"></ion-icon>
                                                             <span>' . date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) . '</span>
                                                          </div>
                                                          <div class="text-muted mt-1 pb-1 d-flex flex-row">
                                                            ' . $menu . '
                                                            <h4>'.$schedule->title.'</h4>
                                                          </div>
                                                       </div>
                                                    </a>
                                                 </li>';
                    }
                }
            } else {
                $list_schedule_mobile .= '<li class="py-2">No appointments.</li>';
            }
            $mobile_html .= '<div class="card mt-2">
                                <div class="card-body pb-1">
                                    <div class="card-title d-flex justify-content-between">
                                        <h3 class="mb-0">'.date('l, d F Y',strtotime($date)).'</h3>
                                        <a href="'.route('frontend.schedule.create',['bdate'=>$date]).'" class="text-warning"><ion-icon name="create-outline" role="img" class="md hydrated" aria-label="create outline"></ion-icon></a>
                                    </div>
                                    <ul class="listview flush transparent">
                                        '.$list_schedule_mobile.'
                                    </ul>
                                </div>
                        </div>';
        }
        if ($request->get('eid')) {
            $facility = $this->equipmentRepo->find($request->get('eid'));
            $html .= '</tr><tr class="js_customization_schedule_user_id_f' . $facility->id . '">
                        <td valign="top" class="calendar_rb_week userBox">
                            <span class="nowrap-grn ">
                               <a href="#">
                                    <img src="/img/facility20.gif" border="0" alt="">' . $facility->name . '
                               </a>
                            </span>
                            <div class="shortcut_box_facility_grn">
                                <span class="nowrap-grn schedule_userbox_item_grn">
                                    <a class="small_link" href="' . route('frontend.schedule.personal_day', ['bdate' => $date_now, 'eid' => $facility->id]) . '">
                                    <img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">
                                    </a>
                                </span>
                                <span class="nowrap-grn schedule_userbox_item_grn">
                                    <a class="small_link" href="' . route('frontend.schedule.personal_week', ['bdate' => $date_now, 'eid' => $facility->id]) . '">
                                    <img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">
                                    </a>
                                </span>
                                <span class="nowrap-grn schedule_userbox_item_grn">
                                    <a class="small_link" href="' . route('frontend.schedule.personal_month', ['bdate' => $date_now, 'eid' => $facility->id]) . '">
                                    <img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">
                                    </a>
                                </span>
                            </div>
                         </td>';
            for ($i = 0; $i < 7; $i++) {
                $date = date('Y-m-d', strtotime(" +" . $i . " days", strtotime($date_now)));
                $schedules = \App\Schedule::join('equipment_schedule', 'equipment_schedule.schedule_id', '=', 'schedule.id')->where('equipment_schedule.equipment_id', $facility->id)
                                ->whereDate('schedule.start_date', '<=', $date)->whereDate('schedule.end_date', '>=', $date)->where('pattern',['2','4'])->get();
                $html .= '<td valign="top" class="s_user_week normalEvent" >
                        <div class="addEvent">
                           <a title="Add" href="/schedule/create?bdate=' . $date . '&amp;eid=' . $facility->id . '&amp;gid=&amp;referer_key=8b883bbc87280ec72b9d3f7195804ef0">
                              <div class="iconWrite-grn"></div>
                           </a>
                        </div>
                        <div class="js_customization_schedule_date_' . $date . '"></div>
                        <div class="groupWeekInfo"></div>';
                foreach ($schedules as $schedule) {
                    $html .= '<div class="share normalEventElement   group_week_calendar_item">
                               <div class="listTime"><a href="' . route('frontend.schedule.view', $schedule->id) . '">' .($schedule->none_time != 1 ? date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) : '' ). '</a></div>
                               <div class="groupWeekEventTitle"><a href="' . route('frontend.schedule.view', $schedule->id) . '"><span class="event_color' . explode(';#', $schedule->menu)[1] . '_grn">' . explode(';#', $schedule->menu)[0] . '</span>' . $schedule->title . ' </a></div>
                         </div>';
                }
            }
            $html .= '</td></tr>';
            $members = Null;
        }
        if (config('global.device') != 'pc') {
            if($request->get('keyword')){
                if(\App\Member::where('full_name',$request->get('keyword'))->first()){
                    $member_id = \App\Member::where('full_name',$request->get('keyword'))->first()->id;
                }else{
                    $member_id = \Auth::guard('member')->user()->id;
                }
                $schedules_all_mobile = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', $member_id)
                                ->where(function($query) {
                                    $query->whereBetween(\DB::raw('DATE(start_date)'), array(date('Y-m-1'), date('Y-m-t')))
                                          ->orwhereBetween(\DB::raw('DATE(end_date)'), array(date('Y-m-1'), date('Y-m-t')));
                                })->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
                $schedules = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', $member_id)
                                ->whereDate('start_date','<=',$date_now)->where('start_date','>=',$date_now)->get();
            }else{
                $schedules_all_mobile = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                                ->where(function($query) {
                                    $query->whereBetween(\DB::raw('DATE(start_date)'), array(date('Y-m-1'), date('Y-m-t')))
                                          ->orwhereBetween(\DB::raw('DATE(end_date)'), array(date('Y-m-1'), date('Y-m-t')));
                                 })->orderByRaw('TIME_FORMAT(schedule.start_date, "%H%i")','ASC')->get();
                $schedules = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                                ->whereDate('start_date','<=',$date_now)->where('start_date','>=',$date_now)->get();
                $member_id = \Auth::guard('member')->user()->id;
            }
            $list_schedule_mobile = [];
            foreach ($schedules_all_mobile as $key => $val) {
                $object = new \stdClass();
                $object->calendar = 'Work';
                $object->id = $val->id;
                if(!is_null($val->menu)){
                    $object->color = 'bg-color'.explode(';#',$val->menu)[1];
                }else{
                    $object->color = 'bg-white';
                }
                if($val->pattern == 2){
                   $object->pattern = 2;
                   $object->eventName ='[All day] '.$val->title;
                }else{
                   $object->pattern = 1;
                   $object->eventName = $val->title;
                }
                $object->start_date = date('m/d/Y',strtotime($val->start_date));
                $object->end_date = date('m/d/Y',strtotime($val->end_date));
                $list_schedule_mobile[] = $object;
            }
            $list_schedule_mobile = json_encode($list_schedule_mobile);
            return view('mobile/schedule/index', compact('member_id','schedules','list_schedule_mobile','mobile_html', 'date_now', 'next_week', 'prev_week', 'department', 'calendar_html', 'departments', 'equipment'));
        } else {
            $list_member = $this->memberRepo->getAllNotMe();
            $department_html = \App\Helpers\StringHelper::getSelectDepartmentParent(0);
            return view('frontend/schedule/index', compact('department_html','list_member','html', 'date_now', 'next_week', 'prev_week', 'department', 'calendar_html', 'departments', 'equipment','members'));
        }
    }

    public function create(Request $request) {
        $input = $request->all();
        if (isset($input['bdate'])) {
            $date = $input['bdate'];
        } else {
            $date = date('Y-m-d');
        }
        $schedules = $this->scheduleRepo->getByMemberDay([\Auth::guard('member')->user()->id], $date);
        $first_day = $this->scheduleRepo->getByMemberFirstDay([\Auth::guard('member')->user()->id],$date);
        $last_day = $this->scheduleRepo->getByMemberLastDay([\Auth::guard('member')->user()->id],$date);
        if(!is_null($first_day) && strtotime(date('Y-m-d',strtotime($first_day->start_date))) < strtotime($date)){
            $first_hour = 0;
        }else{
            if(!is_null($first_day) && date('H',strtotime($first_day->start_date)) < 8){
                $first_hour = date('H',strtotime($first_day->start_date));
            }else{
                $first_hour = 8;
            }
        }
        if(!is_null($last_day) && strtotime(date('Y-m-d',strtotime($last_day->end_date))) > strtotime($date)){
                $last_hour = 23;
        }elseif($last_day && date('H',strtotime($last_day->end_date)) > 18){
                $last_hour = date('H',strtotime($last_day->end_date));
        }else{
                $last_hour = 18;
        }
        $total_hour = $last_hour - $first_hour + 1;
        $total_minute = $total_hour * 6;
        $html = '';
        $td_head = '';
        for($i = $first_hour;$i < $last_hour + 1;$i++){
            if( $i<12 ){
                $mea = 'm';
            }elseif($i> 11 && $i< 18){
                $mea = 'e';
            }else{
                $mea = 'a';
            }
            $td_head .= '<td align="center" class="'.$mea.'" colspan="6">'.$i.'</td>';
        }
        $html .= '<tr class="day_table_time_login bar_login_timezone">
                    <td width="22%" class="group_day_calendar_timebar1"><span class="domain">(UTC+07:00) VietNam</span></td>
                    <td width="10%" class="group_day_calendar_timebar2"><br></td>
                    '.$td_head.'
                </tr>';
        $time_line = '';
        for ($i = 0; $i < $total_minute; $i++) {
            if (isset($input['start_hour']) && isset($input['end_hour'])) {
                if ($i >= $in_time1 && $i < $out_time1) {
                    $time_line .= '<td class="n"></td>';
                } else {
                    if ($i >= 0 && $i < ((12 - $first_hour) * 6)) {
                        $time_line .= '<td class="m"></td>';
                    }
                    if ($i >= ((12 - $first_hour) * 6) && $i < ((18 - $first_hour) * 6)) {
                        $time_line .= '<td class="e"></td>';
                    }
                    if ($i >= ((18 - $first_hour) * 6) && $i < $total_minute) {
                        $time_line .= '<td class="a"></td>';
                    }
                }
            } else {
                if ($i >= 0 && $i < ((12 - $first_hour) * 6)) {
                    $time_line .= '<td class="m"></td>';
                }
                if ($i >= ((12 - $first_hour) * 6) && $i < ((18 - $first_hour) * 6)) {
                    $time_line .= '<td class="e"></td>';
                }
                if ($i >= ((18 - $first_hour) * 6) && $i < $total_minute) {
                    $time_line .= '<td class="a"></td>';
                }
            }
        }
        $html .= '<tr class="day_table_time_login bar_login_timezone">
                    <td class="group_day_calendar_timebar_sec1"><img src="/img/spacer1.gif" border="0" alt=""></td>
                    <td class="group_day_calendar_timebar_sec2"><img src="/img/spacer1.gif" border="0" alt=""></td>
                    ' . $time_line . '
                 </tr>';
        $arr = [];
        if (count($schedules) > 0) {
            foreach ($schedules as $key => $schedule) {
                if(strtotime(date('Y-m-d',strtotime($schedule->start_date))) < strtotime($date)){
                    $start_time = date('Y-m-d 00:00', strtotime($date));
                    $start_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                }else{
                    $start_time = date('Y-m-d H:i', strtotime($schedule->start_date));
                    $start_time1 = date('Y-m-d '.$first_hour.':00', strtotime($schedule->start_date));
                }
                $start = (strtotime($start_time) - strtotime($start_time1)) / (10 * 60);
                if(strtotime(date('Y-m-d',strtotime($schedule->end_date))) > strtotime($date)){
                    $end_time = date('Y-m-d 24:00', strtotime($date));
                    $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                }else{
                    if(strtotime(date('Y-m-d',strtotime($schedule->start_date))) < strtotime($date)){
                        $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                    }else{
                        $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($schedule->start_date));
                    }
                    $end_time = date('Y-m-d H:i', strtotime($schedule->end_date));
                }
                $end = (strtotime($end_time) - strtotime($end_time1)) / (10 * 60);
                $object = new \stdClass();
                $object->start = $start;
                $object->end = $end;
                $object->status = 1;
                $object->count = $end - $start;
                $object->schedule = $schedule;
                $arr[] = $object;
            }
            $data = $this->scheduleRepo->getAboutTime($arr,$total_minute)['data'];
            $duplicate = $this->scheduleRepo->getAboutTime($arr,$total_minute)['duplicate'];
            $line_time = '';
            foreach ($data as $key => $value) {
                if ($value->status == 0) {
                    $line_time .= '<td colspan="' . $value->count . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>';
                } else {
                    $line_time .= '<td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                    <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                        <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $value->schedule->title . '</a>
                                    </div>
                                  </td>
                                  ';
                }
            }
            if (count($duplicate) > 0) {
            foreach ($duplicate as $key => $val) {
                $dup_html = '';
                $title_facility = count($val->schedule->equipment) > 0 ? '[' . implode(',', $val->schedule->equipment()->pluck('name')->toArray()) . ']' : '';
                if($val->schedule->pattern == 3){
                    if($val->schedule->type_repeat == 'week' || $val->schedule->type_repeat == 'weekday'){
                        $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">';
                    }elseif($val->schedule->type_repeat == 'day'){
                        $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">';
                    }else{
                        $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">';
                    }
                 }else{
                    $repeat = '';
                 }
                $menu = is_null($val->schedule->menu) ? '' : '<span class="event_color' . explode(';#', $val->schedule->menu)[1] . '_grn">' . explode(';#', $val->schedule->menu)[0] . '</span>';
                if(in_array(\Auth::guard('member')->user()->id,$val->schedule->member->pluck('id')->toArray()) || $val->schedule->private == 0){
                     $title = '<a href="' . route('frontend.schedule.view', $val->schedule->id) . '">
                                    <span class="attention"><img src="/img/attention16.gif" border="0" title="Conflicting appointments" alt="Conflicting appointments" align="absmiddle"></span>' . $menu . ' ' . $val->schedule->title . ' ' . $title_facility . ' '.$repeat.'
                               </a>';
                }else{
                     $title = '<a><span class="attention"><img src="/img/attention16.gif" border="0" title="Conflicting appointments" alt="Conflicting appointments" align="absmiddle"></span>Đã có lịch</a>';
                }
                $title_facility = '';
                if ($val->start > 0) {
                    if ($val->end >= $total_minute) {
                        $dup_html .= '<td colspan="' . $val->start . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>
                                        <td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . ($total_minute - $val->start) . '">
                                            <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                '.$title.'
                                            </div>
                                        </td>';
                    } else {
                        $dup_html .= '<td colspan="' . $val->start . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>
                                      <td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->count . '">
                                            <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                 '.$title.'
                                            </div>
                                      </td>
                                      <td colspan="' . ($total_minute - $val->end) . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>';
                    }
                } else {
                    if ($val->end >= $total_minute) {
                        $dup_html .= '<td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->end . '">
                                        <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                '.$title.'
                                        </div>
                                      </td>';
                    } else {
                        $dup_html .= '<td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->count . '">
                                            <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                 '.$title.'
                                            </div>
                                      </td>
                                      <td colspan="' . ($total_minute - $val->end) . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>';
                    }
                }
                $line_time .= '<tr>
                <td class="group_day_calendar_conflict_head1"><br></td>
                <td class="group_day_calendar_conflict_head2"><br></td>
                ' . $dup_html . '
            </tr>';
            }
        }
            $img = '<img src="/img/loginuser20.gif" border="0" alt="">';
            $html .= '<tr>
                    <td class="group_day_calendar_user">
                       <div class="userElement"><span class="span_user"><span class="user-grn inline_block_grn">' . $img . ' ' . \Auth::guard('member')->user()->full_name . '</span></span></div>
                       &nbsp;<br>
                    </td>
                    <td class="normalEvent group_day_calendar_event_cell">
                       &nbsp;
                    </td>
                    ' . $line_time . '
                 </tr>';
        } else {
            $img = '<img src="/img/loginuser20.gif" border="0" alt="">';
            $html .= '<tr>
                        <td class="group_day_calendar_user">
                           <div class="userElement"><span class="span_user"><span class="user-grn inline_block_grn">' . $img . ' ' . \Auth::guard('member')->user()->full_name . '</span></span></div>
                           &nbsp;<br>
                        </td>
                        <td class="normalEvent group_day_calendar_event_cell">
                           &nbsp;
                        </td>
                        <td colspan="'.$total_minute.'" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                     </tr>';
        }
        //end 1 member
        $schedules1 = $this->scheduleRepo->getByMemberAllDay([\Auth::guard('member')->user()->id], $date);
        foreach ($schedules1 as $key => $val) {
            $html .= '<tr>
                        <td class="group_day_calendar_banner_head1"><br></td>
                        <td class="group_day_calendar_banner_head2"><br></td>
                        <td class="group_day_calendar_banner_item banner_color" colspan="'.$total_minute.'"><a href="' . route('frontend.schedule.view', $val->id) . '" ><img src="/img/banner16.gif" border="0" alt="">' . $val->title . '</a></td>
                    </tr>';
        }
        $member_html = \App\Helpers\StringHelper::getSelectMemberOption(\App\Member::where('is_deleted', 0)->get());
        $departments = $this->departmentRepo->getAllStack(0);
        $equipments = $this->equipmentRepo->all();
        $equipment = [];
        $department = [];
        foreach ($departments as $key => $val) {
            $object2 = new \stdClass();
            $object2->id = $val->id;
            $object2->name = $val->name;
            $object2->level = $val->level;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->type = "membership";
            $object2 = json_encode($object2);
            $department[] = $object2;
        }
        foreach ($equipments as $key => $val) {
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->count = "0";
            $object2->children = [];
            $object2 = json_encode($object2);
            $equipment[] = $object2;
        }
        $department = implode(',', $department);
        $equipment = implode(',', $equipment);
        $equipment_html = \App\Helpers\StringHelper::getSelectListEquipment($equipments);
        if ($request->get('bdate')) {
            $date = $request->get('bdate');
        } else {
            $date = date('Y-m-d');
        }
        if (isset($input['schedule_id'])) {
            $schedule = $this->scheduleRepo->find($input['schedule_id']);
            $start_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime(($schedule->pattern  == 3 ? $schedule->start_repeat : $schedule->start_date))));
            $start_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime(($schedule->pattern  == 3 ? $schedule->start_repeat : $schedule->start_date))), date('m', strtotime(($schedule->pattern  == 3 ? $schedule->start_repeat : $schedule->start_date))), date('d', strtotime(($schedule->pattern  == 3 ? $schedule->start_repeat : $schedule->start_date))));
            $start_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime(($schedule->pattern  == 3 ? $schedule->start_repeat : $schedule->start_date))));
            $end_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime(($schedule->pattern  == 3 ? $schedule->end_repeat : $schedule->end_date))));
            $end_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime(($schedule->pattern  == 3 ? $schedule->end_repeat : $schedule->end_date))), date('m', strtotime(($schedule->pattern  == 3 ? $schedule->end_repeat : $schedule->end_date))), date('d', strtotime(($schedule->pattern  == 3 ? $schedule->end_repeat : $schedule->end_date))));
            $end_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime(($schedule->pattern  == 3 ? $schedule->end_repeat : $schedule->end_date))));
            if($schedule->none_time != 1){
                $start_minute_html = \App\Helpers\StringHelper::getSelectMinute(date('i', strtotime($schedule->start_date)));
                $start_hour_html = \App\Helpers\StringHelper::getSelectHour(date('H', strtotime($schedule->start_date)));
                $end_hour_html = \App\Helpers\StringHelper::getSelectHour(date('H', strtotime($schedule->end_date)));
                $end_minute_html = \App\Helpers\StringHelper::getSelectMinute(date('i', strtotime($schedule->end_date)));
            }else{
                $start_minute_html = \App\Helpers\StringHelper::getSelectMinute();
                $start_hour_html = \App\Helpers\StringHelper::getSelectHour();
                $end_hour_html = \App\Helpers\StringHelper::getSelectHour();
                $end_minute_html = \App\Helpers\StringHelper::getSelectMinute();
            }
            $select_month_html = '';
            $select_week_html = '';
            if($schedule->type_repeat == 'week'){
                foreach(\App\Schedule::Weekend_arr as $key=>$val){
                    $select_week_html .=' <option value="'.$key.'" '.(in_array($key,explode(',',$schedule->wday)) ? 'selected' : '').'>'.$val.'</option>';
                }
            }else{
                foreach(\App\Schedule::Weekend_arr as $key=>$val){
                    $select_week_html .=' <option value="'.$key.'">'.$val.'</option>';
                }
            }
            if($schedule->type_repeat == 'month'){
                for($i=1;$i<32;$i++){
                    $select_month_html .=' <option value="'.$i.'" '.(in_array($i,explode(',',$schedule->wday)) ? 'selected' : '').'>'.$i.'</option>';
                }
            }else{
                for($i=1;$i<32;$i++){
                    $select_month_html .=' <option value="'.$i.'">'.$i.'</option>';
                }
            }
            $list = new \stdClass();
            $list_member_selected = new \stdClass();
            foreach ($schedule->member as $key => $val) {
                $object = new \stdClass();
                $object->type = 'user';
                $object->id = $val->id;
                $object->foreignKey = $val->full_name;
                $object->displayName = $val->full_name;
                $object->image = $val->avatar;
                $object->isInvalidUser = false;
                $object->isNotUsingApp = false;
                $object->isLoginUser = $val->id == \Auth::guard('member')->user()->id ? true : false;
                $list->$key = $object;
            }
            $list_member_selected->list = $list;
            $list_member_selected = json_encode($list_member_selected);
            $list = new \stdClass();
            $list_facility_selected = new \stdClass();
            foreach ($schedule->equipment as $key => $val) {
                $object = new \stdClass();
                $object->type = 'facility';
                $object->id = $val->id;
                $object->foreignKey = $val->name;
                $object->displayName = $val->name;
                $object->approval = 0;
                $list->$key = $object;
            }
            $list_facility_selected->list = $list;
            $list_facility_selected = json_encode($list_facility_selected);
            $members = $this->memberRepo->getAllEdit($schedule->member()->pluck('id')->toArray());
            if($schedule->pattern  == 3){
                if (config('global.device') != 'pc') {
                    return view('mobile/schedule/create_repeat', compact('members','equipments','list_facility_selected','list_member_selected','select_month_html','select_week_html','equipment', 'schedule', 'date', 'html', 'department', 'departments', 'equipment_html', 'start_month_html', 'start_day_html', 'start_year_html', 'start_hour_html', 'start_minute_html', 'end_month_html', 'end_day_html', 'end_year_html', 'end_hour_html', 'end_minute_html'));
                } else {
                    return view('frontend/schedule/create_repeat', compact('list_facility_selected','list_member_selected','select_month_html','select_week_html','equipment', 'schedule', 'date', 'html', 'department', 'departments', 'equipment_html', 'start_month_html', 'start_day_html', 'start_year_html', 'start_hour_html', 'start_minute_html', 'end_month_html', 'end_day_html', 'end_year_html', 'end_hour_html', 'end_minute_html'));
                }
            }else{
                if (config('global.device') != 'pc') {
                    return view('mobile/schedule/create', compact('members','equipments','equipment', 'schedule', 'date', 'html', 'department', 'departments', 'equipment_html', 'start_month_html', 'start_day_html', 'start_year_html', 'start_hour_html', 'start_minute_html', 'end_month_html', 'end_day_html', 'end_year_html', 'end_hour_html', 'end_minute_html'));
                } else {
                    return view('frontend/schedule/create', compact('equipment', 'schedule', 'date', 'html', 'department', 'departments', 'equipment_html', 'start_month_html', 'start_day_html', 'start_year_html', 'start_hour_html', 'start_minute_html', 'end_month_html', 'end_day_html', 'end_year_html', 'end_hour_html', 'end_minute_html'));
                }
            }
        } else {
            $month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($date)));
            $day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($date)), date('m', strtotime($date)), date('d', strtotime($date)));
            $year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($date)));
            $hour_html = \App\Helpers\StringHelper::getSelectHour();
            $minute_html = \App\Helpers\StringHelper::getSelectMinute();
        }
        $mobile_html = '';
        if ($request->get('uid')) {
            $member = $this->memberRepo->find($request->get('uid'));
        } else {
            $member = \Auth::guard('member')->user();
        }
        $members = $this->memberRepo->getAllNotMe();
        if(\Request::route()->getName() == 'frontend.schedule.create'){
            if (config('global.device') != 'pc') {
                return view('mobile/schedule/create', compact('member','members', 'equipment', 'date', 'html', 'departments', 'equipments', 'equipment_html', 'month_html', 'day_html', 'year_html', 'hour_html', 'minute_html'));
            } else {
                return view('frontend/schedule/create', compact('member', 'equipment', 'date', 'html', 'departments', 'equipment_html', 'month_html', 'day_html', 'year_html', 'hour_html', 'minute_html'));
            }
        }else{
            if (config('global.device') != 'pc') {
                return view('mobile/schedule/create_repeat', compact('member','members','equipment', 'date', 'html', 'departments', 'equipments', 'equipment_html', 'month_html', 'day_html', 'year_html', 'hour_html', 'minute_html'));
            } else {
                return view('frontend/schedule/create_repeat', compact('member', 'equipment', 'date', 'html', 'departments', 'equipment_html', 'month_html', 'day_html', 'year_html', 'hour_html', 'minute_html'));
            }
        }
    }

    public function createAllDay(Request $request) {
        $input = $request->all();
        if ($request->get('bdate')) {
            $date_now = $request->get('bdate');
        } else {
            $date_now = date('Y-m-d');
        }
        if(!is_null(\Auth::guard('member')->user()->department)){
            $department_id = \Auth::guard('member')->user()->department->getIds();
        }else{
            $department_id= [];
        }
        $members = $this->memberRepo->getByDepartment($department_id, \Auth::guard('member')->user()->id);
        $html = '<tr><td style="border-right:1px solid #666666;border-top:1px solid #666666;"><br></td>';
        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', strtotime(" +" . $i . " days", strtotime($date_now)));
            $html .= '<td class="s_date_' . strtolower(date('l', strtotime($date))) . '" align="center" style="border-right:1px solid #666666;border-top:1px solid #666666;padding:2px;font-size:80%;" nowrap="">
                            ' . date('D, d F', strtotime($date)) . '
                          </td>';
        }
        $html .= '</tr><td valign="top" class="calendar_rb">
                    <table width="100%" cellspacing="0" cellpadding="2">
                        <tbody>
                        <tr>
                            <td>
                            <a href="#"><img src="/img/loginuser20.gif" border="0" alt="">' . \Auth::guard('member')->user()->full_name . '</a>&nbsp;<br>&nbsp;<br>
                            <br>
                            </td>
                        </tr>
                       </tbody>
                    </table>
                </td>';
        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', strtotime(" +" . $i . " days", strtotime($date_now)));
            $schedules = \App\Schedule::join('member_schedule', 'member_schedule.schedule_id', '=', 'schedule.id')->where('member_schedule.member_id', \Auth::guard('member')->user()->id)
                            ->whereDate('schedule.start_date', '<=', $date)->whereDate('schedule.end_date', '>=', $date)->whereNotIn('pattern',['2','4'])->get();

            $html .= '<td valign="top" style="border-right:1px solid #666666;border-bottom:1px solid #cccccc;border-top:1px solid #666666;">
                        <table width="100%" cellspacing="0" cellpadding="0">
                          <tbody>';
            foreach ($schedules as $schedule) {
                $html .= '<tr><td class="share" style="font-size:90%;padding:2px;"><div class="listTime">' .($schedule->none_time != 1 ? date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) : '' ). '</div><div class="groupWeekEventTitle">'.($schedule->checkConflict(\Auth::guard('member')->user()->id) ? '<img src="/img/attention16.gif">' : '').'' . $schedule->title . '</div></td></tr>';
            }
            $html .= '<tr><td>&nbsp;</td></tr></tbody></table></td>';
        }
        $html .= '</tr>';
        $member_html = \App\Helpers\StringHelper::getSelectMemberOption(\App\Member::where('is_deleted', 0)->get());

        $department = $this->departmentRepo->getAllStack(0);
        foreach ($department as $key => $val) {
            $val->member = \App\Member::where('department_id', $val->id)->get();
        }
        $equipments = \App\Equipment::get();
        $equipment_html = \App\Helpers\StringHelper::getSelectListEquipment($equipments);
        if (isset($input['schedule_id'])) {
            $schedule = $this->scheduleRepo->find($input['schedule_id']);
            $start_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($schedule->start_date)));
            $start_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($schedule->start_date)), date('m', strtotime($schedule->start_date)), date('d', strtotime($schedule->start_date)));
            $start_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($schedule->start_date)));
            $end_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($schedule->end_date)));
            $end_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($schedule->end_date)), date('m', strtotime($schedule->end_date)), date('d', strtotime($schedule->end_date)));
            $end_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($schedule->end_date)));
            $members = $this->memberRepo->getAllEdit($schedule->member()->pluck('id')->toArray());
            if (config('global.device') != 'pc') {
                return view('mobile/schedule/create_all_day', compact('members','schedule', 'date_now', 'html', 'department', 'equipment_html', 'start_month_html', 'start_day_html', 'start_year_html', 'end_month_html', 'end_day_html', 'end_year_html'));
            } else {
                return view('frontend/schedule/create_all_day', compact('schedule', 'date_now', 'html', 'department', 'equipment_html', 'start_month_html', 'start_day_html', 'start_year_html', 'end_month_html', 'end_day_html', 'end_year_html'));
            }
        }
        $month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($date_now)));
        $day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($date_now)), date('m', strtotime($date_now)), date('d', strtotime($date_now)));
        $year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($date_now)));
        if ($request->get('uid')) {
            $member = $this->memberRepo->find($request->get('uid'));
        } else {
            $member = \Auth::guard('member')->user();
        }
        if (config('global.device') != 'pc') {
             $members = $this->memberRepo->getAllNotMe();
            return view('mobile/schedule/create_all_day', compact('members','member', 'date_now', 'html', 'department', 'equipment_html',  'month_html', 'day_html', 'year_html'));
        } else {
            return view('frontend/schedule/create_all_day', compact('member', 'date_now', 'html', 'department', 'equipment_html',  'month_html', 'day_html', 'year_html'));
        }
    }
    public function store(Request $request) {
        $input = $request->all();
        foreach($input as $key=>$val){
            if(!is_null($val)){
                $input[$key] = Purify::clean($val);
            }
        }
        if (config('global.device') != 'pc') {
            if(($input['pattern'] == 1 || $input['pattern'] == 3) && !is_null($input['start_time']) && !is_null($input['end_time'])){
                $input['start_hour'] = date('H',strtotime($input['start_time']));
                $input['end_hour'] = date('H',strtotime($input['end_time']));
                $input['start_minute'] = date('i',strtotime($input['start_time']));
                $input['end_minute'] = date('i',strtotime($input['end_time']));
            }elseif(($input['pattern'] == 1 || $input['pattern'] == 3) && (is_null($input['start_time']) || is_null($input['end_time']))){
                $input['start_hour'] = NULL;
                $input['end_hour'] = NULL;
            }
            $input['start_day'] = date('d',strtotime($input['start_date_submit']));
            $input['start_month'] = date('m',strtotime($input['start_date_submit']));
            $input['start_year'] = date('Y',strtotime($input['start_date_submit']));
            $input['end_day'] = date('d',strtotime($input['end_date_submit']));
            $input['end_month'] = date('m',strtotime($input['end_date_submit']));
            $input['end_year'] = date('Y',strtotime($input['end_date_submit']));
        }
        if($input['pattern'] == 1 || $input['pattern'] == 3){
            if(is_null($input['start_hour']) && is_null($input['end_hour']) ){
                $input['start_hour'] = '00';
                $input['start_minute'] = '00';
                $input['end_hour'] = '23';
                $input['end_minute'] = '59';
                $input['none_time'] = 1;
            }
        }
        $schedule_id = 0;
        $equipment_name = [];
        if (isset($input['selected_users_sITEM'])) {
            if ($input['selected_users_sITEM'] != '') {
                $equipment_ids = explode(':', $input['selected_users_sITEM']);
                $start_time = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $end_time = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                foreach ($equipment_ids as $key => $equipment_id) {
                    $eq_schedule = $this->scheduleRepo->getByEquipmentAllAboutTime($equipment_id, $start_time, $end_time);
                    if (count($eq_schedule) > 0) {
                        $equipment = $this->equipmentRepo->find($equipment_id);
                        $equipment_name[] = $equipment->name;
                    }
                }
                if (count($equipment_name) > 0) {
                    $equipment_name = implode(',', $equipment_name);
                    $object = new \stdClass();
                    $object->cause = "When reserving a facility, you must set the (time) period so that it does not overlap other appointments.";
                    $object->code = "GRN_SCHD_13208";
                    $object->counter_measure = '"Confirm the appointment for the following facility: "' . $equipment_name . '"."';
                    $object->diagnosis = '""The appointment at the following facility overlaps another appointment: ' . $equipment_name . '"."';
                    return response()->json($object);
                }
            }else{

            }
        }
        if ($input['selected_users_sUID'] == '') {
            $object = new \stdClass();
            $object->cause = "One or more attendees for this appointment are required to add or change the appointment.";
            $object->code = "GRN_SCHD_13021";
            $object->counter_measure = "Select one or more attendees";
            $object->diagnosis = "Attendee has not been specified";
            return response()->json($object);
        }
        if ($input['pattern'] == 3) {
            $equipment_conflic = [];
            if (isset($input['selected_users_sITEM'])) {
                if ($input['selected_users_sITEM'] != '') {
                    $equipment_ids = explode(':', $input['selected_users_sITEM']);
                    $start_time = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                    $end_time = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                    foreach ($equipment_ids as $key => $equipment_id) {
                        $eq_schedules = $this->scheduleRepo->getByEquipmentAllAboutTime($equipment_id, $start_time, $end_time);
                        if ($eq_schedules) {
                            foreach ($eq_schedules as $key => $val) {
                                $equipment = $this->equipmentRepo->find($equipment_id);
                                $object1 = new \stdClass();
                                $object1->col_facility = $equipment->name;
                                $object1->col_setdatetime = strtotime($val->start_date);
                                $object1->setdatetime = date('D, d F', strtotime($val->start_date));
                                $equipment_conflic[] = $object1;
                            }
                        }
                    }
                    if (count($equipment_conflic) > 0) {
                        $object = new \stdClass();
                        $object->conflict_facility = 1;
                        $object->conflict_events = $equipment_conflic;
                        return response()->json($object);
                    }
                }
            }
            // start add schedule repeat
            $start_date = date('Y-m-d', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
            $end_date = date('Y-m-d', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day']));
            $date1 = new \DateTime($start_date);
            $date2 = new \DateTime($end_date);
            $day = $date1->diff($date2)->format("%a");
            $date_arr = [];
            if($day == 0){
               $time = new \DateTime($start_date);
               if ($input['type'] == 'week' && in_array($time->format("w"),$input['wday'])) {
                    $date_arr[] = $time->format("Y-m-d");
                }
                if (($time->format("w") != '0' && $time->format("w") != '6' && ($input['type'] == 'weekday')) || ($input['type'] == 'day')) {
                    $date_arr[] = $time->format("Y-m-d");
                }
                if ($input['type'] == 'month' && $time->format("d") == $input['day']) {
                    $date_arr[] = $time->format("Y-m-d");
                }
            }else{
                for ($i = 0; $i <= $day; $i++) {
                    $date1 = new \DateTime($start_date);
                    $time = $date1->modify('+' . $i . ' day');
                    if ($input['type'] == 'week' && in_array($time->format("w"),$input['wday'])) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                    if (($time->format("w") != '0' && $time->format("w") != '6' && ($input['type'] == 'weekday')) || ($input['type'] == 'day')) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                    if ($input['type'] == 'month' && $time->format("d") == $input['day']) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                }
            }
            if (count($date_arr) > 0) {
                $input['event'] = count(\App\Schedule::get()) + 1;
                $input['uid'] = \Auth::guard('member')->user()->id;
                $input['type_repeat'] = $input['type'];
                if($input['type'] == 'month'){
                    $input['wday'] = $input['day'];
                }elseif($input['type'] == 'week'){
                    $input['wday'] = implode(',',$input['wday']);
                }else{
                    $input['wday'] = NULL;
                }
                $input['start_repeat'] = date('Y-m-d H:i:s', strtotime($start_date . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_repeat'] = date('Y-m-d H:i:s', strtotime($end_date . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                foreach ($date_arr as $key => $val) {
                    $input['start_date'] = date('Y-m-d H:i:s', strtotime($val . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                    $input['end_date'] = date('Y-m-d H:i:s', strtotime($val . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                    $schedule = $this->scheduleRepo->create($input);
                    // Thêm file vào schedule
                    if(isset($input['files']) && count($input['files']) > 0){
                        foreach($input['files'] as $key=>$val){
                            $this->fileRepo->update(['relationship_id'=>$schedule->id],$val);
                        }
                    }
                    if ($key == 0) {
                        $schedule_id = $schedule->id;
                    }
                    if ($input['selected_users_sUID'] != '') {
                        $input['member_id'] = explode(':', $input['selected_users_sUID']);
                        $emails = \App\Member::whereIn('id', $input['member_id'])->pluck('email')->toArray();
                        $schedule->member()->attach($input['member_id']);
                    }
                    if (isset($input['selected_users_sITEM'])) {
                        if ($input['selected_users_sITEM'] != '') {
                            $input['equipment_id'] = explode(':', $input['selected_users_sITEM']);
                            $schedule->equipment()->attach($input['equipment_id']);
                        }
                    }
                    //SendNotificationJob::dispatch($input['title'], $input['memo'], $input['start_date'], $input['end_date'], $emails,$schedule->id)->delay(\Carbon\Carbon::now()->addSeconds(10));
                    }
                   if ($input['member_id']) {
                        if (($key = array_search(\Auth::guard('member')->user()->id, $input['member_id'])) !== false) {
                            unset($input['member_id'][$key]);
                        }
                        $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Bạn đã được thêm vào 1 lịch trình', 'link' => '/schedule/view/' . $schedule_id]);
                        $this->notificationSchRepo->createNotification('Bạn đã được thêm vào 1 lịch trình', \Auth::guard('member')->user()->id, $input['member_id'], route('frontend.schedule.view', $schedule_id));
                   }
                   if (isset($input['selected_users_p_sUID']) && $input['selected_users_p_sUID'] != '') {
                        $member_ids = explode(':', $input['selected_users_p_sUID']);
                        $input['member_id'] = $member_ids;
                        $schedule->seen()->attach($input['member_id']);
                        $this->memberRepo->NotificateMembers($member_ids, ['content' => 'Bạn được chia sẻ thông tin 1 lịch trình', 'link' => '/schedule/view/' . $schedule_id]);
                        $this->notificationSchRepo->createNotification('Bạn được chia sẻ thông tin 1 lịch trình', \Auth::guard('member')->user()->id, $member_ids, route('frontend.schedule.view', $schedule_id));
                    }
            }else{
                $object = new \stdClass();
                $object->cause = "Cannot set because of the following reason. <ul><li>The combination of repeating condition and period is not correct</ul>";
                $object->code = "ORS_SC_13065";
                $object->counter_measure = "Confirm whether or not the date is correct.";
                $object->diagnosis = "Cannot use this repeating period.";
                return response()->json($object);
            }
            // end add schedule repeat
        } else {
            // start add schedule all day, normal
            if ($input['pattern'] == 1) {
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
            } elseif ($input['pattern'] == 2 || $input['pattern'] == 4) {
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day']));
            }
            $input['uid'] = \Auth::guard('member')->user()->id;
            $input['event'] = count(\App\Schedule::get()) + 1;
            $schedule = $this->scheduleRepo->create($input);
            // Thêm file vào schedule
            if(isset($input['files']) && count($input['files']) > 0){
                foreach($input['files'] as $key=>$val){
                    $this->fileRepo->update(['relationship_id'=>$schedule->id],$val);
                }
            }
            $schedule_id = $schedule->id;
            if ($input['selected_users_sUID'] != '') {
                $input['member_id'] = explode(':', $input['selected_users_sUID']);
                $emails = \App\Member::whereIn('id', $input['member_id'])->pluck('email')->toArray();
                $schedule->member()->attach($input['member_id']);
                if (($key = array_search(\Auth::guard('member')->user()->id, $input['member_id'])) !== false) {
                    unset($input['member_id'][$key]);
                }
                $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Bạn đã được thêm vào 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
                $this->notificationSchRepo->createNotification('Bạn đã được thêm vào 1 lịch trình', \Auth::guard('member')->user()->id, $input['member_id'], route('frontend.schedule.view', $schedule->id));
            }
            if (isset($input['selected_users_p_sUID']) && $input['selected_users_p_sUID'] != '') {
                $member_ids = explode(':', $input['selected_users_p_sUID']);
                $input['member_id'] = $member_ids;
                $schedule->seen()->attach($input['member_id']);
                $this->memberRepo->NotificateMembers($member_ids, ['content' => 'Bạn được chia sẻ thông tin 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
                $this->notificationSchRepo->createNotification('Bạn được chia sẻ thông tin 1 lịch trình', \Auth::guard('member')->user()->id, $member_ids, route('frontend.schedule.view', $schedule->id));
            }
            if (isset($input['selected_users_sITEM'])) {
                if ($input['selected_users_sITEM'] != '') {
                    $input['equipment_id'] = explode(':', $input['selected_users_sITEM']);
                    $schedule->equipment()->attach($input['equipment_id']);
                }
            }
            //$event_id = \App\Helpers\Calendar::insert($input['title'], '', $input['memo'], $input['start_date'], $input['end_date'], $emails);
            //$this->scheduleRepo->update(['event_id' => $event_id], $schedule->id);
            //end add schedule all day, normal
        }
        if (config('global.device') != 'pc') {
            return redirect()->route('frontend.schedule.view',$schedule_id);
        } else {
            return response()->json(array('link' => '/schedule/view/' . $schedule_id));
        }
    }

    public function view($id) {
        $schedule = \App\Schedule::find($id);
        if($schedule){
            if (config('global.device') != 'pc') {
                return view('mobile/schedule/view', compact('schedule'));
            } else {
                return view('frontend/schedule/view', compact('schedule'));
            }
        }else{
             if (config('global.device') != 'pc') {
                return view('mobile/schedule/view_not_schedule');
            } else {
                return view('frontend/schedule/view_not_schedule');
            }
        }
    }

    public function seen() {
        if (config('global.device') != 'pc') {
            return view('mobile/schedule/seen');
        } else {
            return view('frontend/schedule/seen');
        }
    }
    public function edit($id) {
        $schedule = $this->scheduleRepo->find($id);
        if(\Auth::guard('member')->user()->id != $schedule->uid){
            abort(403);
        }
        $start_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($schedule->start_date)));
        $start_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($schedule->start_date)), date('m', strtotime($schedule->start_date)), date('d', strtotime($schedule->start_date)));
        $start_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($schedule->start_date)));
        $end_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($schedule->end_date)));
        $end_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($schedule->end_date)), date('m', strtotime($schedule->end_date)), date('d', strtotime($schedule->end_date)));
        $end_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($schedule->end_date)));
        if($schedule->none_time != 1){
            $start_minute_html = \App\Helpers\StringHelper::getSelectMinute(date('i', strtotime($schedule->start_date)));
            $start_hour_html = \App\Helpers\StringHelper::getSelectHour(date('H', strtotime($schedule->start_date)));
            $end_hour_html = \App\Helpers\StringHelper::getSelectHour(date('H', strtotime($schedule->end_date)));
            $end_minute_html = \App\Helpers\StringHelper::getSelectMinute(date('i', strtotime($schedule->end_date)));
        }else{
            $start_minute_html = \App\Helpers\StringHelper::getSelectMinute();
            $start_hour_html = \App\Helpers\StringHelper::getSelectHour();
            $end_hour_html = \App\Helpers\StringHelper::getSelectHour();
            $end_minute_html = \App\Helpers\StringHelper::getSelectMinute();
        }
        $equipments = \App\Equipment::get();
        $equipment_html = \App\Helpers\StringHelper::getSelectListEquipment($equipments);
        $department = $this->departmentRepo->getAllStack(0);
        $list = new \stdClass();
        $list_member_selected = new \stdClass();
        foreach ($schedule->member as $key => $val) {
            $object = new \stdClass();
            $object->type = 'user';
            $object->id = $val->id;
            $object->foreignKey = $val->full_name;
            $object->displayName = $val->full_name;
            $object->image = $val->avatar;
            $object->isInvalidUser = false;
            $object->isNotUsingApp = false;
            $object->isLoginUser = $val->id == \Auth::guard('member')->user()->id ? true : false;
            $list->$key = $object;
        }
        $list_member_selected->list = $list;
        $list_member_selected = json_encode($list_member_selected);
        $list = new \stdClass();
        $list_facility_selected = new \stdClass();
        foreach ($schedule->equipment as $key => $val) {
            $object = new \stdClass();
            $object->type = 'facility';
            $object->id = $val->id;
            $object->foreignKey = $val->name;
            $object->displayName = $val->name;
            $object->approval = 0;
            $list->$key = $object;
        }
        $list_facility_selected->list = $list;
        $list_facility_selected = json_encode($list_facility_selected);
        $list_member_shared = new \stdClass();
        foreach ($schedule->seen as $key => $val) {
            $object = new \stdClass();
            $object->type = 'user';
            $object->id = $val->id;
            $object->foreignKey = $val->full_name;
            $object->displayName = $val->full_name;
            $object->image = $val->avatar;
            $object->isInvalidUser = false;
            $object->isNotUsingApp = false;
            $object->isLoginUser = $val->id == \Auth::guard('member')->user()->id ? true : false;
            $list->$key = $object;
        }
        $list_member_shared->list = $list;
        $list_member_shared = json_encode($list_member_shared);
        $list = new \stdClass();
        if (config('global.device') != 'pc') {
            $departments = $this->departmentRepo->all();
            $members = $this->memberRepo->getAllEdit($schedule->member()->pluck('id')->toArray());
            return view('mobile/schedule/edit', compact('members','departments','list_member_shared','equipments', 'list_facility_selected', 'list_member_selected', 'department', 'equipment_html', 'schedule', 'start_month_html', 'end_month_html', 'start_day_html', 'end_day_html', 'start_year_html', 'end_year_html', 'start_hour_html', 'end_hour_html', 'start_minute_html', 'end_minute_html'));
        } else {
            return view('frontend/schedule/edit', compact('list_member_shared','department', 'equipment_html', 'schedule', 'start_month_html', 'end_month_html', 'start_day_html', 'end_day_html', 'start_year_html', 'end_year_html', 'start_hour_html', 'end_hour_html', 'start_minute_html', 'end_minute_html'));
        }
    }

    public function editAll($id) {
        $schedule = $this->scheduleRepo->find($id);
        if(\Auth::guard('member')->user()->id != $schedule->uid){
            abort(403);
        }
        $start_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($schedule->start_date)));
        $start_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($schedule->start_date)), date('m', strtotime($schedule->start_date)), date('d', strtotime($schedule->start_date)));
        $start_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($schedule->start_date)));
        $start_hour_html = \App\Helpers\StringHelper::getSelectHour(date('H', strtotime($schedule->start_date)));
        $start_minute_html = \App\Helpers\StringHelper::getSelectMinute(date('i', strtotime($schedule->start_date)));
        $end_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($schedule->end_date)));
        $end_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($schedule->end_date)), date('m', strtotime($schedule->end_date)), date('d', strtotime($schedule->end_date)));
        $end_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($schedule->end_date)));
        $end_hour_html = \App\Helpers\StringHelper::getSelectHour(date('H', strtotime($schedule->end_date)));
        $end_minute_html = \App\Helpers\StringHelper::getSelectMinute(date('i', strtotime($schedule->end_date)));
        $equipments = \App\Equipment::get();
        $equipment_html = \App\Helpers\StringHelper::getSelectListEquipment($equipments);
        $department = $this->departmentRepo->getAllStack(0);
        $list = new \stdClass();
        $list_member_selected = new \stdClass();
        foreach ($schedule->member as $key => $val) {
            $object = new \stdClass();
            $object->type = 'user';
            $object->id = $val->id;
            $object->foreignKey = $val->full_name;
            $object->displayName = $val->full_name;
            $object->image = $val->avatar;
            $object->isInvalidUser = false;
            $object->isNotUsingApp = false;
            $object->isLoginUser = $val->id == \Auth::guard('member')->user()->id ? true : false;
            $list->$key = $object;
        }
        $list_member_selected->list = $list;
        $list_member_selected = json_encode($list_member_selected);
        if (config('global.device') != 'pc') {
            $departments = $this->departmentRepo->all();
            $members = $this->memberRepo->getAllEdit($schedule->member()->pluck('id')->toArray());
            return view('mobile/schedule/edit_all_day', compact('members','departments','list_member_selected', 'department', 'equipment_html', 'schedule', 'start_month_html', 'end_month_html', 'start_day_html', 'end_day_html', 'start_year_html', 'end_year_html', 'start_hour_html', 'end_hour_html', 'start_minute_html', 'end_minute_html'));
        } else {
            return view('frontend/schedule/edit_all_day', compact('list_member_selected', 'department', 'equipment_html', 'schedule', 'start_month_html', 'end_month_html', 'start_day_html', 'end_day_html', 'start_year_html', 'end_year_html', 'start_hour_html', 'end_hour_html', 'start_minute_html', 'end_minute_html'));
        }
    }

    public function editRepeat($id) {
        $schedule = $this->scheduleRepo->find($id);
        if(\Auth::guard('member')->user()->id != $schedule->uid){
            abort(403);
        }
        $start_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($schedule->start_repeat)));
        $start_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($schedule->start_repeat)), date('m', strtotime($schedule->start_repeat)), date('d', strtotime($schedule->start_repeat)));
        $start_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($schedule->start_repeat)));
        $end_month_html = \App\Helpers\StringHelper::getSelectMonth(date('m', strtotime($schedule->end_repeat)));
        $end_day_html = \App\Helpers\StringHelper::getSelectDay(date('Y', strtotime($schedule->end_repeat)), date('m', strtotime($schedule->end_repeat)), date('d', strtotime($schedule->end_repeat)));
        $end_year_html = \App\Helpers\StringHelper::getSelectYear(date('Y', strtotime($schedule->end_repeat)));
        if($schedule->none_time != 1){
            $start_hour_html = \App\Helpers\StringHelper::getSelectHour(date('H', strtotime($schedule->start_repeat)));
            $start_minute_html = \App\Helpers\StringHelper::getSelectMinute(date('i', strtotime($schedule->start_repeat)));
            $end_hour_html = \App\Helpers\StringHelper::getSelectHour(date('H', strtotime($schedule->end_repeat)));
            $end_minute_html = \App\Helpers\StringHelper::getSelectMinute(date('i', strtotime($schedule->end_repeat)));
        }else{
            $start_minute_html = \App\Helpers\StringHelper::getSelectMinute();
            $start_hour_html = \App\Helpers\StringHelper::getSelectHour();
            $end_hour_html = \App\Helpers\StringHelper::getSelectHour();
            $end_minute_html = \App\Helpers\StringHelper::getSelectMinute();
        }
        $equipments = \App\Equipment::get();
        $equipment_html = \App\Helpers\StringHelper::getSelectListEquipment($equipments);
        $department = $this->departmentRepo->getAllStack(0);
        $select_week_html = '';
        $select_month_html = '';
        if($schedule->type_repeat == 'week'){
            foreach(\App\Schedule::Weekend_arr as $key=>$val){
                $select_week_html .=' <option value="'.$key.'" '.(in_array($key,explode(',',$schedule->wday)) ? 'selected' : '').'>'.$val.'</option>';
            }
        }else{
            foreach(\App\Schedule::Weekend_arr as $key=>$val){
                $select_week_html .=' <option value="'.$key.'">'.$val.'</option>';
            }
        }
        if($schedule->type_repeat == 'month'){
            for($i=1;$i<32;$i++){
                $select_month_html .=' <option value="'.$i.'" '.(in_array($i,explode(',',$schedule->wday)) ? 'selected' : '').'>'.$i.'</option>';
            }
        }else{
            for($i=1;$i<32;$i++){
                $select_month_html .=' <option value="'.$i.'">'.$i.'</option>';
            }
        }
        if (config('global.device') != 'pc') {
            $members = $this->memberRepo->getAllEdit($schedule->member()->pluck('id')->toArray());
            return view('mobile/schedule/edit_repeat', compact('members','department', 'schedule','equipments'));
        } else {
            return view('frontend/schedule/edit_repeat', compact('select_month_html','select_week_html','department', 'equipment_html', 'schedule', 'start_month_html', 'end_month_html', 'start_day_html', 'end_day_html', 'start_year_html', 'end_year_html', 'start_hour_html', 'end_hour_html', 'start_minute_html', 'end_minute_html'));
        }
    }
    public function update(Request $request, $id) {
        $input = $request->all();
        foreach($input as $key=>$val){
            if(!is_null($val)){
                $input[$key] = Purify::clean($val);
            }
        }
        if (config('global.device') != 'pc') {
            if(($input['pattern'] == 1 || $input['pattern'] == 3) && !is_null($input['start_time']) && !is_null($input['end_time'])){
                $input['start_hour'] = date('H',strtotime($input['start_time']));
                $input['end_hour'] = date('H',strtotime($input['end_time']));
                $input['start_minute'] = date('i',strtotime($input['start_time']));
                $input['end_minute'] = date('i',strtotime($input['end_time']));
            }elseif(($input['pattern'] == 1 || $input['pattern'] == 3) && (is_null($input['start_time']) || is_null($input['end_time']))){
                $input['start_hour'] = NULL;
                $input['end_hour'] = NULL;
            }
            $input['start_day'] = date('d',strtotime($input['start_date_submit']));
            $input['start_month'] = date('m',strtotime($input['start_date_submit']));
            $input['start_year'] = date('Y',strtotime($input['start_date_submit']));
            $input['end_day'] = date('d',strtotime($input['end_date_submit']));
            $input['end_month'] = date('m',strtotime($input['end_date_submit']));
            $input['end_year'] = date('Y',strtotime($input['end_date_submit']));
        }
        if($input['pattern'] == 1 || $input['pattern'] == 3){
            if(is_null($input['start_hour']) && is_null($input['end_hour'])){
                $input['start_hour'] = '00';
                $input['start_minute'] = '00';
                $input['end_hour'] = '23';
                $input['end_minute'] = '59';
                $input['none_time'] = 1;
            }else{
                $input['none_time'] = 0;
            }
        }
        $schedule = $this->scheduleRepo->find($id);
        if ($input['pattern'] == 3 && $input['apply'] != 'this') {
            if ($input['apply'] == 'after') {
                $record = $this->scheduleRepo->find($id);
                $start_time = date('Y-m-d H:i:s', strtotime($record->start_date));
                $start_date = date('Y-m-d', strtotime($record->start_date));
                $this->scheduleRepo->updateBefore($input['event'], date('Y-m-d', strtotime($record->start_date)));
                $this->scheduleRepo->deleteAfter($input['event'], date('Y-m-d', strtotime($record->start_date)));
            } else {
                $this->scheduleRepo->deleteAll($input['event']);
                $start_time = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $start_date = date('Y-m-d', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
            }
            $equipment_conflic = [];
            $end_time = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
            $end_date = date('Y-m-d', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day']));
            if (isset($input['selected_users_sITEM'])) {
                if ($input['selected_users_sITEM'] != '') {
                    $equipment_ids = explode(':', $input['selected_users_sITEM']);
                    foreach ($equipment_ids as $key => $equipment_id) {
                        $eq_schedules = $this->scheduleRepo->getByEquipmentAllAboutTime($equipment_id, $start_time, $end_time);
                        if ($eq_schedules) {
                            foreach ($eq_schedules as $key => $val) {
                                $equipment = $this->equipmentRepo->find($equipment_id);
                                $object1 = new \stdClass();
                                $object1->col_facility = $equipment->name;
                                $object1->col_setdatetime = strtotime($val->start_date);
                                $object1->setdatetime = date('D, d F', strtotime($val->start_date));
                                $equipment_conflic[] = $object1;
                            }
                        }
                    }
                    if (count($equipment_conflic) > 0) {
                        $object = new \stdClass();
                        $object->conflict_facility = 1;
                        $object->conflict_events = $equipment_conflic;
                        return response()->json($object);
                    }
                }
            }
            // start add schedule repeat
            $date1 = new \DateTime($start_date);
            $date2 = new \DateTime($end_date);
            $day = $date1->diff($date2)->format("%a") + 1;
            $date_arr = [];
            if($day == 0){
               $time = new \DateTime($start_date);
               if ($input['type'] == 'week' && in_array($time->format("w"),$input['wday'])) {
                    $date_arr[] = $time->format("Y-m-d");
                }
                if ($time->format("w") != '0' && $time->format("w") != '6' && (($input['type'] == 'weekday') || ($input['type'] == 'day'))) {
                    $date_arr[] = $time->format("Y-m-d");
                }
                if ($time->format("d") == $input['day'] && $input['type'] == 'month') {
                    $date_arr[] = $time->format("Y-m-d");
                }
            }else{
                for ($i = 0; $i <= $day; $i++) {
                    $date1 = new \DateTime($start_date);
                    $time = $date1->modify('+' . $i . ' day');
                    if ($input['type'] == 'week' && in_array($time->format("w"),$input['wday'])) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                    if ($time->format("w") != '0' && $time->format("w") != '6' && (($input['type'] == 'weekday') || ($input['type'] == 'day'))) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                    if ($input['type'] == 'month' && $time->format("d") == $input['day']) {
                        $date_arr[] = $time->format("Y-m-d");
                    }
                }
            }
            if (count($date_arr) > 0) {
                $input['event'] = count(\App\Schedule::get()) + 1;
                $input['uid'] = \Auth::guard('member')->user()->id;
                $input['type_repeat'] = $input['type'];
                if($input['type'] == 'week'){
                    $input['wday'] = implode(',',$input['wday']);
                }
                $input['start_repeat'] = date('Y-m-d H:i:s', strtotime($start_date . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_repeat'] = date('Y-m-d H:i:s', strtotime($end_date . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                foreach ($date_arr as $key => $val) {
                    $input['start_date'] = date('Y-m-d H:i:s', strtotime($val . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                    $input['end_date'] = date('Y-m-d H:i:s', strtotime($val . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                    $schedule = $this->scheduleRepo->create($input);
                    if ($key == 0) {
                        $id = $schedule->id;
                    }
                    if ($input['selected_users_sUID'] != '') {
                        $input['member_id'] = explode(':', $input['selected_users_sUID']);
                        $emails = \App\Member::whereIn('id', $input['member_id'])->pluck('email')->toArray();
                        $schedule->member()->attach($input['member_id']);
                        if (($key = array_search(\Auth::guard('member')->user()->id, $input['member_id'])) !== false) {
                            unset($input['member_id'][$key]);
                        }
                        $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Cập nhật lại 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
                        $this->notificationSchRepo->createNotification('Cập nhật lại 1 lịch trình', \Auth::guard('member')->user()->id, $input['member_id'], route('frontend.schedule.view', $schedule->id));
                    }
                    if (isset($input['selected_users_p_sUID']) && $input['selected_users_p_sUID'] != '') {
                        $member_ids = explode(':', $input['selected_users_p_sUID']);
                        $this->memberRepo->NotificateMembers($member_ids, ['content' => 'Bạn được chia sẻ thông tin 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
                        $this->notificationSchRepo->createNotification('Bạn được chia sẻ thông tin 1 lịch trình', \Auth::guard('member')->user()->id, $member_ids, route('frontend.schedule.view', $schedule->id));
                    }
                    if (isset($input['selected_users_sITEM'])) {
                        if ($input['selected_users_sITEM'] != '') {
                            $input['equipment_id'] = explode(':', $input['selected_users_sITEM']);
                            $schedule->equipment()->attach($input['equipment_id']);
                        }
                    }
                    if (isset($input['private'])) {
                        if ($input['private'] == 1) {
                            $member_id = [];
                            $member_id[] = \Auth::guard('member')->user()->id;
                            $input['member_id'] = $member_id;
                            $schedule->seen()->attach($input['member_id']);
                        }
                    }
                    if (isset($input['public_dashboard'])) {
                        $schedule->update($input);
                    }else{
                        $input['public_dashboard'] = "0";
                        $schedule->update($input);
                    }
                    //$event_id = \App\Helpers\Calendar::insert($input['title'], '', $input['memo'], $input['start_date'], $input['end_date'], $emails);
                    //$this->scheduleRepo->update(['event_id' => $event_id], $schedule->id);
                }
            }else{
                return response()->json(array('link' => '/schedule/create_repeat'));
            }

        } else {
            if ($input['pattern'] == 3 && $input['apply'] == 'this') {
                $schedule = $this->scheduleRepo->find($id);
                $start_day = date('Y-m-d', strtotime($schedule->start_date));
                $end_day = date('Y-m-d', strtotime($schedule->end_date));
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($start_day . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($end_day . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
                $input['event'] = count(\App\Schedule::get()) + 1;
                $input['pattern'] = 1;
                $input['wday']= NULL;
            } elseif ($input['pattern'] == 1) {
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day'] . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day'] . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
            } elseif ($input['pattern'] == 2 || $input['pattern'] == 4) {
                $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
                $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_year'] . '-' . $input['end_month'] . '-' . $input['end_day']));
            }
            $input['update_person'] = \Auth::guard('member')->user()->id;
            $input['update_at'] = date('Y-m-d');
            $this->scheduleRepo->update($input, $id);
            // Thêm file vào schedule
            $this->fileRepo->resetRelationshipID($id,[\App\File::TYPE_SCHEDULE]);
            if(isset($input['files']) && count($input['files']) > 0){
                foreach($input['files'] as $key=>$val){
                    $this->fileRepo->update(['relationship_id'=>$schedule->id],$val);
                }
            }
            $this->fileRepo->deleteFileNull();
            $schedule = $this->scheduleRepo->find($id);
            if ($input['selected_users_sUID'] != '') {
                $input['member_id'] = explode(':', $input['selected_users_sUID']);
                $schedule->member()->sync($input['member_id']);
                if (($key = array_search(\Auth::guard('member')->user()->id, $input['member_id'])) !== false) {
                    unset($input['member_id'][$key]);
                }
                $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Bạn đã được thêm vào 1 lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
            }
            if (isset($input['selected_users_sITEM'])) {
                if ($input['selected_users_sITEM'] != '') {
                    $input['equipment_id'] = explode(':', $input['selected_users_sITEM']);
                    $schedule->equipment()->sync($input['equipment_id']);
                }
            }elseif(count($schedule->equipment) > 0){
                $schedule->equipment()->detach();
            }
            if (isset($input['private'])) {
                if ($input['private'] == 2) {
                    $input['member_id'] = explode(',', $input['selected_users_p_sUID']);
                    $schedule->seen()->sync($input['member_id']);
                }
                if ($input['private'] == 1) {
                    $member_id = [];
                    $member_id[] = \Auth::guard('member')->user()->id;
                    $input['member_id'] = $member_id;
                    $schedule->seen()->sync($input['member_id']);
                }
            }
            if(isset($input['public_dashboard'])){
                $schedule->update($input);
            }else{
                $input['public_dashboard'] = "0";
                $schedule->update($input);
            }
        }
        if (config('global.device') != 'pc') {
            return redirect()->route('frontend.schedule.view',$id);;
        } else {
            return response()->json(array('link' => '/schedule/view/' . $id));
        }
    }

    public function checkMember1($member_id) {
        $member_ids = explode(',', $member_id);
        $member_arr = \App\Member::whereIn('id', $member_ids)->get();
        $member = [];
        foreach ($member_arr as $key => $val) {
            $object1 = new \stdClass();
            $object1->id = ++$key;
            $object1->title = $val->full_name;
            $member[] = $object1;
        }
        return $member;
    }

    public function checkMember2($member_id) {
        $member_ids = explode(',', $member_id);
        $member_arr = \App\Member::whereIn('id', $member_ids)->get();
        $member = [];
        $schedule = [];
        foreach ($member_arr as $key => $val) {
            foreach ($val->schedule as $k => $value) {
                $object2 = new \stdClass();
                $object2->start = date('Y-m-d H:i', strtotime($value->start_date));
                $object2->end = date('Y-m-d H:i', strtotime($value->end_date));
                $object2->title = $value->work;
                $object2->resourceId = ++$key;
                $schedule[] = $object2;
            }
        }
        return $schedule;
    }

    public function checkEquipment1($equipment_id) {
        $equipment_ids = explode(',', $equipment_id);
        $equipment_arr = \App\Equipment::whereIn('id', $equipment_ids)->get();
        $equipment = [];
        foreach ($equipment_arr as $key => $val) {
            $object1 = new \stdClass();
            $object1->id = ++$key;
            $object1->title = $val->name;
            $equipment[] = $object1;
        }
        return $equipment;
    }

    public function checkEquipment2($equipment_id) {
        $equipment_ids = explode(',', $equipment_id);
        $equipment_arr = \App\Equipment::whereIn('id', $equipment_ids)->get();
        $equipment = [];
        $schedule = [];
        foreach ($equipment_arr as $key => $val) {
            foreach ($val->schedule as $k => $value) {
                $object2 = new \stdClass();
                $object2->start = date('Y-m-d H:i', strtotime($value->start_date));
                $object2->end = date('Y-m-d H:i', strtotime($value->end_date));
                $object2->title = $value->work;
                $object2->resourceId = ++$key;
                $schedule[] = $object2;
            }
        }
        return $schedule;
    }

    public function personalDay(Request $request) {
        Session::put('redirect_route','frontend.schedule.personal_day');
        if ($request->get('bdate')){
            $date_now = $request->get('bdate');
        } else {
            $date_now = date('Y-m-d');
        }
        for ($i = 1; $i <= 7; $i++) {
            $date_check_next = date('w', strtotime(" +" . $i . " days", strtotime($date_now)));
            $date_check_prev = date('w', strtotime(" -" . $i . " days", strtotime($date_now)));
            if ($date_check_next == '0') {
                $next_week = date('Y-m-d', strtotime(" +" . $i . " days", strtotime($date_now)));
            }
            if ($date_check_prev == '0') {
                $prev_week = date('Y-m-d', strtotime(" -" . $i . " days", strtotime($date_now)));
            }
        }
        $departments = $this->departmentRepo->getAllStack(0);
        $equipments = $this->equipmentRepo->all();
        $equipment = [];
        $department = [];
        foreach ($departments as $key => $val) {
            $object2 = new \stdClass();
            $object2->id = $val->id;
            $object2->name = $val->name;
            $object2->level = $val->level;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->type = "membership";
            $object2 = json_encode($object2);
            $department[] = $object2;
        }
        foreach ($equipments as $key => $val) {
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->count = "0";
            $object2->children = [];
            $object2 = json_encode($object2);
            $equipment[] = $object2;
        }
        $department = implode(',', $department);
        $equipment = implode(',', $equipment);
        $calendar_html = $this->scheduleRepo->showCalendar(date('Y-m-01', strtotime($date_now)), 'schedule/index');
        return view('frontend/schedule/personal_day', compact('date_now', 'next_week', 'prev_week', 'department', 'departments', 'calendar_html', 'equipment'));
    }

    public function personalWeek(Request $request) {
        Session::put('redirect_route','frontend.schedule.personal_week');
        if ($request->get('date')) {
            $date_now = $request->get('date');
        } else {
            $date_now = date('Y-m-d');
        }
        $departments = $this->departmentRepo->getAllStack(0);
        $equipments = $this->equipmentRepo->all();
        $equipment = [];
        $department = [];
        foreach ($departments as $key => $val) {
            $object2 = new \stdClass();
            $object2->id = $val->id;
            $object2->name = $val->name;
            $object2->level = $val->level;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->type = "membership";
            $object2 = json_encode($object2);
            $department[] = $object2;
        }
        foreach ($equipments as $key => $val) {
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->count = "0";
            $object2->children = [];
            $object2 = json_encode($object2);
            $equipment[] = $object2;
        }
        $department = implode(',', $department);
        $equipment = implode(',', $equipment);
        $calendar_html = $this->scheduleRepo->showCalendar(date('Y-m-01', strtotime($date_now)), 'schedule/index');
        return view('frontend/schedule/personal_week', compact('department', 'date_now', 'calendar_html', 'equipment'));
    }

    public function personalMonth(Request $request){
        Session::put('redirect_route','frontend.schedule.personal_month');
        if ($request->get('date')) {
            $date = $request->get('date');
        } else {
            $date = date('Y-m-d');
        }
        if ($request->get('uid')) {
            $uid = $request->get('uid');
        } else {
            $uid = \Auth::guard('member')->user()->id;
        }
        $member = $this->memberRepo->find($uid);
        $start_date = date('Y-m-01', strtotime($date));
        $end_date = date('Y-m-t', strtotime($date));
        $start_week = (new \DateTime($start_date))->format("W");
        $end_week = (new \DateTime($end_date))->format("W");
        if ($start_week == 53) {
            $start_week = -1;
        }
        if((new \DateTime($end_date))->format("w") == 0){
            $end_week += 1;
        }
        $count_week = $end_week - $start_week + 1;
        $week = (new \DateTime($start_date))->format("W");
        $year = date('Y', strtotime($date));
        if ($week > 52) {
            $year = $year - 1;
        }
        $month = date('m', strtotime($date));
        $year_html = '';
        $html = '';
        for ($i = $week; $i < $week + $count_week; $i++) {
            $html .= '<tr id="um__1">';
            for ($j = 0; $j < 7; $j++) {
                if ($j == 0) {
                    $data = $this->getStartDate($i - 1, $year, 6);
                    $start_week = $data['date'];
                } else {
                    $data = $this->getStartDate($i, $year, $j - 1);
                }
                $list_schedule = '';
                $list_todo = '';
                $schedules = $this->scheduleRepo->getAllByMemberDay([$uid], $data['date']);
                $todos = $this->todoRepo->getListByDate($uid, $data['date']);
                foreach ($schedules as $key => $val) {
                    if($val->pattern == 3){
                        if($val->type_repeat == 'week' || $val->type_repeat == 'weekday'){
                            $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">';
                        }elseif($val->type_repeat == 'day'){
                            $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">';
                        }else{
                            $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">';
                        }
                     }else{
                        $repeat = '';
                     }
                    $menu = is_null($val->menu) ? '' : '<span class="event_color' . explode(';#', $val->menu)[1] . '_grn">' . explode(';#', $val->menu)[0] . '</span>';
                    if(in_array(\Auth::guard('member')->user()->id,$val->member()->pluck('id')->toArray()) || $val->private == 0){
                        $list_time = '<div class="listTime">
                                        <a href="' . route('frontend.schedule.view', $val->id) . '" onclick="cy_um_rd(this);">' .($val->none_time != 1 ? date('h:i A', strtotime($val->start_date)) . '-' . date('h:i A', strtotime($val->end_date)) : '' ). '</a></div>
                                        <div class="'.($val->none_time != 1 ? 'groupWeekEventTitle' : 'groupWeekEventTitleAllday').'">
                                            <a href="' . route('frontend.schedule.view', $val->id) . '" onclick="cy_um_rd(this);">' . $menu . ' ' . $val->title . ' ' . (count($val->equipment) > 0 ? '[' . implode(',', $val->equipment()->pluck('name')->toArray()) . ']' : '') . ' '.$repeat.' </a>
                                        </div>';
                    }else{
                        $list_time = '<div class="listTime">
                                        <a>' .($val->none_time != 1 ? date('h:i A', strtotime($val->start_date)) . '-' . date('h:i A', strtotime($val->end_date)) : '' ). '</a></div>
                                        <div class="'.($val->none_time != 1 ? 'groupWeekEventTitle' : 'groupWeekEventTitleAllday').'">
                                            <a> Đã có lịch </a>
                                         </div>';
                    }
                    $list_schedule .= '<div class="share normalEventElement personalMonth_calendar_item">
                                           '.$list_time.'
                                      </div>';
                }
                foreach ($todos as $key => $todo) {
                    $list_todo .= '<div class="schedule_todo normalEventElement">
                                    <img src="/assets/mobile/img/todoPersonalInSchedule16.png" border="0" alt="">
                                    <a href="' . route('frontend.todo.view', $todo->id) . '" onclick="cy_um_rd(this);">' . $todo->title . '</a>
                                  </div>';
                }
                if ($month == $data['m']) {
                    if ($data['date'] == date('Y-m-d')) {
                        $html .= '<td class="s_today personal_month_calendar_cell normalEvent">
                                    <div class="personalMonthDate-grn"><a href="/schedule/personal_day?bdate=' . date('Y-m-d') . '">' . date('d/m') . '</a></div>
                                    <div class="addEvent"><a href="/schedule/create?bdate=' . date('Y-m-d') . '&amp" onclick="cy_um_rd(this);"><div class="iconWrite-grn" title="Add"></div></a></div>
                                    <div class="js_customization_schedule_date_' . date('Y-m-d') . '" data-date="' . date('Y-m-d') . '"></div>
                                    <div class="personalMonthInfo"></div>
                                    <div class="schedule_todo">
                                        ' . $list_todo . '
                                    </div>
                                    ' . $list_schedule . '
                                 </td>';
                    } else {
                        $html .= '<td class="s_date_' . strtolower(date('l', strtotime($data['date']))) . '_odd personal_month_calendar_cell normalEvent" rel="/schedule/simple_add?bdate=' . $data['date'] . '" plid="0" utype="">
                                    <div class="personalMonthDate-grn"><a href="/schedule/personal_day?bdate=' . $data['date'] . '">' . date('d/m', strtotime($data['date'])) . '</a></div>
                                    <div class="addEvent">
                                       <a href="/schedule/create?bdate=' . $data['date'] . '&amp" onclick="cy_um_rd(this);">
                                          <div class="iconWrite-grn" title="Add"></div>
                                       </a>
                                    </div>
                                    <div class="js_customization_schedule_date_' . $data['date'] . '" data-date="' . $data['date'] . '"></div>
                                    <div class="personalMonthInfo"></div>
                                    <div class="schedule_todo">
                                       ' . $list_todo . '
                                    </div>
                                    ' . $list_schedule . '
                                </td>';
                    }
                } else {
                    $html .= '<td class="s_date_' . strtolower(date('l', strtotime($data['date']))) . '_even personal_month_calendar_cell normalEvent" rel="/schedule/simple_add?bdate=' . $data['date'] . '" plid="0" utype="">
                        <div class="personalMonthDate-grn"><a href="/schedule/personal_day?bdate=' . $data['date'] . '&amp;uid=' . $uid . '&amp;gid=&amp;search_text=">' . date('d/m', strtotime($data['date'])) . '</a></div>
                        <div class="addEvent">
                           <a href="/schedule/create?bdate=' . $data['date'] . '&amp;uid=' . $uid . '" onclick="cy_um_rd(this);">
                              <div class="iconWrite-grn" title="Add"></div>
                           </a>
                        </div>
                        <div class="js_customization_schedule_date_' . $data['date'] . '" data-date="' . $data['date'] . '"></div>
                        <div class="personalMonthInfo"></div>
                        <div class="schedule_todo">
                            ' . $list_todo . '
                        </div>
                        ' . $list_schedule . '
                    </td>';
                }
            }
            $html .= '</tr>';
            $schedules1 = $this->scheduleRepo->getByMemberAllAboutDay([$uid], $start_week, date('Y-m-d', strtotime(" +6 days", strtotime($start_week))));
            foreach ($schedules1 as $val) {
                $time = \App\Helpers\StringHelper::getAboutDay($start_week,$val->start_date,$val->end_date);
                $menu = is_null($val->menu) ? '' : '<span class="event_color' . explode(';#', $val->menu)[1] . '_grn">' . explode(';#', $val->menu)[0] . '</span>';
                if($val->pattern == 2){
                    $html .= '<tr>
                                ' . ($time->first == 0 ? '' : '<td class="br_banner" colspan="' . $time->first . '"><br></td>'). '
                                <td class="s_banner normalEvent" colspan="' . $time->middle . '">
                                <div class="normalEventElement">'.((in_array(\Auth::guard('member')->user()->id,$val->member()->pluck('id')->toArray()) || $val->private == 0) ? '<a href="' . route('frontend.schedule.view', $val->id) . '"><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">' . $menu . ' ' . $val->title . '</a>' : '<a><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">Đã có lịch</a>' ).'</div></td>
                                ' . ($time->last == 0 ? '' : '<td class="br_banner" colspan="' . $time->last . '"><br></td>'). '
                              </tr>';
                }else{
                    $html .= '<tr>
                                ' . ($time->first == 0 ? '' : '<td class="br_banner" colspan="' . $time->first . '"><br></td>'). '
                                <td class="s_task s_banner normalEvent" colspan="' . $time->middle . '">
                                <div><a href="' . route('frontend.schedule.view', $val->id) . '"><img src="' . asset('/img/banner16.gif') . '" border="0" alt="">' . $menu . ' ' . $val->title . ' ('.$val->percent.'%)</a></div></td>
                                ' . ($time->last == 0 ? '' : '<td class="br_banner" colspan="' . $time->last . '"><br></td>'). '
                              </tr>';
                }
            }
        }
        $next_month = date("Y-m-d", strtotime("+1 month", strtotime($date)));
        $prev_month = date("Y-m-d", strtotime("-1 month", strtotime($date)));
        $calendar_html = $this->scheduleRepo->showCalendarMonth(date('Y-m-01', strtotime($date)), 'schedule/personal_month');
        return view('frontend/schedule/personal_month', compact('html', 'next_month', 'prev_month', 'date','calendar_html','member'));
    }

    public function personalYear() {
        Session::put('redirect_route','frontend.schedule.personal_year');
        return view('frontend/schedule/personal_year');
    }

    public function groupDay(Request $request) {
        //start 1 member
        Session::put('redirect_route','frontend.schedule.group_day');
        $input = $request->all();
        $member_ids = [];
        
        if ($request->get('uid')) {
            $members = \App\Member::whereIn('id', explode(',', $request->get('uid')))->paginate(10);
        } else {
            if ($request->get('gid') == 'search' && $request->get('type_search') == 'user') {
                $members = $this->memberRepo->searchByKeyword($request->get('search_text'));
            }elseif($request->get('gid') == 'all'){
                $members = $this->memberRepo->getAll();
            }elseif($request->get('gid')){
                $department = $this->departmentRepo->find($request->get('gid'));
                $department_id = $department->getIds();
                $members = $this->memberRepo->getByDepartment($department_id, \Auth::guard('member')->user()->id);
            }elseif(!$request->get('eid') && !$request->get('gid')) {
                if(!is_null(\Auth::guard('member')->user()->department)){
                    $department_id = \Auth::guard('member')->user()->department->getIds();
                }else{
                    $department_id= [];
                }
                $members = $this->memberRepo->getByDepartment($department_id, \Auth::guard('member')->user()->id);
            }
        }
        $member_ids[] = \Auth::guard('member')->user()->id;
        foreach ($members as $key => $val) {
            $member_ids[] = $val->id;
        }
        if (isset($input['bdate'])) {
            $date = $input['bdate'];
        } else {
            $date = date('Y-m-d');
        }
        $member_arr = implode(',', $member_ids);
        $first_day = $this->scheduleRepo->getByMemberFirstDay($member_ids,$date);
        $last_day = $this->scheduleRepo->getByMemberLastDay($member_ids,$date);
        if(!is_null($first_day) && strtotime(date('Y-m-d',strtotime($first_day->start_date))) < strtotime($date)){
            $first_hour = 0;
        }else{
            if(!is_null($first_day) && date('H',strtotime($first_day->start_date)) < 8){
                $first_hour = date('H',strtotime($first_day->start_date));
            }else{
                $first_hour = 8;
            }
        }
        if(!is_null($last_day) && strtotime(date('Y-m-d',strtotime($last_day->end_date))) > strtotime($date)){
                $last_hour = 23;
        }elseif($last_day && date('H',strtotime($last_day->end_date)) > 18){
                $last_hour = date('H',strtotime($last_day->end_date));
        }else{
                $last_hour = 18;
        }
        $total_hour = $last_hour - $first_hour + 1;
        $total_minute = $total_hour * 6;
        if (isset($input['start_hour']) && isset($input['end_hour'])) {
            $in_time = date('Y-m-d H:i', strtotime(date('Y-m-d') . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
            $out_time = date('Y-m-d H:i', strtotime(date('Y-m-d') . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
            $time1 = date('Y-m-d '.$first_hour.':00');
            $in_time1 = (strtotime($in_time) - strtotime($time1)) / (10 * 60);
            $out_time1 = (strtotime($out_time) - strtotime($time1)) / (10 * 60);
        }
        $html = '';
        if (!$request->get('eid')) {
            foreach ($member_ids as $k => $member_id) {
                $td_head = '';
                for($i = $first_hour;$i < $last_hour + 1;$i++){
                    if( $i<12 ){
                        $mea = 'm';
                    }elseif($i> 11 && $i< 18){
                        $mea = 'e';
                    }else{
                        $mea = 'a';
                    }
                    $td_head .= '<td align="center" class="'.$mea.'" colspan="6">'.$i.'</td>';
                }
                $html .= '<tr class="day_table_time_login bar_login_timezone">
                            <td width="22%" class="group_day_calendar_timebar1"><span class="domain">(UTC+07:00) VietNam</span></td>
                            <td width="10%" class="group_day_calendar_timebar2"><br></td>
                            '.$td_head.'
                        </tr>';
                $time_line = '';
                for ($i = 0; $i < $total_minute; $i++) {
                    if (isset($input['start_hour']) && isset($input['end_hour'])) {
                        if ($i >= $in_time1 && $i < $out_time1) {
                            $time_line .= '<td class="n"></td>';
                        } else {
                            if ($i >= 0 && $i < ((12 - $first_hour) * 6)) {
                                $time_line .= '<td class="m"></td>';
                            }
                            if ($i >= ((12 - $first_hour) * 6) && $i < ((18 - $first_hour) * 6)) {
                                $time_line .= '<td class="e"></td>';
                            }
                            if ($i >= ((18 - $first_hour) * 6) && $i < $total_minute) {
                                $time_line .= '<td class="a"></td>';
                            }
                        }
                    } else {
                        if ($i >= 0 && $i < ((12 - $first_hour) * 6)) {
                            $time_line .= '<td class="m"></td>';
                        }
                        if ($i >= ((12 - $first_hour) * 6) && $i < ((18 - $first_hour) * 6)) {
                            $time_line .= '<td class="e"></td>';
                        }
                        if ($i >= ((18 - $first_hour) * 6) && $i < $total_minute) {
                            $time_line .= '<td class="a"></td>';
                        }
                    }
                }
                $html .= '<tr class="day_table_time_login bar_login_timezone">
                    <td class="group_day_calendar_timebar_sec1"><img src="/img/spacer1.gif" border="0" alt=""></td>
                    <td class="group_day_calendar_timebar_sec2"><img src="/img/spacer1.gif" border="0" alt=""></td>
                    ' . $time_line . '
                 </tr>';
                $schedules = $this->scheduleRepo->getByMemberDay([$member_id], $date);
                $schedule_none_time = $this->scheduleRepo->getByMemberDayNoneTime([$member_id], $date);
                $todos = $this->todoRepo->getListByDate($member_id, $date);
                $member = $this->memberRepo->find($member_id);
                $arr = [];
                $todo_html = '';
                $schedule_none_time_html = '';
                foreach ($todos as $key => $todo) {
                    $todo_html .= '<div class="schedule_todo normalEventElement">
                            <img src="/assets/mobile/img/todoPersonalInSchedule16.png" border="0" alt=""><a href="' . route('frontend.schedule.view', $todo->id) . '">' . $todo->title . '</a>
                         </div>';
                }
                foreach($schedule_none_time as $key=>$schedule_none){
                    $schedule_none_time_html .='<div class="normalEventElement">
                                                    <img src="/img/allday.gif" border="0" alt="">
                                                    <a href="'.route('frontend.schedule.view',$schedule_none->id).'">'.$schedule_none->title.'</a>
                                               </div>';
                }
                if (count($schedules) > 0) {
                    foreach ($schedules as $key => $schedule) {
                        if(strtotime(date('Y-m-d',strtotime($schedule->start_date))) < strtotime($date)){
                            $start_time = date('Y-m-d 00:00', strtotime($date));
                            $start_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                        }else{
                            $start_time = date('Y-m-d H:i', strtotime($schedule->start_date));
                            $start_time1 = date('Y-m-d '.$first_hour.':00', strtotime($schedule->start_date));
                        }
                        $start = (strtotime($start_time) - strtotime($start_time1)) / (10 * 60);

                        if(strtotime(date('Y-m-d',strtotime($schedule->end_date))) > strtotime($date)){
                            $end_time = date('Y-m-d 24:00', strtotime($date));
                            $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                        }else{
                            if(strtotime(date('Y-m-d',strtotime($schedule->start_date))) < strtotime($date)){
                                $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                            }else{
                                $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($schedule->start_date));
                            }
                            $end_time = date('Y-m-d H:i', strtotime($schedule->end_date));
                        }
                        $end = (strtotime($end_time) - strtotime($end_time1)) / (10 * 60);
                        $object = new \stdClass();
                        $object->start = $start;
                        $object->end = $end;
                        $object->status = 1;
                        $object->count = $end - $start;
                        $object->schedule = $schedule;
                        $arr[] = $object;
                    }
                    $data = $this->scheduleRepo->getAboutTime($arr,$total_minute)['data'];
                    $duplicate = $this->scheduleRepo->getAboutTime($arr,$total_minute)['duplicate'];
                    $line_time = '';
                    foreach ($data as $key => $value){
                        if ($value->status == 0) {
                            $line_time .= '<td colspan="' . $value->count . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>';
                        } else {
                            $menu = is_null($value->schedule->menu) ? '' : '<span class="event_color' . explode(';#', $value->schedule->menu)[1] . '_grn">' . explode(';#', $value->schedule->menu)[0] . '</span>';
                            if($value->schedule->pattern == 3){
                                if($value->schedule->type_repeat == 'week' || $value->schedule->type_repeat == 'weekday'){
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">';
                                }elseif($value->schedule->type_repeat == 'day'){
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">';
                                }else{
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">';
                                }
                             }else{
                                $repeat = '';
                             }
                             $title_facility = count($value->schedule->equipment) > 0 ? '[' . implode(',', $value->schedule->equipment()->pluck('name')->toArray()) . ']' : '';
                             if(in_array(\Auth::guard('member')->user()->id,$value->schedule->member->pluck('id')->toArray()) || $value->schedule->private == 0){
                                 $title = '<a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . ' '.$repeat.'</a>';
                             }else{
                                 $title = '<a>Đã có lịch</a>';
                             }
                            $line_time .= '<td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                            <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                '.$title.'
                                            </div>
                                          </td>';
                        }
                    }
                    if (count($duplicate) > 0) {
                        foreach ($duplicate as $key => $val) {
                            $dup_html = '';
                            if($val->schedule->pattern == 3){
                                if($val->schedule->type_repeat == 'week' || $val->schedule->type_repeat == 'weekday'){
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">';
                                }elseif($val->schedule->type_repeat == 'day'){
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">';
                                }else{
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">';
                                }
                             }else{
                                $repeat = '';
                             }
                            $menu = is_null($val->schedule->menu) ? '' : '<span class="event_color' . explode(';#', $val->schedule->menu)[1] . '_grn">' . explode(';#', $val->schedule->menu)[0] . '</span>';
                            if(in_array(\Auth::guard('member')->user()->id,$val->schedule->member->pluck('id')->toArray()) || $val->schedule->private == 0){
                                 $title = '<a href="' . route('frontend.schedule.view', $val->schedule->id) . '">
                                                <span class="attention"><img src="/img/attention16.gif" border="0" title="Conflicting appointments" alt="Conflicting appointments" align="absmiddle"></span>' . $menu . ' ' . $val->schedule->title . ' ' . $title_facility . ' '.$repeat.'
                                           </a>';
                            }else{
                                 $title = '<a><span class="attention"><img src="/img/attention16.gif" border="0" title="Conflicting appointments" alt="Conflicting appointments" align="absmiddle"></span>Đã có lịch</a>';
                            }
                            $title_facility = '';
                            if ($val->start > 0) {
                                if ($val->end >= $total_minute) {
                                    $dup_html .= '<td colspan="' . $val->start . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>
                                                    <td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . ($total_minute - $val->start) . '">
                                                        <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                            '.$title.'
                                                        </div>
                                                    </td>';
                                } else {
                                    $dup_html .= '<td colspan="' . $val->start . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>
                                                  <td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->count . '">
                                                        <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                             '.$title.'
                                                        </div>
                                                  </td>
                                                  <td colspan="' . ($total_minute - $val->end) . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>';
                                }
                            } else {
                                if ($val->end >= $total_minute) {
                                    $dup_html .= '<td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->end . '">
                                                    <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                            '.$title.'
                                                    </div>
                                                  </td>';
                                } else {
                                    $dup_html .= '<td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->count . '">
                                                        <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                             '.$title.'
                                                        </div>
                                                  </td>
                                                  <td colspan="' . ($total_minute - $val->end) . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>';
                                }
                            }
                            $line_time .= '<tr>
                            <td class="group_day_calendar_conflict_head1"><br></td>
                            <td class="group_day_calendar_conflict_head2"><br></td>
                            ' . $dup_html . '
                        </tr>';
                        }
                    }
                    if (is_null(\Auth::guard('member')->user()->file())) {
                        $avatar = '<div class="profileImageUser-grn"></div>';
                    } else {
                        $avatar = '<div class="user_photo_grn" style="background-image: url(/' . $member->file()->link . ');" aria-label=""></div>';
                    }
                    $html .= '<tr>
                    <td class="userBox group_day_calendar_user">
                        <div class="userElement profileImageBase-grn profileImageBaseSchedule-grn">
                           <dl>
                              <dt>
                                 <a href="#">
                                    <div class="profileImage-grn">
                                       <div class="profileImageFrame-grn">
                                          ' . $avatar . '
                                       </div>
                                    </div>
                                 </a>
                              </dt>
                              <dd><a href="#">' . htmlentities($member->full_name) . '</a></dd>
                           </dl>
                           <div class="clear_both_0px"></div>
                        </div>
                        <div class="shortcut_box_full"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pday20.gif" border="0" alt="'.trans('base.Day').'" title="'.trans('base.Day').'" class="small_link">'.trans('base.Day').'</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week??bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pweek20.gif" border="0" alt="'.trans('base.Week').'" title="'.trans('base.Week').'" class="small_link">'.trans('base.Week').'</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month??bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'" class="small_link">'.trans('base.Month').'</a></span></div>
                        <div class="shortcut_box_short" style="display:none"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="'.date('Y-m-d').'"><img src="/img/cal_pday20.gif" border="0" alt="'.trans('base.Day').'" title="'.trans('base.Day').'" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pweek20.gif" border="0" alt="'.trans('base.Week').'" title="'.trans('base.Week').'" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month??bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'" class="small_link"></a></span></div>

                    </td>
                    <td class="normalEvent group_day_calendar_event_cell">
                       ' . $todo_html . '
                    </td>
                    ' . $line_time . '
                 </tr>';
                } else {
                    if (is_null($member->file())) {
                        $avatar = '<div class="profileImageUser-grn"></div>';
                    } else {
                        $avatar = '<div class="user_photo_grn" style="background-image: url(/' . $member->file()->link . ');" aria-label=""></div>';
                    }
                    $html .= '<tr>
                        <td class="userBox group_day_calendar_user">
                            <div class="userElement profileImageBase-grn profileImageBaseSchedule-grn">
                               <dl>
                                  <dt>
                                     <a href="#">
                                        <div class="profileImage-grn">
                                           <div class="profileImageFrame-grn">
                                              ' . $avatar . '
                                           </div>
                                        </div>
                                     </a>
                                  </dt>
                                  <dd><a href="#">' . htmlentities($member->full_name) . '</a></dd>
                               </dl>
                               <div class="clear_both_0px"></div>
                            </div>
                            <div class="shortcut_box_full"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pday20.gif" border="0" alt="'.trans('base.Day').'" title="'.trans('base.Day').'" class="small_link">'.trans('base.Day').'</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pweek20.gif" border="0" alt="'.trans('base.Week').'" title="'.trans('base.Week').'" class="small_link">'.trans('base.Week').'</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month?bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'" class="small_link">'.trans('base.Month').'</a></span></div>
                            <div class="shortcut_box_short" style="display:none"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pday20.gif" border="0" alt="'.trans('base.Day').'" title="'.trans('base.Day').'" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pweek20.gif" border="0" alt="'.trans('base.Week').'" title="'.trans('base.Week').'" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month?bdate='.date('Y-m-d').'&uid='.$member->id.'"><img src="/img/cal_pmon20.gif" border="0" alt="'.trans('base.Month').'" title="'.trans('base.Month').'" class="small_link"></a></span></div>

                        </td>
                        <td class="normalEvent group_day_calendar_event_cell">
                           ' . $todo_html . '
                           '.$schedule_none_time_html.'
                        </td>
                        <td colspan="'.$total_minute.'" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                     </tr>';
                }
                //end 1 member
                $schedules1 = $this->scheduleRepo->getByMemberAllDay([$member_id], $date);
                foreach ($schedules1 as $key => $val) {
                    $html .= '<tr>
                        <td class="group_day_calendar_banner_head1"><br></td>
                        <td class="group_day_calendar_banner_head2"><br></td>
                        <td class="group_day_calendar_banner_item banner_color" colspan="'.$total_minute.'"><a href="' . route('frontend.schedule.view', $val->id) . '" ><img src="/img/banner16.gif" border="0" alt="">' .($val->pattern == 4 ? $val->title.' ('.$val->percent.'%)' : $val->title) . '</a></td>
                    </tr>';
                }
            }
        }
        if ($request->get('eid')) {
            $facility = $this->equipmentRepo->find($request->get('eid'));
            $html .= '<tr class="day_table_time_login bar_login_timezone">
                        <td width="22%" class="group_day_calendar_timebar1"><span class="domain">(UTC+07:00) VietNam</span></td>
                        <td width="10%" class="group_day_calendar_timebar2"><br></td>
                        <td align="center" class="m" colspan="6">8</td>
                        <td align="center" class="m" colspan="6">9</td>
                        <td align="center" class="m" colspan="6">10</td>
                        <td align="center" class="m" colspan="6">11</td>
                        <td align="center" class="e" colspan="6">12</td>
                        <td align="center" class="e" colspan="6">13</td>
                        <td align="center" class="e" colspan="6">14</td>
                        <td align="center" class="e" colspan="6">15</td>
                        <td align="center" class="e" colspan="6">16</td>
                        <td align="center" class="e" colspan="6">17</td>
                        <td align="center" class="a" colspan="6">18</td>
                    </tr>';
             $time_line = '';
             for ($i = 0; $i < $total_minute; $i++) {
                if (isset($input['start_hour']) && isset($input['end_hour'])) {
                    if ($i >= $in_time1 && $i < $out_time1) {
                        $time_line .= '<td class="n"></td>';
                    } else {
                        if ($i >= 0 && $i < ((12 - $first_hour) * 6)) {
                            $time_line .= '<td class="m"></td>';
                        }
                        if ($i >= ((12 - $first_hour) * 6) && $i < ((18 - $first_hour) * 6)) {
                            $time_line .= '<td class="e"></td>';
                        }
                        if ($i >= ((18 - $first_hour) * 6) && $i < $total_minute) {
                            $time_line .= '<td class="a"></td>';
                        }
                    }
                } else {
                    if ($i >= 0 && $i < ((12 - $first_hour) * 6)) {
                        $time_line .= '<td class="m"></td>';
                    }
                    if ($i >= ((12 - $first_hour) * 6) && $i < ((18 - $first_hour) * 6)) {
                        $time_line .= '<td class="e"></td>';
                    }
                    if ($i >= ((18 - $first_hour) * 6) && $i < $total_minute) {
                        $time_line .= '<td class="a"></td>';
                    }
                }
            }
            $html .= '<tr class="day_table_time_login bar_login_timezone">
                        <td class="group_day_calendar_timebar_sec1"><img src="/img/spacer1.gif" border="0" alt=""></td>
                        <td class="group_day_calendar_timebar_sec2"><img src="/img/spacer1.gif" border="0" alt=""></td>
                        ' . $time_line . '
                     </tr>';
            $schedules = $this->scheduleRepo->getByEquipmentDay([$facility->id], $date);
            $arr = [];
            if (count($schedules) > 0) {
                foreach ($schedules as $key => $schedule) {
                    $start_time = date('Y-m-d H:i', strtotime($schedule->start_date));
                    $start_time1 = date('Y-m-d 8:00', strtotime($schedule->start_date));
                    $start = (strtotime($start_time) - strtotime($start_time1)) / (10 * 60);
                    $end_time = date('Y-m-d H:i', strtotime($schedule->end_date));
                    $end_time1 = date('Y-m-d 8:00', strtotime($schedule->end_date));
                    $end = (strtotime($end_time) - strtotime($end_time1)) / (10 * 60);
                    $object = new \stdClass();
                    $object->start = $start;
                    $object->end = $end;
                    $object->status = 1;
                    $object->count = $end - $start;
                    $object->schedule = $schedule;
                    $arr[] = $object;
                }
                $arr = collect($arr);
                $arr = array_values($arr->sortBy('start')->toArray());
                $line_time = '';
                $check = 0;
                foreach ($arr as $key => $value) {
                    $repeat = $value->schedule->pattern == 3 ? '<img src="/img/repeat16.gif" border="0" alt="">' : '';
                    $menu = is_null($value->schedule->menu) ? '' : '<span class="event_color' . explode(';#', $value->schedule->menu)[1] . '_grn">' . explode(';#', $value->schedule->menu)[0] . '</span>';
                    $title_facility = count($value->schedule->equipment) > 0 ? '[' . implode(',', $value->schedule->equipment()->pluck('name')->toArray()) . ']' : '';
                    if ($key == 0) {
                        if (count($arr) == 1) {
                            $line_time .= '<td colspan="' . $value->start . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                                       <td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent">
                                            <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . ' '.$repeat.'</a>
                                            </div>
                                        </td>
                                        <td colspan="' . ($total_minute - $value->end) . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>';
                        } else {
                            if ($value->start > 0) {
                                $line_time .= '<td colspan="' . $value->start . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                                           <td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent">
                                                <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                    <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . '</a>
                                                </div>
                                            </td>';
                            } else {
                                $line_time .= '<td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent">
                                                <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                    <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . '</a>
                                                </div>
                                            </td>';
                            }
                            $check = $value->end;
                        }
                    } elseif ($key > 0 && $key < count($arr) - 1) {
                        if ($value->start > $check) {
                            $line_time .= '<td colspan="' . ($value->start - $check) . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                                       <td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                            <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . '</a>
                                            </div>
                                        </td>';
                        } else {
                            $line_time .= '<td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                            <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . '</a>
                                            </div>
                                        </td>';
                        }
                        $check = $value->end;
                    } elseif ($key == count($arr) - 1) {
                        if ($value->start > $check) {
                            if ($value->end < $total_minute) {
                                $line_time .= '<td colspan="' . ($value->start - $check) . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                                           <td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                                <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                    <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . '</a>
                                                </div>
                                            </td>
                                            <td colspan="' . ($total_minute - $value->end) . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>';
                            } else {
                                $line_time .= '<td colspan="' . ($value->start - $check) . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                                           <td colspan="' . ($total_minute - $value->end) . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                                <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                    <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . '</a>
                                                </div>
                                            </td>';
                            }
                        } else {
                            if ($value->end < $total_minute) {
                                $line_time .= '<td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                                <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                    <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . '</a>
                                                </div>
                                            </td>
                                            <td colspan="' . ($total_minute - $value->end) . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>';
                            } else {
                                $line_time .= '<td colspan="' . ($total_minute - $value->end) . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                                <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                    <a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . '</a>
                                                </div>
                                            </td>';
                            }
                        }
                    }
                }
                $html .= '<tr>
                            <td class="userBox group_day_calendar_user">
                                <span class="nowrap-grn ">
                                    <a href="/facility_info?faid=43&amp;referer_key=628d6eac291a6fe6aa8ab744183cf06c">
                                     <img src="/img/facility20.gif" border="0" alt="">' . $facility->name . '
                                    </a>
                                </span>
                                <div class="shortcut_box_facility_grn"><span class="nowrap-grn ">
                                    <a class="small_link" title="Day" href="/schedule/personal_day?bdate='.$date.'&amp;eid='. $facility->id .'&amp;gid=selected&amp;search_text=&amp;event=">
                                        <img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">
                                    </a>
                                    </span>
                                    <span class="nowrap-grn schedule_userbox_item_grn">
                                        <a class="small_link" href="/schedule/personal_week?bdate='.$date.'&amp;eid='. $facility->id .'&amp;gid=selected&amp;search_text=">
                                        <img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">
                                        </a>
                                    </span>
                                    <span class="nowrap-grn schedule_userbox_item_grn">
                                        <a class="small_link" href="/schedule/personal_month?bdate='.$date.'&amp;eid='. $facility->id .'&amp;gid=f9&amp;search_text=">
                                        <img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link"></a>
                                    </span>
                                </div>
                            </td>
                            <td class="normalEvent group_day_calendar_event_cell">
                            </td>
                        ' . $line_time . '
                     </tr>';
            } else {
                if (is_null(\Auth::guard('member')->user()->file())) {
                    $avatar = '<div class="profileImageUser-grn"></div>';
                } else {
                    $avatar = '<div class="user_photo_grn" style="background-image: url(/' . $member->file()->link . ');" aria-label=""></div>';
                }
                $html .= '<tr>
                            <td class="userBox group_day_calendar_user">
                                <div class="userElement profileImageBase-grn profileImageBaseSchedule-grn">
                                   <dl>
                                      <dt>
                                         <a href="#">
                                            <div class="profileImage-grn">
                                               <div class="profileImageFrame-grn">
                                                  ' . $avatar . '
                                               </div>
                                            </div>
                                         </a>
                                      </dt>
                                      <dd><a href="#">' . htmlentities($member->full_name) . '</a></dd>
                                   </dl>
                                   <div class="clear_both_0px"></div>
                                </div>
                                <div class="shortcut_box_full"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?bdate='.date('Y-m-d').'"><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">Day</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate=2021-01-19&amp;uid=58&amp;gid=selected&amp;search_text="><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">Week</a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month?bdate=2021-01-19&amp;uid=58&amp;gid=&amp;search_text="><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">Month</a></span></div>
                                <div class="shortcut_box_short" style="display:none"><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_day?bdate='.date('Y-m-d').'"><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_week?bdate=2021-01-19&amp;uid=58&amp;gid=selected&amp;search_text="><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link"></a></span><span class="nowrap-grn schedule_userbox_item_grn"><a class="small_link" href="/schedule/personal_month?bdate=2021-01-19&amp;uid=58&amp;gid=&amp;search_text="><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link"></a></span></div>

                            </td>
                            <td class="normalEvent group_day_calendar_event_cell">
                               ' . $todo_html . '
                               '.$schedule_none_time_html.'
                            </td>
                            <td colspan="'.$total_minute.'" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                         </tr>';
                //end 1 member
            }
        }
        if (isset($input['start_hour'])) {
            $start_hour_html = \App\Helpers\StringHelper::getSelectHour($input['start_hour']);
        } else {
            $start_hour_html = \App\Helpers\StringHelper::getSelectHour();
        }
        if (isset($input['start_minute'])) {
            $start_minute_html = \App\Helpers\StringHelper::getSelectMinute($input['start_minute']);
        } else {
            $start_minute_html = \App\Helpers\StringHelper::getSelectMinute();
        }
        if (isset($input['end_hour'])) {
            $end_hour_html = \App\Helpers\StringHelper::getSelectHour($input['end_hour']);
        } else {
            $end_hour_html = \App\Helpers\StringHelper::getSelectHour();
        }
        if (isset($input['end_minute'])) {
            $end_minute_html = \App\Helpers\StringHelper::getSelectMinute($input['end_minute']);
        } else {
            $end_minute_html = \App\Helpers\StringHelper::getSelectMinute();
        }
        $departments = $this->departmentRepo->getAllStack(0);
        $equipments = $this->equipmentRepo->all();
        $equipment = [];
        $department = [];
        foreach ($departments as $key => $val) {
            $object2 = new \stdClass();
            $object2->id = $val->id;
            $object2->name = $val->name;
            $object2->level = $val->level;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->type = "membership";
            $object2 = json_encode($object2);
            $department[] = $object2;
        }
        foreach ($equipments as $key => $val) {
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->is_selected = "";
            $object2->extra_param = "";
            $object2->count = "0";
            $object2->children = [];
            $object2 = json_encode($object2);
            $equipment[] = $object2;
        }
        $department = implode(',', $department);
        $equipment = implode(',', $equipment);
        if (isset($input['gid'])) {
            $calendar_html = $this->scheduleRepo->showCalendar(date('Y-m-01', strtotime($date)), 'schedule/group_day', $input['gid']);
        } else {
            $calendar_html = $this->scheduleRepo->showCalendar(date('Y-m-01', strtotime($date)), 'schedule/group_day');
        }
        $mobile_html = '';
        if((!isset($input['gid']) || strpos($input['gid'],'f') === false) || $input['gid'] == '' || $input['gid'] == 'search'){
            foreach ($member_ids as $k => $member_id) {
                $schedules = $this->scheduleRepo->getByMemberDay([$member_id], $date);
                $member = $this->memberRepo->find($member_id);
                $avatar = $member_id == \Auth::guard('member')->user()->id ? 'mobile_img_userLoginPlofile_grn' : 'mobile_img_userPlofile_grn';
                $mobile_html .= '<div class="mobile_schedule_day_subtitle_grn">
                                    <div class="mobile_icon_grn ' . $avatar . '"></div>
                                    <div class="mobile_title_grn">' . htmlentities($member->full_name) . '</div>
                                    <div class="mobile_subtitle_grn"></div>
                                    <a href="'.route('frontend.schedule.create').'" class="mobile_icon_add_grn ui-link"></a><a data-answer="'.$member->id.'" er="" href="javascript:void(0);" class="mobile_icon_week_grn ui-link"></a>
                                </div>
                                <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_list_normal_padding_grn"></ul>
                                <ul data-role="listview" data-theme="c" class="mobile_schedule_ul_grn mobile-ul-top-grn mobile-ul-bottom-grn mobile_list_normal_padding_grn"></ul>
                                <ul data-role="listview" data-theme="c" class="mobile_schedule_ul_grn mobile-ul-top-grn mobile-ul-bottom-grn mobile_list_normal_padding_grn"></ul>
                                <ul data-role="listview" data-theme="c" class="mobile_schedule_ul_grn mobile-ul-top-grn mobile-ul-bottom-grn mobile_list_normal_padding_grn ui-listview ui-group-theme-c">';
                $list_schedule_html = '';
                if (count($schedules) > 0) {
                    foreach ($schedules as $key => $schedule) {
                        $repeat = $val->pattern == 3 ? '<img src="/img/repeat16.gif" border="0" alt="">' : '';
                        $menu = is_null($val->menu) ? '' : '<span class="mobile_event_menu_grn mobile_event_menu_color' . explode(';#', $val->menu)[1] . '_grn">' . explode(';#', $val->menu)[0] . '</span>';
                        $title_facility = count($schedule->equipment) > 0 ? '[' . implode(',', $schedule->equipment()->pluck('name')->toArray()) . ']' : '';
                         $list_schedule_html ='<li class="py-2">
                                                <a href="'.route('frontend.schedule.view',$schedule->id).'" class="item px-0">
                                                    <div>
                                                        <div class="in text-dark">
                                                            <span>' . date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) . '</span>
                                                        </div>
                                                        <div class="text-muted mt-1 pb-1 d-flex flex-row">
                                                            <h4 class="mb-0">' . $menu . ' ' . $schedule->title . ' ' . $title_facility . ' '.$repeat.'</h4>
                                                        </div>
                                                    </div>
                                                </a>
                                             </li>';
                    }
                } else {
                    $mobile_html .= '<li data-icon="false" class="ui-li-static ui-body-inherit ui-first-child ui-last-child">No appointments.</li>';
                }
                $mobile_html .= '</ul>';
            }
        }else{
            if(substr($input['gid'], 1) == ''){
                $facilitys = $this->equipmentRepo->all();
                foreach($facilitys as $key=>$facility){
                    $schedules = $this->scheduleRepo->getByEquipmentDay([$facility->id], $date);
                    $mobile_html .= '<div class="mobile_schedule_day_subtitle_grn">
                                        <div class="mobile_icon_grn mobile_img_facilityPlofile_grn"></div>
                                        <div class="mobile_title_grn">' . $facility->name . '</div>
                                        <div class="mobile_subtitle_grn"></div>
                                        <a href="'.route('frontend.schedule.create').'" class="mobile_icon_add_grn ui-link"></a><a data-answer="'.$facility->id.'" er="" href="javascript:void(0);" class="mobile_icon_week_grn ui-link"></a>
                                    </div>
                                    <ul data-role="listview" data-theme="c" class="mobile-ul-top-grn mobile-ul-bottom-grn mobile_list_normal_padding_grn"></ul>
                                    <ul data-role="listview" data-theme="c" class="mobile_schedule_ul_grn mobile-ul-top-grn mobile-ul-bottom-grn mobile_list_normal_padding_grn"></ul>
                                    <ul data-role="listview" data-theme="c" class="mobile_schedule_ul_grn mobile-ul-top-grn mobile-ul-bottom-grn mobile_list_normal_padding_grn"></ul>
                                    <ul data-role="listview" data-theme="c" class="mobile_schedule_ul_grn mobile-ul-top-grn mobile-ul-bottom-grn mobile_list_normal_padding_grn ui-listview ui-group-theme-c">';
                     if (count($schedules) > 0) {
                        foreach ($schedules as $key => $schedule) {
                            $repeat = $val->pattern == 3 ? '<img src="/img/repeat16.gif" border="0" alt="">' : '';
                            $menu = is_null($val->menu) ? '' : '<span class="mobile_event_menu_grn mobile_event_menu_color' . explode(';#', $val->menu)[1] . '_grn">' . explode(';#', $val->menu)[0] . '</span>';
                            $title_facility = count($schedule->equipment) > 0 ? '[' . implode(',', $schedule->equipment()->pluck('name')->toArray()) . ']' : '';
                            $mobile_html .= '<li data-icon="false" class="ui-first-child">
                                            <a class="mobile-list-text-grn mobile_list_table_grn ui-btn" href="'.route('frontend.schedule.view',$schedule->id).'">
                                               <div class="mobile_list_table_td_grn">
                                                  <div class="mobile_list_lines_grn">
                                                     <ul>
                                                        <li><span class="mobile_list_users_grn">' . date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) . '</span></li>
                                                        <li><span class="mobile_list_content_grn">' . $menu . ' ' . $schedule->title . ' ' . $title_facility . ' '.$repeat.'</span></li>
                                                     </ul>
                                                  </div>
                                               </div>
                                               <span class="mobile_list_icon_arraylist_grn"></span>
                                            </a>
                                         </li>';
                        }
                    } else {
                        $mobile_html .= '<li data-icon="false" class="ui-li-static ui-body-inherit ui-first-child ui-last-child">No appointments.</li>';
                    }
                    $mobile_html .= '</ul>';
                }
            }else{
                $schedules = $this->scheduleRepo->getByEquipmentDay([substr($input['gid'], 1)], $date);
                $facility = $this->equipmentRepo->find(substr($input['gid'], 1));
                $list_schedule_html = '';
                if (count($schedules) > 0) {
                    foreach ($schedules as $key => $schedule) {
                        $repeat = $val->pattern == 3 ? '<img src="/img/repeat16.gif" border="0" alt="">' : '';
                        $menu = is_null($val->menu) ? '' : '<span class="mobile_event_menu_grn mobile_event_menu_color' . explode(';#', $val->menu)[1] . '_grn">' . explode(';#', $val->menu)[0] . '</span>';
                        $title_facility = count($schedule->equipment) > 0 ? '[' . implode(',', $schedule->equipment()->pluck('name')->toArray()) . ']' : '';
                        $list_schedule_html ='<li class="py-2">
                                                <a href="'.route('frontend.schedule.view',$schedule->id).'" class="item px-0">
                                                    <div>
                                                        <div class="in text-dark">
                                                            <span>' . date('h:i A', strtotime($schedule->start_date)) . '-' . date('h:i A', strtotime($schedule->end_date)) . '</span>
                                                        </div>
                                                        <div class="text-muted mt-1 pb-1 d-flex flex-row">
                                                            <h4 class="mb-0">' . $menu . ' ' . $schedule->title . ' ' . $title_facility . ' '.$repeat.'</h4>
                                                        </div>
                                                    </div>
                                                </a>
                                             </li>';
                    }
                } else {
                    $mobile_html .= '<li class="py-2">No Appointment</li>';
                }
                $mobile_html .= '<div class="card mt-2">
                                    <div class="card-body py-1">
                                        <ul class="listview flush transparent image-listview">
                                            <li>
                                                <div class="px-0 item card-title d-flex justify-content-between">
                                                    <img src="http://kaizen.local/mobile/assets/img/sample/avatar/avatar3.jpg" alt="image" class="image">
                                                    <h4 class="in"> ' . $facility->name . ' </h4>
                                                    <a href="'.route('frontend.schedule.create').'" class="text-warning"><ion-icon name="create-outline" role="img" class="md hydrated" aria-label="create outline"></ion-icon></a>
                                                </div>
                                            </li>
                                            '.$list_schedule_html.'
                                        </ul>
                                    </div>
                                </div>';
            }
        }
        if (config('global.device') != 'pc') {
            return view('mobile/schedule/group_day', compact('calendar_html', 'mobile_html', 'date', 'departments', 'department_id','equipments'));
        } else {
            $list_member = $this->memberRepo->getAllNotMe();
            $department_html = \App\Helpers\StringHelper::getSelectDepartmentParent(0);
            return view('frontend/schedule/group_day', compact('department_html','list_member','calendar_html', 'html', 'start_hour_html', 'start_minute_html', 'end_hour_html', 'end_minute_html', 'date', 'member_arr', 'department', 'equipment'));
        }
    }

    function getStartDate($week, $year, $day) {
        $dateTime = new \DateTime();
        $dateTime->setISODate($year, $week);
        $dateTime->modify('+' . $day . ' days');
        $result['d'] = $dateTime->format('d');
        $result['m'] = $dateTime->format('m');
        $result['Y'] = $dateTime->format('Y');
        $result['l'] = strtolower($dateTime->format('l'));
        $result['date'] = $dateTime->format('Y-m-d');
        return $result;
    }

    public function popupCalendar(Request $request) {
        $date = $request->get('date');
        $prefix = "'" . ($request->get('prefix')) . "'";
        $start_date = date('Y-m-01', strtotime($date));
        $week = (new \DateTime($start_date))->format("W");
        $year = date('Y', strtotime($date));
        if ($week > 52) {
            $year = $year - 1;
        }
        $month = date('m', strtotime($date));
        $year_html = '';
        for ($i = $year; $i < $year + 10; $i++) {
            $year_html .= '<option value="' . $i . '"' . ($i == date('Y', strtotime($date)) ? 'selected' : '') . '>' . $i . '</option>';
        }
        $month_html = '';
        for ($i = 1; $i < 13; $i++) {
            $month_html .= '<option value="' . $i . '"' . ($i == $month ? 'selected' : '') . '>' . date('F', mktime(0, 0, 0, $i, 10)) . '</option>';
        }
        $html = '';
        for ($i = $week; $i < $week + 5; $i++) {
            $html .= '<tr>';
            for ($j = 0; $j < 7; $j++) {
                if ($j == 0) {
                    $data = $this->getStartDate($i - 1, $year, 6);
                } else {
                    $data = $this->getStartDate($i, $year, $j - 1);
                }
                if ($month == $data['m']) {

                    if ($data['date'] == date('Y-m-d')) {
                        $html .= '<td align="center" class="s_date_today">
                                    <a href="javascript:SetDate(' . $data['Y'] . ', ' . $data['m'] . ', ' . $data['d'] . ',  ' . $prefix . ')">' . $data['d'] . '</a>
                                 </td>';
                    } else {
                        $html .= '<td align="center" class="s_date_' . $data['l'] . '">
                                    <a href="javascript:SetDate(' . $data['Y'] . ', ' . $data['m'] . ', ' . $data['d'] . ', ' . $prefix . ')">' . $data['d'] . '</a>
                                 </td>';
                    }
                } else {
                    $html .= '<td align="center" class="s_date_other_month">
                                <a href="javascript:SetDate(' . $data['Y'] . ', ' . $data['m'] . ', ' . $data['d'] . ', ' . $prefix . ')">' . $data['d'] . '</a>
                             </td>';
                }
            }
            $html .= '</tr>';
        }
        $next_month = date("Y-m-d", strtotime("+1 month", strtotime($date)));
        $prev_month = date("Y-m-d", strtotime("-1 month", strtotime($date)));
        $form_name = $request->get('form_name');
        if ($form_name == 'schedule/add') {
            $form = 'add';
        } elseif ($form_name == 'schedule/banner_add') {
            $form = 'banner_add';
        } elseif ($form_name == 'schedule/repeat_add') {
            $form = 'repeat_add';
        } elseif ($form_name == 'schedule/modify') {
            $form = 'modify';
        } elseif ($form_name == 'schedule/banner_modify') {
            $form = 'banner_modify';
        } elseif ($form_name == 'todo/modify') {
            $form = 'modify';
        }
        if (($request->get('prefix')) == 'start_' && $form_name == 'schedule/add') {
            $close = "schedule\/addstart_SetDateCal";
        } elseif (($request->get('prefix')) == 'end_' && $form_name == 'schedule/add') {
            $close = "schedule\/addend_SetDateCal";
        }
        if (($request->get('prefix')) == 'start_' && $form_name == 'schedule/banner_add') {
            $close = "schedule\/banner_addstart_SetDateCal";
        } elseif (($request->get('prefix')) == 'end_' && $form_name == 'schedule/banner_add') {
            $close = "schedule\/banner_addend_SetDateCal";
        }
        if (($request->get('prefix')) == 'start_' && $form_name == 'schedule/repeat_add') {
            $close = "schedule\/repeat_addstart_SetDateCal";
        } elseif (($request->get('prefix')) == 'end_' && $form_name == 'schedule/repeat_add') {
            $close = "schedule\/repeat_addend_SetDateCal";
        }
        if (($request->get('prefix')) == 'start_' && $form_name == 'schedule/modify') {
            $close = "schedule\/modifystart_SetDateCal";
        } elseif (($request->get('prefix')) == 'end_' && $form_name == 'schedule/modify') {
            $close = "schedule\/modifyend_SetDateCal";
        }
        if (($request->get('prefix')) == 'start_' && $form_name == 'schedule/banner_modify') {
            $close = "schedule\/banner_modifystart_SetDateCal";
        } elseif (($request->get('prefix')) == 'end_' && $form_name == 'schedule/banner_modify') {
            $close = "schedule\/banner_modifyend_SetDateCal";
        }
        if ($form_name == 'todo/modify') {
            $close = "todo/modifyldate_SetDateCal";
        }
        $pre = $request->get('prefix');
        return view('frontend/schedule/popup_calendar', compact('form_name', 'date', 'prefix', 'html', 'month_html', 'year_html', 'next_month', 'prev_month', 'close', 'pre', 'form'));
    }

    public function popupTimeSelector(Request $request) {
        return view('frontend/schedule/popup_time_selector');
    }

    public function destroy(Request $request,$id) {
        $input = $request->all();
        $schedule = $this->scheduleRepo->find($id);
        if(isset($input['apply']) && $input['apply'] != 'this'){
            if($input['apply'] == 'after'){
                $record = $this->scheduleRepo->deleteAfter($schedule->event,$schedule->start_date);
                $this->scheduleRepo->updateBefore($schedule->event,$schedule->start_date);
            }else{
                $record = $this->scheduleRepo->deleteAll($schedule->event);
            }
        }else{
            \App\MemberSchedule::where('schedule_id',$id)->delete();
            \App\EquipmentSchedule::where('schedule_id',$id)->delete();
            $record = $this->scheduleRepo->delete($id);
        }
        if(Session::get('redirect_route')){
            return redirect()->route(Session::get('redirect_route'))->with('success', 'Xóa thành công');
        }else{
            return redirect()->route('frontend.schedule.index')->with('success', 'Xóa thành công');
        }

    }

    public function popupMemberSelect(Request $request) {
        if ($request->get('select_name')) {
            Session::put('select_name', $request->get('select_name'));
        }
        if ($request->get('plugin_session_name')) {
            Session::put('pluginSessionName', $request->get('plugin_session_name'));
        }
        if ($request->get('plugin_data_name')) {
            Session::put('pluginDataName', $request->get('plugin_data_name'));
        }
        //dd(Session::get('pluginSessionName'));
        $name = Session::get('select_name');
        $pluginSessionName = Session::get('pluginSessionName');
        $pluginDataName = Session::get('pluginDataName');
        if ($request->get('select_func') == 'apply') {
            $script = '<script>grn.component.popup_member_select.reflectToParentWindow(
                        {
                            name: "' . Session::get('select_name') . '",
                            form_name: "grn/popup_member_select",
                            member_list_name: "' . Session::get('select_name') . '",
                            candidate_list_Name: "popup_' . Session::get('select_name') . '",
                            appId: "schedule",
                            isCalendar: "0",
                            includeOrg: "1",
                            accessPlugin: true,
                            pluginSessionName: "' . Session::get('pluginSessionName') . '",
                            pluginDataName: "' . Session::get('pluginDataName') . '",
                            reflect_to_additional_name: "",
                            isPostMessage: ""
                        }
                    );
                    </script>';
        } else {
            $script = '';
        }
        if (!is_null($request->get('session'))) {
            Session::forget('member_arr');
        }
        $departments = $this->departmentRepo->getAll();
        $department = [];
        foreach ($departments as $key => $val) {
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->expanded = "";
            $object2->count = "0";
            $object2->children = [];
            $department[] = $object2;
        }
        $department = json_encode($department);
        if (!is_null($request->get('selected_users_c_id'))) {
            $in_arr = explode(',', $request->get('selected_users_c_id'));
            if (Session::get('member_arr')) {
                $out_arr = explode(',', Session::get('member_arr'));
            } else {
                $out_arr = [];
            }
            foreach ($in_arr as $val) {
                if (!in_array($val, $out_arr)) {
                    array_push($out_arr, $val);
                }
            }
            $out_arr = implode(',', $out_arr);
            Session::put('member_arr', $out_arr);
        }
        if (!is_null($request->get('selected_users_s_id'))) {
            $in_arr = explode(',', $request->get('selected_users_s_id'));
            $out_arr = explode(',', Session::get('member_arr'));
            $result = array_diff($out_arr, $in_arr);
            $result = implode(',', $result);
            Session::put('member_arr', $result);
        }
        $member_html = '';
        if (!is_null(Session::get('member_arr'))) {
            $arr = explode(',', Session::get('member_arr'));
            foreach ($arr as $key => $val) {
                $member = $this->memberRepo->find($val);
                if($member){
                    $member_html .= '<li id="selectlist_popup_' . Session::get('select_name') . '_member_user_' . $member->id . '" class="selectlist_popup_' . Session::get('select_name') . ' selectlist_selected_grn" data-value="' . $member->id . '" data-type="user" data-name="' . $member->full_name . '" data-id="' . $member->id . '">
                                        <span class="selectlist_user_grn" aria-label="" title=""></span>
                                        <span class="selectlist_text_grn">' . htmlentities($member->full_name) . '</span>
                                    </li>';
               }
            }
        }
        if ($request->get('s_oid')) {
            $members = $this->memberRepo->getByDepartment($request->get('s_oid'), \Auth::guard('member')->user()->id);
            $department_id = $request->get('s_oid');
            return view('frontend/schedule/popup_select_member', compact('department', 'members', 'department_id', 'member_html', 'script', 'name', 'pluginSessionName', 'pluginDataName'));
        }
        return view('frontend/schedule/popup_select_member', compact('department', 'member_html', 'script', 'name', 'pluginSessionName', 'pluginDataName'));
    }

    public function popupUserSelect(Request $request) {
        if ($request->get('select_func') == 'multi_apply') {
            Session::forget('user_select');
            Session::put('user_select', $request->get('c_id'));
            $script = "<script language='JavaScript' type='text/javascript'>
                        function __grn_create_apply_url( parent_opener )
                        {
                            var url = '" . route('frontend.schedule.index') . "?';
                            var selected_param = '?gid=selected';
                            var date_str = '';
                            var param_elements = parent_opener.document.getElementsByName( 'bdate' );
                            for( var i = 0; i < param_elements.length; i++ )
                            {
                                if (param_elements[i].getAttribute('value'))
                                {
                                    date_str = param_elements[i].getAttribute('value');
                                    break;
                                }
                            }
                            selected_param = selected_param + 'event=selected';
                            var url_array = url.split('?');
                            if ( url_array.length < 2 )
                            {
                                var sharp_array = url.split('#');
                                if ( sharp_array.length < 2 )
                                {
                                    return url + selected_param;
                                }
                                else
                                {
                                    return sharp_array[0] + selected_param;
                                }
                            }
                            var url_params = grn.component.url.parseQueryString(url);
                            url_params['gid'] = 'selected';

                            url_params['event'] = 'selected';
                            if ( date_str && date_str != '' )
                            {
                                url_params['bdate'] = date_str;
                            }
                            delete url_params['sp'];

                            return url_array[0] + '?' + jQuery.param(url_params);
                        }
                        var parent_opener = window.parent.opener;
                        parent_opener.location.href = __grn_create_apply_url( parent_opener );
                         window.close();

                </script>";
        } else {
            $script = '';
        }
        if (!is_null($request->get('session'))) {
            Session::forget('user_arr');
        }
        $departments = $this->departmentRepo->getAllStack(0);
        $department = [];
        foreach ($departments as $key => $val) {
            $object2 = new \stdClass();
            $object2->oid = $val->id;
            $object2->name = $val->name;
            $object2->level = $val->level;
            $object2->expanded = "";
            $object2->count = "0";
            $object2->children = [];
            $department[] = $object2;
        }
        $department = json_encode($department);
        if (!is_null($request->get('selected_users_c_id'))) {
            $in_arr = explode(',', $request->get('selected_users_c_id'));
            if (Session::get('user_arr')) {
                $out_arr = explode(',', Session::get('user_arr'));
            } else {
                $out_arr = [];
            }
            foreach ($in_arr as $val) {
                if (!in_array($val, $out_arr)) {
                    array_push($out_arr, $val);
                }
            }
            $out_arr = implode(',', $out_arr);
            Session::put('user_arr', $out_arr);
        }
        if (!is_null($request->get('selected_users_s_id'))) {
            $in_arr = explode(',', $request->get('selected_users_s_id'));
            $out_arr = explode(',', Session::get('user_arr'));
            $result = array_diff($out_arr, $in_arr);
            $result = implode(',', $result);
            Session::put('user_arr', $result);
        }
        $member_html = '';
        if (!is_null(Session::get('user_arr'))) {
            $arr = explode(',', Session::get('user_arr'));
            if(count($arr) > 0){
                foreach ($arr as $key => $val) {
                    $member = $this->memberRepo->find($val);
                    if($member){
                        $member_html .= '<option value="' . $member->id . '">' . htmlentities($member->full_name) . '</option>';
                    }
                }
            }
        }
        if ($request->get('s_oid')) {
            $members = $this->memberRepo->getByDepartment([$request->get('s_oid')], \Auth::guard('member')->user()->id);
            $department_id = $request->get('s_oid');
            return view('frontend/schedule/popup_select_user', compact('department', 'members', 'department_id', 'member_html', 'script'));
        }
        return view('frontend/schedule/popup_select_user', compact('department', 'member_html', 'script'));
    }

    public function confirm(Request $request) {
        //start 1 member
        $input = $request->all();
        if (isset($input['date'])) {
            $date = $input['date'];
        } else {
            $date = date('Y-m-d', strtotime($input['start_year'] . '-' . $input['start_month'] . '-' . $input['start_day']));
        }
        if (isset($input['start_hour']) && isset($input['end_hour'])) {
            $in_time = date('Y-m-d H:i', strtotime(date('Y-m-d') . ' ' . $input['start_hour'] . ':' . $input['start_minute']));
            $out_time = date('Y-m-d H:i', strtotime(date('Y-m-d') . ' ' . $input['end_hour'] . ':' . $input['end_minute']));
            $time1 = date('Y-m-d 8:00');
            $in_time1 = (strtotime($in_time) - strtotime($time1)) / (10 * 60);
            $out_time1 = (strtotime($out_time) - strtotime($time1)) / (10 * 60);
        }
        if (isset($input['member_arr'])) {
            $member_ids = explode(',', $input['member_arr']);
        } else {
            $member_ids = $input['sUID'];
        }
        $member_arr = implode(',', $member_ids);
        $first_day = $this->scheduleRepo->getByMemberFirstDay($member_ids,$date);
        $last_day = $this->scheduleRepo->getByMemberLastDay($member_ids,$date);
        if(!is_null($first_day) && strtotime(date('Y-m-d',strtotime($first_day->start_date))) < strtotime($date)){
            $first_hour = 0;
        }else{
            if(!is_null($first_day) && date('H',strtotime($first_day->start_date)) < 8){
                $first_hour = date('H',strtotime($first_day->start_date));
            }else{
                $first_hour = 8;
            }
        }
        if(!is_null($last_day) && strtotime(date('Y-m-d',strtotime($last_day->end_date))) > strtotime($date)){
                $last_hour = 23;
        }elseif($last_day && date('H',strtotime($last_day->end_date)) > 18){
                $last_hour = date('H',strtotime($last_day->end_date));
        }else{
                $last_hour = 18;
        }
        $total_hour = $last_hour - $first_hour + 1;
        $total_minute = $total_hour * 6;
        $html = '';
        foreach ($member_ids as $k => $member_id) {
            $td_head = '';
            for($i = $first_hour;$i < $last_hour + 1;$i++){
                if( $i<12 ){
                    $mea = 'm';
                }elseif($i> 11 && $i< 18){
                    $mea = 'e';
                }else{
                    $mea = 'a';
                }
                $td_head .= '<td align="center" class="'.$mea.'" colspan="6">'.$i.'</td>';
            }
            $html .= '<tr class="day_table_time_login bar_login_timezone">
                        <td width="22%" class="group_day_calendar_timebar1"><span class="domain">(UTC+07:00) VietNam</span></td>
                        <td width="10%" class="group_day_calendar_timebar2"><br></td>
                        '.$td_head.'
                    </tr>';
            $time_line = '';
            for ($i = 0; $i < $total_minute; $i++) {
                if (isset($input['start_hour']) && isset($input['end_hour'])) {
                    if ($i >= $in_time1 && $i < $out_time1) {
                        $time_line .= '<td class="n"></td>';
                    } else {
                        if ($i >= 0 && $i < ((12 - $first_hour) * 6)) {
                            $time_line .= '<td class="m"></td>';
                        }
                        if ($i >= ((12 - $first_hour) * 6) && $i < ((18 - $first_hour) * 6)) {
                            $time_line .= '<td class="e"></td>';
                        }
                        if ($i >= ((18 - $first_hour) * 6) && $i < $total_minute) {
                            $time_line .= '<td class="a"></td>';
                        }
                    }
                } else {
                    if ($i >= 0 && $i < ((12 - $first_hour) * 6)) {
                        $time_line .= '<td class="m"></td>';
                    }
                    if ($i >= ((12 - $first_hour) * 6) && $i < ((18 - $first_hour) * 6)) {
                        $time_line .= '<td class="e"></td>';
                    }
                    if ($i >= ((18 - $first_hour) * 6) && $i < $total_minute) {
                        $time_line .= '<td class="a"></td>';
                    }
                }
            }
            $html .= '<tr class="day_table_time_login bar_login_timezone">
                    <td class="group_day_calendar_timebar_sec1"><img src="/img/spacer1.gif" border="0" alt=""></td>
                    <td class="group_day_calendar_timebar_sec2"><img src="/img/spacer1.gif" border="0" alt=""></td>
                    ' . $time_line . '
                 </tr>';
            $schedules = $this->scheduleRepo->getByMemberDay([$member_id], $date);
            $member = $this->memberRepo->find($member_id);
            $todos = $this->todoRepo->getListByDate($member_id, $date);
            $arr = [];
            $todo_html = '';
            foreach ($todos as $key => $todo) {
                $todo_html .= '<div class="schedule_todo normalEventElement">
                        <img src="/assets/mobile/img/todoPersonalInSchedule16.png" border="0" alt=""><a href="' . route('frontend.schedule.view', $todo->id) . '">' . $todo->title . '</a>
                     </div>';
            }
            if (count($schedules) > 0) {
                foreach ($schedules as $key => $schedule) {
                        if(strtotime(date('Y-m-d',strtotime($schedule->start_date))) < strtotime($date)){
                            $start_time = date('Y-m-d 00:00', strtotime($date));
                            $start_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                        }else{
                            $start_time = date('Y-m-d H:i', strtotime($schedule->start_date));
                            $start_time1 = date('Y-m-d '.$first_hour.':00', strtotime($schedule->start_date));
                        }
                        $start = (strtotime($start_time) - strtotime($start_time1)) / (10 * 60);
                        if(strtotime(date('Y-m-d',strtotime($schedule->end_date))) > strtotime($date)){
                            $end_time = date('Y-m-d 24:00', strtotime($date));
                            $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                        }else{
                            if(strtotime(date('Y-m-d',strtotime($schedule->start_date))) < strtotime($date)){
                                $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($date));
                            }else{
                                $end_time1 = date('Y-m-d '.$first_hour.':00', strtotime($schedule->start_date));
                            }
                            $end_time = date('Y-m-d H:i', strtotime($schedule->end_date));
                        }
                        $end = (strtotime($end_time) - strtotime($end_time1)) / (10 * 60);
                        $object = new \stdClass();
                        $object->start = $start;
                        $object->end = $end;
                        $object->status = 1;
                        $object->count = $end - $start;
                        $object->schedule = $schedule;
                        $arr[] = $object;
                    }
                $data = $this->scheduleRepo->getAboutTime($arr,$total_minute)['data'];
                $duplicate = $this->scheduleRepo->getAboutTime($arr,$total_minute)['duplicate'];
                $line_time = '';
                foreach ($data as $key => $value){
                        if ($value->status == 0) {
                            $line_time .= '<td colspan="' . $value->count . '" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>';
                        } else {
                            $menu = is_null($value->schedule->menu) ? '' : '<span class="event_color' . explode(';#', $value->schedule->menu)[1] . '_grn">' . explode(';#', $value->schedule->menu)[0] . '</span>';
                            if($value->schedule->pattern == 3){
                                if($value->schedule->type_repeat == 'week' || $value->schedule->type_repeat == 'weekday'){
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">';
                                }elseif($value->schedule->type_repeat == 'day'){
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">';
                                }else{
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">';
                                }
                             }else{
                                $repeat = '';
                             }
                             $title_facility = count($value->schedule->equipment) > 0 ? '[' . implode(',', $value->schedule->equipment()->pluck('name')->toArray()) . ']' : '';
                             if(in_array(\Auth::guard('member')->user()->id,$value->schedule->member->pluck('id')->toArray()) || $value->schedule->private == 0){
                                 $title = '<a href="' . route('frontend.schedule.view', $value->schedule->id) . '" >' . $menu . ' ' . $value->schedule->title . ' ' . $title_facility . ' '.$repeat.'</a>';
                             }else{
                                 $title = '<a>Đã có lịch</a>';
                             }
                            $line_time .= '<td colspan="' . $value->count . '" class="ddtd ddtd_middle group_day_calendar_item group_day_calendar_color_booked normalEvent ">
                                            <div class="normalEventElement" data-event_id="' . $value->schedule->event . '" data-event_data="1" data-event_start_date="2021-01-20 08:00:00" data-event_end_date="2021-01-20 09:00:00"  data-event_no_endtime="">
                                                '.$title.'
                                            </div>
                                          </td>';
                        }
                    }
                    if (count($duplicate) > 0) {
                        foreach ($duplicate as $key => $val) {
                            $dup_html = '';
                            if($val->schedule->pattern == 3){
                                if($val->schedule->type_repeat == 'week' || $val->schedule->type_repeat == 'weekday'){
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pweek20.gif" border="0" alt="Week" title="Week" class="small_link">';
                                }elseif($val->schedule->type_repeat == 'day'){
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pday20.gif" border="0" alt="Day" title="Day" class="small_link">';
                                }else{
                                    $repeat = '<img src="/img/repeat16.gif" border="0" alt=""><img src="/img/cal_pmon20.gif" border="0" alt="Month" title="Month" class="small_link">';
                                }
                             }else{
                                $repeat = '';
                             }
                            $menu = is_null($val->schedule->menu) ? '' : '<span class="event_color' . explode(';#', $val->schedule->menu)[1] . '_grn">' . explode(';#', $val->schedule->menu)[0] . '</span>';

                            $title_facility = '';
                            if(in_array(\Auth::guard('member')->user()->id,$val->schedule->member->pluck('id')->toArray()) || $val->schedule->private == 0){
                                 $title = '<a href="' . route('frontend.schedule.view', $val->schedule->id) . '">
                                                <span class="attention"><img src="/img/attention16.gif" border="0" title="Conflicting appointments" alt="Conflicting appointments" align="absmiddle"></span>' . $menu . ' ' . $val->schedule->title . ' ' . $title_facility . ' '.$repeat.'
                                           </a>';
                             }else{
                                 $title = '<a><span class="attention"><img src="/img/attention16.gif" border="0" title="Conflicting appointments" alt="Conflicting appointments" align="absmiddle"></span>Đã có lịch</a>';
                             }
                            if ($val->start > 0) {
                                if ($val->end >= $total_minute) {
                                    $dup_html .= '<td colspan="' . $val->start . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>
                                                    <td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . ($total_minute - $val->start) . '">
                                                        <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                            '.$title.'
                                                        </div>
                                                    </td>';
                                } else {
                                    $dup_html .= '<td colspan="' . $val->start . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>
                                                  <td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->count . '">
                                                        <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                             '.$title.'
                                                        </div>
                                                  </td>
                                                  <td colspan="' . ($total_minute - $val->end) . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>';
                                }
                            } else {
                                if ($val->end >= $total_minute) {
                                    $dup_html .= '<td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->end . '">
                                                    <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                            '.$title.'
                                                    </div>
                                                  </td>';
                                } else {
                                    $dup_html .= '<td class="ddtd normalEvent group_day_calendar_item_conflicted group_day_calendar_color_booked" colspan="' . $val->count . '">
                                                        <div class="normalEventElement" data-event_id="' . $val->schedule->id . '" data-event_type="share" data-event_no_endtime="" data-event_data="" data-event_start_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->start_date)) . '" data-event_end_date="' . date('Y-m-d H:i:s', strtotime($val->schedule->end_date)) . '" >
                                                             '.$title.'
                                                        </div>
                                                  </td>
                                                  <td colspan="' . ($total_minute - $val->end) . '" class="group_day_calendar_item_conflicted group_day_calendar_color_conflicted_line" "="">&nbsp;</td>';
                                }
                            }
                            $line_time .= '<tr>
                            <td class="group_day_calendar_conflict_head1"><br></td>
                            <td class="group_day_calendar_conflict_head2"><br></td>
                            ' . $dup_html . '
                        </tr>';
                        }
                    }
                    $img = $member->id == \Auth::guard('member')->user()->id ? '<img src="/img/loginuser20.gif" border="0" alt="">' : '<img src="/img/user20.gif" border="0" alt="">';
                    $html .= '<tr>
                                <td class="group_day_calendar_user">
                                       <div class="userElement"><span class="span_user"><span class="user-grn inline_block_grn">' . $img . ' ' . htmlentities($member->full_name) . '</span></span></div>
                                       &nbsp;<br>
                                    </td>
                                <td class="normalEvent group_day_calendar_event_cell">
                                   ' . $todo_html . '
                                </td>
                                ' . $line_time . '
                             </tr>';
            } else {
                $img = $member->id == \Auth::guard('member')->user()->id ? '<img src="/img/loginuser20.gif" border="0" alt="">' : '<img src="/img/user20.gif" border="0" alt="">';
                $html .= '<tr>
                        <td class="group_day_calendar_user">
                           <div class="userElement"><span class="span_user"><span class="user-grn inline_block_grn">' . $img . ' ' . htmlentities($member->full_name) . '</span></span></div>
                           &nbsp;<br>
                        </td>
                        <td class="normalEvent group_day_calendar_event_cell">
                           &nbsp;
                        </td>
                        <td colspan="'.$total_minute.'" class="group_day_calendar_item group_day_calendar_color_available"><br>&nbsp;</td>
                     </tr>';
            }
            //end 1 member
            $schedules1 = $this->scheduleRepo->getByMemberAllDay([$member_id], $date);
            foreach ($schedules1 as $key => $val) {
                $html .= '<tr>
                        <td class="group_day_calendar_banner_head1"><br></td>
                        <td class="group_day_calendar_banner_head2"><br></td>
                        <td class="group_day_calendar_banner_item banner_color" colspan="'.$total_minute.'"><a href="' . route('frontend.schedule.view', $val->id) . '" ><img src="/img/banner16.gif" border="0" alt="">' . $val->title . '</a></td>
                    </tr>';
            }
        }
        if (isset($input['start_hour'])) {
            $start_hour_html = \App\Helpers\StringHelper::getSelectHour($input['start_hour']);
        } else {
            $start_hour_html = \App\Helpers\StringHelper::getSelectHour();
        }
        if (isset($input['start_minute'])) {
            $start_minute_html = \App\Helpers\StringHelper::getSelectMinute($input['start_minute']);
        } else {
            $start_minute_html = \App\Helpers\StringHelper::getSelectMinute();
        }
        if (isset($input['end_hour'])) {
            $end_hour_html = \App\Helpers\StringHelper::getSelectHour($input['end_hour']);
        } else {
            $end_hour_html = \App\Helpers\StringHelper::getSelectHour();
        }
        if (isset($input['end_minute'])) {
            $end_minute_html = \App\Helpers\StringHelper::getSelectMinute($input['end_minute']);
        } else {
            $end_minute_html = \App\Helpers\StringHelper::getSelectMinute();
        }
        return view('frontend/schedule/schedule_confirm', compact('html', 'start_hour_html', 'start_minute_html', 'end_hour_html', 'end_minute_html', 'date', 'member_arr'));
    }

    public function leave($id) {
        $schedule = $this->scheduleRepo->find($id);
        $array = $schedule->member()->pluck('id')->toArray();
        $input['member_id'] = array_diff($array, [\Auth::guard('member')->user()->id]);
        $schedule->member()->sync($input['member_id']);
        $this->memberRepo->NotificateMembers($input['member_id'], ['content' => 'Đã không tham gia lịch trình', 'link' => '/schedule/view/' . $schedule->id]);
        $this->notificationSchRepo->createNotification('Đã không tham gia lịch trình', \Auth::guard('member')->user()->id, $input['member_id'], route('frontend.schedule.view', $schedule->id));
        if (config('global.device') != 'pc') {
            return view('mobile/schedule/view', compact('schedule'));
        } else {
            return view('frontend/schedule/view', compact('schedule'));
        }
    }

    public function participate($id) {
        $schedule = $this->scheduleRepo->find($id);
        $array = $schedule->member()->pluck('id')->toArray();
        if ((count($array) > 0)) {
            foreach ($array as $key => $val) {
                $input['member_id'][] = $val;
            }
        }
        $input['member_id'][] = \Auth::guard('member')->user()->id;
        $schedule->member()->sync($input['member_id']);
        if (config('global.device') != 'pc') {
            return view('mobile/schedule/view', compact('schedule'));
        } else {
            return view('frontend/schedule/view', compact('schedule'));
        }
    }

}
