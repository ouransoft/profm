<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Cors {
    public function handle($request, Closure $next)
    {
       return $next($request)
           ->header('Access-Control-Allow-Origin', 'http://admin.local')
           ->header('Access-Control-Allow-Methods', 'POST')
           ->header('Access-Control-Allow-Credentials', 'true')
           ->header('Access-Control-Allow-Headers', 'X-CSRF-Token');
    }
    
}
