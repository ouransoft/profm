<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class Frontend {
    public function handle($request, Closure $next){
        $config = \DB::table('config')->first();
        \View::share(['share_config' => $config]);
        $rank_member = DB::table('member')->join('project', 'project.member_id', '=', 'member.id')
                       ->select('member.id',DB::raw('count(project.id) as count'))->groupBy('member.id')->orderBy('count','DESC')->take(3)->get();
        $a = Carbon::now();
        $lastQuarter = $a->quarter;
        $project_quarter = DB::table('project')
                   ->where(DB::raw('QUARTER(created_at)'), $lastQuarter)->where('status', \App\Project::STATUS_ACTIVE)->where('is_deleted',0);;
        $team_month = DB::table('project')
                   ->whereMonth('created_at',date('m'))->where('status', \App\Project::STATUS_ACTIVE)->where('is_deleted',0);
        $rank_quarter = DB::table('member')
            ->joinSub($project_quarter, 'project_quarter', function ($join) {
                $join->on('member.id', '=', 'project_quarter.member_id');
            })->select('member.id','member.login_id','member.full_name',DB::raw('count(project_quarter.id) as count'))->groupBy('member.id','member.login_id','member.full_name')->orderBy('count','DESC')->take(3)->get();
        $rank_team = DB::table('member')->joinSub($team_month, 'team_month', function ($join) {
                        $join->on('member.id', '=', 'team_month.member_id');
                    })->join('department','member.department_id','=','department.id')
                    ->where('department.level',\App\Department::max('level'))
                    ->select('department.name',DB::raw('count(team_month.id) as count'),'department.id')
                    ->groupBy('department.name','department.id')
                    ->orderBy('count','DESC')->take(3)->get();
        \View::share(['rank_quarter' => $rank_quarter]);
        \View::share(['rank_team' => $rank_team]);
        if(Auth::guard('member')->user() && session()->getId() != Auth::guard('member')->user()->session_id){
            Auth::guard('member')->logout();
            return redirect()->route('home.index');
         }
        if (!is_null(Auth::guard('member')->user())){
            $members= DB::table('member')->join('project', 'project.member_id', '=', 'member.id')
                ->select('member.id', DB::raw('count(project.id) as count'))
                ->groupBy('member.id')->orderBy('count','DESC')->get();
            foreach($members as $key=>$val){
                if(\Auth::guard('member')->user()->id == $val->id){
                    $position = $key + 1;
                }
            }
            $members_team = DB::table('member')->join('project', 'project.member_id', '=', 'member.id')
                    ->join('department','member.department_id','=','department.id')
                    ->where('department.level',\App\Department::max('level'))
                    ->select('member.id','member.department_id',DB::raw('count(project.id) as count'))->where('member.department_id',\Auth::guard('member')->user()->department_id)
                    ->groupBy('member.id','member.department_id')->orderBy('count','DESC')->get();
            foreach($members_team as $k=>$value){
                if(\Auth::guard('member')->user()->id == $value->id){
                    $position_team = $k + 1;
                }
            }
            \View::share(['position_rank' => isset($position) ? $position : 0]);
            \View::share(['position_team_rank' => isset($position_team) ? $position_team : 0]);
            $count_message = \DB::table('read_message')->where('seen', '=', 0)->where('member_id',\Auth::guard('member')->user()->id)->groupBy('group_id')->count();
            \View::share(['count_message' => $count_message]);
            return $next($request);
        } else {
            return redirect()->route('home.index');
        }
    }

}
