<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDo extends Model
{
    protected $table='todo';
    protected $fillable=['title','start_date','end_date','status','note','priority','created_by','complete_date','purpose','content','address','confirm'];
    const STATUS_COMPLETE = 3;
    const STATUS_UNCOMPLETE = 0;
    
    const CONFIRM_PENDDING = 0;
    const CONFIRM_COMPLETE = 1;
    const CONFIRM_CANCEL = 2;
    
    const STATUS_ARR = [['id'=>1,'name'=>'Chờ xác nhận'],['id'=>2,'name'=>'Đang triển khai'],['id'=>3,'name'=>'Hoàn thành'],['id'=>4,'name'=>'Không tham gia']];
    const PROGRESS_ARR = ['25','50','75','100'];
    const PRIORITY_ARR = [['id'=>1,'name'=>'Khẩn cấp'],['id'=>2,'name'=>'Cao'],['id'=>3,'name'=>'Bình thường'],['id'=>4,'name'=>'Thấp']];
    public function createdby(){
        return $this->hasMany('\App\Member','created_by');
    }
    public function nameStatus(){
        foreach($this::STATUS_ARR as $key=>$val){
            if($val['id'] == $this->status){
                return $val['name'];
            }
        }
    }
    public function start_date(){
        return date('d/m/Y h:i A',strtotime($this->start_date));
    }
    public function end_date(){
        return date('d/m/Y h:i A',strtotime($this->end_date));
    }
    public function complete_date(){
        return date('d/m/Y h:i A',strtotime($this->end_date));
    }
    public function namePriority(){
         foreach($this::PRIORITY_ARR as $key=>$val){
            if($val['id'] == $this->priority){
                return $val['name'];
            }
        }
    }
    public function created_at(){
        return date('d/m/Y',strtotime($this->created_at));
    }
    public function category(){
        return $this->belongsTo('App\ToDoCategory','category_id');
    }
    public function member(){
         return $this->belongsToMany('\App\Member', 'member_todo', 'todo_id', 'member_id')->withPivot('status', 'reason', 'join','progress');;
    }
     public function creator(){
         return $this->belongsTo('\App\Member','created_by');
    }
    public function progress(){
         return $this->hasMany('\App\MemberTodo','todo_id')->where('join',1)->avg('progress');
    }
    public function membertodo(){
        return $this->hasMany('\App\MemberTodo','todo_id');
    }
    public function files(){
        return $this->hasMany('\App\File','relationship_id')->where('type',2)->where('format',2);
    }
    public function comment(){
        return $this->hasMany('\App\Comment','todo_id');
    }
}
