<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationSchedule extends Model
{
    protected $table = 'notification_schedule';
    protected $fillable = [
        'content', 'from','to','link','read_at'
    ];
    public function member(){
        return $this->belongsTo('\App\Member','from','id');
    }
    public function created_at(){
        return date('d/m/Y',strtotime($this->created_at));
    }
}
