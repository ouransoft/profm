<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberProject extends Model
{
    protected $table = 'member_project';
    protected $fillable = ['member_id','project_id', 'todo_id','after_content','status'];
    const STATUS_START = 0;
    const STATUS_SEND = 1;
    const STATUS_RETURN = 2;
    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }
    public function member(){
        return $this->belongsTo('\App\Member');
    }
    public function todo(){
        return $this->belongsTo('App\Todo');
    }
    public function images(){
        return $this->hasMany('\App\File','relationship_id')->where('type',\App\File::TYPE_IMAGE_PROJECT_REPORT)->where('format',1);
    }
}
