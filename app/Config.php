<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model {
    protected $table = 'config';
    protected $fillable = [
        'title', 'company_name','content', 'address', 'phone', 'email','font_size'
    ];
    public $timestamps = false;
    public function logo(){
        return $this->hasMany('\App\File','relationship_id')->where('type',9)->where('format',1);
    }
    public function favicon(){
        return $this->hasMany('\App\File','relationship_id')->where('type',10)->where('format',1);
    }
}
