<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMessage extends Model
{
    protected $table = 'group_message';
    protected $fillable = [
        'from','group_id','message','reply_id'
    ];
    public function member() {
        return $this->belongsToMany('App\Member', 'group_member', 'group_id', 'member_id');
    }
    public function membersend(){
        return $this->belongsTo('App\Member', 'from', 'id');
    }
}
