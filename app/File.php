<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'file';
    protected $fillable = [
        'name','size','link','relationship_id','type','format'
    ];
    const TYPE_SCHEDULE = 1;
    const TYPE_TODO = 2;
    const TYPE_FILE_PROJECT_BEFORE = 3;
    const TYPE_FILE_PROJECT_AFTER = 4;
    const TYPE_IMAGE_PROJECT_BEFORE = 5;
    const TYPE_IMAGE_PROJECT_AFTER = 6;
    const TYPE_IMAGE_PROJECT_REPORT = 7;
    const TYPE_MEMBER = 8;
    const TYPE_LOGO = 9;
    const TYPE_FAVICON = 10;
    const TYPE_SLIDE = 11;
    const FORMAT_IMAGE = 1;
    const FORMAT_FILE =2;
}