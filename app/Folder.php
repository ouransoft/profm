<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    public $timestamps = false;
    protected $table='folder';
    protected $fillable=['name','ordering','parent_id','path','created_by'];
    public function member() {
        return $this->belongsToMany('\App\Member', 'member_folder', 'member_id', 'folder_id');
    }
}
