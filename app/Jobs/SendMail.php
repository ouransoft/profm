<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title,$content,$id,$email,$full_name)
    {
        $this->title = $title;
        $this->content = $content;
        $this->id = $id;
        $this->email = $email;
        $this->full_name = $full_name;
    }
    
    public function handle()
    {
        Mail::send('mail', array('title'=>$this->title,'content'=>$this->content,'link'=>(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/todo/view/'.$this->id), function($message) use ($email,$name){
                            $message->from('ouransoft@gmail.com', 'Ouransoft');
                            $message->to($email, 'Ouransoft')->subject('Thông báo giao việc từ '.$name);
                        });
        \Artisan::call('queue:work');
    }
}
