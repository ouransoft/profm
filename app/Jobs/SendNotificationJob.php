<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $title;
    protected $memo;
    protected $start_date;
    protected $end_date;
    protected $emails;
    protected $schedule_id;
    public function __construct($title,$memo,$start_date,$end_date,$emails,$schedule_id)
    {
        $this->title = $title;
        $this->memo = $memo;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->emails = $emails;
        $this->schedule_id = $schedule_id;
    }
    public function handle()
    {
        $event_id = \App\Helpers\Calendar::insert($this->title,'', $this->memo, $this->start_date, $this->end_date, $this->emails);
        \App\Schedule::find($this->schedule_id)->update(['event_id' => $event_id]);
        \Artisan::call('queue:work');
    }
}
