<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    //
    protected $table = "comment";
    protected $fillable = [
        'member_id', 'comment','todo_id'
    ]; 
    public function created_at(){
        return date('d/m/Y',strtotime($this->created_at));
    }
    public function Member(){
        return $this->belongsTo('\App\Member');
    }
}
