<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReadMessage extends Model
{
    protected $table = 'read_message';
    protected $fillable = [
        'group_id','member_id'
    ];
}
