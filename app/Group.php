<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'group';
    protected $fillable = [
        'name'
    ];
    public function member() {
        return $this->belongsToMany('App\Member', 'group_member', 'group_id', 'member_id')->withPivot('name');
    }
    public function readMessage(){
        return $this->belongsToMany(Member::class, 'read_message', 'group_id', 'member_id')->withPivot('message_id','seen');
    }
}
