<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table='schedule';
    protected $fillable=['start_date','end_date','title','memo','uid','pattern','event_id','event','bdate','update_person','private','public_dashboard','menu','type_repeat','company_name','physical_address','company_telephone_number','percent','wday','start_repeat','end_repeat','none_time'];
    const Weekend_arr = ['0'=>'Sunday','1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday'];
    const PRIVATE_PUBLIC = 0;
    const PRIVATE_SECRET = 1;
    
    const selected_user_sUID = "người tham gia";
    const selected_user_p_sUID= "người được chia sẻ";
    const selected_users_sITEM= "trang thiết bị";

    public function member() {
        return $this->belongsToMany('\App\Member', 'member_schedule', 'schedule_id', 'member_id');
    }
    public function seen() {
        return $this->belongsToMany('\App\Member', 'seen_schedule', 'schedule_id', 'member_id');
    }
    public function equipment() {
        return $this->belongsToMany('\App\Equipment', 'equipment_schedule', 'schedule_id', 'equipment_id');
    }
    public function created_by(){
        return $this->belongsTo('\App\Member','uid','id');
    }
    public function updater(){
        return $this->belongsTo('\App\Member','update_person','id');
    }
    public function files(){
        return $this->hasMany('\App\File','relationship_id')->where('type',1)->where('format',2);
    }
    public function checkConflict($member_id){
        $end_date = $this->end_date;
        $start_date = $this->start_date;
        $scheduele_ids = \App\MemberSchedule::where('member_id',$member_id)->pluck('schedule_id')->toArray();
        $check = $this::where('id','<>',$this->id)->whereIn('id',$scheduele_ids)->whereIn('pattern',['1','3'])->where('none_time',0)->where('start_date','<',$end_date)->where('end_date', '>', $start_date)->get();
        if(count($check) > 0 && $this->none_time == 0){
            return true;
        }else{
            return false;
        }
    }
}
