<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table='department';
    protected $fillable=['parent_id','name','level','company_id'];
    public function children(){
        return $this->hasMany(self::class,'parent_id','id');
    }
    public function parents(){
        return $this->belongsTo(self::class,'id', 'parent_id');
    }
    public function member(){
        return $this->hasMany('\App\Member');
    }
    public function getIds()
    {
        $ids =  [$this->id];
        foreach ($this->children as $child) {
            $ids = array_merge($ids, $child->getIds());
        }
        return $ids;
    }
    public function getChildrenIds()
    {
        $ids =  [];
        foreach ($this->children as $child) {
            $ids = array_merge($ids, $child->getIds());
        }
        return $ids;
    }
}