<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'slide';
    protected $fillable = ['title','image','status','ordering','created_at'];
    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }
    public function updated_at() {
        return date("d/m/Y", strtotime($this->updated_at));
    }
    public function image(){
        return $this->hasMany('\App\File','relationship_id')->where('type',11)->where('format',1);
    }
}
