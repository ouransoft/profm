<?php
namespace App\Helpers;
use Google_Client;
use Carbon\Carbon;
Class Calendar{
    public static function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes(\Google_Service_Calendar::CALENDAR);
        $client->setAuthConfig(__DIR__ .'/credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $tokenPath = 'token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }
        if ($client->isAccessTokenExpired()) {
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(fopen("php://stdin","r")));
                $accessToken = $client->fetchAccessTokenWithAuthCode('4/0AY0e-g7O4NpNBfg2upeT2yf8a61y8XuVVtHhHkem7fga9uu3L2RP4RBLPegfSg6dhaLZ6Q');
                $client->setAccessToken($accessToken);
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
        
        }
        return $client;
    }
    private static function convertTime($time,$timeZone,$min = 0)
    {
        return Carbon::parse($time, $timeZone)->addMinutes($min)->toIso8601String();
    }
     public static function getAttendees($emails)
    {
        $attendees = [];
        foreach ($emails as $email) {
            if ($email) {
                $attendees[] = [
                    'email' => $email
                ];
            }
        }
        return $attendees;
    }
    public static function insert($title ='',$location='',$description ='',$start,$end,$emails=[]){
            $client = Calendar::getClient();
            $service = new \Google_Service_Calendar($client);
            $calendarId = 'primary';
            $optParams = array(
              'maxResults' => 10,
              'orderBy' => 'startTime',
              'singleEvents' => true,
              'timeMin' => date('c'),
            );
            $results = $service->events->listEvents($calendarId, $optParams);
            $events = $results->getItems();
            $event = new \Google_Service_Calendar_Event(array(
              'summary' => $title,
              'location' => $location,
              'description' => $description,
              'start' => ['dateTime'=>Calendar::convertTime($start, 'Asia/Ho_Chi_Minh'),'timeZone'=>'Asia/Ho_Chi_Minh'],
              'end' => ['dateTime'=>Calendar::convertTime($end, 'Asia/Ho_Chi_Minh'),'timeZone'=>'Asia/Ho_Chi_Minh'],
              'recurrence' => array(
                'RRULE:FREQ=DAILY;COUNT=1'
              ),
              'attendees' => Calendar::getAttendees($emails),
              'reminders' => array(
                'useDefault' => FALSE,
                'overrides' => array(
                  array('method' => 'email', 'minutes' => 24 * 60),
                  array('method' => 'popup', 'minutes' => 10),
                ),
              ),
            ));
            $calendarId = 'primary';
            $event = $service->events->insert($calendarId, $event);
            return $event->getId();
    }
    public static function update($title ='',$location='',$description ='',$start,$end,$emails=[],$event_id){
        $client = Calendar::getClient();
        $service = new \Google_Service_Calendar($client);
        $event = $service->events->get('primary',$event_id);
        $event->setSummary($title);
        $event->setlocation($location);
        $formattedStart = Calendar::convertTime($start, 'Asia/Ho_Chi_Minh');
        $formattedEnd = Calendar::convertTime($end, 'Asia/Ho_Chi_Minh');
        $start = new \Google_Service_Calendar_EventDateTime();
        $start->setTimeZone('Asia/Ho_Chi_Minh');
        $start->setDateTime($formattedStart);
        $end = new \Google_Service_Calendar_EventDateTime();
        $end->setTimeZone('Asia/Ho_Chi_Minh');
        $end->setDateTime($formattedEnd);
        $event->setStart($start);
        $event->setEnd($end);
        $event->setAttendees(Calendar::getAttendees($emails));
        $updatedEvent = $service->events->update('primary', $event_id, $event);
    }
}
?>

