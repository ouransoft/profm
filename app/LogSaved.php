<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSaved extends Model
{
    protected $table = 'log_saved';
    protected $fillable = ['member_id','project_id'];
    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }
    public function member(){
        return $this->belongsTo('\App\Member');
    }
}
