<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Collection;

class DepartmentRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Department';
    }
    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:department'
        ];
    }
    public function validateUpdate($id){
        return $rules = [
            'name' => 'required|unique:department,name,' . $id . ',id'
        ];
    }
    public function getAll(){
        return $this->model->get();
    }
    public function checkName($name){
        return $this->model->where('name',$name)->count();
    }
    public function getAllStack($parent_id){
        $sections = new Collection();
        $parents = $this->model->where('parent_id',$parent_id)->get();
        foreach($parents as $key=>$parent){
            $sections->push($parent);
            $sections = $sections->merge($this->getAllStack($parent->id));
        }
        return $sections;
    }
}
