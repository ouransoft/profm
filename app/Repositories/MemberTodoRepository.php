<?php

namespace App\Repositories;

use Repositories\Support\AbstractRepository;
use Carbon\Carbon;
use DB;

class MemberTodoRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model(){
        return 'App\MemberTodo';
    }
    public function getRecord($member_id,$todo_id){
        return $this->model->where('member_id',$member_id)->where('todo_id',$todo_id)->first();
    }
    public function updateRecord($member_id,$todo_id,$data){
        return $this->model->where('member_id',$member_id)->where('todo_id',$todo_id)->update($data);
    }
    public function getStatus($todo_id){
        return $this->model->where('todo_id',$todo_id)->where('join',1)->orderBy('status','ASC')->first();
    }
    public function countTodo($member_ids,$status,$pattern,$join,$time,$groupBy){
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');  
        $lastQuarter = $now->quarter;
        $query = $this->model->leftjoin('todo','member_todo.todo_id','=','todo.id')->whereIn('member_id',$member_ids);
        if($status !== ''){
            if($status == 3){
                $query = $query->where('todo.status',3); 
                if($pattern !== ''){
                    switch ($pattern){
                        case 1:
                            $query = $query->whereColumn('todo.complete_date','<','todo.end_date'); 
                            break;
                        case 2:
                            $query = $query->whereColumn('todo.complete_date','=','todo.end_date'); 
                            break;
                        case 3:
                            $query = $query->whereColumn('todo.complete_date','>','todo.end_date'); 
                            break;
                    }
                }
            }else{
               $query = $query->where('todo.status','<',3);  
            }
        }
        if($join == 1){
            $query = $query->where('join','<>',2); 
        }elseif($join == 2){
            $query = $query->where('join',2); 
        }
        switch ($time){
                case 'week':
                    $query = $query->whereDate('todo.start_date','<=',$weekEndDate)->whereDate('todo.end_date','>=',$weekStartDate);
                            break;
                case 'month':
                    $query = $query->where(function($query) {
                                $query->where(function($query1) {
                                        $query1->whereMonth('start_date', date('m'))
                                              ->whereYear('start_date',date('Y'));
                                    })->orWhere(function($query2) {
                                        $query2->whereMonth('end_date', date('m'))
                                              ->whereYear('end_date',date('Y'));
                                    });
                            });
                            break;
                case 'quater':
                    $query = $query->where(function($query) use ($lastQuarter) {
                                    $query->where(function($query) use ($lastQuarter) {
                                        $query->where(DB::raw('QUARTER(start_date)'), $lastQuarter);
                                    })->orWhere(function($query) use ($lastQuarter){
                                        $query->where(DB::raw('QUARTER(end_date)'), $lastQuarter);
                                    });
                               });
                               break;
                case 'year':
                    $query = $query->where(function($query) {
                                    $query->where(function($query) {
                                        $query->whereYear('start_date',date('Y'));
                                    })->orWhere(function($query){
                                        $query->whereYear('end_date',date('Y')); 
                                    });
                               });
                               break;
        }
        if($groupBy == TRUE){
            $data = count($query->groupBy('member_todo.todo_id')->get()); 
        }else{
            $data = count($query->get());  
        }
        return $data;
    }
}
