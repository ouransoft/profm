<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class EquipmentRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model(){
        return 'App\Equipment';
    }
    public function search($keyword){
        return $this->model->where('name','like','%'. $keyword .'%')->get();
    }
    public function searchEquipApp($keyword, $equip_ids){
        return $this->model->select('id','name')->where('name','like','%'.$keyword.'%')->whereNotIn('id',$equip_ids)->get();
    }
    public function checkName($name){
        return $this->model->where('name',$name)->count();
    }
}
