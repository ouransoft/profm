<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class ScheduleRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Schedule';
    }
    public function getAllByMember($member_id,$date){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->whereIn('member_schedule.member_id',$member_id)
                ->whereDate('schedule.start_date','<=',$date)->whereDate('schedule.end_date','>=',$date)->orderBy('schedule.start_date','ASC')->orderBy('schedule.end_date','DESC')->get();
    }
    public function getAllByMemberDay($member_id,$date){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->whereIn('member_schedule.member_id',$member_id)
                ->whereDate('schedule.start_date','<=',$date)->whereDate('schedule.end_date','>=',$date)->whereNotIn('schedule.pattern',['2','4'])->orderBy('schedule.start_date','ASC')->orderBy('schedule.end_date','DESC')->get();
    }
    public function getByMemberDayNoneTime($member_id,$date){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->whereIn('member_schedule.member_id',$member_id)
                ->whereDate('schedule.start_date','<=',$date)->whereDate('schedule.end_date','>=',$date)->whereNotIn('schedule.pattern',['2','4'])->where('none_time',1)->orderBy('schedule.start_date','ASC')->orderBy('schedule.end_date','DESC')->get();
    }
    public function getByMemberDay($member_id,$date){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->whereIn('member_schedule.member_id',$member_id)
                ->whereDate('schedule.start_date','<=',$date)->whereDate('schedule.end_date','>=',$date)->whereNotIn('schedule.pattern',['2','4'])->where('none_time',0)->orderBy('schedule.start_date','ASC')->orderBy('schedule.end_date','DESC')->get();
    }
    public function getByMemberFirstDay($member_id,$date){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->whereIn('member_schedule.member_id',$member_id)
                ->whereDate('schedule.start_date','<=',$date)->whereDate('schedule.end_date','>=',$date)->whereNotIn('schedule.pattern',['2','4'])->where('none_time',0)->orderBy('schedule.start_date','ASC')->first();
    }
    public function getByMemberLastDay($member_id,$date){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->whereIn('member_schedule.member_id',$member_id)
                ->whereDate('schedule.start_date','<=',$date)->whereDate('schedule.end_date','>=',$date)->whereNotIn('schedule.pattern',['2','4'])->where('none_time',0)->orderBy('schedule.end_date','DESC')->first();
    }
    public function getByMemberAllDay($member_id,$date){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->whereIn('member_schedule.member_id',$member_id)
                ->whereDate('schedule.start_date','<=',$date)->whereDate('schedule.end_date','>=',$date)->whereIn('schedule.pattern',['2','4'])->orderBy('schedule.start_date','ASC')->get();
    }
    public function getByMemberAllAboutDay($member_id,$start_date,$end_date){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->whereIn('member_schedule.member_id',$member_id)
                ->whereDate('schedule.end_date','>=',$start_date)->whereDate('schedule.start_date','<=',$end_date)->whereIn('schedule.pattern',['2','4'])->orderBy('schedule.start_date','ASC')->get();
    }
    public function getByEquipmentAllAboutTime($equipment_id,$start_time,$end_time){
        return $this->model->join('equipment_schedule','schedule.id','equipment_schedule.schedule_id')->select('schedule.*')->where('equipment_schedule.equipment_id',$equipment_id)
                ->where('schedule.end_date','>=',$start_time)->where('schedule.start_date','<=',$end_time)->whereNotIn('schedule.pattern',['2','4'])->orderBy('schedule.start_date','ASC')->get();
    }
    public function getByEquipmentDay($equipment_id,$date){
        return $this->model->join('equipment_schedule','schedule.id','equipment_schedule.schedule_id')->select('schedule.*')->whereIn('equipment_schedule.equipment_id',$equipment_id)
                ->whereDate('schedule.start_date','<=',$date)->whereDate('schedule.end_date','>=',$date)->whereNotIn('schedule.pattern',['2','4'])->orderBy('schedule.start_date','ASC')->get();
    }
    public function getRecently($member_id){
        return $this->model->join('member_schedule','schedule.id','member_schedule.schedule_id')->select('schedule.*')->where('member_schedule.member_id',$member_id)
                ->where('schedule.start_date','>',date('Y-m-d H:i:s'))->whereNotIn('schedule.pattern',['2','4'])->where('none_time',0)->orderBy('schedule.start_date','ASC')->first();
    }
    public function showCalendar($cndate,$location,$gid = ''){
        $start_date = $cndate;
        $end_date = date('Y-m-t',strtotime($start_date));
        $end = new \DateTime(date('Y-m-d',strtotime($end_date)));
        $start = new \DateTime(date('Y-m-d',strtotime($start_date)));
        $day = date('t',strtotime($start_date));
        $start_month = date("Y-m-d", strtotime("-3 months", strtotime($start_date)));
        $select_month = '';
        for($i=0;$i<15;$i++ ){
            $month = date("Y-m-d", strtotime("+".$i." months", strtotime($start_month)));
            if($month == $start_date){
                $select_month .='<option value="'.date('Y-m-d',strtotime($month)).'" selected>'.date('F Y',strtotime($month)).'</option>';
            }else{
                $select_month .='<option value="'.date('Y-m-d',strtotime($month)).'">'.date('F Y',strtotime($month)).'</option>';
            }
        }
        $list_weekend = '';
        for($i=0;$i< $day ;$i++ ){
            $date = date("l", strtotime("+".$i." days", strtotime($start_date)));
            if(date("w", strtotime("+".$i." days", strtotime($start_date))) == 0){
                $list_weekend .= '<td align="center" class="s_date_sunday">Su</td>';
            }elseif(date("w", strtotime("+".$i." days", strtotime($start_date))) == 6){
                $list_weekend .= '<td align="center" class="s_date_saturday">Sa</td>';
            }else{
                $list_weekend .='<td align="center">'.substr(date('l',strtotime($date)),0,1).'</td>';
            }
        }  
        $list_day = '';
        for($i=0;$i< $day ;$i++ ){
            $days = date("Y-m-d", strtotime("+".$i." days", strtotime($start_date)));
            $d = date("j", strtotime("+".$i." days", strtotime($start_date)));
            if($days == date('Y-m-d')){
                $today = 'calendar_navi_today';
            }else{
               $today = ''; 
            }
            $list_day .='<td align="center" class="date_cell date_cell_'.$days.' '.$today.'" data-date="'.$days.'">
                                <span class="nowrap-grn "><a  href="/'.$location.'?uid=&amp;gid='.$gid.'&amp;bdate='.$days.'&amp;event=&amp;event_date=&amp;search_text=&amp;p=">'.$d.'</a></span>
                            </td>';
        }  
        $month_prev = "'".date("Y-m-d", strtotime("-1 months", strtotime($start_date)))."'";
        $month_next = "'".date("Y-m-d", strtotime("+1 months", strtotime($start_date)))."'";
        $html ='<div>
                   <table class="calendar_navi" id="calendar1" >
                        <tr>
                            <td rowspan="2" align="center" >
                            <select name="date" onChange="i=this.selectedIndex;v=this.options[i].value;if(v)doMoveCalednar(v)">
                           '.$select_month.'
                            </select>
                            </td>
                            <td rowspan="2" align="center" nowrap>
                                <a href="javascript:doMoveCalednar('.$month_prev.')"> << '.date("F", strtotime("-1 months", strtotime($start_date))).'</a>&nbsp;
                            </td>
                            '.$list_weekend.'
                             <td rowspan="2" align="center" nowrap>
                                &nbsp;<a href="javascript:doMoveCalednar('.$month_next.')">'.date("F", strtotime("+1 months", strtotime($start_date))).'>></a>
                            </td>
                        </tr>
                        <tr>
                            '.$list_day.'
                        </tr>
                </table>
            </div>';
        return $html;
    }
    public function showCalendarMonth($cndate,$location,$gid = ''){
        $start_date = $cndate;
        $start_month = date("Y-01-01", strtotime(($start_date)));
        $start_year = date("Y-01-01", strtotime("-1 years", strtotime($start_date)));
        $select_year = '';
        for($i=0;$i<5;$i++ ){
            $year = date("Y-m-d", strtotime("+".$i." years", strtotime($start_year)));
            if(date('Y',strtotime($start_date)) == date('Y',strtotime($year))){
                $select_year .='<option value="'.$year.'" selected>'.date('Y',strtotime($year)).'</option>';
            }else{
                $select_year .='<option value="'.$year.'">'.date('Y',strtotime($year)).'</option>';
            }
        }
        $list_month = '';
        for($i=0;$i< 12 ;$i++ ){
            $month = date("Y-m-d", strtotime("+".$i." months", strtotime($start_month)));
            $list_month .='<td nowrap=""><a href="/schedule/personal_month?date='.$month.'">'.date('M',strtotime($month)).'</a></td>';
        }  
        $year_prev = "'".date("Y-m-d", strtotime("-1 years", strtotime($start_date)))."'";
        $year_next = "'".date("Y-m-d", strtotime("+1 years", strtotime($start_date)))."'";
        $html ='<div>
                   <table class="calendar_navi" id="calendar1" >
                        <tr>
                            <td rowspan="2" align="center" >
                                <select name="date" onChange="i=this.selectedIndex;v=this.options[i].value;if(v)doMoveCalednar(v)">
                               '.$select_year.'
                                </select>
                            </td>
                            <td rowspan="2" align="center" nowrap="">
                               <a href="javascript:doMoveCalednar('.$year_prev.');"><<  Previous year</a>
                            </td>
                            '.$list_month.'
                            <td rowspan="2" align="center" nowrap>
                                <a href="javascript:doMoveCalednar('.$year_next.')">Next year >></a>
                            </td>
                        </tr>
                </table>
            </div>';
        return $html;
    }
    public function deleteAll($event){
        $ids = $this->model->where('event',$event)->pluck('id')->toArray();
        \App\MemberSchedule::whereIn('schedule_id',$ids)->delete();
        \App\EquipmentSchedule::whereIn('schedule_id',$ids)->delete();
        return $this->model->where('event',$event)->delete();
    }
    public function deleteAfter($event,$bdate){
        $ids = $this->model->where('event',$event)->where('start_date','>=',$bdate)->pluck('id')->toArray();
        \App\MemberSchedule::whereIn('schedule_id',$ids)->delete();
        \App\EquipmentSchedule::whereIn('schedule_id',$ids)->delete();
        return $this->model->where('event',$event)->where('start_date','>=',$bdate)->delete();
    }
    public function updateBefore($event,$bdate){
        $schedule = $this->model->where('event',$event)->whereDate('start_date','<',$bdate)->orderBy('start_date','DESC')->first();
        if($schedule){
            $end_repeat = date('Y-m-d H:i:s',(strtotime ('-1 day',strtotime($bdate))));
            return $this->model->where('event',$event)->whereDate('start_date','<',$bdate)->update(['end_repeat'=>$end_repeat]);
        }
    }
    public function getAboutTime($arr,$total_minute){
        $arr = collect($arr);
        $arr = array_values($arr->sortBy('start')->toArray());
        $data = [];
        $duplicate = [];
        $check = 0;
        foreach ($arr as $key => $val) {
            if( count($arr) == 1){
                if ($val->start == 0) {
                    $data[] = $val;
                    if($val->end < $total_minute){
                        $object = new \stdClass();
                        $object->start = $val->end;
                        $object->end = $total_minute;
                        $object->status = 0;
                        $object->count = $total_minute - $val->end;
                        $data[]= $object;
                    }
                } else {
                    $object = new \stdClass();
                    $object->start = 0;
                    $object->end = $val->start;
                    $object->status = 0;
                    $object->count = $val->start;
                    $data[] = $object;
                    $data[] = $val;
                    $object = new \stdClass();
                    $object->start = $val->end;
                    $object->end = $total_minute;
                    $object->status = 0;
                    $object->count = $total_minute - $val->end;
                    $data[]= $object;
                }
            }else{
                if ($key == 0) {
                    if ($val->start == 0) {
                        $data[] = $val;
                    } else {
                        $object = new \stdClass();
                        $object->start = 0;
                        $object->end = $val->start;
                        $object->status = 0;
                        $object->count = $val->start;
                        $data[] = $object;
                        $data[] = $val;
                    }
                    $check = $val->end;
                } elseif ($key > 0 && $key < count($arr) - 1 && $check < $total_minute) {
                    if ($val->start == $check) {
                        $data[] = $val;
                        $check = $val->end;
                    } elseif ($val->start > $check) {
                        $object = new \stdClass();
                        $object->start = $check;
                        $object->end = $val->start;
                        $object->status = 0;
                        $object->count = $val->start - $check;
                        $data[] = $object;
                        $data[] = $val;
                        $check = $val->end;
                    } elseif ($val->start < $check) {
                        $duplicate[] = $val;
                        if ($val->end > $check) {
                            $object = new \stdClass();
                            $object->start = $check;
                            $object->end = $val->end;
                            $object->status = 1;
                            $object->schedule = $val->schedule;
                            $object->count = $val->end - $check;
                            $data[] = $object;
                            $check = $val->end;
                        }
                    }
                } elseif ($key == (count($arr) - 1) && $check < $total_minute) {
                    if ($val->start == $check) {
                        $val->start = $check;
                        if ($val->end >= $total_minute) {
                            $data[] = $val;
                        } else {
                            $data[] = $val;
                            $object = new \stdClass();
                            $object->start = $val->end;
                            $object->end = $total_minute;
                            $object->status = 0;
                            $object->count = $total_minute - $val->end;
                            $data[] = $object;
                        }
                    } elseif ($val->start > $check) {
                        if ($val->end >= $total_minute) {
                            $object = new \stdClass();
                            $object->start = $check;
                            $object->end = $val->start;
                            $object->status = 0;
                            $object->count = $val->start - $check;
                            $data[] = $object;
                            $val->end = $total_minute;
                            $val->count = $total_minute - $val->start;
                            $data[] = $val;
                        } else {
                            $object = new \stdClass();
                            $object->start = $check;
                            $object->end = $val->start;
                            $object->status = 0;
                            $object->count = $val->start - $check;
                            $data[] = $object;
                            $data[] = $val;
                    
                            $object1 = new \stdClass();
                            $object1->start = $val->end;
                            $object1->end = $total_minute;
                            $object1->status = 0;
                            $object1->count = $total_minute - $val->end;
                            $data[] = $object1;
                        }
                    } elseif ($val->start < $check) {
                        if ($val->end >= $total_minute) {
                            $duplicate[] = $val;
                            $object1 = new \stdClass();
                            $object1->start = $check;
                            $object1->end = $total_minute;
                            $object1->status = 1;
                            $object1->count = $total_minute - $val->start;
                            $object1->schedule = $val->schedule;
                            $data[] = $object1;
                        } else {
                            if ($val->end > $check) {
                                $duplicate[] = $val;
                                $object1 = new \stdClass();
                                $object1->start = $check;
                                $object1->end = $val->end;
                                $object1->status = 1;
                                $object1->count = $val->end - $check;
                                $object1->schedule = $val->schedule;
                                $data[] = $object1;
                                $object = new \stdClass();
                                $object->start = $val->end;
                                $object->end = $total_minute;
                                $object->status = 0;
                                $object->count = $total_minute - $val->end;
                                $data[] = $object;
                            } else {
                                $duplicate[] = $val;
                                $object = new \stdClass();
                                $object->start = $check;
                                $object->end = $total_minute;
                                $object->status = 0;
                                $object->count = $total_minute - $check;
                                $data[] = $object;
                            }
                        }
                    }
                }elseif($check == $total_minute){
                     $duplicate[] = $val;
                }
            }
        }
        return ['data'=>$data,'duplicate'=>$duplicate];
    }
}
