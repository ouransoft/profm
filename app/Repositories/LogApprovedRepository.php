<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class LogApprovedRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\LogApproved';
    }
    public function deleteProject($project_id){
        return $this->model->whereIn('project_id',$project_id)->delete();
    }
    public function deleteByProject($project_id){
        return $this->model->where('project_id',$project_id)->where('progress','>',2)->delete();
    }
    public function getByProgress($progress,$project_id){
        return $this->model->where('progress',$progress)->where('project_id',$project_id)->first();
    } 
    public function getListByProgress($progress,$project_id){
        return $this->model->where('progress',$progress)->where('project_id',$project_id)->get();
    } 
    public function deleteByProgress($progress,$project_id){
        return $this->model->where('progress',$progress)->where('project_id',$project_id)->delete();
    }
    public function updateByProgress($progress,$project_id,$input){
        return $this->model->where('progress',$progress)->where('project_id',$project_id)->update($input);
    }
    public function checkProgress($progress,$project_id){
       return $this->model->where('progress',$progress)->where('project_id',$project_id)->get(); 
    }
    public function checkProgressByMember($progress,$project_id,$member_id){
        return $this->model->where('progress',$progress)->where('project_id',$project_id)->where('member_id',$member_id)->get();
    }
}
