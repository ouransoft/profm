<?php
namespace Repositories;


use Repositories\Support\AbstractRepository;

class GroupRepository extends AbstractRepository
{
    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Group';
    }
    public function validateCreate(){
        return $rules = [
            'name' => 'unique:group',
        ];
    }
    public function checkName($name){
        return $this->model->where('name',$name)->count();
    }
}
