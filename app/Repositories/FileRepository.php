<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\File; 

class FileRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model(){
        return 'App\File';
    }
    public function updateByMember($id,$member_id){
        return $this->model->where('id',$id)->update(['relationship_id'=>$member_id]);
    }
    public function deleteByMember($member_id){
        $file = $this->model->where('relationship_id',$member_id)->where('type',\App\File::TYPE_MEMBER)->first();
        File::delete('./'.$file->link);
        return $file->delete();
    }
    public function resetRelationshipID($relationship_id,$type){
        return $this->model->where('relationship_id',$relationship_id)->whereIn('type',$type)->update(['relationship_id'=>NULL]);
    }
    public function deleteFileNull(){
        $files = $this->model->where('relationship_id',NULL)->get();
        foreach($files as $key=>$val){
            File::delete('./'.$val->link);
            $val->delete();
        }
    }
}
