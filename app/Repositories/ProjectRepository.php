<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class ProjectRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Project';
    }

    public function validateCreate() {
        return $rules = [
            'title' => 'required|unique:project',
            'alias' => 'required',
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'title' => 'required|unique:project,title,' . $id . ',id',
            'alias' => 'required',
        ];
    }
    public function getByMember($member_id){
        return $this->model->where('member_id',$member_id)->get();
    }
    public function getIndex($limit){
        $query = $this->model->where('is_deleted',0);
        $data = $query->where('member_id',\Auth::guard('member')->user()->id)->orderBy('created_at','DESC')->paginate($limit);
        return $data;   
    }
    public function getAll(){
        $data = $this->model->get();
        return $data;
        
    }
    public function getList($limit){
        $start = (Session::get('p_page')-1) * $limit;
        $query = $this->model->where('member_id',\Auth::guard('member')->user()->id);
        if((Session::get('keyword'))){
            $keyword = Session::get('keyword');
            if($keyword == 'save'){
                $query = $query->where('is_saved',1);
            }
            if($keyword == 'remove'){
                $query = $query->where('is_deleted',1);
            }else{
                $query = $query->where('is_deleted',0);
            }
            if($keyword == 'draft'){
                $query = $query->where('status',0);
            }
            if($keyword == 'send'){
                $query = $query->where('status','>',0);
            }
        }
        Session::put('_p_count',count($query->get()));
        Session::put('_p_pageSize',$limit);
        if((Session::get('p_page') * $limit) > Session::get('_p_count')){
            Session::put('_p_pages',Session::get('_p_count'));
        }else{
            Session::put('_p_pages',Session::get('p_page') * $limit);
        }
        $data = $query->orderBy('created_at','DESC')->offset($start)->limit($limit)->get();
        return $data;   
    }
    public function getListProject($limit,$request){
        $start = (Session::get('list_page')-1) * $limit;
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if((Session::get('keyword_project'))){
            $keyword = Session::get('keyword_project');
            if($keyword == 'all'){
                Session::forget('keyword_project');
                Session::forget('search');
                Session::forget('month');
                Session::forget('year');
            }
            if($keyword == 'pending'){
                $project_ids = \App\LogApproved::where('member_id',\Auth::guard('member')->user()->id)->whereIn('progress',[2,3,6])->pluck('project_id');
                $query = $query->whereIn('id',$project_ids);
                Session::put('keyword_project','pending');
            }
            if($keyword == 'save'){
                $query = $query->where('is_saved',1);
                Session::put('keyword_project','save');
            }
            if($keyword == 'remove'){
                $query = $query->where('is_deleted',1);
                Session::put('keyword_project','remove');
            }
            if($keyword == 'draft'){
                $query = $query->where('status',0);
                 Session::put('keyword_project','draft');
            }
            if($keyword == 'send'){
                $query = $query->where('status','>',0);
                Session::put('keyword_project','send');
            }
            if($keyword == 'approved'){
                $query = $query->where('status','=',\App\Project::STATUS_ACTIVE);
                Session::put('keyword_project','approved');
            }
            if($keyword == 'return'){
                $query = $query->where('status','=',\App\Project::STATUS_CANCEL);
                Session::put('keyword_project','return');
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
                Session::put('keyword_project','save_member');
            }
        }
        if(Session::get('search')){
            $search = Session::get('search');
            if(isset($search['full_name'])){
            $member_ids = \App\Member::where('full_name','like','%'.$search['full_name'].'%')->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
            }
            if(isset($search['department_id'])){
                $member_ids = \App\Member::where('department_id',$search['department_id'])->get()->pluck('id')->toArray();
                $query = $query->whereIn('member_id',$member_ids);
            }
            if(isset($search['level'])){
                if($search['level'] == 0){
                    $query = $query->where('level',NULL);
                }else{
                    $query = $query->where('level',$search['level']);
                }
            }
            if(isset($search['status'])){
                $search['status'] = explode(',',$search['status']);
                $query = $query->whereIn('status',$search['status']);
            }
        }
        
        if(isset($_GET['keywords'])){
            $query = $query->where('name','like','%'.$_GET['keywords'].'%');
        }
        if($request->get('month') && $request->get('year')){
            $query = $query->whereMonth('created_at',$request->get('month'))->whereYear('created_at',$request->get('year'));
            Session::put('month',$request->get('month'));
            Session::put('year',$request->get('year'));
        }
        Session::put('_list_count',count($query->get()));
        if((Session::get('list_page') * $limit) > Session::get('_list_count')){
            Session::put('_list_pages',Session::get('_list_count'));
        }else{
            Session::put('_list_pages',Session::get('list_page') * $limit);
        }
        $data = $query->orderBy('created_at','DESC')->get();
        return $data;   
    }
    public function save($project_id){
        return $this->model->whereIn('id',$project_id)->update(['is_saved'=>1]);
    }
    public function send($project_id,$level){
        if($level == \App\Project::STATUS_ACTIVE){
            $this->model->where('id',$project_id)->update(['approved_at'=>date('Y-m-d h:i:s'),'sort_date'=>date('Y-m-d h:i:s')]);
        }
        return $this->model->where('id',$project_id)->update(['status'=>$level,'sort_date'=>date('Y-m-d h:i:s')]);
    }
    public function remove($project_id){
        return $this->model->whereIn('id',$project_id)->update(['is_deleted'=>1]);
    }
    public function fillter($keyword,$limit){
        $query = $this->model->where('member_id',\Auth::guard('member')->user()->id);
        if($keyword == 'save'){
            $query = $query->where('is_saved',1);
        }
        if($keyword == 'remove'){
            $query = $query->where('is_deleted',1);
        }else{
            $query = $query->where('is_deleted',0);
        }
        if($keyword == 'draft'){
            $query = $query->where('status',0);
        }
        if($keyword == 'send'){
            $query = $query->where('status','>',0);
        }
        $data = $query->orderBy('created_at','DESC')->paginate($limit);
        return $data;
    }
    public function fillterMobile($keyword){
        $query = $this->model->where('member_id',\Auth::guard('member')->user()->id);
        if($keyword == 'save'){
            $query = $query->where('is_saved',1);
        }
        if($keyword == 'remove'){
            $query = $query->where('is_deleted',1);
        }else{
            $query = $query->where('is_deleted',0);
        }
        if($keyword == 'draft'){
            $query = $query->where('status',0);
        }
        if($keyword == 'send'){
            $query = $query->where('status','>',0);
        }
        $data = $query->orderBy('created_at','DESC')->get();
        return $data;
    }
    public function getIndexByMember($request){
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if(!is_null($request->get('keyword_project'))){
            $keyword = $request->get('keyword_project');
            if($keyword == 'pending' && \Auth::guard('member')->user()->level != 5){
                $project_ids = \App\LogApproved::where('member_id',\Auth::guard('member')->user()->id)->whereIn('progress',[2,3,6])->where('status',1)->pluck('project_id');
                $query = $query->whereIn('id',$project_ids);
            }elseif($keyword == 'pending' && \Auth::guard('member')->user()->level == 5){
                $query = $query->whereIn('status',[1,2]);
            }
            if($keyword == 'return'){
                $query = $query->where('status', \App\Project::STATUS_CANCEL);
            }
            if($keyword == 'approved'){
                $query = $query->where('status',\App\Project::STATUS_ACTIVE);
            }
            if($keyword == 'consider'){
                $query = $query->where('consider',1);
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
            }
        }
        if($request->get('full_name')){
            $member_ids = \App\Member::where('full_name','like','%'.$request->get('full_name').'%')->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if($request->get('department_id')){
            $member_ids = \App\Member::where('department_id',$request->get('department_id'))->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if($request->get('team_id')){
            $member_ids = \App\Member::where('team_id',$request->get('team_id'))->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if($request->get('start_date')){
            $query = $query->whereDate('created_at','>=',$request->get('start_date'));
        }
        if($request->get('end_date')){
            $query = $query->whereDate('created_at','<=',$request->get('end_date'));
        }
        if($request->get('level')){
            if($request->get('level') == 'KCD'){
                $query = $query->where('level',NULL);
            }else{
                $query = $query->where('level',$request->get('level'));
            }
        }
        if($request->get('status')){
            if($request->get('status') == 7){
                $new_project_ids = \App\LogApproved::where('progress',7)->where('status',2)->pluck('project_id');
                
            }else{
                $logapproved = \App\LogApproved::where('progress',$request->get('status'))->where('status',1)->get();
                $new_project_ids = [];
                foreach($logapproved as $val){
                    $max = \App\LogApproved::where('project_id',$val->project_id)->orderBy('progress','DESC')->first()->progress;
                    if($val->progress == $max){
                        $new_project_ids[] = $val->project_id;
                    }
                }
            }
            $query = $query->whereIn('id',$new_project_ids);
            
        }
        $keyword = $request->get('keyword');
        if($keyword == 'unapproved'){
            $query = $query->where('status','<',\App\Project::STATUS_ACTIVE)->whereDate('created_at',date('Y-m-d'));
        }
        if(isset($_GET['keywords'])){
            $query = $query->where(function ($que){
                return $que->where('name','like','%'.$_GET['keywords'].'%')->orWhere('id',$_GET['keywords']);
            });
        }
        $data = $query->orderBy('sort_date','DESC')->paginate(20);
        return $data;  
    }
    
     public function export($ids){
        return $this->model->whereIn('id',$ids)->get();
    }
    public function getExport(){
        $query = $this->model->where('is_deleted',0)->where('status','>',0);
        if(isset($_GET['keyword_project'])){
            $keyword = $_GET['keyword_project'];
            if($keyword == 'pending'){
                $ids = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->pluck('id');
                $member = \App\Member::where('department_id',\Auth::guard('member')->user()->department_id)->where('level','<',\Auth::guard('member')->user()->level)->orderBy('level','DESC')->first();
                $query = $query->where('status',$member->level)->whereIn('member_id',$ids);
            }
            if($keyword == 'return'){
                $query = $query->where('status', \App\Project::STATUS_CANCEL);
            }
            if($keyword == 'approved'){
                $query = $query->where('status',\App\Project::STATUS_ACTIVE);
            }
            if($keyword == 'save_member'){
                $ids = \App\LogSaved::where('member_id',\Auth::guard('member')->user()->id)->pluck('project_id')->toArray();
                $query = $query->whereIn('id',$ids);
            }
        }
        if(isset($_GET['full_name']) && $_GET['full_name'] != ''){
            $member_ids = \App\Member::where('full_name','like','%'.$_GET['full_name'].'%')->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if(isset($_GET['department_id']) && $_GET['department_id'] != ''){
            $member_ids = \App\Member::where('department_id',$_GET['department_id'])->get()->pluck('id')->toArray();
            $query = $query->whereIn('member_id',$member_ids);
        }
        if(isset($_GET['level']) && $_GET['level'] != ''){
            if($_GET['level'] == 'KCD'){
                $query = $query->where('level',NULL);
            }else{
                $query = $query->where('level',$_GET['level']);
            }
        }
        if(isset($_GET['status']) && $_GET['status'] != ''){
            $search['status'] = explode(',',$_GET['status']);
            $query = $query->whereIn('status',$search['status']);
        }
        if(isset($_GET['keyword']) && $_GET['keyword'] == 'unapproved'){
            $query = $query->where('status','<',\App\Project::STATUS_ACTIVE)->whereDate('created_at',date('Y-m-d'));
        }
        if(isset($_GET['keywords'])){
            $query = $query->where('name','like','%'.$_GET['keywords'].'%');
        }
        $data = $query->orderBy('created_at','DESC')->get();
        return $data;
    }
    public function checkDraft($project_ids){
        return $this->model->whereIn('id',$project_ids)->where('status','<>',0)->count();
    }
    function htmlToPlainText($str){
        $str = html_entity_decode($str, ENT_QUOTES | ENT_XML1, 'UTF-8');
        $str = htmlspecialchars_decode($str);
        $str = html_entity_decode($str);
        $str = strip_tags($str);
        return $str;
    }
    public function exportForm1($request){
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $department_id = $request->get('department_id');
        $member_ids = $this->model->whereDate('approved_at','>=',$start_date)->whereDate('approved_at','<=',$end_date)->groupBy('member_id')->pluck('member_id')->toArray();
        if(is_null($department_id)){
            $records = \App\Member::whereNotIn('id',$member_ids)->where('level','<',4)->where('department_id','<>',8)->where('is_deleted',0)->get();
        }else{
            $records = \App\Member::whereNotIn('id',$member_ids)->where('level','<',4)->where('department_id',$department_id)->where('is_deleted',0)->get();
        }
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'DANH SÁCH CÁ NHÂN KHÔNG CÓ ĐỀ TÀI THÁNG   NĂM')->mergeCells("A3:F3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('E4', 'Ngày:')->mergeCells("E4:F4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Tên phòng')
            ->setCellValue('C5', 'Tên nhóm')
            ->setCellValue('D5', 'Mã số nhân viên')
            ->setCellValue('E5', 'Họ và tên')
            ->setCellValue('F5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:F5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        $no = 1;
        foreach ($records as $key=>$record)
        {
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record->department->name)
                ->setCellValue('C' . $rows, $record->team ? $record->team->name : '')
                ->setCellValue('D' . $rows, $record->login_id)
                ->setCellValue('E' . $rows, $record->full_name)
                ->setCellValue('F' . $rows, $record->note) 
                ;
            $rows++;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."F" . $rows . "")->applyFromArray($styleArray);
        }
        $rows = $rows +1;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."C" . $rows . "")
                    ->setCellValue('D' . $rows, 'Người phê duyệt')->mergeCells("D". $rows .":"."F" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:F3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:F3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."C" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("D". $rows .":"."F" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DANH_SACH_CA_NHAN_KHONG_CO_DE_TAI_NGAY_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/DANH_SACH_CA_NHAN_KHONG_CO DE_TAI_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/DANH_SACH_CA_NHAN_KHONG_CO DE_TAI_' . $timestamp . '.xls';
        return $href;
    }
    public function exportForm2($request){
        $teams = \App\Team::get();
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        foreach($teams as $key=>$team){
            if(count($team->member) > 0){
                $count_project = 0;
                foreach($team->member as $k=>$member){
                    $count_project += $member->project()->where('status',\App\Project::STATUS_ACTIVE)->whereDate('approved_at','>=',$start_date)->whereDate('approved_at','<=',$end_date)->count();
                }
                $team->total = $count_project;
                $team->rank =0;
                $team->ratio = round(($count_project / count($team->member)),2) ;
                $team->count_member = count($team->member);
                $team->department_name = $team->member()->first()->department ? $team->member()->first()->department->name : ''; 
            }else{
                $teams->forget($key);
            }
        }
        $records = collect($teams->toArray())->sortByDesc('ratio')->values();
        $rank  = 1;
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'DANH SÁCH XẾP LOẠI NHÓM THÁNG   NĂM')->mergeCells("A3:H3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('G4', 'Ngày:')->mergeCells("G4:H4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Tên phòng')
            ->setCellValue('C5', 'Tên nhóm')
            ->setCellValue('D5', 'Tổng số người')
            ->setCellValue('E5', 'Số đề tài')
            ->setCellValue('F5', 'Tỉ lệ đề tài/ người')
            ->setCellValue('G5', 'Xếp hạng')
            ->setCellValue('H5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:H5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5','G5','H5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        $no = 1;
        $total_member = 0;
        $total_project = 0;
        foreach ($records as $key=>$record)
        {
            $total_member += $record['count_member'] ;
            $total_project += $record['total'] ;
            if($key == 0){
                $rank = 1;
                $repeat = 0;
            }else{
                if($records[$key - 1]['ratio'] == $record['ratio']){
                    ++$repeat;
                }elseif($records[$key - 1]['ratio'] > $record['ratio']){
                    if($repeat == 0){
                        ++$rank;
                    }else{
                        $rank = $repeat + $rank + 1;
                        $repeat = 0;
                    }
                }
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record['department_name'])
                ->setCellValue('C' . $rows, $record['name'])
                ->setCellValue('D' . $rows, $record['count_member'])
                ->setCellValue('E' . $rows, $record['total'])
                ->setCellValue('F' . $rows, $record['ratio'])
                ->setCellValue('G' . $rows, $rank)
                ->setCellValue('H' . $rows, '') 
                ;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."H" . $rows . "")->applyFromArray($styleArray);
            $rows++;
        }
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A'. $rows, 'Tổng')->mergeCells("A". $rows .":"."C" . $rows . "");
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."C" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()
                    ->setCellValue('D'. $rows, $total_member)
                    ->setCellValue('E'. $rows, $total_project)
                    ->setCellValue('F'. $rows, round($total_project/$total_member,2));
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."H" . $rows . "")->applyFromArray($styleArray);
        $rows = $rows +2;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."D" . $rows . "")
                    ->setCellValue('E' . $rows, 'Người phê duyệt')->mergeCells("E". $rows .":"."H" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."D" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("E". $rows .":"."H" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DANH_SACH_XEP_LOAI_NHOM_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/DANH_SACH_XEP_LOAI_NHOM_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/DANH_SACH_XEP_LOAI_NHOM_' . $timestamp . '.xls';
        return $href;
       
    }
    public function exportForm3($request){
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $department_id = $request->get('department_id');
        if(is_null($department_id)){
            $records = \App\Member::where('level','<',4)->where('department_id','<>',8)->where('is_deleted',0)->get();
        }else{
            $records = \App\Member::where('level','<',4)->where('department_id',$department_id)->where('is_deleted',0)->get();
        }
        foreach($records as $key=>$record){
            $record->department_name = $record->department ? $record->department->name : '';
            $record->team_name = $record->team ? $record->team->name : '';
            $record->count_project = $record->project()->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->whereDate('approved_at','>=',$start_date)->whereDate('approved_at','<=',$end_date)->count();
        }
        $records = collect($records->toArray())->sortByDesc('count_project')->values();
        $rank  = 1;
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'DANH SÁCH XẾP LOẠI CÁ NHÂN QUÝ   NĂM')->mergeCells("A3:H3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('G4', 'Ngày:')->mergeCells("G4:H4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Tên phòng')
            ->setCellValue('C5', 'Tên nhóm')
            ->setCellValue('D5', 'Mã số nhân viên')
            ->setCellValue('E5', 'Họ và tên')
            ->setCellValue('F5', 'Số đề tài')
            ->setCellValue('G5', 'Xếp hạng')
            ->setCellValue('H5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:H5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5','G5','H5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        foreach ($records as $key=>$record)
        {
            if($key == 0){
                $rank = 1;
                $repeat = 0;
            }else{
                if($records[$key - 1]['count_project'] == $record['count_project']){
                    ++$repeat;
                }elseif($records[$key - 1]['count_project'] > $record['count_project']){
                    if($repeat == 0){
                        ++$rank;
                    }else{
                        $rank = $repeat + $rank + 1;
                        $repeat = 0;
                    }
                }
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record['department_name'])
                ->setCellValue('C' . $rows, $record['team_name'])
                ->setCellValue('D' . $rows, $record['login_id'])
                ->setCellValue('E' . $rows, $record['full_name'])
                ->setCellValue('F' . $rows, $record['count_project'])
                ->setCellValue('G' . $rows, $rank)
                ->setCellValue('H' . $rows, '') 
                ;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."H" . $rows . "")->applyFromArray($styleArray);
            $rows++;
        }
        $rows = $rows +1;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."D" . $rows . "")
                    ->setCellValue('E' . $rows, 'Người phê duyệt')->mergeCells("E". $rows .":"."H" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."D" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("E". $rows .":"."H" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DANH_SACH_XEP_LOAI_CA_NHAN_QUY_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/DANH_SACH_XEP_LOAI_CA_NHAN_QUY_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/DANH_SACH_XEP_LOAI_CA_NHAN_QUY_' . $timestamp . '.xls';
        return $href;
    }
    public function exportForm4($request){
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $department_id = $request->get('department_id');
        if($request->get('project_id') != ''){
            $records = \App\Project::whereIn('id',explode(',',$request->get('project_id')))->get();
        }else{
            if(is_null($department_id)){
                $member_ids = \App\Member::where('is_deleted',0)->pluck('id')->toArray();
                $records = \App\Project::where('status',\App\Project::STATUS_ACTIVE)->whereIn('member_id',$member_ids)->where('is_deleted',0)->whereDate('approved_at','>=',$start_date)->whereDate('approved_at','<=',$end_date)->get();
            }else{
                $member_ids = \App\Member::where('department_id',$department_id)->where('is_deleted',0)->pluck('id')->toArray();
                $records = \App\Project::whereIn('member_id',$member_ids)->where('is_deleted',0)->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->whereDate('approved_at','>=',$start_date)->whereDate('approved_at','<=',$end_date)->get();
            }
        }
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'TỔNG HỢP ĐỀ TÀI THÁNG   NĂM')->mergeCells("A3:J3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('I4', 'Ngày:')->mergeCells("I4:J4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(60);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(100);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(100);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Tên phòng')
            ->setCellValue('C5', 'Tên nhóm')
            ->setCellValue('D5', 'Mã số nhân viên')
            ->setCellValue('E5', 'Họ và tên')
            ->setCellValue('F5', 'Cấp độ đề tài')
            ->setCellValue('G5', 'Tên đề tài')
            ->setCellValue('H5', 'Trước cải tiến')
            ->setCellValue('I5', 'Sau cải tiến')
            ->setCellValue('J5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:J5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5','G5','H5','I5','J5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        foreach ($records as $key=>$record)
        {
            if($record->level > 0){
                $level = 'Cấp độ '.$record->level; 
            }else{
                $level = '';
            }
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record->member->department ? $record->member->department->name : '')
                ->setCellValue('C' . $rows, $record->member->team ? $record->member->team->name : '')
                ->setCellValue('D' . $rows, $record->member->login_id)
                ->setCellValue('E' . $rows, $record->member->full_name)
                ->setCellValue('F' . $rows, $level)
                ->setCellValue('G' . $rows, $record->name)
                ->setCellValue('H' . $rows, $this->htmlToPlainText($record->before_content))
                ->setCellValue('I' . $rows, $this->htmlToPlainText($record->after_content))
                ->setCellValue('J' . $rows, $record->type == 2 ? 'Đề án nhóm' : '');
                ;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."J" . $rows . "")->applyFromArray($styleArray);
            $rows++;
        }
        $rows = $rows +1;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."E" . $rows . "")
                    ->setCellValue('F' . $rows, 'Người phê duyệt')->mergeCells("F". $rows .":"."J" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."E" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("F". $rows .":"."J" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="TONG_HOP_DE_TAI_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/TONG_HOP_DE_TAI_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/TONG_HOP_DE_TAI_' . $timestamp . '.xls';
        return $href;
    }
    public function checkName($name){
       if($this->model->where('name',$name)->count() > 0){
           return true;
       }else{
           return false;
       }
    }
    public function exportForm5($request){
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'DS CÁ NHÂN KHÔNG CÓ ĐỀ ÁN')->mergeCells("A3:M3")
                    ->setCellValue('A4', 'TỪ T1-T6 NĂM 2021')->mergeCells("A4:M4")
                    ->setCellValue('H5', 'Ngày 23/03/2021')->mergeCells("H5:M5");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(40);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $now = getDate()['mon'];
        // dd($now);
        if($now<=6){
            $month = ['01','02','03','04','05','06'];
            $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'STT')
            ->setCellValue('B6', 'Tên phòng')
            ->setCellValue('C6', 'Tên nhóm')
            ->setCellValue('D6', 'Mã số nhân viên')
            ->setCellValue('E6', 'Họ và tên')
            ->setCellValue('F6', 'Tháng 1')
            ->setCellValue('G6', 'Tháng 2')
            ->setCellValue('H6', 'Tháng 3')
            ->setCellValue('I6', 'Tháng 4')
            ->setCellValue('J6', 'Tháng 5')
            ->setCellValue('K6', 'Tháng 6')
            ->setCellValue('L6', 'Số tháng ko có đề án')
            ->setCellValue('M6', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A6:M6" )->applyFromArray($styleArray);
        $bold_range = ['A6', 'B6', 'C6', 'D6','E6','F6','G6','H6','I6','J6','K6','L6','M6'];

        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 7;
        $no = 1;
        }else{
            $month = ['07','08','09','10','11','12'];
            $spreadsheet->getActiveSheet()
            ->setCellValue('A6', 'STT')
            ->setCellValue('B6', 'Tên phòng')
            ->setCellValue('C6', 'Tên nhóm')
            ->setCellValue('D6', 'Mã số nhân viên')
            ->setCellValue('E6', 'Họ và tên')
            ->setCellValue('F6', 'Tháng 7')
            ->setCellValue('G6', 'Tháng 8')
            ->setCellValue('H6', 'Tháng 9')
            ->setCellValue('I6', 'Tháng 10')
            ->setCellValue('J6', 'Tháng 11')
            ->setCellValue('K6', 'Tháng 12')
            ->setCellValue('L6', 'Số tháng ko có đề án')
            ->setCellValue('M6', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A6:M6" )->applyFromArray($styleArray);
        $bold_range = ['A6', 'B6', 'C6', 'D6','E6','F6','G6','H6','I6','J6','K6','L6','M6'];

        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 7;
        $no = 1;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $n = count($month);
        $department_id = $request->get('department_id');
        for($i=0;$i<$n;$i++){
            $member_ids[] = $this->model->whereDate('approved_at','<',date('Y-m-d',strtotime(date('Y-'.$month[$i].'-24'))))->whereDate('approved_at','>',date('Y-m-d',strtotime('-1 months',strtotime(date('Y-'.$month[$i].'-23')))))->groupBy('member_id')->pluck('member_id')->toArray();
            // $y = date('Y-m-d',strtotime("-1 months",strtotime(date('Y-'.$month[$i].'-24'))));
            // $x = date('Y-m-d',strtotime(date('Y-'.$month[$i].'-24')));
            // dd($x);
            if(is_null($department_id)){
                $records[] = \App\Member::whereNotIn('id',$member_ids[$i])->where('level','<',4)->where('department_id','<>',8)->get();
            }else{
                $records[] = \App\Member::whereNotIn('id',$member_ids[$i])->where('level','<',4)->where('department_id','<>',8)->where('department_id',$department_id)->get();
            }
        }
        // dd($member_ids);
        // dd($now);
        for($i=0;$i<$now;$i++){//lay id ca nhan ko co de an
            foreach($records[$i] as $record){
                $member_id[] = $record->id;
            }
        }
        // dd($member_id);
        $records_id = array_unique($member_id);
        // for($i=0;$i<$n;$i++){
            if(is_null($department_id)){//tat ca cac ca nhan ko co de an
                $records_all = \App\Member::whereIn('id',$records_id)->where('level','<',4)->where('department_id','<>',8)->get();
            }else{
                $records_all = \App\Member::whereIn('id',$records_id)->where('level','<',4)->where('department_id','<>',8)->where('department_id',$department_id)->get();
            }
        // }
        // dd($records_all);//TT cac ca nhan ko co de an
        foreach ($records_all as $key=>$record)
        {
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $record->department->name)
                ->setCellValue('C' . $rows, $record->team ? $record->team->name : '')
                ->setCellValue('D' . $rows, $record->login_id)
                ->setCellValue('E' . $rows, $record->full_name)
                ->setCellValue('M' . $rows, $record->note) 
                ;
            $dem = 0;
            $max = 0;
            // for($i=0;$i<$n;$i++){
            $i=0;
            foreach($records[$i] as $key=>$record_month){   
                $id_month[] = $record_month->id;
                // dd($record_month->id);
            }
            if(in_array($record->id,$id_month)){
                // dd($month[$i]);
                // dd($i+1);
                // dd(date('Y-m-d',strtotime(date('Y-'.$month[$i].'-24'))));
                if(strtotime(date('Y-m-d',strtotime(date('Y-'.$month[$i].'-23')))) < strtotime(date('Y-m-d',strtotime(date('Y-'.$now.'-d'))))){
                    $spreadsheet->getActiveSheet()->setCellValue('F' . $rows, 'X');
                    // $i++;
                }else{
                    $spreadsheet->getActiveSheet()->setCellValue('F' . $rows, '');
                    // $i++;
                }
                    $dem++;
                    if($dem > $max){
                        $max = $dem;
                        $i++;
                    }
                    else{
                        $max = $max;
                        $i++;
                    }
            }else{
                $dem = 0;
                $max = $max;
                $i++;
            }
            //////////////////////
            foreach($records[$i] as $key=>$record_month){
                $id_month1[] = $record_month->id;
            }
            if(in_array($record->id,$id_month1)){
                
                if(strtotime(date('Y-m-d',strtotime(date('Y-'.$month[$i].'-23')))) < strtotime(date('Y-m-d',strtotime(date('Y-'.$now.'-d'))))){
                    $spreadsheet->getActiveSheet()->setCellValue('G' . $rows, 'X');
                    $dem++;
                    if($dem > $max){
                        $max = $dem;
                        $i++;
                    }
                    else{
                        $max = $max;
                        $i++;
                    }
                }else{
                    $spreadsheet->getActiveSheet()->setCellValue('G' . $rows, '');
                    // $i++;
                }
            }else{
                $dem = 0;
                $max = $max;
                $i++;
            }
            //////////////////////
            foreach($records[$i] as $key=>$record_month){
                $id_month2[] = $record_month->id;
            }
            // dd($id_month2);
            if(in_array($record->id,$id_month2)){
                // dd($i);
                if(strtotime(date('Y-m-d',strtotime(date('Y-'.$month[$i].'-23')))) < strtotime(date('Y-m-d',strtotime(date('Y-'.$now.'-d'))))){
                    $spreadsheet->getActiveSheet()->setCellValue('H' . $rows, 'X');
                    $dem++;
                    if($dem > $max){
                        $max = $dem;
                        $i++;
                    }
                    else{
                        $max = $max;
                        $i++;
                    }
                }else{
                    $spreadsheet->getActiveSheet()->setCellValue('H' . $rows, '');
                    // $i++;
                }
            }else{
                $dem = 0;
                $max = $max;
                $i++;
            }
            //////////////////////4
            foreach($records[$i] as $key=>$record_month){
                $id_month3[] = $record_month->id;
            }
            if(in_array($record->id,$id_month3)){
                // dd($i);
                if(strtotime(date('Y-m-d',strtotime(date('Y-'.$month[$i].'-23')))) < strtotime(date('Y-m-d',strtotime(date('Y-'.$now.'-d'))))){
                    $spreadsheet->getActiveSheet()->setCellValue('I' . $rows, 'X');
                    $dem++;
                    if($dem > $max){
                        $max = $dem;
                        $i++;
                    }
                    else{
                        $max = $max;
                        $i++;
                    }
                }else{
                    $spreadsheet->getActiveSheet()->setCellValue('I' . $rows, '');
                    // $i++;
                }
            }else{
                $dem = 0;
                $max = $max;
                $i++;
            }
            //////////////////////
            foreach($records[$i] as $key=>$record_month){
                $id_month4[] = $record_month->id;
            }
            if(in_array($record->id,$id_month4)){
                // dd($i);
                if(strtotime(date('Y-m-d',strtotime(date('Y-'.$month[$i].'-23')))) < strtotime(date('Y-m-d',strtotime(date('Y-'.$now.'-d'))))){
                    $spreadsheet->getActiveSheet()->setCellValue('J' . $rows, 'X');
                    $dem++;
                    if($dem > $max){
                        $max = $dem;
                        $i++;
                    }
                    else{
                        $max = $max;
                        $i++;
                    }
                }else{
                    $spreadsheet->getActiveSheet()->setCellValue('J' . $rows, '');
                    // $i++;
                }
            }else{
                $dem = 0;
                $max = $max;
                $i++;
            }
            //////////////////////
            foreach($records[$i] as $key=>$record_month){
                $id_month5[] = $record_month->id;
            }
            if(in_array($record->id,$id_month5)){
                // dd($i);
                // $time_i = strtotime(date('Y-m-d',strtotime(date('Y-'.$month[$i].'-23'))));
                // $time_now = strtotime(date('Y-m-d',strtotime(date('Y-'.$now.'-d'))));
                // dd($time_now);
                if(strtotime(date('Y-m-d',strtotime(date('Y-'.$month[$i].'-23')))) < strtotime(date('Y-m-d',strtotime(date('Y-'.$now.'-d'))))){
                    $spreadsheet->getActiveSheet()->setCellValue('K' . $rows, 'X');
                    $dem++;
                    if($dem > $max){
                        $max = $dem;
                        $i++;
                    }
                    else{
                        $max = $max;
                        $i++;
                    }
                }else{
                    $spreadsheet->getActiveSheet()->setCellValue('K' . $rows, '');
                    // $i++;
                }
            }else{
                $dem = 0;
                $max = $max;
                $i++;
            }
            //////////////////////
            $spreadsheet->getActiveSheet()->setCellValue('L' . $rows, $max);
            // }
            
            $rows++;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A7:"."M" . ($rows-1) . "")->applyFromArray($styleArray);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $rows = $rows +1;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."G" . $rows . "")
                    ->setCellValue('H' . $rows, 'Người kiểm tra')->mergeCells("H". $rows .":"."M" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:M4')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A4:M4')->getFont()->setItalic(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:M4')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle('H5:M5')->getFont()->setItalic(true);
        $spreadsheet->getActiveSheet()->getStyle('H5:M5')->getAlignment()->setHorizontal('right');
        $spreadsheet->getActiveSheet()->getStyle("F7:"."L" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."G" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("H". $rows .":"."M" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."M" . $rows . "")->getFont()->setBold(true);
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DS_CA_NHAN_KO_CO_DE_AN_T1-T6_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/DS_CA_NHAN_KO_CO_DE_AN_T1-T6_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/DS_CA_NHAN_KO_CO_DE_AN_T1-T6_' . $timestamp . '.xls';
        return $href;
    }
    public function exportForm6($request){
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        // $department_id = $request->get('department_id');     
        // if(is_null($department_id)){
        //     $departments=\App\Department::where('id','<>',5)->get();
        //     $nameDepartments = "TẤT CẢ PHÒNG BAN";
        // }else{
        //     $departments=\App\Department::where('id',$department_id)->get();
        //     // foreach($departments as $department )
        //     //     $nameDepartments= Str::upper($department->name);
        //     $nameDepartments =Str::upper(\App\Department::where('id',$department_id)->value("name"));
        // }
        $teams = \App\Team::get();
        foreach($teams as $key=>$team){
            $members = $team->member()->get();
            $team->CountMember= $members->count();
            $team->CountProject = 0;
            $team->CountMemberNoneProject=0;
            foreach($members as $member){
                $dem = $member->project()->where('status',\App\Project::STATUS_ACTIVE)->where('is_deleted',0)->whereDate('approved_at','>=',$start_date)->whereDate('approved_at','<=',$end_date)->count();
                if($dem==0){
                    $team->CountMemberNoneProject=$team->CountMemberNoneProject+1;
                }
                else{
                    $team->CountProject=$team->CountProject+$dem;
                }
                
            }
            if($team->CountMember==0){
                $team->rate1 =$team->rate2 =$team->CountMemberNoneProject = 0;
            }
            else{
                $team->rate1 = round($team->CountProject/$team->CountMember,2);
                $team->rate2=round($team->CountMemberNoneProject/$team->CountMember,2);
            }
        }
        $team = collect($team->toArray())->sortByDesc('rate1')->values();
        $rank  = 1;
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A3', 'BẢNG XẾP THỨ TỰ NHÓM VÀ TỈ LỆ ĐỀ ÁN  TỪ: '.date('d/m/Y',strtotime($start_date))." ĐẾN ".date('d/m/Y',strtotime($end_date)))->mergeCells("A3:H3")
                    ->setCellValue('A4', 'Chuẩn bị bởi:')->mergeCells("A4:B4")
                    ->setCellValue('G4', 'Ngày:')->mergeCells("G4:H4");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(6);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()
            ->setCellValue('A5', 'STT')
            ->setCellValue('B5', 'Họ và tên')
            ->setCellValue('C5', 'Tổng số người')
            ->setCellValue('D5', 'Số đề tài')
            ->setCellValue('E5', 'Tỉ lệ đề án/ người')
            ->setCellValue('F5', 'Số người không có đề án')
            ->setCellValue('G5', 'Tỉ lệ người không có đề án/ tổng số người')
            ->setCellValue('H5', 'Ghi chú');
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                )
            )
        );
        $spreadsheet->getActiveSheet()->getStyle("A5:H5" )->applyFromArray($styleArray);
        $bold_range = ['A5', 'B5', 'C5', 'D5','E5','F5','G5','H5'];
        foreach ($bold_range as $val) {
            $spreadsheet->getActiveSheet()->getStyle($val)->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle($val)->getAlignment()->setHorizontal('center');
        }
        $rows = 6;
        foreach ($teams as $key=>$team)
        {
            
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $rows, ++$key) 
                ->setCellValue('B' . $rows, $team['name'])
                ->setCellValue('C' . $rows, $team['CountMember'])
                ->setCellValue('D' . $rows, $team['CountProject'])
                ->setCellValue('E' . $rows, $team['rate1'])
                ->setCellValue('F' . $rows, $team['CountMemberNoneProject'])
                ->setCellValue('G' . $rows, $team['rate2'])
                ->setCellValue('H' . $rows, '') 
                ;
            $styleArray = array(
                'borders' => array(
                    'allBorders' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    )
                )
            );
            $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."H" . $rows . "")->applyFromArray($styleArray);
            $rows++;
        }
        $rows = $rows +1;
        $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $rows, 'Người chuẩn bị')->mergeCells("A". $rows .":"."D" . $rows . "")
                    ->setCellValue('E' . $rows, 'Người phê duyệt')->mergeCells("E". $rows .":"."H" . $rows . "");
        
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true)->setSize(18);
        $spreadsheet->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle('C6:H'.$rows)->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A". $rows .":"."D" . $rows . "")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("E". $rows .":"."H" . $rows . "")->getAlignment()->setHorizontal('center');
        header("Content-Description: File Transfer");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="BANG_XEP_THU_TU_VA_TY_LE_DE_AN_' . date('d/m/y') . '.xlsx"');
        header('Cache-Control: max-age=0');
        header("Content-Transfer-Encoding: binary");
        header('Expires: 0');
        $writer =  new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $timestamp = time();
        $writer->save('file/BANG_XEP_THU_TU_VA_TY_LE_DE_AN_'.$timestamp.'.xls');
        $href = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") .'://' . \Request::server('SERVER_NAME') . '/file/BANG_XEP_THU_TU_VA_TY_LE_DE_AN_' . $timestamp . '.xls';
        return $href;
    }
}
