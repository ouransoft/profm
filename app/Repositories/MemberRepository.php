<?php
namespace App\Repositories;
use App\Notifications\Notificate;
use Pusher\Pusher;


use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;

class MemberRepository extends AbstractRepository
{
    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Member';
    }
    
    public function validateCreate(){
        return $rules = [
            'login_id' => 'required|unique:member',
            'password' => 'required',
        ];
    }

    public function validateUpdate($id){
        return $rules = [
            'login_id' => 'required|unique:member,login_id,'.$id.',id',
        ];
    }
    public function getAll(){
        $data = $this->model->where('is_deleted',0)->orderBy('created_at','DESC')->get();
        return $data;
    }
    public function getPaginate($limit){
        $data = $this->model->where('is_deleted',0)->where('id','<>',\Auth::guard('member')->user()->id)->orderBy('created_at','DESC')->paginate($limit);
        return $data;
    }
    public function getAllNotMe(){
        $data = $this->model->where('is_deleted',0)->where('id','<>',\Auth::guard('member')->user()->id)->orderBy('created_at','DESC')->get();
        return $data;
    }
    public function getAllEdit($member_id){
        $data = $this->model->where('is_deleted',0)->whereNotIn('id',$member_id)->orderBy('created_at','DESC')->get();
        return $data;
    }
    public function getAllRemove(){
        $data = $this->model->where('is_deleted',1)->orderBy('created_at','DESC')->get();
        return $data;
        
    }
    public function getIndex($request,$limit){
        $start = (Session::get('page')-1) * $limit;
        $query = $this->model->where('is_deleted',0);
        if($request->get('full_name')){
            $query = $query->where('full_name','like','%'.$request->get('full_name').'%');
        }
        if($request->get('position_id')){
            $query = $query->where('position_id',$request->get('position_id'));
        }
        if($request->get('department_id')){
            $query = $query->where('department_id',$request->get('department_id'));
        }
        $data = $query->orderBy('updated_at','DESC')->paginate($limit);
        return $data;
        
    }
    public function getIndexRemove($request,$limit){
        $start = (Session::get('page')-1) * $limit;
        $query = $this->model->where('is_deleted',1);
        if($request->get('full_name')){
            $query = $query->where('full_name','like','%'.$request->get('full_name').'%');
        }
        if($request->get('position_id')){
            $query = $query->where('position_id',$request->get('position_id'));
        }
        if($request->get('department_id')){
            $query = $query->where('department_id',$request->get('department_id'));
        }
        $data = $query->orderBy('updated_at','DESC')->paginate($limit);
        return $data;  
    }
    public function remove($member_id){
        $members = $this->model->whereIn('id',$member_id)->get();
        foreach($members as $key=>$member){
            $member->project()->update(['is_destroy'=>1]);
        }
        return $this->model->whereIn('id',$member_id)->update(['is_deleted'=>1]);
        
    }
    public function restore($member_id){
        $members = $this->model->whereIn('id',$member_id)->get();
        foreach($members as $key=>$member){
            $member->project()->update(['is_destroy'=>0]);
        }
        return $this->model->whereIn('id',$member_id)->update(['is_deleted'=>0]);
    }
    public function checkactivation($key) {
        return $this->model->where('activation', $key)->first();
    }
    public function export($ids){
        return $this->model->whereIn('id',$ids)->get();
    }
    public function Notificate($level_id, $data) {
        $members = \App\Member::where('level', $level_id)->where('department_id',\Auth::guard('member')->user()->department_id)->get();
        $data['time'] = date("H:i");
        $data['full_name'] = \Auth::guard('member')->user()->full_name;
        foreach ($members as $member) {
            $member->notify(new Notificate($data));
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true
            );
            $pusher = new Pusher(
                    env('PUSHER_APP_KEY') ,
                    env('PUSHER_APP_SECRET'),
                    env('PUSHER_APP_ID'), $options
            );
            $notification = $member->unreadNotifications()->orderBy('created_at', 'DESC')->first();
            $data['count'] = count($member->unreadNotifications);
            $data['member_id'] = $member->id;
            $data['id'] = $notification->id;
            $pusher->trigger('NotificationEvent', 'send-notification', $data);
        }
    }
    public function NotificateByMember($member_id,$level_id, $data) {
        $members = \App\Member::where('level', $level_id)->where('department_id',\Auth::guard('member')->user()->department_id)->get();
        $data['time'] = date("H:i");
        $record = $this->model->find($member_id);
        $data['full_name'] = $record->full_name;
        foreach ($members as $member) {
            $member->notify(new Notificate($data));
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true
            );
            $pusher = new Pusher(
                    env('PUSHER_APP_KEY') ,
                    env('PUSHER_APP_SECRET'),
                    env('PUSHER_APP_ID'), $options
            );
            $notification = $member->unreadNotifications()->orderBy('created_at', 'DESC')->first();
            $data['count'] = count($member->unreadNotifications);
            $data['member_id'] = $member->id;
            $data['id'] = $notification->id;
            $pusher->trigger('NotificationEvent', 'send-notification', $data);
        }
    }
    public function NotificateMember($member_id, $data) {
        $member = \App\Member::find($member_id);
        $data['time'] = date("H:i");
        $data['full_name'] = \Auth::guard('member')->user()->full_name;
        $member->notify(new Notificate($data));
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
                env('PUSHER_APP_KEY') ,
                env('PUSHER_APP_SECRET'),
                env('PUSHER_APP_ID'), $options
        );
        $notification = $member->unreadNotifications()->orderBy('created_at', 'DESC')->first();
        $data['count'] = count($member->unreadNotifications);
        $data['member_id'] = $member->id;
        $data['id'] = $notification->id;
        $pusher->trigger('NotificationEvent', 'send-notification', $data);
    }
    public function NotificateMembers($member_ids, $data) {
        $members = \App\Member::whereIn('id',$member_ids)->get();
        foreach($members as $key=>$member){
            $data['time'] = date("H:i");
            $data['full_name'] = \Auth::guard('member')->user()->full_name;
            $member->notify(new Notificate($data));
            $options = array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => true
            );
            $pusher = new Pusher(
                    env('PUSHER_APP_KEY') ,
                    env('PUSHER_APP_SECRET'),
                    env('PUSHER_APP_ID'), $options
            );
            $notification = $member->unreadNotifications()->orderBy('created_at', 'DESC')->first();
            $data['count'] = count($member->unreadNotifications);
            $data['member_id'] = $member->id;
            $data['id'] = $notification->id;
            $pusher->trigger('NotificationEvent', 'send-notification', $data);
        }
    }
    public function rank(){
        $members= DB::table('member')->join('project', 'project.member_id', '=', 'member.id')
                ->select('member.id', DB::raw('count(project.id) as count'))
                ->groupBy('member.id')->orderBy('count','DESC')->get();
        foreach($member as $key=>$val){
            if(\Auth::guard('member')->user()->id == $val->id){
                $position = $key + 1;
            }
        }
        return $position;
    }
    public function checkID($ID){
        return $this->model->where('login_id',$ID)->first();
    }
    public function searchByKeyword($keyword){
        return $this->model->where('full_name','like','%'.$keyword.'%')->where('id','<>',\Auth::guard('member')->user()->id)->get();
    }
    public function searchByKeywordPaginate($keyword){
        return $this->model->where('full_name','like','%'.$keyword.'%')->where('id','<>',\Auth::guard('member')->user()->id)->paginate(10);
    }
    public function whereArray($ids){
        return $this->model->whereIn('id',$ids);
    }
    public function getByDepartment($department_id,$member_id=''){
        if($member_id==''){
            return $this->model->whereIn('department_id',$department_id)->where('is_deleted',0)->orderBy('level','DESC')->get();
        }else{
            return $this->model->whereIn('department_id',$department_id)->where('is_deleted',0)->where('id','<>',$member_id)->orderBy('level','DESC')->get();
        }
    }
    public function getByDepartmentMe($department_id,$member_id){
        return $this->model->where(function($query) use ($department_id,$member_id){
                                                    $query->whereIn('department_id',$department_id)->orWhere('id',$member_id);
                                                   })->where('is_deleted',0)->orderBy('level','DESC')->get();
    }
    public function getByDepartmentPaginate($department_id,$member_id=''){
        if($member_id==''){
            return $this->model->whereIn('department_id',$department_id)->where('is_deleted',0)->orderBy('level','DESC')->paginate(9);
        }else{
            return $this->model->whereIn('department_id',$department_id)->where('is_deleted',0)->where('id','<>',$member_id)->orderBy('level','DESC')->paginate(9);
        }
    }
    public function searchMember($keyword){
        return $this->model->where('full_name','like','%'.$keyword.'%')->where('is_deleted',0)->get();
    }
    public function searchMemberApp($keyword,$member_ids){
        return $this->model->select('id','full_name','avatar')->where('full_name','like','%'.$keyword.'%')->whereNotIn('id',$member_ids)->where('is_deleted',0)->get();
    }
    public function searchMemberApprovedApp($keyword,$member_ids){
        return $this->model->select('id','full_name','avatar')->where('full_name','like','%'.$keyword.'%')->whereIn('id',$member_ids)->where('is_deleted',0)->get();
    }
    public function searchMemberMobile($input){
        $query = $this->model;
        if($input['member_id'] != ''){
            $query = $query->whereNotIn('id',explode(':',$input['member_id']));
        }
        if(isset($input['keyword'])){
            $query = $query->where('full_name','like','%'.$input['keyword'].'%');
        }
        if(isset($input['department_id'])){
            $query = $query->where('department_id',$input['department_id']);
        }
        return $query->where('is_deleted',0)->get();
    }
    public function getBySearch($input){
        $query = $this->model;
        if(isset($input['uids']) && $input['uids'] != ''){
            $query = $query->whereIn('id',explode(' ',$input['uids']));
        }
        if(isset($input['search_text']) && $input['search_text']){
            $query = $query->where('full_name','like','%'.$input['search_text'].'%');
        }
        return $query->get();
        
    }
    public function getByToken($token){
        return $this->model->where('api_token',$token)->first();
        
    }
    
}
