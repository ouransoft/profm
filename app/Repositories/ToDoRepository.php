<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ToDoRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\ToDo';
    }
    public function validateCreate(){
        return $rules = [
            'title' => 'required',
        ];
    }

    public function validateUpdate($id){
        return $rules = [
            'title' => 'required',
        ];
    }
    public function getIndex($request){
        $todo_ids = \Auth::guard('member')->user()->todo->pluck('id')->toArray();
        $query = $this->model;
        if(($request->get('filter_result') && $request->get('filter_result') == 'all') || !$request->get('filter_result') ){
            $query = $query->where(function($query) use ($todo_ids) {
                $query->where('created_by',\Auth::guard('member')->user()->id)
                        ->orWhereIn('id',$todo_ids);
            });
        }elseif($request->get('filter_result') && $request->get('filter_result') == 'give'){
            $query = $query->where('created_by',\Auth::guard('member')->user()->id);
        }elseif($request->get('filter_result') && $request->get('filter_result') == 'receive'){
            $query = $query->whereIn('id',$todo_ids);
        }
        if($request->get('title')){
            $query = $query->where('title','like','%'.$request->get('title').'%');
        }
        if($request->get('priority')){
            $query = $query->where('priority',$request->get('priority'));
        }
        if($request->get('start_date')){
            $query = $query->whereDate('end_date','>=',$request->get('start_date'));
        }
        if($request->get('end_date')){
            $query = $query->whereDate('start_date','<=',$request->get('end_date'));
        }
        $data = $query->where('status','<>',3)->orderBy('created_at','DESC')->get();
        return $data;
    }
    public function getHistory($request){
        $search = $request->all();
        $query = $this->model;
        $todo_ids = \Auth::guard('member')->user()->todo->pluck('id')->toArray();
        if(count($todo_ids) > 0){
            $query = $query->where(function($query) use ($todo_ids) {
                $query->where('created_by',\Auth::guard('member')->user()->id)
                      ->orWhereIn('id',$todo_ids);
            });
        }
        if(($request->get('filter_result') && $request->get('filter_result') == 'all') || !$request->get('filter_result') ){
            // dd(123);
            $query = $query->where(function($query) use ($todo_ids) {
                $query->where('created_by',\Auth::guard('member')->user()->id)
                        ->orWhereIn('id',$todo_ids);
            });
        }elseif($request->get('filter_result') && $request->get('filter_result') == 'give'){
            $query = $query->where('created_by',\Auth::guard('member')->user()->id);
        }elseif($request->get('filter_result') && $request->get('filter_result') == 'receive'){
            $query = $query->whereIn('id',$todo_ids);
        }
        if($request->get('title')){
            $query = $query->where('title','like','%'.$request->get('title').'%');
        }
        if($request->get('priority')){
            $query = $query->where('priority',$request->get('priority'));
        }
        if($request->get('start_date')){
            $query = $query->whereDate('end_date','>=',$request->get('start_date'));
        }
        if($request->get('end_date')){
            $query = $query->whereDate('start_date','<=',$request->get('end_date'));
        }
        // else{
        //     $query = $query->where('created_by',\Auth::guard('member')->user()->id);
        // }
        return $query->where('status',3)->orderBy('confirm','ASC')->get();
    }
    public function getByStatus($status){
        $query = $this->model;
        if($status == 'pending'){
            $query = $query->whereIn('status',['1','2']);
        }else{
            $query = $query->where('status',3);
        }
        $todo_ids = \Auth::guard('member')->user()->todo->pluck('id')->toArray();
        if(count($todo_ids) > 0){
            $query = $query->where(function($query) use ($todo_ids) {
                $query->where('created_by',\Auth::guard('member')->user()->id)
                      ->orWhereIn('id',$todo_ids);
            });
        }else{
            $query = $query->where('created_by',\Auth::guard('member')->user()->id);
        }
        return $query->orderBy('created_at','DESC')->get();
    }
    public function deleteAll(){
        return $this->model->where('status',3)->delete();
    }
    public function updateList($ids){
        return $this->model->whereIn('id',$ids)->update(['status'=>1]);
    }
    public function deleteList($ids){
        return $this->model->whereIn('id',$ids)->delete();
    }
    public function getListByDate($member_id,$date){
        $query = $this->model->whereIn('status',['1','2']);
        $member = \App\Member::find($member_id);
        $todo_ids = $member->todo->pluck('id')->toArray();
        if(count($todo_ids) > 0){
            $query = $query->where(function($query) use ($todo_ids,$member_id) {
                $query->where('created_by',$member_id)
                      ->orWhereIn('id',$todo_ids);
            });
        }else{
            $query = $query->where('created_by',$member_id);
        }
        return $query->whereDate('end_date',$date)->get();
    }
    public function getListByMember($member_id,$date){
        $member = \App\Member::find($member_id);
        $todo_ids = $member->todo->pluck('id')->toArray();
        $query = $this->model;
        if(count($todo_ids) > 0){
            $query = $query->where(function($query) use ($todo_ids,$member_id) {
                $query->where('created_by',$member_id)
                      ->orWhereIn('id',$todo_ids);
            });
        }else{
            $query = $query->where('created_by',$member_id);
        }
        return $query->whereDate('end_date',$date)->whereIn('status',[1,2])->get();
    }
    public function getList($start_date,$end_date,$member){
        $query = $this->model->whereIn('status',['1','2']);
        $todo_ids = $member->todo->pluck('id')->toArray();
        if(count($todo_ids) > 0){
            $query = $query->where(function($query) use ($todo_ids,$member) {
                $query->where('created_by',$member->id)
                      ->orWhereIn('id',$todo_ids);
            });
        }else{
            $query = $query->where('created_by',$member->id);
        }
        $query = $query->where('start_date','<=',$end_date)->where('end_date','>=',$start_date);
        $data = $query->get();
        return $data;

    }
    public function getListByPattern($member_ids,$status,$pattern,$time,$groupBy){
        $now = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');
        $lastQuarter = $now->quarter;
        $query = $this->model->rightjoin('member_todo','todo.id','=','member_todo.todo_id')->whereIn('member_id',$member_ids)->where('join','<>',2);
        if($status !== ''){
            if($status == 3){
                $query = $query->where('todo.status',3);
                if($pattern !== ''){
                    switch ($pattern){
                        case 1:
                            $query = $query->whereColumn('todo.complete_date','<','todo.end_date');
                            break;
                        case 2:
                            $query = $query->whereColumn('todo.complete_date','=','todo.end_date');
                            break;
                        case 3:
                            $query = $query->whereColumn('todo.complete_date','>','todo.end_date');
                            break;
                    }
                }
            }else{
               $query = $query->where('todo.status','<',3);
            }
        }
        switch ($time){
                case 'week':
                    $query = $query->whereDate('todo.start_date','<=',$weekEndDate)->whereDate('todo.end_date','>=',$weekStartDate);
                            break;
                case 'month':
                    $query = $query->where(function($query) {
                                $query->where(function($query) {
                                        $query->whereMonth('start_date', date('m'))
                                              ->whereYear('start_date',date('Y'));
                                    })->orWhere(function($query) {
                                        $query->whereMonth('end_date', date('m'))
                                              ->whereYear('end_date',date('Y'));
                                    });
                            });
                            break;
                case 'quater':
                    $query = $query->where(function($query) use ($lastQuarter) {
                                    $query->where(function($query) use ($lastQuarter) {
                                        $query->where(DB::raw('QUARTER(start_date)'), $lastQuarter);
                                    })->orWhere(function($query) use ($lastQuarter){
                                        $query->where(DB::raw('QUARTER(end_date)'), $lastQuarter);
                                    });
                               });
                               break;
                case 'year':
                    $query = $query->where(function($query) {
                                    $query->where(function($query) {
                                        $query->whereYear('start_date',date('Y'));
                                    })->orWhere(function($query){
                                        $query->whereYear('end_date',date('Y'));
                                    });
                               });
                               break;
        }
        if($groupBy == TRUE){
            $data = $query->select('todo.*')->groupBy('todo.id','todo.status')->orderBy('todo.end_date','ASC')->get();
        }else{
            $data = $query->select('todo.*')->groupBy('todo.id','todo.status')->orderBy('todo.end_date','ASC')->get();
        }
        return $data;
    }
}
