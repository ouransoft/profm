<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class LevelRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Level';
    }
    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:level'
        ];
    }
    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required|unique:level,name,' . $id . ',id'
        ];
    }
    public function getAll(){
        return $this->model->get();
    }
}
