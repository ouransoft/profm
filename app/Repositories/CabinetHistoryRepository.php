<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class CabinetHistoryRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model(){
        return 'App\CabinetHistory';
    }
    public function getByCabinet($cabinet_id){
        return $this->model->where('cabinet_id',$cabinet_id)->get();
    }
}

