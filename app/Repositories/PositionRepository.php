<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class PositionRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Position';
    }
    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:position'
        ];
    }
    public function validateUpdate($id) {
        return $rules = [
            'name' => 'required|unique:position,name,' . $id . ',id'
        ];
    }
    public function getAll(){
        return $this->model->get();
    }
    public function checkName($name){
        return $this->model->where('name',$name)->count();
    }
}
