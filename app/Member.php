<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Member extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;
    protected $table = 'member';
    protected $fillable = ['login_id', 'password','status','full_name','department_id','level','note','is_deleted','position_id','email','api_token','company_id','device_token','phone','address','session_id','company_id'];
    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }
    const LEVEL_ARR = [['id'=>1,'name'=>1],['id'=>2,'name'=>2],['id'=>3,'name'=>3],['id'=>4,'name'=>4],['id'=>5,'name'=>5]];
    const LEVEL_NV = 1;
    const LEVEL_D1 = 2;
    const LEVEL_D2 = 3;
    const LEVEL_TK  = 4;
    const LEVEL_ADMIN = 5;
    public function department(){
        return $this->belongsTo('\App\Department');
    }
    public function position(){
        return $this->belongsTo('\App\Position');
    }
    public function project(){
        return $this->hasMany('\App\Project');
    }
     public function schedule() {
        return $this->belongsToMany('\App\Schedule', 'member_schedule', 'member_id', 'schedule_id');
    }
    public function seen() {
        return $this->belongsToMany('\App\Schedule', 'seen_schedule', 'member_id', 'schedule_id');
    }
    public function todo(){
        return $this->belongsToMany('\App\Todo', 'member_todo', 'member_id', 'todo_id')->withPivot('status', 'reason', 'join','complete_date','pattern');;
    }
    public function membertodo(){
        return $this->hasMany('\App\MemberTodo');
    }
    public function group(){
        return $this->belongsToMany('\App\Group', 'group_member', 'group_id', 'member_id')->withPivot('name');
    }
    public function readMessage(){
        return $this->belongsToMany(Group::class, 'read_message', 'group_id', 'member_id')->withPivot('message_id','seen');
    }
    public function file(){
        return $this->hasMany('\App\File','relationship_id','id')->where('type',\App\File::TYPE_MEMBER)->where('format',\App\File::FORMAT_IMAGE)->first();
    }
}
