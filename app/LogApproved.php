<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogApproved extends Model
{
    protected $table = 'log_approved';
    protected $fillable = ['member_id','project_id', 'level','progress','comment','status','reason'];
    const STATUS_CANCEL = 0;
    const STATUS_PENDDING = 1;
    const STATUS_ACTIVE = 2;
    public function created_at(){
        return date("d/m/Y", strtotime($this->created_at));
    }
    public function updated_at(){
        return date("d/m/Y", strtotime($this->created_at));
    }
    public function member(){
        return $this->belongsTo('\App\Member');
    }
}
