<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    //
    protected $table = "project";
    protected $fillable = [
        'name', 'member_id', 'before_content', 'after_content', 'status','level','is_deleted','is_saved','reason',
        'type','is_destroy','created_at','approved_at','reason_update','sort_date','pattern','consider'
    ];
    const Progress_arr = [1=>'Submit_the_project',2=>'Approved_1_time',3=>'Approved_2_time',4=>'Assign',5=>'Report',6=>'Completed'];
    const Progress_arr1 = [1=>'Submit_the_project',2=>'Approved_1_time',3=>'Approved_2_time'];
    const STATUS_LEVEL_2 = 1;
    const STATUS_LEVEL_3 = 2;
    const STATUS_ACTIVE = 3;
    const STATUS_INTIVE = 4;
    const STATUS_CANCEL = 5;
    const TYPE_PERSONAL = 1;
    const TYPE_TEAM = 2;
    public function member() {
        return $this->belongsTo('\App\Member');
    }
    public function created_at(){
        return date('d/m/Y',strtotime($this->created_at));
    }
    public function logapproved(){
        return $this->hasMany('\App\LogApproved');
    }
    public function levels(){
        return $this->belongsTo('\App\Level','level','id');
    }
    public function memberproject(){
        return $this->hasMany('\App\MemberProject');
    }
    public function before_files(){
        return $this->hasMany('\App\File','relationship_id')->where('type',3)->where('format',2);
    }
    public function after_files(){
        return $this->hasMany('\App\File','relationship_id')->where('type',4)->where('format',2);
    }
    public function before_images(){
        return $this->hasMany('\App\File','relationship_id')->where('type',5)->where('format',1);
    }
    public function after_images(){
        return $this->hasMany('\App\File','relationship_id')->where('type',6)->where('format',1);
    }
}
