<?php

Route::group(['middleware' => 'frontend'], function() {
    Route::get('/home', ['as' => 'home', 'uses' => 'Frontend\FrontendController@home']);
    Route::get('/dashboard', ['as' => 'frontend.dashboard.index', 'uses' => 'Frontend\FrontendController@dashboard']);
    Route::get('/create', ['as' => 'home.create', 'uses' => 'Frontend\FrontendController@create']);
    Route::get('/', ['as' => 'home.view', 'uses' => 'Frontend\FrontendController@view']);
    // Department
    Route::get('/department', ['as' => 'frontend.department.index', 'uses' => 'Frontend\DepartmentController@index']);
    Route::delete('/department/delete/{id}', ['as' => 'frontend.department.destroy', 'uses' => 'Frontend\DepartmentController@destroy']);
    //Level
    Route::get('/level', ['as' => 'frontend.level.index', 'uses' => 'Frontend\LevelController@index']);
    Route::delete('/level/delete/{id}', ['as' => 'frontend.level.destroy', 'uses' => 'Frontend\LevelController@destroy']);
    //Position
    Route::get('/position', ['as' => 'frontend.position.index', 'uses' => 'Frontend\PositionController@index']);
    Route::delete('/position/delete/{id}', ['as' => 'frontend.position.destroy', 'uses' => 'Frontend\PositionController@destroy']);
    //Slide
    Route::get('/slide', ['as' => 'frontend.slide.index', 'uses' => 'Frontend\SlideController@index']);
    Route::delete('/slide/delete/{id}', ['as' => 'frontend.slide.destroy', 'uses' => 'Frontend\SlideController@destroy']);
    //Config
    Route::get('/configs', ['as' => 'frontend.config.index', 'uses' => 'Frontend\ConfigController@index']);
    Route::post('/configs/update', ['as' => 'frontend.config.update', 'uses' => 'Frontend\ConfigController@update']);
    Route::get('/equipment', ['as' => 'frontend.equipment.index', 'uses' => 'Frontend\EquipmentController@index']);
    Route::delete('/equipment/delete/{id}', ['as' => 'frontend.equipment.destroy', 'uses' => 'Frontend\EquipmentController@destroy']);
    //Chat
    Route::get('/chats', ['as' => 'frontend.chat.index', 'uses' => 'Frontend\ChatController@index']);

    // Member
    Route::get('/member', ['as' => 'frontend.member.index', 'uses' => 'Frontend\MemberController@index']);
    Route::get('/member-deleted', ['as' => 'frontend.member.history', 'uses' => 'Frontend\MemberController@history']);
    Route::get('/profile', ['as' => 'frontend.member.profile', 'uses' => 'Frontend\MemberController@profile']);
    Route::post('/member/stote', ['as' => 'frontend.member.store', 'uses' => 'Frontend\MemberController@store']);
    Route::post('/member/update/{id}', ['as' => 'frontend.member.update', 'uses' => 'Frontend\MemberController@update']);
    Route::get('/member', ['as' => 'frontend.member.index', 'uses' => 'Frontend\MemberController@index']);

    //Project
    Route::get('/project/list', ['as' => 'frontend.project.list', 'uses' => 'Frontend\ProjectController@listProject']);
    Route::get('/project/view/{id}', ['as' => 'frontend.project.view', 'uses' => 'Frontend\ProjectController@view']);
    Route::get('/project/list', ['as' => 'frontend.project.list', 'uses' => 'Frontend\ProjectController@listProject']);
    Route::get('/project/view/{id}', ['as' => 'frontend.project.view', 'uses' => 'Frontend\ProjectController@view']);
    Route::get('/project', ['as' => 'frontend.project.index', 'uses' => 'Frontend\ProjectController@index']);
    Route::get('/project/create', ['as' => 'frontend.project.create', 'uses' => 'Frontend\ProjectController@create']);
    Route::get('/project/edit/{id}', ['as' => 'frontend.project.edit', 'uses' => 'Frontend\ProjectController@edit']);
    Route::post('/project/store', ['as' => 'frontend.project.store', 'uses' => 'Frontend\ProjectController@store']);
    Route::post('/project/update/{id}', ['as' => 'frontend.project.update', 'uses' => 'Frontend\ProjectController@update']);
    Route::get('/project/fillter/{keyword}', ['as' => 'frontend.project.fillter', 'uses' => 'Frontend\ProjectController@fillter']);
    Route::get('/project/chart', ['as' => 'frontend.project.chart', 'uses' => 'Frontend\ProjectController@chart']);
    Route::get('/project/approved/{id}', ['as' => 'frontend.project.approved', 'uses' => 'Frontend\ProjectController@approved']);

    // Schedule
    Route::get('/schedule/index', ['as' => 'frontend.schedule.index', 'uses' => 'Frontend\ScheduleController@index']);
    Route::get('/schedule/view/{id}', ['as' => 'frontend.schedule.view', 'uses' => 'Frontend\ScheduleController@view']);
    Route::get('/schedule/create', ['as' => 'frontend.schedule.create', 'uses' => 'Frontend\ScheduleController@create']);
    Route::post('/schedule/store', ['as' => 'frontend.schedule.store', 'uses' => 'Frontend\ScheduleController@store']);
    Route::get('/schedule/edit/{id}', ['as' => 'frontend.schedule.edit', 'uses' => 'Frontend\ScheduleController@edit']);
    Route::get('/schedule/edit_all/{id}', ['as' => 'frontend.schedule.edit_all', 'uses' => 'Frontend\ScheduleController@editAll']);
    Route::get('/schedule/edit_repeat/{id}', ['as' => 'frontend.schedule.edit_repeat', 'uses' => 'Frontend\ScheduleController@editRepeat']);
    Route::get('/schedule/edit_task/{id}', ['as' => 'frontend.schedule.edit_task', 'uses' => 'Frontend\ScheduleController@editTask']);
    Route::delete('/schedule/delete/{id}', ['as' => 'frontend.schedule.destroy', 'uses' => 'Frontend\ScheduleController@destroy']);
    Route::get('/schedule/calendar', ['as' => 'frontend.schedule.calendar', 'uses' => 'Frontend\ScheduleController@calendar']);
    Route::get('/schedule/respopup', ['as' => 'frontend.schedule.respopup', 'uses' => 'Frontend\ScheduleController@respopup']);
    Route::get('/schedule/seen', ['as' => 'frontend.schedule.seen', 'uses' => 'Frontend\ScheduleController@seen']);
    Route::get('/schedule/detail', ['as' => 'frontend.schedule.detail', 'uses' => 'Frontend\ScheduleController@detail']);
    Route::get('/schedule/personal_day', ['as' => 'frontend.schedule.personal_day', 'uses' => 'Frontend\ScheduleController@personalDay']);
    Route::get('/schedule/personal_week', ['as' => 'frontend.schedule.personal_week', 'uses' => 'Frontend\ScheduleController@personalWeek']);
    Route::get('/schedule/personal_month', ['as' => 'frontend.schedule.personal_month', 'uses' => 'Frontend\ScheduleController@personalMonth']);
    Route::get('/schedule/personal_year', ['as' => 'frontend.schedule.personal_year', 'uses' => 'Frontend\ScheduleController@personalYear']);
    Route::get('/schedule/group_day', ['as' => 'frontend.schedule.group_day', 'uses' => 'Frontend\ScheduleController@groupDay']);
    Route::post('/schedule/update/{id}', ['as' => 'frontend.schedule.update', 'uses' => 'Frontend\ScheduleController@update']);
    Route::get('/schedule/popup_calendar', ['as' => 'frontend.schedule.popup_calendar', 'uses' => 'Frontend\ScheduleController@popupCalendar']);
    Route::get('/schedule/popup_time_selector', ['as' => 'frontend.schedule.popup_time_selector', 'uses' => 'Frontend\ScheduleController@popupTimeSelector']);
    Route::get('/schedule/create_all_day', ['as' => 'frontend.schedule.create_all_day', 'uses' => 'Frontend\ScheduleController@createAllDay']);
    Route::get('/schedule/create_task', ['as' => 'frontend.schedule.create_task', 'uses' => 'Frontend\ScheduleController@createTask']);
    Route::get('/schedule/create_repeat', ['as' => 'frontend.schedule.create_repeat', 'uses' => 'Frontend\ScheduleController@create']);
    Route::get('/schedule/edit_all/{id}', ['as' => 'frontend.schedule.edit_all', 'uses' => 'Frontend\ScheduleController@editAll']);
    Route::get('/schedule/leave/{id}', ['as' => 'frontend.schedule.leave', 'uses' => 'Frontend\ScheduleController@leave']);
    Route::get('/schedule/participate/{id}', ['as' => 'frontend.schedule.participate', 'uses' => 'Frontend\ScheduleController@participate']);
    Route::get('/schedule/schedule_navi_calendar_month', ['as' => 'frontend.schedule.schedule_navi_calendar_month', 'uses' => 'Frontend\ScheduleController@calendarMonth']);
    Route::match(array('GET','POST'),'/schedule/popup_member_select', 'Frontend\ScheduleController@popupMemberSelect');
    Route::match(array('GET','POST'),'/schedule/popup_user_select', 'Frontend\ScheduleController@popupUserSelect');
    Route::match(array('GET','POST'),'/schedule/confirm', 'Frontend\ScheduleController@confirm');

    // Todo
    Route::get('/todo', ['as' => 'frontend.todo.index', 'uses' => 'Frontend\ToDoController@index']);
    Route::get('/todo/history', ['as' => 'frontend.todo.history', 'uses' => 'Frontend\ToDoController@history']);
    Route::get('/todo/statistical', ['as' => 'frontend.todo.statistical', 'uses' => 'Frontend\ToDoController@statistical']);
    Route::get('/todo/create', ['as' => 'frontend.todo.create', 'uses' => 'Frontend\ToDoController@create']);
    Route::post('/todo/store', ['as' => 'frontend.todo.store', 'uses' => 'Frontend\ToDoController@store']);
    Route::get('/todo/view/{id}', ['as' => 'frontend.todo.view', 'uses' => 'Frontend\ToDoController@view']);
    Route::get('/todo/edit/{id}', ['as' => 'frontend.todo.edit', 'uses' => 'Frontend\ToDoController@edit']);
    Route::post('/todo/update/{id}', ['as' => 'frontend.todo.update', 'uses' => 'Frontend\ToDoController@update']);
    Route::post('/todo/update-status', ['as' => 'frontend.todo.update_status', 'uses' => 'Frontend\ToDoController@updateStatus']);
    Route::delete('/todo/delete/{id}', ['as' => 'frontend.todo.destroy', 'uses' => 'Frontend\ToDoController@destroy']);
    Route::post('/todo/delete-multi', ['as' => 'frontend.todo.destroymulti', 'uses' => 'Frontend\ToDoController@destroyMulti']);
    Route::post('/todo/delete-all', ['as' => 'frontend.todo.destroyall', 'uses' => 'Frontend\ToDoController@destroyAll']);
    Route::get('/todo/join/{id}', ['as' => 'frontend.todo.join', 'uses' => 'Frontend\ToDoController@join']);
    Route::post('/todo/unjoin', ['as' => 'frontend.todo.unjoin', 'uses' => 'Frontend\ToDoController@unjoin']);
    Route::post('/todo/change-status', ['as' => 'frontend.todo.changeStatus', 'uses' => 'Frontend\ToDoController@changeStatus']);
    Route::get('/todo/confirm/{id}', ['as' => 'frontend.todo.confirm', 'uses' => 'Frontend\ToDoController@confirm']);
    Route::get('/todo/unconfirm/{id}', ['as' => 'frontend.todo.return', 'uses' => 'Frontend\ToDoController@unconfirm']);
    // Todo
    Route::get('/tpm', ['as' => 'frontend.tpm.index', 'uses' => 'Frontend\TPMController@index']);
    

    Route::get('/check-schedule1/{member_id}', ['as' => 'api.schedule.check', 'uses' => 'Frontend\ScheduleController@checkMember1']);
    Route::get('/check-schedule2/{member_id}', ['as' => 'api.schedule.check', 'uses' => 'Frontend\ScheduleController@checkMember2']);
    Route::get('/check-equipment-schedule1/{equipment_id}', ['as' => 'api.schedule.check', 'uses' => 'Frontend\ScheduleController@checkEquipment1']);
    Route::get('/check-equipment-schedule2/{equipment_id}', ['as' => 'api.schedule.check', 'uses' => 'Frontend\ScheduleController@checkEquipment2']);
    //Start messenger
    Route::get('/messenger', ['as' => 'frontend.messenger.index', 'uses' => 'Frontend\FrontendController@listMessenger']);
    Route::get('/messenger/detail/{id}', ['as' => 'frontend.messenger.detail', 'uses' => 'Frontend\FrontendController@detailMessenger']);
    //end messenger
    //Start notification
    Route::get('/notification', ['as' => 'frontend.notification.index', 'uses' => 'Frontend\FrontendController@listNotification']);
    //end messenger
});
