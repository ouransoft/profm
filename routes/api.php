<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
/*
  Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
  });
 */
Route::post('/mobile/login', ['uses' => 'Api\MobileController@login']);
Route::group(['middleware' => 'auth:api'], function() {

Route::group(['prefix' => '/mobile'], function() {
   Route::get('/dashboard', ['uses' => 'Api\MobileController@dashboard']);
   Route::get('/search/member', ['uses' => 'Api\MobileController@searchMember']);
   Route::get('/search/member-approved', ['uses' => 'Api\MobileController@searchMemberApproved']);
   Route::get('/search/equip', ['uses' => 'Api\MobileController@searchEquipment']);
   Route::get('/notification', ['uses' => 'Api\MobileController@notification']);
   Route::get('/info', ['uses' => 'Api\MobileController@info']);
   Route::post('/info/update', ['uses' => 'Api\MobileController@infoUpdate']);
   Route::post('/info/update-password', ['uses' => 'Api\MobileController@updatePassword']);
   //Todo
   Route::get('/todolist', ['uses' => 'Api\MobileController@todolist']);
   Route::post('/todolist/update', ['uses' => 'Api\MobileController@updateTodolist']);
   Route::post('/todolist/store', ['uses' => 'Api\MobileController@storeTodolist']);
   Route::post('/todolist/delete', ['uses' => 'Api\MobileController@deleteTodolist']);
   Route::get('/todolist/edit-progress', ['uses' => 'Api\MobileController@editProgress']);
   Route::post('/todolist/update-progress', ['uses' => 'Api\MobileController@updateProgress']);
   Route::get('/todolist/detail', ['uses' => 'Api\MobileController@detailTodolist']);
   Route::post('/todolist/join', ['uses' => 'Api\MobileController@joinTodolist']);

   //Schedule
   Route::get('/schedule', ['uses' => 'Api\MobileController@schedule']);
   Route::get('/schedule/search', ['uses' => 'Api\MobileController@searchSchedule']);
   Route::get('/schedule/detail', ['uses' => 'Api\MobileController@detailSchedule']);
   Route::post('/schedule/delete', ['uses' => 'Api\MobileController@deleteSchedule']);
   Route::post('/schedule/leave', ['uses' => 'Api\MobileController@leaveSchedule']);
   Route::post('/schedule/update', ['uses' => 'Api\MobileController@updateSchedule']);
   Route::post('/schedule/store', ['uses' => 'Api\MobileController@storeSchedule']);

   //Statistical
   Route::get('/statistical', ['uses' => 'Api\MobileController@statistical']);

   //Project
   Route::get('/project/list', ['uses' => 'Api\MobileController@projectList']);
   Route::get('/project', ['uses' => 'Api\MobileController@projectIndex']);
   Route::get('/project/view', ['uses' => 'Api\MobileController@projectView']);
   Route::get('/project/member-level', ['uses' => 'Api\MobileController@projectMemberLevel']);
   Route::post('/project/store', ['uses' => 'Api\MobileController@projectStore']);
   Route::post('/send-project', ['as' => 'api.send-project', 'uses' => 'Api\ProjectController@send']);
   Route::post('/return-project', ['as' => 'api.return-project', 'uses' => 'Api\ProjectController@returnProject']);
   Route::post('/get-history',['uses'=>'Api\ProjectController@getHistory']);

   Route::post('/chat/sendtext', ['uses' => 'Api\MobileController@sendText']);
   Route::get('/chat/sendfile', ['uses' => 'Api\MobileController@sendFile']);

});
//Member
Route::post('/edit-list-member', ['as' => 'api.edit-list-member', 'uses' => 'Api\MemberController@editList']);
Route::post('/edit-detail-member', ['as' => 'api.edit-detail-member', 'uses' => 'Api\MemberController@editDetail']);
Route::post('/update-member', ['as' => 'api.update-member', 'uses' => 'Api\MemberController@update']);
Route::post('/remove-member', ['as' => 'api.remove-member', 'uses' => 'Api\MemberController@destroy']);
Route::post('/list-member', ['as' => 'api.list-member', 'uses' => 'Api\MemberController@listMember']);
Route::post('/list-member-remove', ['as' => 'api.list-member-remove', 'uses' => 'Api\MemberController@listMemberRemove']);
Route::post('/restore-member', ['as' => 'api.restore-member', 'uses' => 'Api\MemberController@restoreMember']);
Route::post('/search-member', ['as' => 'api.search-member', 'uses' => 'Api\MemberController@searchMember']);
Route::post('/check-password', ['as' => 'api.check-password', 'uses' => 'Api\MemberController@checkPassword']);
Route::post('/reset-password', ['as' => 'api.reset-password', 'uses' => 'Api\MemberController@resetPassword']);
Route::post('/getInfoMember', ['as' => 'api.info-member', 'uses' => 'Api\MemberController@getInfo']);
Route::post('/import-member', ['as' => 'api.member.import', 'uses' => 'Api\MemberController@import']);
Route::post('/export-member', ['as' => 'api.member.export', 'uses' => 'Api\MemberController@export']);
Route::post('/getInfoMemberMoblie', ['as' => 'api.info-member-mobile', 'uses' => 'Api\MemberController@getInfoMobile']);
Route::post('/checkid-member', ['as' => 'api.member.checkid', 'uses' => 'Api\MemberController@checkID']);
Route::post('/update-device-token',['uses'=>'Api\MemberController@updateToken']);
Route::post('/getListName',['uses'=>'Api\MemberController@getListName']);
Route::post('/get-list-member-html',['uses'=>'Api\MemberController@getListHtml']);
Route::post('/update-info', ['as' => 'api.member.update-info', 'uses' => 'Api\MemberController@updateInfo']);

//Project
Route::post('/search-project', ['as' => 'api.search-project', 'uses' => 'Api\ProjectController@searchProject']);
Route::post('/save-project', ['as' => 'api.save-project', 'uses' => 'Api\ProjectController@save']);
Route::post('/send-project', ['as' => 'api.send-project', 'uses' => 'Api\ProjectController@send']);
Route::post('/remove-project', ['as' => 'api.remove-project', 'uses' => 'Api\ProjectController@destroy']);
Route::post('/getListProject', ['as' => 'api.get-project', 'uses' => 'Api\ProjectController@getListProject']);
Route::post('/return-project', ['as' => 'api.return-project', 'uses' => 'Api\ProjectController@returnProject']);
Route::post('/export-project', ['as' => 'api.project.export', 'uses' => 'Api\ProjectController@export']);
Route::post('/export-member-project', ['as' => 'api.project.export', 'uses' => 'Api\ProjectController@exportProject']);
Route::post('/send-assign',['uses'=>'Api\ProjectController@addAssign']);
Route::post('/get-history',['uses'=>'Api\ProjectController@getHistory']);
Route::post('/report-project',['uses'=>'Api\ProjectController@report']);
Route::post('/unapproved-project',['uses'=>'Api\ProjectController@unapproved']);
Route::post('/change-member-approved','Api\ProjectController@changeMemberApproved');
Route::post('/complete-project','Api\ProjectController@complete');

//Department
Route::post('/add-department', ['as' => 'api.department.store', 'uses' => 'Api\FrontendController@addDepartment']);
Route::post('/edit-department', ['as' => 'api.department.edit', 'uses' => 'Api\FrontendController@editDepartment']);
Route::post('/update-department', ['as' => 'api.department.update', 'uses' => 'Api\FrontendController@updateDepartment']);
Route::post('/delete-department', ['as' => 'api.department.update', 'uses' => 'Api\FrontendController@deleteDepartment']);
Route::post('/check-delete-department','Api\FrontendController@checkDeleteDepartment');

//Level
Route::post('/add-level', ['as' => 'api.level.store', 'uses' => 'Api\FrontendController@addLevel']);
Route::post('/edit-level', ['as' => 'api.level.edit', 'uses' => 'Api\FrontendController@editLevel']);
Route::post('/update-level', ['as' => 'api.level.update', 'uses' => 'Api\FrontendController@updateLevel']);

//Position
Route::post('/add-position', ['as' => 'api.position.store', 'uses' => 'Api\FrontendController@addPosition']);
Route::post('/edit-position', ['as' => 'api.position.edit', 'uses' => 'Api\FrontendController@editPosition']);
Route::post('/update-position', ['as' => 'api.position.update', 'uses' => 'Api\FrontendController@updatePosition']);
Route::post('/check-delete-position','Api\FrontendController@checkDeletePosition');

//Slide
Route::post('/add-slide', ['as' => 'api.slide.store', 'uses' => 'Api\FrontendController@addSlide']);
Route::post('/edit-slide', ['as' => 'api.slide.edit', 'uses' => 'Api\FrontendController@editSlide']);
Route::post('/update-slide', ['as' => 'api.slide.update', 'uses' => 'Api\FrontendController@updateSlide']);

//Equipment
Route::post('/add-equipment', ['as' => 'api.equipment.store', 'uses' => 'Api\FrontendController@addEquipment']);
Route::post('/edit-equipment', ['as' => 'api.equipment.edit', 'uses' => 'Api\FrontendController@editEquipment']);
Route::post('/update-equipment', ['as' => 'api.equipment.update', 'uses' => 'Api\FrontendController@updateEquipment']);

//Schedule
Route::post('/next-schedule', ['as' => 'api.schedule.next', 'uses' => 'Api\ScheduleController@next']);
Route::post('/prev-schedule', ['as' => 'api.schedule.prev', 'uses' => 'Api\ScheduleController@prev']);
Route::post('/change-schedule', ['as' => 'api.schedule.change', 'uses' => 'Api\ScheduleController@change']);
Route::post('/get-list-info', ['as' => 'api.schedule.getlist', 'uses' => 'Api\ScheduleController@getListInfo']);
Route::post('/check-schedule', ['as' => 'api.schedule.check', 'uses' => 'Api\ScheduleController@check']);
Route::post('/check-duplicate_schedule', ['as' => 'api.schedule.checkduplicate', 'uses' => 'Api\ScheduleController@checkDuplicate']);
Route::post('/ajax_user_add_select_by_group', ['as' => 'api.schedule.ajax_user_add_select_by_group', 'uses' => 'Api\ScheduleController@getUserSelect']);
Route::post('/uploaded_files', ['as' => 'api.schedule.uploaded_files', 'uses' => 'Api\ScheduleController@uploadedFiles']);
Route::post('/command_add_tmp_file', ['as' => 'api.schedule.addFile', 'uses' => 'Api\ScheduleController@addFile']);
Route::post('/search_facility', ['as' => 'api.schedule.addFile', 'uses' => 'Api\ScheduleController@seachFacility']);
Route::post('/search_members_by_keyword', ['as' => 'api.schedule.searchMember', 'uses' => 'Api\MemberController@seachMemberByKeyword']);
Route::post('/add_file_cabinet', ['as' => 'api.schedule.addFileCabinet', 'uses' => 'Api\CabinetController@addFile']);
Route::post('/load-schedule', ['as' => 'api.schedule.load', 'uses' => 'Api\ScheduleController@loadSchedule']);
Route::post('/ajax_user_org_facility_list', ['as' => 'api.member.search', 'uses' => 'Api\MemberController@searchMemberByName']);
Route::get('/schedule_navi_calendar', ['as' => 'api.schedule.navi_calendar', 'uses' => 'Api\ScheduleController@naviCalendar']);
Route::get('/schedule_navi_calendar_month', ['as' => 'api.schedule.schedule_navi_calendar_month', 'uses' => 'Api\ScheduleController@naviCalendarMonth']);
Route::post('/command_navi_calendar_display', ['as' => 'api.schedule.navi_calendar_display', 'uses' => 'Api\ScheduleController@naviCalendarDisplay']);
Route::post('/ajax_select_user_org_facility_dialog', ['as' => 'api.schedule.select_user', 'uses' => 'Api\ScheduleController@selectUser']);
Route::post('/get_more_member_select_list_in_popup', ['as' => 'api.schedule.get_more_member', 'uses' => 'Api\ScheduleController@getMoreMember']);
Route::post('/ajax_get_user_for_mobile_selection', ['as' => 'api.schedule.get_user_facility', 'uses' => 'Api\MemberController@getUserForMobile']);
Route::post('/accessible_facility', ['as' => 'api.schedule.get_list_equipment', 'uses' => 'Api\EquipmentController@getList']);
Route::post('/load-notification', ['as' => 'api.schedule.load_notification', 'uses' => 'Api\NotificationController@loadList']);
Route::post('/search-attendees', ['as' => 'api.member.search-attendees', 'uses' => 'Api\MemberController@searchAttendees']);
Route::post('/upload-file-mobile', ['as' => 'api.file.upload', 'uses' => 'Api\ScheduleController@addFileMobile']);
Route::post('/get-schedule-by-month', ['as' => 'api.getschedule', 'uses' => 'Api\ScheduleController@getScheduleByMonth']);
Route::post('/load-schedule-day', ['as' => 'api.getscheduleday', 'uses' => 'Api\ScheduleController@getScheduleByDay']);

//Todo
Route::post('/create-todolist', ['as' => 'api.todolist.add', 'uses' => 'Api\ToDolistController@add']);
Route::post('/change-status-todolist', ['as' => 'api.todolist.change', 'uses' => 'Api\ToDolistController@change']);
Route::post('/ajax_event_list', ['as' => 'api.schedule.list', 'uses' => 'Api\ScheduleController@ajaxEventList']);
Route::post('/ajax_user_org_facility_list', ['as' => 'api.schedule.list', 'uses' => 'Api\ScheduleController@ajaxEventList']);
Route::post('/load-todo', ['as' => 'api.todo.load', 'uses' => 'Api\ToDoController@loadTodo']);
Route::post('/complete-list-todo', ['as' => 'api.todo.complete', 'uses' => 'Api\ToDoController@completeList']);
Route::post('/delete-list-todo', ['as' => 'api.todo.delete', 'uses' => 'Api\ToDoController@deleteList']);
Route::post('/folder_json', ['as' => 'api.folder.folder_json', 'uses' => 'Api\FolderController@getJson']);
Route::post('/send-comment', ['as' => 'api.comment.send', 'uses' => 'Api\CommentController@send']);
Route::post('/delete-comment', ['as' => 'api.comment.delete', 'uses' => 'Api\CommentController@delete']);
Route::post('/getSelectStatus', ['as' => 'api.todo.getSelectStatus', 'uses' => 'Api\ToDoController@getSelectStatus']);
Route::post('/update-level-project',['uses'=>'Api\ProjectController@updateLevel']);
Route::post('/checkProject',['uses'=>'Api\ProjectController@checkName']);
Route::post('/getSelectLevel',['uses'=>'Api\ProjectController@getSelectLevel']);
Route::post('/command_add_tmp_file_todo','Api\ToDoController@addFile');
Route::post('/delete-todo','Api\ToDoController@destroy');
Route::post('/delete-todo-all','Api\ToDoController@destroyAll');
Route::post('/upload-file-todo-mobile','Api\ToDoController@addFileMobile');
Route::post('/get-chart-todo','Api\ToDoController@getChart');
Route::post('/get-todo','Api\ToDoController@getTodo');
Route::post('/get-statistical-todo','Api\ToDoController@getStatistical');
Route::post('/get-list-todo','Api\ToDoController@getList');

//Other
Route::post('/add-group','Api\GroupController@store');
Route::post('/get-group-message','Api\GroupController@getMessage');
Route::post('/register-company','Api\CompanyController@store');
Route::post('/upload', ['as' => 'api.upload', 'uses' => 'Api\FrontendController@upload']);
Route::post('/upload-image-project', ['as' => 'api.upload', 'uses' => 'Api\FrontendController@uploadImageProject']);
Route::post('/uploadImage', ['as' => 'api.uploadimage', 'uses' => 'Api\FrontendController@uploadImage']);
Route::post('/delete_image', ['as' => 'api.upload', 'uses' => 'Api\FrontendController@delete_image']);
Route::post('/get-all-message', ['as' => 'api.get-all-message', 'uses' => 'Api\MemberController@getAllMessage']);
Route::post('/get-message','Api\MemberController@getMessage');
Route::post('/send-message','Api\MemberController@sendMessage');
Route::post('/send-file-message','Api\MemberController@sendFileMessage');
Route::post('/seen-notification', ['as' => 'frontend.project.seen', 'uses' => 'Api\MemberController@seenNotification']);
Route::post('/seen-all-notification', ['as' => 'api.notification.seenAll', 'uses' => 'Api\MemberController@seenNotificationAll']);
Route::post('/get-messenger', ['as' => 'api.messenger.get', 'uses' => 'Api\MemberController@getMessenger']);
Route::post('/searching-member', ['as' => 'api.searching.member', 'uses' => 'Frontend\ChatController@searchingMember']);
Route::post('/searching-member-group', ['as' => 'api.searching.member.group', 'uses' => 'Frontend\ChatController@searchingMemberGroup']);
Route::post('/get-list-member', ['as' => 'api.member.list', 'uses' => 'Frontend\ChatController@getListMember']);
Route::post('/create-group', ['as' => 'api.create.group', 'uses' => 'Frontend\ChatController@createGroup']);
Route::post('/leave-group', ['as' => 'api.leave.group', 'uses' => 'Frontend\ChatController@leaveGroup']);
Route::post('/add-member-group', ['as' => 'api.create.group', 'uses' => 'Frontend\ChatController@addMember']);
Route::post('/delete-message','Frontend\ChatController@deleteMessage');
Route::post('/upload','Api\FrontendController@upload');
Route::post('/upload-images','Api\FrontendController@uploadImages');
Route::post('/upload-files','Api\FrontendController@uploadFiles');
});
